
/**************************************************************************
 *
 *  $Id: mbgstatus.c 1.39 2022/12/21 15:46:41 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for mbgstatus program which demonstrates how to
 *    access a Meinberg device via IOCTL calls and prints device
 *    information.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgstatus.c $
 *  Revision 1.39  2022/12/21 15:46:41  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from some functions,
 *  and use feature check functions instead of macros.
 *  Revision 1.38  2022/07/19 16:07:11  martin.burnicki
 *  Account for a changed function name.
 *  Fixed a spelling error in a status message.
 *  Revision 1.37  2022/07/06 15:09:51  martin.burnicki
 *  Support SYN1588  insync range boundary code.
 *  Revision 1.36  2022/06/30 13:00:22  martin.burnicki
 *  Support reading XMR status information from devices that support this.
 *  New option -F to force reading XMR stuff if the feature is not enabled,
 *  which should only be used with very much care.
 *  Revision 1.35  2022/05/31 14:03:30  martin.burnicki
 *  Improved display of SYN1588 sync status.
 *  Revision 1.34  2022/04/01 15:01:09  martin.burnicki
 *  Print the firmware ID string in verbose mode.
 *  Revision 1.33  2021/11/15 17:08:52  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.32  2021/11/08 21:50:59  martin.burnicki
 *  Show SYN1588-specific info, if appropriate.
 *  Revision 1.31  2021/05/27 20:57:38  martin
 *  Refactured printing of GPS/UTC time offset and
 *  leap second announcement.
 *  Revision 1.30  2021/04/29 15:05:49  martin
 *  Support reading device CPU info.
 *  Revision 1.29  2021/04/12 21:58:05  martin
 *  Updated printing of usage information.
 *  Revision 1.28  2021/04/12 21:37:18  martin
 *  Use global year_limit.
 *  Revision 1.27  2021/03/21 22:40:31  martin
 *  Updated a bunch of comments.
 *  Revision 1.26  2021/03/12 12:32:20  martin
 *  Updated some comments.
 *  Revision 1.25  2020/08/17 16:08:14  martin
 *  If no device could be found, call a specific new function
 *  to print associated information.
 *  Revision 1.24  2020/03/31 08:18:34  martin
 *  Modified condition under which true GPS LS week number is displayed.
 *  Revision 1.23  2020/02/28 14:08:25  martin
 *  Enhanced display of UTC parameters / leap second
 *  information as well as BVAR status.
 *  Revision 1.22  2020/02/27 13:15:26  martin
 *  Fixed unprintable prefix in snprint_dms() in case the position is uninitialized.
 *  Started cleanup. First collect all information, then print it.
 *  UTC message says 'Last leap second probably' instead of 'eventually'.
 *  Print 'BVAR' status in verbose mode.
 *  Changed year limit to 1970.
 *  Fixed a typo in an earlier log message.
 *  Revision 1.20  2019/04/02 15:15:45  martin
 *  Also print raw GPS time in verbose mode.
 *  Removed obsolete debug code.
 *  Revision 1.19  2019/03/11 16:10:24  martin
 *  Also print memory mapped timestamp in verbose mode.
 *  Revision 1.18  2019/02/08 11:08:47  martin
 *  Also print EPLD name in verbose mode.
 *  Revision 1.17  2018/11/15 12:12:37  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.16  2018/07/03 15:28:55  martin
 *  Use -L rather than -l to display unique names of installed devices
 *  Now use -l to list installed devices.
 *  Also read and print TTM in verbose mode.
 *  Use specific API call to read IRIG debug status.
 *  Revision 1.15  2017/12/21 15:57:43  martin
 *  Improved displaying of UTC/leap second parameters.
 *  Revision 1.14  2017/07/05 18:34:46  martin
 *  New way to maintain version information.
 *  Support build on Windows.
 *  Show many more details, at different verbosity levels.
 *  Use more functions from common library modules.
 *  Use codes and inline functions from mbgerror.h.
 *  Proper return codes and exit codes.
 *  Revision 1.13  2009/09/29 15:02:16  martin
 *  Updated version number to 3.4.0.
 *  Revision 1.12  2009/07/24 14:02:59  martin
 *  Display LAN and PTP status of PTP cards.
 *  Updated version number to 3.3.0.
 *  Revision 1.11  2009/06/19 14:20:36  martin
 *  Display raw IRIG time with TCR cards which support this.
 *  Revision 1.10  2009/06/16 08:21:08  martin
 *  Intermediate version 3.1.0a.
 *  Display IRIG debug status, if supported by the card.
 *  Revision 1.9  2009/03/20 11:35:41  martin
 *  Updated version number to 3.1.0.
 *  Updated copyright year to include 2009.
 *  Display signal source after signal level.
 *  Display GPS UTC parameter info, if supported by the card.
 *  Display IRIG control bits, if supported by the card.
 *  Revision 1.8  2008/12/22 12:48:18  martin
 *  Updated description, copyright, revision number and string.
 *  Use unified functions from toolutil module.
 *  Warn if a PCI Express device with unsafe IRQ support is detected.
 *  Account for signed irq_num.
 *  Accept device name(s) on the command line.
 *  Don't use printf() without format, which migth produce warnings
 *  with newer gcc versions.
 *  Revision 1.7  2007/07/24 09:33:52  martin
 *  Fixed display of port and IRQ resources.
 *  Updated copyright to include 2007.
 *  Revision 1.6  2006/03/10 12:38:22  martin
 *  Fixed printing of sign in print_position().
 *  Revision 1.5  2004/11/08 15:41:56  martin
 *  Modified formatted printing of date/time string.
 *  Using type casts to avoid compiler warnings.
 *  Revision 1.4  2003/07/30 08:16:39  martin
 *  Also displays oscillator DAC values for GPS.
 *  Revision 1.3  2003/07/08 15:38:57  martin
 *  Call mbg_find_devices().
 *  Account for swap_doubles() now being called inside
 *  the mbgdevio API functions.
 *  Show IRQ number assigned to a device.
 *  Revision 1.2  2003/04/25 10:28:05  martin
 *  Use new functions from mbgdevio library.
 *  New program version v2.1.
 *  Revision 1.1  2001/09/17 15:08:59  martin
 *
 **************************************************************************/

// Include Meinberg headers.
#include <mbgdevio.h>
#include <mbgutil.h>
#include <mbg_xmr_util_devio.h>
#include <mbg_syn1588_util.h>
#include <mbgtimex.h>
#include <pcpslstr.h>
#include <toolutil.h>  // Common utility functions.
#include <lan_util.h>
#include <deviohlp.h>
#include <timeutil.h>
#include <str_util.h>
#include <nanotime.h>

// Include system headers.
#include <stdio.h>
#include <stdlib.h>


#define MBG_FIRST_COPYRIGHT_YEAR   2001
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.

static const char *pname = "mbgstatus";


static int loops;
static int list_only;               // List only which devices are installed.
static int list_only_unique_names;  // List only the unique names of all devices.
static long sleep_secs;
static long sleep_usecs;
static int verbose;
static int force_read;

static const char *ref_name[N_PCPS_REF]= PCPS_REF_NAMES_ENG;
static const char *icode_rx_names[N_ICODE_RX] = DEFAULT_ICODE_RX_NAMES;
static const char *osc_name[N_GPS_OSC] = DEFAULT_GPS_OSC_NAMES;

static int max_ref_offs_h = MBG_REF_OFFS_MAX / MINS_PER_HOUR;
static int invt_reason;

#define STR_INDENT  "  "
static const char str_indent[] = STR_INDENT;

static const char *wdays[7] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

LANGUAGE language;
CTRY ctry;



typedef struct
{
  bool is_syn1588;

  // bool has_receiver_info;
  // bool has_gps_data;
  // bool has_generic_io;
  // bool has_asic_version;
  // bool has_asic_features;
  bool has_xmr;

  // bool is_isa;
  // bool is_mca;
  // bool is_pci;
  // bool is_pci_express;
  bool is_usb;

  bool is_gps;
  bool is_gnss;

  bool is_lwr;
  bool is_dcf;
  bool has_pzf;
  bool is_msf;
  bool is_wwvb;
  bool is_jjy;

  bool is_tcr;
  // bool has_lan_intf;
  // bool has_ptp;
  // bool has_ptp_unicast;
  bool has_hr_time;
  bool has_fast_hr_timestamp;
  // bool has_time_scale;
  bool has_event_time;
  // bool has_ucap;
  // bool can_clr_ucap_buff;
  // bool has_tzdl;
  // bool has_pcps_tzdl;
  // bool has_tzcode;
  // bool has_tz;
  // bool has_irig;
  bool has_irig_rx;
  bool has_irig_tx;
  bool has_irig_ctrl_bits;
  bool has_raw_irig_data;
  bool has_irig_time;
  bool has_signal;
  // bool has_mod;
  bool has_sync_time;
  bool has_rcvr_pos;
  bool has_bvar_stat;
  bool has_stat_info;
  bool has_stat_info_mode;
  bool has_stat_info_svs;
  bool has_ttm_time;
  // bool has_serial;
  // bool has_serial_hs;
  // bool has_synth;
  // bool has_gpio;
  // bool has_cab_len;
  bool has_ref_offs;
  // bool has_opt_flags;
  bool has_utc_parm;
  // bool has_corr_info;
  // bool has_tr_distance;
  // bool has_debug_status;
  // bool has_evt_log;
  // bool has_dac_ctrl;


  PCPS_DEV dev_info;
  RECEIVER_INFO ri;

  PCPS_TIME t;
  bool pcps_time_avail;

  PCPS_HR_TIME ht;
  bool hr_time_avail;

  PCPS_TIME_STAMP ts;
  bool fast_hr_timestamp_avail;

  TTM ttm;
  bool ttm_time_avail;

  PCPS_TIME sync_time;
  bool sync_time_avail;

  CORR_INFO corr_info;
  bool corr_info_avail;

  IRIG_INFO irig_rx_info;
  bool irig_rx_info_avail;

  MBG_REF_OFFS ref_offs;
  bool ref_offs_avail;

  PCPS_IRIG_TIME it;
  bool irig_time_avail;

  BVAR_STAT bvar_stat;
  bool bvar_stat_avail;

  POS rcvr_pos;
  bool rcvr_pos_avail;

  ALL_GNSS_INFO agi;
  bool agi_avail;

  UTC utc_info;
  bool utc_info_avail;

  MBG_IRIG_CTRL_BITS irig_ctrl_bits;
  bool irig_ctrl_bits_avail;

  MBG_RAW_IRIG_DATA raw_irig_data;
  bool raw_irig_data_avail;

  SYN1588_PTP_SYNC_STATUS syn1588_ptp_sync_status;
  bool syn1588_ptp_sync_status_avail;

} DEVICE_STATUS;



static /*HDR*/
void show_invt_reason( void )
{
  static const char fmt[] = "\n** Warning: %s\nThe command %s.\n";

  switch ( invt_reason )
  {
    case 2:
      printf( fmt, DEFAULT_STR_IRIG_NOT_CFGD_EN,
              "\"mbgirigcfg\" can be used to change the settings" );
      break;

    case 1:
      printf( fmt, DEFAULT_STR_IRIG_INVT_EN,
              "\"mbgctrl DATE=...\" can be used to set the on-board date" );
      break;

  }  // switch

}  // show_invt_reason



static /*HDR*/
void show_dev_cpu_info( MBG_DEV_HANDLE dh )
{
  static const char * const cpu_type_names[N_MBG_DEV_CPU_TYPES] = MBG_DEV_CPU_TYPE_NAMES;

  PCPS_CPU_INFO cpu_info = { 0 };
  char s[80];
  const char *cp = NULL;

  int rc = mbg_get_pcps_cpu_info( dh, &cpu_info );

  if ( mbg_rc_is_error( rc ) )
    goto fail;


  // An unspecified processor type is only displayed
  // in very verbose mode.
  if ( verbose < 3 )
    if ( cpu_info.cpu_type == MBG_DEV_CPU_TYPE_UNSPEC )
      goto out;

  // If the processor type is known, print the name
  // from the list.
  if ( cpu_info.cpu_type < N_MBG_DEV_CPU_TYPES )
  {
    cp = cpu_type_names[cpu_info.cpu_type];
    goto out;
  }

  // The processor type is beyond the range
  // of known processors.
  snprintf_safe( s, sizeof( s ), "unknown code %u", cpu_info.cpu_type );
  cp = s;


out:
  if ( cp )
    printf( "Processor type: %s\n", cp );

  return;


fail:
  fprintf( stderr, "Failed to get device CPU info: %s (%i)\n",
           mbg_strerror( rc ), rc );

}  // show_dev_cpu_info



static /*HDR*/
void print_pcps_time( const char *s, const PCPS_TIME *tp, const char *tail )
{
  const char *fmt = "%s";
  char ws[256];

  if ( s )
    printf( fmt, s );

  printf( fmt, pcps_date_time_str( ws, sizeof( ws ), tp, mbg_exp_year_limit,
          pcps_tz_name( tp, PCPS_TZ_NAME_FORCE_UTC_OFFS, 0 ) ) );

  if ( ( verbose > 0 ) && _pcps_time_is_read( tp ) )
    printf( ", st: 0x%02lX", (ulong) tp->status );

  if ( tail )
    printf(  fmt, tail );

}  // print_pcps_time



static /*HDR*/
void print_dms( const char *s, const DMS *p, const char *tail )
{
  const char *fmt = "%s";

  // If the position is unknown / uninitialized,
  // the prefix character can be invalid, so print
  // a space character in this case.
  char prefix = (char) p->prefix;

  if ( ( prefix < ' ' ) || ( prefix >=  '\x7F' ) )
    prefix = ' ';

  printf( "%s %c %3i deg %02i min %06.3f sec",
          s,
          prefix,
          p->deg,
          p->min,
          p->sec
        );

  if ( tail )
    printf( fmt, tail );

}  // print_dms



static /*HDR*/
void print_position( const char *s, const POS *p, const char *tail )
{
  const char *fmt = "%s";
  double r2d = 180 / PI;


  if ( s )
  {
    printf( fmt, s );

    if ( verbose )
      printf( "\n" );
  }

  if ( verbose > 1 )
  {
    printf( STR_INDENT "x: %.0fm y: %.0fm z: %.0fm",
            p->xyz[XP], p->xyz[YP], p->xyz[ZP] );

    if ( tail )
      printf( fmt, tail );
  }

  // LLA latitude and longitude are in radians, convert to degrees.
  printf( STR_INDENT "lat: %+.4f, lon: %+.4f, alt: %.0fm",
          p->lla[LAT] * r2d, p->lla[LON] * r2d, p->lla[ALT] );

  if ( tail )
    printf( fmt, tail );

  if ( verbose )
  {
    print_dms( STR_INDENT "latitude: ", &p->latitude, tail );
    print_dms( STR_INDENT "longitude:", &p->longitude, tail );
  }

}  // print_position



static /*HDR*/
void show_signal( const DEVICE_STATUS *p_ds, int signal )
{
  int ref_type = _pcps_ref_type( &p_ds->dev_info );

  if ( ref_type >= N_PCPS_REF )
    ref_type = PCPS_REF_NONE;

  printf( "Signal: %u%%  (%s", signal * 100 / PCPS_SIG_MAX, ref_name[ref_type] );

  if ( p_ds->has_irig_rx )
  {
    if ( p_ds->irig_rx_info_avail )
    {
      int idx = p_ds->irig_rx_info.settings.icode;

      if ( idx < N_ICODE_RX )
      {
        printf( " %s", icode_rx_names[idx] );

        if ( !( MSK_ICODE_RX_HAS_TZI & ( 1UL << idx ) ) )
        {
          if ( p_ds->ref_offs_avail )
          {
            int ref_offs_h = p_ds->ref_offs / MINS_PER_HOUR;

            if ( abs( ref_offs_h ) > max_ref_offs_h )
              printf( ", ** UTC offs not configured **" );
            else
              printf( ", UTC%+ih", ref_offs_h );
          }

        }
      }
    }
  }
  else
    if ( p_ds->has_pzf )
      printf( "/PZF" );

  printf( ")\n" );

}  // show_signal



static /*HDR*/
void show_time_and_status( const DEVICE_STATUS *p_ds )
{
  static const char tail[] = "\n";  // TODO

  const char status_fmt[] = "Status info: %s%s\n";
  const char status_err[] = "*** ";
  const char status_ok[] = "";
  const char *info_err = ( p_ds->is_gps || p_ds->is_lwr ) ?
                         "ANTENNA FAULTY" : "NO INPUT SIGNAL";
  const char info_ok[] = "Input signal available";
  char ws[80];
  PCPS_STATUS_STRS strs;
  PCPS_TIME_STATUS_X status_x = 0;
  int signal;
  int i;

  if ( p_ds->pcps_time_avail )
    print_pcps_time( "Date/time:  ", &p_ds->t, tail );

  if ( p_ds->hr_time_avail )
  {

    mbg_snprint_hr_time( ws, sizeof( ws ), &p_ds->ht, 0 );  // Raw timestamp?
    printf( "Local HR time:  %s", ws );

    if ( verbose > 0 )
      printf( ", st: 0x%04lX", (ulong) p_ds->ht.status );

    printf( "%s", tail );
  }

  if ( p_ds->fast_hr_timestamp_avail )
  {
    mbg_snprint_hr_tstamp( ws, sizeof( ws ), &p_ds->ts, 0, 0 );

    printf( "Fast MM time:   %s", ws );
    printf( "%s", tail );
  }

  if ( p_ds->ttm_time_avail )
  {
    int n = pcps_str_tm_gps_date_time( ws, sizeof( ws ), &p_ds->ttm.tm );
    n += snprint_utc_offs( &ws[n], sizeof( ws ) - n, " UTC", p_ds->ttm.tm.offs_from_utc );

    printf( "TTM time:   %s", ws );
    printf( ", TTM st: 0x%04X", p_ds->ttm.tm.status );
    printf( "%s", tail );

    printf( "Raw GPS time:   wn %u|%06lu.%07lu s", (unsigned) p_ds->ttm.t.wn,
            (ulong) p_ds->ttm.t.sec, (ulong) p_ds->ttm.t.tick );
    printf( "%s", tail );
  }

  signal = p_ds->t.signal - PCPS_SIG_BIAS;  // TODO or from hr time?

  if ( signal < 0 )
    signal = 0;
  else
    if ( signal > PCPS_SIG_MAX )
      signal = PCPS_SIG_MAX;

  if ( p_ds->has_signal )
    show_signal( p_ds, signal );

  if ( p_ds->corr_info_avail )
  {
    mbg_show_pzf_corr_info( &p_ds->corr_info, 0 );
    printf( "\n" );
  }

  if ( p_ds->irig_time_avail )
    printf( "Raw IRIG time: yday %u, %02u:%02u:%02u\n",
            p_ds->it.yday, p_ds->it.hour, p_ds->it.min, p_ds->it.sec );

  printf( status_fmt,
          ( signal < PCPS_SIG_ERR ) ? status_err : status_ok,
          ( signal < PCPS_SIG_ERR ) ? info_err : info_ok );


  // FIXME TODO What if no status is available?

  if ( p_ds->hr_time_avail )
    status_x = p_ds->ht.status;
  else
    if ( p_ds->pcps_time_avail )
      status_x = p_ds->t.status;

  // Evaluate the status code and setup status messages.
  pcps_status_strs( status_x, 1, p_ds->is_gps, &strs );  // TODO ref_type instead of GPS yes/no?

  // Print the status messages.
  for ( i = 0; i < N_PCPS_STATUS_STR; i++ )
  {
    PCPS_STATUS_STR *pstr = &strs.s[i];

    if ( pstr->cp )
      printf( status_fmt,
              pstr->is_err ? status_err : status_ok,
              pstr->cp );
  }


  invt_reason = 0;

  if ( p_ds->has_irig_rx && ( status_x & PCPS_INVT ) )
  {
    if ( p_ds->ref_offs_avail && _pcps_ref_offs_out_of_range( p_ds->ref_offs ) )
      invt_reason = 2;
    else
      invt_reason = 1;
  }

}  // show_time_and_status



static /*HDR*/
int collect_time_and_status( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  _pcps_time_set_unread( &p_ds->t );

  rc = mbg_get_time( dh, &p_ds->t );
  p_ds->pcps_time_avail = !mbg_cond_err_msg( rc, "mbg_get_time" );


  if ( p_ds->has_hr_time && ( ( verbose > 0 ) || !_pcps_time_is_read( &p_ds->t ) ) )
  {
    rc = mbg_get_hr_time( dh, &p_ds->ht );
    p_ds->hr_time_avail = !mbg_cond_err_msg( rc, "mbg_get_hr_time" );
  }


  if ( p_ds->has_fast_hr_timestamp && ( verbose > 1 ) )
  {
    rc = mbg_get_fast_hr_timestamp( dh, &p_ds->ts );
    p_ds->fast_hr_timestamp_avail = !mbg_cond_err_msg( rc, "mbg_get_fast_hr_timestamp" );
  }


  if ( p_ds->has_ttm_time && ( verbose > 1 ) )
  {
    rc = mbg_get_ttm_time( dh, &p_ds->ttm );
    p_ds->ttm_time_avail = !mbg_cond_err_msg( rc, "mbg_get_ttm_time" );
  }

  return MBG_SUCCESS;  // TODO

}  // collect_time_and_status



static /*HDR*/
int collect_irig_status( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  if ( p_ds->has_irig_rx )
  {
    rc = mbg_get_irig_rx_info( dh, &p_ds->irig_rx_info );
    p_ds->irig_rx_info_avail = !mbg_cond_err_msg( rc, "mbg_get_irig_rx_info" );
  }


  if ( p_ds->has_ref_offs )
  {
    rc = mbg_get_ref_offs( dh, &p_ds->ref_offs );
    p_ds->ref_offs_avail = !mbg_cond_err_msg( rc, "mbg_get_ref_offs" );
  }


  if ( p_ds->has_irig_time && verbose )
  {
    rc = mbg_get_irig_time( dh, &p_ds->it );
    p_ds->irig_time_avail =  !mbg_cond_err_msg( rc, "mbg_get_irig_time" );
  }


  return MBG_SUCCESS;  // TODO

}  // collect_irig_status



static /*HDR*/
void show_sync_time( const DEVICE_STATUS *p_ds )
{
  if ( p_ds->sync_time_avail )
    print_pcps_time( "Last sync:  ", &p_ds->sync_time, "\n" );

}  // show_sync_time



static /*HDR*/
void collect_sync_time( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  if ( p_ds->has_sync_time )
  {
    rc = mbg_get_sync_time( dh, &p_ds->sync_time );
    p_ds->sync_time_avail = !mbg_cond_err_msg( rc, "mbg_get_sync_time" );
  }

}  // collect_sync_time



/*HDR*/
void print_bvar_stat( const BVAR_STAT *p )
{
  static const char *bvar_flag_names[N_BVAR_BIT] = BVAR_FLAG_NAMES_SHORT;

  int printed;
  int i;

  printf( "0x%04X ", *p );

  if ( *p == 0 )
  {
    printf( "(all OK)\n" );
    return;
  }

  printf( "(incomplete: " );

  printed = 0;

  for ( i = 0; i < N_BVAR_BIT; i++ )
  {
    if ( (*p) & ( 1UL << i ) )
    {
      if ( printed )
        printf( ", " );
      else
        printed = 1;

      printf( "%s", bvar_flag_names[i] );
    }
  }

  printf( ")\n" );

}  // print_bvar_stat



static /*HDR*/
void show_ext_stat_info( const DEVICE_STATUS *p_ds )
{
  const RECEIVER_INFO *p_ri = &p_ds->ri;
  char ws[80];
  const char *cp;

  if ( verbose )
  {
    printf( "Feature mask: 0x%08lX\n", (ulong) p_ri->features );

    if ( verbose > 2 )
      printf( "EPLD name: \"%s\"\n", p_ri->epld_name );
  }

  if ( p_ds->has_stat_info )
  {
    if ( p_ds->has_stat_info_mode )
    {
      switch ( p_ds->agi.stat_info.mode )
      {
        case AUTO_166: cp = "Normal Operation";  break;
        case WARM_166: cp = "Warm Boot";         break;
        case COLD_166: cp = "Cold Boot";         break;

        default:  // This should never happen!
          snprintf_safe( ws, sizeof( ws ), "%s mode of operation: %02Xh",
                         str_unknown, p_ds->agi.stat_info.mode );
          cp = ws;

      }  // switch

      printf( "%s", cp );
    }
  }

  if ( p_ds->agi.n_gnss_supp )
  {
    int i;

    #if 0 && defined( DEBUG )  // FIXME TODO
      int must_print_sv_list = p_ds->has_stat_info_svs && verbose;
    #else
      int must_print_sv_list = p_ds->is_gnss && verbose;
    #endif
    int print_multi_lines = ( p_ds->agi.n_gnss_supp > 1 ) || must_print_sv_list;

    // Print multiple lines,
    // or append to line.
    printf( print_multi_lines ? ":\n" : ", " );

    for ( i = 0; i < p_ds->agi.n_gnss_supp; i++ )
    {
      static const char * const gnss_type_names[N_GNSS_TYPES] = GNSS_TYPE_STRS;

      const GNSS_SAT_INFO *p_gsi;
      int gnss_type;

      p_gsi = &p_ds->agi.gnss_sat_info_idx[i].gnss_sat_info;
      gnss_type = p_gsi->gnss_type;

      if ( gnss_type >= N_GNSS_TYPES )
      {
        mbg_snprintf( ws, sizeof( ws ), "(%s GNSS type %i): ", str_unknown, gnss_type );
        cp = ws;
      }
      else
        cp = gnss_type_names[gnss_type];

      if ( print_multi_lines )
        printf( "  " );

      if ( p_ds->agi.gnss_mode_info.settings.gnss_set & ( 1UL << gnss_type ) )
      {
        printf( "%i %s sats tracked, %i expected to be visible\n", p_gsi->good_svs, cp, p_gsi->svs_in_view );

        if ( must_print_sv_list )
        {
          int sat_idx;

          for ( sat_idx = 0; sat_idx < MAX_USED_SATS; sat_idx++ )
          {
            int sat_num = p_gsi->svs[sat_idx];

            #if !defined( DEBUG )
              if ( sat_num == 0 )
                break;
            #endif

            printf( "%s%i", ( sat_idx == 0 ) ? "  Sats: " : ", ", sat_num );
          }

          if ( sat_idx )  // The satellite list has been printed.
            printf( "\n" );
        }
      }
      else
        printf( "%s reception disabled by configuration\n", cp );
    }
  }


  if ( p_ds->bvar_stat_avail && ( p_ds->bvar_stat || verbose ) )
  {
    printf( "BVAR stat: " );
    print_bvar_stat( &p_ds->bvar_stat );
  }


  if ( verbose )
  {
    printf( "Osc type: %s", osc_name[( p_ri->osc_type < N_GPS_OSC ) ? p_ri->osc_type : GPS_OSC_UNKNOWN] );

    if ( p_ds->has_stat_info )
    {
      printf( ", DAC cal: %+i, fine: %+i",
              (int) ( p_ds->agi.stat_info.dac_cal - OSC_DAC_BIAS ),
              (int) ( p_ds->agi.stat_info.dac_val - OSC_DAC_BIAS ) );
    }

    puts( "" );
  }

}  // show_ext_stat_info



static /*HDR*/
int collect_ext_stat_info( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  if ( p_ds->is_gps )
  {
    rc = mbg_chk_get_all_gnss_info( dh, &p_ds->agi );
    p_ds->agi_avail = !mbg_cond_err_msg( rc, "mbg_chk_get_all_gnss_info" );
  }

  // We always read the bvar_stat, if supported, and we
  // decide later if we print the values, or not.
  if ( p_ds->has_bvar_stat )
  {
    rc = mbg_get_gps_bvar_stat( dh, &p_ds->bvar_stat );
    p_ds->bvar_stat_avail = !mbg_cond_err_msg( rc, "mbg_get_gps_bvar_stat" );
  }

  return MBG_SUCCESS;

}  // collect_ext_stat_info



static /*HDR*/
void show_syn1588_ptp_sync_status( const DEVICE_STATUS *p_ds )
{
  bool sync_status_valid;
  uint32_t ptp_state;
  const char *ptp_state_str;
  bool ptp_in_sync;
  uint32_t insync_boundary_code;
  char insync_boundary_str[80];
  bool lsync_running;

  printf( "PTP state: " );

  // If the status could not be read, there are no details to display.
  if ( !p_ds->syn1588_ptp_sync_status_avail )
  {
    printf( "%s\n", str_not_avail );
    return;
  }


  // The status word could be read, so we can decode some fields
  // in any case.
  ptp_state = syn1588_ptp_state_from_sync_status( p_ds->syn1588_ptp_sync_status );
  ptp_state_str = mbg_syn1588_chk_get_ptp_state_str( ptp_state );

  ptp_in_sync = syn1588_ptp_in_sync( p_ds->syn1588_ptp_sync_status );
  insync_boundary_code = syn1588_get_insync_boundary_code( p_ds->syn1588_ptp_sync_status );
  mbg_syn1588_snprint_insync_boundary_code( insync_boundary_str, sizeof( insync_boundary_str ),
                                            insync_boundary_code, true );
  lsync_running = syn1588_lsync_running( p_ds->syn1588_ptp_sync_status );


  // Show if the status is valid, or not.
  sync_status_valid = syn1588_ptp_sync_status_valid( p_ds->syn1588_ptp_sync_status );

  if ( !sync_status_valid )
  {
    printf( "NOT VALID!\n" );

    if ( verbose < 3 )
      goto out;

    goto print_details;
  }


  // The PTP sync status is valid, so we can now display
  // the state of the PTP stack.
  printf( "%s", ptp_state_str );

  if ( ( ( verbose > 1 ) && ( ptp_state == SYN1588_PTP_STATE_SLAVE ) )
    || ( verbose > 2 ) )
    if ( ptp_in_sync )
      printf( ", sync to boundary %s", insync_boundary_str );

  printf( "\n" );

  if ( verbose < 2 )
    goto out;


print_details:
  printf( "PTP sync status details:\n" );
  {
    bool must_show_values = verbose > 2;
    int utc_tai_offs = syn1588_utc_offs_from_sync_status( p_ds->syn1588_ptp_sync_status );
    bool utc_tai_offs_valid = syn1588_sync_status_utc_valid( p_ds->syn1588_ptp_sync_status );
    bool leap_59 = ( p_ds->syn1588_ptp_sync_status & DEF_PSS_BIT_LEAP59 ) != 0;
    bool leap_61 = ( p_ds->syn1588_ptp_sync_status & DEF_PSS_BIT_LEAP61 ) != 0;
    bool timescale_is_ptp = syn1588_is_timescale_ptp( p_ds->syn1588_ptp_sync_status );

    printf( "  Time scale: %s\n", timescale_is_ptp ? "PTP/TAI" : "arbitrary" );
    printf( "  UTC/TAI offs valid: %i, offs: %i s,\n", utc_tai_offs_valid, utc_tai_offs );
    printf( "  Leap 59: %i, Leap 61: %i\n", leap_59, leap_61 );
    printf( "  PTP State: %s", ptp_state_str );

    if ( must_show_values )
      printf( " (0x%02X)", ptp_state );

    printf( ", in sync: %i, boundary: %s", ptp_in_sync, sync_status_valid ?
            insync_boundary_str : "undefined" );

    if ( must_show_values )
      printf( " (0x%02X)", insync_boundary_code );

    printf( "\n" );

    printf( "  lSync running: %s\n", lsync_running ? "Yes" : "No" );
  }

out:
  // Display a warning if the firmware of a SYN1588 NIC is too old.
  // If the firmware is too old, the PTP sync status may not
  // be supported, either.
  if ( _pcps_fw_rev_num( &p_ds->dev_info ) < SYN1588_MIN_REQ_BUILD_NUM )
  {
    printf( "\n"
            "** Warning: FW build %i is required, please update!\n"
            "Without an update, this SYN1588 card may not work properly!",
            SYN1588_MIN_REQ_BUILD_NUM );
  }
  else
  {
    // If the status could be read, but is not valid, show a hint
    // what could be the reason.
    if ( p_ds->syn1588_ptp_sync_status_avail && !sync_status_valid )
      printf( "\n"
              "** Warning: The PTP sync status is not available!\n"
              "Make sure the PTP stack is running, adjusting the time\n"
              "and updating the status on the card.\n" );

    #if defined( MBG_TGT_WIN32 )
      // This warning is only significant for Windows, not for Linux.
      if ( lsync_running )
        printf( "\n"
                "** Warning: The lSync program is also running!\n"
                "lSync is not thread-safe and concurrent access with these tools\n"
                "may cause inconsistent data to bread from the card.\n" );
    #endif  // defined( MBG_TGT_WIN32 )
  }

}  // show_syn1588_ptp_sync_status



static /*HDR*/
void collect_syn1588_info( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc = mbg_get_syn1588_ptp_sync_status( dh, &p_ds->syn1588_ptp_sync_status );
  p_ds->syn1588_ptp_sync_status_avail = !mbg_cond_err_msg( rc, "mbg_get_syn1588_ptp_sync_status" );

}  // collect_syn1588_info



static /*HDR*/
void show_rcvr_pos( const DEVICE_STATUS *p_ds )
{
  if ( p_ds->rcvr_pos_avail )
    print_position( "Receiver Position:", &p_ds->rcvr_pos, "\n" );

}  // show_rcvr_pos



static /*HDR*/
int collect_rcvr_pos( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  if ( p_ds->has_rcvr_pos )
  {
    rc = mbg_get_gps_pos( dh, &p_ds->rcvr_pos );
    p_ds->rcvr_pos_avail = !mbg_cond_err_msg( rc, "mbg_get_gps_pos" );
  }

  return MBG_SUCCESS;

}  // collect_rcvr_pos



static /*HDR*/
void show_utc_info( const DEVICE_STATUS *p_ds )
{
  MBG_LS_INFO lsi;
  GPS_WNUM true_wn_ls;
  struct tm tm = { 0 };
  char tmp_str[80];
  const char *cp;
  int rc;

  if ( !p_ds->utc_info_avail )
    goto out;

  // Try to evaluate the UTC parameter set and determine
  // the date of the nearest leap second.
  rc = mbg_set_ls_info_from_gps_utc( &lsi, &p_ds->utc_info, &true_wn_ls );

  if ( verbose > 1 )
  {
    printf( "UTC correction parameters:\n" );

    printf( STR_INDENT "CSUM: %04X, valid: %04X\n", p_ds->utc_info.csum, p_ds->utc_info.valid );
    printf( STR_INDENT "t0t: %u|%u.%07u, A0: %g A1: %g\n",
            p_ds->utc_info.t0t.wn, p_ds->utc_info.t0t.sec, p_ds->utc_info.t0t.tick,
            p_ds->utc_info.A0, p_ds->utc_info.A1 );
    printf( STR_INDENT "WNlsf: %u", p_ds->utc_info.WNlsf );

    // Under certain conditions, the week number read from GPS receivers
    // can be ambiguous, and the evaluation function called above may have
    // determined and returned the true week number, so we show it here.
    if ( mbg_rc_is_success( rc ) && p_ds->utc_info.WNlsf && lsi.valid )
      printf( " (true: %u)", true_wn_ls );

    printf( ", DN: %u, offs: %i/%i\n",
            p_ds->utc_info.DNt, p_ds->utc_info.delta_tls, p_ds->utc_info.delta_tlsf );
  }

  if ( ( p_ds->utc_info.WNlsf == 0 ) && ( p_ds->utc_info.DNt == 0 ) )
  {
    if ( verbose )
      printf( "Leap second information not set\n" );

    goto out;
  }


  // Check once more if the evaluation above was successful.
  if ( mbg_rc_is_error( rc ) )
  {
    if ( rc == MBG_ERR_INV_PARM )
      puts( "** UTC parameters not valid" );
    else
      fprintf( stderr, "** Failed to evaluate GNSS UTC parameters: %s\n",
               mbg_strerror( rc ) );
    goto out;
  }

  // Evaluation was successful, now check if valid data
  // could be determined.
  if ( !lsi.valid )
  {
    if ( verbose )
      puts( "UTC parameters could not be evaluated" );

    goto out;
  }

  // A valid UTC timestamp of the second *after* the nearest leap second
  // could be determined. We display the time associated with that
  // timestamp because we may not be able to figure out whether the
  // leap second is or was a positive one (second 60 inserted), or a
  // negative one (second 59 deleted).
  rc = mbg_gmtime64( &tm, &lsi.t64_ls_utc );

  if ( mbg_rc_is_success( rc ) )
  {
    snprintf_safe( tmp_str, sizeof( tmp_str ), "UTC midnight on %s, %04i-%02i-%02i",
                   wdays[tm.tm_wday], tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday );
    cp = tmp_str;
  }
  else  // Timestamp conversion failed, which is unlikely, but ...
    cp = "unknown point in time";

  // Now print information, depending on whether a leap second is being
  // announced, or not, and whether we want verbose output, or not.
  if ( lsi.ls_step )  // A leap second is currently being announced.
  {
    printf( "%s of a leap second is announced!\n",
            ( lsi.ls_step < 0 ) ? "Deletion" : "Insertion" );

    printf( "%sThe GPS/UTC offset steps from %i s to %i s\n",
            verbose ? str_indent : str_empty,
            p_ds->utc_info.delta_tls, p_ds->utc_info.delta_tlsf );

    printf( "%sat %s.\n", verbose ? str_indent : str_empty, cp );
  }
  else  // No leapsecond currently announced.
  {
    printf( "Current GPS/UTC offset: %i s, no leap second announced.\n",
            lsi.offs_gps_utc );

    if ( verbose )
      printf( "Nearest leap second just before %s.\n", cp );
  }


out:
  return;

}  // show_utc_info



static /*HDR*/
int collect_utc_info( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  if ( p_ds->has_utc_parm && ( p_ds->is_gps || ( verbose > 0 ) ) )
  {
    rc = mbg_get_utc_parm( dh, &p_ds->utc_info );
    p_ds->utc_info_avail = !mbg_cond_err_msg( rc, "mbg_get_utc_parm" );
  }

  return MBG_SUCCESS;

}  // collect_utc_info



static /*HDR*/
void show_irig_ctrl_bits( const DEVICE_STATUS *p_ds )
{
  if ( p_ds->irig_ctrl_bits_avail )
  {
    printf( "IRIG control bits: %08lX (hex, LSB first)", (ulong) p_ds->irig_ctrl_bits );
    printf( ", TFOM: 0x%X", _pcps_tfom_from_irig_ctrl_bits( &p_ds->irig_ctrl_bits ) );
    printf( "\n" );
  }

}  // show_irig_ctrl_bits



static /*HDR*/
int collect_irig_ctrl_bits( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  if ( p_ds->has_irig_ctrl_bits && verbose )
  {
    rc = mbg_get_irig_ctrl_bits( dh, &p_ds->irig_ctrl_bits );
    p_ds->irig_ctrl_bits_avail = !mbg_cond_err_msg( rc, "mbg_get_irig_ctrl_bits" );
  }

  return MBG_SUCCESS;

}  // collect_irig_ctrl_bits



static /*HDR*/
char *str_raw_irig_utc_offs_hours( char *s, size_t max_len, const MBG_RAW_IRIG_DATA *p )
{
  size_t n;
  long offs = ( p->data_bytes[8] & 0x08 )
            | ( ( p->data_bytes[8] >> 2 ) & 0x04 )
            | ( ( p->data_bytes[8] >> 4 ) & 0x02 )
            | ( ( p->data_bytes[8] >> 6 ) & 0x01 );

  n = mbg_snprintf( s, max_len, "%c%li", ( p->data_bytes[8] & 0x80 ) ? '-' : '+', offs );

  if ( p->data_bytes[8] & 0x02 )
    n += mbg_snprintf( &s[n], max_len - n, "%s", ".5" );

  return s;

}  // str_raw_irig_utc_offs_hours



static /*HDR*/
void show_raw_irig_data( const DEVICE_STATUS *p_ds )
{
  if ( p_ds->raw_irig_data_avail )
  {
    char ws[80];
    size_t i;

    printf( "Raw IRIG data:" );

    for ( i = 0; i < sizeof( p_ds->raw_irig_data ); i++ )
      printf( " %02X", p_ds->raw_irig_data.data_bytes[i] );

    printf( " (hex)" );
    printf( ", TFOM: 0x%X", _pcps_tfom_from_raw_irig_data( &p_ds->raw_irig_data ) );
    printf( ", UTC offs: %sh", str_raw_irig_utc_offs_hours( ws, sizeof( ws ), &p_ds->raw_irig_data ) );
    printf( "\n" );
  }

}  // show_raw_irig_data



static /*HDR*/
int collect_raw_irig_data( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  int rc;

  if ( p_ds->has_raw_irig_data && verbose )
  {
    rc = mbg_get_raw_irig_data( dh, &p_ds->raw_irig_data );
    p_ds->raw_irig_data_avail = !mbg_cond_err_msg( rc, "mbg_get_raw_irig_data" );
  }

  return MBG_SUCCESS;

}  // show_raw_irig_data



static /*HDR*/
void show_irig_debug_status( MBG_DEV_HANDLE dh )
{
  static const char *status_str[N_MBG_DEBUG_BIT] = MBG_DEBUG_STATUS_STRS;

  MBG_DEBUG_STATUS st;
  int i;
  int rc = mbg_get_debug_status( dh, &st );

  if ( mbg_cond_err_msg( rc, "show_irig_debug_status" ) )
    return;

  printf( "Debug status (hex): %08lX\n", (ulong) st );

  for ( i = 0; i < N_MBG_DEBUG_BIT; i++ )
    if ( st & ( 1UL << i ) )
      printf( "  %s\n", status_str[i] );

}  // show_irig_debug_status



static /*HDR*/
void show_lan_intf_state( MBG_DEV_HANDLE dh )
{
  IP4_SETTINGS ip4_settings;
  LAN_IF_INFO lan_if_info;
  char ws[100];

  int rc = mbg_get_ip4_state( dh, &ip4_settings );

  if ( mbg_cond_err_msg( rc, "mbg_get_ip4_state" ) )
    return;

  rc = mbg_get_lan_if_info( dh, &lan_if_info );

  if ( mbg_cond_err_msg( rc, "mbg_get_lan_if_info" ) )
    return;


  printf( "On-board LAN interface settings:\n" );

  snprint_mac_addr( ws, sizeof( ws ), &lan_if_info.mac_addr );
  printf( "  MAC Address:    %s\n", ws );

  snprint_ip4_addr( ws, sizeof( ws ), &ip4_settings.ip_addr, NULL );
  printf( "  IP Address:     %s%s\n", ws, ( ip4_settings.flags & IP4_MSK_DHCP ) ?
          " (DHCP)" : "" );

  snprint_ip4_addr( ws, sizeof( ws ), &ip4_settings.netmask, NULL );
  printf( "  Net Mask:       %s\n", ws );

  snprint_ip4_addr( ws, sizeof( ws ), &ip4_settings.broad_addr, NULL );
  printf( "  Broadcast Addr: %s\n", ws );

  snprint_ip4_addr( ws, sizeof( ws ), &ip4_settings.gateway, NULL );
  printf( "  Gateway:        %s\n", ws );

  printf( "  Link detected:  %s\n", ( ip4_settings.flags & IP4_MSK_LINK ) ? "YES" : "NO" );

}  // show_lan_intf_state



static /*HDR*/
void show_ptp_state( MBG_DEV_HANDLE dh )
{
  static const char *ptp_role_strs[N_PTP_ROLES] = PTP_ROLE_STRS;
  static const char *ptp_state_strs[N_PTP_PORT_STATE] = PTP_PORT_STATE_STRS;
  static const char *ptp_nw_prot_strs[N_PTP_NW_PROT] = PTP_NW_PROT_STRS;
  static const char *ptp_delay_mech_names[N_PTP_DELAY_MECH] = PTP_DELAY_MECH_NAMES;
  static const PTP_TABLE ptp_time_source_tbl[] = PTP_TIME_SOURCE_TABLE;
  static const char *ptp_clock_accuracy_strs[] = PTP_CLOCK_ACCURACY_STRS;

  char ws[100];
  const char *cp;
  int must_show_slave_mode_info;
  int must_show_master_mode_info;
  int slave_mode_active;
  int master_mode_active;
  int any_mode_active;
  int utc_offset_valid;
  PTP_STATE ptp_state;
  PTP_CFG_INFO ptp_info;
  int tmp;

  int rc = mbg_get_ptp_state( dh, &ptp_state );

  if ( mbg_cond_err_msg( rc, "mbg_get_ptp_state" ) )
    return;

  rc = mbg_get_ptp_cfg_info( dh, &ptp_info );

  if ( mbg_cond_err_msg( rc, "mbg_get_ptp_info" ) )
    return;


  // Set up some flags controlling which information to be shown.

  must_show_slave_mode_info = ( verbose > 1 ) ||
        ( ( ( 1UL << ptp_info.settings.ptp_role ) & PTP_ROLE_MSK_SLAVES ) != 0 );

  must_show_master_mode_info = ( verbose > 1 ) ||
        ( ( ( 1UL << ptp_info.settings.ptp_role ) & PTP_ROLE_MSK_MASTERS ) != 0 );

  slave_mode_active = ( ptp_state.port_state == PTP_PORT_STATE_UNCALIBRATED )
                   || ( ptp_state.port_state == PTP_PORT_STATE_SLAVE );

  master_mode_active = ( ptp_state.port_state == PTP_PORT_STATE_PRE_MASTER )
                    || ( ptp_state.port_state == PTP_PORT_STATE_MASTER )
                    || ( ptp_state.port_state == PTP_PORT_STATE_PASSIVE );

  any_mode_active = slave_mode_active || master_mode_active;


  // PTP role and port state.

  printf( "PTP port state:\n" );

  printf( "  Port mode:       " );

  if ( any_mode_active )
    printf( "%s", ( ptp_state.flags & PTP_FLAG_MSK_IS_UNICAST ) ?
            "Unicast " : "Multicast " );

  if ( ptp_state.port_state < N_PTP_PORT_STATE )
    printf( "%s", ptp_state_strs[ptp_state.port_state] );
  else
    printf( "%s, code %i", str_undefined, ptp_state.port_state );

  if ( !any_mode_active  || ( verbose > 1 ) )
    printf( " in %s role",
            ( ptp_info.settings.ptp_role < N_PTP_ROLES ) ?
            ptp_role_strs[ptp_info.settings.ptp_role] : str_undefined );

  printf( "\n" );


  if ( !any_mode_active || verbose )
  {
    printf( "  PTP protocol:    " );

    // If not fully synchronized, not all fields of the PTP_STATE
    // structure may have been filled, so we show configuration settings
    // in this case.

    if ( ptp_state.ptp_prot_version )
      printf( "v%i, ", ptp_state.ptp_prot_version );

    tmp = any_mode_active ? ptp_state.nw_prot : ptp_info.settings.nw_prot;
    printf( "%s", ( tmp < N_PTP_NW_PROT ) ?
            ptp_nw_prot_strs[tmp] : str_undefined );

    printf( ", %s step",
            ( ptp_state.flags & PTP_FLAG_MSK_ONE_STEP ) ? "one" : "two" );

    tmp = any_mode_active ? ptp_state.domain_number : ptp_info.settings.domain_number;
    printf( ", domain %i", tmp );

    tmp = any_mode_active ? ptp_state.delay_mech : ptp_info.settings.delay_mech;
    printf( ", delay mech. %s", ( tmp < N_PTP_DELAY_MECH ) ?
              ptp_delay_mech_names[tmp] : str_undefined );

    tmp = any_mode_active ? ptp_state.log_delay_req_intv : ptp_info.settings.delay_req_intv;
    printf( ", dly req. intv. 2^%i s", tmp );

    printf( "\n" );
  }


  if ( must_show_slave_mode_info )
  {
    cp = ( slave_mode_active || ( verbose > 1 ) ) ? ws : str_not_avail;

    snprint_octets( ws, sizeof( ws ), ptp_state.gm_id.b,
                    sizeof( ptp_state.gm_id.b ), MAC_SEP_CHAR_ALT, NULL );
    printf( "  Grandmaster ID:  %s\n", cp );

    snprint_nano_time( ws, sizeof( ws ), &ptp_state.offset );
    printf( "  PTP time offset: %s\n", cp );

    snprint_nano_time( ws, sizeof( ws ), &ptp_state.path_delay );
    printf( "  PTP path delay:  %s\n", cp );

    if ( verbose > 1 )
    {
      snprint_nano_time( ws, sizeof( ws ), &ptp_state.mean_path_delay );
      printf( "  Mean path delay: %s\n", cp );

      snprint_nano_time( ws, sizeof( ws ), &ptp_state.delay_asymmetry );
      printf( "  Delay asymmetry: %s\n", cp );
    }
  }


  // PTP time scale.

  cp = NULL;

  if ( ptp_state.flags & PTP_FLAG_MSK_TIMESCALE_IS_PTP )  // This is the default.
  {
    if ( must_show_master_mode_info || verbose )
      cp = "TAI (standard)";
  }
  else  // Unusual case, print info if available.
    if ( any_mode_active )
      cp = "arbitrary (non-standard)";

  if ( cp )
    printf( "  PTP time scale:  %s\n", cp );


  // UTC offset and leap second status.

  utc_offset_valid = ( ptp_state.flags & PTP_FLAG_MSK_UTC_VALID ) != 0;

  printf( "  PTP UTC offset: " );

  if ( !utc_offset_valid )
    printf( " %s", str_unknown );  // UTC offset not valid.

  if ( utc_offset_valid || ( verbose > 1 ) )
  {
    printf( utc_offset_valid ? " " : " (" );

    printf( "%+i s", ptp_state.utc_offset );

    if ( ptp_state.flags & PTP_FLAG_MSK_LS_ANN )  // Leap second is announced.
    {
      // Distinguish between leap second insertion and deletion.
      printf( ", leap second %s scheduled",
              ( ptp_state.flags & PTP_FLAG_MSK_LS_ANN_NEG ) ? "deletion" : "insertion" );
    }

    if ( !utc_offset_valid )
      printf( ")" );
  }

  printf( "\n" );


  if ( verbose > 1 )
  {
    const PTP_TABLE *p_tbl;

    printf( "PTP clock state:\n" );

    for ( p_tbl = ptp_time_source_tbl; p_tbl->name; p_tbl++ )
      if ( p_tbl->value == ptp_state.time_source )
        break;

    printf( "  Time source: %s\n", p_tbl->name ? p_tbl->name : str_unknown );

    printf( "  Clock class: %i\n", ptp_state.clock_class );

    tmp = N_PTP_CLOCK_ACCURACY - PTP_CLOCK_ACCURACY_NUM_BIAS;

    if ( ( ptp_state.clock_accuracy >= PTP_CLOCK_ACCURACY_NUM_BIAS ) &&
         ( ptp_state.clock_accuracy < N_PTP_CLOCK_ACCURACY ) )
      cp = ptp_clock_accuracy_strs[ptp_state.clock_accuracy - PTP_CLOCK_ACCURACY_NUM_BIAS];
    else
      cp = str_undefined;

    printf( "  Clock accuracy: %s\n", cp );

#if 0
    time_source
    clock_class
    clock_accuracy
#endif
    printf( "  Offs sc. log var: %u", ptp_state.clock_offset_scaled_log_variance );
    printf( "\n" );
  }

}  // show_ptp_state



static /*HDR*/
/*HDR*/
/**
 * @brief Check whether it is safe to use IRQs with this device.
 *
 * The API call ::mbg_chk_dev_has_asic_version checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p_dev  Pointer to a device info structure read from the device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @see ::mbg_chk_dev_has_asic_version
 */
int check_irq_safe( MBG_DEV_HANDLE dh, const PCPS_DEV *p_dev )
{
  PCPS_IRQ_STAT_INFO irq_stat_info;
  int rc = mbg_get_irq_stat_info( dh, &irq_stat_info );

  if ( rc == MBG_ERR_NOT_SUPP_BY_DEV || rc == MBG_ERR_UNSPEC )
  {
    // Not supported by device, so no need to check.
    rc = MBG_SUCCESS;
    goto out;
  }

  if ( mbg_cond_err_msg( rc, "mbg_get_irq_stat_info" ) )
    goto out;

  if ( irq_stat_info & PCPS_IRQ_STAT_UNSAFE )
  {
    static const char *warn_line = "************************************************************************************";

    puts( "" );
    puts( warn_line );

    printf(
      "**  WARNING!\n"
      "**\n"
      "**  Device %s with S/N %s has a firmware version and ASIC version\n"
      "**  which do not allow safe operation with hardware interrupts (IRQs) enabled.\n"
      "**\n"
      "**  Please see http://www.meinberg.de/english/info/pex-upgrades.htm\n"
      "**  for information how the card can easily be upgraded, or contact\n"
      "**  Meinberg support (Email: support@meinberg.de) or your local\n"
      "**  representative.\n"
      ,
      _pcps_type_name( p_dev ), _pcps_sernum( p_dev )
    );

    if ( irq_stat_info & PCPS_IRQ_STAT_ENABLED )
    {
      printf(
        "**\n"
        "**  Interrupts are currently enabled for this device (NTP daemon running?)\n"
        "**  so other access is inhibited to prevent the system from hanging.\n"
      );

      rc = MBG_ERR_IRQ_UNSAFE;
    }

    puts( warn_line );
    puts( "" );
  }

out:
  return rc;

}  // check_irq_safe



static /*HDR*/
int check_all_device_features( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds,
                               const PCPS_DEV *p_dev, bool is_syn1588 )
{
  int rc = MBG_SUCCESS;

  memset( p_ds, 0, sizeof( *p_ds ) );

  p_ds->dev_info = *p_dev;

  p_ds->is_syn1588 = is_syn1588;

  rc = mbg_setup_receiver_info( dh, p_dev, &p_ds->ri );

  if ( mbg_cond_err_msg( rc, "mbg_setup_receiver_info" ) )
    goto out;

  // p_ds->has_receiver_info = mbg_rc_is_success( ( dh ) );
  // p_ds->has_gps_data = mbg_rc_is_success( ( dh ) );
  // p_ds->has_generic_io = mbg_rc_is_success( ( dh ) );
  // p_ds->has_asic_version = mbg_rc_is_success( ( dh ) );
  // p_ds->has_asic_features = mbg_rc_is_success( ( dh ) );
  p_ds->has_xmr = mbg_rc_is_success( mbg_chk_dev_has_xmr( dh ) );
  // p_ds->is_isa = mbg_rc_is_success( ( dh ) );
  // p_ds->is_mca = mbg_rc_is_success( ( dh ) );
  // p_ds->is_pci = mbg_rc_is_success( ( dh ) );
  // p_ds->is_pci_express = mbg_rc_is_success( ( dh ) );
  p_ds->is_usb = mbg_rc_is_success( mbg_chk_dev_is_usb( dh ) );

  p_ds->is_gps = mbg_rc_is_success( mbg_chk_dev_is_gps( dh ) );
  if ( p_ds->is_gps )
    p_ds->is_gnss = mbg_rc_is_success( mbg_chk_dev_is_gnss( dh ) );

  p_ds->is_lwr = mbg_rc_is_success( mbg_chk_dev_is_lwr( dh ) );
  if ( p_ds->is_lwr )
  {
    p_ds->is_dcf = mbg_rc_is_success( mbg_chk_dev_is_dcf( dh ) );
    p_ds->has_pzf = mbg_rc_is_success( mbg_chk_dev_has_pzf( dh ) );
    p_ds->is_msf = mbg_rc_is_success( mbg_chk_dev_is_msf( dh ) );
    p_ds->is_wwvb = mbg_rc_is_success( mbg_chk_dev_is_wwvb( dh ) );
    p_ds->is_jjy = mbg_rc_is_success( mbg_chk_dev_is_jjy( dh ) );
  }

  p_ds->is_tcr = mbg_rc_is_success( mbg_chk_dev_is_tcr( dh ) );
  // p_ds->has_lan_intf = mbg_rc_is_success( ( dh ) );
  // p_ds->has_ptp = mbg_rc_is_success( ( dh ) );
  // p_ds->has_ptp_unicast = mbg_rc_is_success( ( dh ) );
  p_ds->has_hr_time = mbg_rc_is_success( mbg_chk_dev_has_hr_time( dh ) );
  p_ds->has_fast_hr_timestamp = mbg_rc_is_success( mbg_chk_dev_has_fast_hr_timestamp( dh ) );
  // p_ds->has_time_scale = mbg_rc_is_success( ( dh ) );
  p_ds->has_event_time = mbg_rc_is_success( mbg_chk_dev_has_event_time( dh ) );
  // p_ds->has_ucap = mbg_rc_is_success( ( dh ) );
  // p_ds->can_clr_ucap_buff = mbg_rc_is_success( ( dh ) );
  // p_ds->has_tzdl = mbg_rc_is_success( ( dh ) );
  // p_ds->has_pcps_tzdl = mbg_rc_is_success( ( dh ) );
  // p_ds->has_tzcode = mbg_rc_is_success( ( dh ) );
  // p_ds->has_tz = mbg_rc_is_success( ( dh ) );
  // p_ds->has_irig = mbg_rc_is_success( ( dh ) );
  p_ds->has_irig_rx = mbg_rc_is_success( mbg_chk_dev_is_tcr( dh ) );
  p_ds->has_irig_tx = mbg_rc_is_success( mbg_chk_dev_has_irig_tx( dh ) );
  p_ds->has_irig_ctrl_bits = mbg_rc_is_success( mbg_chk_dev_has_irig_ctrl_bits( dh ) );
  p_ds->has_raw_irig_data = mbg_rc_is_success( mbg_chk_dev_has_raw_irig_data( dh ) );
  p_ds->has_irig_time = mbg_rc_is_success( mbg_chk_dev_has_irig_time( dh ) );
  p_ds->has_signal = mbg_rc_is_success( mbg_chk_dev_has_signal( dh ) );
  // p_ds->has_mod = mbg_rc_is_success( ( dh ) );
  p_ds->has_sync_time = mbg_rc_is_success( mbg_chk_dev_has_sync_time( dh ) );
  p_ds->has_rcvr_pos = mbg_rc_is_success( mbg_chk_dev_has_rcvr_pos( dh ) );
  p_ds->has_bvar_stat = mbg_rc_is_success( mbg_chk_dev_has_bvar_stat( dh ) );
  p_ds->has_stat_info = p_ds->is_gps || p_ds->has_pzf;  // TODO
  p_ds->has_stat_info_mode = p_ds->is_gps;  // TODO
  p_ds->has_stat_info_svs = p_ds->is_gps;   // TODO
  p_ds->has_ttm_time = mbg_rc_is_success( mbg_chk_dev_has_ttm_time( dh ) );
  // p_ds->has_serial = mbg_rc_is_success( ( dh ) );
  // p_ds->has_serial_hs = mbg_rc_is_success( ( dh ) );
  // p_ds->has_synth = mbg_rc_is_success( ( dh ) );
  // p_ds->has_gpio = mbg_rc_is_success( ( dh ) );
  // p_ds->has_cab_len = mbg_rc_is_success( ( dh ) );
  p_ds->has_ref_offs = mbg_rc_is_success( mbg_chk_dev_has_ref_offs( dh ) );
  // p_ds->has_opt_flags = mbg_rc_is_success( ( dh ) );
  p_ds->has_utc_parm = mbg_rc_is_success( mbg_chk_dev_has_utc_parm( dh ) );
  // p_ds->has_corr_info = mbg_rc_is_success( ( dh ) );
  // p_ds->has_tr_distance = mbg_rc_is_success( ( dh ) );
  // p_ds->has_debug_status = mbg_rc_is_success( ( dh ) );
  // p_ds->has_evt_log = mbg_rc_is_success( ( dh ) );
  // p_ds->has_dac_ctrl = mbg_rc_is_success( ( dh ) );

out:
  return rc;

}  // check_all_device_features



static /*HDR*/
int collect_all_device_status( MBG_DEV_HANDLE dh, DEVICE_STATUS *p_ds )
{
  if ( p_ds->is_syn1588 )
    collect_syn1588_info( dh, p_ds );
  else
    collect_ext_stat_info( dh, p_ds );

  collect_time_and_status( dh, p_ds );
  collect_irig_status( dh, p_ds );
  collect_sync_time( dh, p_ds );
  collect_rcvr_pos( dh, p_ds );
  collect_utc_info( dh, p_ds );
  collect_irig_ctrl_bits( dh, p_ds );
  collect_raw_irig_data( dh, p_ds );

  return MBG_SUCCESS;

}  // collect_all_device_status



static /*HDR*/
void print_all_device_status( const DEVICE_STATUS *p_ds )
{
  if ( !p_ds->is_syn1588 )
    show_ext_stat_info( p_ds );

  show_time_and_status( p_ds );
  show_sync_time( p_ds );
  show_rcvr_pos( p_ds );
  show_utc_info( p_ds );
  show_irig_ctrl_bits( p_ds );
  show_raw_irig_data( p_ds );

  if ( p_ds->is_syn1588 )
    show_syn1588_ptp_sync_status( p_ds );

}  // print_all_device_status



static /*HDR*/
int do_mbgstatus( MBG_DEV_HANDLE dh )
{
  PCPS_DEV dev;
  DEVICE_STATUS device_status;
  DEVICE_STATUS *p_ds = &device_status;
  int ret_val = 0;
  int rc;

  // First we collect some information without accessing
  // the device hardware.

  // SYN1588 devices are somewhat different ...
  bool is_syn1588 = mbg_rc_is_success( mbg_chk_dev_is_syn1588_type( dh ) );

  rc = mbg_get_device_info( dh, &dev );

  if ( mbg_cond_err_msg( rc, "mbg_get_device_info" ) )
    goto done;

  if ( !is_syn1588 )
  {
    if ( mbg_rc_is_error( check_irq_safe( dh, &dev ) ) )
      goto done;

    if ( verbose )
    {
      show_dev_cpu_info( dh );
      printf( "Firmware ID: \"%s\"\n", _pcps_fw_id( &dev ) );
    }
  }

  if ( list_only )
    goto done;


  // Now we collect the majority of device information.
  check_all_device_features( dh, p_ds, &dev, is_syn1588 );

  for (;;)
  {
    collect_all_device_status( dh, p_ds );
    print_all_device_status( p_ds );

    if ( verbose && p_ds->is_tcr )
      show_irig_debug_status( dh );

    if ( mbg_rc_is_success( mbg_chk_dev_has_lan_intf( dh ) ) )
      show_lan_intf_state( dh );

    if ( mbg_rc_is_success( mbg_chk_dev_has_ptp( dh ) ) )
      show_ptp_state( dh );

    if ( verbose >= 4 )
    {
      bool do_force_read = p_ds->has_xmr ? false : force_read;

      if ( p_ds->has_xmr || do_force_read )
        show_all_xmr_status( dh, do_force_read ? "MRS status (disabled, forced access):" : "MRS status:",
                             "  ", do_force_read );
    }

    show_invt_reason();

    if ( loops > 0 )
      loops--;

    if ( loops == 0 )
      break;

    if ( sleep_secs )
      sleep( sleep_secs );
    else
      if ( sleep_usecs )
        usleep( sleep_usecs );

    // If this_loops is < 0, loop forever.

    printf( "\n" );
  }

done:

  return ret_val;

}  // do_mbgstatus

static MBG_DEV_HANDLER_FNC do_mbgstatus;



static /*HDR*/
void usage( void )
{
  mbg_print_usage_intro( pname, true,
    "This program prints status information for a device.\n"
    "The displayed information depends on the type of the card."
  );
  mbg_print_help_options();
  mbg_print_opt_info( "-c", "Run continuously" );
  mbg_print_opt_info( "-l", "List devices only" );
  mbg_print_opt_info( "-L", "List unique names of all devices" );
  mbg_print_opt_info( "-n num", "Run num loops" );
  mbg_print_opt_info( "-s num", "Sleep num seconds between calls (implies -c)" );
  mbg_print_opt_info( "-u num", "Sleep num microseconds between calls (implies -c)" );
  mbg_print_opt_info( "-v", "Increase verbosity" );
  mbg_print_opt_info( "-F", "Force-read data. This can be very dangerous, so use with care!" );
  mbg_print_usage_outro( DEV_OPT_PRINT_BUS_LEVEL, true );

}  // usage



int main( int argc, char *argv[] )
{
  int c;
  int rc;

  ctry_setup( 0 );
  language = LNG_ENGLISH;
  ctry.dt_fmt = DT_FMT_YYYYMMDD;
  ctry.dt_sep = '-';

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "cFlLn:s:u:vh?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'c':
        loops = -1;
        break;

      case 'F':
        force_read = 1;
        break;

      case 'l':
        list_only = 1;
        break;

      case 'L':
        list_only_unique_names = 1;
        break;

      case 'n':
        loops = atoi( optarg );
        break;

      case 's':
        sleep_secs = atoi( optarg );
        loops = -1;
        break;

      case 'u':
        sleep_usecs = atoi( optarg );
        loops = -1;
        break;

      case 'v':
        verbose++;
        break;

      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }


  if ( list_only_unique_names )
  {
    MBG_DEV_NAME_LIST_ENTRY *list_head = NULL;
    int n_dev = mbg_find_devices_with_names( &list_head, N_SUPP_DEV_BUS );

    if ( n_dev )
    {
      MBG_DEV_NAME_LIST_ENTRY *pos;

      printf( "Unique names of devices found:\n" );

      for ( pos = list_head; pos; pos = pos->next )
        printf( "  %s\n", pos->dev_name );
    }
    else
      mbg_print_no_device_found();

    mbg_free_device_name_list( list_head );
    printf( "\n" );

    return MBG_EXIT_CODE_SUCCESS;
  }

  if ( verbose )
    pcps_date_time_dist = 1;

  // Handle each of the specified devices.
  rc = mbg_handle_devices( argc, argv, optind, do_mbgstatus, CHK_DEV_ALL_DEVICES );

  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
