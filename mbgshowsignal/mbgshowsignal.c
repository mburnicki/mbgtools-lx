
/**************************************************************************
 *
 *  $Id: mbgshowsignal.c 1.17 2022/12/21 15:40:14 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for mbgshowsignal program which demonstrates how to
 *    access a Meinberg device via IOCTL calls and show the modulation
 *    signal (second marks) of a longwave receiver, e.g. for DCF77.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgshowsignal.c $
 *  Revision 1.17  2022/12/21 15:40:14  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from a function.
 *  Revision 1.16  2021/11/15 17:08:55  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.15  2021/04/12 21:57:52  martin
 *  Updated printing of usage information.
 *  Revision 1.14  2021/03/21 22:40:19  martin
 *  Updated some comments.
 *  Revision 1.13  2021/03/12 12:32:22  martin
 *  Updated some comments.
 *  Revision 1.12  2020/02/27 13:30:35  martin
 *  mbg_show_pzf_corr_info() now just prints the  correlation info,
 *  but doesn't read it from the device, so we read it here.
 *  Revision 1.11  2019/03/19 16:43:51  martin
 *  Support force reading status port even if device provides no modulation signal.
 *  Revision 1.10  2018/11/15 12:12:36  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.9  2017/07/05 18:31:14  martin
 *  New way to maintain version information.
 *  Support build on Windows.
 *  Update modulation status continuously.
 *  Show PZF correlation state.
 *  Use codes and inline functions from mbgerror.h.
 *  Proper return codes and exit codes.
 *  Revision 1.8  2009/09/29 15:02:15  martin
 *  Updated version number to 3.4.0.
 *  Revision 1.7  2009/07/24 09:50:09  martin
 *  Updated version number to 3.3.0.
 *  Revision 1.6  2009/06/19 12:38:52  martin
 *  Updated version number to 3.2.0.
 *  Revision 1.5  2009/03/19 17:04:26  martin
 *  Updated version number to 3.1.0.
 *  Updated copyright year to include 2009.
 *  Revision 1.4  2008/12/22 12:44:43  martin
 *  Changed ealier program name mbgdcfmod to mbgshowsignal.
 *  Updated description, copyright, revision number and string.
 *  Use unified functions from toolutil module.
 *  Accept device name(s) on the command line.
 *  Don't use printf() without format, which migth produce warnings
 *  with newer gcc versions.
 *  Revision 1.3  2007/07/24 09:32:11  martin
 *  Updated copyright to include 2007.
 *  Revision 1.2  2003/04/25 10:28:05  martin
 *  Use new functions from mbgdevio library.
 *  New program version v2.1.
 *  Revision 1.1  2001/09/17 15:08:09  martin
 *
 **************************************************************************/

// Include Meinberg headers.
#include <mbgdevio.h>
#include <toolutil.h>  // Common utility functions.

// Include system headers.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define MBG_FIRST_COPYRIGHT_YEAR   2001
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.

static const char *pname = "mbgshowsignal";

static int force_read;



static /*HDR*/
int show_modulation( MBG_DEV_HANDLE dh )
{
  static time_t prv_sys_t;
  time_t sys_t;
  PCPS_STATUS_PORT status_port;   // Current value of the status port of the device.
  PCPS_TIME t;
  int signal;

  bool dev_has_pzf = mbg_chk_dev_has_pzf( dh ) == MBG_SUCCESS;

  int rc = mbg_get_status_port( dh, &status_port );  // Read status port.

  if ( mbg_cond_err_msg( rc, "mbg_get_status_port" ) )
    return rc;

  // Show signal only once per second.
  sys_t = time( NULL );

  if ( sys_t != prv_sys_t )
  {
    rc = mbg_get_time( dh, &t );

    if ( mbg_cond_err_msg( rc, "mbg_get_time" ) )
      return rc;

    prv_sys_t = sys_t;
  }

  // The current value of the modulation (second mark) is returned
  // in a single bit of the status port, so ignore the other bits.
  printf( "\rMod: %c", ( status_port & PCPS_ST_MOD ) ? '*' : '_' );

  signal = t.signal - PCPS_SIG_BIAS;

  if ( signal < 0 )
    signal = 0;
  else
    if ( signal > PCPS_SIG_MAX )
      signal = PCPS_SIG_MAX;

  printf( "  Signal: %u%%  ", signal * 100 / PCPS_SIG_MAX );

  if ( dev_has_pzf )
  {
    CORR_INFO ci;

    rc = mbg_get_corr_info( dh, &ci );

    if ( mbg_rc_is_success( rc ) )
      mbg_show_pzf_corr_info( &ci, 1 );
  }

  printf( "  " );

  return rc;

}  // show_modulation



static /*HDR*/
int do_mbgshowsignal( MBG_DEV_HANDLE dh )
{
  int do_force_read = 0;
  int rc = mbg_chk_dev_has_mod( dh );

  if ( mbg_rc_is_error( rc ) )
  {
    if ( ( rc != MBG_ERR_NOT_SUPP_BY_DEV ) || !force_read )
    {
      mbg_cond_err_msg_info( rc, "mbg_dev_has_mod",
                             "support monitoring signal modulation" );
      return rc;
    }
    else
      do_force_read = 1;
  }


  printf( "\nMonitoring signal modulation" );

  if ( do_force_read )
    printf( " (forced)" );

  printf( ":\n" );

  for (;;)
  {
    rc = show_modulation( dh );

    if ( mbg_rc_is_error( rc ) )
      break;
  }

  return rc;

}  // do_mbgshowsignal

static MBG_DEV_HANDLER_FNC do_mbgshowsignal;



static /*HDR*/
void usage( void )
{
  mbg_print_usage_intro( pname, true,
    "This program displays the modulation signal of devices which receive\n"
    "a slowly modulated input signal, e.g. the longwave signal from DCF77.\n"
  );
  mbg_print_help_options();
  mbg_print_opt_info( "-F", "Force reading status port even if signal not supported" );
  mbg_print_usage_outro( DEV_OPT_PRINT_BUS_LEVEL, false );

}  // usage



int main( int argc, char *argv[] )
{
  int c;
  int rc;

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "Fh?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'F':
        force_read = 1;
        break;

      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }

  // Handle each of the specified devices.
  rc = mbg_handle_devices( argc, argv, optind, do_mbgshowsignal, 0 );

  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
