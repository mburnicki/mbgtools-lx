
/**************************************************************************
 *
 *  $Id: mbgversion.h 1.30 2023/12/18 14:14:55 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Program version definitions for package mbgtools-lx and mbgtools-win.
 *
 **************************************************************************/

#include <words.h>   // Also includes mbg_tgt.h.

// --------------------------------------------------------------------
// The following definitions have to be updated to reflect the
// copyright year, and the software version numbers.
// --------------------------------------------------------------------

#define MBG_CURRENT_COPYRIGHT_YEAR      2023

#define MBG_MAJOR_VERSION_CODE          4
#define MBG_MINOR_VERSION_CODE          2
#define MBG_MICRO_VERSION_CODE          26


// --------------------------------------------------------------------
// The following definitions are derived from the definitions above.
// --------------------------------------------------------------------

#define MBG_CURRENT_COPYRIGHT_YEAR_STR  STRINGIFY( MBG_CURRENT_COPYRIGHT_YEAR )

#define MBG_MAJOR_VERSION_STR    STRINGIFY( MBG_MAJOR_VERSION_CODE )
#define MBG_MINOR_VERSION_STR    STRINGIFY( MBG_MINOR_VERSION_CODE )
#define MBG_MICRO_VERSION_STR    STRINGIFY( MBG_MICRO_VERSION_CODE )

#define MBG_SHORT_VERSION_CODE    ( ( MBG_MAJOR_VERSION_CODE << 8 ) | MBG_MINOR_VERSION_CODE )
#define MBG_SHORT_VERSION_STR     MBG_MAJOR_VERSION_STR "." MBG_MINOR_VERSION_STR

#define MBG_NUMERIC_VERSION_STR  MBG_SHORT_VERSION_STR "." MBG_MICRO_VERSION_STR

// If a version string has been derived from the git repo,
// we use that one preferably.
#if defined( MBG_VERSION_FROM_GIT )
  #define MBG_FULL_VERSION_STR  STRINGIFY( MBG_VERSION_FROM_GIT )
#else
  #define MBG_FULL_VERSION_STR  MBG_NUMERIC_VERSION_STR
#endif

