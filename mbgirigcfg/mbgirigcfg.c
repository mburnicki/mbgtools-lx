
/**************************************************************************
 *
 *  $Id: mbgirigcfg.c 1.19 2023/02/23 15:55:27 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for the mbgirigcfg program which can be used to configure
 *    the time code format and the UTC offset of the time contained in the
 *    time code.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgirigcfg.c $
 *  Revision 1.19  2023/02/23 15:55:27  martin.burnicki
 *  Fixed bugs that occurred when changing IRIG settings.
 *  Support of the TFOM flag was incorrectly always checked against
 *  the timecode format configured for the timecode output, so setting
 *  the flag for the input happened to be accepted, or not.
 *  Before trying to save the configuration of the timecode output,
 *  it was incorrectly checked for the presence of a timecode input,
 *  so saving the configuration of the output of a device failed
 *  if the device did not also have an input.
 *  Also modified some message texts and did some cleanup.
 *  Revision 1.18  2022/12/21 15:20:27  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from some functions,
 *  and use feature check functions instead of macros.
 *  Removed some obsolete _int_from_size_t() stuff.
 *  Revision 1.17  2021/04/12 21:57:44  martin
 *  Updated printing of usage information.
 *  Revision 1.16  2021/03/21 18:03:09  martin
 *  Updated some comments, and a few output messages.
 *  Revision 1.15  2021/03/12 11:50:19  martin
 *  Corrected the wording of some comments.
 *  Revision 1.14  2018/11/15 12:12:34  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.13  2018/01/15 15:16:00  martin
 *  Let snprint_hours_mins() return an int value.
 *  Revision 1.12  2017/08/18 08:15:44  martin
 *  Fixed error handling.
 *  Revision 1.11  2017/07/05 19:05:27  martin
 *  New way to maintain version information.
 *  Support build on Windows.
 *  New parameter -i to let incoming TFOM flag be ignored.
 *  Accept parameter -? to print usage.
 *  Use getopt() for option processing.
 *  Support multiple devices.
 *  New parameter -X which set ref offs to 'unconfigured'.
 *  Support specification of minutes for ref offset.
 *  Use safe string functions.
 *  Use more functions from common library modules.
 *  Use codes and inline functions from mbgerror.h.
 *  Proper return codes and exit codes.
 *  Revision 1.10  2009/09/29 15:02:15  martin
 *  Updated version number to 3.4.0.
 *  Revision 1.9  2009/07/24 09:50:08  martin
 *  Updated version number to 3.3.0.
 *  Revision 1.8  2009/06/19 12:38:51  martin
 *  Updated version number to 3.2.0.
 *  Revision 1.7  2009/03/19 17:07:16  martin
 *  Updated version number to 3.1.0.
 *  Updated copyright year to include 2009.
 *  Include mbgtime.h for some symbolic constants.
 *  Revision 1.6  2008/12/22 12:42:58  martin
 *  Updated description, copyright, revision number and string.
 *  Use unified functions from toolutil module.
 *  Accept device name(s) on the command line.
 *  Don't use printf() without format, which migth produce warnings
 *  with newer gcc versions.
 *  Revision 1.5  2007/07/24 09:34:46  martin
 *  Changes due to renamed library symbols.
 *  Updated copyright to include 2007.
 *  Revision 1.4  2006/08/28 10:08:17  martin
 *  Display TFOM settings only it the selected IRIG code format supports TFOM.
 *  Revision 1.3  2006/07/03 13:27:31  martin
 *  Source code cleanup and fixed a typo.
 *  Revision 1.2  2004/11/08 15:59:10  martin
 *  Support separate config of IRIG RX and TX.
 *  Changes due to renamed symbols.
 *  Revision 1.1  2003/04/25 10:42:10  martin
 *
 **************************************************************************/

// Include Meinberg headers.
#include <mbgdevio.h>
#include <mbgutil.h>
#include <mbgtime.h>
#include <pcpsmktm.h>
#include <pcpslstr.h>
#include <myutil.h>
#include <toolutil.h>  // Common utility functions.

// Include system headers.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#define MBG_FIRST_COPYRIGHT_YEAR   2003
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.

static const char pname[] = "mbgirigcfg";


static int glb_argc;
static char **glb_argv;

static const char str_yes[] = "YES";
static const char str_no[] = "NO";
static const char str_not_supp[] = "  (not supp. by this code format)";

static const char info_curr[] = "Current";
static const char info_new[] = "New";
static const char msg_rx[] = DEFAULT_OPT_NAME_IRIG_RX_EN;
static const char msg_tx[] = DEFAULT_OPT_NAME_IRIG_TX_EN;

static IRIG_INFO irig_rx_info;
static IRIG_INFO irig_tx_info;
static MBG_REF_OFFS ref_offs;
static MBG_OPT_INFO opt_info;

static bool cfg_err_rx;
static bool cfg_err_tx;
static bool changed_cfg_rx;
static bool changed_cfg_tx;
static bool warned_no_rx;
static bool warned_no_tx;

static bool must_print_help_info;

static const char *icode_rx_names[N_ICODE_RX] = DEFAULT_ICODE_RX_NAMES;
static const char *icode_rx_descr[N_ICODE_RX] = DEFAULT_ICODE_RX_DESCRIPTIONS_ENG;

static const char *icode_tx_names[N_ICODE_TX] = DEFAULT_ICODE_TX_NAMES;
static const char *icode_tx_descr[N_ICODE_TX] = DEFAULT_ICODE_TX_DESCRIPTIONS_ENG;

static char str_ref_offs_min[16];   // Need to be snprint_hours_mins'ed with -MBG_REF_OFFS_MAX.
static char str_ref_offs_max[16];   // Need to be snprint_hours_mins'ed with +MBG_REF_OFFS_MAX.



static /*HDR*/
int snprint_hours_mins( char *s, size_t max_len, long num_minutes )
{
  ldiv_t ldt = ldiv( labs( num_minutes ), MINS_PER_HOUR );

  int n = mbg_snprintf( s, max_len, "%c%li",
                           ( num_minutes < 0 ) ? '-' : '+', ldt.quot );

  if ( ldt.rem )
    n += mbg_snprintf( &s[n], max_len - n, ":%02li", ldt.rem );

  n += mbg_snprintf( &s[n], max_len - n, "h" );

  return n;

}  // snprint_hours_mins



static /*HDR*/
void print_cfg_rx( const char *info, const char *msg )
{
  int idx = irig_rx_info.settings.icode;
  char ws[16];
  const char *cp = NULL;

  printf( "%s %s configuration:\n", info, msg );

  printf( "  " DEFAULT_STR_IRIG_FMT_EN ":   %s (%s)\n",
          icode_rx_names[idx],
          icode_rx_descr[idx]
        );

  printf( "  " DEFAULT_IGNORE_RX_TFOM_EN ":   %s%s\n",
          ( irig_rx_info.settings.flags & IFLAGS_DISABLE_TFOM ) ? str_yes : str_no,
          ( _idx_bit( idx ) & MSK_ICODE_RX_HAS_TFOM ) ? "" : str_not_supp );

  printf( "  " DEFAULT_STR_IRIG_OFFS_EN ":   " );

  if ( labs( ref_offs ) > MBG_REF_OFFS_MAX )
    cp = "** not configured **";
  else
  {
    snprint_hours_mins( ws, sizeof( ws ), ref_offs );
    cp = ws;
  }

  printf( "%s", cp );

  if ( _idx_bit( idx ) & MSK_ICODE_RX_HAS_TZI )
    printf( " (ignored with selected code format \"%s\")", icode_rx_names[idx] );

  printf( "\n" );

  if ( opt_info.supp_flags & MBG_OPT_FLAG_STR_UTC )
    printf( "  " DEFAULT_STR_IRIG_TIMESTR_UTC_EN ":   %s\n",
            ( opt_info.settings.flags & MBG_OPT_FLAG_STR_UTC ) ?
            str_yes : str_no );

}  // print_cfg_rx



static /*HDR*/
void print_cfg_tx( const char *info, const char *msg )
{
  int idx = irig_tx_info.settings.icode;

  printf( "%s %s configuration:\n", info, msg );

  printf( "  " DEFAULT_STR_IRIG_FMT_EN ":   %s (%s)\n",
          icode_tx_names[idx],
          icode_tx_descr[idx]
        );

  printf( "  " DEFAULT_STR_TFOM_ALWAYS_SYNC_EN ":   %s%s\n",
          ( irig_tx_info.settings.flags & IFLAGS_DISABLE_TFOM ) ? str_yes : str_no,
          ( _idx_bit( idx ) & MSK_ICODE_TX_HAS_TFOM ) ? "" : str_not_supp );

  printf( "  " DEFAULT_STR_IRIG_OUTPUT_LOC_TM_EN ":   %s\n",
    ( irig_tx_info.settings.flags & IFLAGS_TX_GEN_LOCAL_TIME ) ?
    str_yes : str_no
  );

}  // print_cfg_tx



static /*HDR*/
void set_new_icode_rx( char *s )
{
  int idx = atoi( s );

  if ( idx >= N_ICODE_RX )
  {
    fprintf( stderr, "** IRIG RX code index %i is out of range (0..%i).\n",
             idx, N_ICODE_RX - 1 );
    cfg_err_rx = true;
    return;
  }

  if ( _is_supported( idx, irig_rx_info.supp_codes ) )
  {
    irig_rx_info.settings.icode = idx;
    changed_cfg_rx = true;
  }
  else
  {
    fprintf( stderr, "** IRIG RX code \"%s\" not supported by this device.\n",
             icode_rx_names[idx] );
    cfg_err_rx = true;
  }

}  // set_new_icode_rx



static /*HDR*/
void set_new_ref_offs( char *s )
{
  long new_ref_offs;
  char *cp = s;
  int is_negative = 0;

  // Expected format: [-]hh[:mm]

  // In case of e.g. "-0:30", conversion of the first part "-0" would yield
  // 0 and thus the sign would get lost, so we check the sign explicitly.
  if ( *cp == '-' )
    is_negative = 1;

  new_ref_offs = strtol( cp, &cp, 10 );

  if ( cp == s )  // No number found at beginning.
    goto invalid;

  new_ref_offs *= MINS_PER_HOUR;

  if ( *cp++ == ':' )  // Offset minutes seem to follow.
  {
    long tmp = strtol( cp, &cp, 10 );

    // The value behind the colon should always be positive.
    if ( tmp < 0 || tmp >= MINS_PER_HOUR )
      goto invalid;

    // Apply the minutes offset according to the sign.
    new_ref_offs += is_negative ? -tmp : tmp;
  }

  if ( labs( new_ref_offs ) > MBG_REF_OFFS_MAX )
  {
    char ws[16];
    snprint_hours_mins( ws, sizeof( ws ), new_ref_offs );
    fprintf( stderr, "** New IRIG time offset %s exceeds range (%s..%s).\n",
             ws, str_ref_offs_min, str_ref_offs_max );
    goto invalid;
  }

  ref_offs = (MBG_REF_OFFS) new_ref_offs;
  changed_cfg_rx = true;

  return;


invalid:
  cfg_err_rx = true;

}  // set_new_ref_offs



static /*HDR*/
void set_ref_offs_not_cfgd( void )
{
  ref_offs = MBG_REF_OFFS_NOT_CFGD;
  changed_cfg_rx = true;

}  // set_ref_offs_not_cfgd



static /*HDR*/
void set_new_str_utc( char *s )
{
  int this_cfg_err;

  if ( !( opt_info.supp_flags & MBG_OPT_FLAG_STR_UTC ) )
    return;  // Not supported.


  this_cfg_err = 0;

  if ( strcmp( s, "1" ) == 0 )
    opt_info.settings.flags |= MBG_OPT_FLAG_STR_UTC;
  else
    if ( strcmp( s, "0" ) == 0 )
      opt_info.settings.flags &= ~MBG_OPT_FLAG_STR_UTC;
    else
      this_cfg_err = 1;

  if ( this_cfg_err )
    cfg_err_rx = true;
  else
    changed_cfg_rx = true;

}  // set_new_str_utc



static /*HDR*/
void set_new_icode_tx( char *s )
{
  int idx = atoi( s );

  if ( idx >= N_ICODE_TX )
  {
    fprintf( stderr, "** IRIG TX code index %i is out of range (0..%i).\n",
             idx, N_ICODE_TX - 1 );
    cfg_err_tx = true;
    return;
  }

  if ( _is_supported( idx, irig_tx_info.supp_codes ) )
  {
    irig_tx_info.settings.icode = idx;
    changed_cfg_tx = true;
  }
  else
  {
    fprintf( stderr, "** IRIG TX code \"%s\" not supported by this device.\n",
             icode_tx_names[idx] );
    cfg_err_tx = true;
  }

}  // set_new_icode_tx



static /*HDR*/
void set_new_irig_tx_local( char *s )
{
  int this_cfg_err = 0;

  if ( strcmp( s, "1" ) == 0 )
    irig_tx_info.settings.flags |= IFLAGS_TX_GEN_LOCAL_TIME;
  else
    if ( strcmp( s, "0" ) == 0 )
      irig_tx_info.settings.flags &= ~IFLAGS_TX_GEN_LOCAL_TIME;
    else
      this_cfg_err = 1;

  if ( this_cfg_err )
    cfg_err_tx = true;
  else
    changed_cfg_tx = true;

}  // set_new_irig_tx_local



static /*HDR*/
void set_new_tfom_flag( char *s, IRIG_SETTINGS *p, int supp_mask,
                        bool *changed_flag, bool *err_flag )
{
  if ( !(_idx_bit( p->icode ) & supp_mask ) )
    return;  // not supported


  if ( strcmp( s, "1" ) == 0 )
    p->flags |= IFLAGS_DISABLE_TFOM;
  else
    if ( strcmp( s, "0" ) == 0 )
      p->flags &= ~IFLAGS_DISABLE_TFOM;
    else
    {
      *err_flag = true;
      return;
    }

  *changed_flag = true;

}  // set_new_tfom_flag



static /*HDR*/
bool chk_dev_rx( bool dev_opened, bool dev_is_rx )
{
  if ( dev_opened )
  {
    if ( dev_is_rx )
      return true;

    if ( !warned_no_rx )
    {
      fprintf( stderr, "** This device does not provide an IRIG input.\n" );
      warned_no_rx = 1;
    }

    cfg_err_rx = true;
  }

  return false;

}  // chk_dev_rx



static /*HDR*/
bool chk_dev_tx( bool dev_opened, bool dev_has_tx )
{
  if ( dev_opened )
  {
    if ( dev_has_tx )
      return true;

    if ( !warned_no_tx )
    {
      fprintf( stderr, "** This device does not provide an IRIG output.\n" );
      warned_no_tx = 1;
    }

    cfg_err_tx = true;
  }

  return false;

}  // chk_dev_tx



static /*HDR*/
void check_cmd_line( int argc, char *argv[], MBG_DEV_HANDLE dh, bool dev_is_tcr, bool dev_has_tx )
{
  bool dev_opened = dh != MBG_INVALID_DEV_HANDLE;
  int c;

  // Force checking all parameters because this function
  // may be called several times.
  optind = 1;

  while ( ( c = getopt( argc, argv, "h?" "r:o:u:i:X" "t:l:s:"  ) ) != -1 )
  {
    switch ( c )
    {
      case '?':
      case 'h':
        must_print_usage = true;
        break;


      // IRIG receiver options.

      case 'r':    // IRIG RX code format.
        if ( chk_dev_rx( dev_opened, dev_is_tcr ) )
          set_new_icode_rx( optarg );
        break;

      case 'o':    // Incoming time offset from UTC.
        if ( chk_dev_rx( dev_opened, dev_is_tcr ) )
          set_new_ref_offs( optarg );
        break;

      case 'u':    // Option flag: serial output is always UTC.
        if ( chk_dev_rx( dev_opened, dev_is_tcr ) )
          set_new_str_utc( optarg );
        break;

      case 'i':    // Option flag: ignore incoming TFOM flag, alway sync.
        if ( chk_dev_rx( dev_opened, dev_is_tcr ) )
          set_new_tfom_flag( optarg, &irig_rx_info.settings, MSK_ICODE_RX_HAS_TFOM,
                             &changed_cfg_rx, &cfg_err_rx );
        break;

      case 'X':    // Set UTC ref_offs to "not configured".
        if ( chk_dev_rx( dev_opened, dev_is_tcr ) )
          set_ref_offs_not_cfgd();
        break;


      // IRIG output options.

      case 't':    // IRIG TX code format.
        if ( chk_dev_tx( dev_opened, dev_has_tx ) )
          set_new_icode_tx( optarg );
        break;

      case 'l':    // Option flag: transmit local time, not UTC.
        if ( chk_dev_tx( dev_opened, dev_has_tx ) )
          set_new_irig_tx_local( optarg );
        break;

      case 's':    // Option flag: always send TFOM as "sync".
        if ( chk_dev_tx( dev_opened, dev_has_tx ) )
          set_new_tfom_flag( optarg, &irig_tx_info.settings, MSK_ICODE_TX_HAS_TFOM,
                             &changed_cfg_tx, &cfg_err_tx );
        break;

    }  // switch

  }

}  // check_cmd_line



static /*HDR*/
void usage( void )
{
  const char *frame_fmt = "           %4i  %s, %s\n";
  int i;

  printf(
    "Usage:  %s [-h]|[option] [option] ...\n"
    "\n",
    pname
  );

  printf(
    "  -h or -?  prints this info\n"
    "\n"
  );


  printf(
    "Options supported by IRIG receivers:\n"
    "\n"
  );

  printf(
    "  -r code   Specifies the IRIG receiver code format, where \"code\" \n"
    "            can be one a number according to the table below:\n"
  );

  for ( i = 0; i < N_ICODE_RX; i++ )
    printf( frame_fmt, i, icode_rx_names[i], icode_rx_descr[i] );

  printf( "\n" );

  printf(
    "  -o offs   Specifies the IRIG input time offset from UTC, in hours and optional minute,\n"
    "            where \"offs\" can be a value in the range %s .. %s.\n"
    "            When the device is shipped this parameter is set to \"not configured\"\n"
    "            to prevent the receiver from synchronizing to an IRIG input signal with\n"
    "            unknown UTC offset immediately after installation, which could cause\n"
    "            the system time to be set wrong by the time synchronization software.\n"
    "\n",
    str_ref_offs_min, str_ref_offs_max
  );

  printf(
    "  -X        Set the IRIG input time offset from UTC to \"not configured\".\n"
    "            See also the -o option.\n"
    "\n"
  );

  printf(
    "  -u flag   Determines whether the time string sent via the\n"
    "            serial output of the device contains always UTC time\n"
    "            or the same time as received from the IRIG input\n"
    "            signal. \"flag\" can be \"1\" for always UTC,\n"
    "            or \"0\" for IRIG time.\n"
    "\n"
  );

  printf(
    "  -i flag   If \"flag\" is set to \"1\", the TFOM qualifier in the incoming\n"
    "            time code signal is ignored, i.e. the receiver even synchronizes\n"
    "            to an input signal if the TFOM codes says the generator is freewheeling.\n"
    "            Please note that most IRIG codes do not support a TFOM qualifier.\n"
    "\n"
  );


  printf(
    "Options supported by IRIG transmitters:\n"
    "\n"
  );

  printf(
    "  -t code   Specifies the IRIG transmitter code format, where \"code\" \n"
    "            can be one a number according to the table below:\n"
  );

  for ( i = 0; i < N_ICODE_TX; i++ )
    printf( frame_fmt, i, icode_tx_names[i], icode_tx_descr[i] );

  printf( "\n" );

  printf(
    "  -l flag   Determines whether IRIG output shall contain local\n"
    "            time, or UTC. \"flag\" must be \"1\" for local time,\n"
    "            or \"0\" for UTC.\n"
    "\n"
  );

  printf(
    "  -s flag   If \"flag\" is set to \"1\", the TFOM qualifier in the outgoing\n"
    "            time code signal is always set to \"synchronized\". If it is \"0\",\n"
    "            the qualifier reflects the current status of the input signal.\n"
    "            Please note that most IRIG codes do not support a TFOM qualifier.\n"
    "\n"
  );


  printf(
    "If no parameters are given, the program reports the current settings.\n"
    "Please note that not all IRIG codes must be supported by all devices.\n"
  );

}  // usage



static /*HDR*/
int do_mbgirigcfg( MBG_DEV_HANDLE dh )
{
  int rc = MBG_SUCCESS;

  bool dev_opened = dh != MBG_INVALID_DEV_HANDLE;
  bool dev_is_tcr = mbg_rc_is_success( mbg_chk_dev_is_tcr( dh ) );
  bool dev_has_irig_tx = mbg_rc_is_success( mbg_chk_dev_has_irig_tx( dh ) );

  cfg_err_rx = false;
  cfg_err_tx = false;
  changed_cfg_rx = false;
  changed_cfg_tx = false;
  warned_no_rx = false;
  warned_no_tx = false;

  if ( !( dev_is_tcr || dev_has_irig_tx ) )
  {
    fprintf( stderr, "** This device does not provide an IRIG input or output.\n" );
    rc = MBG_ERR_NOT_SUPP_BY_DEV;
    must_print_help_info = true;
    goto done;
  }

  if ( dev_is_tcr )
  {
    rc = mbg_get_all_irig_rx_info( dh, NULL, &irig_rx_info,
                                   &ref_offs, &opt_info );
    if ( mbg_cond_err_msg( rc, "read IRIG input configuration" ) )
      goto done;
  }

  if ( dev_has_irig_tx )
  {
    rc = mbg_get_irig_tx_info( dh, &irig_tx_info );

    if ( mbg_cond_err_msg( rc, "read IRIG output configuration" ) )
      goto done;
  }

  check_cmd_line( glb_argc, glb_argv, dh, dev_is_tcr, dev_has_irig_tx );

  if ( cfg_err_rx || cfg_err_tx )
  {
    printf( "** Invalid configuration options specified.\n" );
    must_print_help_info = true;
    rc = MBG_ERR_CFG;
    goto done;
  }

  if ( changed_cfg_rx )
  {
    if ( chk_dev_rx( dev_opened, dev_is_tcr ) )
    {
      print_cfg_rx( info_new, msg_rx );

      rc = mbg_save_all_irig_rx_settings( dh, NULL, &irig_rx_info.settings,
                                          &ref_offs, &opt_info.settings );
      if ( mbg_cond_err_msg( rc, "write IRIG input configuration" ) )
        goto done;
    }
  }
  else  // RX cfg not changed.
    if ( dev_is_tcr )
      print_cfg_rx( info_curr, msg_rx );


  if ( changed_cfg_tx )
  {
    if ( chk_dev_tx( dev_opened, dev_has_irig_tx ) )
    {
      print_cfg_tx( info_new, msg_tx );

      rc = mbg_set_irig_tx_settings( dh, &irig_tx_info.settings );

      if ( mbg_cond_err_msg( rc, "write IRIG output configuration" ) )
        goto done;
    }
  }
  else
    if ( dev_has_irig_tx )
      print_cfg_tx( info_curr, msg_tx );

done:
  return rc;

}  // do_mbgirigcfg

static MBG_DEV_HANDLER_FNC do_mbgirigcfg;



int main( int argc, char *argv[] )
{
  int rc;

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  snprint_hours_mins( str_ref_offs_min, sizeof( str_ref_offs_min ), -MBG_REF_OFFS_MAX );
  snprint_hours_mins( str_ref_offs_max, sizeof( str_ref_offs_max ), MBG_REF_OFFS_MAX );

  check_cmd_line( argc, argv, MBG_INVALID_DEV_HANDLE, false, false );

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }


  // We work with copies of the arguments here because the arguments
  // are evaluated once more for each device.
  glb_argc = argc;
  glb_argv = argv;

  // Handle each of the specified devices.
  rc = mbg_handle_devices( glb_argc, glb_argv, optind, do_mbgirigcfg, 0 );

  if ( must_print_help_info )
    printf( "For help type \"%s -h\"\n\n", pname );

  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
