
/**************************************************************************
 *
 *  $Id: mbgsvcd.c 1.19 2022/08/25 14:34:07 martin.burnicki REL_M $
 *
 *  Main file for mbgsvcd which compares the system time to the
 *  time from a Meinberg device, and feeds these data pairs
 *  to NTP shared memory (SHM) segments.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgsvcd.c $
 *  Revision 1.19  2022/08/25 14:34:07  martin.burnicki
 *  Don't enter holdover mode if device has no valid time.
 *  Fixed usage msg with regard to trust time.
 *  Revision 1.18  2022/08/24 14:12:23  martin.burnicki
 *  Use feature check functions instead of macros.
 *  Revision 1.17  2021/12/01 19:06:54  martin.burnicki
 *  Cleaned up conditional support for serial devices.
 *  Added conditional support for PPS.
 *  Revision 1.16  2021/11/16 14:41:03  martin.burnicki
 *  A SYN1588 device has to be identified as bus level device.
 *  Revision 1.15  2021/11/16 14:18:12  martin.burnicki
 *  Improved device status tracking and logging.
 *  New parameter -u which lets the SHM unit be updated
 *  even if the NTP leap bits are 0x03.
 *  Revision 1.14  2021/11/15 17:08:54  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.13  2021/06/10 13:45:55  martin
 *  Added usage info for -Q option.
 *  Revision 1.12  2021/05/05 10:53:46  martin
 *  Conditional basic support for serial time strings.
 *  Revision 1.11  2021/04/29 14:20:13  martin
 *  Variable pc_cycles_frequency was renamed to mbg_pc_cycles_frequency.
 *  Revision 1.10  2021/04/29 13:24:55  martin
 *  Updated printing of usage information.
 *  Use bool type for some flag variables.
 *  Renamed some local variables.
 *  Revision 1.9  2021/03/26 10:35:36  martin
 *  New parameter -Q for a query-only mode in which the program runs in
 *  the foreground, doesn't write to the shared memory, and hence can be
 *  run even if another instance is already running in the backround.
 *  Huge code cleanup, using library functions for forking and PID file
 *  handling.
 *  Updated lots of comments.
 *  Revision 1.8  2020/10/15 09:40:29  martin
 *  Fixed segfault when launching mbgsvcd with -s option.
 *  Caused by a missing ':' specifier in getopt string resulting
 *  in atoi() possibly being called with NULL parameter.
 *  Patch provided by Jean-Yves Faye <jean-yves.faye@csgroup.eu>.
 *  Revision 1.7  2019/09/25 16:07:45  martin
 *  Removed inclusion of obsolete header pcpsutil.h.
 *  Revision 1.6  2019/02/11 09:57:20  martin
 *  Account for renamed library symbols.
 *  Use an individual filter for each device.
 *  Revision 1.5  2018/11/15 12:12:33  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.4  2017/07/05 18:45:29  martin
 *  New way to maintain version information.
 *  Print PC cycles counter frequency at program start.
 *  Use /var/run as directory for the lockfile.
 *  Using generic MBG_SYS_TIME with nanosecond resolution.
 *  Workaround in case cycle frequency can not be determined.
 *  Compute execution time limit in cycles instead of us so this can also
 *  be done if the cycle counter clock rate can not be determined.
 *  Skip devices which don't support HR time immediately at startup.
 *  Log reasons for error if function calls fail.
 *  Combined printf() and syslog() to mbg_log().
 *  Added leap second support.
 *  Moved some code to some extra modules which can be shared.
 *  Use seconds for the trust time.
 *  Support options -q and -r.
 *  Use more functions from common library modules.
 *  Use codes and inline functions from mbgerror.h.
 *  Patch submitted by <juergen.perlinger@t-online.de>:
 *  Support variable number of SHM units and number
 *  of the first unit to use.
 *  Proper return codes and exit codes.
 *  Revision 1.3  2010/03/03 14:59:36  martin
 *  Support -p parameter to pretend sync.
 *  Revision 1.2  2010/02/03 16:15:09  daniel
 *  Revision 1.1  2010/02/03 16:07:18  daniel
 *
 **************************************************************************/

// Include Meinberg headers.
#include <mbg_daemonize.h>
#include <mbg_pidfile.h>
#include <mbgdevio.h>
#include <toolutil.h>  // Common utility functions.
#include <str_util.h>
#include <mbgerror.h>
#include <pcpsmktm.h>
#include <chk_time_info.h>
#include <ntp_shm.h>

#if SUPP_SERIAL
  #include <mbgserio.h>
  #include <pcpststr.h>
#endif

#if !defined( SUPP_PPS )
  #define SUPP_PPS  SUPP_SERIAL
#endif

#if SUPP_PPS
  #include <mbg_pps.h>
#endif

// Include system headers.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <syslog.h>
#include <stdarg.h>
#include <time.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#define MBG_FIRST_COPYRIGHT_YEAR   2010
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.


#if defined( DEBUG )
  #define DEBUG_DEV_STATE 1  // Can be 0 or > 0.
#else
  #define DEBUG_DEV_STATE 0  // Always 0.
#endif


#if 0
  #define DEFAULT_TRUST_DAYS       4      ///< 4 days default refclock trust time.
  #define DEFAULT_TRUST_TIME_SECS  ( DEFAULT_TRUST_DAYS * SECS_PER_DAY ) ///< Trust time, in seconds.
#else
  #define DEFAULT_TRUST_TIME_SECS  0
#endif


static const char *pname = "mbgsvcd";

static bool foreground;                   ///< -f  Stay in foreground, don't fork.
static bool quiet;                        ///< -q  Quiet mode, suppress some output.
static bool query_only;                   ///< -Q  Query-only mode, don't update SHM units.
static bool glb_pretend_sync;             ///< -p  Pretend to SHM that the device is synchronized, even if it is not.
static bool always_update_shm;            ///< -U  Update SHM unit even if LEAP_NOTINSYNC.
static bool print_raw;                    ///< -r  Print raw time stamps.
static int sleep_intv = 1;                ///< -s  Sleep interval between time comparisons.
static ulong glb_trust_time_seconds       ///< -t  Global trust time [seconds].
         = DEFAULT_TRUST_TIME_SECS;
static int n_units = MAX_SHM_REFCLOCKS;   ///< -n  Total number of usable SHM units, default: all.
static int n_unit0 = 0;                   ///< -o  Index of first usable SHM unit, default: 0.

#if SUPP_SERIAL
  static bool serial_first;               ///< -S  Register serial sources first.
  static int adj_bits;                    ///< -a  Additional number of serial bits to adjust.
  static int ts_format_idx;               ///< -F  Time string format (index).
#endif

#if SUPP_PPS
  static int default_pps_mode = PPS_CAPTUREASSERT;  ///< -A (default), or -C for PPS_CAPTURECLEAR
#endif


static int frac_digits = 9;          ///< Number of fractional digits to print for time stamps.
static volatile int exit_requested;  ///< Exit request was signalled.

static int n_dev_bus;  ///< Number of bus level devices detected.
static int n_dev_shm;  ///< Number of devices used with NTP SHM.

#if SUPP_SERIAL
  #define POLL_TIMEOUT     1500  // [ms]

  // Support up to 2 serial devices
  // specified on the command line.
  #define MAX_DEV_SERIAL   2

  typedef struct
  {
    char *dev_name;           // The specified device name.

    BAUD_RATE cfgd_baud_rate;  // Configured baud rate, e.g. specified on the command line.
    char *cfgd_framing;        // Configured baud rate, e.g. specified on the command line.

    MBGSERIO_DEV *sdev;
    int tstr_idx;

    #if SUPP_PPS
      int pps_mode;
      PPS_DEV_INFO pps_dev_info;
    #endif

  } SER_DEV_INFO;

  static SER_DEV_INFO ser_dev_info[MAX_DEV_SERIAL];
  static int n_dev_serial;

  // Will be set to the number of entries in tstr_spec_table.
  static int n_tstr_fmt;

#endif  // SUPP_SERIAL



/**
 * @brief Device state flags (bit-coded).
 *
 * @see ::DEV_STATE_FLAG_MASKS
 */
typedef uint DEV_STATE_FLAGS;



/**
 * @brief Flag masks used with ::DEV_STATE_FLAGS.
 */
enum DEV_STATE_FLAG_MASKS
{
  DEV_STATE_SET                   = 0x0001,  ///< Hidden flag, indicating that state has been set.
  DEV_STATE_DATA_AVAIL            = 0x0002,  ///< Data could be read from the device.
  DEV_STATE_SYNCHRONIZED          = 0x0004,  ///< Device is synchronized.
  DEV_STATE_IN_HOLDOVER           = 0x0008,  ///< Device is not synchronized, but in holdover mode.
  DEV_STATE_INVALID_TIME          = 0x0010,  ///< Device reported "invalid time".
  DEV_STATE_INVALID_TIME_REPORTED = 0x0020   ///< "Invalid time" has already been reported.
};



/**
 * @brief A structure used to keep track of a device status.
 */
typedef struct
{
  MBG_DEV_HANDLE dh;          ///< Device handle.
  PCPS_DEV dev_info;          ///< Device info. See ::is_bus_level_device.

  #if SUPP_SERIAL
    SER_DEV_INFO *p_ser_di;
    MBG_THREAD_INFO ti;
    MBG_TSTR_RCV_INFO tstr_rcv_info;
  #endif

  MBG_CHK_TIME_INFO cti;      ///< The results of the latest ::mbg_chk_time_info call.
  int retries;                ///< Number of retries required when taking time stamp pairs.
  bool exec_time_ok;          ///< Indicates if the execution time to take a time stamp pair was short enough.
  int rc;                     ///< Return code from the API calls, data may be invalid if < 0.

  CYCLES_FILTER_DATA filter;  ///< Filter for the time stamp pairs.

  int dev_idx;                ///< The device index.
  int shm_unit;               ///< The SHM unit, -1 if unused.

  bool pretend_sync;          ///< Pretend the device is synchronized, even if it isn't.

  DEV_STATE_FLAGS state;      ///< Bit-coded current device state, see ::DEV_STATE_FLAG_MASKS.
  DEV_STATE_FLAGS prv_state;  ///< Bit-coded previous device state, see ::DEV_STATE_FLAG_MASKS.

  ulong trust_time_seconds;   ///< Trust time for this device [seconds].
  time_t trust_time_start;    ///< If != 0, the (monotonic) time when holdover mode started.
  time_t trust_time_end;      ///< If != 0, the (monotonic) time when holdover mode ends.

  int ntp_precision;          ///< The NTP presision value fed into the SHM unit.
  int ntp_leap;               ///< The NTP leap bits fed into the SHM unit.

} DEV_STATE;


static DEV_STATE devs[MAX_SHM_REFCLOCKS];           ///< Device info and states.

static struct shmTime *shmTime[MAX_SHM_REFCLOCKS];  ///< Pointers to SHM units.

static const char pidfile_name[] = DEFAULT_PIDFILE_DIR "mbgsvcd.pid";



static /*HDR*/
/**
 * @brief The function that actually outputs the logging message strings.
 *
 * Called by ::mbg_log and ::mbg_log_dev which create the strings.
 * Where the output goes, depends on the mode of operation.
 *
 * @param[in]  lvl       The log level, as with @a syslog.
 * @param[in]  s         Message string to be logged.
 * @param[in]  log_time  Flag telling whether a time stamp is to be prepended to the output.
 *
 * @see ::mbg_log
 * @see ::mbg_log_dev
 */
void do_log( int lvl, const char *s, bool log_time )
{
  // If running in the foreground (even as service under systemd)
  // we just need to send the output to stdout/stderr. Only if we
  // have forked, we send it to syslog.
  if ( foreground )
  {
    FILE *fp = ( lvl <= LOG_WARNING ) ? stderr : stdout;

    if ( log_time )
    {
      struct timespec ts;
      struct tm tm = { 0 };
      MBG_TIME64_T t64;
      clock_gettime( CLOCK_REALTIME, &ts );
      mbg_exp_time_t_to_time64_t( &t64, &ts.tv_sec );
      mbg_gmtime64( &tm, &t64 );

      fprintf( fp, "%04i-%02i-%02i %02i:%02i:%02i ",
               tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
               tm.tm_hour, tm.tm_min, tm.tm_sec );
    }

    fprintf( fp, "%s\n", s );
  }
  else
    syslog( lvl, "%s", s );

}  // do_log



/*HDR*/
/**
 * @brief Our own, main logging function.
 *
 * Compatible with @a syslog, see ::MBG_SYSLOG_FNC.
 *
 * Finally calls ::do_log to output the log message.
 *
 * @param[in]  lvl  The log level, as with @a syslog.
 * @param[in]  fmt  Format string according to subsequent parameters.
 * @param[in]  ...  Variable argument list according to the @p fmt format string.
 *
 * @see ::mbg_log_dev
 * @see :MBG_SYSLOG_FNC
 */
void mbg_log( int lvl, const char *fmt, ... )
{
  char s[256];
  va_list args;

  va_start( args, fmt );
  vsnprintf_safe( s, sizeof( s ), fmt, args );
  va_end( args );

  do_log( lvl, s, getppid() != 1 );

}  // mbg_log

MBG_SYSLOG_FNC mbg_log;



/*HDR*/
/**
 * @brief A specific log function that also shows specific device info.
 *
 * Finally calls ::do_log to output the log message.
 *
 * @param[in]  lvl   The log level, as with @a syslog.
 * @param[in]  p_ds  Optional pointer to device/status information, can be @a NULL.
 * @param[in]  fmt   Format string according to subsequent parameters.
 * @param[in]  ...   Variable argument list according to the @p fmt format string.
 *
 * @see ::mbg_log
 */
__attribute__( ( format( printf, 3, 4 ) ) )
void mbg_log_dev( int lvl, const DEV_STATE *p_ds, const char *fmt, ... )
{
  char s[256];
  va_list args;
  int n = 0;

  if ( p_ds )
  {
    n += snprintf_safe( &s[n], sizeof( s ) - n, "%s",
                        _pcps_type_name( &p_ds->dev_info ) );

    if ( p_ds->shm_unit >= 0 )
      n += snprintf_safe( &s[n], sizeof( s ) - n, " (SHM unit %i)",
                          p_ds->shm_unit );

    n += snprintf_safe( &s[n], sizeof( s ) - n, ": " );
  }

  va_start( args, fmt );
  vsnprintf_safe( &s[n], sizeof( s ) - n, fmt, args );
  va_end( args );

  do_log( lvl, s, getppid() != 1 );

}  // mbg_log_dev



static /*HDR*/
const char *get_plural_str( int val )
{
  return ( val == 1 ) ? str_empty : "s";

}  // get_plural_str



static /*HDR*/
/**
 * @brief Our own signal handler, see @ref MBG_SIG_HANDLER_FNC.
 *
 * @param[in]  sig  A signal number passed by the OS.
 */
void sig_handler( int sig )
{
  switch ( sig )
  {
    case SIGTERM:
    case SIGINT:
      exit_requested = 1;
      break;

  }  // switch

  #if defined( DEBUG )
    mbg_log( LOG_INFO, "Caught signal %i in process %i", sig, getpid() );
  #endif

}  // sig_handler

MBG_SIG_HANDLER_FNC sig_handler;



static /*HDR*/
/**
 * @brief Register our own signal handler(s), see @ref MBG_REG_SIG_HANDLER_FNC.
 */
void register_sig_handlers( void )
{
  #if defined( DEBUG )
    mbg_log( LOG_INFO, "Registering signal handlers in process %i", getpid() );
  #endif

  signal( SIGTERM, sig_handler );
  signal( SIGINT, sig_handler );

}  // register_sig_handlers

MBG_REG_SIG_HANDLER_FNC register_sig_handlers;



static /*HDR*/
/**
 * @brief Check if a device can be considered synchronized.
 *
 * @param[in]  p_status  Pointer to device status flags, see ::PCPS_TIME_STATUS_X.
 *
 * @return @a true if device is synchronized, else @a false.
 */
bool check_if_sync( PCPS_TIME_STATUS_X *p_status )
{
  bool synced = true;

  if ( *p_status & PCPS_FREER )
    synced = false;

  if ( ( *p_status & PCPS_SYNCD ) == 0 )
    synced = false;

  if ( ( *p_status & PCPS_INVT ) )
    synced = false;

  // TODO Check more info, e.g. PCPS_ANT_FAIL.

  return synced;

}  // check_if_sync



static /*HDR*/
/**
 * @brief Update a SHM unit.
 *
 * @param[in]   p_ds  Pointer to device/status information.
 */
void update_shm( DEV_STATE *p_ds )
{
  MBG_CHK_TIME_INFO *p_cti = &p_ds->cti;
  MBG_SYS_TIME_CYCLES *p_sys_tic = &p_cti->hrti.sys_time_cycles;
  struct shmTime *p_shm = shmTime[p_ds->shm_unit];

  // Fill SHM structure.
  p_shm->count++;
  p_shm->clockTimeStampSec = (time_t) p_cti->d_ref_comp;
  p_shm->clockTimeStampUSec = (int) ( ( p_cti->d_ref_comp - p_shm->clockTimeStampSec ) * 1e6 ); // Microseconds.
  p_shm->receiveTimeStampSec = (time_t) p_sys_tic->sys_time.secs;
  p_shm->receiveTimeStampUSec = (int) ( p_sys_tic->sys_time.nano_secs / 1000 );

  // These fields are not supported by very old versions of ntpd.
  p_shm->clockTimeStampNSec = (int) ( ( p_cti->d_ref_comp - p_shm->clockTimeStampSec ) * 1e9 ); // Nanoseconds.
  p_shm->receiveTimeStampNSec = (int) ( p_sys_tic->sys_time.nano_secs );

  p_shm->leap = p_ds->ntp_leap;
  p_shm->precision = p_ds->ntp_precision;
  p_shm->count++;
  p_shm->valid = 1;

}  // update_shm



static /*HDR*/
/**
 * @brief Check and update a SHM unit, if appropriate.
 *
 * @param[in]   p_ds  Pointer to device/status information.
 */
void chk_update_shm( DEV_STATE *p_ds )
{
  if ( p_ds->shm_unit < 0 )
    return;  // No SHM unit assigned, nothing to update.

  if ( !( p_ds->state & DEV_STATE_DATA_AVAIL ) )
    return;  // No data available.

  if ( !p_ds->exec_time_ok )
    return;  // Time stamp pair could not be read fast enough.

  if ( ( p_ds->ntp_leap != LEAP_NOTINSYNC ) || always_update_shm )
    update_shm( p_ds );

}  // chk_update_shm



static /*HDR*/
void dev_state_update_state( DEV_STATE *p_ds, DEV_STATE_FLAGS flag_mask, bool cond )
{
  if ( cond )
    p_ds->state |= flag_mask;
  else
    p_ds->state &= ~flag_mask;

  p_ds->state |= DEV_STATE_SET;

};  // dev_state_update_state



static /*HDR*/
void stop_holdover( DEV_STATE *p_ds )
{
  p_ds->trust_time_start = 0;
  p_ds->trust_time_end = 0;
  p_ds->state &= ~DEV_STATE_IN_HOLDOVER;

}  // stop_holdover



static /*HDR*/
bool check_holdover_expired( DEV_STATE *p_ds )
{
  struct timespec ts;
  bool b;

  clock_gettime( CLOCK_MONOTONIC, &ts );

  b = ( p_ds->state & DEV_STATE_IN_HOLDOVER ) &&
      ( ts.tv_sec >= p_ds->trust_time_end );

  if ( b )
    stop_holdover( p_ds );

  return b;

}  // check_holdover_expired



static /*HDR*/
void start_holdover( DEV_STATE *p_ds )
{
  struct timespec ts;

  clock_gettime( CLOCK_MONOTONIC, &ts );

  p_ds->trust_time_start = ts.tv_sec;
  p_ds->trust_time_end = ts.tv_sec + p_ds->trust_time_seconds;
  p_ds->state |= DEV_STATE_IN_HOLDOVER;

}  // start_holdover



static /*HDR*/
void dev_state_check_changes( DEV_STATE *p_ds )
{
  DEV_STATE_FLAGS changed_state = p_ds->state ^ p_ds->prv_state;

  #if DEBUG_DEV_STATE
    printf( "state %-9s: 0x%04X, changed: 0x%04X%s \n",
            _pcps_type_name( &p_ds->dev_info ),
            p_ds->state, changed_state,
            changed_state ? " *" : str_empty );
  #endif

  if ( changed_state & DEV_STATE_SET )
  {
    // DEV_STATE_SET is expected to change only at the first update
    // after startup, so we just report the initial state.
    // In any case, holdover doesn't make sense.
    stop_holdover( p_ds );

    if ( !( p_ds->state & DEV_STATE_DATA_AVAIL ) )
    {
      mbg_log_dev( LOG_WARNING, p_ds, "No data available" );
      goto done;
    }

    if ( p_ds->state & DEV_STATE_SYNCHRONIZED )
      mbg_log_dev( LOG_INFO, p_ds, "Is synchronized" );
    else
      if ( p_ds->state & DEV_STATE_INVALID_TIME )
        mbg_log_dev( LOG_WARNING, p_ds, "Has no valid time" );
      else
        mbg_log_dev( LOG_WARNING, p_ds, "Is not synchronized" );

    goto done;
  }


  if ( changed_state & DEV_STATE_DATA_AVAIL )
  {
    // Data may have become available or unavailable, e.g.
    // if an external device is connected or disconnected.
    // In any case, holdover doesn't make sense.
    stop_holdover( p_ds );

    if ( p_ds->state & DEV_STATE_DATA_AVAIL )
    {
      // Data started to become available, and the
      // device may be synchronized, or not.
      if ( p_ds->state & DEV_STATE_SYNCHRONIZED )
        mbg_log_dev( LOG_INFO, p_ds, "Data has become available, and device is synchronized" );
      else
        if ( p_ds->state & DEV_STATE_INVALID_TIME )
          mbg_log_dev( LOG_WARNING, p_ds, "Data has become available, but device has no valid time" );
        else
          mbg_log_dev( LOG_WARNING, p_ds, "Data has become available, but device is NOT synchronized" );

      goto done;
    }

    // No more data available, so we don't know if the device
    // is synchronized, or not.
    mbg_log_dev( LOG_WARNING, p_ds, "No more data available" );
    goto done;
  }


  if ( changed_state & DEV_STATE_SYNCHRONIZED )
  {
    if ( p_ds->state & DEV_STATE_SYNCHRONIZED )
    {
      // The device is now synchronized.
      mbg_log_dev( LOG_INFO, p_ds, ( p_ds->state & DEV_STATE_IN_HOLDOVER ) ?
                   "Is now synchronized, stopping holdover" :
                   "Is now synchronized" );

      stop_holdover( p_ds );
      goto done;
    }

    // The device has lost synchronization.
    //
    // Depending on configuration options, we still pretend
    // that the device is synchronized, or we enter holdover,
    // or we immediately propagate the "unsync" state.

    if ( p_ds->pretend_sync )
    {
      mbg_log_dev( LOG_WARNING, p_ds, "Sync. lost, but pretending synchronization" );
      stop_holdover( p_ds );
      goto done;
    }

    // If a trust time has been specified, we possibly start holdover mode.
    if ( p_ds->trust_time_seconds )
    {
      if ( p_ds->state & DEV_STATE_INVALID_TIME )
      {
        if ( p_ds->state & DEV_STATE_INVALID_TIME_REPORTED )
          goto done;

        mbg_log_dev( LOG_WARNING, p_ds, "Sync. lost, not entering holdover: device time is not valid" );
        dev_state_update_state( p_ds, DEV_STATE_INVALID_TIME_REPORTED, true );
        goto done;
      }

      mbg_log_dev( LOG_WARNING, p_ds, "Sync. lost, entering holdover for %lu second%s",
                   p_ds->trust_time_seconds, get_plural_str( p_ds->trust_time_seconds ) );
      start_holdover( p_ds );
      goto done;
    }

    mbg_log_dev( LOG_WARNING, p_ds, "Sync. lost, no holdover (trust time is 0)" );
    goto done;
  }


done:
  if ( p_ds->state & DEV_STATE_IN_HOLDOVER )
    if ( check_holdover_expired( p_ds ) )
      mbg_log_dev( LOG_WARNING, p_ds, "Sync. lost, holdover ends after trust time (%lu second%s) expired",
                   p_ds->trust_time_seconds, get_plural_str( p_ds->trust_time_seconds ) );

  if ( changed_state & DEV_STATE_INVALID_TIME )
    dev_state_update_state( p_ds, DEV_STATE_INVALID_TIME_REPORTED, false );

  p_ds->prv_state = p_ds->state;

}  // dev_state_check_changes



static /*HDR*/
/**
 * @brief Evaluate device status changes.
 *
 * Should be called after ::mbg_chk_time_info has been called.
 *
 * @param[in]   p_ds  Pointer to device/status information.
 */
void eval_status( DEV_STATE *p_ds )
{
  PCPS_TIME_STATUS_X *p_status = &p_ds->cti.hrti.ref_hr_time_cycles.t.status;

  // Only if the device is accessible, we check
  // if it is synchronized.
  if ( p_ds->state & DEV_STATE_DATA_AVAIL )
  {
    dev_state_update_state( p_ds, DEV_STATE_SYNCHRONIZED, check_if_sync( p_status ) );
    dev_state_update_state( p_ds, DEV_STATE_INVALID_TIME, ( *p_status & PCPS_INVT ) != 0 );
  }

  // Check for status changes.
  dev_state_check_changes( p_ds );

  // Set up the leap bits for NTP.
  if ( ( p_ds->state & DEV_STATE_DATA_AVAIL ) &&
       ( ( p_ds->state & DEV_STATE_SYNCHRONIZED ) ||
         ( p_ds->state & DEV_STATE_IN_HOLDOVER ) ||
           p_ds->pretend_sync ) )
  {
    if ( *p_status & PCPS_LS_ANN_NEG )
      p_ds->ntp_leap = LEAP_DELSECOND;
    else
      if ( *p_status & PCPS_LS_ANN )
        p_ds->ntp_leap = LEAP_ADDSECOND;
      else
        p_ds->ntp_leap = LEAP_NOWARNING;
  }
  else
    p_ds->ntp_leap = LEAP_NOTINSYNC;

  chk_update_shm( p_ds );

}  // eval_status



static /*HDR*/
void clear_dev_state( DEV_STATE *p_ds )
{
  memset( p_ds, 0, sizeof( *p_ds ) );

}  // clear_dev_state



static /*HDR*/
int get_shm_unit( int shm_dev_num )
{
  return query_only ? -1 : shm_dev_num;

}  // get_shm_unit



static /*HDR*/
void setup_dev_state( DEV_STATE *p_ds, int dev_idx, int shm_unit, int ntp_precision )
{
  p_ds->dev_idx = dev_idx;
  p_ds->pretend_sync = glb_pretend_sync;
  p_ds->trust_time_seconds = glb_trust_time_seconds;  // TODO Or device/oscillator-specific?

  p_ds->ntp_precision = ntp_precision;
  p_ds->ntp_leap = LEAP_NOTINSYNC;

  p_ds->shm_unit = shm_unit;

}  // setup_dev_state



#if SUPP_SERIAL

static /*HDR*/
int tstr_rcv_callback( MBG_TSTR_RCV_INFO *p_tsri, int rc )
{
  DEV_STATE *p_ds = (DEV_STATE *) p_tsri->ext_data;

  dev_state_update_state( p_ds, DEV_STATE_DATA_AVAIL, mbg_rc_is_success( rc ) );

  if ( p_ds->state & DEV_STATE_DATA_AVAIL )
  {
    MBG_CHK_TIME_INFO *p_cti = &p_ds->cti;

    #if SUPP_PPS
    {
      PPS_DEV_INFO *p_pps_di = &p_ds->p_ser_di->pps_dev_info;

      // Fetch a PPS time stamp info structure from the kernel.
      mbg_pps_fetch_ts( &p_tsri->ts_pps, p_pps_di );
    }
    #endif

    // Convert the data derived from a timestring
    // to the same format as used with bus-level devices.
    mbg_rcv_info_to_check_time_info( p_cti, p_tsri );

    // Do some common processing.
    mbg_proc_chk_time_info( p_cti, NULL );

    p_ds->exec_time_ok = true;

    #if defined( DEBUG )
    {
      char s[256];

      snprint_time_str( s, sizeof( s ), p_tsri->buffer );
      mbg_log( LOG_INFO, "%s", s );
    }
    #endif
  }

  eval_status( p_ds );

  return rc;

}  // tstr_rcv_callback



static /*HDR*/
/**
 * @brief The serial read thread function.
 *
 * @param[in]  p_void  Generic pointer to a thread function argument,
 *                     in this case the address of the associated
 *                     ::DEV_STATE structure.
 *
 * @return  A thread-function specific return value, usually representing
 *          ::MBG_SUCCESS or one of the @ref MBG_ERROR_CODES.
 */
MBG_THREAD_FNC_RET_VAL MBG_THREAD_FNC_ATTR do_serial( void *p_void )
{
  DEV_STATE *p_ds = (DEV_STATE *) p_void;
  MBGSERIO_DEV *sdev = p_ds->p_ser_di->sdev;

  #if defined( DEBUG )
  {
    ulong prv_poll_timeout = mbgserio_set_dev_poll_timeout( sdev, POLL_TIMEOUT );
    mbg_log( LOG_INFO, "Changed serial poll timeout from %lu to %u.",
             prv_poll_timeout, POLL_TIMEOUT );
  }
  #else
    mbgserio_set_dev_poll_timeout( sdev, POLL_TIMEOUT );
  #endif

  for (;;)
  {
    char b[1];
    int i;

    p_ds->rc = mbgserio_read_wait( sdev, b, sizeof( b ) );

    if ( exit_requested )
    {
      #if defined( DEBUG )
        mbg_log( LOG_INFO, "Serial read thread is going to terminate." );
      #endif

      p_ds->rc = MBG_SUCCESS;
      break;
    }

    if ( mbg_rc_is_success( p_ds->rc ) )
    {
      for( i = 0; i < p_ds->rc; i++ )
        mbg_tstr_receive( b[i], &p_ds->tstr_rcv_info );

      continue;
    }

    // Error occurred ... but timeouts are not fatal.
    if ( p_ds->rc == MBG_ERR_TIMEOUT )
    {
      #if defined( DEBUG )
        if ( p_ds->state & DEV_STATE_DATA_AVAIL )
          mbg_log( LOG_INFO, "Serial read timeout." );
      #endif

      tstr_rcv_callback( &p_ds->tstr_rcv_info, p_ds->rc );
      continue;
    }

    // Fatal error. TODO Should we stop and restart the thread?
    mbg_log( LOG_ERR, "Serial read failed: %s.", mbg_strerror( p_ds->rc ) );
    break;
  }

  _mbg_thread_exit( (intptr_t) p_ds->rc );

}  // do_serial

MBG_THREAD_FNC do_serial;



static /*HDR*/
/**
 * @brief Prepare device status information for serial devices.
 */
void prepare_serial_devices( void )
{
  int i = 0;

  for ( i = 0; i < n_dev_serial; i++ )
  {
    DEV_STATE *p_ds;
    MBG_TSTR_RCV_INFO *p_tsri;
    SER_DEV_INFO *p_ser_di;
    int shm_unit;
    int ntp_precision;
    int rc;

    if ( n_dev_shm >= n_units )
      break;  // No more free SHM units.

    // Try to open one of the configured serial devices.
    p_ser_di = &ser_dev_info[i];

    rc = mbgserio_open( &p_ser_di->sdev, p_ser_di->dev_name );

    if ( mbg_rc_is_error( rc ) )
    {
      mbg_log( LOG_WARNING, "Failed to open serial port %s: %s",
               p_ser_di->dev_name, mbg_strerror( rc ) );
      continue;
    }


    // Serial device could be opened successfully.
    // Try to set the serial port parameters.
    rc = mbgserio_set_parms( p_ser_di->sdev, p_ser_di->cfgd_baud_rate, p_ser_di->cfgd_framing );

    if ( mbg_rc_is_error( rc ) )
    {
      mbg_log( LOG_WARNING, "Failed to configure serial port %s: %s",
               p_ser_di->dev_name, mbg_strerror( rc ) );
      mbgserio_close( &p_ser_di->sdev );
      continue;
    }


    shm_unit = get_shm_unit( n_dev_shm );

    #if defined( DEBUG )
      if ( shm_unit >= 0 )
        mbg_log( LOG_INFO, "Using %s with %lu/%s for SHM unit %i.",
                 p_ser_di->dev_name, (ulong) p_ser_di->cfgd_baud_rate, p_ser_di->cfgd_framing, shm_unit );
      else
        mbg_log( LOG_INFO, "Using %s with %lu/%s, not updating SHM.",
                 p_ser_di->dev_name, (ulong) p_ser_di->cfgd_baud_rate, p_ser_di->cfgd_framing );

      mbg_log( LOG_INFO, "RX threshold: %i bytes, delay: %li ns",
               p_ser_di->sdev->rcv_fifo_threshold, (long) p_tsri->rcv_delay_ns );
    #endif


    // If PPS is supported, check if a PPS source
    // is or can be attached.
    #if SUPP_PPS
      rc = mbg_pps_setup_pps_for_serial( &p_ser_di->pps_dev_info, p_ser_di->dev_name,
                                         p_ser_di->sdev, p_ser_di->pps_mode );

      #if DEBUG_PPS > 1
        mbg_log( LOG_INFO, "mbg_pps_setup_pps_for_serial returned %i for %s",
                 rc, p_ser_di->dev_name );
      #else
        (void) rc;  // Avoid compiler warning like "never used".
      #endif

      // TODO Log a warning if no PPS source is assigned.
      // If this fails, we continue anyway, but we may only
      // yield limited accuracy.
    #endif


    // Now set up a device state info structure.
    p_ds = &devs[n_dev_shm];
    memset( p_ds, 0, sizeof( *p_ds ) );

    p_ds->p_ser_di = p_ser_di;

    {
      // Set up a short name for the serial port.
      char *dup_name = NULL;
      const char *cp = mbgserio_get_port_basename( &dup_name, p_ser_di->dev_name );
      sn_cpy_str_safe( p_ds->dev_info.type.name, sizeof( p_ds->dev_info.type.name ), cp );
      mbgserio_free_port_basename( &dup_name );
    }


    ntp_precision = -10;  // FIXME TODO This is preliminary.

    // TODO Should we pass dev_idx instead of shm_idx ?
    setup_dev_state( p_ds, n_dev_shm, shm_unit, ntp_precision );


    // Set up the time string receiver stuff.
    p_tsri = &p_ds->tstr_rcv_info;

    // Setup the receive buffer for a specific time string.
    rc = mbg_tstr_setup_tstr_buffer( p_tsri, p_ser_di->tstr_idx, 100 );

    if ( mbg_rc_is_error( rc ) )
    {
      mbg_log( LOG_ERR, "Failed to set up time string receiver for %s: %s",
               p_ser_di->dev_name, mbg_strerror( rc ) );
      continue;
    }


    // Setup the expected receive delay.
    mbg_tstr_setup_rcv_delay( p_tsri, p_ser_di->sdev, adj_bits );


    // Register our own function to be called whenever
    // a complete time string has been received.
    mbg_tstr_set_rcv_callback( p_tsri, tstr_rcv_callback );

    #if defined( DEBUG )
      // If not NULL, unexpectedly received characters
      // will be dumped to the console.
      p_tsri->info = "Debug";
    #endif

    p_tsri->log_fn = NULL;  // FIXME TODO log_fn;


    // Let the extended data field of the time string receive info
    // point back to the device state, so we can use it our
    // callback function.
    p_tsri->ext_data = (void *) p_ds;


    // Start the receive thread for this serial port.
    p_ds->rc = mbg_thread_create( &p_ds->ti, do_serial, p_ds );

    n_dev_shm++;
  }

}  // prepare_serial_devices

#endif  // SUPP_SERIAL



static /*HDR*/
/**
 * @brief Prepare device status information for bus level devices.
 */
void prepare_bus_level_devices( void )
{
  int i = 0;

  n_dev_bus = mbg_find_devices();

  for ( i = 0; i < n_dev_bus; i++ )
  {
    DEV_STATE *p_ds;
    int ntp_precision;

    if ( n_dev_shm >= n_units )
      break;  // No more free SHM units.


    p_ds = &devs[n_dev_shm];

    // Initialize data set for a new SHM unit.
    clear_dev_state( p_ds );

    p_ds->dh = mbg_open_device( i );

    p_ds->rc = mbg_get_device_info( p_ds->dh, &p_ds->dev_info );

    if ( mbg_rc_is_error( p_ds->rc ) )
    {
      mbg_log( LOG_WARNING, "Failed to read device info from device #%i: %s",
               i, mbg_strerror( p_ds->rc ) );
      mbg_close_device( &p_ds->dh );
      continue;
    }


    // Check if the device supports HR time.
    p_ds->rc = mbg_chk_dev_has_hr_time( p_ds->dh );

    if ( mbg_rc_is_error( p_ds->rc ) )
    {
      mbg_log( LOG_WARNING, "Device %s does not support HR time stamps.",
               _pcps_type_name( &p_ds->dev_info ) );
      mbg_close_device( &p_ds->dh );
      continue;
    }


    // Determine precision depending on the device type.
    if ( mbg_rc_is_success( mbg_chk_dev_is_lwr( p_ds->dh ) ) )
    {
      // Any kind of long wave receiver.
      // TODO May be better if PZF is supported and used.
      ntp_precision = -8;
    }
    else
    {
      if ( mbg_rc_is_success( mbg_chk_dev_is_tcr( p_ds->dh ) ) )
      {
        if ( _pcps_is_usb( &p_ds->dev_info ) )
          ntp_precision = -10;  // IRIG receiver connected via USB.
        else
          ntp_precision = -18;  // IRIG receiver connected via PCI.
      }
      else
        ntp_precision = -20;    // GPS/GNSS receivers, and PTP clocks.
    }

    // TODO Should pass dev_idx instead of shm_idx.
    setup_dev_state( p_ds, n_dev_shm, get_shm_unit( n_dev_shm ), ntp_precision );

    #if defined( DEBUG )
      if ( p_ds->shm_unit >= 0 )
        mbg_log( LOG_INFO, "Using %s %s for SHM unit %i.",
                 _pcps_type_name( &p_ds->dev_info ), _pcps_sernum( &p_ds->dev_info ), p_ds->shm_unit );
      else
        mbg_log( LOG_INFO, "Using %s %s, not updating SHM.",
                 _pcps_type_name( &p_ds->dev_info ), _pcps_sernum( &p_ds->dev_info ) );
    #endif

    n_dev_shm++;
  }

}  // prepare_bus_level_devices



static /*HDR*/
/**
 * @brief Check if a device is a bus level device.
 *
 * @param[in]  p_ds  Pointer to device/status information.
 *
 * @return @a true if device is bus-level, else @a false.
 */
bool is_bus_level_device( const DEV_STATE *p_ds )
{
  return mbg_rc_is_success( mbg_chk_dev_is_syn1588_type( p_ds->dh ) )
         || p_ds->dev_info.type.dev_id != 0;

}  // is_bus_level_device



static /*HDR*/
/**
 * @brief Try to determine the cycles counter frequency.
 */
void determine_cycles_counter_frequency( void )
{
  int i;

  for ( i = 0; i < n_dev_shm; i++ )
  {
    DEV_STATE *p_ds = &devs[i];
    int rc;

    if ( !is_bus_level_device( p_ds ) )
      continue;  // Not a bus level device.

    // A bus level device has been found, so we can retrieve the
    // cycles counter frequency from the kernel driver.
    rc = mbg_get_default_cycles_frequency_from_dev( p_ds->dh, &mbg_pc_cycles_frequency );

    if ( mbg_cond_err_msg( rc, "mbg_get_default_cycles_frequency_from_dev" ) )
      continue;

    // If the API call succeeded but the cycles counter frequency
    // is anyway 0, we may be running on some uncommon hardware platform,
    // e.g. Intel IA64.
    mbg_log( ( mbg_pc_cycles_frequency == 0 ) ? LOG_WARNING : LOG_INFO,
             "%sPC cycles counter frequency: %Lu Hz",
             ( mbg_pc_cycles_frequency == 0 ) ? "*** Warning: " : "",
             (unsigned long long) mbg_pc_cycles_frequency );
    break;
  }

  if ( i >= n_dev_shm )
  {
    // If no bus level device is available, it's normal that we are
    // unable to determine the cycles counter frequency, and we can
    // just optionally give a hint, e.g. in DEBUG builds.
    #if defined( DEBUG )
      mbg_log( LOG_INFO, "PC cycles counter frequency is unknown." );
    #endif
  }

}  // determine_cycles_counter_frequency



static /*HDR*/
/**
 * @brief Initialize device/status information.
 *
 * Should be called before ::mbg_chk_time_info is called.
 *
 * @param[in]  p_ds  Pointer to device/status information.
 */
void init_svc_dev_state( DEV_STATE *p_ds )
{
  p_ds->ntp_leap = LEAP_NOTINSYNC;

}  // init_svc_dev_state



static /*HDR*/
/**
 * @brief Show time comparison results.
 */
void show_results( void )
{
  char s[256];
  int i;

  for ( i = 0; i < n_dev_shm; i++ )
  {
    DEV_STATE *p_ds = &devs[i];
    bool append_marker = false;
    int n;

    if ( !( p_ds->state & DEV_STATE_SET ) )
      continue;

    if ( p_ds->state & DEV_STATE_DATA_AVAIL )  // The normal case.
    {
      PCPS_TIME_STATUS_X *p_status = &p_ds->cti.hrti.ref_hr_time_cycles.t.status;

      n = snprint_chk_time_info( s, sizeof( s ), &p_ds->cti, &p_ds->dev_info,
                                 frac_digits, print_raw );
      n += snprintf_safe( &s[n], sizeof( s ) - n, ", st: 0x%04X", *p_status );
    }
    else  // No data available.
    {
      n = snprint_chk_time_dev_name( s, sizeof( s ), _pcps_type_name( &p_ds->dev_info ) );
      n += sn_cpy_str_safe( &s[n], sizeof( s ) - n, "no data available" );
      append_marker = true;
    }

    n += snprintf_safe( &s[n], sizeof( s ) - n, ", leap: 0x%02X", p_ds->ntp_leap );

    if ( p_ds->retries )
    {
      n += snprintf_safe( &s[n], sizeof( s ) - n, ", retries: %i", p_ds->retries );
      append_marker = true;
    }

    if ( ( p_ds->state & DEV_STATE_DATA_AVAIL ) && !p_ds->exec_time_ok )
    {
      n += sn_cpy_str_safe( &s[n], sizeof( s ) - n, " *" );
      append_marker = true;
    }

    if ( append_marker )
      n += sn_cpy_str_safe( &s[n], sizeof( s ) - n, " <<" );

    do_log( LOG_INFO, s, 0 );
  }

  if ( n_dev_shm == 2 )
  {
    static bool min_max_initialized;

    if ( ( devs[0].state & DEV_STATE_DATA_AVAIL ) &&
         ( devs[1].state & DEV_STATE_DATA_AVAIL ) )
    {
      // Show the difference between the differences.

      static double max_delta;
      static double min_delta;

      double d0 = devs[0].cti.d_ref_comp - devs[0].cti.d_sys;
      double d1 = devs[1].cti.d_ref_comp - devs[1].cti.d_sys;
      double d = d1 - d0;

      if ( d > max_delta || !min_max_initialized )
        max_delta = d;

      if ( d < min_delta || !min_max_initialized  )
        min_delta = d;

      snprintf_safe( s, sizeof( s ), "%*s delta: %*.6f   (%+.1f ... %+.1f us)\n", 65, "",
                     17, d, min_delta * 1e6, max_delta * 1e6 );
      do_log( LOG_INFO, s, 0 );

      min_max_initialized = true;
    }
    else
      min_max_initialized = false;
  }

}  // show_results



static /*HDR*/
/**
 * @brief The main worker function for bus level devices.
 */
int do_mbgsvctasks( void )
{
  int rc = 0;
  int i;

  n_dev_shm = 0;

  #if SUPP_SERIAL
    if ( serial_first )
      prepare_serial_devices();
  #endif

  prepare_bus_level_devices();

  #if SUPP_SERIAL
    if ( !serial_first )
      prepare_serial_devices();
  #endif

  if ( n_dev_shm == 0 )
  {
    mbg_log( LOG_WARNING, "No usable device found!" );
    goto done;
  }

  determine_cycles_counter_frequency();

  mbg_log( LOG_INFO, "Found %d device%s usable for the NTP daemon.",
           n_dev_shm, ( n_dev_shm == 1 ) ? "" : "s" );

  // If in query-only mode, just print a corresponding message,
  // else initialize the NTP shared memory segments.
  if ( query_only )
    mbg_log( LOG_INFO, "Query-only mode, not updating SHM." );
  else
    ntpshm_init( shmTime, n_dev_shm, n_unit0 );


  for (;;)
  {
    for ( i = 0; i < n_dev_shm; i++ )
    {
      DEV_STATE *p_ds = &devs[i];
      MBG_CHK_TIME_INFO *p_cti;

      if ( !is_bus_level_device( p_ds ) )
        continue;  // Not a bus level device.

      p_cti = &p_ds->cti;

      init_svc_dev_state( p_ds );

      for ( p_ds->retries = 0; p_ds->retries < 3; p_ds->retries++ )
      {
        p_ds->rc = mbg_chk_time_info( p_ds->dh, p_cti, &p_ds->filter, 0 );

        dev_state_update_state( p_ds, DEV_STATE_DATA_AVAIL, !mbg_cond_err_msg( p_ds->rc, "mbg_chk_time_info" ) );

        if ( !( p_ds->state & DEV_STATE_DATA_AVAIL ) )
          continue;

        p_ds->exec_time_ok = p_cti->exec_cyc <= p_cti->exec_cyc_limit;

        if ( p_ds->exec_time_ok )
          break;

        usleep( 100 );
      }

      eval_status( p_ds );
    }

    if ( !quiet && ( getppid() != 1 ) )
      show_results();

    if ( exit_requested )
      break;

    if ( sleep_intv )
      sleep( sleep_intv );
  }

done:
  for ( i = 0; i < n_dev_shm; i++ )
  {
    DEV_STATE *p_ds = &devs[i];

    if ( is_bus_level_device( p_ds ) )
      mbg_close_device( &p_ds->dh );
    #if SUPP_SERIAL
      else
      {
        #if defined( DEBUG )
          mbg_log( LOG_INFO, "Stopping serial thread." );
        #endif
        mbg_thread_stop( &p_ds->ti );
      }
    #endif
  }

  if ( getppid() == 1 )  // Running as daemon.
    mbg_log( LOG_WARNING, "Shutting down." );

  return rc;

}  // do_mbgsvctasks



static /*HDR*/
/**
 * @brief Print usage information.
 */
void usage( void )
{
  mbg_print_usage_intro( pname, false,
    "This program periodically reads a reference time stamp and an associated\n"
    "system time stamp from every mbgclock device, and feeds the time stamp pairs\n"
    "to the NTP daemon's shared memory refclock driver.\n"
    "It usually runs as daemon but can also be run in the foreground to monitor the\n"
    "time stamps and offsets.\n"
    "This works only for devices that support high resolution time stamps."
  );
  mbg_print_help_options();
  mbg_print_opt_info( "-f", "Run program in foreground" );
  mbg_print_opt_info( "-Q", "Query only, don't update SHM segment(s)" );
  mbg_print_opt_info( "-q", "Quiet, don't print time differences on stdout" );
  mbg_print_opt_info( "-r", "Print raw time stamps when printing on stdout" );
  mbg_print_opt_info( "-s num", "Sleep num seconds between calls" );
  mbg_print_opt_info( "-p", "Pretend device is always synchronized" );
  mbg_print_opt_info( "-U", "Even update SHM unit if NTP leap bits are 'not synchronized'" );
  mbg_print_opt_info( "-t num", "Set refclock trust time to num seconds, default %lu", glb_trust_time_seconds );
  mbg_print_opt_info( "-n num", "Number of SHM segments to use" );
  mbg_print_opt_info( "-o num", "SHM segment number offset (default 0)" );

  #if SUPP_SERIAL
  {
    int i;

    puts( "" );
    puts( "  and specifically for serial devices:" );
    mbg_print_opt_info( "-S", "Register serial sources first, else bus-level devices first." );
    mbg_print_opt_info( "-a num", "Number of additional serial bits to compensate (default 0)" );
    mbg_print_opt_info( "-F idx", "Index of the time string format (0..%i, default 0), i.e.:", n_tstr_fmt - 1);

    for ( i = 0; tstr_spec_table[i]; i++ )
      printf( "%*i  %s\n", 17, i, tstr_spec_table[i]->name );
  }
  #endif

  #if SUPP_PPS
    puts( "" );
    puts( "  and specifically for PPS support:" );
    mbg_print_opt_info( "-A", "Capture the PPS \"Assert\" slope (default)." );
    mbg_print_opt_info( "-C", "Capture the PPS \"Clear\" slope." );
  #endif

  // TODO Should be or'ed with DEV_OPT_PRINT_BUS_LEVEL if
  // specification of bus-level devices on the command line
  // is also supported.
  mbg_print_usage_outro( DEV_OPT_PRINT_SERIAL, true );

}  // usage



int main( int argc, char *argv[] )
{
  char ws[256];
  int rc;
  int c;
  int i;

  // Redirect the logging of some modules to our own log function.
  pidfile_log_fnc = mbg_log;
  mbg_daemonize_log_fnc = mbg_log;
  ntp_shm_log_fnc = mbg_log;
  #if SUPP_PPS
    mbg_pps_log_fnc = mbg_log;
  #endif

  #if SUPP_SERIAL
    #define SER_OPTS "Sa:F:"
    n_tstr_fmt = mbg_tstr_get_fmt_cnt();
  #else
    #define SER_OPTS ""
  #endif

  #if SUPP_SERIAL
    #define PPS_OPTS "AC"
  #else
    #define PPS_OPTS ""
  #endif

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "fpqQrs:t:Un:o:" SER_OPTS PPS_OPTS "h?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'f':
        foreground = true;
        break;

      case 'p':
        glb_pretend_sync = true;
        break;

      case 'q':
        quiet = true;
        break;

      case 'Q':
        query_only = true;
        foreground = true;  // Always stay in foreqround if querying only.
        break;

      case 'r':
        print_raw = true;
        break;

      case 's':
        sleep_intv = atoi( optarg );
        break;

      case 't':
      {
        long tt = atol( optarg );

        if ( tt > 0 )
          glb_trust_time_seconds = tt;

        break;
      }

      case 'U':
        always_update_shm = true;
        break;

      case 'n':
        n_units = atoi( optarg );
        break;

      case 'o':
        n_unit0 = atoi( optarg );
        break;

      #if SUPP_SERIAL
        case 'S':
          serial_first = true;
          break;

        case 'a':
          adj_bits = atoi( optarg );
          break;

        case 'F':
          ts_format_idx = atoi( optarg );
          break;
      #endif

      #if SUPP_PPS
        case 'A':
          default_pps_mode = PPS_CAPTUREASSERT;
          break;

        case 'C':
          default_pps_mode = PPS_CAPTURECLEAR;
          break;
      #endif

      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }


  // If the output has been redirected, make stdout unbuffered,
  // e.g. to see the output immediately even though piped through 'tee'.
  if ( !isatty( fileno( stdout ) ) )
    setvbuf( stdout, NULL, _IONBF, 0 );


  mbg_program_info_str( ws, sizeof( ws ), pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  if ( foreground || must_print_usage )
    printf( "\n%s\n\n", ws );
  else
    mbg_log( LOG_INFO, "%s", ws );


  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }

  #if 1 && defined( DEBUG )  // TODO
    mbg_log( LOG_INFO, "pid: %i, ppid: %i", getpid(), getppid() );
  #endif


  // Check some parameters whether they are in a valid range.
  if ( n_units < 0 || n_units > MAX_SHM_REFCLOCKS )
  {
    mbg_log( LOG_WARNING, "Configured number of SHM units %i out of range, truncating to %i",
            n_units, MAX_SHM_REFCLOCKS );
    n_units = MAX_SHM_REFCLOCKS;
  }

  if ( n_unit0 < 0 || n_unit0 >= MAX_SHM_UNIT_OFFSET )
  {
    mbg_log( LOG_WARNING, "Configured SHM unit offset %i out of range %i to %i, truncating to %i",
            n_unit0, 0, MAX_SHM_UNIT_OFFSET, 0 );
    n_unit0 = 0;
  }

  #if SUPP_SERIAL
    if ( ts_format_idx >= n_tstr_fmt )
    {
      mbg_log( LOG_WARNING, "Configured time string format index %i out of range %i to %i, setting to %i",
               ts_format_idx, 0, n_tstr_fmt - 1, 0 );
      ts_format_idx = 0;
    }
  #endif


  for ( i = optind; i < argc; i++ )
  {
    char *dev_name = argv[i];

    #if SUPP_SERIAL
      // Set up the specifications of the serial
      // device(s) to use.
      // TODO Pass more details via the command line.
      if ( device_id_is_serial( dev_name ) )
      {
        if ( n_dev_serial < MAX_DEV_SERIAL )
        {
          SER_DEV_INFO *p_ser_di = &ser_dev_info[n_dev_serial];
          p_ser_di->dev_name = dev_name;

          // First set default settings.
          p_ser_di->cfgd_baud_rate = 19200;
          p_ser_di->cfgd_framing = "8N1";

          #if SUPP_PPS
            p_ser_di->pps_mode = default_pps_mode;
          #endif

          // Now setings can be overridden, if required.
          p_ser_di->tstr_idx = ts_format_idx;

          // Count devices.
          n_dev_serial++;
        }
      }
    #endif  // SUPP_SERIAL

    (void) dev_name;  // Avoid warning "never used".
  }


  // Unless we query only and don't update the SHM segments, we use a PID file
  // to avoid having several instances of this program running and updating
  // the SHM segments concurrently. Such instances could either run as daemon,
  // or as a foreground process, so we check for and create / lock a PID file
  // *before* we are forking.
  if ( !query_only )
  {
    // Check for or create a PID file.
    if ( ( rc = mbg_pidfile_open_and_lock( pidfile_name ) ) < 0 )
    {
      mbg_log( LOG_WARNING, "Another instance seems to be running, exiting ..." );
      exit( EXIT_FAILURE );
    }
  }


  if ( !foreground )
  {
    // We are going to fork, and we pass the function that registers
    // our signal handlers to mbg_daemonize(), which will appropriately
    // call it after the fork. If we didn't pass that function,
    // mbg_daemonize() would register its own default signal handler.
    mbg_daemonize( NULL, register_sig_handlers );
  }
  else
  {
    // If we don't fork, we register our signal handlers directly.
    register_sig_handlers();
  }


  // Unless we query only, in which case we don't use the PID file,
  // we register an exit function that removes the PID file when we exit,
  // and we write our own PID to the PID file.
  if ( !query_only )
  {
    mbg_pidfile_register_cleanup();

    // When we write our PID here, it can be the original PID
    // of the process that has not forked, or the PID of the
    // forked process (the child) if mbg_daemonize() has been
    // called before.
    if ( mbg_pidfile_write_own_pid() < 0 )
      exit( EXIT_FAILURE );
  }

  if ( glb_trust_time_seconds )
    mbg_log( LOG_INFO, "Refclock trust time: %lu seconds", glb_trust_time_seconds );

  rc = do_mbgsvctasks();

  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
