
/**************************************************************************
 *
 *  $Id: mbgfasttstamp.c 1.14 2022/12/21 15:13:13 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for mbgfasttstamp program which demonstrates how to access
 *    a Meinberg device via memory mapped I/O, if supported by the device.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgfasttstamp.c $
 *  Revision 1.14  2022/12/21 15:13:13  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from a function.
 *  Revision 1.13  2021/11/15 17:08:52  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.12  2021/04/12 21:58:08  martin
 *  Updated printing of usage information.
 *  Revision 1.11  2021/03/21 17:53:49  martin
 *  Updated some comments.
 *  Revision 1.10  2021/03/12 11:50:14  martin
 *  Corrected the wording of some comments.
 *  Revision 1.9  2020/11/04 17:16:24  martin
 *  Added option -C to support checking the continuity of the timestamps
 *  and status sequentially read from a device.
 *  Revision 1.8  2018/11/15 12:12:32  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.7  2017/07/05 18:55:22  martin
 *  New way to maintain version information.
 *  Support build on Windows.
 *  Abort if MM access not supported on the target OS.
 *  Support signal handler to catch CTRL-C.
 *  Added options to sleep between calls.
 *  Parameters -u and -s imply -c.
 *  Use more functions from common library modules.
 *  Use codes and inline functions from mbgerror.h.
 *  Proper return codes and exit codes.
 *  Revision 1.6  2010/05/21 12:54:33  martin
 *  Print warning if no cycles supported on the target platform
 *  and thus latencies can not be computed.
 *  Revision 1.5  2009/09/29 15:02:14  martin
 *  Updated version number to 3.4.0.
 *  Revision 1.4  2009/07/24 09:50:08  martin
 *  Updated version number to 3.3.0.
 *  Revision 1.3  2009/06/19 14:01:32  martin
 *  Made print_hr_timestamp() to toolutil.c as mbg_print_hr_timestamp().
 *  Updated version number to 3.2.0.
 *  Revision 1.2  2009/03/20 11:48:19  martin
 *  Updated version number to 3.1.0.
 *  Updated copyright year to include 2009.
 *  New parameter -r lets the program read raw timestamps
 *  using the new API call mbg_get_fast_hr_timestamp().
 *  Revision 1.1  2008/12/22 11:45:01  martin
 *  Initial revision.
 *  Revision 1.1  2008/12/22 11:05:24  martin
 *  Initial revision.
 *
 **************************************************************************/

// Include Meinberg headers.
#include <mbgdevio.h>
#include <toolutil.h>       // Common utility functions.
#include <cmp_time_util.h>

// Include system headers.
#include <stdio.h>
#include <stdlib.h>


#define MBG_FIRST_COPYRIGHT_YEAR   2007
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.

static const char *pname = "mbgfasttstamp";


#define MAX_TS_BURST  1000

static int loops;
static int burst_read;
static int read_raw;
static int must_check_continuity;
static double max_allowed_delta;
static long sleep_secs;
static long sleep_usecs;



static /*HDR*/
/**
 * @brief Read and immediately print a number of timestamps.
 *
 * This doesn't read from thedevice as fast as possible
 * because printing the timestamp takes some time to execute.
 * However, this can run continuously forever.
 *
 * @param[in]  dh   Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int show_fast_hr_timestamp( MBG_DEV_HANDLE dh )
{
  int this_loops = loops;
  PCPS_TIME_STAMP ts = { 0 };
  PCPS_TIME_STAMP prv_ts = { 0 };
  int prv_ts_avail = 0;
  int32_t hns_latency = 0;

  for (;;)
  {
    int rc = read_raw ? mbg_get_fast_hr_timestamp( dh, &ts ) :
             mbg_get_fast_hr_timestamp_comp( dh, &ts, &hns_latency );

    if ( mbg_cond_err_msg( rc, "mbg_get_fast_hr_timestamp..." ) )
      return rc;

    mbg_print_hr_timestamp( &ts, hns_latency, NULL, read_raw, 1 );  // Raw timestamp?

    if ( prv_ts_avail && must_check_continuity )
      mbg_check_continuity( &ts, &prv_ts, NULL, NULL, max_allowed_delta );

    prv_ts = ts;
    prv_ts_avail = 1;

    if ( this_loops > 0 )
      this_loops--;

    if ( this_loops == 0 )
      break;

    // If this_loops is < 0, loop forever.

    if ( sleep_secs )
      sleep( sleep_secs );
    else
      if ( sleep_usecs )
        usleep( sleep_usecs );
  }

  return MBG_SUCCESS;

}  // show_fast_hr_timestamp



static /*HDR*/
/**
 * @brief Read a number of time stamps to a buffer, and print them afterwards.
 *
 * After the time stamps have been read to the buffer,
 * a second loop prints them from the buffer.
 * This way the time stamps can be read as fast as possible,
 * but this can not run continuously forever because the buffer size
 * is somewhat limited.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int show_fast_hr_timestamp_burst( MBG_DEV_HANDLE dh )
{
  PCPS_TIME_STAMP ts[MAX_TS_BURST];
  int32_t hns_latency[MAX_TS_BURST];
  int this_loops;
  int i;

  this_loops = ( loops && ( loops < MAX_TS_BURST ) ) ? loops : MAX_TS_BURST;


  if ( read_raw )
  {
    for ( i = 0; i < this_loops; i++ )
    {
      int rc = mbg_get_fast_hr_timestamp( dh, &ts[i] );

      if ( mbg_cond_err_msg( rc, "mbg_get_fast_hr_timestamp" ) )
        return rc;
    }
  }
  else
    for ( i = 0; i < this_loops; i++ )
    {
      int rc = mbg_get_fast_hr_timestamp_comp( dh, &ts[i], &hns_latency[i] );

      if ( mbg_cond_err_msg( rc, "mbg_get_fast_hr_timestamp_comp" ) )
        return rc;
    }

  for ( i = 0; i < this_loops; i++ )
  {
    PCPS_TIME_STAMP *p_prv_ts = i ? &ts[i - 1] : NULL;
    mbg_print_hr_timestamp( &ts[i], hns_latency[i], p_prv_ts, read_raw, 0 );  // Raw timestamp?
  }

  return MBG_SUCCESS;

}  // show_fast_hr_timestamp_burst



static /*HDR*/
int do_mbgfasttstamp( MBG_DEV_HANDLE dh )
{
  int rc = mbg_chk_dev_has_fast_hr_timestamp( dh );

  if ( mbg_rc_is_error( rc ) )
  {
    mbg_cond_err_msg_info( rc, "mbg_chk_dev_has_fast_hr_timestamp",
                           "support fast (memory mapped) time stamps" );
    return rc;
  }

  if ( burst_read )
    show_fast_hr_timestamp_burst( dh );
  else
    show_fast_hr_timestamp( dh );

  return MBG_SUCCESS;

}  // do_mbgfasttstamp

static MBG_DEV_HANDLER_FNC do_mbgfasttstamp;



static /*HDR*/
void usage( void )
{
  mbg_print_usage_intro( pname, true,
    "This example program reads fast high resolution time stamps.\n"
    "\n"
    "This is done using memory mapped I/O in the kernel driver, so\n"
    "this works only with devices that support memory mapped I/O."
  );
  mbg_print_help_options();
  mbg_print_opt_info( "-c", "Run continuously" );
  mbg_print_opt_info( "-n num", "Run num loops" );
  mbg_print_opt_info( "-b", "Burst read first, then show results" );
  mbg_print_opt_info( "-r", "Read raw time stamps, no cycles" );
  mbg_print_opt_info( "-C val", "Check for continuity, max allowed delta (seconds, with fractions)" );
  mbg_print_opt_info( "-s num", "Sleep num seconds between calls (implies -c)" );
  mbg_print_opt_info( "-u num", "Sleep num microseconds between calls (implies -c)" );
  mbg_print_usage_outro( DEV_OPT_PRINT_BUS_LEVEL, true );

}  // usage



int main( int argc, char *argv[] )
{
  int rc;
  int c;

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "bcn:rC:s:u:h?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'b':
        burst_read = 1;
        break;

      case 'c':
        loops = -1;
        break;

      case 'n':
        loops = atoi( optarg );
        break;

      case 'r':
        read_raw = 1;
        break;

      case 'C':
       must_check_continuity = 1;
       max_allowed_delta = atof( optarg ) * 1E6;
       break;

      case 's':
        sleep_secs = atoi( optarg );
        loops = -1;
        break;

      case 'u':
        sleep_usecs = atoi( optarg );
        loops = -1;
        break;

      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }

  #if !MBG_TGT_SUPP_MEM_ACC
    printf( "** Memory mapped access not supported on this target platform.\n\n" );
    return 1;
  #endif

  #if !MBG_PC_CYCLES_SUPPORTED
    printf( "** Warning: No cycles support to compute real latencies on this platform!\n" );

    if ( !read_raw )
    {
      read_raw = 1;
      printf( "** Falling back to raw mode.\n" );
    }

    printf( "\n" );
  #endif

  // Handle each of the specified devices.
  rc = mbg_handle_devices( argc, argv, optind, do_mbgfasttstamp, 0 );

  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
