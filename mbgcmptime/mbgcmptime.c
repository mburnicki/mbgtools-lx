
/**************************************************************************
 *
 *  $Id: mbgcmptime.c 1.12 2022/12/21 14:46:41 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for mbgcmptime program which compares the time on 2 devices
 *    by reading the time plus cycles from both devices immediately after
 *    each other, and compensates the execution delay based on the returned
 *    cycles counts.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgcmptime.c $
 *  Revision 1.12  2022/12/21 14:46:41  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from a function.
 *  Revision 1.11  2021/11/15 17:08:52  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.10  2021/11/08 18:01:08  martin.burnicki
 *  Account for modified MBG_DEV_FN type.
 *  Revision 1.9  2021/04/12 21:57:54  martin
 *  Updated printing of usage information.
 *  Revision 1.8  2021/03/21 17:39:41  martin
 *  Updated some comments.
 *  Revision 1.7  2021/03/12 14:20:13  martin
 *  Use the new global variable pc_cycles_frequency.
 *  Revision 1.6  2020/06/17 10:06:48  martin
 *  New option -R to also print raw (hex) timestamps.
 *  Workaround for clreol which is unsupported on Windows.
 *  Revision 1.5  2020/05/11 08:47:01  martin
 *  Removed erroneous duplicate definition of variable 'cyc_freq'.
 *  Revision 1.4  2018/12/14 12:51:29  martin
 *  Fixed evaluation of device specification parameters.
 *  Improved error handling.
 *  Revision 1.3  2018/11/15 12:39:28  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.2  2018/08/08 09:01:45  martin
 *  New option -a to compensate an initial offset automatically.
 *  New option -C which to append a marker '#' to the output if status changed.
 *  New option -L to specify a time offset limit. If the time offset change
 *  exceeds the specified limit, a marker '<<' is appended to the output.
 *  New option -q for quiet operation, i.e. output lines on the console
 *  are overwritten unless the previous line has a marker appended.
 *  New option -o to specify an output file that provides the full log
 *  even is -q is used, too.
 *  Options -s and -u also imply continuous mode.
 *  Fixed Windows build.
 *  Moved some common code to cmp_time_util module.
 *  Moved output unbuffering to toolutil.c::mbg_check_devices(), and account
 *  for additional parameter expected by the current version of that function.
 *  Use new, safe library functions.
 *  Close device handle when done.
 *  Proper return codes and exit codes.
 *  Revision 1.1  2013/01/25 10:26:13  martin
 *  Initial revision.
 *
 **************************************************************************/

// Include Meinberg headers.
#include <cmp_time_util.h>
#include <mbgdevio.h>
#include <toolutil.h>  // Common utility functions.
#include <str_util.h>

// Include system headers.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MBG_FIRST_COPYRIGHT_YEAR   2013
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.

static const char *pname = "mbgcmptime";


static int loops;
static int read_fast;
static int read_raw;
static long sleep_secs;
static long sleep_usecs;
static int print_raw;
static double warn_limit;
static double chk_limit;
static int auto_offset;
static int check_status;
static int quiet;

static const char *log_fn;

static const char *ref_dev_param;
static MBG_DEV_HANDLE dh_ref;
static PCPS_DEV dev_info_ref;

#if defined( MBG_TGT_WIN32 )
static const char term_clreol[] = "     ";     // ANSI not supported; just print some spaces to highlight end of line.
#else
static const char term_clreol[] = "\x1B[0K";   // ANSI escape sequence for clreol.
#endif



static /*HDR*/
FILE *fopen_log( const char *mode )
{
  FILE *fp = fopen( log_fn, mode );

  if ( fp == NULL )
  {
    fprintf( stderr, "Failed to open log file %s: %s. Giving up logging.\n", log_fn, strerror( errno ) );
    log_fn = NULL;
  }

  return fp;

}  // fopen_log



static /*HDR*/
int do_mbgcmptime( MBG_DEV_HANDLE dh )
{
  PCPS_HR_TIME_CYCLES prv_htc1 = { 0 };
  PCPS_HR_TIME_CYCLES prv_htc2 = { 0 };
  PCPS_HR_TIME_CYCLES htc1 = { 0 };
  PCPS_HR_TIME_CYCLES htc2 = { 0 };
  double delta_ts;
  double delta_cyc;
  double delta_t;
  double prv_delta_t = 0.0;
  int prv_delta_t_avail = 0;
  int this_loops = loops;
  int rc;
  size_t n;
  char ws[256];
  char ws1[128];
  char ws2[128];
  char ws3[64];
  int add_marker;

  if ( read_fast )
  {
    rc = chk_fast_tstamp_supp( dh, dh_ref, NULL );  // TODO err_msg_fnc ?

    if ( mbg_rc_is_error( rc ) )
    {
      if ( rc != MBG_ERR_NOT_SUPP_BY_DEV )
        goto done;

      // TODO Show msg ...
      read_fast = 0;
    }
  }

  rc = mbg_init_pc_cycles_frequency( dh );

  if ( mbg_cond_err_msg( rc, "mbg_init_pc_cycles_frequency" ) )
    goto done;


  for (;;)
  {
    rc = get_htc_timestamps( dh, &htc1, dh_ref, &htc2, read_fast );

    if ( rc != MBG_SUCCESS )
      break;

    delta_t = get_htc_delta( &htc1, &htc2, &delta_ts, &delta_cyc );

    if ( auto_offset )
    {
      static double initial_offset;
      static int initial_offset_set;

      if ( !initial_offset_set )
      {
        initial_offset = delta_t;
        initial_offset_set = 1;
        printf( "Initial offset: %.1f us\n", initial_offset * 1e6 );
      }

      delta_t -= initial_offset;
    }

    n = mbg_snprint_hr_tstamp_ext( ws1, sizeof( ws1 ), &htc1.t.tstamp, print_raw );

    if ( check_status )
      snprintf_safe( ws1 + n, sizeof( ws1 ) - n, " st: %04X", htc1.t.status );

    n = mbg_snprint_hr_tstamp_ext( ws2, sizeof( ws2 ), &htc2.t.tstamp, print_raw );

    if ( check_status )
      snprintf_safe( ws2 + n, sizeof( ws2 ) - n, " st: %04X", htc2.t.status );

    snprintf_safe( ws3, sizeof( ws3 ), "%+.1f us", delta_t * 1e6 );

    n = 0;

    n += snprintf_safe( &ws[n], sizeof( ws ) - n, "%s, %s", ws1, ws2 );

    n += snprintf_safe( &ws[n], sizeof( ws ) - n, " ts: %.1f us, cyc: %.1f us, delta: %s",
                        delta_ts * 1e6, delta_cyc * 1e6, ws3 );

    add_marker = 0;

    if ( ( warn_limit > 0.0 ) && ( fabs( delta_t ) > warn_limit ) )
    {
      n += sn_cpy_str_safe( &ws[n], sizeof( ws ) - n, " *" );
      add_marker = 1;
    }

    if ( prv_delta_t_avail )
    {
      double ddelta_t = delta_t - prv_delta_t;

      if ( ( chk_limit > 0.0 ) && ( fabs( ddelta_t ) > chk_limit ) )
      {
        n += sn_cpy_str_safe( &ws[n], sizeof( ws ) - n, " <<" );
        add_marker = 1;
      }

      if ( check_status )
      {
        if ( ( htc1.t.status != prv_htc1.t.status )
          || ( htc2.t.status != prv_htc2.t.status ) )
        {
          n += sn_cpy_str_safe( &ws[n], sizeof( ws ) - n, " #" );
          add_marker = 1;
        }
      }
    }

    #if defined( DEBUG )
      if ( htc2.t.tstamp.sec != htc1.t.tstamp.sec )
      {
        n += sn_cpy_str_safe( &ws[n], sizeof( ws ) - n, " X" );
        add_marker = 1;
      }
    #endif

    if ( add_marker )
      n += sn_cpy_str_safe( &ws[n], sizeof( ws ) - n, " !!" );

    if ( quiet )
    {
      // Overwrite last line, and only append new line if the
      // current line has a marker appended, so it's visible
      // on the console that something unexpected has happened.
      printf( "\r%s%s%s", ws, term_clreol, ( add_marker ) ? "\n" : "" );
    }
    else
      printf( "%s\n", ws );

    if ( log_fn )
    {
      FILE *fp = fopen_log( "a" );

      if ( fp )
      {
        fprintf( fp, "%s\n", ws );
        fclose( fp );
      }
    }

    if ( this_loops > 0 )
      this_loops--;

    if ( this_loops == 0 )
      break;

    // If this_loops is < 0, loop forever.

    prv_htc1 = htc1;
    prv_htc2 = htc2;
    prv_delta_t = delta_t;
    prv_delta_t_avail = 1;

    if ( sleep_secs )
      sleep( sleep_secs );
    else
      if ( sleep_usecs )
        usleep( sleep_usecs );

    // printf( "\n" );
  }

done:
  return rc;

}  // do_mbgcmptime



static /*HDR*/
void usage( void )
{
  mbg_print_usage_intro( pname, true,
    "Compare the time on two devices by reading the time stamps plus cycles from\n"
    "both devices immediately after each other, and compensate the execution delay\n"
    "based on the returned cycles counts.\n"
    "The device to be used as reference has to be specified by parameter -i (see\n"
    "below), whereas the device under test is specified as last parameter on the\n"
    "command line."
  );
  mbg_print_help_options();
  mbg_print_opt_info( "-i dev", "Reference device to which the time of the device under test shall be compared" );
  mbg_print_opt_info( "-l limit", "Append a marker '*' to the output if delta exceeds limit [us]" );
  mbg_print_opt_info( "-L limit", "Append a marker '<<' to the output if delta changes [us]" );
  mbg_print_opt_info( "-C", "Append a marker '#' to the output if status changed" );
  mbg_print_opt_info( "-f", "Read fast (memory mapped) timestamps" );
  mbg_print_opt_info( "-c", "Run continuously" );
  mbg_print_opt_info( "-n num", "Run num loops" );
  mbg_print_opt_info( "-r", "Read raw time stamps, no cycles" );
  mbg_print_opt_info( "-R", "Also display raw timestamps (hex)" );
  mbg_print_opt_info( "-s num", "Sleep num seconds between calls (implies -c)" );
  mbg_print_opt_info( "-u num", "Sleep num microseconds between calls (implies -c)" );
  mbg_print_opt_info( "-o file", "Also log output to <file>" );
  mbg_print_opt_info( "-q", "quiet, Only log errors to console (use -o to specify file for full log)" );
  mbg_print_opt_info( "-a", "Auto-compensate initial offset" );
  mbg_print_device_options( DEV_OPT_PRINT_BUS_LEVEL );

  printf( "\nExamples:\n\n" );
  printf(
    "Run once and compare the time from the 2 devices with indexes 1 and 2\n"
    "to the time from reference device with index 0:\n\n"
    "  %s -i 0 1 2\n\n", pname
  );
  printf(
    "Run continuously in 1 s intervals and compare the time from %s\n"
    "to the time from reference device %s:\n\n"
    "  %s -s 1 -i %s %s\n\n", EXAMPLE_DEV_NAME_2, EXAMPLE_DEV_NAME_1,
    pname, EXAMPLE_DEV_NAME_1, EXAMPLE_DEV_NAME_2
  );

  mbg_print_usage_syn1588_support();

}  // usage



int main( int argc, char *argv[] )
{
  MBG_DEV_FN ref_dev_fn;
  int rc;
  int c;

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "acCfi:l:L:n:o:qrRs:u:h?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'a':
        auto_offset = 1;
        break;

      case 'C':
        check_status = 1;
        break;

      case 'i':
        ref_dev_param = optarg;
        break;

      case 'l':
        warn_limit = (double) atoi( optarg ) / 1e6;
        break;

      case 'L':
        chk_limit = (double) atoi( optarg ) / 1e6;
        break;

      case 'f':
        read_fast = 1;
        break;

      case 'c':
        loops = -1;
        break;

      case 'n':
        loops = atoi( optarg );
        break;

      case 'o':
        log_fn = optarg;
        break;

      case 'q':
        quiet = 1;
        break;

      case 'r':
        read_raw = 1;
        break;

      case 'R':
        print_raw = 1;
        break;

      case 's':
        sleep_secs = atoi( optarg );
        loops = -1;
        break;

      case 'u':
        sleep_usecs = atoi( optarg );
        loops = -1;
        break;

      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }

  if ( ref_dev_param == NULL )
  {
    // No ref device specified.
    if ( !must_print_usage )
      fprintf( stderr, "A reference device has to be specified using the -i parameter.\n\n" );
    must_print_usage = true;
  }

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }

  #if !MBG_TGT_SUPP_MEM_ACC
    fprintf( stderr, "** Memory mapped access not supported on this target platform.\n\n" );
    return MBG_EXIT_CODE_NOT_SUPP;
  #endif

  #if !MBG_PC_CYCLES_SUPPORTED
    fprintf( stderr, "** Warning: No cycles support to compute real latencies on this platform!\n" );
    return MBG_EXIT_CODE_NOT_SUPP;
  #endif

  if ( quiet )  // Make stdout unbuffered.
    setvbuf( stdout, NULL, _IONBF, 0 );

  if ( log_fn )
  {
    // Create new or truncate existing log file.
    FILE *fp = fopen_log( "w" );

    if ( fp )
      fclose( fp );
  }

  // We don't call mbg_open_device_by_param_chk() here because
  // we want to print our own error message in case of failure.
  rc = mbg_open_device_by_param( &dh_ref, ref_dev_param, 0,
                                 ref_dev_fn.s, sizeof( ref_dev_fn.s ) );

  if ( mbg_rc_is_error( rc ) )
  {
    fprintf( stderr, "** Failed to open ref device %s: %s.\n",
             ref_dev_param,  mbg_strerror( rc ) );
    goto out;
  }

  #if defined( DEBUG )
    fprintf( stderr, "Ref device %s (%s) opened successfully.\n",
             ref_dev_param, ref_dev_fn.s );
  #endif

  mbg_get_device_info( dh_ref, &dev_info_ref );

  if ( mbg_rc_is_error( rc ) )
  {
    fprintf( stderr, "** Failed to get device info for ref device %s: %s.\n",
                     ref_dev_param, mbg_strerror( rc ) );
    goto out_close;
  }

  // Handle each of the specified devices.
  rc = mbg_handle_devices( argc, argv, optind, do_mbgcmptime, 0 );

out_close:
  mbg_close_device( &dh_ref );

out:
  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
