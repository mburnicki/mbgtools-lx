
#########################################################################
#
#  $Id: makefile-supp-syn1588.mk 1.7 2023/08/02 15:21:48 martin.burnicki REL_M $
#
#  Makefile to be included in the top-level makefile
#  of projects with SYN1588 support.
#
# -----------------------------------------------------------------------
#  $Log: makefile-supp-syn1588.mk $
#  Revision 1.7  2023/08/02 15:21:48  martin.burnicki
#  Avoid unintentionally making syn1588_dist the default goal.
#  Revision 1.6  2023/08/02 10:06:56  martin.burnicki
#  Improved support for SYN1588 debugging.
#  Revision 1.5  2022/09/07 17:26:43  martin.burnicki
#  Cleanup.
#  Revision 1.4  2022/07/06 14:39:27  martin.burnicki
#  Updated syn1588 module list.
#  Revision 1.3  2022/05/30 09:23:28  martin.burnicki
#  Support build option DEBUG_SYN1588_C_API.
#  Revision 1.2  2022/03/10 15:43:52  martin.burnicki
#  If a link has been set up that points to a specific directory with SYN1588
#  support files, assume that SYN1588 support is to be included by default.
#  Revision 1.1  2022/02/18 16:41:44  martin.burnicki
#  Initial revision where most SYN1588 stuff was
#  moved here from the top level Makefile.
#
#########################################################################

# If a link has been set up that points to a specific directory
# with SYN1588 support files, we assume that SYN1588 support
# is to be included by default.
ifndef SYN1588_DIR
  ifneq (,$(wildcard $(MBGLIB)/syn1588-dir))
    SYN1588_DIR = syn1588-dir
  endif
endif

# If a SYN1588 subdirectory (within mbglib) has explicitly
# been defined, we enable support for SYN1588 cards by default.
ifdef SYN1588_DIR
  SUPP_SYN1588 ?= 1
endif

# Unless support for SYN1588 cards has explicitly been enabled,
# it is disabled by default,
# The reason is that a C++ compiler is required only
# for the SYN1588 stuff, and the C++ compiler has to be
# e.g. gcc/g++ 8 or newer.
SUPP_SYN1588 ?= 0


# Some secondary symbols related to SYN1588.
SUPP_SYN1588_USR_SPC ?= $(SUPP_SYN1588)
SUPP_SYN1588_USR_SPC_CPP ?= $(SUPP_SYN1588_USR_SPC)
SUPP_SYN1588_SHM ?= 0
DEBUG_SYN1588_C_API ?= 0
DEBUG_SYN1588_DETECTION ?= 0
DEBUG_SYN1588_OPEN_CLOSE ?= 0


# The default subdirectory within the mbglib directory
# where the distributed SYN1588-specific files are located.
SYN1588_DIST_DIR = syn1588

# Unless a directory for the SYN1588 API has explicitly
# been specified, we use the default directory.
SYN1588_DIR ?= $(SYN1588_DIST_DIR)


# The basic list of SYN1588 API files required
# to build the driver package with SYN1588 support.
SYN1588_API_FILES = src/basicfunctions.h
SYN1588_API_FILES += src/basictypes.h
SYN1588_API_FILES += src/datasets.h
SYN1588_API_FILES += src/libs/clock/config_clock.h
SYN1588_API_FILES += src/libs/clock_ifc.h
SYN1588_API_FILES += src/libs/config/configdata.h
SYN1588_API_FILES += src/libs/config/simple_utils.cpp
SYN1588_API_FILES += src/libs/config/simple_utils.h
SYN1588_API_FILES += src/libs/config_ifc.h
SYN1588_API_FILES += src/libs/datatypes.h
SYN1588_API_FILES += src/libs/hwclock_ifc.h
SYN1588_API_FILES += src/libs/io_ifc.h
SYN1588_API_FILES += src/libs/log/config_log.h
SYN1588_API_FILES += src/libs/log/log.cpp
SYN1588_API_FILES += src/libs/log/log.h
SYN1588_API_FILES += src/libs/log_ifc.h
SYN1588_API_FILES += src/libs/network/config_network.h
SYN1588_API_FILES += src/libs/os.h
SYN1588_API_FILES += src/libs/os/$(TGT_SYSTEM)/os.cpp
SYN1588_API_FILES += src/libs/ptpdatatypes/basictypes.cpp
SYN1588_API_FILES += src/libs/security/config_security.h
SYN1588_API_FILES += src/libs/syn1588/$(TGT_SYSTEM)/syn1588_impl.cpp
SYN1588_API_FILES += src/libs/syn1588/syn1588.cpp
SYN1588_API_FILES += src/libs/syn1588/syn1588.h
SYN1588_API_FILES += src/libs/syn1588/syn1588_c_api.cpp
SYN1588_API_FILES += src/libs/syn1588/syn1588_check_register_access.cpp
SYN1588_API_FILES += src/libs/syn1588/syn1588_check_register_access.h
SYN1588_API_FILES += src/libs/syn1588/syn1588_ifc.cpp
SYN1588_API_FILES += src/libs/syn1588/syn1588_impl.h
SYN1588_API_FILES += src/libs/syn1588/ts_config.cpp
SYN1588_API_FILES += src/libs/syn1588/ts_config.h
SYN1588_API_FILES += src/libs/syn1588_c_api.h
SYN1588_API_FILES += src/libs/syn1588_ifc.h
SYN1588_API_FILES += src/msgx.h
SYN1588_API_FILES += src/ptptime.h

ifneq ($(ALT_SYSTEM),)
  # A list of API files for the alternate system only.
  SYN1588_API_FILES_ALT_SYS += src/libs/os/$(ALT_SYSTEM)/os.cpp
  SYN1588_API_FILES_ALT_SYS += src/libs/syn1588/$(ALT_SYSTEM)/syn1588_impl.cpp
endif


ifneq ($(SUPP_SYN1588_SHM),0)
  # These files are only required if shared memory support
  # is to be included.
  SYN1588_API_FILES += src/libs/sharedmem/config_io.h
  SYN1588_API_FILES += src/libs/sharedmem/io.cpp
  SYN1588_API_FILES += src/libs/sharedmem/io.h
  SYN1588_API_FILES += src/libs/sharedmem/io_reduced.cpp
  SYN1588_API_FILES += src/libs/sharedmem/io_reduced.h
  SYN1588_API_FILES += src/libs/sharedmem/$(TGT_SYSTEM)/semaphore_impl.cpp
  SYN1588_API_FILES += src/libs/sharedmem/$(TGT_SYSTEM)/semaphore_impl.h
  SYN1588_API_FILES += src/libs/sharedmem/$(TGT_SYSTEM)/sharedmem.cpp
  SYN1588_API_FILES += src/libs/sharedmem/$(TGT_SYSTEM)/sharedmem.h
  SYN1588_API_FILES += src/libs/sharedmem/semaphore_ifc.h
  SYN1588_API_FILES += src/libs/sharedmem/sharedmem_api.cpp
  SYN1588_API_FILES += src/libs/sharedmem/sharedmem_layout.h
  SYN1588_API_FILES += src/libs/sharedmem/sharedmem_layout_ptpv2.h
  SYN1588_API_FILES += src/libs/sharedmem_api.h

  ifneq ($(ALT_SYSTEM),)
    # A list of API files for the alternate system only.
    SYN1588_API_FILES_ALT_SYS += src/libs/sharedmem/$(ALT_SYSTEM)/semaphore_impl.cpp
    SYN1588_API_FILES_ALT_SYS += src/libs/sharedmem/$(ALT_SYSTEM)/semaphore_impl.h
    SYN1588_API_FILES_ALT_SYS += src/libs/sharedmem/$(ALT_SYSTEM)/sharedmem.cpp
    SYN1588_API_FILES_ALT_SYS += src/libs/sharedmem/$(ALT_SYSTEM)/sharedmem.h
  endif
endif


# The next file is yet only required for the win32 target.
ifeq ($(TGT_SYSTEM),$(SYSTEM_WIN32))
  SYN1588_API_FILES += src/libs/snprintf_safe.h
else
  SYN1588_API_FILES_ALT_SYS += src/libs/snprintf_safe.h
endif


ifneq ($(SUPP_SYN1588_USR_SPC_CPP),0)

  # Create lists of .cpp and .c source files that belong to the API.
  SYN1588_SOURCES_CPP = $(filter %.cpp,$(SYN1588_API_FILES))
  SYN1588_SOURCES_C = $(filter %.c,$(SYN1588_API_FILES))
  SYN1588_SOURCES = $(SYN1588_SOURCES_CPP) $(SYN1588_SOURCES_C)

  # Determine the list of files that have to be added
  # to the OBJS list.
  SYN1588_SRC_FILES_CPP = $(notdir $(SYN1588_SOURCES_CPP))
  SYN1588_SRC_FILES_C = $(notdir $(SYN1588_SOURCES_C))
  SYN1588_OBJS = $(SYN1588_SRC_FILES_CPP:.cpp=.o) $(SYN1588_SRC_FILES_C:.c=.o)
  SYN1588_OBJS += simple_utils.o

  # Determine the list of directories that have
  # to be added to VPATH.
  SYN1588_SRC_DIRS = $(addprefix $(MBGLIB)/$(SYN1588_DIR)/,$(sort $(dir $(SYN1588_SOURCES))))

  # Determine the list of directories that have
  # to be added to the include search path.
  SYN1588_HEADERS = $(filter %.h,$(SYN1588_API_FILES))

  ifeq ($(TGT_SYSTEM),$(SYSTEM_WIN32))
    # On Windows we need a specific list of include search
    # paths. Otherwise there may be name clashes of some
    # header files from the SYN1588 API with standard headers,
    # resulting in build errors.
    SYN1588_INC_DIR_LIST = .
    SYN1588_INC_DIR_LIST += src
    SYN1588_INC_DIR_LIST += proj/gcc
    SYN1588_INC_DIR_LIST += src/libs
    SYN1588_INC_DIR_LIST += src/libs/config
    SYN1588_INC_DIR_LIST += src/libs/ptpdatatypes
    SYN1588_INC_DIR_LIST += src/libs/log
    SYN1588_INC_DIR_LIST += src/libs/os/win32
    SYN1588_INC_DIR_LIST += src/libs/syn1588/
    SYN1588_INC_DIR_LIST += src/libs/syn1588/win32
    ifneq ($(SUPP_SYN1588_SHM),0)
      SYN1588_INC_DIR_LIST += src/libs/sharedmem/win32
    endif
#    SYN1588_INC_DIR_LIST += $(dir $(filter %.h,$(SYN1588_API_FILES_OPT)))

    SYN1588_INC_DIRS = $(addprefix $(MBGLIB)/$(SYN1588_DIR)/,$(SYN1588_INC_DIR_LIST))
  else
    # For other targets we just add the directories that
    # contain any of the header files in $(SYN1588_API_FILES).
    SYN1588_INC_DIR_LIST = $(dir $(SYN1588_HEADERS))

    SYN1588_INC_DIRS = $(addprefix $(MBGLIB)/$(SYN1588_DIR)/,$(sort $(SYN1588_INC_DIR_LIST)))
  endif

  CPPFLAGS_SYN1588_INC = $(patsubst %,-I%,$(SYN1588_INC_DIRS))
endif

export SUPP_SYN1588_USR_SPC
export SUPP_SYN1588_USR_SPC_CPP
export DEBUG_SYN1588_C_API
export DEBUG_SYN1588_DETECTION
export DEBUG_SYN1588_OPEN_CLOSE
export SYN1588_OBJS


ifdef DEBUG
  ifneq ($(SUPP_SYN1588_USR_SPC),0)
    SUPP_SYN1588_USR_SPC_DEBUG ?= $(DEBUG)
  endif
endif

SUPP_SYN1588_USR_SPC_DEBUG ?= 0

$(call debug_msg,$(INFO) SYN1588_DIR:                $(SYN1588_DIR))
$(call debug_msg,$(INFO) SYN1588_DIR_FOUND:          $(SYN1588_DIR_FOUND))
$(call debug_msg,$(INFO) SUPP_SYN1588:               $(SUPP_SYN1588))
$(call debug_msg,$(INFO) SUPP_SYN1588_USR_SPC:       $(SUPP_SYN1588_USR_SPC))
$(call debug_msg,$(INFO) SUPP_SYN1588_USR_SPC_CPP:   $(SUPP_SYN1588_USR_SPC_CPP))
$(call debug_msg,$(INFO) SUPP_SYN1588_USR_SPC_DEBUG: $(SUPP_SYN1588_USR_SPC_DEBUG))
$(call debug_msg,$(INFO) DEBUG_SYN1588_C_API:        $(DEBUG_SYN1588_C_API))
$(call debug_msg,$(INFO) DEBUG_SYN1588_DETECTION:    $(DEBUG_SYN1588_DETECTION))
$(call debug_msg,$(INFO) DEBUG_SYN1588_OPEN_CLOSE:   $(DEBUG_SYN1588_OPEN_CLOSE))


CPPFLAGS_SYN1588 += -DSUPP_SYN1588=$(SUPP_SYN1588)
CPPFLAGS_SYN1588 += -DSUPP_SYN1588_USR_SPC=$(SUPP_SYN1588_USR_SPC)
CPPFLAGS_SYN1588 += -DSUPP_SYN1588_USR_SPC_CPP=$(SUPP_SYN1588_USR_SPC_CPP)
CPPFLAGS_SYN1588 += -DSUPP_SYN1588_SHM=$(SUPP_SYN1588_SHM)
CPPFLAGS_SYN1588 += -DSUPP_SYN1588_USR_SPC_DEBUG=$(SUPP_SYN1588_USR_SPC_DEBUG)
CPPFLAGS_SYN1588 += -DDEBUG_SYN1588_C_API=$(DEBUG_SYN1588_C_API)
CPPFLAGS_SYN1588 += -DDEBUG_SYN1588_DETECTION=$(DEBUG_SYN1588_DETECTION)
CPPFLAGS_SYN1588 += -DDEBUG_SYN1588_OPEN_CLOSE=$(DEBUG_SYN1588_OPEN_CLOSE)
CPPFLAGS_SYN1588 += -DNO_LOGGING=1
CPPFLAGS_SYN1588 += -DLOG_LEVEL=-1


ifneq ($(SUPP_SYN1588_USR_SPC_CPP),0)
  ifneq ($(SUPP_SYN1588_SHM),0)
    # If we want to support the SYN1588 SHM with
    # newer versions of the SYN1588 source code,
    # we have to define _SHARED_MEM_.
    CPPFLAGS_SYN1588 += -D_SHARED_MEM_=1
  endif

  ifeq ($(TGT_SYSTEM),$(SYSTEM_WIN32))
    CPPFLAGS_SYN1588 += -include winsock2.h
  endif

  # If we use some C++ files, we probably also need
  # to link against the standard C++ run time library.
  LDLIBS_SYN1588 += -lstdc++
endif

#==================================================================================

# The section below is only useful for a maintainer of the driver package.
# The target syn1588_dist can be used to update the SYN1588 distribution files
# from a full PTPv2 repo.

# If no targets were specified, avoid that one of the phony targets
# defined below becomes the default goal when this makefile is included.

ifneq ($(MAKECMDGOALS),)

# Check if the src repo directory exists.
SYN1588_REPO_DIR = $(MBGLIB)/PTPv2
SYN1588_REPO_DIR_FOUND = $(call chk_exist_dir, $(SYN1588_REPO_DIR))

ifneq ($(SYN1588_REPO_DIR_FOUND),)
  # The src repo directory exists, so we can copy
  # the API files.

  SYN1588_DST_DIR = $(MBGLIB)/$(SYN1588_DIST_DIR)

  SYN1588_DIST_DST_FILES := $(addprefix $(SYN1588_DST_DIR)/,$(SYN1588_API_FILES))
  ifdef SYN1588_DIST_ALT_SYS
    SYN1588_DIST_DST_FILES += $(addprefix $(SYN1588_DST_DIR)/,$(SYN1588_API_FILES_ALT_SYS))
  endif

.PHONY: syn1588_dist
syn1588_dist: syn1588_dist_clean $(SYN1588_DIST_DST_FILES)


$(SYN1588_DIST_DST_FILES) : $(addprefix $(SYN1588_DST_DIR), %) : $(addprefix $(SYN1588_REPO_DIR), %)
	@mkdir -p $(dir $@)
	@cp -pv $< $@
	@chmod 644 $@

else

  # The src repo directory doesn't exist, so we
  # just print an error message.

.PHONY: syn1588_dist
syn1588_dist:
	@echo "Repo dir $(SYN1588_REPO_DIR) not found."

endif


.PHONY: syn1588_dist_clean
syn1588_dist_clean:
	rm -rf $(SYN1588_DST_DIR)

endif  # neq ($(MAKECMDGOALS),)
