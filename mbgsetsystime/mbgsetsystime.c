
/**************************************************************************
 *
 *  $Id: mbgsetsystime.c 1.19 2022/12/21 15:39:49 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for mbgsetsystime program which reads the current date
 *    and time from a Meinberg device and sets the system time accordingly.
 *    The program returns 0 if successful, otherwise a value greater
 *    than 0.
 *
 *  Be careful if using this program while (x)ntpd is running because
 *  (x)ntpd does not like time steps and may react unexpectedly.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgsetsystime.c $
 *  Revision 1.19  2022/12/21 15:39:49  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from a function.
 *  Revision 1.18  2021/11/15 17:08:53  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.17  2021/04/12 21:57:46  martin
 *  Updated printing of usage information.
 *  Revision 1.16  2021/03/21 22:40:08  martin
 *  Updated some comments.
 *  Revision 1.15  2021/03/12 12:32:41  martin
 *  Updated some comments.
 *  Revision 1.14  2020/02/27 13:29:20  martin
 *  Account for enhanced library functions.
 *  Revision 1.13  2019/07/19 14:23:03  martin
 *  Removed obsolete conditional code.
 *  Revision 1.12  2018/11/15 12:12:35Z  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.11  2018/10/30 14:49:55  martin
 *  Account for renamed library function in Windows-specific code.
 *  Revision 1.10  2018/01/15 18:21:49Z  martin
 *  Changed return type of snprintf_timespec() to int.
 *  Account for renamed library symbol NSEC_PER_SEC.
 *  Revision 1.9  2017/07/05 18:24:44  martin
 *  New way to maintain version information.
 *  Support build on Windows.
 *  Use high resolution time if the device supports it.
 *  Use codes and inline functions from mbgerror.h.
 *  Use functions from new module timeutil.
 *  Account for frac_sec_from_bin() obsoleted by bin_frac_32_to_dec_frac().
 *  Proper return codes and exit codes.
 *  Revision 1.8  2009/09/29 15:02:15  martin
 *  Updated version number to 3.4.0.
 *  Revision 1.7  2009/07/24 09:50:09  martin
 *  Updated version number to 3.3.0.
 *  Revision 1.6  2009/06/19 12:38:52  martin
 *  Updated version number to 3.2.0.
 *  Revision 1.5  2009/03/19 17:04:26  martin
 *  Updated version number to 3.1.0.
 *  Updated copyright year to include 2009.
 *  Revision 1.4  2008/12/22 12:43:31  martin
 *  Updated description, copyright, revision number and string.
 *  Use unified functions from toolutil module.
 *  Accept device name(s) on the command line.
 *  Don't use printf() without format, which migth produce warnings
 *  with newer gcc versions.
 *  Revision 1.3  2007/07/24 09:33:01  martin
 *  Updated copyright to include 2007.
 *  Revision 1.2  2003/04/25 10:28:05  martin
 *  Use new functions from mbgdevio library.
 *  New program version v2.1.
 *  Revision 1.1  2001/09/17 15:08:46  martin
 *
 **************************************************************************/

// Include Meinberg headers.
#include <mbgdevio.h>
#include <pcpsmktm.h>
#include <toolutil.h>  // Common utility functions.
#include <timeutil.h>
#include <str_util.h>
#include <mbgerror.h>

// Include system headers.
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>


#define MBG_FIRST_COPYRIGHT_YEAR   2001
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.

static const char *pname = "mbgsetsystime";



static /*HDR*/
int sn_printf_timespec( char *s, size_t max_len, const struct timespec *p_ts )
{
  struct tm tm = { 0 };
  int rc = mbg_gmtime( &tm, &p_ts->tv_sec );

  if ( mbg_rc_is_error( rc ) )  // Conversion failed.
    return sn_cpy_str_safe( s, max_len, "(invalid time)" );

  return snprintf_safe( s, max_len, "%04i-%02i-%02i %02i:%02i:%02i.%09li UTC",
                        tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
                        tm.tm_hour, tm.tm_min, tm.tm_sec,
                        (long) p_ts->tv_nsec );

}  // sn_printf_timespec



static /*HDR*/
int set_system_time( const struct timespec *p_ts )
{
  char ws[100];

  int rc = clock_settime( CLOCK_REALTIME, p_ts );

  sn_printf_timespec( ws, sizeof( ws ), p_ts );

  if ( rc < 0 )  // Usually 0 on success, -1 on error.
  {
    rc = mbg_get_last_error( NULL );

    fprintf( stderr, "Failed to set system time to %s: %s\n", ws, mbg_strerror( rc ) );
    return rc;
  }

  printf( "Date/time set to %s\n", ws );

  return MBG_SUCCESS;

}  // set_system_time



static /*HDR*/
int set_system_time_from_pcps_time( const PCPS_TIME *p_t )
{
  struct tm tm = { 0 };
  MBG_TIME64_T t_rev;
  MBG_TIME64_T t;

  int rc = pcps_mktime64( &t, p_t, 1970 );

  if ( mbg_rc_is_error( rc ) )
  {
    fprintf( stderr, "Failed to convert %02u.%02u.%02u %02u:%02u:%02u.%02u (UTC%+02ih) to system time: %s\n",
             p_t->mday, p_t->month, p_t->year,
             p_t->hour, p_t->min, p_t->sec, p_t->sec100,
             p_t->offs_utc, mbg_strerror( rc ) );
    return rc;
  }

  t_rev = t + p_t->offs_utc * SECS_PER_HOUR;

  rc = mbg_gmtime64( &tm, &t_rev );

  if ( mbg_rc_is_error( rc ) )
    return rc;

  if ( ( tm.tm_year % 100 != p_t->year ) ||
       ( tm.tm_mon + 1 != p_t->month ) ||
       ( tm.tm_mday != p_t->mday ) ||
       ( tm.tm_hour != p_t->hour ) ||
       ( tm.tm_min != p_t->min ) ||
       ( tm.tm_sec != p_t->sec ) )
  {
    fprintf( stderr, "reversely computed date/time differs from original\n" );
    return MBG_ERR_RANGE;
  }

  #if defined( MBG_TGT_WIN32 )
  {
    SYSTEMTIME st;

    union
    {
      FILETIME ft;
      ULONGLONG ull;
    } u;

    // Convert seconds and fractions to 100 ns units.
    u.ull = FILETIME_1970 +
      (ULONGLONG) t * 10 * 1000 * 1000 +
      (ULONGLONG) (ulong) p_t->sec100 * 100000;

    if ( !FileTimeToSystemTime( &u.ft, &st ) )
    {
      int err = mbg_win32_sys_err_to_mbg( GetLastError(), NULL );

      fprintf( stderr, "Failed to convert FILETIME to system time: %s\n",
               mbg_strerror( err ) );
      return err;
    }

    if ( !SetSystemTime( &st ) )
    {
      #if 0
        LPVOID lpMsgBuf;
        DWORD last_error = GetLastError();

        FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER |
                       FORMAT_MESSAGE_FROM_SYSTEM |
                       FORMAT_MESSAGE_IGNORE_INSERTS,
                       NULL,
                       last_error,
                       MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), // Default language
                       (LPTSTR) &lpMsgBuf,
                       0,
                       NULL
                     );

        fprintf( stderr, "Failed to set system time: %s (code 0x%08lX)\n",
                 lpMsgBuf, (ulong) last_error );

        // Free the buffer.
        LocalFree( lpMsgBuf );
      #else
        int err = mbg_win32_sys_err_to_mbg( GetLastError(), NULL );

        fprintf( stderr, "Failed to set system time: err %i\n", err );
      #endif
      return err;
    }

    return MBG_SUCCESS;
  }
  #else  // Assuming POSIX.
  {
    struct timespec ts;

    ts.tv_sec = t;
    ts.tv_nsec = (long) p_t->sec100 * ( NSEC_PER_SEC / 100 );  // Convert to nanoseconds.

    return set_system_time( &ts );
  }
  #endif

}  // set_system_time_from_pcps_time



static /*HDR*/
int do_set_system_time_from_pcps_time( MBG_DEV_HANDLE dh )
{
  PCPS_TIME t;

  int rc = mbg_get_time( dh, &t );

  if ( mbg_cond_err_msg( rc, "mbg_get_time" ) )
    return rc;

  if ( t.status & PCPS_INVT )
  {
    // This may happen if the battery onboard the device
    // has been discharged or disconnected.
    printf( "The device has no valid date/time.\n" );
    return MBG_ERR_INV_TIME;
  }

  rc = set_system_time_from_pcps_time( &t );

  return rc;

}  // do_set_system_time_from_pcps_time



static /*HDR*/
int do_set_system_time_from_pcps_hr_time( MBG_DEV_HANDLE dh )
{
  PCPS_HR_TIME ht;
  struct timespec ts = { 0 };

  int rc = mbg_get_hr_time( dh, &ht );

  if ( mbg_cond_err_msg( rc, "mbg_get_hr_time" ) )
    return rc;

  if ( ht.status & PCPS_INVT )
  {
    // This may happen if the battery onboard the device
    // has been discharged or disconnected.
    printf( "The device has no valid date/time.\n" );
    return MBG_ERR_INV_TIME;
  }

  ts.tv_sec = ht.tstamp.sec;
  ts.tv_nsec = bin_frac_32_to_dec_frac( ht.tstamp.frac, NSEC_PER_SEC );

  return set_system_time( &ts );

}  // do_set_system_time_from_pcps_hr_time



static /*HDR*/
int do_mbgsetsystime( MBG_DEV_HANDLE dh )
{
  static int system_time_has_been_set;

  int rc = MBG_SUCCESS;

  if ( system_time_has_been_set )
    goto done;

  rc = mbg_chk_dev_has_hr_time( dh );

  if ( mbg_rc_is_success( rc ) )
    rc = do_set_system_time_from_pcps_hr_time( dh );
  else
    rc = do_set_system_time_from_pcps_time( dh );

  if ( mbg_rc_is_success( rc ) )
    system_time_has_been_set = 1;

  puts( "" );

done:
  return rc;

}  // do_mbgsetsystime

static MBG_DEV_HANDLER_FNC do_mbgsetsystime;



static /*HDR*/
void usage( void )
{
  mbg_print_usage_intro( pname, true,
    "This program can be used to set the system time to the time of a device.\n"
    "This should be done only at boot time, before the NTP daemon is started.\n"
    "Please *don't* run this program while ntpd is already active."
  );
  mbg_print_help_options();
  mbg_print_usage_outro( DEV_OPT_PRINT_BUS_LEVEL, true );

}  // usage



int main( int argc, char *argv[] )
{
  int c;
  int rc;

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "h?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }

  // Handle each of the specified devices.
  rc = mbg_handle_devices( argc, argv, optind, do_mbgsetsystime, 0 );

  // Determine the exit code based on the return code.

  if ( mbg_rc_is_success( rc ) )
    return MBG_EXIT_CODE_SUCCESS;   // Success.

  if ( rc == MBG_ERR_INV_TIME )
    return MBG_EXIT_CODE_INV_TIME;  // Device has no valid time to set the system time with.

  return MBG_EXIT_CODE_FAIL;        // Any error occurred.
}
