
/**************************************************************************
 *
 *  $Id: mbgtcrcal.c 1.19 2022/12/21 15:07:32 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for mbgtcrcal program which can be used to calibrate
 *    the IRIG input of some TCR devices.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgtcrcal.c $
 *  Revision 1.19  2022/12/21 15:07:32  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from some functions,
 *  and use feature check functions instead of macros.
 *  Revision 1.18  2021/11/15 17:08:53  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.17  2021/04/12 21:58:03  martin
 *  Updated printing of usage information.
 *  Revision 1.16  2021/03/21 22:40:47  martin
 *  Updated a bunch of comments.
 *  Revision 1.15  2021/03/12 12:32:55  martin
 *  Updated some comments.
 *  Revision 1.14  2020/10/20 10:46:54  martin
 *  Use function mbg_has_admin_rights() to determine
 *  if the program runs with admin rights.
 *  Revision 1.13  2018/12/14 13:04:53  martin
 *  Unified usage output.
 *  Use predefined macros to check return codes.
 *  Revision 1.12  2018/12/11 16:15:39  martin
 *  Fixed compiler warning on Windows.
 *  Revision 1.11  2018/12/11 11:57:32Z  martin
 *  Fixed compiler warnings on Windows.
 *  Revision 1.10  2018/11/15 12:39:29Z  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.9  2017/07/03 17:33:50  martin
 *  Account for renamed library symbols.
 *  Revision 1.8  2017/06/22 15:26:55  martin
 *  Proper return codes and exit codes.
 *  Revision 1.7  2016/10/20 14:12:42  martin
 *  Unified usage information.
 *  Revision 1.6  2016/08/10 12:54:07  martin
 *  Check for MBG_TGT_POSIX instead of MBG_TGT_UNIX.
 *  Revision 1.5  2016/07/22 09:53:54  martin
 *  Use new, safe library functions.
 *  Revision 1.4  2013/12/04 10:00:14  martin
 *  Fixed build on Windows.
 *  Revision 1.3  2013/07/22 16:26:42Z  martin
 *  Updated a comment.
 *  Revision 1.2  2013/07/11 08:28:44  martin
 *  Account for modified function call parameter list.
 *  Revision 1.1  2012/01/17 14:04:22  martin
 *  Initial revision.
 *
 **************************************************************************/

// Include Meinberg headers.
#include <irig_cal.h>
#include <toolutil.h>  // Common utility functions.
#include <str_util.h>

// Include system headers.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


#define MBG_FIRST_COPYRIGHT_YEAR   2012
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.


static const char *pname = "mbgtcrcal";

static int verbose;

static const char *icode_rx_names[N_ICODE_RX] = DEFAULT_ICODE_RX_NAMES;
static const char *irig_comp_names[N_IRIG_RX_COMP] = DEFAULT_IRIG_RX_COMP_NAMES;

static double cal_val;          // [microseconds]
static int must_set_cal_val;
static size_t max_label_len;    // Determined at runtime.



static /*HDR*/
char *str_comp_data( char *s, int max_len, int comp_data )
{
  div_t dt = div( comp_data, 10 );

  snprintf_safe( s, max_len, "%i.%i", dt.quot, abs ( dt.rem ) );

  return s;

}  // str_comp_data



static /*HDR*/
void print_cal_recs( int num_rec, CAL_REC_IRIG_RX_COMP cal_rec_comp[] )
{
  int idx;
  char ws[20];

  for ( idx = 0; idx < num_rec; idx++ )
  {
    CAL_REC_IRIG_RX_COMP *p = &cal_rec_comp[idx];
    int l = (int) ( max_label_len - strlen( irig_comp_names[idx] ) + 1 );

    printf( "  Signal group %i (%s)%-*c %s", idx, irig_comp_names[idx], l, ':',
            str_comp_data( ws, sizeof( ws ), p->comp_data.c[0] ) );

    if ( verbose )
    {
      int i;

      for ( i = 1; i < N_IRIG_RX_COMP_VAL; i++ )
        printf( ", %s", str_comp_data( ws, sizeof( ws ), p->comp_data.c[i] ) );
    }

    printf( " us\n" );
  }

}  // print_cal_recs



static /*HDR*/
int do_mbgtcrcal( MBG_DEV_HANDLE dh )
{
  IRIG_INFO irig_rx_info;
  CAL_REC_IRIG_RX_COMP cal_rec_comp[N_IRIG_RX_COMP] = { { { 0 } } };
  CAL_REC_IRIG_RX_COMP *p;
  int ret_val = MBG_SUCCESS;
  int num_rec = 0;
  int rc;
  uint16_t idx;
  char ws[20];
  int curr_group_idx;

  // Check if this device provides an IRIG input.
  rc = mbg_chk_dev_is_tcr( dh );

  if ( mbg_rc_is_error( rc ) )
  {
    printf( "This device does not provide an IRIG input.\n" );
    goto done;
  }


  // Read the current IRIG input configuration.
  rc = mbg_get_irig_rx_info( dh, &irig_rx_info );

  if ( mbg_rc_is_error( rc ) )
  {
    printf( "** Failed to read IRIG RX configuration, rc: %i\n", rc );
    goto done;
  }


  // Check if the currently configured IRIG code is supported by this program.
  if ( irig_rx_info.settings.icode >= N_ICODE_RX )
  {
    printf( "IRIG code %i configured on this device exceeds the maximum code supported by this program (%i)!\n",
            irig_rx_info.settings.icode, N_ICODE_RX - 1 );
    goto done;
  }

  // Determine the compensation code group for the current IRIG code.
  curr_group_idx = mbg_icode_rx_to_group_idx( irig_rx_info.settings.icode );

  if ( curr_group_idx < 0 )
    printf( "Warning: Unable to assign IRIG RX code %s to a know compensation code group.\n",
            icode_rx_names[irig_rx_info.settings.icode] );

  // We may continue here and display the current settings,
  // but we will not be able to assign and save new settings.


  if ( mbg_rc_is_error( mbg_chk_dev_has_generic_io( dh ) ) )
  {
    printf( "This device does not support IRIG calibration\n" );
    goto done;
  }


  // Read current compensation settings.
  rc = mbg_get_cal_rec_irig_rx_num_rec( dh, &num_rec );

  #if defined( DEBUG )
    printf( "mbg_get_cal_rec_irig_rx_num_rec() returned %i", rc );

    if ( mbg_rc_is_success( rc ) )
      printf( ", irig_rx_num_rec: %i", num_rec );

    printf( "\n" );
  #endif

  if ( mbg_rc_is_error( rc ) )
  {
    printf( "** Failed to read number of IRIG RX calibration records, rc: %i\n", rc );
    goto done;
  }


  // Do some plausibility checks.

  if ( curr_group_idx >= num_rec )
  {
    printf( "** Warning: the compensation group index for the current IRIG code (%i)\n"
            "exceeds the number of records supported by the device (%i).\n",
            curr_group_idx, num_rec - 1 );
  }


  if ( num_rec > N_IRIG_RX_COMP )
  {
    printf( "** The number of calibration records actually supported by the device (%i)\n"
            "exceeds the number of records supported by this program (%i).\n",
            num_rec, N_IRIG_RX_COMP );
    num_rec = N_IRIG_RX_COMP;
  }


  if ( num_rec == 0 )
  {
    printf( "** This device does not support any IRIG RX calibration records.\n" );
    goto done;
  }


  // Determine the maximum length of the group names to be displayed later.
  for ( idx = 0; idx < num_rec; idx++ )
  {
    size_t l = strlen( irig_comp_names[idx] );

    if ( l > max_label_len )
      max_label_len = l;
  }


  for ( idx = 0; idx < num_rec; idx++ )
  {
    p = &cal_rec_comp[idx];

    rc = mbg_get_cal_rec_irig_rx_comp( dh, idx, p );

    if ( mbg_rc_is_error( rc ) )
    {
      printf( "** Failed to read IRIG RX calibration record #%i. rc = %i\n", idx, rc );
      goto done;
    }

    if ( p->hdr.type != CAL_REC_TYPE_IRIG_RX_COMP )
    {
      printf( "** Calibration record header type is %i, no %i as expected.\n",
              p->hdr.type, CAL_REC_TYPE_IRIG_RX_COMP );
      goto done;
    }
  }

  printf( "\nCurrent compensation settings:\n" );

  print_cal_recs( num_rec, cal_rec_comp );

  printf( "\nCurrent IRIG code: %s --> group %i compensation effective\n",
          icode_rx_names[irig_rx_info.settings.icode],
          curr_group_idx );

  if ( !must_set_cal_val )
    goto done;

  if ( curr_group_idx < 0 )
    goto done;

  if ( curr_group_idx >= num_rec )
    goto done;


  printf( "\n" );

  // Save new calibration setting effective for the current IRIG code.
  p = &cal_rec_comp[curr_group_idx];
  p->hdr.type = CAL_REC_TYPE_IRIG_RX_COMP;  // Just to be sure.
  memset( &p->comp_data, 0, sizeof( p->comp_data ) );
  p->comp_data.c[0] = (int16_t) ( cal_val * 10 );   // us to hns.

  rc = mbg_set_cal_rec_irig_rx_comp( dh, p );

  if ( mbg_rc_is_error( rc ) )
  {
    printf( "** Failed to save new IRIG calibration setting for group %s.\n",
            irig_comp_names[curr_group_idx] );
    goto done;
  }


  printf( "IRIG calibration setting for code group %s has been set to %s us.\n",
          irig_comp_names[curr_group_idx], str_comp_data( ws, sizeof( ws ), p->comp_data.c[0] ) );


done:
  mbg_close_device( &dh );

  return ret_val;

}  // do_mbgtcrcal



static /*HDR*/
void usage( void )
{
  char ws[20];

  mbg_print_usage_intro( pname, true,
    "Show or set IRIG RX calibration parameters of devices which support this." );

  mbg_print_help_options();
  mbg_print_opt_info( "-c val", "Set calibration value \'val\'" );
  mbg_print_opt_info( "-v", "Increase verbosity" );
  mbg_print_device_options( DEV_OPT_PRINT_BUS_LEVEL );

  puts( "" );
  printf( "Example usage:\n"
          "\n"
          "%s                   Print current settings of the default device\n"
          "%s [dev]             Print current settings of specific device [dev]\n"
          "%s -c 10             Compensate +10.0 us on default device\n"
          "%s -c -12.1 [dev]    Compensate -12.1 us on specific device [dev]\n"
          "\n", pname, pname, pname, pname );
  printf( "New settings become effective for a group of IRIG codes with similar characteristics\n"
          "to which the currently configured IRIG RX code belongs. Calibration values are\n"
          "in microseconds, with 0.1 us resolution, in the range +/-%s us.\n",
          str_comp_data( ws, sizeof( ws ), IRIG_RX_COMP_MAX )
        );
  puts( "" );

}  // usage



int main( int argc, char *argv[] )
{
  int rc;
  int c;

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "c:vh?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'c':
        cal_val = atof( optarg );
        must_set_cal_val = 1;
        break;

      case 'v':
        verbose++;
        break;

      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }

  if ( !mbg_has_admin_rights() )
    must_print_usage = true;

  if ( must_print_usage )
  {
    printf( "This program must be run " ROOT_PRIVILEGES_STR " to work correctly.\n" );
    #if defined( DEBUG )
      if ( !must_print_usage )
        printf( "(Built with DEBUG, so resuming anyway)\n" );
    #else
      must_print_usage = true;
    #endif
    printf( "\n" );
  }

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }

  // Handle each of the specified devices.
  rc = mbg_handle_devices( argc, argv, optind, do_mbgtcrcal, 0 );

  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
