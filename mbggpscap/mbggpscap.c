
/**************************************************************************
 *
 *  $Id: mbggpscap.c 1.21 2022/12/21 15:16:24 martin.burnicki REL_M $
 *
 *  Description:
 *    Main file for mbggpscap program which demonstrates how to access
 *    a Meinberg device via IOCTL calls and read entries from the time
 *    capture FIFO buffer.
 *
 *    Please note that this may only work with devices which provide
 *    time capture input(s).
 *
 * -----------------------------------------------------------------------
 *  $Log: mbggpscap.c $
 *  Revision 1.21  2022/12/21 15:16:24  martin.burnicki
 *  Improved error handling, message output, and comments.
 *  Removed obsolete (PCPS_DEV *) parameter from some functions,
 *  and use feature check functions instead of macros.
 *  Revision 1.20  2021/11/15 17:08:53  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.19  2021/04/12 21:57:50  martin
 *  Updated printing of usage information.
 *  Revision 1.18  2021/03/21 17:56:33  martin
 *  Updated a bunch of comments.
 *  Revision 1.17  2021/03/12 11:50:22  martin
 *  Corrected the wording of some comments.
 *  Revision 1.16  2020/08/10 15:55:06  martin
 *  Fixed build with compilers that don't support 'long long'.
 *  Revision 1.15  2020/02/27 13:21:41Z  martin
 *  New options -u and -Q.
 *  Revision 1.14  2018/11/15 12:12:36  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.13  2018/01/10 13:36:35  martin
 *  Use current API mbg_get_serial_settings() to check
 *  serial settings. Don't change configuration but just  warn
 *  if capture events might be spit out via a serial port.
 *  Added some doxygen comments.
 *  Revision 1.12  2017/11/29 12:49:31  martin
 *  Added feature to log capture events to a file.
 *  Revision 1.11  2017/07/05 18:59:14  martin
 *  New way to maintain version information.
 *  Support build on Windows.
 *  Improved code execution paths.
 *  Improved reading capture events arriving at a high rate.
 *  Support validation of capture signals arriving at a constant rate.
 *  Fixed a bug when displaying the capture event status. TTM and
 *  PCPS_HR_TIME are using different sets of status flags.
 *  New option -r which displays raw timestamps and raw status.
 *  New option -o which forces usage of old API.
 *  New command line option to clear buffer.
 *  Account for PCPS_HRT_BIN_FRAC_SCALE renamed to MBG_FRAC32_UNITS_PER_SEC.
 *  Use more functions from common library modules.
 *  Use codes and inline functions from mbgerror.h.
 *  Proper return codes and exit codes.
 *  Revision 1.10  2009/09/29 15:02:15  martin
 *  Updated version number to 3.4.0.
 *  Revision 1.9  2009/07/24 09:50:08  martin
 *  Updated version number to 3.3.0.
 *  Revision 1.8  2009/06/19 12:38:51  martin
 *  Updated version number to 3.2.0.
 *  Revision 1.7  2009/03/19 17:04:26  martin
 *  Updated version number to 3.1.0.
 *  Updated copyright year to include 2009.
 *  Revision 1.6  2008/12/22 12:00:55  martin
 *  Updated description, copyright, revision number and string.
 *  Use unified functions from toolutil module.
 *  Accept device name(s) on the command line.
 *  Don't use printf() without format, which migth produce warnings
 *  with newer gcc versions.
 *  Revision 1.5  2007/07/24 09:32:26  martin
 *  Updated copyright to include 2007.
 *  Revision 1.4  2006/02/22 15:29:17  martin
 *  Support new ucap API.
 *  Print an error message if device can't be opened.
 *  Revision 1.3  2004/11/08 15:47:10  martin
 *  Using type cast to avoid compiler warning.
 *  Revision 1.2  2003/04/25 10:28:05  martin
 *  Use new functions from mbgdevio library.
 *  New program version v2.1.
 *  Revision 1.1  2001/09/17 15:08:22  martin
 *
 **************************************************************************/

// Include Meinberg headers.
#include <mbgdevio.h>
#include <deviohlp.h>
#include <toolutil.h>  // Common utility functions.
#include <str_util.h>

// Include system headers.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define DEFAULT_USLEEP_INTV  10000  // [microseconds]


#define MBG_FIRST_COPYRIGHT_YEAR   2001
#define MBG_LAST_COPYRIGHT_YEAR    0     // Use default.

static const char *pname = "mbggpscap";


static int continuous;
static double nom_cap_intv;    // Nominal capture interval to check, [s].
static double max_cap_jitter;  // Max. allowed jitter, [s].
static int raw;
static int force_old_api;
static int clear_buffer;
static const char *log_fn;

static int has_been_called;
static int must_check_intv;
static int query_only;
static int query_continuously;
static int sleep_usecs = DEFAULT_USLEEP_INTV;

static double delta_ucap;
static int jitter_exceeded;
static uint64_t jitter_exceed_cnt;
static uint64_t cap_overrun_cnt;
static uint64_t buf_overrun_cnt;



static /*HDR*/
/**
 * @brief Show the current and max. number of entries in the capture FIFO buffer.
 *
 * Retrieve and print information on the maximum number of events that can
 * be stored in the onboard FIFO, and the number of events that are currently
 * stored.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p_uce  Address of a ::PCPS_UCAP_ENTRIES variable to be filled.
 * @param[in]   info_1 An optional string to be printed before the buffer information, may be @a NULL.
 * @param[in]   info_2 An optional string to be appended to the buffer information, may be @a NULL.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::show_ucap_event
 * @see ::mbg_get_ucap_entries
 */
int show_ucap_entries( MBG_DEV_HANDLE dh, PCPS_UCAP_ENTRIES *p_uce,
                       const char *info_1, const char *info_2 )
{
  int rc = mbg_get_ucap_entries( dh, p_uce );

  if ( mbg_rc_is_success( rc ) )
  {
    // Many devices report that they could save one more capture event
    // than they actually do save, so adjust the reported value
    // for a proper display.
    if ( p_uce->max )
      p_uce->max--;

    if ( info_1 )
      printf( "%s", info_1 );

    printf( "%u of %u entries",
            p_uce->used, p_uce->max );

    if ( info_2 )
      printf( "%s", info_2 );

  }

  return rc;

}  // show_ucap_entries



static /*HDR*/
/**
 * @brief Check if capture events occurred in specified intervals.
 *
 * Can be used to check if periodic trigger slopes generate
 * events according to the trigger intervals.
 *
 * The parameters ::nom_cap_intv and ::max_cap_jitter can be
 * specified on the command line to define a tolerance range.
 *
 * @param[in]  ucap  The capture event read from a device.
 *
 * @return  1 if the specified jitter was exceeded, else 0.
 */
int check_ucap_intv( const PCPS_HR_TIME *ucap )
{
  static PCPS_HR_TIME prv_ucap;

  jitter_exceeded = 0;

  if ( has_been_called )
  {
    double abs_delta;

    delta_ucap = ucap->tstamp.sec - prv_ucap.tstamp.sec;
    delta_ucap += ( (double) ucap->tstamp.frac - prv_ucap.tstamp.frac ) / MBG_FRAC32_UNITS_PER_SEC;

    abs_delta = delta_ucap - nom_cap_intv;

    if ( abs_delta < 0.0 )
      abs_delta = -abs_delta;

    if ( abs_delta > max_cap_jitter )
    {
      jitter_exceeded = 1;
      jitter_exceed_cnt++;
    }
  }

  prv_ucap = *ucap;
  has_been_called = 1;

  return jitter_exceeded;

}  // check_ucap_intv



static /*HDR*/
/**
 * @brief Show a user capture event in ::PCPS_HR_TIME format.
 *
 * The current API call ::mbg_get_ucap_event provides
 * capture events as ::PCPS_HR_TIME structure, but
 * there may be older devices which only support the
 * obsolete API call ::mbg_get_gps_ucap.
 *
 * @param[in]  ucap  The capture event read from a device.
 *
 * @see ::mbg_get_ucap_event
 * @see ::mbg_get_gps_ucap
 * @see ::show_gps_ucap
 */
void show_ucap_event( const PCPS_HR_TIME *ucap )
{
  char ws[80];
  char s[400];
  int n;

  // Print converted date and time to a string.
  mbg_snprint_hr_time( ws, sizeof( ws ), ucap, raw );

  // Print the timestamp.
  // ucap->signal contains the channel number.
  n = snprintf_safe( s, sizeof( s ), "New capture: CH%i: %s", ucap->signal, ws );

  if ( must_check_intv )
  {
    n += snprintf_safe( &s[n], sizeof( s ) - n, " %+.7f", delta_ucap );

    if ( jitter_exceed_cnt )
      n += snprintf_safe( &s[n], sizeof( s ) - n, " %" PRIu64 "%s", jitter_exceed_cnt, jitter_exceeded ? " <<" : "" );
  }


  // Status bit definitions can be found in pcpsdefs.h,
  // see @ref PCPS_TIME_STATUS_FLAGS.

  if ( raw )
    n += snprintf_safe( &s[n], sizeof( s ) - n, ", st: 0x%04X", ucap->status );

  if ( ucap->status & PCPS_UCAP_OVERRUN )     // Capture events have occurred too fast.
    n += snprintf_safe( &s[n], sizeof( s ) - n, " << CAP OVR" );

  if ( ucap->status & PCPS_UCAP_BUFFER_FULL ) // Capture buffer was full, events have been lost.
    n += snprintf_safe( &s[n], sizeof( s ) - n, " << BUF OVR" );

  printf( "%s\n", s );

  if ( log_fn )
  {
    FILE *fp = fopen( log_fn, "at" );

    if ( fp )
    {
      fprintf( fp, "%s\n", s );
      fclose( fp );
    }
    else
    {
      fprintf( stderr, "** Failed to open file %s for writing: %s\n",
               log_fn, strerror( errno ) );
      log_fn = NULL;
    }
  }

}  // show_ucap_event



static /*HDR*/
/**
 * @brief Show a user capture event in ::TTM format read from device.
 *
 * This function is actually obsolete because it shows user
 * capture events provided as ::TTM structure, as returned
 * by the obsolete API call ::mbg_get_gps_ucap.
 *
 * The current API call ::mbg_get_ucap_event is faster
 * and provides capture events as ::PCPS_HR_TIME structure,
 * but some very old devices may not support the newer API.
 *
 * @param[in]  ucap  The capture event read from a device.
 *
 * @see ::mbg_get_ucap_event
 * @see ::mbg_get_gps_ucap
 * @see ::show_ucap_event
 */
void show_gps_ucap( const TTM *ucap )
{
  printf( "New capture: CH%i: %04i-%02i-%02i  %2i:%02i:%02i.%07li",
          ucap->channel,
          ucap->tm.year,
          ucap->tm.month,
          ucap->tm.mday,
          ucap->tm.hour,
          ucap->tm.min,
          ucap->tm.sec,
          (ulong) ucap->tm.frac
        );

  if ( raw )
    printf( ", st: 0x%04lX", (ulong) ucap->tm.status );

  printf( "\n" );

}  // show_gps_ucap



static /*HDR*/
/**
 * @brief Check the modes of a serial port of a device.
 *
 * If one of the serial ports of the device has been
 * configured to send a serial string automatically,
 * this is usually to send a capture event string
 * automatically as soon as an event has occurred.
 *
 * As a consequence, capture events are removed from
 * the on-board FIFO and aren't available anymore when
 * an application tries to read capture events via the
 * PCI bus, using the API functions ::mbg_get_ucap_event
 * or ::mbg_get_gps_ucap.
 *
 * So we check and print a warning if this may be the case.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @see ::mbg_get_ucap_event
 * @see ::mbg_get_gps_ucap
 */
void check_serial_mode( MBG_DEV_HANDLE dh )
{
  RECEIVER_INFO ri;
  RECEIVER_PORT_CFG rpcfg;
  int i;
  int must_warn = 0;

  int rc = mbg_setup_receiver_info( dh, NULL, &ri );

  if ( mbg_cond_err_msg( rc, "mbg_setup_receiver_info" ) )
    return;

  if ( ri.n_com_ports == 0 )   // The device provides no serial port,
    return;                    // so there's nothing to check.

  // Read the current serial port settings from the device.
  rc = mbg_get_serial_settings( dh, &rpcfg, &ri );

  if ( mbg_cond_err_msg( rc, "mbg_get_serial_settings" ) )
    return;


  // Check if one of the serial ports uses STR_AUTO mode.
  for ( i = 0; i < ri.n_com_ports; i++ )
  {
    PORT_SETTINGS *p = &rpcfg.pii[i].port_info.port_settings;

    if ( p->mode == STR_AUTO )
    {
      must_warn = 1;
      break;
    }
  }

  if ( must_warn )
    fprintf( stderr,
             "ATTENTION: At least one of the serial ports of the device has been\n"
             "configured to send time strings automatically, which can remove\n"
             "capture events from the on-board FIFO, so they are unavailable\n"
             "to this application\n"
             "\n"
             "Please change the configuration of the serial port(s) in a way that\n"
             "none of the serial ports sends a capture event string automatically.\n"
             "\n"
             "For example, use the \"mbgctrl\" program to let the serial ports\n"
             "transmit the Meinberg standard time string once per second.\n"
             "\n"
             "See \"mbgctrl -?\" for details.\n"
             "\n" );

}  // check_serial_mode



static /*HDR*/
void check_clear_buffer( MBG_DEV_HANDLE dh, PCPS_UCAP_ENTRIES *p_ucap_entries )
{
  int rc;

  printf( "\nClearing the on-board FIFO buffer" );

  rc = mbg_chk_dev_can_clr_ucap_buff( dh );

  if ( mbg_rc_is_success( rc ) )
  {
    printf( ".\n" );
    mbg_clr_ucap_buff( dh );
  }
  else
    printf( ": %s\n", mbg_strerror( rc ) );

  if ( p_ucap_entries )
    show_ucap_entries( dh, p_ucap_entries, NULL, " remaining.\n" );

}  // check_clear_buffer



static /*HDR*/
int do_mbggpscap( MBG_DEV_HANDLE dh )
{
  int rc;
  bool dev_has_ucap = mbg_rc_is_success( mbg_chk_dev_has_ucap( dh ) );
  bool dev_is_gps = mbg_rc_is_success( mbg_chk_dev_is_gps( dh ) );

  must_check_intv = continuous && ( nom_cap_intv != 0 );
  jitter_exceed_cnt = 0;
  cap_overrun_cnt = 0;
  buf_overrun_cnt = 0;
  has_been_called = 0;

  if ( !( dev_has_ucap || dev_is_gps ) )
  {
    printf( "This device type does not provide time capture inputs.\n" );
    return 0;
  }

  check_serial_mode( dh );

  printf( "Be sure the device has been properly configured to enable capture inputs.\n\n" );

  // There's an older API which has been introduced with the first GPS devices
  // and uses the TTM structure to return the capture events. However, that
  // API call is slow and not very flexible, so a new set of API calls has been
  // introduced to handle capture events.
  // The new API calls are supported by all newer devices that provide
  // user capture inputs, and also for older devices with a firmware update.

  if ( !force_old_api && dev_has_ucap )  // Use the new API.
  {
    PCPS_UCAP_ENTRIES ucap_entries;
    struct timespec prv_t = { 0 };
    int64_t prv_events = 0;
    int64_t events = 0;

    // The new API provides the following functions:
    //   mbg_clr_ucap_buff()     Clear the on-board FIFO buffer.
    //   mbg_get_ucap_entries()  Get the max number of FIFO entries, and the current number of entries.
    //   mbg_get_ucap_event()    Read one entry from the FIFO.

    show_ucap_entries( dh, &ucap_entries, "On-board FIFO: ", " used.\n" );

    if ( clear_buffer )
      check_clear_buffer( dh, &ucap_entries );

    // If the program is not to run continuously, and no
    // capture events are available, we're done.
    if ( query_only || ( !continuous && ucap_entries.used == 0 ) )
      return 0;

    printf( "\n" );

    clock_gettime( CLOCK_MONOTONIC, &prv_t );

    printf( ( ucap_entries.used == 0 ) ?
            "Waiting for capture events:\n" :
            "Reading capture events:\n"
          );

    if ( must_check_intv )
      if ( max_cap_jitter == 0.0 )
        max_cap_jitter = 0.5e-6;

    // Now read out all events from the FIFO and wait
    // for new events if the FIFO is empty.
    for (;;)
    {
      PCPS_HR_TIME ucap_event;

      rc = mbg_get_ucap_event( dh, &ucap_event );

      if ( mbg_cond_err_msg( rc, "mbg_get_ucap_event" ) )
        break;  // an error has occurred

      // If a time capture event has been read, it has
      // been removed from the FIFO buffer onboard device.

      // If the timestamp is not 0, a new capture event
      // has been retrieved.
      if ( ucap_event.tstamp.sec || ucap_event.tstamp.frac )
      {
        events++;

        if ( ucap_event.status & PCPS_UCAP_OVERRUN )
          cap_overrun_cnt++;

        if ( ucap_event.status & PCPS_UCAP_BUFFER_FULL )
          buf_overrun_cnt++;

        if ( must_check_intv )
          check_ucap_intv( &ucap_event );

        if ( !query_continuously )
          show_ucap_event( &ucap_event );
      }

      if ( query_continuously )
      {
        struct timespec t;

        clock_gettime( CLOCK_MONOTONIC, &t );

        if ( t.tv_sec != prv_t.tv_sec )  // Second has changed.
        {
          double d = delta_timespec_d_s( &t, &prv_t );

          printf( "%li evts/s, %" PRIu64 " buf ovr, %" PRIu64 " cap ovr, ",
                  (long) ( ( events - prv_events ) / d ),
                  buf_overrun_cnt, cap_overrun_cnt );

          if ( must_check_intv )
            printf( " %" PRIu64 " jitter ovr., ", jitter_exceed_cnt );

          show_ucap_entries( dh, &ucap_entries, NULL, NULL );

          prv_t = t;
          prv_events = events;
        }
      }

      if ( ucap_event.tstamp.sec == 0 && ucap_event.tstamp.frac == 0 )
        if ( sleep_usecs )
          usleep( sleep_usecs );   // Sleep, then try again.
    }
  }
  else    // Use the old API.
  {
    if ( clear_buffer )
      printf( "Clearing the buffer is not supported by the old API.\n" );

    printf( "Checking for capture events using the old API:\n" );

    for (;;)
    {
      TTM ucap_ttm;

      rc = mbg_get_gps_ucap( dh, &ucap_ttm );

      if ( mbg_cond_err_msg( rc, "mbg_get_gps_ucap" ) )
        break;  // An error has occurred.


      // If a user capture event has been read, it has
      // been removed from the FIFO buffer of the device.

      // If a new capture event has been available,
      // ucap.tm contains a timestamp.
      if ( _pcps_time_is_read( &ucap_ttm.tm ) )
        show_gps_ucap( &ucap_ttm );

      if ( !continuous )
      {
        printf( "No capture event available!\n" );
        break;
      }

      if ( sleep_usecs )
        usleep( sleep_usecs );   // Sleep, then try again.
    }
  }

  return 0;

}  // do_mbggpscap

static MBG_DEV_HANDLER_FNC do_mbggpscap;



static /*HDR*/
void usage( void )
{
  mbg_print_usage_intro( pname, true,
    "This example program reads time capture events from a card.\n"
    "This works only with devices that provide time capture inputs."
  );
  mbg_print_help_options();
  mbg_print_opt_info( "-c", "Run continuously" );
  mbg_print_opt_info( "-u num", "Sleep num microseconds if buffer empty, default %i us (implies -c)", DEFAULT_USLEEP_INTV );
  mbg_print_opt_info( "-C", "Clear capture buffer" );
  mbg_print_opt_info( "-i val", "Check interval between captures events [s]" );
  mbg_print_opt_info( "-j val", "Max. allowed jitter of capture interval [s]" );
  mbg_print_opt_info( "-q", "Query FIFO buffer status only" );
  mbg_print_opt_info( "-Q", "Query continuously once per second, read events as fast as possible" );
  mbg_print_opt_info( "-r", "Show raw (hex) timestamp and status)" );
  mbg_print_opt_info( "-o", "Force usage of old API (for testing only)" );
  mbg_print_usage_outro( DEV_OPT_PRINT_BUS_LEVEL, false );

}  // usage



int main( int argc, char *argv[] )
{
  int rc;
  int c;

  mbg_print_program_info( pname, MBG_FIRST_COPYRIGHT_YEAR, MBG_LAST_COPYRIGHT_YEAR );

  // Check command line parameters.
  while ( ( c = getopt( argc, argv, "cCi:j:l:oqQru:h?" ) ) != -1 )
  {
    switch ( c )
    {
      case 'c':
        continuous = 1;
        break;

      case 'C':
        clear_buffer = 1;
        break;

      case 'i':
        nom_cap_intv = atof( optarg );
        break;

      case 'j':
        max_cap_jitter = atof( optarg );
        break;

      case 'l':
        log_fn = optarg;
        break;

      case 'o':
        force_old_api = 1;
        break;

      case 'q':
        query_only = 1;
        break;

      case 'Q':
        query_continuously = 1;
        continuous = 1;
        break;

      case 'r':
        raw = 1;
        break;

      case 'u':
        sleep_usecs = atoi( optarg );
        continuous = 1;
        break;

      case 'h':
      case '?':
      default:
        must_print_usage = true;
    }
  }

  if ( must_print_usage )
  {
    usage();
    return MBG_EXIT_CODE_USAGE;
  }

  if ( nom_cap_intv != 0.0 )
  {
    printf( "Nominal capture interval: %f s\n", nom_cap_intv );
    printf( "Maximum allowed capture jitter: %f s\n", max_cap_jitter );
  }

  // Handle each of the specified devices.
  rc = mbg_handle_devices( argc, argv, optind, do_mbggpscap, 0 );

  return mbg_rc_is_success( rc ) ? MBG_EXIT_CODE_SUCCESS : MBG_EXIT_CODE_FAIL;
}
