/*****************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 *****************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 *****************************************************************************
 *
 *                            Copyright (c) 2021
 *
 *****************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_MSGX
#define INC_MSGX

#include "basictypes.h"

// Define INC_EXT_MSGX as a header file to include alternate
// extraction functions/macros from there.
#ifdef INC_EXT_MSGX
#  include INC_EXT_MSGX
#endif

// 16 Bit Net 2 Host conversion & vice versa
#ifndef N2H_16
#define N2H_16(msg) \
    (((uint16_t) *((const uint8_t *)&msg) << 8) |  \
     ((uint16_t) *((const uint8_t *)&msg + 1)))
#define H2N_16 N2H_16
#endif


// 32 Bit Net 2 Host conversion & vice versa
#ifndef N2H_32
#define N2H_32(msg) \
    (( (uint32_t) N2H_16(msg) << 16) |  \
     ( (uint32_t) N2H_16(msg + 2) ))
#define H2N_32 N2H_32
#endif

// 64 Bit Net 2 Host conversion & vice versa
#ifndef N2H_64
#define N2H_64(msg) \
    (( (uint64_t) N2H_32(msg) << 32) |  \
     ( (uint64_t) N2H_32(msg + 4) ))
#define H2N_64 N2H_64
#endif

#ifndef BYTEX
#define BYTEX(i, nr) \
  (uint8_t) ( ( ((uint32_t) (i)) >> ((nr) << 3)) & 0xFF)
#endif

#endif
