/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_PTPTIME
#define INC_PTPTIME

#include "basictypes.h"
#include "msgx.h"

#pragma pack(1)

/// PTP Timestamp

/// Extended timestamp structure
///
struct PTP_Timestamp : public Timestamp_t {

  /// Convert to and from network byte order
  void convert_nw() ;
  /// Default Constructor
  PTP_Timestamp() { sec_h = 0; sec_l = 0; nsec = 0; }
  /// Alternate Constructor
  PTP_Timestamp(uint16_t sh, uint32_t sl, uint32_t ns) {
    sec_h = sh;
    sec_l = sl;
    nsec = ns;
  }
  /// Copy Constructor
  PTP_Timestamp(const Timestamp_t &v) {
    sec_h = v.sec_h;
    sec_l = v.sec_l;
    nsec = v.nsec;
  }
  /// Copy Assignment Constructor
  PTP_Timestamp& operator=(Timestamp_t &rhs) {
    sec_h = rhs.sec_h;
    sec_l = rhs.sec_l;
    nsec = rhs.nsec;
    return *this;
  }
  /// Valid if at least one member not zero
  bool valid() { return sec_h || sec_l || nsec; }
  // Equalitiy
  bool operator==(const Timestamp_t &rhs) { return sec_h == rhs.sec_h &&
                                                   sec_l == rhs.sec_l &&
                                                   nsec  == rhs.nsec; }
  /// Comparison
  bool operator>(const Timestamp_t &rhs) {

    if(sec_h > rhs.sec_h) return true;

    if(sec_h == rhs.sec_h &&
       sec_l > rhs.sec_l) return true;

    if(sec_h == rhs.sec_h &&
       sec_l == rhs.sec_l &&
       nsec  > rhs.nsec) return true;

    return false;
  }
  bool operator<(const Timestamp_t &rhs) {
    return !(*this > rhs || *this == rhs);
  }

  // Convert to nanoseconds
  int64_t toNs();

}__PACKME__;

/// PTP Time Interval

/// Extended time interval structure
///
struct PTP_TimeInterval : public TimeInterval_t {

  /// Convert to and from network byte order
  void convert_nw() {
    scaledNs = (int64_t)N2H_64(scaledNs);
  }
  /// Default Constructor
  PTP_TimeInterval() { scaledNs = 0; }
  /// Alternate Constructor
  PTP_TimeInterval(const int64_t v) { scaledNs = v; }
  /// Copy Constructor
  PTP_TimeInterval(const TimeInterval_t &v) { scaledNs = v.scaledNs; }
  /// Extraction of nanaseconds part
  int32_t nsec() const;
  /// Extraction os sub nanosecond part for printing
  uint16_t subNs();
}__PACKME__;

/// Adding time interval \a rhs to time interval \a lhs
const PTP_TimeInterval operator +(const PTP_TimeInterval &lhs,
                                         const PTP_TimeInterval &rhs);
/// Subtracting time interval \a rhs from time interval \a lhs
const PTP_TimeInterval operator -(const PTP_TimeInterval &lhs,
                                         const PTP_TimeInterval &rhs);

/// Adding time interval \a rhs to timestamp \a lhs
const PTP_Timestamp operator +(const PTP_Timestamp &lhs,
                                         const PTP_TimeInterval &rhs);

/// Subtracting time interval \a rhs to timestamp \a lhs
const PTP_Timestamp operator -(const PTP_Timestamp &lhs,
                                         const PTP_TimeInterval &rhs);

/// Subtracting timestamp \a rhs from timestamp \a lhs
const PTP_TimeInterval operator -(const PTP_Timestamp &lhs,
                                         const PTP_Timestamp &rhs);
/// Shifting timestamp \a lhs by \a n
const PTP_TimeInterval operator >>(const PTP_TimeInterval &lhs,
                                         const int n);

#pragma pack()

#endif
