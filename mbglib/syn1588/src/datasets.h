/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/// @defgroup datasets "PTP Datasets"
/// @brief Datasets used internally by the PTP library

#ifndef INC_DATASETS
#define INC_DATASETS

#include "basictypes.h"

#ifndef ALT_TIME_OFF_MAX_KEY
#define ALT_TIME_OFF_MAX_KEY 3
#endif

// Set the maximum length of the path trace list
#ifndef PATH_TRACE_LIST_LEN
#define PATH_TRACE_LIST_LEN 8
#endif
#ifndef PTP_UC_NUM_CONN
#define PTP_UC_NUM_CONN 512
#endif

#define CLOCKID_LEN 8

#ifndef ACC_MASTER_MAX_SIZE
#define ACC_MASTER_MAX_SIZE 5
#endif

#ifdef __cplusplus

#include "datatypes.h"
#include "ptptime.h"

#else
// For C only programs, we have to define a few
// datatypes here instead of getting them from
// ptptime.h and datatypes.h. The basic types are
// defined in basictypes.h

#pragma pack(1)

typedef struct ClockQuality_t PTP_ClockQuality;

typedef struct TimeInterval_t PTP_TimeInterval;

typedef struct PortId_t PTP_PortId;

#pragma pack()

#endif

/******************************************************************************
 * Datasets
 *****************************************************************************/
/// @file datasets.h This file defines all datasets described in IEEE1588-2008 chapter 8. They
/// are used for characterizing a PTP port/clock.
/// @note Datasets are also in a packed structure format to be able to use the
/// format within management messages.

#pragma pack(1)

/// @brief PTP Default dataset
///
/// Default Dataset as defined in IEEE1588-2008 chapter 8.2.1. Please refer
/// to the standard for a detailed description of the attributes.
/// @ingroup datasets
struct DefaultDataset_t {

  uint8_t          flags;              ///< Flags field for TwoStep and SlaveOnly
  uint8_t          res1;               ///< reserved
  uint16_t         numPorts;           ///< Number of Ports
  uint8_t          prio1;              ///< Clock Priority 1
  PTP_ClockQuality clkQual;            ///< Clock Quality
  uint8_t          prio2;              ///< Clock Priority 2
  uint8_t          clkId[CLOCKID_LEN]; ///< Clock Id
  uint8_t          domain;             ///< Domain
  SdoId            sdoId;              ///< Standart Development Organisation Id ,PTPv2.1
  uint8_t          res2;               ///< reserved

} __PACKME__;


/// @brief PTP Current Dataset
///
/// Current Dataset as defined in IEEE1588-2008 chapter 8.2.2.  Please refer
/// to the standard for a detailed description of the attributes.
/// @ingroup datasets
struct CurrentDataset_t {

  uint16_t         stepsRemoved;     ///< Steps removed from grandmaster
  PTP_TimeInterval offsetFromMaster; ///< Offset from Master
  PTP_TimeInterval meanPathDelay;    ///< Mean Path Delay

} __PACKME__;


/// @brief PTP Parent Dataset
///
/// Parent Dataset as defined in IEEE1588-2008 chapter 8.2.3.  Please refer
/// to the standard for a detailed description of the attributes.
/// @ingroup datasets
struct ParentDataset_t {

  PTP_PortId       portId;               ///< Parent Port Id
  uint8_t          flags;                ///< Flags for Enable parent statistics
  uint8_t          res;                  ///< reserved
  uint16_t         obsVariance;          ///< Observed parent offset scaled log variance
  int32_t          obsPhaseChangeRate;   ///< Observed parent clock phase change rate
  uint8_t          gmPrio1;              ///< Grandmaster priority 1
  PTP_ClockQuality gmClkQual;            ///< Grandmaster clock Quality
  uint8_t          gmPrio2;              ///< Grandmaster priority 2
  uint8_t          gmClkId[CLOCKID_LEN]; ///< Grandmaster clock Id

} __PACKME__;


/// @brief PTP Time Properties Dataset
///
/// Time Properties Dataset as defined in IEEE1588-2008 chapter 8.2.4.
/// Please refer to the standard for a detailed description of the attributes.
/// @ingroup datasets
struct TimePropDataset_t {

  int16_t currUTCOffs; ///< Current UTC offset
  uint8_t flags;       ///< Combined flags field
  uint8_t timeSrc;     ///< Time Source

} __PACKME__;


/// @brief PTP Port Dataset
///
/// Port Dataset as defined in IEEE1588-2008 chapter 8.2.5. Please refer
/// to the standard for a detailed description of the attributes.
/// @ingroup datasets
struct PortDataset_t {

  PTP_PortId       Id;                ///< Port Id
  PTP_State        state;             ///< Port state
  int8_t           dlyReqInterval;    ///< Log min mean Delay Request interval
  PTP_TimeInterval peerPathDelay;     ///< Peer mean path delay
  int8_t           announceInterval;  ///< Log mean Announce interval
  uint8_t          announceRecptTout; ///< Announce receipt timeout
  int8_t           syncInterval;      ///< Log mean Sync interval
  uint8_t          dlyMechanism;      ///< Delay mechanism (E2E 0x01, P2P 0x02)
  int8_t           pdlyReqInterval;   ///< Log min mean PDelay Request interval
  uint8_t          version;           ///< Version number
  uint8_t          minorVersion;      ///< Minor version number
  PTP_TimeInterval delayAsymmetry;    ///< Delay asymmetry PTPv2.1
  bool             portEnable;        ///< Port enable PTPv2.1
  bool             masterOnly;        ///< Master only PTPv2.1

} __PACKME__;


/******************************************************************************
 * Other required data structures for PTPv2
 *****************************************************************************/

#ifdef _PATH_TRACE_
/// @brief Path Trace List
///
/// This structure holds the list for the path trace feature described in
/// IEEE1588-2008 16.2. This list contains a sequence of clock IDs indicating
/// the path through a (PTP) network. If enabled, a TLV containing the list
/// is appended to each Announce message.
/// @ingroup datasets
struct PathTraceList_t {
  /// Current length of path trace list
  uint16_t len;
  /// List of clockIDs for path trace feature
  uint8_t pathSequence[PATH_TRACE_LIST_LEN][CLOCKID_LEN];

} __PACKME__;
#endif

#ifdef ALT_TIME_OFFSET
/// @brief Alternate Time Offset Properties
///
/// This structure holds all necessary data for Alternate Time
/// Offsets as defined in IEEE1588-2008 chapter 16.3. A grandmaster
/// may indicate the offset of an alternate time from its node time
/// by transmitting an ALTERNATE_TIME_OFFSET_INDICATOR TLV. This structure
/// offers the required datastructure.
/// @ingroup datasets
struct AltTimeOffsetProp_t {

  uint8_t enabled;           ///< Enables (1)/disables(0) the timescale
  int32_t currentOffset;     ///< Offset of the alternate time in seconds
  int32_t jumpSeconds;       ///< Seize of next disconinuity
  uint8_t timeOfNextJump[6]; ///< Value in seconds when discont. will occur
  uint8_t displayName[11];   ///< Text name of the timescale

} __PACKME__;
#endif

#ifdef _C37_238_2011_PROFILE_
/// @brief C37_238_2011 Profile data structure
///
/// This structure holds all the necessary configuration data required only
/// for the power profile (C37.238-2011). This structure is only used for internal
/// storage of the data. The data is distributed through a network by using
/// the dedicated organization extension TLVs defined in the C37.238-2011 power profile.
/// @ingroup datasets
struct PowerProfileData_t {

  uint16_t gmId;             ///< GrandmasterID, valid range is 0x0003-0x00FE
  uint32_t gmTimeInaccuracy; ///< Grandmaster time inaccuracy
  uint32_t nwTimeInaccuracy; ///< Network time inaccuracy

} __PACKME__;
#endif

#ifdef _C37_238_2017_PROFILE_
/// @brief C37_238_2017 Profile data structure
///
/// This structure holds all the necessary configuration data required only
/// for the power profile (C37.238-2017). This structure is only used for internal
/// storage of the data. The data is distributed through a network by using
/// the dedicated organization extension TLVs defined in the C37.238-2017 power profile.
/// @ingroup datasets
struct C37_238ProfileData_t {

  uint16_t gmId;                ///< GrandmasterID, valid range is 0x0000-0xFFFF
  uint32_t totalTimeInaccuracy; ///< total time inaccuracy (= gmTimeInaccuracy + nwTimeInaccuracy)

} __PACKME__;
#endif


#if defined(_G8275_1_PROFILE_) || defined(_G8275_2_PROFILE_)
/// @brief Telecom Profile G8275_1 and G8275.2 data structure
///
/// This structure holds all the necessary dataset members defined in the
/// telecom profile (G.8275.1/Y.1367.1 or G8275.2). Even though the profile
/// defines that the existing datasets (default Ds and port Ds) are enhanced,
/// we use a seperate data structure for easier internal data handling.
/// @ingroup datasets
struct G8275_1_2ProfileData_t {

  /// Master Only
  ///
  /// The alternate BMCA of G.8275.1 considers the per-port Boolean attribute
  /// masterOnly. If masterOnly is TRUE, the port is never placed in the SLAVE
  /// state. If masterOnly is FALSE, the port can be placed in the SLAVE state.
  /// The value may be updated during runtime.
  bool    portMasterOnly;
  ///  Port Local Priority
  ///
  /// The per-port attribute localPriority is assigned to each port r of a
  /// clock and is used in the determination of Erbest and Ebest. This
  /// attribute is used as a tie-breaker in the dataset comparison algorithm,
  /// in the event that all other previous attributes of the datasets being
  /// compared are equal. Range is {1 - 255}. The default value is 128.
  /// The value may be updated during runtime.
  uint8_t portLocalPriority;
  /// Local Priority
  ///
  /// The attribute localPriority is assigned to the local clock, to be used
  /// if needed when the data associated with the local clock, D0, is compared
  /// with data on another potential grandmaster received via an Announce
  /// message. Range is {1 - 255}. The default value is 128.
  uint8_t defaultLocalPriority;
} __PACKME__;
#endif

#ifdef _SMPTE_PROFILE_
/// @brief Synchronization Metadata
///
/// This stucture holds the synchronization metadata required
/// for the SMPTE profile.
/// This structure is only used for internal storage of the data. The data is
/// distributed through a network by using management messages and
/// the dedicated organization extension TLV defined in the SMPTE profile.
/// @ingroup datasets
struct SyncMetadata_t {
  /// Default system frame rate
  /// Default video frame rate of the slave system as a lowest term rational.
  /// The data type shall be composed of a pair of unsigned Int32 values coded
  /// in big-endian form where the first shall be the numerator and the second
  /// shall be the denominator. The denominator shall be the smallest value
  /// that represents the frame rate denominator
  /// For example, 29.97 Hz: (30000/1001) or 25 Hz: (25/1).
  uint32_t defaultSystemFrameRateNum;
  uint32_t defaultSystemFrameRateDenum;
  /// Master locking status
  /// Complementary information to clockClass (0: Not in use, 1: Free Run,
  /// 2: Cold Locking, 3: Warm Locking, 4: Locked)
  uint8_t masterLockingStatus;
  /// Time Address Flags
  /// Indicates the intended ST 12-1 flags.
  /// Bit 0: Drop frame (0: Non-drop-frame, 1: Drop-frame)
  /// Bit 1: Color Frame Identification (0: Not in use, 1: In use)
  /// Bits 2-7:  Reserved
  uint8_t timeAddressFlags;
  /// Current local offset
  /// Offset in seconds of Local Time from grandmaster PTP time. For example,
  /// if Local Time is Eastern Standard Time (North America) UTC-5 and the
  /// number of leap seconds is 35, the value will be -18035 (decimal).
  int32_t currentLocalOffset;
  /// Jump seconds
  /// The size of the next discontinuity, in seconds, of Local Time. A value
  /// of zero indicates that no discontinuity is expected. A positive value
  /// indicates that the discontinuity will cause
  /// the currentLocalOffset to increase.
  int32_t jumpSeconds;
  /// Time of next jump
  /// The value of the seconds portion of the grandmastermaster PTP time
  /// at the time that the next discont. of the currentLocalOffset will occur.
  /// The discontinuity occurs at the start of the second indicated
  uint8_t timeOfNextJump[6];
  /// Time of next jam
  /// The value of the seconds portion of the PTP time corresponding to
  /// the next scheduled occurrence of the Daily Jam.
  /// If no Daily Jam is scheduled, the value of timeOfNextJam shall be zero.
  uint8_t timeOfNextJam[6];
  /// Time of previous jam
  /// The value of the seconds portion of the PTP time corresponding to the
  /// previous occurrence of the Daily Jam.
  uint8_t timeOfPreviousJam[6];
  /// Previous jam local offset
  /// The value of currentLocalOffset at the previous daily jam time.
  /// If a discontinuity of Local Time occurs at the jam time, this parameter
  /// reflects the offset after the discontinuity.
  int32_t previousJamLocalOffset;
  /// Daylight saving
  /// Bit 0:  Current Daylight Saving (0: Not in effect, 1: In effect)
  /// Bit 1: Daylight Saving at next discontinuity
  ///        (0: Not in effect, 1: In effect)
  /// Bit 2: Daylight Saving at previous daily jam time
  ///        (0: Not in effect, 1: In effect)
  /// Bits 3-7: Reserved
  uint8_t daylightSaving;
  /// Leap second jump
  /// The reason for the forthcoming discontinuity of currentLocalOffset
  /// indicated by timeOfNextJump
  /// Bit 0:
  ///   0: Other than a change in the number of leap seconds (default)
  ///   1: A change in number of leap seconds
  /// Bits 1-7: Reserved
  uint8_t leapSecondJump;
} __PACKME__;
#endif

#ifdef _802_1AS_PROFILE_
/// @brief 802.1as data structure
///
/// This structure contains all required data for 802.1as. It is shared by
/// all ports of a boundary clock (802.1as bridge).
/// @ingroup datasets
struct Ieee802_1asData_t {

  /// Scaled nanoseconds structure (96 bit integer).
  /// This format is only used by 802.1as.
  struct ScaledNs_t {
    int32_t ns_h;
    int64_t ns_l;
  } __PACKME__;

  /// cumulativeScaledRateOffset
  /// Indicates the ratio of the frequency of the clockSource/GM to the
  /// frequency of the local clock.
  /// Format: cumulativeScaledRateOffset = (rateRatio * 1.0) * (2^41)
  int32_t cumulativeScaledRateOffset;
  /// gmTimeBaseIndicator
  /// Used to identify the time base.
  /// If this clock acts as GM, it has to be managed via the IO
  /// interface (shared mem) and is equivalent to clockSourceTimeBaseIndicator
  /// (See clockSourceTimeBaseIndicator in 10.2.3.8 and gmTimeBaseIndicator in
  /// 10.2.3.15)
  uint16_t gmTimeBaseIndicator;
  /// lastGmPhaseChange
  /// Indicates the last phase change of the clockSource/GM.
  /// Is changed whenever the time base changes.
  /// (See clockSourceLastGmPhaseChange in 10.2.3.10 and
  ///  lastGmPhaseChange 10.2.3.16)
  ScaledNs_t lastGmPhaseChange;
  /// lastGmFreqChange
  /// Indicates the fractional frequency offset of the current clockSource/GM
  /// to the last clockSource/GM.
  /// It changes whenever the time base changes.
  /// (See clockSourceLastGmFreqChange in 10.2.3.11 and lastGmFreqChange in
  /// 10.2.3.17).
  /// lastGmFreqChange = rateRatio * 2^41
  int32_t lastGmFreqChange;

} __PACKME__;

/// @brief 802.1as port data structure
///
/// This structure contains additional data for 802.1as. It is managed on a per
/// port basis.
/// @ingroup datasets
struct Ieee802_1asPortData_t {

  /// asCapable
  /// A Boolean that is TRUE if and only if it is determined that
  /// this time-aware system and the time - aware system at the other end of
  /// the link attached to this port can interoperate with each other via
  /// the IEEE 802.1AS protocol. This means that
  ///   a) this time-aware system is capable of
  ///      executing the IEEE 802.1AS protocol,
  ///   b) the time-aware system at the other end of the link is capable of
  ///      executing the IEEE 802.1AS protocol, and
  ///   c) there are no non-IEEE-802.1AS systems in between
  ///      this time-aware system and the time-aware system at the other end of
  ///      the link that introduce sufficient impairments that the end-to-end
  ///      time synchronization performance of B.3 cannot be met.
  bool asCapable;
  /// asNeighbor
  /// PortId of the current asCapable neighbor.
  /// The Id is only valid if asCapable is true. As a slave,
  /// this Id is equal to the PortId of the current master in
  /// the parent dataset.
  PortId_t asNeighbor;
  /// neighborPropDelayThresh
  /// the propagation time threshold,
  /// above which a port is not considered capable of participating
  /// in the IEEE 802.1AS protocol.
  /// If neighborPropDelay(see 10.2.4.7) exceeds neighborPropDelayThresh,
  /// then asCapable(see 11.2.12.4) is set to FALSE.
  /// The data type for neighborPropDelayThresh is UScaledNs.
  TimeInterval_t neighborPropDelayThresh;
  /// neighborRateRatio
  /// Current rate ratio to the port's neighbor
  /// in scaled ratio (rateRatio * 2^41)
  int32_t neighborRateRatio;
  /// lastGmPhaseChange
  /// Current phase difference (offset) to
  /// the current GM (in slave and passive state).
  /// It will be used as lastGmPhaseChange after this clock becomes GM.
  int64_t lastGmPhaseChange;
  /// lastGmFreqChange
  /// Current frequency rate ratio to
  /// the current GM (in slave and passive state).
  /// It will be used as lastGmFreqChange after this clock becomes GM.
  int32_t lastGmFreqChange;
};
#endif

/// @brief Extended Parent Dataset
///
/// This datastructure is used for keeping needful information about the
/// current Master/Grandmaster. It may be used for application specific purpose
/// as well as for improved application behavior. However, this is no official
/// part of IEEE1588 but may still necessary for certain applications.
/// @ingroup datasets
struct ExtParentDataset_t {
  /// Sync interval of current master (will be adopted in multicast master mode)
  int8_t           logSyncIval;
  /// Announce interval of current master (will be adopted in multicast master mode)
  int8_t           logAnnIval;
  /// minimum delay request interval of current master (will be adopted in multicast master mode)
  int8_t           logDlyReqIval;
  /// Port address of current Master
  PortAddress_t    pa;
  /// ClkID of current GM, even in Passive state
  uint8_t          gmClkId[CLOCKID_LEN];
  /// Priority 1 of current GM, even in Passive state
  uint8_t          gmPrio1;
  /// Clock Quality of current GM, even in Passive state
  PTP_ClockQuality gmClkQual;
  /// Priority 2 of current GM, even in Passive state
  uint8_t          gmPrio2;
} __PACKME__;

/// @brief Acceptable Master Table Dataset
///
/// As defined in IEEE1588-2017 Draft,
/// this structure holds the max tableSize, actual tableSize,
/// as well as a list of acceptable masters.
/// @ingroup datasets
struct AcceptableMasterTableDataset_t {
  uint16_t           maxTableSize;    ///< max allowed number of acceptable masters
  uint16_t           actualTableSize; ///< actual number of acceptable masters
  AcceptableMaster_t list[ACC_MASTER_MAX_SIZE]; ///< list of configured acceptable masters
} __PACKME__;

/// @brief Acceptable Master Port Dataset
///
/// As defined in IEEE1588-2017 Draft,
/// holds per port enable flag for Acceptable Master table
/// @ingroup datasets
struct AcceptableMasterPortDataset_t {
  bool enable;  ///< enable the use of the acceptable master dataset for a port
} __PACKME__;


#ifdef STATISTICS_OUTPUT
/// @brief Packet Counter
///
/// This structure contains packet counter for all individual packet types.
/// There are seperate counters for RX and TX packets.
/// @ingroup datasets

struct MessageCounter_t {
  uint32_t status; ///< status word flags (use PacketCounterStat_e)
  uint32_t rx;     ///< overall Rx packet counter
  uint32_t tx;     ///< overall Tx packet cntr

  /// @brief invalid Rx packet counter
  ///
  /// Indicates one of the following issues:<br>
  /// * wrong PTP version, wrong domain number,<br>
  /// * message from self, message from other BC port,<br>
  /// * multicast message in unicast mode<br>
  /// * or message extraction error (size error or inconsistent format).
  uint32_t errorRx;
  uint32_t announceMsgRx;        ///< Accepted Announce message Rx cntr
  uint32_t announceMsgTx;        ///< Announce message Tx cntr
  uint32_t syncMsgRx;            ///< Accepted Sync message Rx cntr
  uint32_t syncMsgTx;            ///< Sync message Tx cntr
  uint32_t followUpMsgRx;        ///< Accepted Follow-up message Rx cntr
  uint32_t followUpMsgTx;        ///< Follow-up message Tx cntr
#ifndef REDUCED_MESSAGE_COUNTERS
  uint32_t dlyReqMsgRx;          ///< Accepted Delay request message Rx cntr
  uint32_t dlyReqMsgTx;          ///< Delay request message Tx cntr
  uint32_t dlyRespMsgRx;         ///< Accepted Delay response message Rx cntr
  uint32_t dlyRespMsgTx;         ///< Delay response message Tx cntr
#endif
  uint32_t pDlyReqMsgRx;         ///< Accepted PDelay Request message Rx cntr
  uint32_t pDlyReqMsgTx;         ///< PDelay Request message Tx cntr
  uint32_t pDlyRespMsgRx;        ///< Accepted PDelay Response message Rx cntr
  uint32_t pDlyRespMsgTx;        ///< PDelay Response message Tx cntr
  uint32_t pDlyFollowUpRx;       ///< Accepted PDelay Follow-Up message Rx cntr
  uint32_t pDlyFollowUpTx;       ///< PDelay Follow-Up message Tx cntr
  uint32_t signallingRx;         ///< Accepted Signalling message Rx cntr
  uint32_t signallingTx;         ///< Signalling message Tx cntr
  uint32_t mgmtRx;               ///< Accepted Management message Rx cntr
  uint32_t mgmtTx;               ///< Management message Tx cntr
  uint32_t mgmtErr;              ///< Management error cntr (rx)
  uint32_t annReceptTout;        ///< Announce receipt timeout cnt
#ifdef _EXTENDED_MESSAGE_COUNTERS_
  /// @{
  /// @name extended message counters
  /// The following counters (msg/second counter) are represented in
  /// fixed point format. The lower 8 bit represent digits after the
  /// radix (comma), the higher 24 bits digits before it. That's a
  /// scaling factor of 1/(2^8). To display the number in decimal
  /// notation use:
  /// printf("%d.%d", rxPerSec >> 8, ((rxPerSec & 0xFF) * 100) >> 8)
  uint32_t rxPerSec;             ///< overall Rx msg/second
  uint32_t txPerSec;             ///< overall Tx msg/second
  uint32_t announceMsgRxPerSec;  ///< Accepted Announce msg Rx msg/second
  uint32_t announceMsgTxPerSec;  ///< Announce msg Tx msg/second
  uint32_t syncMsgRxPerSec;      ///< Accepted Sync msg Rx msg/second
  uint32_t syncMsgTxPerSec;      ///< Sync msg Tx msg/second
  uint32_t followUpMsgRxPerSec;  ///< Accepted Follow-up msg Rx msg/second
  uint32_t followUpMsgTxPerSec;  ///< Follow-up msg Tx msg/second
  uint32_t dlyReqMsgRxPerSec;    ///< Accepted Delay request msg Rx msg/second
  uint32_t dlyReqMsgTxPerSec;    ///< Delay request msg Tx msg/second
  uint32_t dlyRespMsgRxPerSec;   ///< Accepted Delay response msg Rx msg/second
  uint32_t dlyRespMsgTxPerSec;   ///< Delay response msg Tx msg/second
  uint32_t pDlyReqMsgRxPerSec;   ///< Accepted PDelay Request msg Rx msg/second
  uint32_t pDlyReqMsgTxPerSec;   ///< PDelay Request msg Tx msg/second
  uint32_t pDlyRespMsgRxPerSec;  ///< Accepted PDelay Response msg Rx msg/second
  uint32_t pDlyRespMsgTxPerSec;  ///< PDelay Response msg Tx msg/second
  uint32_t pDlyFollowUpRxPerSec; ///< Accepted PDelay Follow-Up msg Rx msg/sec.
  uint32_t pDlyFollowUpTxPerSec; ///< PDelay Follow-Up msg Tx msg/second
  uint32_t signallingRxPerSec;   ///< Accepted Signalling msg Rx msg/second
  uint32_t signallingTxPerSec;   ///< Signalling msg Tx msg/second
  uint32_t mgmtRxPerSec;         ///< Accepted Management msg Rx msg/second
  uint32_t mgmtTxPerSec;         ///< Management msg Tx msg/second
  /// @}
#endif
} __PACKME__;
#endif

/// @brief Unicast connection
///
/// This structure holds all essential information for a unicast connection
/// like the protocol address and the interval and the duration for the
/// different message types. If duration is 0, there is currently no active
/// connection for this message type. If \a portAddr.len is 0, the whole
/// connection is inactive.
/// @ingroup datasets
struct Connection_t {

  enum { MAnnc, MSync, MDRsp, MaxMsgType };
  /// Message Info structure
  struct MsgInfo_t {
    uint32_t duration;
    int8_t   ival;
  };
  PortAddress_t portAddr;     ///< address of the connection
  MsgInfo_t     msg[MaxMsgType];  ///< Info about the different message types
} __PACKME__;

#ifdef _UNICAST_INFO_DATA_
/// @brief Unicast information structure
///
/// This structure contains information about all current unicast connections.
/// It may be used for monitoring purposes in master as well as in slave mode.
/// It contains the current number of connections, message load in percent, a
/// timestamp (PTP time) of the last update as well as an array of all config.
/// unicast connections (protocol address, message intervals and duration).
/// If the duration field of a connection is zero, the connection is currently
/// inactive (request has been denied or the device doesn't respond).
/// The duration field of the unicast connections contains the number of
/// passed seconds since the last contact (unicast request via signalling
/// messages) with the a device if the connection is currently active.
/// The absolut time of the last contact can be
/// calculated by \a ts - \a connections[i].msg[t].duration.
/// @ingroup datasets
struct UnicastInfoData_t {
  uint16_t     numConnections; ///< Current number of unicast connections
  uint8_t      utilization;    ///< current resource utilization (msg/sec) in %
  uint8_t      dummy;          ///< dummy byte for alignment
  uint32_t     ts;             ///< timstamp in seconds (PTP epoch)
  Connection_t connections[PTP_UC_NUM_CONN];
} __PACKME__;
#endif

#ifdef _PTP_SLAVE_MONITORING_
struct SlaveRxSyncTimingData_t {
  uint16_t         sequenceId;
  PTP_Timestamp    syncOriginTS;
  PTP_TimeInterval totalCorrectionField;
  int32_t          scaledCumulativeRateOffset;
  PTP_Timestamp    syncEventIngressTS;
} __PACKME__;

struct SlaveRxSyncComputedData_t {
  uint16_t         sequenceId;
  PTP_TimeInterval offsetFromMaster;
  PTP_TimeInterval meanPathDly;
  int32_t          scaledNeighborRateRatio;
} __PACKME__;

struct SlaveTxEventTs_t {
  uint16_t         sequenceId;
  PTP_Timestamp    eventEgressTS;
} __PACKME__;

struct SlaveMonitoringPortDataset_t {
  uint16_t         slaveEventMonitoringEnable;
  uint8_t          slaveEventMonitoringEventsPerRxSyncTimingTLV;
  uint8_t          slaveEventMonitoringEventsPerRxSyncComputedTLV;
  uint8_t          slaveEventMonitoringEventsPerTxEventTimestampsTLV;
  uint8_t          slaveEventMonitoringTxEventType;
  uint8_t          slaveEventMonitoringRxSyncTimingEventMessageM;
  uint8_t          slaveEventMonitoringRxSyncComputedEventMessageM;
  uint8_t          slaveEventMonitoringTxEventTimestampsEventMessageM;
} __PACKME__;

#endif // _PTP_SLAVE_MONITORING_

#ifndef __cplusplus

// C requires these typedefs...
typedef struct DefaultDataset_t               DefaultDataset_t;
typedef struct CurrentDataset_t               CurrentDataset_t;
typedef struct ParentDataset_t                ParentDataset_t;
typedef struct TimePropDataset_t              TimePropDataset_t;
typedef struct PortDataset_t                  PortDataset_t;
typedef struct AcceptableMaster_t             AcceptableMaster_t;
typedef struct AcceptableMasterTableDataset_t AcceptableMasterTableDataset_t;
typedef struct AcceptableMasterPortDataset_t  AcceptableMasterPortDataset_t;
#ifdef _PATH_TRACE_
typedef struct PathTraceList_t                PathTraceList_t;
#endif
#ifdef ALT_TIME_OFFSET
typedef struct AltTimeOffsetProp_t            AltTimeOffsetProp_t;
#endif
#ifdef _C37_238_2011_PROFILE_
typedef struct PowerProfileData_t             PowerProfileData_t;
#endif
#ifdef _SMPTE_PROFILE_
typedef struct SyncMetadata_t                 SyncMetadata_t;
#endif
#ifdef _802_1AS_PROFILE_
typedef struct Ieee802_1asData_t              Ieee802_1asData_t;
#endif
typedef struct Ieee802_1asPortData_t          Ieee802_1asPortData_t;
typedef struct ExtParentDataset_t             ExtParentDataset_t;
#ifdef STATISTICS_OUTPUT
typedef struct MessageCounter_t               MessageCounter_t;
#endif
#ifdef _UNICAST_INFO_DATA_
typedef struct UnicastInfoData_t              UnicastInfoData_t;
#endif
#ifdef _PTP_SLAVE_MONITORING_
typedef struct SlaveRxSyncTimingData_t        SlaveRxSyncTimingData_t;
typedef struct SlaveRxSyncComputedData_t      SlaveRxSyncComputedData_t;
typedef struct SlaveTxEventTs_t               SlaveTxEventTs_t;
typedef struct SlaveMonitoringPortDataset_t   SlaveMonitoringPortDataset_t;
#endif

#endif

/// @brief PTPv2 Context
///
/// This structure is a collection of all PTPv2 Datasets and
/// all addtional datasets, plus two 8 bit values for status information and
/// a unique id for identifying the origin of the data.
/// It is used to share PTP related information to other applications by
/// using the PTP_IoIfc (if enabled).
/// Please contact Oregano Systems for more details about this feature.
/// @ingroup datasets
struct Context_t {

  uint32_t               status;                   ///< status of PTP Context, see @ref ContextStat_e
  uint32_t               id;                       ///< origin of data, see @ref ContextId_e
  DefaultDataset_t       defaultDataset;
  CurrentDataset_t       currentDataset;
  ParentDataset_t        parentDataset;
  TimePropDataset_t      timePropDataset;
  PortDataset_t          portDataset;
#ifdef _PATH_TRACE_
  PathTraceList_t        pathTraceList;
#endif
#ifdef ALT_TIME_OFFSET
  AltTimeOffsetProp_t    altTimeOffsetProp[ALT_TIME_OFF_MAX_KEY];
#endif
#if defined _C37_238_2011_PROFILE_
  PowerProfileData_t     powerProfileData;
#endif
#ifdef _C37_238_2017_PROFILE_
  C37_238ProfileData_t   c37_238ProfileData;
#endif
#ifdef _SMPTE_PROFILE_
  SyncMetadata_t         syncMetadata;
  bool                   syncMetadataValid;
#endif
#if defined(_G8275_1_PROFILE_) || defined(_G8275_2_PROFILE_)
  G8275_1_2ProfileData_t g8275_1_2Data;
#endif
#ifdef _802_1AS_PROFILE_
  Ieee802_1asData_t      ieee802_1asData;
  Ieee802_1asPortData_t  ieee802_1asPortData;
  Timestamp_t            ieee802_1asGmPhaseChangeUpdateTime;
#endif
  ExtParentDataset_t     extParentDataset;
#ifdef STATISTICS_OUTPUT
  MessageCounter_t       messageCounter;
#endif
#ifdef _UNICAST_INFO_DATA_
  UnicastInfoData_t      unicastInfoData;
#endif
  uint32_t               contextHeartbeat;///< heartbeat for context to be able to check via shared memif ptp is alive
}__PACKME__;

#ifdef _SHARED_MEM_
/// this structure defines the "in bounds" interface of the shared memory.
struct BoundaryIf_t {
  Boundaries_e boundary : 8;
  uint8_t      inSync   : 1;
  uint8_t      selected : 1;
  uint8_t      reserved : 6;
}__PACKME__;
#endif


// Useful enumerations for status and id information
#ifdef __cplusplus
namespace ContextId_e {
#endif
  enum {ERes, EPtpv1, EPtpv2, EEsync, ELsync, EMbg = 0x10};
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
namespace ContextStat_e {
#endif
  enum  {
    // Basic flags
    EEmpty,
    ENewDataWr,
    ENewDataRd          = 0x04,
    // Profile flags
    EAltTimeOff         = 0x10,
    EPowerData          = 0x20,
    ESmpteData          = 0x40,
    EG8275_1_2Data      = 0x80,
    EC37_238Data        = 0x100,
    EPathTrace          = 0x200,
    E802_1as            = 0x400,
    // Other flags
    EMessageCounter     = 0x10000,
    EUnicastConnections = 0x20000
  };
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
namespace MessageCounterStat_e {
#endif
  enum  {
    EValid = 0x1, ///< Counter are valid if one
    EReset = 0x2  ///< Reset counters if one
  };
#ifdef __cplusplus
}
#endif

#pragma pack()

#endif
