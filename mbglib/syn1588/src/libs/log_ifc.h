/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#ifndef INC_LOG_IFC
#define INC_LOG_IFC

#include "basictypes.h"

#ifndef LOG_LEVEL
#  define LOG_LEVEL 4
#endif

/// Logger Interface Class

/// This class defines the interface of the PTP logging mechanism. The loglevel is
/// used to determine the severity level of different logging messages.
/// A derived class may be used to redirect all logging output to a certain
/// location, e.g. stdout, a file or a serial interface.
/// All member functions defined herein have to be implemented
/// outside the PTP library, e.g. by building a derived class.
/// @ingroup interfaces
class PTP_LogIfc {

  public:
    /// minimum log level
    static const uint8_t CMinLogLevel = 0;
    /// maximum log level
    static const uint8_t CMaxLogLevel = 4;

    /// Logging with Severity Always (level 0)
#if !defined(NO_LOGGING)
    virtual void Always(const char *fmt, ...) = 0;
#else
    void Always(const char *, ...) { }
#endif

    /// Logging with Severity Error (level 0)
#if LOG_LEVEL >= 0 && !defined(NO_LOGGING)
    virtual void Error(const char *fmt, ...) = 0;
#else
    void Error(const char *, ...) { }
#endif

    /// Logging with Severity Warning (level 1)
#if LOG_LEVEL >= 1 && !defined(NO_LOGGING)
    virtual void Warning(const char *fmt, ...) = 0;
#else
    void Warning(const char *, ...) { }
#endif

    /// Logging with Severity Notice (level 2)
#if LOG_LEVEL >= 2 && !defined(NO_LOGGING)
    virtual void Notice(const char *fmt, ...) = 0;
#else
    void Notice(const char *, ...) { }
#endif

    /// Logging with Severity Info (level 3)
#if LOG_LEVEL >= 3 && !defined(NO_LOGGING)
    virtual void Info(const char *fmt, ...) = 0;
#else
    void Info(const char *, ...) { }
#endif

    /// Logging with Severity Debug (level 4)
#if LOG_LEVEL >= 4 && !defined(NO_LOGGING)
    virtual void Debug(const char *fmt, ...) = 0;
#else
    void Debug(const char *, ...) { }
#endif

    /// @brief Get the current log level
    /// @return the currently set log level
    virtual uint8_t getLevel() { return 0; }

    /// @brief Return OS Time & date formatted as string
    /// @return const pointer to date string
    virtual const char *dateStr() { return 0; }

    /// Destructor
    virtual ~PTP_LogIfc() {}

};

#endif
