/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_IO_IFC
#define INC_IO_IFC

#include "basictypes.h"

/// @brief Input/Output Interface Class
///
/// This class defines the interface of the PTP Stack's Input/Ouput operations. It is
/// used by the PTP Stack to communicate with its local environment. The ReadData/WriteData
/// functions is used to read/write the \a Context_t structure to provide status informatio
/// as well as to receive updated dataset information.
/// A derived class may implement the WriteData function in a way that the \a Context_t structure
/// is made available to other application, e.g. by writing to a shared memory.
/// All member functions defined herein have to be implemented
/// outside the PTP library, e.g. by building a derived class.
/// @ingroup interfaces
class PTP_IoIfc {

 public:

    /// Io Type
    ///
    ///enumeration to signify type of data for I/O operations
    // update shared memory sizes(sharedmem_layout.h) when adding memory access types here
    typedef enum {
      /// PTPv2 data as read only mode
      ioTypeSlave,
      /// PTPv2 data in read/write mode
      ioTypeMaster,
      /// Event notification data
      ioTypeEventNotification,
      /// inBounds data in read only mode
      ioTypeInBoundsSlave,
      /// inBounds data in read/write mode
      ioTypeInBoundsMaster,
      /// PTPv1 data in read only mode
      ioTypeV1Slave,
      /// PTPv1 data in read/write mode
      ioTypeV1Master,
      // This has to be the last/highest element in the enumeration.
      // Use this value as the first index for customer specific
      // shared memory access enumerations.
#ifdef _DYNAMIC_RECONFIGURATION_
      // network configuration data read only mode
      ioTypeConfigSlave,
      // network configuration data read/write only mode
      ioTypeConfigMaster,
#endif
      ioTypeMax
    } IoType_t;

    /// Shared memory Read Flags
    typedef enum {
      rdFlagNone           = 0,         ///< none @hideinitializer
      rdFlagWaitForUpdate  = (1 << 0),  ///< wait for data update before reading @hideinitializer
      rdFlagWaitFor802_1as = (1 << 1)   ///< wait for 802.1AS data update before reading @hideinitializer
    } ReadFlags_t;

    /// Shared memory Write Flags
    typedef enum {
      wrFlagNone           = 0,         ///< none @hideinitializer
      wrFlagUpdatedShmem   = (1 << 0),  ///< signal data update upon writing @hideinitializer
      wrFlagUpdated802_1as = (1 << 1)   ///< signal 802.1AS data update upon writing @hideinitializer
    } WriteFlags_t;

    enum SemTimeout_e {
      ESemRwTout = 0,                   ///< sharedMemory rw timeout @hideinitializer
      ESemUpdTout                       ///< sharedMemory update timeout
    };

  /// Initialization function
  virtual uint8_t init() = 0;

  /// @brief Input function
  ///
  /// Read from \a stdin \a max characters into \a str.
  /// @param [in,out] str
  /// @param [in] max maximum characters to read from stdin
  /// @return EErr on failure, ENoErr otherwise.
  virtual uint8_t Read(char *str, size_t max) = 0;

  /// @brief Read data of type \a type and store in \a data.
  ///
  /// The source of data is implementation specific.
  /// If \a type is 0, a structure of type \a Context_t is expected.
  /// @param[in,out] data
  /// @param[in] type specifies type of \a data
  /// @param[in] flags additional read flags
  /// @return EErr on failure, ENoErr otherwise.
  virtual uint8_t ReadData(void *data, IoType_t type = ioTypeSlave, ReadFlags_t flags = rdFlagNone) = 0;

  /// @brief Output function
  ///
  /// Write \a str directly to stdout.
  /// @param [in] str a formatted string
  virtual void Write(const char *str,...) = 0;

  /// @brief Write \a data of type \a type.
  ///
  /// The data destination is implementation specific.
  /// If \a type is 0, \a data is expected to be of type \a Context_t
  /// @param[in] data pointer to data to be written
  /// @param[in] type specifies data type of \a data
  /// @param[in] flags additional write flags
  virtual void WriteData(const void *data, IoType_t type = ioTypeSlave, WriteFlags_t flags = wrFlagNone) = 0;

  /// @brief Set timeout for semaphore and shared memory operations
  /// @param[in] t enum to specify which timeout to set (rw / update)
  /// @param[in] us timeout in microseconds
  virtual void setTout(SemTimeout_e t, uint32_t us) = 0;

  /// Destructor
  virtual ~PTP_IoIfc() {}

  /// @brief PTP state change reason codes for the ioTypeEventNotification interface.
  ///
  /// @note Every reason below REASON_EXTERNAL_SYNC_LOST (0x80) shows up in the state change diagram of
  /// the 1588-2008 standard.
  typedef enum {
    REASON_NO_REASON = 0,                 ///< no reason @hideinitializer
    REASON_INIT_COMPLETE = 1,             ///< initialisation complete  @hideinitializer
    REASON_DESIGNATED_DISABLED,           ///<
    REASON_DESIGNATED_ENABLED,            ///<
    REASON_RECOMMENDED_STATE_SLAVE,       ///< recommended state: slave
    REASON_RECOMMENDED_STATE_MASTER,      ///< recommended state: master
    REASON_RECOMMENDED_STATE_PASSIVE,     ///< recommended state: passive
    REASON_MASTER_CLOCK_SELECTED,         ///< master clock selected
    REASON_NEW_MASTER,                    ///< new master
    REASON_ANNOUNCE_TIMEOUT_EXPIRED,      ///< announce timeout expired
    REASON_QUALIFICATION_TIMEOUT_EXPIRED, ///< qual. timeout expired
    REASON_SYNCHRONIZATION_FAULT,         ///< sync fault
    REASON_FAULT_DETECTED,                ///< fault detected
    REASON_FAULT_CLEARED,                 ///< fault cleared
    REASON_EXTERNAL_SYNC_LOST = 0x80,     ///< external sync lost @hideinitializer
    REASON_SYNC_IN_BOUND,                 ///< in bounds
    REASON_SYNC_OUT_OF_BOUND,             ///< out of bounds
    REASON_PRESCALER_RESTART,             ///< prescaler restart
    REASON_MAX_REASONS
  } IoStateChangeReason_t;

  /// @brief Event Notification struct
  struct EventNotification_t {
    uint16_t port;                  ///< event port
    IoStateChangeReason_t reason;   ///< event reason

    EventNotification_t (uint16_t _port, IoStateChangeReason_t _reason) { port = _port; reason = _reason; };
  };
};


#endif
