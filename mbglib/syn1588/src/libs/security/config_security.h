/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ********************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef _INC_CONFIG_SECURITY_
#define _INC_CONFIG_SECURITY_

#ifdef _USE_PTP_SECURITY_

#include <stdint.h>
#include <string.h>
#include "basictypes.h"

#define PTP_SEC_MAX_KEY_NO      5         ///< max amount of keys allowed per SA
#define PTP_SEC_MAX_SA          10        ///< max amount of SAs allowed in SAD

#define PTP_SEC_MAX_OID_LEN     60        ///< max length of an algorithm OID

// max lengths of individual fields (in bytes!)
// if we add a new algorithm with longer keys/icvs -> update these fields!
#define PTP_SEC_MAX_KEY_LEN     (256 / 8) ///< 32bytes  / 256bit
#define PTP_SEC_MAX_ICV_LEN     (128 / 8) ///< 16bytes  / 128bit
#define PTP_SEC_MAX_SEQNO_LEN   4
#define PTP_SEC_MAX_RES_LEN     4

typedef enum {
  EAlgoNone = 0,  ///< invalid
  EHmacSha256_128 ///< HMAC SHA256-128 (OID 1.2.840.113549.2.9)
} AlgoType_e;     ///< enumeration of supported algorithms

#pragma pack(1)
struct secKey_t {
  uint32_t keyId;                     ///< key identifier
  uint8_t  key[PTP_SEC_MAX_KEY_LEN];  ///< key
}__PACKME__; ///< security key structure

struct PTP_SecurityAssociation_t {

  uint8_t    spp;                      ///< Security Parameter Pointer (unique identifiert for an SA)
  AlgoType_e integrityAlgType;         ///< hash algorithm to use
  uint16_t   icvLen;                   ///< length of ICV (in bytes)
  secKey_t   keys[PTP_SEC_MAX_KEY_NO]; ///< list of available keys
  uint16_t   keyLen;                   ///< length of key (in bytes)
  bool       useSeqNo;                 ///< flag to enforce use of sequence number in sec TLV
  uint8_t    seqNoLen;                 ///< length of sequence number field, if used (in bytes)
  bool       useRes;                   ///< flag to enforce use of reserved field in sec TLV
  uint8_t    ResLen;                   ///< length of reserved field, if used (in bytes)
  uint8_t    curKeyIdx;                ///< pointer to currently used key
  bool       useImmediateSec;          ///< flag to enforce use of immediate security processing

  /// default constructor
  PTP_SecurityAssociation_t() {
    spp              = static_cast<uint8_t>(-1);
    integrityAlgType = EAlgoNone;
    icvLen           = 0;
    memset(keys, 0, sizeof(secKey_t) * PTP_SEC_MAX_KEY_NO);
    keyLen           = 0;
    useSeqNo         = false;
    seqNoLen         = 0;
    useRes           = false;
    ResLen           = 0;
    useImmediateSec  = true;
    curKeyIdx        = static_cast<uint8_t>(-1);
  }

}__PACKME__; ///< PTP Security Association structure

/// PTP Security (v2.1) configuration structure
struct PTP_SecurityPolicyDatabase_t {
  uint16_t   msgTypes;     ///< bitfield declares which PTP messages need to be secured
  uint8_t    tx_spp;       ///< defines SA to use for sending
  AlgoType_e minAlgorithm; ///< minimum valid algorithm to use

  PTP_SecurityPolicyDatabase_t() {
    msgTypes     = 0x00FFu; // we want an ICV for every message type
    tx_spp       = 0;
    minAlgorithm = EAlgoNone;
  }
}__PACKME__;

struct ConfigSec_t {
  uint8_t                      numSa;               ///< number of registered SAs
  PTP_SecurityAssociation_t    sad[PTP_SEC_MAX_SA]; ///< holds all registered SAs
  PTP_SecurityPolicyDatabase_t spd;                 ///< global security policy

  ConfigSec_t() {
    numSa = 0;
  }
}__PACKME__;
#pragma pack()

/// @brief Config structure for PTP Security
struct PTP_ConfigSec : public ConfigSec_t {

  static const uint8_t CSecMaxKeyLen    = PTP_SEC_MAX_KEY_LEN;    ///< max allowed key length
  static const uint8_t CSecMaxIcvLen    = PTP_SEC_MAX_ICV_LEN;    ///< max allowed ICV length
  static const uint8_t CSecMaxSeqNoLen  = PTP_SEC_MAX_SEQNO_LEN;  ///< max allowed sequence number length
  static const uint8_t CSecMaxResLen    = PTP_SEC_MAX_RES_LEN;    ///< max allowed reserved field length

  static const uint8_t CSecMaxKeys      = PTP_SEC_MAX_KEY_NO;     ///< max allowed keys per SA
  static const uint8_t CSecMaxSa        = PTP_SEC_MAX_SA;         ///< max allowed SAa per SAD

  /// Default constructor
  PTP_ConfigSec() {
  }

  /// @brief Add a Security Association to the SAD
  /// @param [in] sa the SA to add
  /// @return ENoErr on success, EErr on failure
  uint8_t addSA(PTP_SecurityAssociation_t &sa) {
    if (!isValid(sa) || (numSa >= CSecMaxSa)) {
      return EErr;
    }

    for (uint8_t i = 0; i < numSa; i++) {
      if (sad[i].spp == sa.spp) {
        /// SPP already in use!
        return EErr;
      }
    }

    // store SA data
    memcpy(&sad[numSa++], (const void*)&sa, sizeof(PTP_SecurityAssociation_t));
    return ENoErr;
  }

  /// @brief Update an existing SA with new data (e.g. on key change)
  /// @param [in] sa the SA to update
  /// @return ENoErr on success, EErr on failure
  uint8_t updateSA(PTP_SecurityAssociation_t &sa) {
    if (!isValid(sa)) {
      return EErr;
    }

    for (uint8_t i = 0; i < numSa; i++) {
      if (sad[i].spp == sa.spp) {
        // replace old SA with new one
        memcpy(&sad[i],(const void*)&sa, sizeof(PTP_SecurityAssociation_t));
        return ENoErr;
      }
    }

    // specified SA not found in SAD
    return EErr;
  }

  /// @brief Check if a given Sa holds valid data
  /// @param [in] sa the SA to check
  /// @return ENoErr if valid, EErr otherwise
  bool isValid(PTP_SecurityAssociation_t &sa) {
    // check if this object holds a valid SA configuration
    if ((sa.integrityAlgType == EAlgoNone) ||
        (sa.keyLen   > CSecMaxKeyLen)      ||
        (sa.icvLen   > CSecMaxIcvLen)      ||
        (sa.seqNoLen > CSecMaxSeqNoLen)    ||
        (sa.ResLen   > CSecMaxResLen)) {
      return false;
    }
    return true;
  }
};

#endif // _USE_PTP_SECURITY_
#endif //!_INC_CONFIG_SECURITY_
