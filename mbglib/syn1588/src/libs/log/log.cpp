/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#include "os.h"
#include "log.h"
#include "basictypes.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>

const uint8_t LOG_ERROR   = 0;
const uint8_t LOG_WARNING = 1;
const uint8_t LOG_NOTICE  = 2;
const uint8_t LOG_INFO    = 3;
const uint8_t LOG_DEBUG   = 4;
const uint8_t LOG_ALWAYS  = 5;

const char *SEV_STR[] = {"[ERROR]  ",
  "[WARNING]",
  "[NOTICE] ",
  "[INFO]   ",
  "[DEBUG]  ",
  "[ALWAYS] "};

extern void reopenLogs(int param) {
  (void)param;
  PTP_Log::m_reopenLogfile = true;
}

bool PTP_Log::m_reopenLogfile;
PTP_Log::PTP_LogHandle **PTP_Log::m_handles = NULL;

// only print Prefix after '\n' (*)
void PTP_Log::log(const uint8_t lev, const char *fmt, va_list ap) {
  if (m_reopenLogfile && m_cfg->useFileLogging) {
    m_reopenLogfile = false;
    if (m_handles) {
      for (int i = 0; i < m_numPorts; i++) {
        if (m_handles[i] == NULL) break;
        if (m_handles[i]->fd != NULL) {
          fflush(m_handles[i]->fd);
          m_handles[i]->fd = freopen(m_handles[i]->filename, "wt", m_handles[i]->fd);
        } else break;
      }
    }
  }
  if (lev == LOG_ALWAYS || lev <= (int8_t)m_cfg->logLevel) {
    if (m_lastChar == '\n') {
      if (m_cfg->addTimestamp) {
        fprintf(m_logDest, "%s ", OS::getDateStr());
      }
      if (m_cfg->addSeverity) {
        fprintf(m_logDest, "%s ", SEV_STR[lev]);
      }
      fprintf(m_logDest, "%s", m_cfg->prfx);
    }

    vfprintf(m_logDest, (fmt), ap);
    m_lastChar = fmt[strlen(fmt) - 1];
  }
}

/// The enhanced constructor with configuration settings
PTP_Log::PTP_Log(PTP_ConfigLog *cfg, uint8_t numPorts) : m_cfg(cfg), m_lastChar('\n') {
  m_errCounter = 0;
  m_numPorts = numPorts;
  m_fdCnt = NULL;
  if (m_handles == NULL) {
    m_handles = new PTP_LogHandle*[numPorts];
    for (int i = 0; i < numPorts; i++) {
      m_handles[i] = NULL;
    }
  }

  if (m_cfg->useFileLogging) {
    // go through array of handles:
    // if filename not found, open new handle && add to array
    // else use existing handle
    int i = 0;
    for (; i < numPorts; i++) {
      if (m_handles[i] == NULL) {
        m_handles[i] = new PTP_LogHandle();
        break;
      } else if (strncmp(m_handles[i]->filename,
                         m_cfg->filename,
                         strlen(m_cfg->filename)) == 0) {
        break;
      }
    }

    assert(i < numPorts);
    assert(m_handles[i]);

    if (m_handles[i]->fd == NULL) {
      m_handles[i]->fd = fopen(m_cfg->filename, "wt");
      if (m_handles[i]->fd == NULL) {
        //use default logging instead
        m_logDest = stdout;
        m_cfg->useFileLogging = false;
        Error("ERROR: Logfile \"%s\" cannot be opened.\n", m_cfg->filename);
      } else {
        memcpy(m_handles[i]->filename, m_cfg->filename, sizeof(m_handles[i]->filename));
        m_logDest = m_handles[i]->fd;
        m_handles[i]->cnt++;
        m_fdCnt = &m_handles[i]->cnt;
      }
    } else {
      m_logDest = m_handles[i]->fd;
      m_handles[i]->cnt++;
      m_fdCnt = &m_handles[i]->cnt;
    }
  } else {
    // use default logging
    m_logDest = stdout;
  }
  // disable stdout buffering
  setvbuf(stdout, NULL, _IONBF, 0);

  m_reopenLogfile     = false;
}

PTP_Log::~PTP_Log() {
  if (m_cfg->useFileLogging) {
    if (m_logDest != NULL) { // not necessarily needed anymore, but safety first
      fflush(m_logDest);
      if (m_fdCnt && --*m_fdCnt < 1) {
        Debug("Closing handle to file \"%s\"\n", m_cfg->filename);
        fclose(m_logDest);
        m_logDest = NULL;
      }
    }
  }
  bool lastItem = true;
  for (int i = 0; i < m_numPorts; ++i) {
    if (m_handles && m_handles[i]) lastItem = false;
  }
  if (lastItem) {
    delete[] m_handles;
    m_handles = NULL;
  }
}

// Log always to destination. No Loglevel-Checking or other Errorhandling.
void PTP_Log::Always(const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  log(LOG_ALWAYS, fmt, ap);
  va_end(ap);
}

/// Output variable arguments list according to format string fmt.
/// This is very much like using printf(). Error has highest priority
/// and is always printed. (if filelogging is used, a hint will be put to stderr once)
void PTP_Log::Error(const char *fmt, ...) {
# if LOG_LEVEL >= 0
  va_list ap;
  va_start(ap, fmt);
  log(LOG_ERROR, fmt, ap);
  va_end(ap);
  if (m_errCounter < ERR_COUNTER_MAX)
    m_errCounter++;
  if (this->m_cfg->useFileLogging && m_errCounter == 1)
    fprintf(stderr, "There were ERRORS. Please check the log file for details.\n");
# else
  (void)fmt;
# endif
}

/// See Error, but with lower priority
///
void PTP_Log::Warning(const char *fmt, ...) {
# if LOG_LEVEL >= 1
  va_list ap;
  va_start(ap, fmt);
  log(LOG_WARNING, fmt, ap);
  va_end(ap);
# else
  (void)fmt;
# endif
}

/// See Warning, but with lower priority
///
void PTP_Log::Notice(const char *fmt, ...) {
# if LOG_LEVEL >= 2
  va_list ap;
  va_start(ap, fmt);
  log(LOG_NOTICE, fmt, ap);
  va_end(ap);
# else
  (void)fmt;
# endif
}

/// See Notice, but with lower priority
///
void PTP_Log::Info(const char *fmt, ...) {
# if LOG_LEVEL >= 3
  va_list ap;
  va_start(ap, fmt);
  log(LOG_INFO, fmt, ap);
  va_end(ap);
# else
  (void)fmt;
# endif
}

/// See Info, but with lower priority
///
void PTP_Log::Debug(const char *fmt, ...) {
# if LOG_LEVEL >= 4
  va_list ap;
  va_start(ap, fmt);
  log(LOG_DEBUG, fmt, ap);
  va_end(ap);
# else
  (void)fmt;
# endif
}
