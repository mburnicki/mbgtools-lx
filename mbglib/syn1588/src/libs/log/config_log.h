/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/
#ifndef INC_CONFIG_LOG
#define INC_CONFIG_LOG

#include "basictypes.h"

#include <string.h>

// we do not use the system maximum on purpose:
// the config may be written to the shared-memory - we dont want tu clutter the shared memory with
// a few thousand bytes which is in most cases unnecessary. The number below is used because
// windows (file-) systems do/did not support longer path names.
// When longer filenames are to be supported the change shall also be done in the parser!
#if defined(__NIOS__)
#  define PATH_LEN 1
#elif defined(__linux__)
#  define PATH_LEN 260
#elif defined(WIN32)
#  define PATH_LEN 260
#else
#  error no path length is defined for this platform.
#endif

/// Logger configuration structure

/// This structure defines all logger related configuration parameters.
///
struct PTP_ConfigLog {

  uint8_t logLevel;           ///< Logging level (0..4)
  char    prfx[8];            ///< Logging Prefix
  bool    useFileLogging;     ///< indicates file logging if TRUE;
  bool    addTimestamp;       ///< Add a system timestamp to each log line
  bool    addSeverity;        ///< Add severity level to each log line
  char    filename[PATH_LEN]; ///< filename for a logfile
#ifdef _DYNAMIC_RECONFIGURATION_
  bool    changed;            ///< flag to indicate that the config has been changed
#endif

  /// Default Constructor
  PTP_ConfigLog() {
    logLevel       = 0;
    prfx[0]        = 0;
    useFileLogging = false;
    addTimestamp   = false;
    addSeverity    = false;
    memset(&filename[0], 0, sizeof(filename));
#ifdef _DYNAMIC_RECONFIGURATION_
    changed        = true;
#endif
  }
};
#endif // INC_CONFIG_LOG
