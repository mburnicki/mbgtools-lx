/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#ifndef INC_LOG
#define INC_LOG

#include "config_log.h"
#include "log_ifc.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

/// signal handler for SIGHUB
extern void reopenLogs(int param);

/// Logger Class

/// This class is derived from the abstract base PTP_LogIfc.
/// It implements all neccessary functions declared there by
/// using ACE's logging mechanism.

class PTP_Log : public PTP_LogIfc {

  protected:
    PTP_ConfigLog *m_cfg;

  private:

    void log(const uint8_t lev, const char *fmt, va_list ap);

    /// Structure to hold filepointer to open logfiles
    ///
    struct PTP_LogHandle {

      FILE      *fd;                 ///< file descriptor
      char      filename[PATH_LEN];  ///< filename
      uint16_t  cnt;                 ///< usage counter

      /// Default Constructor
      PTP_LogHandle() {
        fd = NULL;
        memset(&filename[0], 0, sizeof(filename));
        cnt = 0;
      }

    };

    static const uint16_t ERR_COUNTER_MAX =  0xFFFFU;

    FILE                 *m_logDest;   ///< indicates logging Destination. Default: stderr
    char                 m_lastChar;
    uint16_t             m_errCounter; ///< counts number of errors logged.
    uint16_t             m_numPorts;   ///< number of ports
    static PTP_LogHandle **m_handles;  ///< pointer to array of open file descriptors
    uint16_t             *m_fdCnt;     ///< usage counter of this port's file descriptor

  public:
    static bool m_reopenLogfile;

    /// The enhanced constructor with configuration settings
    PTP_Log(PTP_ConfigLog *cfg, uint8_t numPorts = 1);

    /// Log always, like for printing copyright information, but which is no error
    void Always(const char *fmt, ...);
    /// Logging with Severity Error (level 0)
    void Error(const char *fmt, ...);
    /// Logging with Severity Warning (level 1)
    void Warning(const char *fmt, ...);
    /// Logging with Severity Notice (level 2 )
    void Notice(const char *fmt, ...);
    /// Logging with Severity Info (level 3)
    void Info(const char *fmt, ...);
    /// Logging with Severity Debug (level 4)
    void Debug(const char *fmt, ...);

    /// Set logging level to lev. Setting logging level to 1 means
    /// logging of all Error and Warning messages, a value of 2 means logging
    /// of all Error, Warning and Notice messages, etc.
    /// \arg \c lev logging level, range 0..4
    void setLevel(uint8_t lev) { m_cfg->logLevel = lev; }

    uint8_t getLevel() { return m_cfg->logLevel; }

    //Destructor
    ~PTP_Log();

};

#endif
