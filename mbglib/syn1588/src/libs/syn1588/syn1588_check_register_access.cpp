/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#include "syn1588_check_register_access.h"
#include "syn1588_impl.h"

const struct Syn1588Check::capabilityStruct Syn1588Check::registerLookupTable[] = {
  {Syn1588Ifc::MIITS0_RXCRTL, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS0}, // MII time stamper 0 registers start
  {Syn1588Ifc::MIITS0_TXCRTL, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS0},
  {Syn1588Ifc::MIITS0_RXMEM,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS0},
  {Syn1588Ifc::MIITS0_TXMEM,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS0}, // MII time stamper 0 registers end
  {Syn1588Ifc::MIITS1_RXCRTL, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS1}, // MII time stamper 1 registers start
  {Syn1588Ifc::MIITS1_TXCRTL, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS1},
  {Syn1588Ifc::MIITS1_RXMEM,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS1 | Syn1588Ifc::CAP1_MEASURE},
  {Syn1588Ifc::MIITS1_TXMEM,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_MIITS1}, // MII time stamper 1 registers end
  {Syn1588Ifc::EVENTTIME0_L,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_EVENT0}, // Event 0 registers start
  {Syn1588Ifc::EVENTTIME0_H,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_EVENT0}, // Event 0 registers end
  {Syn1588Ifc::EVENTTIME1_L,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_EVENT1}, // Event 1 registers start
  {Syn1588Ifc::EVENTTIME1_H,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_EVENT1}, // Event 1 registers end
  {Syn1588Ifc::TRIGTIME0_L,   Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_TRIGGER0}, // Trigger 0 registers start
  {Syn1588Ifc::TRIGTIME0_H,   Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_TRIGGER0}, // Trigger 0 registers end
  {Syn1588Ifc::TRIGTIME1_L,   Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_TRIGGER1}, // Trigger 1 registers start
  {Syn1588Ifc::TRIGTIME1_H,   Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_TRIGGER1}, // Trigger 1 registers end
  {Syn1588Ifc::PERIODTIME0_L, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PERIOD0}, // Period 0 registers start
  {Syn1588Ifc::PERIODTIME0_H, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PERIOD0}, // Period 0 registers end
  {Syn1588Ifc::PERIODTIME1_L, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PERIOD1}, // Period 1 registers start
  {Syn1588Ifc::PERIODTIME1_H, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PERIOD1}, // Period 1 registers end
  {Syn1588Ifc::TIME2_NS,      Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_TIME2}, // Time 2 registers start
  {Syn1588Ifc::TIME2_SEC,     Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_TIME2}, // Time 2 registers end
  // CAP1_IRIGB does not correspond to a register, thus no entry was created
  {Syn1588Ifc::ONESTEPCTRL, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PROG1STEP}, // Programmable one step register
  // CAP1_PERIODSET does not correspond to a register, thus no entry was created
  // CAP1_NTPMODE does not correspond to a register, thus no entry was created
  {Syn1588Ifc::LEAP_SEC,       Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_LEAPAPPLY}, // Leap applytime registers start
  {Syn1588Ifc::LEAP_APPLYTIME, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_LEAPAPPLY}, // Leap applytime registers end
  // CAP1_DUTYCYCLE1 does not correspond to a register, thus no entry was created
#ifdef _USE_PKTGEN_
  {Syn1588Ifc::VIP_PKTGEN_CTRL,        Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN}, // Packet generator registers start
  {Syn1588Ifc::VIP_PKTGEN_SL_RAM_LINE, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_DATA,        Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_SYNC_SEQID,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_ANN_SEQID,   Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_MODE,        Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_MAX_RATE,    Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_SLAVE_LIST,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_ANN_LEN,     Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN},
  {Syn1588Ifc::VIP_PKTGEN_STATUS,      Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_PKTGEN}, // Packet generator registers end
  // CAP1_PKTGENFMT does not correspond to a register, thus no entry was created
#endif
  {Syn1588Ifc::FRAC_NUM,         Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_GENLOCK}, // Genlock registers start
  {Syn1588Ifc::FRAC_DENUM,       Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_GENLOCK},
  {Syn1588Ifc::FRAME_COUNT,      Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_GENLOCK},
  {Syn1588Ifc::SHD_SDITIME27M,   Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_GENLOCK},
  {Syn1588Ifc::GENLOCK_CTRL,     Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_GENLOCK},
  {Syn1588Ifc::SHD_SDITIME90k,   Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_GENLOCK}, // Genlock registers end
  {Syn1588Ifc::IRIGB_IN_SECONDS, Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_IRIGBIN}, // IRIG-B IN registers start
  {Syn1588Ifc::IRIGB_IN_STATUS,  Syn1588Ifc::CAPABILITIES1, Syn1588Ifc::CAP1_IRIGBIN}, // IRIG-B IN registers end
  // CAP1_PKTGENEXT does not correspond to a register, thus no entry was created
  // CAP1_STOPCLOCK does not correspond to a register, thus no entry was created
  {Syn1588Ifc::VR_ETH_TSRXCTRL,     Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_RX_TIMESTAMPER}, // MAC TS RX registers start
  {Syn1588Ifc::VR_ETH_TSFIFORX,     Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_RX_TIMESTAMPER},
  {Syn1588Ifc::VR_ETH_PHY_DELAY_RX, Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_RX_TIMESTAMPER},
  {Syn1588Ifc::VR_ETH_TS_RXMEM,     Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_RX_TIMESTAMPER}, // MAC TS RX registers end
  {Syn1588Ifc::VR_ETH_TSTXCTRL,     Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_TX_TIMESTAMPER}, // MAC TS TX registers start
  {Syn1588Ifc::VR_ETH_TSFIFOTX,     Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_TX_TIMESTAMPER},
  {Syn1588Ifc::VR_ETH_PHY_DELAY_TX, Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_TX_TIMESTAMPER},
  {Syn1588Ifc::VR_ETH_TS_TXMEM,     Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_TX_TIMESTAMPER}, // MAC TS TX registers end
  {Syn1588Ifc::VR_ETH_ONESTEPCTRL,  Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_1STEP_TIMESTAMPER}, // MAC one step time stamper register
  // MAC_CAP0_STAT_COUNTER_LENGTH does not correspond to a register, thus no entry was created
  // MAC_CAP0_VLAN_NUM does not correspond to a register, thus no entry was created
  // MAC_CAP0_VLAN_GROUP_ENABLE does not correspond to a register, thus no entry was created
  {Syn1588Ifc::VR_ETH_CNT_VLANERR,   Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_ADD_STAT_COUNTER}, // MAC additional stat counter registers start
  {Syn1588Ifc::VR_ETH_CNT_VLAN,      Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_ADD_STAT_COUNTER},
  {Syn1588Ifc::VR_ETH_CNT_BROADCAST, Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_ADD_STAT_COUNTER},
  {Syn1588Ifc::VR_ETH_CNT_MULTICAST, Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_ADD_STAT_COUNTER},
  {Syn1588Ifc::VR_ETH_CNT_RX_TS,     Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_ADD_STAT_COUNTER},
  {Syn1588Ifc::VR_ETH_CNT_RX_TS2,   Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_ADD_STAT_COUNTER},
  {Syn1588Ifc::VR_ETH_CNT_TX_TS2,   Syn1588Ifc::VR_ETH_CAPABILITIES_0, Syn1588Ifc::MAC_CAP0_ADD_STAT_COUNTER} // MAC additional stat counter registers end
};

const size_t Syn1588Check::registerLutSize = sizeof(Syn1588Check::registerLookupTable) / sizeof(Syn1588Check::capabilityStruct);

uint8_t Syn1588Check::checkRegisterAccess(uint32_t addr, Syn1588Impl *syn1588) {
  int i;
  uint32_t caps;
  if ((i = Syn1588Check::findInTable(addr)) >= 0) {
    // read the capability register and compare bits
    caps = syn1588->readReg(registerLookupTable[i].capRegAddr);
    if (caps & registerLookupTable[i].testBit) return ENoErr;
    else return EErr;
  }
  // addr not in map -> no capability necessary
  return ENoErr;
}

uint8_t Syn1588Check::checkCapabilities(uint32_t addr, uint32_t cap1, uint32_t mac_cap0) {
  int i;
  if ((i = Syn1588Check::findInTable(addr)) >= 0) {
    if (registerLookupTable[i].capRegAddr == Syn1588Ifc::CAPABILITIES1) {
      if (cap1 & registerLookupTable[i].testBit) return ENoErr;
      else return EErr;
    } else if (registerLookupTable[i].capRegAddr == Syn1588Ifc::VR_ETH_CAPABILITIES_0) {
      if (mac_cap0 & registerLookupTable[i].testBit) return ENoErr;
      else return EErr;
    }
  }
  // addr not in map -> no capability necessary
  return ENoErr;
}

int Syn1588Check::findInTable(uint32_t addr) {
  for (uint8_t i = 0; i < registerLutSize; i++) {
    if (addr == registerLookupTable[i].regAddr) return i;
  }
  //no match found -> no capability required for access
  return -1;
}
