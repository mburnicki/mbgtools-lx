/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#include "ts_config.h"
#include "string.h"

// Enable this macro to enable packet filtering for timestamping by VLAN ID
//#define _USE_VLANID_TS_FILTER_

// Arrays for different nwProtos:
// 0: NA, 1: L3_IPv4, 2: L3_IPv6, 3:L2
// Define the packet length
static const uint8_t patternLen[] = {0, 50, 70, 22};
static const uint8_t ipLen[]      = {0, 20, 40,  0};
static const uint8_t udpLen[]     = {0,  8,  8,  0};

/// UDP checksum position
static const uint8_t udpChkPos[]  = {0, 40, 60,  0};

/// Timestamp position
static const uint8_t tsPosPTP[]      = {0, 76, 96, 48};
static const uint8_t tsPosNTP[]      = {0, 80, 100, 0};

// Define the extract pattern
static const uint8_t exOffset0[]   =
{0, 62, 82, 34}; // PTP PortId (PTP ClkId + PortNumber)
static const uint8_t exOffset1[]   =
{0, 72, 92, 44}; // PTP PortId (PTP ClkId + PortNumber)
static const uint8_t exOffset0PTPv1[] =
{0, 64, 0, 0}; // UUID
static const uint8_t exOffset1PTPv1[] =
{0, 72, 0, 0}; // sequence ID
static const uint8_t exLength0      = 10; //sizeof(PortId_t);
static const uint8_t exLength1      =  2; //sizeof(uint16_t);
static const uint8_t exLength0PTPv1 =  6; //sizeof(MAC);

/// pattern

/// This function configures the pattern for the timestamper,
/// depending on nwProto, nwMode, domain and vlanID. It returns
/// a pointer to an array containg the whole sequence of bytes.
uint8_t *TSConfig::pattern(uint8_t *len) {

  ::memset(&pat, 0, sizeof(pat));

  uint8_t *ptr = (uint8_t *)&pat;

  *len = patternLen[nwProto];

  // build the Ethernet packet
  ptr += 12; // Ethernet addresses
  // insert vlan tag
  if (vlanId) {
    ptr[0] = 0x81; ptr[1] = 0x00;  // VLAN type
#ifdef _USE_VLANID_TS_FILTER_
    ptr[2] = (vlanId >> 8) & 0xF;  // VLAN Id
    ptr[3] = vlanId & 0xFF;
#endif
    ptr += 4;
    *len = *len + 4;
  }
  // Ethernet type
  switch (nwProto) {
    case L2:
      ptr[0] = 0x88; ptr[1] = 0xF7; break;
    case L3_IPv4:
      ptr[0] = 0x08; ptr[1] = 0x00; break;
    case L3_IPv6:
      ptr[0] = 0x86; ptr[1] = 0xDD; break;
    default: // unknown nw proto
      break;
  }
  ptr += 2; // Ethernet type

  // Do some protocol specific stuff
  switch (nwProto) {
    case L2:
      break; // nothing to do here
    case L3_IPv4:
      ptr[0] = 0x45; ptr[1] = 0x00;   // IP 0x4500
      ptr[9] = 0x11;                  // UDP 0x11
      break;
    case L3_IPv6:
      ptr[0] = 0x60;                  // IP version
      ptr[6] = 0x11;                  // UDP 0x11
      break;
    default: // unknown nw proto
      break;
  }
  ptr += ipLen[nwProto]; // ip packet

  // build the UDP packet
  PktUDP udp;
  ::memset(udp.hdr, 0, sizeof(udp.hdr));
  uint8_t port = recv ? 2 : 0; // UDP dest port (2) or src port (0)
  if (timeProto == CTimeProtoPTPv2 || timeProto == CTimeProtoPTPv1) {
    udp.hdr[port] = 0x01; udp.hdr[port+1] = 0x3F; // Port 319 (0x013F)
  } else if (timeProto == CTimeProtoNTP) {
    udp.hdr[port] = 0x00; udp.hdr[port+1] = 0x7B; // Port 123 (0x007B)
  }
  if (dlyReqOnly) {
    if (timeProto == CTimeProtoNTP) {
      // if ntp mode 
      switch (nwProto) {
        case L3_IPv4:
        case L3_IPv6:
          udp.hdr[4] = 0x00;
          udp.hdr[5] = 0x38;
          break;
        case L2:
        default:
          // invalid modes
          break;
      }
    }else {
      switch (nwProto) {
        case L3_IPv4:
          // Do not respond to delay requests with TLVs.
          // A delay request without TLVs is 52 octets long
          udp.hdr[4] = 0x00;
          udp.hdr[5] = 0x34;
          break;
        case L3_IPv6:
          // Do not respond to delay requests with TLVs.
          // A delay request without TLVs is 54 octets long
          // (two additional padding bytes see ANNEX E of IEEE1588-2008)
          udp.hdr[4] = 0x00;
          udp.hdr[5] = 0x36;
          break;
        case L2:
        default:
          break;
      }
    }
  }

  // copy udp packet to correct place
  switch (nwProto) {
    case L2:
      break; // nothing to do here
    case L3_IPv4: // fall through
    case L3_IPv6:
      ::memcpy(ptr, &udp, sizeof(udp.hdr));break;
    default: // unknown nw proto
      break;
  }
  ptr += udpLen[nwProto]; // udp packet

  if (timeProto == CTimeProtoPTPv2) {
    // build the PTP packet
    PktPTP ptp;
    ::memset(&ptp, 0, sizeof(ptp));
    if (dlyReqOnly) ptp.hdr[0] = 0x01; // Transport specific/ MsgType 0-3
    else ptp.hdr[0] = 0x00; // Transport specific/ MsgType 0-3
    if (nwProto != L3_IPv6) ptp.hdr[1] = 0x02; // reserved/ Version 2
    ptp.hdr[4] = domain; // domain
    if (nwMode == 1) ptp.hdr[6] = 0x04; // Unicast Flag
    // copy ptp packet to correct place
    ::memcpy(ptr, &ptp, sizeof(PktPTP));

  } else if (timeProto == CTimeProtoNTP) {
    // Build the NTP packet
    PktNTP ntp;
    ::memset(&ntp, 0, sizeof(PktNTP));

    // copy ptp packet to correct place
    ::memcpy(ptr, &ntp, sizeof(PktNTP));
  } else if (timeProto == CTimeProtoPTPv1) {

    // Build the PTPv1 packet
    PktPTP ptp;
    ::memset(&ptp, 0, sizeof(ptp));
    ptp.hdr[0] = 0x00;
    ptp.hdr[1] = 0x01; // reserved/ Version 1
    // copy ptp packet to correct place
    ::memcpy(ptr, &ptp, sizeof(PktPTP));
  }


  return (uint8_t *)&pat;
}


/// mask

/// This function configures the mask for the timestamper,
/// depending on nwProto, nwMode, domain and vlanID. It returns
/// a pointer to an array containg the whole sequence of bytes.
uint8_t * TSConfig::mask() {

  ::memset(&msk, 0, sizeof(msk));

  uint8_t *ptr = (uint8_t *)&msk;

  // build the Ethernet packet
  ptr += 12; // Ethernet addresses
  // insert vlan tag
  if (vlanId) {
    ptr[0] = 0xFF; ptr[1] = 0xFF; // VLAN type
#ifdef _USE_VLANID_TS_FILTER_
    ptr[2] = 0xF;  ptr[3] = 0xFF; // VLAN Id
#else
    ptr[2] = 0x0;  ptr[3] = 0x0; // dont't use VLAN Id
#endif
    ptr += 4;
  }
  // Ethernet type mask, same for all protocols
  ptr[0] = 0xFF; ptr[1] = 0xFF;
  ptr += 2; // Ethernet type

  // Do some protocol specific stuff
  switch (nwProto) {
    case L2:
      break; // nothing to do here
    case L3_IPv4:
      ptr[0] = 0xFF; ptr[1] = 0x00;   // IP 0x4500
      ptr[9] = 0xFF;                  // UDP 0x11
      break;
    case L3_IPv6:
      ptr[0] = 0xF0;                  // IP version
      ptr[6] = 0xFF;                  // UDP 0x11
      break;
    default: // unknown nw proto
      break;
  }
  ptr += ipLen[nwProto]; // ip packet

  // build the UDP packet
  PktUDP udp;
  ::memset(udp.hdr, 0, sizeof(udp.hdr));
  uint8_t port = recv ? 2 : 0; // UDP dest port (2) or src port (0)
  udp.hdr[port] = 0xFF; udp.hdr[port + 1] = 0xFF; // Port number
  if (dlyReqOnly) {
    // Do not respond to delay requests with TLVs.
    // A delay request without TLVs is 52 octets long
    udp.hdr[4] = 0xFF; udp.hdr[5] = 0xFF;
  }
  // copy udp packet to correct place
  switch (nwProto) {
    case L2:
      break; // nothing to do here
    case L3_IPv4:
    case L3_IPv6:
      ::memcpy(ptr, &udp, sizeof(udp.hdr));break;
    default: // unknown nw proto
      break;
  }
  ptr += udpLen[nwProto]; // udp packet

  if (timeProto == CTimeProtoPTPv2) {

    // build the PTP packet
    PktPTP ptp;
    ::memset(&ptp, 0, sizeof(ptp));
    if (dlyReqOnly) ptp.hdr[0] = 0x01; // Transport specific/ MsgType 0-3
    else ptp.hdr[0] = 0x0C; // Transport specific/ MsgType 0-3
    if (nwProto != L3_IPv6) ptp.hdr[1] = 0x0F; // reserved/ Version 2
    ptp.hdr[4] = 0xFF; // domain
    if (nwMode == 1) ptp.hdr[6] = 0x04; // Unicast Flag

    // copy ptp packet to correct place
    ::memcpy(ptr, &ptp, sizeof(PktPTP));
  } else if (timeProto == CTimeProtoNTP) {

    // Build the NTP packet
    PktNTP ntp;
    ::memset(&ntp, 0, sizeof(PktNTP));

    // copy ptp packet to correct place
    ::memcpy(ptr, &ntp, sizeof(PktNTP));
  } else if (timeProto == CTimeProtoPTPv1) {

    // Build the PTPv1 packet
    PktPTP ptp;
    ::memset(&ptp, 0, sizeof(ptp));
    ptp.hdr[0] = 0x0C;
    ptp.hdr[1] = 0x0F; // reserved/ Version 1
    // copy ptp packet to correct place
    ::memcpy(ptr, &ptp, sizeof(PktPTP));
  }

  return (uint8_t *)&msk;
}

/// extractOff

/// Returns a pointer to an array with the extraction offsets.
uint8_t *TSConfig::extractOff(){

  // We need different values for PTPv1
  if (timeProto == CTimeProtoPTPv1) {
    exOff[0] = exOffset0PTPv1[nwProto];
    exOff[1] = exOffset1PTPv1[nwProto];
  } else {
    exOff[0] = exOffset0[nwProto];
    exOff[1] = exOffset1[nwProto];
  }

  if (vlanId) {
    exOff[0] += 4;
    exOff[1] += 4;
  }
  return &exOff[0];
}

/// extractLen

/// Returns a pointer to an array with the extraction length.
uint8_t *TSConfig::extractLen() {

  // We need different values for PTPv1
  if (timeProto == CTimeProtoPTPv1)
    exLen[0] = exLength0PTPv1;
  else
    exLen[0] = exLength0;

  exLen[1] = exLength1;
  return &exLen[0];
}

/// Return the position of the timestamp in the packet. Required for
/// programmable one step timestamper
uint8_t TSConfig::tsPosition() {
  if (timeProto == CTimeProtoPTPv2)
    return vlanId ? tsPosPTP[nwProto] + 4 : tsPosPTP[nwProto];
  else if (timeProto == CTimeProtoNTP)
    return vlanId ? tsPosNTP[nwProto] + 4 : tsPosNTP[nwProto];

  return 0;
}

/// Return the position of the UDP checksum in the packet. Required for
/// programmable one step timestamper
uint8_t TSConfig::udpPosition() {
  return vlanId ? udpChkPos[nwProto] + 4 : udpChkPos[nwProto];
}
