/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#include "syn1588_ifc.h"
#include "syn1588_impl.h"
#include "syn1588_check_register_access.h"

#define I2C_FUNCTIONS_L 0x20C0
#define I2C_FUNCTIONS_H 0x20FF

bool Syn1588Ifc::m_alreadyLogged = false;

/// Interface to implement register access validity checks
Syn1588Ifc::Syn1588Ifc(PTP_LogIfc *log, const uint8_t card) : m_log(log) {

  if (!m_alreadyLogged) {
    m_log->Always("Syn1588Ifc requires at least:\n"
                  "- linux driver version 1.4-15-g05b7283\n"
                  "- windows driver version 10/05/2017, 10.9.16.182\n");
    m_alreadyLogged = true;
  }

  m_syn1588Impl = new Syn1588Impl(log, card);
  m_log->Debug("Syn1588 object created for card number %u\n", card);
  if (!present()) return;
  m_log->Debug("Checking build number and capabilities\n");
#ifdef __SYN1588_IP_CORE__
  m_buildnum = 0;
  m_cap1 = m_syn1588Impl->readReg(CAPABILITIES1);
  m_macCap0 = 0;
#else
  m_buildnum = m_syn1588Impl->readReg(VR_ETH_BUILD);

  // check if build allows for  capability register access, else set all capabilities to 0
  if (m_buildnum >= CAP_REG_BUILD_VER) m_cap1 = m_syn1588Impl->readReg(CAPABILITIES1);
  else m_cap1 = 1; // LSB indicates support for MIITS0, which according to EM is always safe to assume exists
  if (m_buildnum >= MAC_CAP_BUILD_VER) m_macCap0 = m_syn1588Impl->readReg(VR_ETH_CAPABILITIES_0);
  else m_macCap0 = 0;
#endif// __SYN1588_IP_CORE__
}

Syn1588Ifc::~Syn1588Ifc() {
  m_log->Debug("deleting syn1588Ifc!\n");
  delete m_syn1588Impl;
}

bool Syn1588Ifc::present() {
  return m_syn1588Impl->present();
}

uint32_t Syn1588Ifc::readRegRaw(const uint32_t addr, Return_e * const err) {
  return m_syn1588Impl->readReg(addr, err);
}

uint32_t Syn1588Ifc::readReg(const uint32_t addr, Return_e * const err) {
  if (check(addr) == ENoErr) {
    return m_syn1588Impl->readReg(addr, err);
  } else if (err) *err = EErr;
  return 0; // Returns 0 if register cannot be read
}

Return_e Syn1588Ifc::writeReg(const uint32_t addr, const uint32_t data) {
  if (check(addr) == ENoErr) {
    return m_syn1588Impl->writeReg(addr, data);
  } else {
     m_log->Error("Error writing to address %04X\n", addr);
     return EErr;
  }
}

Return_e Syn1588Ifc::setBit(const uint32_t addr, const uint32_t mask) {
  if (check(addr) == ENoErr) {
    return m_syn1588Impl->setBit(addr, mask);
  } else {
    m_log->Error("Error setting bit at address %04X\n", addr);
    return EErr;
  }
}

Return_e Syn1588Ifc::clearBit(const uint32_t addr, const uint32_t mask) {
  if (check(addr) == ENoErr) {
    return m_syn1588Impl->clearBit(addr, mask);
  } else {
    m_log->Error("Error clearing bit at address %04X\n", addr);
    return EErr;
  }
}

Return_e Syn1588Ifc::registerEvent(uint32_t event) {
  return m_syn1588Impl->registerEvent(event);
}

Return_e Syn1588Ifc::disableEvent(uint32_t event) {
  return m_syn1588Impl->disableEvent(event);
}

uint32_t Syn1588Ifc::waitForEvent(uint32_t timeout, Return_e * const err) {
  return m_syn1588Impl->waitForEvent(timeout, err);
}

uint8_t Syn1588Ifc::getType() {
  return m_syn1588Impl->getType();
}

SYN1588_DEV_HANDLE Syn1588Ifc::getHandle() {
  return m_syn1588Impl->getHandle();
}

Return_e Syn1588Ifc::check(const uint32_t addr) {
#ifdef __SYN1588_IP_CORE__
  if (Syn1588Check::checkCapabilities(addr, m_cap1, m_macCap0)) {
    if (addr == Syn1588Ifc::TIME_L ||addr == Syn1588Ifc::HWBUILD) {
      m_log->Info("Accessed register address %04X is deprecated and shouldn't be used\n", addr);
    }
    return ENoErr;
  }
  return EErr;
#else
  if ((addr >= I2C_FUNCTIONS_L && addr <= I2C_FUNCTIONS_H && m_buildnum < I2C_REG_BUILD_VER) ||
      (addr == Syn1588Ifc::AUXILIARY && m_buildnum < AUX_REG_BUILD_VER) ||
      (addr == VR_ETH_CAPABILITIES_0 && m_buildnum <= MAC_CAP_BUILD_VER))  {
    m_log->Error("Build number doesn't support register at address %04X\n", addr);
    return EErr;
  } else if (Syn1588Check::checkCapabilities(addr, m_cap1, m_macCap0)) {
    if (addr == Syn1588Ifc::TIME_L || addr == Syn1588Ifc::HWBUILD) {
      m_log->Info("Accessed register address %04X is deprecated and shouldn't be used\n", addr);
    }
    return ENoErr;
  }
  return EErr;
#endif//__SYN1588_IP_CORE__
}

bool Syn1588Ifc::canProvidePacketAttachedTimestamp(void) const {
  return m_syn1588Impl->canProvidePacketAttachedTimestamp();
}
