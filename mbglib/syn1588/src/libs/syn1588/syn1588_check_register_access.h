/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_SYN1588_CHECK_REGISTER_ACCESS
#define INC_SYN1588_CHECK_REGISTER_ACCESS

#include "syn1588_ifc.h"
#include "basictypes.h"


class Syn1588Check {
  private:
    // struct for register addresses, the corresponding capability register and
    // their bit/index within the register
    struct capabilityStruct {
      uint32_t regAddr;
      uint32_t capRegAddr;
      uint32_t testBit;
    };

    // table containing all addresses that require certain capabilities
    const static struct capabilityStruct registerLookupTable[];

    static const size_t registerLutSize;

    /// function searches for the index of an address in the registerLookupTable
    /// @param address to find an index for
    /// @return returns the index at which the address can be found on success or -1 if the address is not in the table
    static int findInTable(uint32_t addr);

  public:
    /// checks if an address can be safely accessed by reading from the corresponding capability register
    /// according to the registerLookupTable
    /// @param addr address of the register to check
    /// @param Syn1588Impl syn1588 interface to be checked
    /// @return returns EErr if the capability bit is 0 or
    ///         returns ENoErr if the address is not in the table or the capability bit is 1
    static uint8_t checkRegisterAccess(uint32_t addr, Syn1588Impl *syn1588);

    /// checks if an address can be safely accessed by checking the provided capability register values
    /// @param addr the address to check
    /// @param cap1 content of the CAPABILITIES1 register
    /// @param mac_cap0 content of the MAC_CAPABILTIES_0 register, can be accessed via VR_ETH_CAPABILITIES_0
    /// @return returns EErr if the capability bit is 0 or
    ///         returns ENoErr if the address is not in the table or the capability bit is 1
    static uint8_t checkCapabilities(uint32_t addr, uint32_t cap1 = 0, uint32_t mac_cap0 = 0);

  private:
};

#endif
