/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

/// @defgroup syn1588API "Syn1588 API"
/// @brief The syn1588 API
///
/// The syn1588 API may be used for direct register access
/// and exteded functionality of the syn1588 PCIe NIC.
/// The syn1588 API allows the user to access registers and configure functions
/// of the syn1588® PCIe NIC directly.<br>This includes but is not limited to
/// the Period, Timer, and Event functionality as well as Timestamper
/// configuration.

#ifndef INC_SYN1588
#define INC_SYN1588

#include "syn1588_ifc.h"
#include "log_ifc.h"
#include "ts_config.h"

#if defined __NIOS__ && !defined _NO_ANALOG_RATE_ADJUST_
#include "i2c_tcxo.h"
#endif

/// Syn1588 Application Programming Interface Class

/// This class inherits from Syn1588Ifc and implements the low-level
/// clock access functions in an operating system independet fashion.
/// It may be used to access the features of syn1588(R) hardware like
/// getting/setting the current time, configuring the timestamper, or
/// programming a frequency output.
///
/// @ingroup syn1588API
class Syn1588 {

  private:

    static const uint8_t CEepromMACLength    = 3;
    static const uint8_t CEepromSerialLength = 4;

    Syn1588Ifc m_syn1588Ifc; ///< hardware interface object
    PTP_LogIfc *m_log;       ///< logger
    uint16_t m_clkmul;       ///< clock multiplication factor
    uint32_t m_freq;         ///< clock frequency
    uint8_t  m_clkscale;     ///< clock scaling (shift left) factor
    bool m_newTSStruct;      ///< indicates new timestamp struct without 'sec_h' field
    bool m_ultimateTS;       ///< indiactes the ultimate version of the timestamper
    bool m_prog1TS;          ///< indicates that the programmable one step ts is available
    bool m_initTS;           ///< indicates if the timestamper was initialized (true == PTP Stack)
    bool m_subOffset;        ///< indicates if the hardware has support for subracting offsets

#if defined __NIOS__ && !defined _NO_ANALOG_RATE_ADJUST_
    I2C_tcxo *m_tc;          ///< interface used to control TCXO
    TimeInterval_t m_drift;  ///< store last drift used in adjustRate()
#endif

  public:
    /// @name Rate Adjustment Modes
    /// Adjust the rate in digital or analog mode. Right now, only digital mode
    /// should be used.
    /// @{
    static const uint8_t INIT_ADJUST_MODE    = 2; ///< Initialize adjust mode
    static const uint8_t DIGITAL_ADJUST_MODE = 1; ///< digital adjust mode
    static const uint8_t ANALOG_ADJUST_MODE  = 0; ///< analog adjust mode

    /// @}

    static const uint8_t CSerialStringLength = 17;
    static const uint8_t CRevStringLength    = 8;

    /// TsInstance_t
    ///
    /// This instance can be used to access the different Timestamper instances
    /// in the syn1588 hardware. There are up to three different timestampers available:
    /// Two directly in the clock core and one integrated into the MAC. Please note that
    /// not all instances are always physically available (contact Oregano Systems for details).
    /// The system timestamper (TsSys) as well as the MAC timestamper (TsMac) are usually used
    /// for PTP timestamping (the syn1588 PTP Stack always uses one of those timestampers, depending
    /// on the hardware version). For custom timestamper applications of arbitrary packets, the
    /// user timestamper (TsSys) should be used.
    typedef enum {
      TsSysRx,  ///< Id for the System Receive Timestamper (MIITS0)
      TsSysTx,  ///< Id for the System Transmit Timestamper (MIITS0)
      TsUsrRx,  ///< Id for the User Receive Timestamper (MIITS1)
      TsUsrTx,  ///< Id for the User Transmit Timestamper (MIITS1)
      TsMacRx,  ///< Id for the Receive Timestamper integrated in the MAC
      TsMacTx,  ///< Id for the Transmit Timestamper integrated in the MAC
      TsMac2Rx, ///< Id for the 2nd Receive Timestamper integrated in the MAC
      TsMac2Tx  ///< Id for the 2nd Transmit Timestamper integrated in the MAC
#ifdef _SYN1588_EXT_TS_REGS_
      ,
      TsMii2Rx, ///< Id for the Receive Timestamper MIITS2
      TsMii2Tx, ///< Id for the Transmit Timestamper MIITS2
      TsMii3Rx, ///< Id for the Receive Timestamper MIITS3
      TsMii3Tx, ///< Id for the Transmit Timestamper MIITS3
      TsMii4Rx, ///< Id for the Receive Timestamper MIITS4
      TsMii4Tx, ///< Id for the Transmit Timestamper MIITS4
      TsMii5Rx, ///< Id for the Receive Timestamper MIITS5
      TsMii5Tx  ///< Id for the Transmit Timestamper MIITS5
#endif //_SYN1588_EXT_TS_REGS_
    } TsInstance_t;

    /// TsBase_t
    ///
    /// This is the basic datatyp for selecting a certain timestamper
    /// unit. It may be used to select the appropriate Ts instance, e.g. when
    /// using the arrays \a TsTimeCtrl, \a TsRx, or \a TsTx. For accessing the
    /// appropriate time control register of the timestamper instance the following
    /// syntax should be used: \a TsTimeCtrl[Syn1588::TsSys]
    typedef enum {
      TsSys,  ///< M-Core Timestamper 0 (MIITS0), used for PTP applications
      TsUsr,  ///< M-Core Timestamper 1 (MIITS1), used for user applicatsion
      TsMac,  ///< MAC Timestamper, used for PTP applications
      TsMac2  ///< 2nd MAC Timestamper, used for PTP applications
#ifdef _SYN1588_EXT_TS_REGS_
      ,
      TsMii2, ///< M-Core Timestamper 2 (MIITS2)
      TsMii3, ///< M-Core Timestamper 3 (MIITS3)
      TsMii4, ///< M-Core Timestamper 4 (MIITS4)
      TsMii5  ///< M-Core Timestamper 5 (MIITS5)
#endif //_SYN1588_EXT_TS_REGS_
    } TsBase_t;

  private:
    TsBase_t m_PTPTimestamper; ///< current system timestamper used for PTP operation

    /// signals if the PTP Timestamper can do 1-step timestamping (true) or not (false)
    bool ptpTimestamperIs1StepCapable();

  public:

    /// Constructor
    Syn1588(PTP_LogIfc *log, const uint8_t card);

    /// Probe function checks if syn1588 HW is available.
    static Syn1588* probe(PTP_LogIfc* log, const uint8_t* id);

    /// @{
    /// @name Public PTPHwClockIfc compatible functions
    /// The following functions are compatible with the PTP_HwClockIfc.
    /// The Syn1588 class does not inherit from the PTP_HwClockIfc directly.
    /// These functions are wrapped via Syn1588HwClock which inherits from the
    /// PTP_HwClockIfc to keep the Syn1588 class independent of the PTP_HwClockIfc.

    /// Initialization function
    int8_t init(TSConfig cfg);

    /// Return the device file handle
    SYN1588_DEV_HANDLE getHandle() { return m_syn1588Ifc.getHandle(); }

    /// Safely get et current time
    int8_t getSafeTimestamp(Timestamp_t &ts);

    /// Safely get current time and sync status
    int8_t getSafeTimeAndStatus(TimestampStatus_t &tss);

    /// Get actual clock time
    int8_t getTime(Timestamp_t &ts);

    /// Set the clock time
    int8_t setTime(const Timestamp_t &ts);

    /// Return PTP sync status
    int8_t getPtpSyncStatus(SYN1588_PTP_SYNC_STATUS &st);

    /// Return leap second info
    int8_t getLeapInfo(uint32_t &leap_sec, uint32_t &leapapply_time);

    /// Get a Timestamp
    int8_t getTimestamp(bool recv,
                        Timestamp_t &ts,
                        TimeInterval_t &subNs,
                        const PortId_t *pId,
                        const uint16_t seqId);

    bool providesPTPPacketAttachedTimestamp(void) const;

    /// Adjust clock rate
    void adjustRate(const TimeInterval_t &drift, uint8_t , int8_t ival = 0);

    /// Compensate offset
    int8_t compensateOffset(const TimeInterval_t &offset);

    /// Return current drift
    int8_t getDrift(TimeInterval_t &drift, uint8_t mode);

    /// Get (PTP) clock identifier
    int8_t getId(uint8_t *id);

    /// Get a string with HW version information
    int8_t getVersionInfo(char *string);

    /// Get the hardware capabilities
    uint16_t getCapabilities();

    /// @}

    /// @{
    /// @name Regular public functions
    ///

    /// Disable delivery of transmit timestamp for the PTP Timestamper
    void disableTxTsDelivery();

    /// Enable delivery of transmit timestamp for the PTP Timestamper
    void enableTxTsDelivery();

    /// Get syn1588 clock core version
    int8_t getVersion(int8_t *version);

    /// Get the Version ID of the Ethernet MAC.
    int8_t getMacVersion(char *version);

    /// Get the Mac address from the EEPROM
    int8_t getEEPROMMacAddress(char *macAddress);

    /// Get the Revision from EEPROM
    int8_t getEEPROMBoardRevision(char* revisionNum);

    /// Get Serial Number from EEPROM
    int8_t getEEPROMSerialNumber(char* serialNum);

    int8_t getEEPROMBoardCapability(uint32_t &boardCap,
                                    uint32_t &rawBoadCapReg3210,
                                    uint32_t &rawBoadCapReg7654);

    /// Get the Build ID of the HW design.
    int8_t getBuildID(uint32_t* d);

    /// Check if there is syn1588 hardware available
    bool present() { return m_syn1588Ifc.present(); }

    /// Get the syn1588Ifc object for direct register access
    Syn1588Ifc* getSyn1588Ifc() { return &m_syn1588Ifc; }

    /// Get the currently selected timestamper instance
    TsBase_t getTsBase() { return m_PTPTimestamper; }

    /// Configure timestamper
    int8_t configTimestamper(TsInstance_t ti, uint8_t *pat, uint8_t *msk, uint8_t len,
                             uint8_t *exOff, uint8_t *exLen);

    /// Get a single timestamp, non blocking
    int8_t getSingleTimestamp(TsInstance_t ti, Timestamp_t &ts, TimeInterval_t &subNs,
                              uint8_t *pat, const uint8_t len);

    /// Poll for a timestamp
    int8_t pollTimestamp(TsInstance_t ti, Timestamp_t &ts, TimeInterval_t &subNs,
                         const uint8_t *pat, const uint8_t len);

    /// Clear timestamp FIFO
    int8_t clearFifo(TsInstance_t ti);

    /// Set the initial rate according to the clock frequency
    void initRate(const TimeInterval_t &drift, uint8_t ctrl_mode);

    /// Set the step
    void setStep(uint32_t sh, uint32_t sl);

    /// Generate trigger/period output
    int8_t generateEvent(const uint8_t port, const int8_t nr,
                         const Timestamp_t &start, const TimeInterval_t &per);

    /// Generate trigger/period output (raw period access)
    int8_t generateEvent(const uint8_t port, const int8_t nr,
                         const Timestamp_t &start, uint32_t per_l, uint32_t per_h);

    /// Generate 1pps output
    int8_t generate1pps(const uint8_t port);

    /// Generate IRIG-B output
    int8_t generateIRIGB(const uint8_t port);

    /// Set IO-Matrix
    int8_t setIomatrix(const uint8_t dst, const uint8_t src);

    /// Returns the correct value for the SMA Port sources in the IO-Matrix
    static uint8_t smaSrc(uint8_t smaPort);

    /// Returns the correct destination bit address for the SMA Port destination
    /// in the IO-Matrix
    static uint8_t smaDst(uint8_t smaPort);

    /// Returns the correct destinaton bit address for the event destination
    /// in the IO-Matrix
    static uint8_t evtDst(uint8_t evtNr);

    /// Clear the event fifo for event 0
    void clearEventFifo();

    /// Register event
    int8_t registerEvent(const uint8_t port, const int8_t nr);

    /// Disable a registered event
    int8_t disableEvent(const uint8_t port, const uint8_t nr);

    /// Disable a set period timer (0 or 1)
    int8_t disablePeriod(const uint8_t nr);

    /// Disable a set trigger timer (0 or 1)
    int8_t disableTrigger(const uint8_t nr);

    /// Wait for event
    int8_t waitForEvent(uint8_t &mask, Timestamp_t *ts, const TimeInterval_t &tout);

    /// Poll for event on event0 register
    int8_t pollEvent0(Timestamp_t &last, const TimeInterval_t &tout);

    /// Set the the timestamper instance
    int8_t setTsBase(TsBase_t base);

    /// Get Tx Ts instance according to current ts base
    TsInstance_t getTsTx();

    /// Get Rx Ts instance according to current ts base
    TsInstance_t getTsRx();

    /// Initialize System Timestamper for PTP packets
    int8_t initSystemTimestamper(TSConfig tsConfig);

    /// Clear Event0 Fifo
    void clearEvent0Fifo();

    /// Destructor
    ~Syn1588();

    /// @}

};

#endif
