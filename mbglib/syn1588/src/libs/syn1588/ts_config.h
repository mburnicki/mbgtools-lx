/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#ifndef INC_TS_CONFIG
#define INC_TS_CONFIG

#include "basictypes.h"

/// Pattern configuration structure

/// @brief Timestamper configuration structure
struct TSConfig {
public:

  /// @name Network protocol options
  /// @{
  static const uint16_t L2 = 3; ///< IEEE 802.3/Ethernet Protocol (Annex F of IEEE1588)
  static const uint16_t L3_IPv4 = 1;  ///< UDP/IPv4 Protocol (Annex D of IEEE1588)
  static const uint16_t L3_IPv6 = 2;  ///< UDP/IPv6 Protocol (Annex E of IEEE1588)

  /// @}

  /// @name Time protocol options
  /// @{
  static const uint8_t CTimeProtoPTPv2 = 0; ///< PTPv2 time protocol
  static const uint8_t CTimeProtoNTP   = 1; ///< NTP time protocol
  static const uint8_t CTimeProtoPTPv1 = 2; ///< PTPv1 time protocol

  /// @}

  static const uint16_t CVlanEnableMask = 0x8000; ///< VLAN enable mask (used for VLAN 0)

  uint8_t timeProto;    ///< time protcol
  uint16_t nwProto;     ///< Network Protocol
  uint8_t nwMode;       ///< Network mode (MC, UC,...)
  uint8_t domain;       ///< PTP domain
  uint16_t vlanId;      ///< VLAN identifier
  uint8_t twoStep;      ///< 2 step flag
  bool initTimestamper; ///< initialize timestamper flag
  bool initRate;     ///< initialize rate
  bool dlyReqOnly;   ///< determines if only dly reqs are timestamped
  bool recv;         ///< determines if the config is RX (true) or TX (false)


  /// Default Constructor
  TSConfig() {
    timeProto = CTimeProtoPTPv2;
    nwProto = L3_IPv4;
    nwMode  = 0;
    domain  = 0;
    vlanId  = 0;
    twoStep = 0;
    initTimestamper = 0;
    initRate = 0;
    dlyReqOnly = 0;
    recv = 1;
  }

  /// Build the pattern
  uint8_t *pattern(uint8_t *len);

  /// Build the mask
  uint8_t *mask();

  /// Get the exraction offset
  uint8_t *extractOff();

  /// Get the extraction length
  uint8_t *extractLen();

  /// Get the timestamp position
  uint8_t tsPosition();

  /// Get UDP checksum position
  uint8_t udpPosition();

private:

#pragma pack (1)
struct PktNTP {
   uint8_t hdr[4];
}__PACKME__;
struct PktPTP {
   uint8_t hdr[8];
}__PACKME__;
struct PktUDP {
   uint8_t hdr[8];
   PktPTP ptp;
}__PACKME__;
struct PktIP {
   uint8_t hdr[20];
   PktUDP udp;
}__PACKME__;
struct PktIPv6 {
   uint8_t hdr[40];
   PktUDP udp;
}__PACKME__;
struct PktEthernet {

  uint8_t hdr[14];
   union {
      PktIP ip;
      PktIPv6 ipv6;
      PktPTP ptp;
   };
   uint8_t dummy[4]; // space for Vlan insertion
   uint8_t reserved[6]; // for HW patch - see also configTimestamper in syn1588 class
}__PACKME__;

PktEthernet pat, msk;
uint8_t exOff[2], exLen[2];


#pragma pack()

};

#endif
