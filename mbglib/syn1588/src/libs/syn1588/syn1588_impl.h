/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_SYN1588_IMPL
#define INC_SYN1588_IMPL

#include "basictypes.h"
#include "log_ifc.h"

#include <stddef.h>

class Syn1588Impl {

  protected:
    PTP_LogIfc *m_log;   // logging pointer
    // TODO by martin: The generic type SYN1588_DEV_HANDLE
    // could be used instead of the union below.
    union {
      int fd;            // unix uses file descriptors
      void* handle;      // windows uses handles
    } m_fd;              // device file descriptor
    void *m_evSet;       // event set
    bool m_isSyn1588;    // indicates Syn1588 hardware support
    uint8_t m_hwType;    // indicates the hardware type

  public:
    /// Constructor
    Syn1588Impl(PTP_LogIfc *log, const uint8_t card);

    /// Destructor
    ~Syn1588Impl();

    /// Returns true if Syn1588 NIC has been found.
    bool present() { return m_isSyn1588; }

    /// Return the device file handle
    SYN1588_DEV_HANDLE getHandle() {
#ifdef WIN32
      return m_fd.handle;
#else  // Linux / POSIX
      return m_fd.fd;
#endif
    }

    /// returns the hw type:
    /// 0: unknown
    /// 1: PCI NIC
    /// 2: PCIe NIC
    /// 4: VIP
    uint8_t getType();

    /// Register read
    /// @param addr register address
    /// @param [out] err ENoErr is written on success EErr otherwise
    /// @return register value
    uint32_t readReg(const uint32_t addr, Return_e * const err = NULL);

    /// Register write
    /// @param addr register address
    /// @param data register value
    /// @return ENoErr on success EErr otherwise
    Return_e writeReg(const uint32_t addr, const uint32_t data);

    /// Set register bits according to \a mask
    /// @param addr register address
    /// @param mask bitmask with bits to set
    /// @return ENoErr on success EErr otherwise
    Return_e setBit(const uint32_t addr, const uint32_t mask);

    /// clear register bits according to \a mask
    /// @param addr register address
    /// @param mask bitmask with bits to clear
    /// @return ENoErr on success EErr otherwise
    Return_e clearBit(const uint32_t addr, const uint32_t mask);

    /// Register Event (for select processing)
    /// @param event bitmask of interrupt bits to set
    /// @return ENoErr on success EErr otherwise
    Return_e registerEvent(uint32_t event);

    /// Disable a registered event
    /// @param [in] event bitmask of interrupt bits to remove
    /// @return ENoErr on success EErr otherwise
    Return_e disableEvent(uint32_t event);

    /// Wait for an event
    /// @param timeout Time in milliseconds for blocking wait for an interrupt
    /// @param [out] err ENoErr is written on success EErr otherwise
    /// @returns bitmask of set interrupt bits
    uint32_t waitForEvent(uint32_t timeout, Return_e * const err = NULL);

    /// signals if the underlying system can provide timestamps with a packet
    bool canProvidePacketAttachedTimestamp(void) const;
};

#endif
