/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2007
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 ******************************************************************************
 * $Id: syn1588_c_api.h 366 2014-09-29 07:12:57Z mroczkowski $
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#define _SYN1588_C_API
  #include "syn1588_c_api.h"
#undef _SYN1588_C_API

#include "log/log.h"
#include "syn1588/syn1588.h"
#include "syn1588_ifc.h"
#include "datatypes.h"

#if !defined( MBG_INVALID_DEV_HANDLE )
  #if defined( _WIN32 ) || defined( MBG_TGT_WIN32 )
    #define MBG_INVALID_DEV_HANDLE   INVALID_HANDLE_VALUE  // Originally from mbg_tgt.h
  #else
    #define MBG_INVALID_DEV_HANDLE   -1  // Assuming POSIX.
  #endif
#endif

#if !defined( SUPP_SYN1588_SHM )
  #define SUPP_SYN1588_SHM  0
#endif

#if SUPP_SYN1588_SHM
  #include "sharedmem_api.h"
  #include "config_ifc.h"
#endif

#include <stdio.h>

#if !defined( DEBUG_SYN1588_C_API )
  #if ( defined( DEBUG ) && DEBUG )
    #define DEBUG_SYN1588_C_API  1  // Can be 0 or > 0.
  #else
    #define DEBUG_SYN1588_C_API  0  // Always 0.
  #endif
#endif


static Syn1588       *m_synObj[Syn1588Ifc::CMaxCards];
static PTP_Log       *m_log = NULL;
static PTP_ConfigLog  m_logCfg;

#if SUPP_SYN1588_SHM && defined( _SHARED_MEM_ )
  static PTP_SharedMemoryAPI *m_sharedMemory[Syn1588Ifc::CMaxCards];
#endif



/// Function to check if a card is initialized.
/// @param card: the number of the card requested
/// @return On success \a ENoErr is returned else \a EErr
///
static
SYN1588_RET_VAL cardExists(SYN1588_DEV_IDX card) {
  if(card >= Syn1588Ifc::CMaxCards) {
    if (m_log) m_log->Warning("Warning: Requested card %d exceeds maximum card number of %d!\n",
                   card, Syn1588Ifc::CMaxCards);
    return EErr;
  }
  if(!m_synObj[card]) {
    if (m_log) m_log->Warning("Warning: Requested card %d is not initialized \n", card);
    return EErr;
  }
  return ENoErr;
}



#if SUPP_SYN1588_SHM && defined( _SHARED_MEM_ )

static /*HDR*/
SYN1588_RET_VAL syn1588_sm_read_context( SYN1588_DEV_IDX card, Context_t *p_c )
{
  SYN1588_RET_VAL ret;
  PTP_SharedMemoryAPI *p = m_sharedMemory[card];

  if ( p == NULL )
    return EErr;

#ifdef CONFIGURE_PTP
  PTP_SharedMemoryAPI::AccessMode_e accMode = PTP_SharedMemoryAPI::EMaster;
#else
  PTP_SharedMemoryAPI::AccessMode_e accMode = PTP_SharedMemoryAPI::ESlave;
#endif

#ifdef WAIT_FOR_SHMEM_UPDATE
  ret = p->readPtpContext(&c, accMode,
#  ifndef ONLY_802_1AS_DATASET
                    PTP_IoIfc::rdFlagWaitForUpdate);
#  else // !ONLY_802_1AS_DATASET
                    PTP_IoIfc::rdFlagWaitFor802_1as);
#  endif // !ONLY_802_1AS_DATASET

#else // WAIT_FOR_SHMEM_UPDATE
  ret = p->readPtpContext( p_c, accMode );
#endif // WAIT_FOR_SHMEM_UPDATE

  return ret;

}  // syn1588_sm_read_context



static __mbg_inline /*HDR*/
SYN1588_RET_VAL syn1588_sm_init( SYN1588_DEV_IDX card )
{
  PortId_t id;

  m_synObj[card]->getId( id.clkId );
  id.port = 1;  // FIXME TODO Do we have to set up a real port number?

  if ( m_sharedMemory[card] )
    delete m_sharedMemory[card];

  // Create shared memory object
  m_sharedMemory[card] = new PTP_SharedMemoryAPI();

  PTP_SharedMemoryAPI *p = m_sharedMemory[card];

  if ( p == NULL )
    return EErr;

#ifdef CONFIGURE_PTP
  PTP_SharedMemoryAPI::AccessMode_e accMode = PTP_SharedMemoryAPI::EMaster;
#else
  PTP_SharedMemoryAPI::AccessMode_e accMode = PTP_SharedMemoryAPI::ESlave;
#endif

  if ( p->open( accMode, id ) != ENoErr )
  {
    if (m_log) m_log->Error("Failed to open shared memory\n");
    return EErr;
  }

  // set appropriate timeouts (in us) for shared memory operations
  // ESemUpdTout is only necessary if WAIT_FOR_SHMEM_UPDATE is defined
  // default values for both timeouts are 0 ( = wait forever)
  p->setTout(PTP_IoIfc::ESemUpdTout, 1200000);
  p->setTout(PTP_IoIfc::ESemRwTout, 500000);

  // This is related to the "in bounds" interface of the shared memory.
  // TODO Not sure, if we need this.
  BoundaryIf_t inBounds;
  p->readSyncBoundary(&inBounds);

  // TODO This structure is probably what we need for monitoring.
  Context_t c;
  syn1588_sm_read_context( card, &c );

  return ENoErr;

}  // syn1588_sm_init



static __mbg_inline /*HDR*/
int32_t syn1588_get_offs_tai_utc_from_sm( SYN1588_DEV_IDX card )
{
  Context_t ctx;

  SYN1588_RET_VAL ret = syn1588_sm_read_context( card, &ctx );

  if ( ret == ENoErr )
    if ( ctx.timePropDataset.flags & PTP_ConfigIfc::CCurrUtcOffsValid )
      return ctx.timePropDataset.currUTCOffs;

  return 0;

}  // syn1588_get_offs_tai_utc_from_sm

#endif  // SUPP_SYN1588_SHM && defined( _SHARED_MEM_ )



/*HDR*/
/// Initialize the C API
///
/// NOTE: if this function is called after configuring one or more devices,
///       the configuration is lost.
///
/// This function brings the system into a defined state:
/// If any Syn1588 objects are existing they are destroyed.
/// If the logger is initialized it will be created anew.
/// @param logLevel: the log level for the logger
/// @return none
void syn1588_init_c_api(uint8_t logLevel) {

  // destroy all objects
  for(uint8_t i = 0; i < Syn1588Ifc::CMaxCards; i++) {
    if(m_synObj[i]) delete m_synObj[i];
    m_synObj[i] = NULL;
  }

  // initialize logger
  if(m_log) delete m_log;
  m_logCfg.logLevel = logLevel;
  m_logCfg.useFileLogging = false;
  #if !defined( NO_LOGGING )
    m_log = new PTP_Log(&m_logCfg);
  #endif
}


/*HDR*/
/// Probe for a SYN1588 card identified by its MAC address.
///
/// This function tries to find a SYN1588 card with the MAC address
/// @a mac. If a card is found, it returns the card's index. Otherwise,
/// -1 is returned.
/// ::syn1588_init still has to be called for the respective card.
/// @param mac: MAC address of the desired SYN1588 card
/// @return On success, the card index, otherwise -1 otherwise
int8_t syn1588_probe(const uint8_t *mac) {

  // Convert mac address to PTPv2 clock id format
  uint8_t hw_id[8];

  Syn1588* syn1588 = NULL;
  SYN1588_DEV_IDX card = 0;
  int8_t ret = -1;

  // save log level and set to zero
  uint8_t logLevel = 0;
  if (m_log) {
    logLevel = m_log->getLevel();
    m_log->setLevel(0);
  }
  while (1) {

    syn1588 = new Syn1588(m_log, card);
    if (!syn1588->present()) break;

    // We found syn1588 HW, check ClockID
    syn1588->getId(hw_id);
    if ((::memcmp(mac, hw_id, 3) == 0) && (::memcmp(&mac[3], &hw_id[5], 3) == 0)) {
      ret = card;
      break;
    }
    delete syn1588;
    card++;
  }

  //restore log level
  if (m_log) m_log->setLevel(logLevel);
  delete syn1588;
  return ret;
}


/*HDR*/
/// Initialize a syn1588(R) card.
///
/// Create a SYN1588 object for the card, where the index number
/// is 0 for the first card found by the operating system.
/// @param card: The index number of the card requested.
/// @return On success @a ENoErr is returned else @a EErr
///
SYN1588_RET_VAL syn1588_init(SYN1588_DEV_IDX card) {
  SYN1588_RET_VAL ret = ENoErr;

  if(card >= Syn1588Ifc::CMaxCards) {
    if (m_log) m_log->Error("Error: Exceeding the maximum card number of %d!\n", Syn1588Ifc::CMaxCards);
    return EErr;
  }
  if(m_synObj[card]) delete m_synObj[card];
  m_synObj[card] = new Syn1588(m_log, card);
  if(!m_synObj[card]->present()) {
    if (m_log) m_log->Error("Error: Could not find card %d\n", card);
    delete m_synObj[card];
    m_synObj[card] = NULL;
    return EErr;
  }

  #if SUPP_SYN1588_SHM && defined( _SHARED_MEM_ )
    ret = syn1588_sm_init( card );

    if ( ret == ENoErr )
    {
      uint32_t id;
      m_synObj[card]->getBuildID(&id);
      if (m_log) m_log->Notice("Found syn1588(R) NIC, Build %d\n", id);
    }
  #endif  // SUPP_SYN1588_SHM && defined( _SHARED_MEM_ )

  return ret;
}


/*HDR*/
SYN1588_DEV_HANDLE syn1588_get_handle(SYN1588_DEV_IDX card) {
  if(!m_synObj[card]->present())
    return MBG_INVALID_DEV_HANDLE;

  return m_synObj[card]->getHandle();

}  // syn1588_get_handle


/*HDR*/
/// Configure the time stamper instance for PTPv2
///
/// Please refer to the application note an_syn1588_miits.pdf for detailed
/// information about the time stampers.
/// @param card: the number of the card requested
/// @param nwProto: the network protocol
/// @param nwMode: the traffic type timestamped
/// @param domain: domain of the traffic timestamped
/// @param vlanId: the vlan tag of the traffic timestamped
/// @param two_step: activate/deactivate two step for outgoing traffic
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_configurePTPv2Timestamper(SYN1588_DEV_IDX card,
                                                  uint16_t nwProto,
                                                  uint8_t nwMode,
                                                  uint8_t domain,
                                                  uint16_t vlanId,
                                                  uint8_t two_step) {
  if(EErr == cardExists(card)) return EErr;

  TSConfig cfg = TSConfig();

  cfg.nwProto         = nwProto;
  cfg.nwMode          = nwMode;
  cfg.domain          = domain;
  cfg.vlanId          = vlanId;
  cfg.twoStep         = two_step;
  cfg.initTimestamper = true;

  // clear fifos
  m_synObj[card]->clearFifo(m_synObj[card]->getTsTx());
  m_synObj[card]->clearFifo(m_synObj[card]->getTsRx());

  m_synObj[card]->initSystemTimestamper(cfg);

  return ENoErr;
}


/*HDR*/
/// Configure the timestamper for PTPv1 packets
///
/// Most parameters are fixed (IPv4, UDP, two step,
/// multicast only).
/// @param card: the number of the card requested
/// @return On success \a ENoErr is returned else \a EErr
SYN1588_RET_VAL syn1588_configurePTPv1Timestamper(SYN1588_DEV_IDX card) {

  if (EErr == cardExists(card)) return EErr;

  TSConfig cfg = TSConfig();

  cfg.timeProto = TSConfig::CTimeProtoPTPv1;
  cfg.nwProto   = TSConfig::L3_IPv4;
  cfg.vlanId    = 0;
  cfg.twoStep   = true;
  cfg.initTimestamper = true;

  // clear fifos
  m_synObj[card]->clearFifo(m_synObj[card]->getTsTx());
  m_synObj[card]->clearFifo(m_synObj[card]->getTsRx());

  m_synObj[card]->initSystemTimestamper(cfg);

  return ENoErr;
}


/*HDR*/
/// Get a timestamp for a PTPv2 packet.
///
/// The timestamp is retrieved for PortId \a portId
/// and sequence ID \a seqId.
/// @param card: the number of the card requested
/// @param recv: 1 for RX timestamp, 0 for TX timestamp
/// @param ts: pointer to timestamp buffer
/// @param subNs: pointer to sub nanosecond buffer
/// @oaram portId: PTP port ID of the PTPv2 packet
/// @param seqId: sequence ID of the PTPv2 packet
/// @return On success \a ENoErr is returned else \a EErr
SYN1588_RET_VAL syn1588_getTimestamp(SYN1588_DEV_IDX card,
                                     uint8_t recv,
                                     Timestamp_t *ts,
                                     TimeInterval_t *subNs,
                                     const PortId_t *portId,
                                     uint16_t seqId) {
  TimeInterval_t ti;
  SYN1588_RET_VAL ret = 0;

  bool rx = recv ? true : false;

  if(ts && portId)
    ret = m_synObj[card]->getTimestamp(rx, *ts, ti, portId, seqId);

  if((ret == ENoErr) && subNs)
    *subNs = ti;

  return ret;
}


/// Get a timestamp for an arbitrary packet.
///
/// The packet is specified by a pattern \a pat of
/// length \a len.
/// @param card: the number of the card requested
/// @param recv: 1 for RX timestamp, 0 for TX timestamp
/// @param ts: pointer to timestamp buffer
SYN1588_RET_VAL syn1588_pollTimstamp(SYN1588_DEV_IDX card,
                                     uint8_t recv,
                                     Timestamp_t *ts,
                                     const uint8_t *pat,
                                     uint8_t len) {
  TimeInterval_t ti;
  SYN1588_RET_VAL ret = 0;

  Syn1588::TsInstance_t instance = recv ?
    m_synObj[card]->getTsRx() : m_synObj[card]->getTsTx();

  if (ts && pat)
    ret = m_synObj[card]->pollTimestamp(instance, *ts, ti, pat, len);

  return ret;
}


/*HDR*/
SYN1588_RET_VAL syn1588_get_event_cap(SYN1588_DEV_IDX card, Timestamp_t *cap )
{
  cap->nsec =  m_synObj[card]->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTTIME0_L);
  cap->sec_l = m_synObj[card]->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTTIME0_H);
  cap->sec_h = 0;

  // empty event 0 fifo
  if(cap->nsec || cap->sec_l)
  {
    while(m_synObj[card]->getSyn1588Ifc()->readReg(Syn1588Ifc::EVENTTIME0_H));
  }

  return ENoErr;
}


/*HDR*/
SYN1588_RET_VAL syn1588_registerEvent(SYN1588_DEV_IDX card, const uint8_t port, const int8_t nr) {
  if(!cardExists(card)) return EErr;
  return m_synObj[card]->registerEvent(port, nr);
}


/*HDR*/
uint32_t syn1588_readReg(SYN1588_DEV_IDX card, const uint16_t addr) {

  if(!cardExists(card)) return EErr;  // FIXME TODO How to distinguish EErr from a valid number?

  return m_synObj[card]->getSyn1588Ifc()->readReg(addr);
}


/*HDR*/
SYN1588_RET_VAL syn1588_writeReg(SYN1588_DEV_IDX card, const uint16_t addr, const uint32_t data) {

  if(!cardExists(card)) return EErr;  // FIXME TODO Shouldn't we return an error?

  // The actual method doesn't provide a return code.
  m_synObj[card]->getSyn1588Ifc()->writeReg(addr, data);
  return ENoErr;
}


/*HDR*/
SYN1588_RET_VAL syn1588_setBit(SYN1588_DEV_IDX card, const uint16_t addr, const uint32_t mask) {
  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getSyn1588Ifc()->setBit(addr, mask);
}


/*HDR*/
SYN1588_RET_VAL syn1588_clearBit(SYN1588_DEV_IDX card, const uint16_t addr, const uint32_t mask)  {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getSyn1588Ifc()->clearBit(addr, mask);
}


/*HDR*/
/// Get the 8 byte PTP clock identifier.
///
/// @param card: the number of the card requested
/// @param id: a pointer to a memory location with at least
///            8 bytes of memory to hold the data
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_getId(SYN1588_DEV_IDX card, uint8_t *id) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getId(id);
}


/*HDR*/
/// Get the hardware capabilities.
uint32_t syn1588_getCapabilities(SYN1588_DEV_IDX card) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getSyn1588Ifc()->readReg(Syn1588Ifc::CAPABILITIES1);
}


/*HDR*/
/// Disable delivery of transmit timestamps.
SYN1588_RET_VAL syn1588_disableTxTsDelivery(SYN1588_DEV_IDX card) {

  if(!cardExists(card)) return EErr;

  //Syn1588::disableTxTsDelivery() doesn't provide a return code.
  m_synObj[card]->disableTxTsDelivery();
  return ENoErr;
}


/*HDR*/
/// Enable delivery of transmit timestamps.
SYN1588_RET_VAL syn1588_enableTxTsDelivery(SYN1588_DEV_IDX card) {

  if(!cardExists(card)) return EErr;

  //Syn1588::enableTxTsDelivery() doesn't provide a return code.
  m_synObj[card]->enableTxTsDelivery();
  return ENoErr;
}


/*HDR*/
/// Get the current time from specified device.
///
/// Please consider the latencies caused by the operating system
/// as well as by the hardware access. It may take up to a few milliseconds
/// until the function returns with the time information. So this function
/// should not be used for precise time information.
/// @param card: the number of the card requested
/// @param now: a pointer to a Timestamp structure to store the time information.
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL _DEPRECATED_BY( "syn1588_getSafeTimeAndStatus" )
syn1588_getTime(SYN1588_DEV_IDX card, struct Timestamp_t *now) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getTime(*now);
}


/*HDR*/
/// Set the clock to time \a t.
///
/// Please consider the latencies caused by the operating system
/// as well as by the hardware access. It may take up to a few milliseconds
/// until the function sets the time at hardware level. So this function
/// should not be used for applying precise timing information to the hardware.
/// @param card: the number of the card requested
/// @param t   : the time for the hardware clock
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_setTime(SYN1588_DEV_IDX card, struct Timestamp_t t) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->setTime(t);
}


/*HDR*/
/// Get leap info ::TODO
SYN1588_RET_VAL syn1588_getLeapInfo(SYN1588_DEV_IDX card, LEAP_INFO *p) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getLeapInfo( p->leap_sec, p->leap_applytime );
}


/*HDR*/
/// Get PTP sync status
SYN1588_RET_VAL syn1588_getPtpSyncStatus(SYN1588_DEV_IDX card, SYN1588_PTP_SYNC_STATUS *p) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getPtpSyncStatus( *p );
}


/*HDR*/
/// Safely get the current time from a card
SYN1588_RET_VAL syn1588_getSafeTimestamp(SYN1588_DEV_IDX card, struct Timestamp_t *p_ts) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getSafeTimestamp( *p_ts );
}


/*HDR*/
/// Safely get the current time and sync status from a card
SYN1588_RET_VAL syn1588_getSafeTimeAndStatus(SYN1588_DEV_IDX card, struct TimestampStatus_t *p_tss) {

  if(!cardExists(card)) return EErr;

  return m_synObj[card]->getSafeTimeAndStatus( *p_tss );
}


/*HDR*/
/// Get a string with HW version information
///
/// Unfortunately, neither a buffer size can be specified,
/// nor any precaution is taken to avoid a buffer overflow.
///
/// Looking at Sim1588::getVersionInfo(), at least 25 characters
/// are written to the buffer s.
SYN1588_RET_VAL syn1588_getVersionInfo(SYN1588_DEV_IDX card, char *s)
{
  SYN1588_RET_VAL ret;

  if(!cardExists(card)) return EErr;

  ret = m_synObj[card]->getVersionInfo( s );

  #if DEBUG_SYN1588_C_API > 1
    fprintf( stderr, "%s: \"%s\" (%u)\n", __func__, s, ret );
  #endif

  return ret;
}


/*HDR*/
/// Get syn1588 clock core version.
///
/// Unfortunately, neither a buffer size can be specified,
/// nor any precaution is taken to avoid a buffer overflow.
///
/// Looking at Sim1588::getVersion(), at least 8 characters
/// are written to the buffer s.
SYN1588_RET_VAL syn1588_getVersion(SYN1588_DEV_IDX card, char *s) {
  SYN1588_RET_VAL ret;

  if(!cardExists(card)) return EErr;

  /// Sim1588::getVersion() expects an int8_t[8] array
  /// even though the buffer is in fact used for a string.
  ret = m_synObj[card]->getVersion( (int8_t *) s );

  #if DEBUG_SYN1588_C_API > 1
    fprintf( stderr, "%s: \"%s\" (%u)\n", __func__, s, ret );
  #endif

  return ret;
}


/*HDR*/
/// Get the Version ID of the Ethernet MAC.
///
/// Unfortunately, neither a buffer size can be specified,
/// nor any precaution is taken to avoid a buffer overflow.
///
/// Looking at Sim1588::getMacVersion(), at least 8 characters
/// are written to the buffer s.
SYN1588_RET_VAL syn1588_getMacVersion(SYN1588_DEV_IDX card, char *s) {
  SYN1588_RET_VAL ret;

  if(!cardExists(card)) return EErr;

  ret = m_synObj[card]->getMacVersion( s );

  #if DEBUG_SYN1588_C_API > 1
    fprintf( stderr, "%s: \"%s\" (%u)\n", __func__, s, ret );
  #endif

  return ret;
}


/*HDR*/
/// ::TODO
SYN1588_RET_VAL syn1588_getEEPROMMacAddress(SYN1588_DEV_IDX card, char *s) {
  SYN1588_RET_VAL ret;

  if(!cardExists(card)) return EErr;

  /// Get the Mac address from the EEPROM
  ret = m_synObj[card]->getEEPROMMacAddress( s );

  #if DEBUG_SYN1588_C_API > 1
    fprintf( stderr, "%s: \"%s\" (%u)\n", __func__, s, ret );
  #endif

  return ret;
}


/*HDR*/
/// ::TODO
SYN1588_RET_VAL syn1588_getEEPROMBoardRevision(SYN1588_DEV_IDX card, char *s) {
  SYN1588_RET_VAL ret;

  if(!cardExists(card)) return EErr;

  /// Get the Board Revision from EEPROM
  ret = m_synObj[card]->getEEPROMBoardRevision( s );

  #if DEBUG_SYN1588_C_API > 1
    fprintf( stderr, "%s: \"%s\" (%u)\n", __func__, s, ret );
  #endif

  return ret;
}


/*HDR*/
// Get the serial number of a card.
SYN1588_RET_VAL syn1588_getSerialNumber(SYN1588_DEV_IDX card, char *s) {
  SYN1588_RET_VAL ret;

  if(!cardExists(card)) return EErr;

  ret = m_synObj[card]->getEEPROMSerialNumber(s);

  #if DEBUG_SYN1588_C_API > 1
    fprintf( stderr, "%s: \"%s\" (%u)\n", __func__, s, ret );
  #endif

  return ret;
}


/*HDR*/
/// Get the Build ID of the HW design.
SYN1588_RET_VAL syn1588_getBuildID(SYN1588_DEV_IDX card, uint32_t *id) {
  SYN1588_RET_VAL ret;

  if(!cardExists(card)) return EErr;

  /// Get the Build ID of the HW design.
  ret = m_synObj[card]->getBuildID( id );

  #if DEBUG_SYN1588_C_API > 1
    fprintf( stderr, "%s: %u (%u)\n", __func__, *id, ret );
  #endif

  return ret;
}


/*HDR*/
/// Set the initial rate according to the clock frequency.
///
/// This function performs the correct calculation for the step size.
/// It uses an accurate way to calculate the inverse of
/// the clock frequency.
/// It requires more CPU resources than the standard adjustRate
/// function but results in less inaccuracy.
/// @param card: the number of the card requested
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_initRate(SYN1588_DEV_IDX card) {

  if(!cardExists(card)) return EErr;

  TimeInterval_t drift;
  drift.scaledNs = 0;

  // Syn1588::initRate doesn't provide a return code.
  m_synObj[card]->initRate(drift, (uint8_t)0);
  return ENoErr;
}


/*HDR*/
/// Adjust the clock rate by \a drift.
///
/// This function uses less CPU resources than the initRate function but is less accurate
/// @param card: the number of the card requested
/// @param drift: a drift to adjust the clock rate
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_adjustRate(SYN1588_DEV_IDX card, const struct TimeInterval_t *drift) {

  if(!cardExists(card)) return EErr;

  // Syn1588::adjustRate doesn't provide a return code.
  m_synObj[card]->adjustRate(*drift, 0, 0);
  return ENoErr;
}


/*HDR*/
/// Close a previously initialized card.
///
/// Deactivate the time stamper, clear the FIFO and
/// disable further configuration of this card.
/// @param card: the number of the card requested
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_close(SYN1588_DEV_IDX card) {

  if(!cardExists(card)) return EErr;

  delete m_synObj[card];
  m_synObj[card] = NULL;
  return ENoErr;
}

