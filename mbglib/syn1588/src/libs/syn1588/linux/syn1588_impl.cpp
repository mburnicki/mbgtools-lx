 /******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#include "syn1588_ifc.h"
#include "syn1588_impl.h"

#include <sys/timex.h>
#include <sys/types.h>
#include <sys/select.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

// #define SYN1588_DEBUG

#ifndef _GNU_SOURCE
#  define _GNU_SOURCE    // needed for pread/pwrite
#endif

static char CLK_DEVICE[] = "/dev/syncD0";

static fd_set fdset;    // device descriptor set

/// Constructor
///
Syn1588Impl::Syn1588Impl(PTP_LogIfc *log, const uint8_t card) : m_log(log) {

  m_isSyn1588 = false;
  m_fd.fd = 0;
  // replace clock device string with actual index
  CLK_DEVICE[sizeof(CLK_DEVICE) - 2] = (char)card + '0';
  int fd = ::open(CLK_DEVICE, O_RDWR);
  if (fd < 0) {
    return;
  }
  m_fd.fd = fd;
  if (m_log) m_log->Warning("Syn1588Impl: Device %s found\n", CLK_DEVICE);
  m_isSyn1588 = true;
  // set up event descriptor/handle
  FD_SET(m_fd.fd, &fdset);
  // Set initial unknown nic
#ifdef __SYN1588_IP_CORE__
  m_hwType = IP_Core;
#else
  m_hwType = Unknown_NIC;
#endif

}

/// Destructor
///
Syn1588Impl::~Syn1588Impl() {

  m_log->Debug("the syn1588 impl is being destroyed!\n");
  if (m_fd.fd > 0) {
    FD_CLR(m_fd.fd, &fdset);
    close(m_fd.fd);
  }
}

/// Get HW type: ETH_VERSION: LSB 0: PCI - LSB 1: PCIe
///
uint8_t Syn1588Impl::getType() {
  // in the case we know already the hardware we return the value immediate
  if (m_hwType != Unknown_NIC) {
    return m_hwType;
  }

  uint32_t build, version;
  build = readReg(Syn1588Ifc::VR_ETH_BUILD);
  if (build >= 520) {
    version = readReg(Syn1588Ifc::VR_ETH_VERSION);
  } else {
    return Unknown_NIC;
  }

  uint32_t id = readReg(Syn1588Ifc::CLOCKID_H);

  if ((build <= 523) && ((version & 1) == 0) && (id == CLOCKID_H_PCI)) {
    m_hwType = PCI_NIC;
  } else if ((build <= 757) && ((version & 1) == 1) &&
             ((id == CLOCKID_H_PCIE15_0) || (id == CLOCKID_H_PCIE15_1))) {
    m_hwType = PCIe_NIC;
  } else if ((build >= 747) && (id != CLOCKID_H_PCIE15_0) &&
             (id != CLOCKID_H_PCIE21)) {
    m_hwType = PCIe_NIC2_0;
  } else if ((build >= 795) && (id == CLOCKID_H_PCIE21)) {
    m_hwType = PCIe_NIC2_1;
  }

  return m_hwType;
}

/// Write \a data to register of address \a addr.
///
Return_e Syn1588Impl::writeReg(const uint32_t addr, const uint32_t data) {

#ifdef SYN1588_DEBUG
  m_log->Debug("W %04X %08X\n", addr, data);
#endif
#ifdef USE_IOCTL
  struct csc_dev_ioctl ioctl_data;

  ioctl_data.address = addr;
  ioctl_data.data = data;
  if (ioctl(m_fd.fd, CLOCK_IOC_WR, &ioctl_data)) {
    m_log->Error("Syn1588Impl: writeReg error: 0x%04x - 0x%08x\n", addr, data);
    return EErr;
  }

#else
  if (0 > pwrite(m_fd.fd, &data, 4, addr)) {
    m_log->Error("Syn1588Impl: writeReg error: 0x%04x - 0x%08x\n", addr, data);
    return EErr;
  }
#endif
  return ENoErr;
}

/// Read register of address \a addr and return value.
///
uint32_t Syn1588Impl::readReg(const uint32_t addr, Return_e * const err) {

  uint32_t data;
#ifdef USE_IOCTL
  struct csc_dev_ioctl ioctl_data;

  ioctl_data.address = addr;
  if (ioctl(m_fd.fd, CLOCK_IOC_RD, &ioctl_data)) {
    m_log->Error("Syn1588Impl: readReg error: 0x%04x\n", addr);
    if (err) *err = EErr;
  }
  data =  ioctl_data.data;
#else
  if (0 > pread(m_fd.fd, &data, sizeof(data), addr)) {
    m_log->Error("Syn1588Impl: readReg error: 0x%04x\n", addr);
    if (err) *err = EErr;
  }
#endif
#ifdef SYN1588_DEBUG
  m_log->Debug("R %#04X %08X\n", addr, data);
#endif

  if (err) *err = ENoErr;

  return data;
}

/// Set \a mask bits of register denoted by /a addr.
///
Return_e Syn1588Impl::setBit(const uint32_t addr, const uint32_t mask) {
  Return_e err;
  uint32_t d = readReg(addr, &err);
  if (err == EErr) return err;
  return writeReg(addr, d | mask);
}

/// Clear \a mask bits of register denoted by /a addr.
///
Return_e Syn1588Impl::clearBit(const uint32_t addr, const uint32_t mask) {
  Return_e err;
  uint32_t d = readReg(addr, &err);
  if (err == EErr) return err;
  return writeReg(addr, d & ~mask);
}

/// Register Event for select processing of clock interrupts. Only for hardware clock.
Return_e Syn1588Impl::registerEvent(uint32_t event) {
  m_log->Debug("register event %X\n", event);

  return setBit(Syn1588Ifc::IREN, event);
}

/// Disable interrupt for an event.
Return_e Syn1588Impl::disableEvent(uint32_t event) {
  if (m_log) m_log->Debug("disable event %X\n", event);

  return clearBit(Syn1588Ifc::IREN, event);
}

/// Wait for a clock event. The event has to be registered before using
/// this function. Return value is the interrupt source if Event occured
/// and 0 for timeout.
uint32_t Syn1588Impl::waitForEvent(uint32_t timeout, Return_e * const err) {

  struct timeval tout;
  tout.tv_usec = (timeout % 1000) * 1000;
  tout.tv_sec = timeout / 1000;
  fd_set act_fd = fdset;
  if (::select(m_fd.fd + 1, &act_fd, NULL, NULL, &tout) <= 0) {
    // return 0 if timeout or an error occured (e.g. sigint was sent)
    if (err) *err = EErr;
    return 0;
  }
  // fetch interrupt information
  return readReg(Syn1588Ifc::IRSRC, err);
}

bool Syn1588Impl::canProvidePacketAttachedTimestamp(void) const {
  return true;
}