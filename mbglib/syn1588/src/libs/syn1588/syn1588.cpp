 /********************************************************************
 *           _____           ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

/// @file Implementation of the syn1588 class

#include "syn1588.h"
#include "simple_utils.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>

static const size_t RXTX_FIFO_SIZE   = 1024;
static const size_t MAX_TS_READOUT   =   4;
static const size_t MAX_WRONG_TS     =  32;
static const size_t MIN_PREFIX_LEN   =   3;
static const size_t EVENTFIFO_SIZE   =  16;
static const size_t MAX_PAT_MASK_LEN = 128;
static const int64_t NSPERSECscaled  = int64_t(1000000000) << 16;



// M Core versions

// M-Core 2.3.0: new timestamp struct without 'sec_h' field:
static const uint32_t c_MCoreNewTSStruct = 230;
// newer versions assume that previous version features are avaiable as well


// Ethernet Mac versions

// Ethernet MAC 3.1.30 : starting with this version, MAC has the integrated timestamper:
static const uint32_t c_MacTs = 3130;
// Ethernet MAC 3.1.40 : starting with this version, MAC has programmable 1step timestamper:
static const uint32_t c_prog1Ts = 3140;


/// TsTimeCtrl
///
/// Contains the appropriate Time Control register for the different
/// timestamper instances. Use the syn1588 enum TsBase_t to select
/// the correct instance: TsTimeCtrl[...]
static const uint16_t TsTimeCtrl[] = {
  Syn1588Ifc::TIMECTRL,
  Syn1588Ifc::TIMECTRL,
  Syn1588Ifc::VR_ETH_TIMECTRL,
  Syn1588Ifc::VR_ETH_TIMECTRL //better to add a VR_ETH_TIMESTRL_TS2 reg?!
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588Ifc::TIMECTRL,
  Syn1588Ifc::TIMECTRL,
  Syn1588Ifc::TIMECTRL,
  Syn1588Ifc::TIMECTRL
#endif
};

/// Ts1StepCtrl
///
/// Contains the appropriate one step control register for different
/// timestamper instanes. Use the syn1588 enum TsBase_t to select
/// the correct instance: TsTimeCtrl[...]
static const uint16_t Ts1StepCtrl[] = {
  Syn1588Ifc::ONESTEPCTRL,
  Syn1588Ifc::ONESTEPCTRL,
  Syn1588Ifc::VR_ETH_ONESTEPCTRL,
  Syn1588Ifc::VR_ETH_TS2_ONESTEPCTRL
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588Ifc::MIITS2_ONESTEPCTRL,
  Syn1588Ifc::MIITS3_ONESTEPCTRL,
  Syn1588Ifc::MIITS4_ONESTEPCTRL,
  Syn1588Ifc::MIITS5_ONESTEPCTRL
#endif
};

static const bool Ts1StepCapable[] ={
  true,
  false,
  true,
  true
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  true,
  true,
  true,
  true
#endif
};

/// TsRx
///
/// Returns the correct Rx timestamper instance.
/// Use the syn1588 enum TsBase_t to select the instance: TsRx[...]
static const Syn1588::TsInstance_t TsRx[] = {
  Syn1588::TsSysRx,
  Syn1588::TsUsrRx,
  Syn1588::TsMacRx,
  Syn1588::TsMac2Rx
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588::TsMii2Rx,
  Syn1588::TsMii3Rx,
  Syn1588::TsMii4Rx,
  Syn1588::TsMii5Rx
#endif
};

/// TsRx
///
/// Returns the correct Tx timestamper instance.
/// Use the syn1588 enum TsBase_t to select the instance: TsTx[...]
static const Syn1588::TsInstance_t TsTx[] = {
  Syn1588::TsSysTx,
  Syn1588::TsUsrTx,
  Syn1588::TsMacTx,
  Syn1588::TsMac2Tx
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588::TsMii2Tx,
  Syn1588::TsMii3Tx,
  Syn1588::TsMii4Tx,
  Syn1588::TsMii5Tx
#endif
};

// TsStatus
///
/// Returns the register address of the FIFO status for the timestamper
/// instance  Use the syn1588 enum TsBase_t to select the
/// instance: TsStatus[...]
static const uint16_t TsStatus[] = {
  Syn1588Ifc::TSFIFO0_STATUS,
  Syn1588Ifc::TSFIFO1_STATUS,
  Syn1588Ifc::VR_ETH_TSFIFO_STATUS,
  Syn1588Ifc::VR_ETH_TSFIFO_STATUS
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588Ifc::MIITS2_FIFO_STATUS,
  Syn1588Ifc::MIITS3_FIFO_STATUS,
  Syn1588Ifc::MIITS4_FIFO_STATUS,
  Syn1588Ifc::MIITS5_FIFO_STATUS
#endif
};

/// Signals if the respective timestamper can return
/// timestamps with the packet (e.g., SO_TIMESTAMPING) or not
/// Use the syn1588 enum TsBase_t to select the timestamper
static const bool TsTimestampAttachedToPacket[] = {
  false,
  false,
  true,
  true
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  false,
  false,
  false,
  false
#endif
};

/// TsFifo Array
///
/// Contains all register addresses to access the Rx and Tx timestamper FIFOs. It
/// offers an easy way to use the correct register address, depending on the
/// desired timestamp instance. Use \enum TsInstance_t to select the instance.
static const uint16_t TsFifo[] = {
  Syn1588Ifc::TSFIFO0RX,       Syn1588Ifc::TSFIFO0TX,
  Syn1588Ifc::TSFIFO1RX,       Syn1588Ifc::TSFIFO1TX,
  Syn1588Ifc::VR_ETH_TSFIFORX, Syn1588Ifc::VR_ETH_TSFIFOTX,
  Syn1588Ifc::VR_ETH_TSFIFORX, Syn1588Ifc::VR_ETH_TSFIFOTX
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588Ifc::MIITS2_FIFO_RX, Syn1588Ifc::MIITS2_FIFO_TX,
  Syn1588Ifc::MIITS3_FIFO_RX, Syn1588Ifc::MIITS3_FIFO_TX,
  Syn1588Ifc::MIITS4_FIFO_RX, Syn1588Ifc::MIITS4_FIFO_TX,
  Syn1588Ifc::MIITS5_FIFO_RX, Syn1588Ifc::MIITS5_FIFO_TX
#endif
};

/// TsMemBase Array
///
/// Contains all register addresses to access the timestamper memory base. It
/// offers an easy way to use the correct register address, depending on the
/// desired timestamp instance. Use \enum TsInstance_t to select the instance.
static const uint16_t TsMemBase[] = {
  Syn1588Ifc::MIITS0_RXMEM,    Syn1588Ifc::MIITS0_TXMEM,
  Syn1588Ifc::MIITS1_RXMEM,    Syn1588Ifc::MIITS1_TXMEM,
  Syn1588Ifc::VR_ETH_TS_RXMEM, Syn1588Ifc::VR_ETH_TS_TXMEM,
  Syn1588Ifc::VR_ETH_TS2_RXMEM, Syn1588Ifc::VR_ETH_TS2_TXMEM
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588Ifc::MIITS2_RXMEM, Syn1588Ifc::MIITS2_TXMEM,
  Syn1588Ifc::MIITS3_RXMEM, Syn1588Ifc::MIITS3_TXMEM,
  Syn1588Ifc::MIITS4_RXMEM, Syn1588Ifc::MIITS4_TXMEM,
  Syn1588Ifc::MIITS5_RXMEM, Syn1588Ifc::MIITS5_TXMEM
#endif
};

/// TsCtrl Array
///
/// Contains all register addresses to access the timestamper control register.
/// It offers an easy way to use the correct register address, depending on the
/// desired timestamp instance. Use \enum TsInstance_t to select the instance.
static const uint16_t TsCtrl[] = {
  Syn1588Ifc::MIITS0_RXCRTL,   Syn1588Ifc::MIITS0_TXCRTL,
  Syn1588Ifc::MIITS1_RXCRTL,   Syn1588Ifc::MIITS1_TXCRTL,
  Syn1588Ifc::VR_ETH_TSRXCTRL, Syn1588Ifc::VR_ETH_TSTXCTRL,
  Syn1588Ifc::VR_ETH_TS2_RXCTRL, Syn1588Ifc::VR_ETH_TS2_TXCTRL
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  Syn1588Ifc::MIITS2_RXCTRL, Syn1588Ifc::MIITS2_TXCTRL,
  Syn1588Ifc::MIITS3_RXCTRL, Syn1588Ifc::MIITS3_TXCTRL,
  Syn1588Ifc::MIITS4_RXCTRL, Syn1588Ifc::MIITS4_TXCTRL,
  Syn1588Ifc::MIITS5_RXCTRL, Syn1588Ifc::MIITS5_TXCTRL
#endif
};

/// TsEvent Array
///
/// Contains the interrupt bits for the IREN (interrupt enable) register.
static const uint8_t TsEvent[] = {
  Syn1588Ifc::IREN_MIITS0_RX, Syn1588Ifc::IREN_MIITS0_TX,
  Syn1588Ifc::IREN_MIITS1_RX, Syn1588Ifc::IREN_MIITS1_TX,
  0, 0,
  0, 0
#ifdef _SYN1588_EXT_TS_REGS_
    , 0, 0, 0, 0, 0, 0, 0, 0
#endif
};

/// TsString
/// This array contains strings describing the different timestamper
static const char *TsString[] = {
  "TSFIFO0RX", "TSFIFO0TX",
  "TSFIFO1RX", "TSFIFO1TX",
  "TSFIFOMRX", "TSFIFOMTX",
  "TSFIFO_M2RX", "TSFIFO_M2TX"
#ifdef _SYN1588_EXT_TS_REGS_
    ,
  "TSFIFO2RX", "TSFIFO2TX",
  "TSFIFO3RX", "TSFIFO3TX",
  "TSFIFO4RX", "TSFIFO4TX",
  "TSFIFO5RX", "TSFIFO5TX"
#endif
};

/// TsCount
/// Get the FIFO count of the timestamper instance \a ti from the
/// the current value of the Fifo Status register \a val.
inline uint32_t TsCount(Syn1588::TsInstance_t ti, uint32_t val) {
  if (ti & 1) {
    return Syn1588Ifc::TXFCNT(val);
  } else {
    return Syn1588Ifc::RXFCNT(val);
  }
}

/// TsEmpty
/// Get the timstamp FIFO empty bit for the ts instance \a ti.
inline uint32_t TsEmpty(Syn1588::TsInstance_t ti) {
  if (ti & 1) {
    return Syn1588Ifc::TXFEMPTY;
  } else {
    return Syn1588Ifc::RXFEMPTY;
  }
}

/// TsStatusAddr
/// Get the register address of the FIFO status for the timestamper
inline uint32_t TsFull(Syn1588::TsInstance_t ti) {
  if (ti & 1) {
    return Syn1588Ifc::TXFFULL;
  } else {
    return Syn1588Ifc::RXFFULL;
  }
}
/// instance \a ti.
/// TsStatusAddr
/// Get the register address of the FIFO status for the timestamper
/// instance \a ti.
inline uint16_t TsStatusAddr(Syn1588::TsInstance_t ti) {
  return TsStatus[ti >> 1];
}

/// Construct a Syn1588 object:
/// Calculate the clock rate coefficients. Requires a pointer to the logging
/// object \a log. Selects the syn1588 hardware (e.g. PCIe NIC) via \a card,
/// which starts with 0 for the first card found by the operating system. Checks
/// the hardware version and sets the timestamper instance according to the hardware
/// revision. It is assumed that PTP traffic should be timestamped. If a user wants
/// to timestamp custom packets he/she should select a different timestamper
/// than the one used for the PTP timestamping
Syn1588::Syn1588(PTP_LogIfc *log, const uint8_t card) : m_syn1588Ifc(log, card), m_log(log) {

  if (!m_syn1588Ifc.present()) return;
  char v[8];

  getVersion((int8_t *) v);
  m_freq = m_syn1588Ifc.readReg(Syn1588Ifc::CLKFREQ);
  // see .h file for explanation of scaling factors
  m_clkscale = 16;
  m_clkmul = ((uint64_t(1) << 24 << m_clkscale) / m_freq) & 0xFFFF;

  m_log->Notice("syn1588(R) Hardware Clock %s f=%ld Hz\n", &v[0], m_freq);

  m_newTSStruct = false;
  m_ultimateTS  = false;
  m_prog1TS     = false;
  m_initTS      = false;
  m_subOffset   = false;

  // Check capabilites
  uint32_t cap1 = m_syn1588Ifc.readReg(Syn1588Ifc::CAPABILITIES1);

  // Ultimate timestamper
  if (cap1 & Syn1588Ifc::CAP1_ULTIMATE) {
#ifndef WIN32
    m_ultimateTS  = true;
#endif
    m_log->Info("Found Ultimate TS support\n");
  }

  // Stop clock
  if (cap1 & Syn1588Ifc::CAP1_STOPCLOCK) {
    m_subOffset = true;
    m_log->Info("Found stop clock support\n");
  }

  // Check also M Core version for capabilities
  uint32_t version = (v[2]-'0') * 100 + (v[4]-'0') * 10 +  (v[6]-'0');

  if (v[0] == 'M') {
    if (version >= c_MCoreNewTSStruct) m_newTSStruct = true;
  }

#ifdef __SYN1588_IP_CORE__
  //the following configures the default Ttimestamper that is used,
  // this can be changed during operation via the function Syn1588::setTsBase
  //modify this according to the system setup
  m_PTPTimestamper = TsMii2;
  m_log->Info("Setting TS instances %s and %s\n", TsString[m_PTPTimestamper*2], TsString[m_PTPTimestamper*2 + 1]);
#else
  // Check the MAC version
  getMacVersion(v);
  uint32_t ethVersion = (v[0] - '0') * 1000 + (v[2] - '0') * 100 +
    (v[4] - '0') * 10 + (v[5] - '0');

  // select which timestamper to use..
  m_PTPTimestamper = TsMac;
  // read from MAC Capability register 0 and check if RX and TX MAC Timestamper are enabled
  uint32_t eth_caps;
  if ((int32_t)m_syn1588Ifc.readReg(Syn1588Ifc::VR_ETH_BUILD) >= MAC_CAP_BUILD_VER) {
    eth_caps = m_syn1588Ifc.readReg(Syn1588Ifc::VR_ETH_CAPABILITIES_0);
  } else {
    eth_caps = 0;
  }

  if (ethVersion >= c_MacTs && (eth_caps & Syn1588Ifc::MAC_CAP0_RX_TIMESTAMPER) &&
      (eth_caps & Syn1588Ifc::MAC_CAP0_TX_TIMESTAMPER)) {
    m_PTPTimestamper = TsMac;
    m_log->Info("Using MAC TS Version %d\n", ethVersion);
  }
  if (ethVersion >= c_prog1Ts) {
    m_prog1TS = true;
    m_log->Info("Using programmable 1-step TS\n");
  }
#endif// __SYN1588_IP_CORE__

#if defined __NIOS__ && !defined _NO_ANALOG_RATE_ADJUST_
  m_tc = new I2C_tcxo(m_log, this);
  m_tc->setPullRange(I2C_tcxo::E6_25ppm);
  m_tc->setFreq(0x0);
  m_log->Always("Using analog clock adjustment\n");
  m_drift.scaledNs = 0;
#endif
}

/// Deactivate the timestamper, clear the FIFO, but only if
/// it has been initialized as well (initTimestamper bit in the
/// TSConfig class of the init function).
Syn1588::~Syn1588() {

  m_log->Debug("Closing syn1588 object\n");

  if (!m_syn1588Ifc.present()) return;

  // Disable timestamper only if it has been initilized as well.
  // -> disable it only for the PTP Stack
  if (m_initTS) {
    // Disable one step timestamper
    //m_syn1588Ifc.clearBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TICB_EN1STEP);

    // Always deliever TX TS
    //m_syn1588Ifc.clearBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_NOTXTS);

    // Disbale timestamper
    m_syn1588Ifc.writeReg(TsCtrl[TsTx[m_PTPTimestamper]], 0);
    m_syn1588Ifc.writeReg(TsCtrl[TsRx[m_PTPTimestamper]], 0);

    // clear fifos
    clearFifo(TsTx[m_PTPTimestamper]);
    clearFifo(TsRx[m_PTPTimestamper]);

  }

#if defined __NIOS__ && !defined _NO_ANALOG_RATE_ADJUST_
  delete m_tc;
#endif

}

/// Initialize the syn1588 hardware. If the initTimestamper bit
/// in TSConfig \a cfg is set to true, the timestamper will be initialized.
/// This should be only done if the application wants to timestamp packets.
/// If the timestamper is initialized, the timestamp FIFO is cleared, the
/// rate is initialized, and the timestamper is configured to timestamp
/// PTP packets according to the parameters of \a cfg.
int8_t Syn1588::init(TSConfig cfg) {

  if (cfg.initTimestamper) {
    // clear fifos
    clearFifo(TsTx[m_PTPTimestamper]);
    clearFifo(TsRx[m_PTPTimestamper]);

    m_initTS = true;

    int8_t err = initSystemTimestamper(cfg);
    if (err == EErr) {
      return err;
    }
    // Always deliever TX TS
    m_syn1588Ifc.clearBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_NOTXTS);

    if (cfg.initRate) adjustRate(TimeInterval_t(), INIT_ADJUST_MODE);
  }
  return ENoErr;
}

/// Get the clock drift and store it in \a drift.
/// It is calculated by using the current value
/// of the step register and the actual clock frequency.
int8_t Syn1588::getDrift(TimeInterval_t &drift, uint8_t mode) {
  (void)mode; // explicitely dont use mode parameter

#if defined __NIOS__ && !defined _NO_ANALOG_RATE_ADJUST_
  // just return the last used drift value from adjustRate()
  drift = m_drift;
#else
  uint64_t step;

  step = m_syn1588Ifc.readReg(Syn1588Ifc::SHDWSTEPPU_L);
  step = step | (uint64_t)m_syn1588Ifc.readReg(Syn1588Ifc::SHDWSTEPPU_H) << 32;

  int64_t freq = m_freq;

  // We are handling large numbers which are close to the resolution limits.
  // Therefore, we reduce precision before calculating the acutal result
  step = step >> 8;

  drift.scaledNs = NSPERSECscaled - ((freq * (int64_t)step) >> m_clkscale);
  m_log->Debug("Syn1588: drift is %d\n", PRINT_SCALED_NS(drift.scaledNs));

#endif
  return ENoErr;
}

/// Disable delivery of transmit timestamps. This could be used to
/// enhance performance in PTP one step mode.
void Syn1588::disableTxTsDelivery() {
  // Disable delivery of TX TS in one step mode.
  // This is used in one step master mode,
  // because a one step master does not need transmit timestamps.
  m_syn1588Ifc.setBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_NOTXTS);
}

/// Enable delivery of transmit timestamps. This mode should be used
/// for normal opertaion.
void Syn1588::enableTxTsDelivery() {
  // Enable delivery of TX TS
  m_syn1588Ifc.clearBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_NOTXTS);
}


/// Get the 8 byte PTP clock identifier and store it in \a id. At least
/// 8 bytes of memory are required to hold the data.
/// On success ENoErr is returned.
int8_t Syn1588::getId(uint8_t *id) {

  // get the ID directly from the clock driver
  uint32_t h;
  h = m_syn1588Ifc.readReg(Syn1588Ifc::CLOCKID_H);
  id[0] = (int8_t) (h >> 24);
  id[1] = (int8_t) (h >> 16);
  id[2] = (int8_t) (h >>  8);
  id[3] = (int8_t) h;
  h = m_syn1588Ifc.readReg(Syn1588Ifc::CLOCKID_L);
  id[4] = (int8_t) (h >> 24);
  id[5] = (int8_t) (h >> 16);
  id[6] = (int8_t) (h >>  8);
  id[7] = (int8_t) h;
  return ENoErr;
}

/// Get the version string into \a version. The string is formatted as
/// "T x.x.x", where T denotes the core type (M, S, L) and x decimal
/// digits. At least 8 bytes of memory are required to hold the string.
/// On success ENoErr is returned.
int8_t Syn1588::getVersion(int8_t *version) {

  uint32_t d;
  d = m_syn1588Ifc.readReg(Syn1588Ifc::SYN1588_VER);
  version[0] = (d >> 24) & 0xFF;
  version[1] = ' ';
  version[2] = (d >> 16) & 0xFF;
  version[3] = '.';
  version[4] = (d >> 8) & 0xFF;
  version[5] = '.';
  version[6] = d & 0xFF;
  version[7] = '\0';
  return ENoErr;
}

/// Get the Version ID of the Ethernet MAC.
/// The register contains 4 ASCII characters:
/// 31-24: major version
/// 23-16: minor version
/// 15-0 : sub-minor version
/// At least 7 bytes of memory are required to hold the string.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::getMacVersion(char *version) {
#ifdef __SYN1588_IP_CORE__
  version[0] = 0x00;
  version[1] = 0x00;
  version[2] = 0x00;
  version[3] = 0x00;
  version[4] = 0x00;
  version[5] = 0x00;
  version[6] = 0x00;
  return ENoErr;
#else
  uint32_t d;
  d = m_syn1588Ifc.readReg(Syn1588Ifc::VR_ETH_VERSION);
  if(d == 0) {
    version[0] = 0x00;
    version[1] = 0x00;
    version[2] = 0x00;
    version[3] = 0x00;
    version[4] = 0x00;
    version[5] = 0x00;
    version[6] = 0x00;
    return EErr;
  } else {
    version[0] = (char)((d >> 24) & 0xFF); version[1] = '.';
    version[2] = (char)((d >> 16) & 0xFF); version[3] = '.';
    version[4] = (char)((d >>  8) & 0xBF); // mask OCXO adapter bit
    version[5] = (char)(d & 0xFE); version[6] = '\0'; // mask PCIe/PCI bit
    return ENoErr;
  }
#endif// __SYN1588_IP_CORE__
}

/// Get the Ethernet MAC address from the EEPROM.
/// The 3 registers contains 12 ASCII characters:
/// mac0 upper  four bytes (8|9|10|11)
/// mac1 middle four bytes (4|5|6|7)
/// mac2 lower  four bytes (0|1|2|3)
/// At least 18 bytes of memory are required to hold the string.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::getEEPROMMacAddress(char *macAddress) {

  uint32_t mac[CEepromMACLength];

  mac[0] = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_0_MAC_0);
  mac[1] = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_1_MAC_1);
  mac[2] = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_2_MAC_2);

  if ((mac[0] == 0) || (mac[1] == 0) || (mac[2] == 0)) return EErr;

  macAddress[0]  = (char)mac[0];
  macAddress[1]  = (char)(mac[0] >> 8);
  macAddress[2]  = ':';
  macAddress[3]  = (char)(mac[0] >> 16);
  macAddress[4]  = (char)(mac[0] >> 24);
  macAddress[5]  = ':';
  macAddress[6]  = (char)mac[1];
  macAddress[7]  = (char)(mac[1] >> 8);
  macAddress[8]  = ':';
  macAddress[9]  = (char)(mac[1] >> 16);
  macAddress[10] = (char)(mac[1] >> 24);
  macAddress[11] = ':';
  macAddress[12] = (char)mac[2];
  macAddress[13] = (char)(mac[2] >> 8);
  macAddress[14] = ':';
  macAddress[15] = (char)(mac[2] >> 16);
  macAddress[16] = (char)(mac[2] >> 24);
  macAddress[17] = '\0';
  return ENoErr;
}

/// Get the Board Revision Number from the EEPROM
/// Board revision(major | minor | subminor | subsubminor)
/// At least 8 bytes of memory are required to hold the string.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::getEEPROMBoardRevision(char* revisionNum) {

  uint32_t rev = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_3_BOARD_REV);

  if (rev == 0) return EErr;

  revisionNum[0] = (char)rev;
  revisionNum[1] = '.';
  revisionNum[2] = (char)(rev >> 8);
  revisionNum[3] = '.';
  revisionNum[4] = (char)(rev >> 16);
  revisionNum[5] = '.';
  revisionNum[6] = (char)(rev >> 24);
  revisionNum[7] = '\0';
  return ENoErr;
}

/// Get the Serial Number from the EEPROM
/// serial number (3|2|1|0)
/// serial number (7|6|5|4)
/// serial number (11|10|9|8)
/// serial number (11|10|9|8)
/// serial number (15|14|13|12)
/// At least 17 bytes of memory are required to hold the string.
int8_t Syn1588::getEEPROMSerialNumber(char* serialNum) {

  uint32_t s[CEepromSerialLength];

  s[0] = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_4_SER_NUM_0);
  s[1] = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_5_SER_NUM_1);
  s[2] = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_6_SER_NUM_2);
  s[3] = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_7_SER_NUM_3);

  for (uint8_t i = 0; i < CEepromSerialLength; i++) {

    if (s[i] == 0) return EErr; // error handling

    for (uint8_t j = 0; j < sizeof(uint32_t); j++) {

      uint8_t k      = 0;
      uint8_t offset = 0;

      k = j + (sizeof(uint32_t) * i); // absolute position in string
      offset = (8 * j);               // offset for shift, 8 = Bits per Byte
      serialNum[k] = (char)(s[i] >> offset);
    }
  }
  serialNum[(CEepromSerialLength * sizeof(uint32_t))] = '\0';

  return ENoErr;
}

/// Get the Build ID of the HW design.
/// On success ENoErr is returned.
int8_t Syn1588::getEEPROMBoardCapability(uint32_t &boardCap,
                                         uint32_t &rawBoadCapReg3210,
                                         uint32_t &rawBoadCapReg7654) {

  uint32_t tmpCap_part_0 = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_8_BOARD_CAP_0);
  uint32_t tmpCap_part_1 = m_syn1588Ifc.readReg(Syn1588Ifc::I2C_EEPROM_9_BOARD_CAP_1);

  rawBoadCapReg3210 = tmpCap_part_0;
  rawBoadCapReg7654 = tmpCap_part_1;

  char tmpBoardCapabilitiesASCII[8] = {};

  tmpBoardCapabilitiesASCII[0] = tmpCap_part_0 & 0xFF;
  tmpBoardCapabilitiesASCII[1] = (tmpCap_part_0 >> 8) & 0xFF;
  tmpBoardCapabilitiesASCII[2] = (tmpCap_part_0 >> 16) & 0xFF;
  tmpBoardCapabilitiesASCII[3] = (tmpCap_part_0 >> 24) & 0xFF;
  tmpBoardCapabilitiesASCII[4] = tmpCap_part_1 & 0xFF;
  tmpBoardCapabilitiesASCII[5] = (tmpCap_part_1 >> 8) & 0xFF;
  tmpBoardCapabilitiesASCII[6] = (tmpCap_part_1 >> 16) & 0xFF;
  tmpBoardCapabilitiesASCII[7] = (tmpCap_part_1 >> 24) & 0xFF;

  uint8_t tmpBoardCapabilitiesINT[4] = {};

  if (parse_hexId(tmpBoardCapabilitiesASCII,
                  tmpBoardCapabilitiesINT,
                  8,
                  4,
                  true,
                  0x00) != NOERR) {
    boardCap = 0;
    return EErr;
  }

  boardCap = 0;
  boardCap |= tmpBoardCapabilitiesINT[3];
  boardCap |= tmpBoardCapabilitiesINT[2] << 8;
  boardCap |= tmpBoardCapabilitiesINT[1] << 16;
  boardCap |= tmpBoardCapabilitiesINT[0] << 24;

  return ENoErr;
}

/// Get the Build ID of the HW design.
/// On success ENoErr is returned.
int8_t Syn1588::getBuildID(uint32_t* d) {
#ifdef __SYN1588_IP_CORE__
  *d = 0;
  return ENoErr;
#else
  *d = m_syn1588Ifc.readReg(Syn1588Ifc::VR_ETH_BUILD);
  return ENoErr;
#endif
}

/// Get a string with HW version information.
/// At least 64 bytes of memory are required to hold the string.
/// On success ENoErr is returned.
int8_t Syn1588::getVersionInfo(char *string) {
#ifdef __SYN1588_IP_CORE__
  int8_t   version[8];

  getVersion(&version[0]);
  sprintf(string, "syn1588(R) Clock %s; ", version);
  return ENoErr;
#else
  uint32_t id;
  int8_t   version[8];

  getBuildID(&id);
  getVersion(&version[0]);
  sprintf(string, "HW build %d; syn1588(R) Clock %s; ", (uint16_t)id, version);
  return ENoErr;
#endif// __SYN1588_IP_CORE__
}

/// Safely get the current time.
///
/// On Linux this can be done by reading the TIME_NS and TIME_SEC
/// registers which are duplicated by the kernel driver for each process,
/// so no locking is needed.
///
/// On Windows, usage of the TIME_NS and TIME_SEC registers is reserved
/// for the PTP stack only. Applications that read the time from the card
/// must use the TIME2_NS and TIME2_SEC registers, but reading those registers
/// is not thread-safe in the kernel driver.
///
/// So if this funtion is called via an API call implemented in a library,
/// the library should read the TIME2_NS and TIME2_SEC registers from
/// within a critical section.
///
/// Care must be taken if the lsync program is also running because that
/// program does no locking yet, so it may interfere with other programs.
///
/// The ::PSS_BIT_LSYNC_RUNNING bit of the ::PTP_SYNC_STATUS register
/// can be checked to determine if lsync is running, and the time stamp
/// returned when this function is called from another application
/// may be inconsistent.
///
/// On success ENoErr is returned.
int8_t Syn1588::getSafeTimestamp(Timestamp_t &ts) {

  // trigger time readout COPYME
#ifdef _WIN32
  ts.nsec  = m_syn1588Ifc.readRegRaw(Syn1588Ifc::TIME2_NS);
  ts.sec_l = m_syn1588Ifc.readRegRaw(Syn1588Ifc::TIME2_SEC);
#else  // Linux
  ts.nsec  = m_syn1588Ifc.readRegRaw(Syn1588Ifc::TIME_NS);
  ts.sec_l = m_syn1588Ifc.readRegRaw(Syn1588Ifc::TIME_SEC);
#endif
  ts.sec_h = 0;  // FIXME by martin: Should be register TIME_E, which is marked deprecated.
                 // Otherwise sec_h could be undefined.

  return ENoErr;
}

/// Safely get the current time and sync status.
/// See the notes for ::getSafeTimestamp.
int8_t Syn1588::getSafeTimeAndStatus(TimestampStatus_t &tss) {

  getSafeTimestamp(tss.ts);
  tss.sync_status = m_syn1588Ifc.readRegRaw(Syn1588Ifc::PTP_SYNC_STATUS);

  return ENoErr;
}

/// Get the current time into the timestamp object \a now.
/// Please consider the latencies caused by the operating system
/// as well as by the hardware access. It may take up to a few milliseconds
/// until the function returns with the time information. So this function
/// should not be used for precise time information.
/// On success ENoErr is returned.
int8_t Syn1588::getTime(Timestamp_t &now) {

  // trigger time readout
  now.nsec  = m_syn1588Ifc.readReg(Syn1588Ifc::TIME_NS);
  now.sec_l = m_syn1588Ifc.readReg(Syn1588Ifc::TIME_SEC);
  now.sec_h = 0;  // FIXME by martin: Should be register TIME_E, which is marked deprecated.
                  // Otherwise now.sec_h could be undefined.
  return ENoErr;
}

/// Set the clock to time \a t.
/// Please consider the latencies caused by the operating system
/// as well as by the hardware access. It may take up to a few milliseconds
/// until the function sets the time at hardware level. So this function
/// should not be used for applying precise timing information to the hardware.
/// On success ENoErr is returned.
int8_t Syn1588::setTime(const Timestamp_t &t) {

  // save event control
  uint32_t reg = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTCTRL);

  // assign shadow registers
  m_syn1588Ifc.writeReg(Syn1588Ifc::SHDWTIME_NS,  t.nsec);
  m_syn1588Ifc.writeReg(Syn1588Ifc::SHDWTIME_SEC, t.sec_l);
  // enable loading of time reg
  m_syn1588Ifc.clearBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TICB_LTIME);
  m_syn1588Ifc.setBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TICB_LTIME);

  // restore event control
  m_syn1588Ifc.writeReg(Syn1588Ifc::EVENTCTRL, 0x00);
  m_syn1588Ifc.writeReg(Syn1588Ifc::EVENTCTRL, reg);

  return ENoErr;
}

/// Get current leap second info.
/// On success ENoErr is returned.
int8_t Syn1588::getLeapInfo(uint32_t &leap_sec, uint32_t &leap_applytime) {
  leap_sec = m_syn1588Ifc.readReg(Syn1588Ifc::LEAP_SEC);
  leap_applytime = m_syn1588Ifc.readReg(Syn1588Ifc::LEAP_APPLYTIME);
  return ENoErr;
}

/// Get the current PTP sync status.
/// On success ENoErr is returned.
int8_t Syn1588::getPtpSyncStatus(SYN1588_PTP_SYNC_STATUS &status) {

  status = m_syn1588Ifc.readReg(Syn1588Ifc::PTP_SYNC_STATUS);

  return ENoErr;
}

bool Syn1588::providesPTPPacketAttachedTimestamp(void) const {
  return (TsTimestampAttachedToPacket[m_PTPTimestamper]
          & m_syn1588Ifc.canProvidePacketAttachedTimestamp());
}

/// Configure the timestamper instance \a ti with a pattern \a pat and mask \a msk.
/// The capture length is set to \a len. The data to be extracted is defined by
/// two offsets in \a exOff and two lengths \a exLen. This data is expected to
/// be filled into the respective timestamp FIFO in front of the timestamp and will
/// be returned in additional fields of the \a pollTimestamp and \a getTimestamp
/// functions. Please refer to the application note an_syn1588_miits.pdf for detailed
/// information about the timestampers. On success ENoErr is returned.
///
/// Relationships between \a configTimestamper, \a pollTimestamp and \a
/// getSingleTimestamp:
///
/// There is a requirement regarding the \a exLen[0] and \a exlen[1] parameter of
/// configTimestamper. The sum of extraction length 0 (\a exLen[0]) and extraction
/// length 1 (\a exLen[1]) has to be divisible by 4.
///
/// In other words: \a exLen[0] + \a exLen[1] % 4 == 0
///
/// The \a len parameter of \a pollTimestamp and/or \a getSingleTimestamp has to be the sum
/// of the \a exLen[0] + \a exLen[1] parameters of \a configTimestamper.
int8_t Syn1588::configTimestamper(TsInstance_t ti, uint8_t *pat, uint8_t *msk, uint8_t len,
    uint8_t *exOff, uint8_t *exLen) {

  uint32_t h    = 0;
  uint16_t base = TsMemBase[ti];

  // write pattern and mask memory:
  // one word: msk[1] | pat[1] | msk[0] | pat[0]
  // patch for HW: write 80 byte every time
  for (uint8_t i = 0; i < 80; i++) {
    uint32_t mp = ((uint32_t) msk[i] << 8) | pat[i];
    h = i & 1 ? h | mp << 16 : mp;
    if ((i & 1) || (i == len - 1))
      m_syn1588Ifc.writeReg(base + ((i & ~1) << 1), h);
  }

  // write control register
  base = TsCtrl[ti];

  // modify exLen[1] for minimum overall prefix length
  if ((exLen[0] + exLen[1]) < (MIN_PREFIX_LEN * 4))
    exLen[1] = MIN_PREFIX_LEN * 4 - exLen[0];
  // RX extract:           SeqId off | SrcIP off| SeqId len | SrcIP len| cap len
  m_syn1588Ifc.writeReg(base, ((uint32_t) exOff[1] << 24) |
                              ((uint32_t) exOff[0] << 16) |
                              ((uint32_t) exLen[1] << 12) |
                              ((uint32_t) exLen[0] << 8)  |
                              len);
  // set interrupt enable
  if (ti & 1) m_syn1588Ifc.registerEvent(TsEvent[ti]);

  return ENoErr;
}

/// Get a Timestamp for a PTP packet. Construct pattern to identify a timestamp
/// and poll for a timestamp. This is the function called by the PTP Stack to
/// match a PTP packet according to the PTP sequence ID \a seqId and the the PTP
/// Port ID \a pId of the packet. \a recv defines if a RX (true) or a TX (false)
/// timestamp is  requested. The timestamp is stored in \a ts and the sub nanosecond
/// part (if available) is stored in \a subNs.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::getTimestamp(bool recv, Timestamp_t &ts, TimeInterval_t &subNs,
    const PortId_t *pId, const uint16_t seqId) {

  uint8_t pattern[12];

  ::memcpy(pattern, pId, sizeof(PortId_t) - 2); // ClkID - Port number
  pattern[8]  = (pId->port >> 8) & 0xFF;
  pattern[9]  = pId->port & 0xFF;
  pattern[10] = (seqId >> 8) & 0xFF;
  pattern[11] = seqId & 0xFF;
  return pollTimestamp(recv ? TsRx[m_PTPTimestamper] : TsTx[m_PTPTimestamper],
                       ts, subNs, pattern, sizeof(PortId_t) + sizeof(seqId));
}

/// Poll for a timestamp from the instance \a ti into \a ts. This function repeatedly
/// calls \a getTimestamp until the extraction pattern \a pat of length \a len matches.
///
/// Relationships between \a configTimestamper, \a pollTimestamp and \a
/// getSingleTimestamp:
///
/// There is a requirement regarding the \a exLen[0] and \a exlen[1] parameter of
/// configTimestamper. The sum of extraction length 0 (\a exLen[0]) and extraction
/// length 1 (\a exLen[1]) has to be divisible by 4.
///
/// In other words: \a exLen[0] + \a exLen[1] % 4 == 0
///
/// The \a len parameter of \a pollTimestamp and/or \a getSingleTimestamp has to be the sum
/// of the \a exLen[0] + \a exLen[1] parameters of \a configTimestamper.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::pollTimestamp(TsInstance_t ti, Timestamp_t &ts, TimeInterval_t &subNs,
    const uint8_t *pat, const uint8_t len) {

  uint8_t prefix[32];
  bool    gotTs    = false;
  uint8_t wrong_ts = 0;

  while ((wrong_ts < MAX_WRONG_TS) && (!gotTs)) {
    if (!getSingleTimestamp(ti, ts, subNs, prefix, len)) break;
    // check validity
    if (::memcmp(pat, prefix, len) == 0) {
      gotTs = true;
    } else {
      wrong_ts++;
      m_log->Debug("%s: Pattern check failed (MSG FIFO): ", TsString[ti]);
      for (int i = 0; i < len; i++) {
        m_log->Debug("(%02X %02X)", pat[i], prefix[i]);
      }
      m_log->Debug("\n");
    }
  }
  if (wrong_ts) m_log->Notice("%s: Foreign %d timestamp(s) detected\n", TsString[ti], wrong_ts);
  if (!gotTs) {
    m_log->Warning("%s: No Timestamp available\n", TsString[ti]);
    return EErr;
  }
  return ENoErr;
}

/// Get a single timestamp from the timestamper instance \a ti into \a ts without
/// blocking or polling for the timetamp.
/// The timestamp fifo is checked to be not empty and contain a whole timestamp
/// entry. It is cleared, if an error condition occurs. The contents of
/// \a len extracted packet words is copied to \a pat.
///
/// Relationships between \a configTimestamper, \a pollTimestamp and \a
/// getSingleTimestamp:
///
/// There is a requirement regarding the \a exLen[0] and \a exlen[1] parameter of
/// configTimestamper. The sum of extraction length 0 (\a exLen[0]) and extraction
/// length 1 (\a exLen[1]) has to be divisible by 4.
///
/// In other words: \a exLen[0] + \a exLen[1] % 4 == 0
///
/// The \a len parameter of \a pollTimestamp and/or \a getSingleTimestamp has to be the sum
/// of the \a exLen[0] + \a exLen[1] parameters of \a configTimestamper.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::getSingleTimestamp(TsInstance_t ti, Timestamp_t &ts, TimeInterval_t &subNs,
    uint8_t *pat, const uint8_t len) {

  // round up size of Timestamp_t and add (also rounded) prefix length
  uint8_t prfxLen = len < MIN_PREFIX_LEN * 4 ?
                    MIN_PREFIX_LEN :
                   (len + sizeof(uint32_t) - 1) / sizeof(uint32_t);

  // check if new timestamp-struct format has to be used
  uint8_t expEntries;
  if (m_newTSStruct && !m_ultimateTS) {
    expEntries = prfxLen + (sizeof(Timestamp_t) - sizeof(uint16_t) + sizeof(uint32_t) - 1) /
                           sizeof(uint32_t);
  } else {
    // If ultimate TS is enabled, new TSStruct is active as well. Therefore we never have more
    // TS entries than this:
    expEntries = prfxLen + (sizeof(Timestamp_t) + sizeof(uint32_t) - 1) / sizeof(uint32_t);
  }

  uint32_t status = 0;
  for (uint8_t loop = 0; loop < MAX_TS_READOUT; loop++) {
    status = m_syn1588Ifc.readReg(TsStatusAddr(ti));
    if ((TsEmpty(ti) & status) || (TsCount(ti, status) % expEntries)){
      // in case of TX fifo wait
      if (ti & 1) m_syn1588Ifc.waitForEvent(10);
      continue;
    }

    // read prefix, whatever is in there
    uint32_t p = 0;
    for (uint8_t i = 0; i < prfxLen * 4; i++) {
      if ((i & 3) == 0) {
        p = m_syn1588Ifc.readReg(TsFifo[ti]);
      } else {
        p <<= 8;
      }
      if (i < len) pat[i] = (uint8_t) (p >> 24);
    }
    // read timestamp, check if new timestamp format has to be used
    if (m_newTSStruct) {
      ts.sec_h = 0;
    } else {
      ts.sec_h = (uint16_t) m_syn1588Ifc.readReg(TsFifo[ti]);
    }

    ts.sec_l = m_syn1588Ifc.readReg(TsFifo[ti]);
    ts.nsec  = m_syn1588Ifc.readReg(TsFifo[ti]);

    if (m_ultimateTS) {
      // fetch the extended timestamp.
      // right now, we have 2 bits (.0 ,.25, .50 and .75), use scaled ns format
      subNs.scaledNs = m_syn1588Ifc.readReg(TsFifo[ti]);
    } else {
      subNs.scaledNs = 0;
    }

    return ENoErr;
  }
  m_log->Info("%s status: 0x%08x\n", TsString[ti], status);
  clearFifo(ti);
  return EErr;
}

/// Calculte the clock rate. This function performs the correct calculation
/// for the step size. It uses an accurate way to calculate the invers of
/// the clock frequency. The initial rate may be corrected by \a drift.
/// \a ctrl_mode provides the possibility to distinguish different modes.
/// It requires more CPU resources than the standard adjustRate
/// function but results in less inaccuracy.
void Syn1588::initRate(const TimeInterval_t &drift, uint8_t ctrl_mode) {
  (void)ctrl_mode; // explicitely dont use parameter ctrl_mode

  // use initial drift calculation from driver
  uint32_t sh, sl; // values for the step registers
  uint32_t values[11];
  int64_t  rest;

  int64_t a = NSPERSECscaled - drift.scaledNs;
  int64_t freq = (int64_t) m_freq << 16;


  // Calculate the inverse
  for (uint8_t i = 0; i < 11; i++) {
    values[i]  = (uint32_t)(a / freq);
    rest       = a % freq;
    a          = rest << 4;
  }

  // shifting the values at the correct location in the registers:
  // step_ns(sh) holds the whole numbers part + 1 byte of the rest.
  // step_sub_ns(sl) holds everything else
  sh = values[0]<<8 | (values[1]&0xF)<<4 | (values[2]&0xF);
  sl = (values[3]&0xF) << 28  | (values[4]&0xF) << 24 |
       (values[5]&0xF) << 20  | (values[6]&0xF) << 16 |
       (values[7]&0xF) << 12  | (values[8]&0xF) <<  8 |
       (values[9]&0xF) <<  4  | (values[10]&0xF);

  m_log->Debug("Setting step to (%08lX, %08lX)\n", sh, sl);

  setStep(sh, sl);
}

/// Adjust the clock rate by \a drift, which must be interpreted as
/// a time interval relative to a scaling interval \a ival (scaledNs/sec).
/// scaled nanoseconds must be interpreted as nanoseconds shifted left by 16.
/// \a ival is meant to be an exponential at the power of 2: 0 means relative
/// to 1 second, 1 means relative to 2 seconds, -1 means relative to 0.5 seconds,
/// and so on.
void Syn1588::adjustRate(const TimeInterval_t &drift, uint8_t ctrl_mode, int8_t ival) {
  (void)ival; // explicitely dont use parameter ival

  m_log->Info("Adjusting Rate by %d.%u ns/s\n", -int32_t(drift.scaledNs >> 16), (uint16_t)drift.scaledNs);

#if defined __NIOS__ && !defined _NO_ANALOG_RATE_ADJUST_
  // store the current drift in member var, for use in getDrift()
  m_drift = drift;
  // adjust TCXO frequency
  int64_t driftns = -int64_t(drift.scaledNs);  // change sign
  int32_t freq = 0x0;
  m_tc->calcFrequencyControl(driftns, &freq);
  m_log->Debug("Calculated tcxo freq ctrl word: 0x%X\n", freq);
  m_tc->setFreq(freq);
#else

  // Use precise step calculation by default
  initRate(drift, ctrl_mode);

#endif // !_NO_ANALOG_RATE_ADJUST_
}

/// Sets the higher part of the shadow step register to \a sh and
/// the lower part to \a sl. Afterwards, it activates their content.
void Syn1588::setStep(uint32_t sh, uint32_t sl) {

  // Write the STEP registers
  m_syn1588Ifc.writeReg(Syn1588Ifc::SHDWSTEPPU_H, sh);
  m_syn1588Ifc.writeReg(Syn1588Ifc::SHDWSTEPPU_L, sl);
  // Activate their contents by writing the TIMECTRL register
  m_syn1588Ifc.clearBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TICB_LSTEP);
  m_syn1588Ifc.setBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TICB_LSTEP);
}

/// Add time to compensate an offset \a offset. May be used to
/// eliminate an offset with a single operation. \a offset must be a negative
/// value and not larger than 1 second.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::compensateOffset(const TimeInterval_t &offset) {

  // only negative offsets can be added
  if (m_subOffset == false && offset.scaledNs > 0 ) {
    return EErr;
  }

  // Check boundaries
  if ((offset.scaledNs < -NSPERSECscaled) || (offset.scaledNs > NSPERSECscaled)) {
    return EErr;
  }

  // save event event control
  uint32_t evctrl = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTCTRL);
  m_syn1588Ifc.writeReg(Syn1588Ifc::EVENTCTRL, 0x00);

  m_log->Notice("syn1588: compensating %d ns\n", PRINT_SCALED_NS(offset.scaledNs));

  if (offset.scaledNs > 0) {
      // subtract time by stopping the clock for a certain number of clock cycles
#if defined __NIOS__ && !defined _NO_ANALOG_RATE_ADJUST_
    uint32_t reg = (uint32_t)(FROM_SCALED_NS(offset.scaledNs) / (NSEC_PER_SEC / m_freq));
#else
    // calculate the current step size from the current drift instead of using the nominal step size
    TimeInterval_t drift;
    getDrift(drift, 0);
    double currStep = ((double)NSEC_PER_SEC - (double)FROM_SCALED_NS(drift.scaledNs)) / (double)m_freq;

    uint32_t reg = (uint32_t)((double)FROM_SCALED_NS(offset.scaledNs) / currStep);
#endif
    m_syn1588Ifc.writeReg(Syn1588Ifc::SHDWTIME_NS, reg);
    m_syn1588Ifc.setBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TCIB_STOPCLOCK);
  } else {
    // add time
    uint32_t reg = (uint32_t)FROM_SCALED_NS(-offset.scaledNs) & 0x3FFFFFFF; // 30 bit only

    m_syn1588Ifc.writeReg(Syn1588Ifc::SHDWTIME_NS, reg);
    m_syn1588Ifc.setBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TICB_ADDTIME);
  }

  // restore event control
  m_syn1588Ifc.writeReg(Syn1588Ifc::EVENTCTRL, evctrl);

  return ENoErr;
}

/// Clear the fifo of timestamper instance \a ti.
/// On success ENoErr is returned.
int8_t Syn1588::clearFifo(TsInstance_t ti) {

  size_t cnt = 0;
  m_log->Debug("%s: clearFifo\n", TsString[ti]);
  uint32_t status = 0;
  status = m_syn1588Ifc.readReg(TsStatusAddr(ti));

  // empty Fifo; oversize of 8 because in meanwhile new TSs could be entered
  do {
    uint32_t d = m_syn1588Ifc.readReg(TsFifo[ti]);
    cnt++;
    m_log->Debug("%08lX\n", d);
    status = m_syn1588Ifc.readReg(TsStatusAddr(ti));
  } while (!(TsEmpty(ti) & status) && cnt < (size_t)(RXTX_FIFO_SIZE*1.2));// SIZE + %20

  if (cnt == (size_t)(RXTX_FIFO_SIZE*1.2)) {
    m_log->Warning("Fifo clear error\n");
  }

  // read status again
  status = m_syn1588Ifc.readReg(TsStatusAddr(ti));
  if(TsEmpty(ti) & status){
    m_log->Debug("fifo is cleared completely! \n");
  } else {
    m_log->Debug("fifo has still %d time stamps! \n",TsCount(ti, status));
  }

  return ENoErr;
}

/// Generate a (possibly periodic) event using one of two period/trigger
/// ressources (denoted by \a nr, 0 or 1) and route it to \a port (0..3).
/// The event start time is denoted by \a start, while its period is denoted
/// by the interval \a per. A value of 0 for the period \a per deactivates the
/// periodicity of events (resulting in one-shots, if \a start is unequal 0).
/// To start an periodic event immediately, the second value of \a start should
/// be 0.
/// To disable an event for a resource, \a start and \a per should be 0.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::generateEvent(const uint8_t port, const int8_t nr,
    const Timestamp_t &start, const TimeInterval_t &per) {

  if ((port > 3) || (nr > 1)) {
    m_log->Warning("generatePeriod: invalid port|nr arguments\n");
    return EErr;
  }
  // set trigger and period time registers
  uint32_t sec = (uint64_t)(per.scaledNs / NSPERSECscaled) & uint32_t(-1);
  uint64_t nsec = per.scaledNs - NSPERSECscaled * sec;

  return generateEvent(port, nr, start,
      nsec & uint32_t(-1), (sec << 16) + ((nsec >> 32) & uint32_t(-1)));
}

/// Generate a (possibly periodic) event using one of two period/trigger
/// ressources (denoted by \a nr, 0 or 1) and route it to \a port (0..3).
/// The event start time is denoted by \a start, while its period is denoted
/// by the raw register values \a per_l and \a per_h. A value of 0 for the
/// period \a per_l and \a per_h deactivates the periodicity of events
/// (resulting in one-shots, if \a start is unequal 0). To start a
/// periodic event immediately, the second value of \a start should be 0.
/// To disable an event for a resource, \a start, \a per_l and \a per_h should
/// be 0.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::generateEvent(const uint8_t port, const int8_t nr,
    const Timestamp_t &start, uint32_t per_l, uint32_t per_h) {

  if ((port > 3) || (nr > 1)) {
    m_log->Warning("generatePeriod: invalid port|nr arguments\n");
    return EErr;
  }

  // handle event control register
  uint32_t evctrl = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTCTRL);
  if (((per_l != 0) || (per_h != 0)) && nr >= 0) {
    // activate periodtimer
    evctrl |= uint32_t(5) << (4 + nr);
    if (start.sec_l != 0) {
      // start with trigger
      if (nr == 0) evctrl |= uint32_t(3) << 15;
      else evctrl = (evctrl & ~(uint32_t(1) << 18)) | (uint32_t(1) << 17);
    }
  } else {
    // deactivate periodtimer
    evctrl &= ~(uint32_t(5) << (4 + nr));
  }
  // handle trigger, start if seconds are set
  if ((start.sec_l != 0) && (nr >= 0)) {
    evctrl |= uint32_t(4) << nr;
  } else {
    evctrl &= ~(uint32_t(4) << nr);
  }
  m_syn1588Ifc.writeReg(Syn1588Ifc::EVENTCTRL, evctrl);


  // set trigger and period time registers
  if (nr == 0) {
    if (start.sec_l || start.nsec) {
      m_syn1588Ifc.writeReg(Syn1588Ifc::TRIGTIME0_L, start.nsec);
      m_syn1588Ifc.writeReg(Syn1588Ifc::TRIGTIME0_H, start.sec_l);
    }
    m_syn1588Ifc.writeReg(Syn1588Ifc::PERIODTIME0_L, per_l);
    m_syn1588Ifc.writeReg(Syn1588Ifc::PERIODTIME0_H, per_h);
  } else {
    if (start.sec_l || start.nsec) {
      m_syn1588Ifc.writeReg(Syn1588Ifc::TRIGTIME1_L, start.nsec);
      m_syn1588Ifc.writeReg(Syn1588Ifc::TRIGTIME1_H, start.sec_l);
    }
    m_syn1588Ifc.writeReg(Syn1588Ifc::PERIODTIME1_L, per_l);
    m_syn1588Ifc.writeReg(Syn1588Ifc::PERIODTIME1_H, per_h);
  }

  // route source of the event (see IOMATRIX register) to output
  uint8_t src = nr < 0 ?
                  Syn1588Ifc::IOM_SRC_DIS :           // disabled
                  ((per_l != 0) || (per_h != 0)) ?
                    Syn1588Ifc::IOM_SRC_PER0 + nr :   // period 0/1
                    start.sec_l != 0 ?
                      Syn1588Ifc::IOM_SRC_TRG0 + nr : // trigger 0/1
                      Syn1588Ifc::IOM_SRC_DIS;        // disabled
  setIomatrix(smaDst(port), src);
  return ENoErr;
}

/// Generate a 1PPS output on \a port.
/// @param [in] port output SMA port [0...3]
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::generate1pps(const uint8_t port) {
  if (port > 3) {
    m_log->Warning("generate1pps: invalid port argument\n");
    return EErr;
  }
  // route 1pps (see IOMATRIX register) to output
  setIomatrix(smaDst(port), Syn1588Ifc::IOM_SRC_1PPS);
  return ENoErr;
}

///
/// Generate IRIG-B putput on port \a port.
/// @param [in] port output SMA port [0...3]
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::generateIRIGB(const uint8_t port) {

  m_syn1588Ifc.setBit(Syn1588Ifc::EVENTCTRL, Syn1588Ifc::EVCB_IRIGB);

  if (port > 3) {
    m_log->Warning("generateIRIGB: invalid port argument\n");
    return EErr;
  }
  // route IRIG-B (see IOMATRIX register) to output (NIC only)
  setIomatrix(smaDst(port), Syn1588Ifc::IOM_SRC_IRIGB);
  return ENoErr;
}

/// Set IOMATRIX register according to \a dst (bit offset) and \a src (value).
/// On success ENoErr is returned.
int8_t Syn1588::setIomatrix(const uint8_t dst, const uint8_t src) {
  uint32_t d = m_syn1588Ifc.readReg(Syn1588Ifc::IOMATRIX);
  d &= ~((uint32_t) Syn1588Ifc::IOM_SRC_MSK << dst);
  d |= (uint32_t) src << dst;
  m_syn1588Ifc.writeReg(Syn1588Ifc::IOMATRIX, d);
  return ENoErr;
}

/// Translates the I/O port (0-3) to the respective SMA source of the
/// IO Matrix register.
uint8_t Syn1588::smaSrc(uint8_t smaPort) {
  switch(smaPort) {                    // Input    | Output in IOMATRIX source encoding
    case 0:
      return Syn1588Ifc::IOM_SRC_SMA0; // 0 -> X4  | 0x1 -> SMA Connector 0 (X4)
    case 1:
      return Syn1588Ifc::IOM_SRC_SMA1; // 1 -> X7  | 0x4 -> SMA Connector 1 (X7)
    case 2:
      return Syn1588Ifc::IOM_SRC_SMA2; // 2 -> X6  | 0x3 -> SMA Connector 2 (X6)
    case 3:
      return Syn1588Ifc::IOM_SRC_SMA3; // 3 -> X5  | 0x2 -> SMA Connector 3 (X5)
    default:
      assert(smaPort <= 3);
      // in the case no valid smaPort is given we deliver
      // an invalid destination value
      return Syn1588Ifc::IOM_DST_INVALID;
  }
}

/// Translates the I/O port (0-3) to the respective SMA destination of the
/// IO Matrix register.
uint8_t Syn1588::smaDst(uint8_t smaPort) {
  // dpending on the sma port number we have to return the bit position in the
  // io matrix register. The bit positions are defined through constants.
  switch(smaPort) {
    case 0:
      return Syn1588Ifc::IOM_DST_SMA0;
    case 1:
      return Syn1588Ifc::IOM_DST_SMA1;
    case 2:
      return Syn1588Ifc::IOM_DST_SMA2;
    case 3:
      return Syn1588Ifc::IOM_DST_SMA3;
    default:
      assert(smaPort <= 3);
      // in the case no valid smaPort is given we deliver
      // an invalid destination value
      return Syn1588Ifc::IOM_DST_INVALID;
  }
}

/// Translates the event number (0-1) to the correct start bit position for the
/// IO-Matrix register.
uint8_t Syn1588::evtDst(uint8_t evtNr) {
  // dpending on the sma port number we have to return the bit position in the
  // io matrix register. The bit positions are defined through constants.
  switch(evtNr) {
    case 0:
      return Syn1588Ifc::IOM_DST_EVT0;
    case 1:
      return Syn1588Ifc::IOM_DST_EVT1;
    default:
      assert(evtNr <= 1);
      // in the case no valid smaPort is given we deliver
      // an invalid destination value
      return Syn1588Ifc::IOM_DST_INVALID;
  }
}

/// Clear the event fifo for event 0
void Syn1588::clearEventFifo() {
  for (uint8_t i = 0; i < EVENTFIFO_SIZE + 2; i++)
    m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME0_H);
}

/// Register incoming events on \a port using one of two event capture
/// ressources (\a nr, 0 or 1). Subsequent calls of \a waitForEvent will
/// block until such events happen.
/// To disable event capturing on a port, set \a nr to -1
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::registerEvent(const uint8_t port, const int8_t nr) {
  if ((port > 3) || (nr > 1)) {
    m_log->Warning("registerEvent: invalid port|nr arguments\n");
    return EErr;
  }
  if (nr >= 0) {

    // activate event input
    m_syn1588Ifc.setBit(Syn1588Ifc::EVENTCTRL, 1 << nr);

    // clear event fifo
    if (nr == 0) {
      clearEventFifo();
    }
        // activate interrupt
    m_syn1588Ifc.registerEvent(((uint32_t) (nr + 1) << 10));

    // connect port to event input && disconnect any previously set output on port
    setIomatrix(evtDst(nr),   smaSrc(port));
    setIomatrix(smaDst(port), Syn1588Ifc::IOM_SRC_DIS);
  } else {
    uint32_t d = m_syn1588Ifc.readReg(Syn1588Ifc::IOMATRIX);
    // deactivate event input on port
    // first re-assign event number
    int8_t num = (uint8_t)((d >> Syn1588Ifc::IOM_DST_EVT0) & Syn1588Ifc::IOM_SRC_MSK) ==
      Syn1588Ifc::IOM_SRC_SMA0 + port ? 0 :
      (uint8_t)((d >> Syn1588Ifc::IOM_DST_EVT1) & Syn1588Ifc::IOM_SRC_MSK) ==
      Syn1588Ifc::IOM_SRC_SMA0 + port ? 1 :
      -1;
    if (num < 0) return EErr;
    m_syn1588Ifc.clearBit(Syn1588Ifc::EVENTCTRL, num);
  }
  return ENoErr;
}

/// Disable (Un-register) an event /nr (0 or 1) on /a port.
/// After the call, the specified event is no longer registered
/// and no interrupts will be generated.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::disableEvent(const uint8_t port, const uint8_t nr) {
  if ((port > 3) || (nr > 1)) {
    m_log->Error("disableEvent: invalid port|nr arguments\n");
    return EErr;
  }

  // deactivate event input
  m_syn1588Ifc.clearBit(Syn1588Ifc::EVENTCTRL, 1 << nr);

  // clear event fifo (only for event 0)
  if (nr == 0) {
    clearEventFifo();
  }

  // disable interrupt for event nr
  // we shift left by 10, because EVENT0 enable is bit10 in EVENTCTRL,
  // and EVENT1 enable is bit11
  m_syn1588Ifc.disableEvent(((uint32_t)(nr + 1) << 10));

  // reset IOMATRIX event source
  setIomatrix(evtDst(nr), Syn1588Ifc::IOM_SRC_DIS);

  return ENoErr;
}

/// Disable a period timer \a nr (0 or 1)
/// @return ENoErr on success
int8_t Syn1588::disablePeriod(const uint8_t nr) {

  uint32_t eventCtrl = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTCTRL);
  eventCtrl &= ~(Syn1588Ifc::EVCB_PERIOD0_OUT << nr);
  eventCtrl &= ~(Syn1588Ifc::EVCB_PERIOD0 << nr);
  m_syn1588Ifc.writeReg(Syn1588Ifc::EVENTCTRL, eventCtrl);

  return ENoErr;
}

/// Disable a trigger \a nr (0 or 1)
/// @return ENoErr on success
int8_t Syn1588::disableTrigger(const uint8_t nr) {

  uint32_t eventCtrl = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTCTRL);
  eventCtrl &= ~(Syn1588Ifc::EVCB_TRIGGER0 << nr);
  m_syn1588Ifc.writeReg(Syn1588Ifc::EVENTCTRL, eventCtrl);

  return ENoErr;
}

/// Wait for events previously registered with \a registerEvent(). The I/O
/// parameter \a mask is used as a mask for event ressources (bits 0 and 1) on
/// input and specifies which event occured when used as an output.
/// The event timestamp is stored in \a ts.
/// The blocking operation is bounded by the timeout \a tout.
/// @return 1 if an event was detected, 0 otherwise
int8_t Syn1588::waitForEvent(uint8_t &mask, Timestamp_t *ts, const TimeInterval_t &tout) {
  uint32_t src;
  // loop as long as no timeout and no masked bits are active
  uint32_t t = (((uint64_t) tout.scaledNs >> 16) / 1000000) & uint32_t(-1);
  do {
    src = m_syn1588Ifc.waitForEvent(t);
    // we use bits 0 and 1 for mask, so shift src
    src = (src >> 10) & 3;
  } while (src && !(src & mask));
  // adjust mask to return active bits
  mask &= src;

  if (mask & 1) {
    ts[0].nsec  = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME0_L);
    ts[0].sec_l = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME0_H);
    ts[0].sec_h = 0;
  }

  if (mask & 2) {
    ts[1].nsec  = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME1_L);
    ts[1].sec_l = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME1_H);
    ts[1].sec_h = 0;
  }
  // return 0 if no event detected, else 1
  return mask ? 1 : 0;
}

/// pollEvent0
///
/// This function polls the eventtime0 FIFO for an event. If the polling
/// was successful, 1 is returned and the timestamp is stored in \a ts.
/// The blocking function returns after the \a tout ns timed out.
/// @return ENoErr on success, EErr on failure
int8_t Syn1588::pollEvent0(Timestamp_t &ts, const TimeInterval_t &tout) {

  static const uint32_t c_pollingInterval = 50; // polling Interval in ms

  uint32_t t = 1 + ((((uint64_t) tout.scaledNs >> 16) / 1000000) & uint32_t(-1));

  while (t) {
    ts.nsec  = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME0_L);
    ts.sec_l = m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME0_H);
    ts.sec_h = 0;

    // if the timestamp is not 0, there occured an event
    if (ts.nsec || ts.sec_l) {
      clearEvent0Fifo();
      return ENoErr;
    }

    m_syn1588Ifc.waitForEvent(c_pollingInterval);

    if (t > c_pollingInterval) {
      t-= c_pollingInterval;
    } else {
      t = 0;
    }
  }
  return EErr;
}

/// Set the timestamper to use for all following PTP timestamper
/// related calls like initSystemTimestamper, getTimestamp, or clearFifo.
int8_t Syn1588::setTsBase(TsBase_t base) {
  m_PTPTimestamper = base;
  return ENoErr;
}

/// Get the transmit timestamper instance to the currently activated timestamper
/// base.
Syn1588::TsInstance_t Syn1588::getTsTx() {
  return TsTx[m_PTPTimestamper];
}

/// Get the receive timestamper instance to the currently activated timestamper
/// base.
Syn1588::TsInstance_t Syn1588::getTsRx() {
  return TsRx[m_PTPTimestamper];
}

/// clear Event FIFO
///
/// Reads the FIFO of EVENT0 until it is empty.
void Syn1588::clearEvent0Fifo() {

  uint8_t max_ts = 0;
  // empty event 0 fifo
  while (max_ts < MAX_WRONG_TS && m_syn1588Ifc.readReg(Syn1588Ifc::EVENTTIME0_H)) {
    max_ts++;
  }

}

bool Syn1588::ptpTimestamperIs1StepCapable(void) {
  bool general1StepCapability = Ts1StepCapable[m_PTPTimestamper];

#ifdef _SYN1588_EXT_TS_REGS_
  uint32_t m_miits_1StepCapable;
#endif

  uint32_t m_mac_cap0 = m_syn1588Ifc.readReg(Syn1588Ifc::VR_ETH_CAPABILITIES_0);

  bool operational1StepCapability = false;
  switch(m_PTPTimestamper) {
    case TsUsr:
      operational1StepCapability = false;
      break;
    case TsSys:
      operational1StepCapability = true;
      break;
    case TsMac:
    case TsMac2:
      operational1StepCapability = ( m_mac_cap0 & Syn1588Ifc::MAC_CAP0_1STEP_TIMESTAMPER ) != 0;
      break;
#ifdef _SYN1588_EXT_TS_REGS_
    case TsMii2:
      m_miits_1StepCapable = m_syn1588Ifc.readReg(Syn1588Ifc::MIITS2_SPEEDSEL)
                                  & Syn1588Ifc::MIITS_SPEEDSEL_1STEPCAPABLE;
      operational1StepCapability = m_miits_1StepCapable ? true : false;
      break;
    case TsMii3:
      m_miits_1StepCapable = m_syn1588Ifc.readReg(Syn1588Ifc::MIITS3_SPEEDSEL)
                                  & Syn1588Ifc::MIITS_SPEEDSEL_1STEPCAPABLE;
      operational1StepCapability = m_miits_1StepCapable ? true : false;
      break;
    case TsMii4:
      m_miits_1StepCapable = m_syn1588Ifc.readReg(Syn1588Ifc::MIITS4_SPEEDSEL)
                                  & Syn1588Ifc::MIITS_SPEEDSEL_1STEPCAPABLE;
      operational1StepCapability = m_miits_1StepCapable ? true : false;
      break;
    case TsMii5:
      m_miits_1StepCapable = m_syn1588Ifc.readReg(Syn1588Ifc::MIITS5_SPEEDSEL)
                                  & Syn1588Ifc::MIITS_SPEEDSEL_1STEPCAPABLE;
      operational1StepCapability = m_miits_1StepCapable ? true : false;
      break;
#endif
    default:
      operational1StepCapability = false;
  }

  return (general1StepCapability && operational1StepCapability);
}

/// Initialize the System Timestamper for specific network operation. If \a nwLayer2
/// is set to true, Layer 2 operation is expected, otherwise Layer 3 (IP) operation is
/// expected. If \a nwMode is set to 1, unicast operation is assumed, otherwise multicast
/// operation is assumed. If \a vlanId is non-zero, the timestamper is configured for
/// VLAN operation in the given VLAN. Due to the fact that it is necessary to support
/// VLAN 0 and the vlanId is only 12 bit, a mask is used to signal VLAN operation
/// @return ENoErr on success
int8_t Syn1588::initSystemTimestamper(TSConfig tsCfg) {
  // check if the given tsCfg can be achieved with the currently selected Timestamper
  // e.g., the userTS cannot be configured for 1-step operation
  if (!(tsCfg.twoStep) && !(ptpTimestamperIs1StepCapable())) {
    m_log->Error("The PTP operation requires a 1-step capable Timestamper, but the selected timestamper pair (RX: %s, TX: %s) is not 1-step capable!\n",
                 TsString[m_PTPTimestamper],
                 TsString[m_PTPTimestamper+1]);
    return EErr;
  }

  // First, disable timestamper
  m_syn1588Ifc.writeReg(TsCtrl[TsTx[m_PTPTimestamper]], 0);
  m_syn1588Ifc.writeReg(TsCtrl[TsRx[m_PTPTimestamper]], 0);

  // Configure the 1 step timestamper
  // One Step Timestamper must be disabled first. Afterwards we can change
  // the configuration
  if (m_prog1TS) {
    m_syn1588Ifc.writeReg(Ts1StepCtrl[m_PTPTimestamper], 0);
  } else {
    m_syn1588Ifc.clearBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TICB_EN1STEP);
  }

  // Clear TS memory
  uint16_t base1 = TsMemBase[TsTx[m_PTPTimestamper]];
  uint16_t base2 = TsMemBase[TsRx[m_PTPTimestamper]];

  // write needs to be word alligned -> i += 4
  for (uint8_t i = 0; i < MAX_PAT_MASK_LEN; i += 4) {
    m_syn1588Ifc.writeReg(base1 + i, 0);
    m_syn1588Ifc.writeReg(base2 + i, 0);
  }

  uint8_t len = 0;
  uint8_t *pat, *msk;

  // Configure TX timestamper
  tsCfg.recv = false;
  pat = tsCfg.pattern(&len);
  msk = tsCfg.mask();
  configTimestamper(TsTx[m_PTPTimestamper], pat, msk, len,
                    tsCfg.extractOff(),
                    tsCfg.extractLen());

  // Configure RX timestamper
  tsCfg.recv = true;
  pat = tsCfg.pattern(&len);
  msk = tsCfg.mask();
  configTimestamper(TsRx[m_PTPTimestamper], pat, msk, len,
                    tsCfg.extractOff(),
                    tsCfg.extractLen());

  if (tsCfg.timeProto == TSConfig::CTimeProtoNTP) {
    m_syn1588Ifc.setBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TCIB_NTPTS);
  } else {
    m_syn1588Ifc.clearBit(Syn1588Ifc::TIMECTRL, Syn1588Ifc::TCIB_NTPTS);
  }

  if (tsCfg.twoStep) {
    enableTxTsDelivery();
    return ENoErr;
  }

  // Finally, enable the 1-step timestamper again
  if (m_prog1TS) {
    // programmable one step timestamper
    uint32_t data = ((uint32_t)tsCfg.tsPosition()   << 16) |
                     ((uint32_t)tsCfg.udpPosition() << 8);
    m_syn1588Ifc.writeReg(Ts1StepCtrl[m_PTPTimestamper], data);
  } else {
    // VLAN
    if(tsCfg.vlanId)
      m_syn1588Ifc.setBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_VLAN);
    else
      m_syn1588Ifc.clearBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_VLAN);
    // IPv6
    if(tsCfg.nwProto == TSConfig::L3_IPv6)
      m_syn1588Ifc.setBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_IPV6);
    else
      m_syn1588Ifc.clearBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TCIB_IPV6);

    m_syn1588Ifc.setBit(TsTimeCtrl[m_PTPTimestamper], Syn1588Ifc::TICB_EN1STEP);
  }

  return ENoErr;
}

/// This is the probe function to determine if there is a card
/// with the ClockID \a id available in the system.
/// If it finds the appropriate card, it returns a handle to
/// object.
/// @return a handle to the found syn1588 object, or NULL
Syn1588* Syn1588::probe(PTP_LogIfc* log, const uint8_t* id) {

  // Parse through NICs
  uint8_t  card = 0;
  uint8_t  hw_id[8];
  Syn1588* syn1588 = NULL;

  log->Debug("Probing for syn1588(R) hardware...\n");

  while(1) {

    syn1588 = new Syn1588(log, card);
    if(!syn1588->present()) break;

    // We found syn1588 HW, check ClockID
    syn1588->getId(hw_id);
    // compare the 3 left most bytes and the 3 right most bytes.
    // we ignore the middle 2 bytes, they can be different.
    // have a look at: http://standards.ieee.org/develop/regauth/tut/eui64.pdf
    if ((::memcmp(id, hw_id, 3) == 0) && (::memcmp(&id[5], &hw_id[5], 3) == 0)) {
      // We found a NIC with the correct ClockID
      return syn1588;
    }
    delete syn1588;
    card++;
  }
  // There is no card with the given ClockID available
  delete syn1588;
  return NULL;
}
