/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/// @file
/// This file contains the implementation of utilities

#include "simple_utils.h"

int32_t get_ClkId(char *str, uint8_t *clkId) {

  //check if the given string basically adheres to the following rules:
  // 16 digits length: "0123456789abcdef"
  // or 23 digits: "00:11:22:33:44:55:66:77"
  if (((uint8_t)strlen(str) != 16) &&
      ((uint8_t)strlen(str) != 23)) {
        return ERR_STRING;
      }
  //parse
  if (parse_hexId(str,
                  clkId,
                  (uint8_t)strlen(str),
                  (uint8_t)sizeof(clkId),
                  false,
                  0x00) != NOERR) {
    return ERR_STRING;
  }

  // check if clkId is a valid clkId
  if ((clkId[3] != 0xFF) || (clkId[4] != 0xFE)) {
    return ERR_STRING;
  }

  return NOERR;
}

int32_t parse_hexId(char *str,
                    uint8_t *id,
                    uint8_t lenIn,
                    uint8_t lenOut,
                    bool healFaultyNibble,
                    uint8_t faultyNibblePatchValue) {
  /// parse a generic hexadecimal ID string (MAC addr or ClkId)
  /// lenIn is the number of characters in input string \a str
  /// lenOut is the number of elements in output array \a id

  uint8_t h, j;
  bool wasColon = false;
  j = 0;
  for (uint8_t i = 0; i < lenIn; i++) {
    if (j == (2 * lenOut)) break;
    if (!wasColon && (str[i] == ':')) {
      wasColon = true;
      continue;
    }
    wasColon = false;
    if ((str[i] >= '0') && (str[i] <= '9')) h = str[i] - '0';
    else if ((str[i] >= 'a') && (str[i] <= 'f')) h = str[i] - 'a' + 10;
    else if ((str[i] >= 'A') && (str[i] <= 'F')) h = str[i] - 'A' + 10;
    else  {
      if (healFaultyNibble) {
        h = 0x0F & faultyNibblePatchValue;
      } else {
        return ERR_INT;
      }
    }
    if (j % 2) id[j / 2] |= h;
    else id[j / 2] = h << 4;
    j++;
  }

  return NOERR;
}

int32_t convert_hexSdoId(char *str, uint16_t *id, uint8_t lenIn) {
  /// parse a generic hexadecimal ID string (sdoId)
  /// lenIn is the number of characters in input string \a str

  uint8_t h;
 
  for (uint8_t i = 0; i <lenIn; i++) {
    if ((str[i] >= '0') && (str[i] <= '9')) h = str[i]-'0';
    else if ((str[i] >= 'a') && (str[i] <= 'f')) h = str[i] - 'a' + 10;
    else if ((str[i] >= 'A') && (str[i] <= 'F')) h = str[i] - 'A' + 10;
    else {
      return ERR_INT;
    }
    *id = (*id << 4) | (h);
  }
  return NOERR;
}