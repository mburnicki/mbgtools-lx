/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_UTILS
#define INC_UTILS

#include "basictypes.h"
#include "datasets.h" //CLOCKID_LEN
#include "ptptime.h"

enum {
  NOERR = ENoErr,
  OK_NEXTPORT,
  ERR_RANGE,
  ERR_INT,
  ERR_STRING,
  ERR_ARG,
  ERR_UNKOWN,
  ERR_OTHER
};

int32_t get_ClkId(char *str, uint8_t *clkId);
int32_t parse_hexId(char *str,
                    uint8_t *id,
                    uint8_t lenIn,
                    uint8_t lenOut,
                    bool healFaultyNibble,
                    uint8_t faultyNibblePatchValue);
int32_t convert_hexSdoId(char *str, uint16_t *id, uint8_t lenIn);

#endif
