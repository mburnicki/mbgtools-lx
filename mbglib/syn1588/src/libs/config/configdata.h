/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/
#ifndef INC_CONFIGDATA
#define INC_CONFIGDATA

#include "log/config_log.h"
#include "clock/config_clock.h"
#include "sharedmem/config_io.h"
#include "network/config_network.h"
#include "security/config_security.h"

/// @brief ConfigData class
///
/// This class holds the configuration of the syn1588 PTP Stack
class PTP_ConfigData : public PTP_ConfigLog,
                       public PTP_ConfigNet,
                       public PTP_ConfigIo,
                       public PTP_ConfigClk
#ifdef _USE_PTP_SECURITY_
                      , public PTP_ConfigSec
#endif
                       {


  public:
    static const uint16_t CCfgLogChanged = (1 << 0);  ///< mask to indicate changes in logger configuration
    static const uint16_t CCfgNetChanged = (1 << 1);  ///< mask to indicate changes in network configuration
    static const uint16_t CCfgIoChanged  = (1 << 2);  ///< mask to indicate changes in io configuration
    static const uint16_t CCfgClkChanged = (1 << 3);  ///< mask to indicate changes in clock configuration
#ifdef _USE_PTP_SECURITY_
    static const uint16_t CCfgSecChanged = (1 << 4);  ///< mask to indicate changes in security configuration
#endif

    uint16_t configChangedFlags;  ///< flag to indicate a change in configuration

    PTP_ConfigData() : PTP_ConfigLog(),
                       PTP_ConfigNet(),
                       PTP_ConfigIo(),
                       PTP_ConfigClk()
#ifdef _USE_PTP_SECURITY_
                      , PTP_ConfigSec()
#endif
                      {
      configChangedFlags = 0;
    }
};
#endif // INC_CONFIGDATA
