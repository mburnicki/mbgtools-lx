/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_OS
#define INC_OS

#include "basictypes.h"

class OS {
  private:
    static struct timeval currentTime;

  public:
    static const size_t CDateBufferSize = 30;
    // Get Timer period range in ms
    static uint16_t getTimerPeriodRange(uint32_t &min, uint32_t &max);
    // set Timer period
    static uint16_t setTimerPeriod(uint32_t resolution);
    // reset Timer period to previous value,
    // the timer period is automatically reset, when the program is closed
    static uint16_t restoreTimerPeriod();
    /// Get OS Time
    static int8_t getTime(struct timeval &now);
    /// Get OS Time as timestamp_t
    static int8_t getTime(Timestamp_t &now);
    // Get OS Time as a struct
    static char * tsToDateStr(const Timestamp_t &ts);
    //  Return OS Time & date formatted as string
    static char * getDateStr();
    // sleep for some time (ms)
    static void sleep_ms(int time);

  private:
    // remember timer period for restore
    static uint32_t m_timerPeriod;
    // for returning Date String
    static char buffer[CDateBufferSize];
};

#endif
