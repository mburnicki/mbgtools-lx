/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2007
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

// Syn1588 Application Programming Interface

// This API provides an C interface for managing syn1588(R) hardware
// in an operating system independent fashion. It may be used to
// access the features of syn1588(R) hardware like getting/setting
// the current time, configuring the time stamper, or programming
// a frequency output.
//
// THIS CLASS IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
// AS SOURCE CODE

#ifndef _SYN1588_C_API_H
#define _SYN1588_C_API_H

#ifdef _SYN1588_C_API
// Some includes required to initialize some variables.
  #include "syn1588/syn1588.h"
  #include "syn1588_ifc.h"
#endif

#include "basictypes.h"

#if defined( SUPP_SYN1588 )
  #include <mbg_tgt.h>
#else
  #define _DEPRECATED_BY( _s )  // empty definition
#endif


#ifdef _SYN1588_C_API
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


#ifdef __cplusplus
extern "C" {
#endif


/// Data type used for by the SYN1588 API for the device index.
typedef uint8_t SYN1588_DEV_IDX;


/// Data type used for by the SYN1588 API for a return code.
///
/// Usually ::ENoErr or ::EErr, see enum ::Return_e
typedef int8_t SYN1588_RET_VAL;


#if 1  // TODO Do we need this?
#define UNICAST               ( (uint8_t) 0x1 )
#define MULTICAST             ( (uint8_t) 0x2 )
#define UINCAST_AND_MULTICAST ( (uint8_t) 0x3 )
#define HYBRID                ( (uint8_t) 0x4 )
#define LAYER2                ( (uint16_t)  3 )
#define IPv4                  ( (uint16_t)  1 )
#define IPv6                  ( (uint16_t)  2 )
#endif


typedef struct
{
  uint32_t leap_sec;        // from register LEAP_SEC, current leap seconds (8 bit) R (current value / W (future value)
  uint32_t leap_applytime;  // from register LEAP_APPLYTIME, leap seconds apply time (32 bit seconds) R/W
} LEAP_INFO;


/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /// Initialize the C API
///
/// NOTE: if this function is called after configuring one or more devices,
///       the configuration is lost.
///
/// This function brings the system into a defined state:
/// If any Syn1588 objects are existing they are destroyed.
/// If the logger is initialized it will be created anew.
/// @param logLevel: the log level for the logger
/// @return none
void syn1588_init_c_api(uint8_t logLevel) ;

 /// Probe for a SYN1588 card identified by its MAC address.
///
/// This function tries to find a SYN1588 card with the MAC address
/// @a mac. If a card is found, it returns the card's index. Otherwise,
/// -1 is returned.
/// ::syn1588_init still has to be called for the respective card.
/// @param mac: MAC address of the desired SYN1588 card
/// @return On success, the card index, otherwise -1 otherwise
int8_t syn1588_probe(const uint8_t *mac) ;

 /// Initialize a syn1588(R) card.
///
/// Create a SYN1588 object for the card, where the index number
/// is 0 for the first card found by the operating system.
/// @param card: The index number of the card requested.
/// @return On success @a ENoErr is returned else @a EErr
///
SYN1588_RET_VAL syn1588_init(SYN1588_DEV_IDX card) ;

 SYN1588_DEV_HANDLE syn1588_get_handle(SYN1588_DEV_IDX card) ;
 /// Configure the time stamper instance for PTPv2
///
/// Please refer to the application note an_syn1588_miits.pdf for detailed
/// information about the time stampers.
/// @param card: the number of the card requested
/// @param nwProto: the network protocol
/// @param nwMode: the traffic type timestamped
/// @param domain: domain of the traffic timestamped
/// @param vlanId: the vlan tag of the traffic timestamped
/// @param two_step: activate/deactivate two step for outgoing traffic
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_configurePTPv2Timestamper(SYN1588_DEV_IDX card, uint16_t nwProto, uint8_t nwMode, uint8_t domain, uint16_t vlanId, uint8_t two_step) ;

 /// Configure the timestamper for PTPv1 packets
///
/// Most parameters are fixed (IPv4, UDP, two step,
/// multicast only).
/// @param card: the number of the card requested
/// @return On success \a ENoErr is returned else \a EErr
SYN1588_RET_VAL syn1588_configurePTPv1Timestamper(SYN1588_DEV_IDX card) ;

 /// Get a timestamp for a PTPv2 packet.
///
/// The timestamp is retrieved for PortId \a portId
/// and sequence ID \a seqId.
/// @param card: the number of the card requested
/// @param recv: 1 for RX timestamp, 0 for TX timestamp
/// @param ts: pointer to timestamp buffer
/// @param subNs: pointer to sub nanosecond buffer
/// @oaram portId: PTP port ID of the PTPv2 packet
/// @param seqId: sequence ID of the PTPv2 packet
/// @return On success \a ENoErr is returned else \a EErr
SYN1588_RET_VAL syn1588_getTimestamp(SYN1588_DEV_IDX card, uint8_t recv, Timestamp_t *ts, TimeInterval_t *subNs, const PortId_t *portId, uint16_t seqId) ;

 SYN1588_RET_VAL syn1588_get_event_cap(SYN1588_DEV_IDX card, Timestamp_t *cap ) ;
 SYN1588_RET_VAL syn1588_registerEvent(SYN1588_DEV_IDX card, const uint8_t port, const int8_t nr) ;
 uint32_t syn1588_readReg(SYN1588_DEV_IDX card, const uint16_t addr) ;
 SYN1588_RET_VAL syn1588_writeReg(SYN1588_DEV_IDX card, const uint16_t addr, const uint32_t data) ;
 SYN1588_RET_VAL syn1588_setBit(SYN1588_DEV_IDX card, const uint16_t addr, const uint32_t mask) ;
 SYN1588_RET_VAL syn1588_clearBit(SYN1588_DEV_IDX card, const uint16_t addr, const uint32_t mask)  ;
 /// Get the 8 byte PTP clock identifier.
///
/// @param card: the number of the card requested
/// @param id: a pointer to a memory location with at least
///            8 bytes of memory to hold the data
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_getId(SYN1588_DEV_IDX card, uint8_t *id) ;

 /// Get the hardware capabilities.
uint32_t syn1588_getCapabilities(SYN1588_DEV_IDX card) ;

 /// Disable delivery of transmit timestamps.
SYN1588_RET_VAL syn1588_disableTxTsDelivery(SYN1588_DEV_IDX card) ;

 /// Enable delivery of transmit timestamps.
SYN1588_RET_VAL syn1588_enableTxTsDelivery(SYN1588_DEV_IDX card) ;

 /// Get the current time from specified device.
///
/// Please consider the latencies caused by the operating system
/// as well as by the hardware access. It may take up to a few milliseconds
/// until the function returns with the time information. So this function
/// should not be used for precise time information.
/// @param card: the number of the card requested
/// @param now: a pointer to a Timestamp structure to store the time information.
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL _DEPRECATED_BY( "syn1588_getSafeTimeAndStatus" ) syn1588_getTime(SYN1588_DEV_IDX card, struct Timestamp_t *now) ;

 /// Set the clock to time \a t.
///
/// Please consider the latencies caused by the operating system
/// as well as by the hardware access. It may take up to a few milliseconds
/// until the function sets the time at hardware level. So this function
/// should not be used for applying precise timing information to the hardware.
/// @param card: the number of the card requested
/// @param t   : the time for the hardware clock
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_setTime(SYN1588_DEV_IDX card, struct Timestamp_t t) ;

 /// Get leap info ::TODO
SYN1588_RET_VAL syn1588_getLeapInfo(SYN1588_DEV_IDX card, LEAP_INFO *p) ;

 /// Get PTP sync status
SYN1588_RET_VAL syn1588_getPtpSyncStatus(SYN1588_DEV_IDX card, SYN1588_PTP_SYNC_STATUS *p) ;

 /// Safely get the current time from a card
SYN1588_RET_VAL syn1588_getSafeTimestamp(SYN1588_DEV_IDX card, struct Timestamp_t *p_ts) ;

 /// Safely get the current time and sync status from a card
SYN1588_RET_VAL syn1588_getSafeTimeAndStatus(SYN1588_DEV_IDX card, struct TimestampStatus_t *p_tss) ;

 /// Get a string with HW version information
///
/// Unfortunately, neither a buffer size can be specified,
/// nor any precaution is taken to avoid a buffer overflow.
///
/// Looking at Sim1588::getVersionInfo(), at least 25 characters
/// are written to the buffer s.
SYN1588_RET_VAL syn1588_getVersionInfo(SYN1588_DEV_IDX card, char *s) ;

 /// Get syn1588 clock core version.
///
/// Unfortunately, neither a buffer size can be specified,
/// nor any precaution is taken to avoid a buffer overflow.
///
/// Looking at Sim1588::getVersion(), at least 8 characters
/// are written to the buffer s.
SYN1588_RET_VAL syn1588_getVersion(SYN1588_DEV_IDX card, char *s) ;

 /// Get the Version ID of the Ethernet MAC.
///
/// Unfortunately, neither a buffer size can be specified,
/// nor any precaution is taken to avoid a buffer overflow.
///
/// Looking at Sim1588::getMacVersion(), at least 8 characters
/// are written to the buffer s.
SYN1588_RET_VAL syn1588_getMacVersion(SYN1588_DEV_IDX card, char *s) ;

 /// ::TODO
SYN1588_RET_VAL syn1588_getEEPROMMacAddress(SYN1588_DEV_IDX card, char *s) ;

 /// ::TODO
SYN1588_RET_VAL syn1588_getEEPROMBoardRevision(SYN1588_DEV_IDX card, char *s) ;

 // Get the serial number of a card.
SYN1588_RET_VAL syn1588_getSerialNumber(SYN1588_DEV_IDX card, char *s) ;

 /// Get the Build ID of the HW design.
SYN1588_RET_VAL syn1588_getBuildID(SYN1588_DEV_IDX card, uint32_t *id) ;

 /// Set the initial rate according to the clock frequency.
///
/// This function performs the correct calculation for the step size.
/// It uses an accurate way to calculate the inverse of
/// the clock frequency.
/// It requires more CPU resources than the standard adjustRate
/// function but results in less inaccuracy.
/// @param card: the number of the card requested
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_initRate(SYN1588_DEV_IDX card) ;

 /// Adjust the clock rate by \a drift.
///
/// This function uses less CPU resources than the initRate function but is less accurate
/// @param card: the number of the card requested
/// @param drift: a drift to adjust the clock rate
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_adjustRate(SYN1588_DEV_IDX card, const struct TimeInterval_t *drift) ;

 /// Close a previously initialized card.
///
/// Deactivate the time stamper, clear the FIFO and
/// disable further configuration of this card.
/// @param card: the number of the card requested
/// @return On success \a ENoErr is returned else \a EErr
///
SYN1588_RET_VAL syn1588_close(SYN1588_DEV_IDX card) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif

#undef _ext
#undef _DO_INIT

#endif  // _SYN1588_C_API_H
// EOF
