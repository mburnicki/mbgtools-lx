 /********************************************************************
 *           _____           ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#include "os.h"
#include <cinttypes>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


// gcc 7 and newer may generate an ugly warning that e.g. the size
// of a string buffer passed to snprintf() may not be sufficient,
// if the values to be printed require more characters than expected.
// It's tricky to get rid of this warning even if it's sure that the
// required buffer size will always be sufficient.
// We define the symbol below if we need the workaround to avoid
// this ugly warning.
#if !defined( __clang__ ) && defined( __GNUC__ ) && ( __GNUC__ >= 7 )
  #define FIX_WARN_FORMAT_TRUNCATION  1
#endif

char OS::buffer[CDateBufferSize];

/// Convert a timeval structure to a string
void timevalToString(char * buf, size_t max_len, const timeval *now) {

  struct tm newtime;
  time_t sec;

  sec = (time_t)now->tv_sec;

  // Convert time to struct tm form.
  newtime = *gmtime(&sec);

#if defined( FIX_WARN_FORMAT_TRUNCATION )
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-truncation"
#endif
  // Print local time to string (newtime.tm_year is given as years from 1900).
  snprintf(buf, max_len, "%04d-%02d-%02d %02d:%02d:%02d.%06ld",
    (newtime.tm_year + 1900), (newtime.tm_mon + 1), newtime.tm_mday,
    newtime.tm_hour, newtime.tm_min, newtime.tm_sec, (long) now->tv_usec);
#if defined( FIX_WARN_FORMAT_TRUNCATION )
#pragma GCC diagnostic pop
#endif
}

int8_t OS::getTime(struct timeval &now) {
  if (::gettimeofday(&now, 0) < 0) return 0;
  return 1;
}

char * OS::tsToDateStr(const Timestamp_t &ts) {

  timeval now;

  // omit sec_h since windows can't deal with it anyway
  now.tv_sec = ts.sec_l;
  now.tv_usec = ts.nsec / 1000;

  timevalToString(OS::buffer, sizeof(OS::buffer), &now);

  return OS::buffer;
}

char * OS::getDateStr() {

  timeval now;

  OS::getTime(now);

  timevalToString(OS::buffer, sizeof(OS::buffer), &now);

  return OS::buffer;
}

void OS::sleep_ms(int sleep){
  usleep(sleep*1000);
}

/// @returns Processor frequency in MHz
float getProcessorFrequency() {

  static const char *fileName = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq";

  FILE *infoFile = fopen(fileName, "r");

  if (infoFile) {
    // This buffer will be sufficient, since the file only contains the mhz of
    // the processor as a string
    char buffer[100];

    if (fgets(buffer, 100, infoFile) == NULL) {
      printf("Error reading from CPU Frequency file '%s': %s\n", fileName, strerror(errno));
    } else {
      long unsigned int hertz = atoi(buffer);
      fclose(infoFile);
      return static_cast<float>(hertz) / 1000.0f;
    }
    fclose(infoFile);
  } else {
    printf("Error opening CPU frequency file '%s': %s\n", fileName, strerror(errno));
  }
  return 1000.0f;
}
