/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/
#ifndef INC_CONFIG_NETWORK
#define INC_CONFIG_NETWORK

#include "basictypes.h"
#include "config_ifc.h"

#ifndef PTP_MC_TTL
#define PTP_MC_TTL 1
#endif

#ifndef PTP_UC_TTL
#define PTP_UC_TTL 128
#endif

/// Network configuration structure

/// This structure defines all network related configuration parameters.
///

struct PTP_ConfigNet {

  static const uint16_t   CPtpEventPort   = 319;
  static const uint16_t   CPtpGeneralPort = 320;

  char                    localAddr[16];     ///< IP address of the local interface
  uint16_t                nwProto;           ///< Network Protocol
  uint8_t                 dsf;               ///< Differentiated Service field (IPv4 header)
  uint8_t                 mcTTL;             ///< Multicast Time to Live field for IPv4 & IPv6
  uint8_t                 ucTTL;             ///< Unicast Time to Live field for IPv4 & IPv6
  uint8_t                 mcScope;           ///< Multicast Address Scope
  bool                    isP2P;             ///< Network Interface is of Peer-toPeer type
  bool                    twoStep;           ///< indicates if two step or one step mode is used
  bool                    hasOneStepSupport; ///< Hardware offers one step support
  bool                    zeroRuntime;       ///< use timing compensation in case of user interaction (e.g. ptpmmm)

  /// Software mode of the PTP Stack (no dedicated hardware support)
  bool                    swmode;

  /// explicity use SoTimestamping when running in software-mode
  bool                    forceSoTimestamping;

  /// Determines if alternative multicast address (non-forwardable)
  /// is used (Layer 2 only)
  bool                    useAltMCAddress;
  bool                    canProvidePacketAttachedTimestamps; ///< for a SO_TIMESTMAPING capable or equivalent setup
#ifdef _DYNAMIC_RECONFIGURATION_
  bool                    update;           ///< flag to indicate that the config has been changed
  bool                    active;           ///< flag to indicate whether the network part of the port is active
  bool                    fault;            ///< flag to indicate a fault in the network
#endif

  typedef struct networkIfcInfo {
    uint8_t   mac[6];
    uint32_t  ifcIndex;
    uint16_t  vlanId;
    enum {
      EUnknown,
      ELayer2,
      EIpV4
#ifndef NO_IPV6
        , EIpV6
#endif
    } mode;

    networkIfcInfo() {
      for (uint32_t i = 0; sizeof(mac) > i; ++i) {
        mac[i] = 0;
      }
      ifcIndex = 0;
      vlanId   = 0;
      mode     = EUnknown;
    }
  } networkIfcInfo_t;
  networkIfcInfo_t ifc;

  /// Default Constructor
  PTP_ConfigNet() {
    for (uint32_t i = 0; sizeof(localAddr) > i; ++i) {
      localAddr[i] = 0;
    }
    nwProto             = PTP_ConfigIfc::CNwProtoL3IPv4;
    dsf                 = 0;
    mcTTL               = PTP_MC_TTL;
    ucTTL               = PTP_UC_TTL;
    mcScope             = 0;
    isP2P               = false;
    twoStep             = PTP_TWOSTEP;
    hasOneStepSupport   = false;
    zeroRuntime         = false;
    swmode              = false;
    forceSoTimestamping = false;
    useAltMCAddress     = false;
    canProvidePacketAttachedTimestamps = true;
    ifc                 = networkIfcInfo();
#ifdef _DYNAMIC_RECONFIGURATION_
    update              = false;
    active              = true;
    fault               = false;
#endif
  }
};
#endif // INC_CONFIG_NETWORK
