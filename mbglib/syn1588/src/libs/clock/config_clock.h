/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_CONFIG_CLOCK
#define INC_CONFIG_CLOCK

#include "clock_ifc.h"
#include "hwclock_ifc.h"
#if defined PTP_CONFIG_DEBUG_DUMP
#include "log_ifc.h"
#endif

#ifndef CLOCK_TSRANGEWIN
#define CLOCK_TSRANGEWIN 2
#endif
// Sets the boundary for reagrding the clock as being in sync (locked)
#ifndef CLOCK_SYNCBOUNDARY
#define CLOCK_SYNCBOUNDARY (((uint64_t)1000) << 16) // 1 us
#endif
// Sets the boundary for changing the epoch (= setting the time to a new value)
#ifndef CLOCK_CHANGEEPOCHBOUNDARY
#define CLOCK_CHANGEEPOCHBOUNDARY (((uint64_t)500000000) << 16) // 500 ms
#endif
#ifndef CLOCK_FASTLOCKINGBOUNDARY
#define CLOCK_FASTLOCKINGBOUNDARY (((uint64_t)1000) << 16) // 1 us
#endif
// Set a boundary for all rate ratios (802.1as)
#ifndef CLOCK_RATE_RATIO_BOUNDARY
#define CLOCK_RATE_RATIO_BOUNDARY TO_SCALED_RATIO(1.001) // +/- 1000 ppm
#endif

#ifndef SERVO_PI_K
  #if defined __NIOS__
  #define SERVO_PI_K       16253
  #else
  #define SERVO_PI_K       16384
  #endif
#endif
#ifndef SERVO_PI_T
  #if defined __NIOS__
  #define SERVO_PI_T        1299
  #else
  #define SERVO_PI_T        512
  #endif
#endif
#ifndef SERVO_IIR_ADJPRD
#define SERVO_IIR_ADJPRD  (4)
#endif
#ifndef SERVO_IIR_ADJGAIN
#define SERVO_IIR_ADJGAIN (16)
#endif
#ifndef SERVO_IIR_SMIN
#define SERVO_IIR_SMIN    (0x8000)
#endif

#ifndef SERVO_PI_K_INBOUNDS
  #if defined __NIOS__
  #define SERVO_PI_K_INBOUNDS     6500
  #else
  #define SERVO_PI_K_INBOUNDS     6500
  #endif
#endif
#ifndef SERVO_PI_T_INBOUNDS
  #if defined __NIOS__
  #define SERVO_PI_T_INBOUNDS     100
  #else
  #define SERVO_PI_T_INBOUNDS     100
  #endif
#endif
#ifndef SERVO_IIR_SMIN_INBOUNDS
#define SERVO_IIR_SMIN_INBOUNDS (0x8000)
#endif
#ifndef _DRIFT_CALC_USE_DOUBLE_
#define _DRIFT_CALC_USE_DOUBLE_ 1
#endif
#ifndef SERVO_ANTI_WINDUP_MAX_DEFAULT
/* Anti Windup limit integrator m_oldval to a maximum of 250 000 000 ns/s */
#define SERVO_ANTI_WINDUP_MAX_DEFAULT 0xEE6B2800000LL
#endif // SERVO_ANTI_WINDUP_MAX_DEFAULT

#ifndef SERVO_DELTA_RATE_MAX_DEFAULT
  #if defined __NIOS__
  #define SERVO_DELTA_RATE_MAX_DEFAULT 0x80000LL
  #else
  #define SERVO_DELTA_RATE_MAX_DEFAULT 0x200000000LL
  #endif
#endif

#ifndef _NO_LUCKY_PACKET_FILTER_
#ifndef LUCKY_PACKET_FILTER_DEPTH
#define LUCKY_PACKET_FILTER_DEPTH -1
#endif
#endif //  _NO_LUCKY_PACKET_FILTER_

/// Configuration structure for PI Controller

/// This structure defines all the parameters for the PI controller.
///
struct ConfigPIController {

  uint32_t   K; //< Proportinal part (shifted left by 16)
  uint32_t   T; //< Integral part (shifted left by 16)
  // Anti Windup value for controller output and controller integrator
  int64_t    antiWindupMax;
#ifndef _NO_RATE_LIMITER_FILTER_
  // Maximum allowed rate change from one output value to the next
  int64_t    deltaRateMax;
#endif

  /// Default Constructor
  explicit ConfigPIController(bool inBounds = false) {
    if (inBounds) {
      K = SERVO_PI_K_INBOUNDS;
      T = SERVO_PI_T_INBOUNDS;
    } else {
      K = SERVO_PI_K;
      T = SERVO_PI_T;
    }
    antiWindupMax = SERVO_ANTI_WINDUP_MAX_DEFAULT;
#ifndef _NO_RATE_LIMITER_FILTER_
    deltaRateMax = SERVO_DELTA_RATE_MAX_DEFAULT;
#endif
  }

  ///compare operator
  bool operator ==(const ConfigPIController& rhs) const {
    if ((K == rhs.K) &&
        (T == rhs.T) &&
# ifndef _NO_RATE_LIMITER_FILTER_
        (deltaRateMax == rhs.deltaRateMax) &&
# endif
        (antiWindupMax == rhs.antiWindupMax)
       ) {
      return true;
    } else {
      return false;
    }
  }
}__PACKME__;

/// Configuration structure for IIR Filter

/// This structure defines all the parameters for the IIR Filter.
///
struct ConfigIIRFilter {

  int8_t     logAdjPrd;  //< log. adjustment period
  int8_t     logAdjGain; //< log. adjustment Gain
  uint16_t   Smin;       //< minimum Stiffness

  /// Default Constructor
  ConfigIIRFilter() :
    logAdjPrd(SERVO_IIR_ADJPRD),
    logAdjGain(SERVO_IIR_ADJGAIN),
    Smin(SERVO_IIR_SMIN) {}

  ///compare operator
  bool operator ==(const ConfigIIRFilter& rhs) const {
    if ((logAdjPrd == rhs.logAdjPrd)   &&
        (logAdjGain == rhs.logAdjGain) &&
        (Smin == rhs.Smin)
       ) {
      return true;
    } else {
      return false;
    }
  }
}__PACKME__;

#if !defined(_NO_SPIKE_FILTER_) && !defined(_NO_CLOCK_STATISTIC_CALC_)
/// @brief Spike Filter configuration structure
///
/// This structure is used to configure a spike filter, it contains all
/// necessary members for valid configuration
struct SpikeFilterConfig {
  /// this value determines the maximum number of spikes before the filter will
  /// reset its buffer. The actual value is calculated like this:
  ///   maxSpikes = 1 << (maxSpikeDuration + ((ival < 0) ? -ival : 0))
  /// As a logical definition for negative message rates this value is the
  /// logarithmic value of "only spikes" seconds after which the filter will
  /// reset.
  /// E.g.: maxSpikeDuration = 2, ival = -1 -> maxSpikes = 8
  ///   With a message rate of -1 (two per second) this means that the filter
  ///   will reset if only spikes occurr for 4 seconds.
  uint16_t maxSpikeDuration;

  /// This value is used to determine when something counts as a spike, if the
  /// difference of the value from the median is larger than
  /// (calculationThreshold * stddev) the value is replaced by the median but
  /// added to the list for statistical purposes
  uint8_t calculationThreshold;

  /// This value is used to determine when something counts as an "unnormal"
  /// spike, if the difference between the value and the median is larger than
  /// (discardThreshold * stddev) the value is discarded entirely and replaced
  /// by the median
  uint8_t discardThreshold;

  /// This is the minimum size for the data buffer of the SpikeFilter, the
  /// actual value is calculated like this:
  ///   bufferSize = 1 << (minLogBufSize + ((ival < 0) ? -ival : 0))
  uint8_t minLogBufSize;

  /// This value determines the number of seconds after which the filter must
  /// start filtering, even if the buffer is not yet full. For very slow message
  /// rates a warning will be output if the number of seconds is defined too
  /// low. E.g.: If the filter only receives one packet every 16 seconds, it
  /// cannot filter meaningfully after 6 seconds, therefore the filter will in
  /// this case fall back to a start after two received packets and output a
  /// warning.
  uint16_t startupSeconds;

  /// This is the default constructor, which sets default values for the configuration
  explicit SpikeFilterConfig() : maxSpikeDuration(2),
                                 calculationThreshold(3),
                                 discardThreshold(6),
                                 minLogBufSize(4),
                                 startupSeconds(2) { }

  /// This is the normal constructor, where all values have to be supplied
  explicit SpikeFilterConfig(uint16_t maxSpikeDurationVal,
                             uint8_t  calculationThresholdVal,
                             uint8_t  discardThresholdVal,
                             uint8_t  minLogBufSizeVal,
                             uint16_t startupSecondsVal) :
                                 maxSpikeDuration(maxSpikeDurationVal),
                                 calculationThreshold(calculationThresholdVal),
                                 discardThreshold(discardThresholdVal),
                                 minLogBufSize(minLogBufSizeVal),
                                 startupSeconds(startupSecondsVal) { }

  /// This is the copy constructor used to create copies from other SpikeFilterConfigs
  /// @param other The SpikeFilterConfig from which to create a copy from
  SpikeFilterConfig(const SpikeFilterConfig& other) :
                                 maxSpikeDuration(other.maxSpikeDuration),
                                 calculationThreshold(other.calculationThreshold),
                                 discardThreshold(other.discardThreshold),
                                 minLogBufSize(other.minLogBufSize),
                                 startupSeconds(other.startupSeconds) { }

  /// This is the assignment operator
  /// @param rhs The SpikeFilterConfig from which the values should be assigned
  SpikeFilterConfig& operator=(const SpikeFilterConfig& rhs) {
    maxSpikeDuration      = rhs.maxSpikeDuration;
    calculationThreshold  = rhs.calculationThreshold;
    discardThreshold      = rhs.discardThreshold;
    minLogBufSize         = rhs.minLogBufSize;
    startupSeconds        = rhs.startupSeconds;

    return *this;
  }
  
  /// compare operator
  bool operator ==(const SpikeFilterConfig& rhs) const {
    if ((maxSpikeDuration     == rhs.maxSpikeDuration)     &&
        (calculationThreshold == rhs.calculationThreshold) &&
        (discardThreshold     == rhs.discardThreshold)     &&
        (minLogBufSize        == rhs.minLogBufSize)        &&
        (startupSeconds       == rhs.startupSeconds)
       ) {
      return true;
    } else {
      return false;
    }
  }
};
#endif

/// Clock configuration structure

/// This structure defines all clock related configuration parameters.
/// The Clock servo is made up of a non-linear low-pass filter, which
/// averages on a reduced set of input values (nearest to the average
/// of the whole set of input values), and a PI controller.

struct PTP_ConfigClk : PTP_ConfigHwClk {

  uint8_t              Id[8];                  ///< Clock Identifier

// include necessary padding, for the structure to have the same size as a
// 64 bit version (32bit version does not need to be as big to be correctly
// aligned)
#ifdef _IS_32_BIT_
  unsigned char __padding[4];
#endif

  ConfigPIController   piOutbound;             ///< PI Controller configuration outOfBounds
  ConfigPIController   piInbound;              ///< PI Controller configuration inBounds
  ConfigIIRFilter      iirM2S;                 ///< IIR Filter M2S configuration
  ConfigIIRFilter      iirPath;                ///< IIR Filter Path configuration
  TimeInterval_t       syncBoundary;           ///< Synchronization Boundary
  TimeInterval_t       changeEpochBoundary;    ///< Change epoch boundary
  TimeInterval_t       fastLockingBoundary;    ///< fast locking boundary

  int8_t               adjInterval;            ///< Clock adjust interval
  uint8_t              tsRangeWin;             ///< Timestamp out of range window
  bool                 noClockAdjustment;      ///< Flag to indicate if the clock is adjusted (flase)
  bool                 lastNoClockAdjustment;  ///< Flag to detect changes during operation (i.e., switch from not adjusting the clock to adjusting the clock
  // or not (true)
  bool                 noInitialization;       ///< Flag to prevent clock re-initialization during
  // operation
  bool                 noDriftCalc;            ///< Flag to prevent drift calculation, even if it is
  // supported by hardware
  bool                 useAnalogAdjusting;     ///< Flag to enable analog clock adjusting
  bool                 useUserTimestamper;     ///< Flag to enable Usage of user timestamper
  int64_t              rateRatioBoundary;      ///< boundary for rate ratio calculations (sanity check)
  /// specify log2 depth of lucky packet filter
#ifndef _NO_LUCKY_PACKET_FILTER_
  int8_t               luckyPacketDepth;       ///< filter depth in log seconds (-1 means lucky packet is disabled)
  uint8_t              reInitLuckyPacket;      ///< flag set to reinitialize luckyPacket filters
  uint32_t             luckyMedian;            ///< count of packets which are used for
  //   median calculation in lucky packet filter
#endif
#ifndef _NO_IIR_FILTER_
  bool                 useIirFilter;           ///< flag to indicate if the IIR filter is used
#endif
#if !defined(_NO_SPIKE_FILTER_) && !defined(_NO_CLOCK_STATISTIC_CALC_)
  bool                 useSpikeFilter;         ///< flag to indicate if the Spike filter is used
  SpikeFilterConfig    spikeFilterConfig;      ///< Configuration for the Spike filter (see servo.h)
#endif
#ifndef _NO_SAMPLE_RATE_CONVERTER_
  bool                 useSampleRateConverter; ///< flag to indicate if the sample rate converter is used
#endif
#ifdef _DYNAMIC_RECONFIGURATION_
  bool                 update;                    ///< flag to command an update of the clock
  bool                 swmode;                    ///< indicates whether the clock runs in software mode
  bool                 fault;                     ///< indicates whether the network is operational or a fault occured
  bool                 reInit;                    ///< indicates if a re-initialization of the clock is required when an update is issued
#endif

  /// Default Constructor
  PTP_ConfigClk() {
    ::memset(Id, 0, sizeof(Id));
    piInbound                    = ConfigPIController(true);
    piOutbound                   = ConfigPIController(false);
    syncBoundary.scaledNs        = CLOCK_SYNCBOUNDARY;
    changeEpochBoundary.scaledNs = CLOCK_CHANGEEPOCHBOUNDARY;
    fastLockingBoundary.scaledNs = CLOCK_FASTLOCKINGBOUNDARY;
    adjInterval                  = PTP_ADJINTERVAL;
    tsRangeWin                   = CLOCK_TSRANGEWIN;
    noClockAdjustment            = false;
    lastNoClockAdjustment        = false;
    noInitialization             = false;
    noDriftCalc                  = false;
    useAnalogAdjusting           = false;
    useUserTimestamper           = false;
    rateRatioBoundary            = CLOCK_RATE_RATIO_BOUNDARY;
#ifndef _NO_LUCKY_PACKET_FILTER_
    luckyPacketDepth             = LUCKY_PACKET_FILTER_DEPTH;
    reInitLuckyPacket            = 0;
#if -1 != LUCKY_PACKET_FILTER_DEPTH
    luckyMedian                  = (1 << LUCKY_PACKET_FILTER_DEPTH) - (1 << LUCKY_PACKET_FILTER_DEPTH) / 10;
#else
    luckyMedian                  = 1;
#endif
#endif //  _NO_LUCKY_PACKET_FILTER_
#ifndef _NO_IIR_FILTER_
    useIirFilter                 = true;
#endif
#if !defined(_NO_SPIKE_FILTER_) && !defined(_NO_CLOCK_STATISTIC_CALC_)
    useSpikeFilter                         = true;
    spikeFilterConfig.maxSpikeDuration     = 2;
    spikeFilterConfig.calculationThreshold = 3;
    spikeFilterConfig.discardThreshold     = 6;
    spikeFilterConfig.minLogBufSize        = 4;
    spikeFilterConfig.startupSeconds       = 2;
#endif
#ifndef _NO_SAMPLE_RATE_CONVERTER_
    useSampleRateConverter       = false;
#endif
#ifdef _DYNAMIC_RECONFIGURATION_
    update                       = false;
    swmode                       = false;
    fault                        = false;
    reInit                       = true;
#endif
  }

  /// compare operator 
  bool operator ==(const PTP_ConfigClk& rhs) const {
    int result = 0;
    if (::memcmp(Id,rhs.Id,sizeof(Id)) != 0) {
      result += 1;
    }
    result += (piInbound                    == rhs.piInbound) ? 0 : 1;
    result += (piOutbound                   == rhs.piOutbound) ? 0 : 1;
    result += (iirM2S                       == rhs.iirM2S) ? 0 : 1;
    result += (iirPath                      == rhs.iirPath) ? 0 : 1;
    result += (syncBoundary.scaledNs        == rhs.syncBoundary.scaledNs) ? 0 : 1;
    result += (changeEpochBoundary.scaledNs == rhs.changeEpochBoundary.scaledNs) ? 0 : 1;
    result += (fastLockingBoundary.scaledNs == rhs.fastLockingBoundary.scaledNs) ? 0 : 1;
    result += (adjInterval                  == rhs.adjInterval) ? 0 : 1;
    result += (tsRangeWin                   == rhs.tsRangeWin) ? 0 : 1;
    result += (noClockAdjustment            == rhs.noClockAdjustment) ? 0 : 1;
    //the following is used for internal operation, exclude from comparison
    //result += (lastNoClockAdjustment        == rhs.lastNoClockAdjustment) ? 0 : 1;
    result += (noInitialization             == rhs.noInitialization) ? 0 : 1;
    result += (noDriftCalc                  == rhs.noDriftCalc) ? 0 : 1;
    result += (useAnalogAdjusting           == rhs.useAnalogAdjusting) ? 0 : 1;
    result += (rateRatioBoundary            == rhs.rateRatioBoundary) ? 0 : 1;
# ifndef _NO_LUCKY_PACKET_FILTER_
    result += (luckyPacketDepth             == rhs.luckyPacketDepth) ? 0 : 1;
    result += (reInitLuckyPacket            == rhs.reInitLuckyPacket) ? 0 : 1;
    result += (luckyMedian                  == rhs.luckyMedian) ? 0 : 1;
# endif
# ifndef _NO_IIR_FILTER_
    result += (useIirFilter                 == rhs.useIirFilter) ? 0 : 1;
# endif
# if !defined(_NO_SPIKE_FILTER_) && !defined(_NO_CLOCK_STATISTIC_CALC_)
    result += (useSpikeFilter               == rhs.useSpikeFilter) ? 0 : 1;
    result += (spikeFilterConfig            == rhs.spikeFilterConfig) ? 0 : 1;
# endif
# ifndef _NO_SAMPLE_RATE_CONVERTER_
    result += (useSampleRateConverter       == rhs.useSampleRateConverter) ? 0 : 1;
# endif
# ifdef _DYNAMIC_RECONFIGURATION_
    //result += (update == rhs.update) ? 0 : 1; //excluded from comparison
    result += (swmode                       == rhs.swmode) ? 0 : 1;
    //result += (fault == rhs.fault) ? 0 : 1; //excluded from comparison
# endif
    if (result == 0) {
      return true;
    } else {
      return false;
    }
  }

  /// Determine, if critical parameters would be modified
  /// If this is the case (true) the clock will have to be
  //  re-initialized
  bool criticalParameterModified(PTP_ConfigClk * newCfg) {
    int result = 0;
    if (::memcmp(Id,newCfg->Id,sizeof(Id)) != 0) {
      result += 1;
    }    
    result += (adjInterval        == newCfg->adjInterval) ? 0 : 1;
    result += (noDriftCalc        == newCfg->noDriftCalc) ? 0 : 1;
    result += (useAnalogAdjusting == newCfg->useAnalogAdjusting) ? 0 : 1;
# ifdef _DYNAMIC_RECONFIGURATION_
    result += (swmode             == newCfg->swmode) ? 0 : 1;
# endif
    if (result == 0) {
      return false;
    } else {
      return true;
    }
  }

#if defined PTP_CONFIG_DEBUG_DUMP
  void dump(PTP_LogIfc* log) {
    log->Always("PTP_ConfigClk\n");
    log->Always("  Id[8]: %02X%02X%02X%02X%02X%02X%02X%02X\n",
                Id[0], Id[1], Id[2], Id[3], Id[4], Id[5], Id[6],Id[7]);
    log->Always("  syncBoundary: %08X\n", FROM_SCALED_NS(syncBoundary.scaledNs));
    log->Always("  PI values: Outbound K: %d - T: %d\n", piOutbound.K, piOutbound.T);
    log->Always("  PI values: Inbound  K: %d - T: %d\n", piInbound.K, piInbound.T);
  }
#endif
}__PACKME__;
#endif // INC_CONFIG_CLOCK
