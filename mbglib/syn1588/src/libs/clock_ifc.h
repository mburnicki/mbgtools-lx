/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_CLOCK_IFC
#define INC_CLOCK_IFC

#include "basictypes.h"

/// Clock Interface Class

/// This class defines the interface of the PTP clock responsible for
/// managing a hardware/software clock implementation. Generally, a PTP
/// clock should contain a filter implementation to filter path delays and
/// offset values as well as a controller to control the actual hardware clock.
/// All member functions defined herein have to be implemented
/// outside the PTP library, i.e. by building a derived class of this interface.
/// Not all functions have to be implemented (required functions are pure virtual
/// functions).
/// Oregano Systems provides a ready-to-use PTP clock class implementation which
/// may be used instead of writing a new one. If desired, filters and controllers
/// may be replaced with custom implementations. However, it is still necessary to
/// implemented the hardware clock interface for access to the actual harware clock.
/// Please see the PTP_HwClockIfc class for more information.
/// @ingroup interfaces
class PTP_ClockIfc {

public:
    /// @brief Initialization function
    ///
    /// The Initialization function is called from the PTP stack
    /// every time it is in the Initializing state and can be used to set up the
    /// clock.
    virtual int8_t init() { return 0; }

    /// @brief Get Clock Identifier
    ///
    /// This function is called by the stack to obtain its unique
    /// identifier, which is an 8 byte octet (IEEE EUI-64 address). The return
    /// value has to be a uint8_t pointer to memory location where to clock Id
    /// is stored.
    virtual const uint8_t *Id() = 0;

    /// @brief Get current clock time
    ///
    /// Returns the current timestamp in the Timestamp_t structure \a ts.
    /// Please be aware that this is no real time function in most clock implementations.
    /// Due to underlying software/hardware layers, it may take some time until
    /// this function returns. There may be undefined delays and random jitter from the
    /// time when the function is called and the time when the function
    /// returns with the current time. In most implementations, this function call is
    /// simply forwarded to the hardware clock interface (PTP_HwClockIfc).
    /// @param [in,out] ts holds current clock time after call
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getTime(Timestamp_t &ts) = 0;

    /// @brief Set the clock time
    ///
    /// Set the clock time to \a ts. In most implementations, this function call is
    /// simply forwarded to the hardware clock interface (PTP_HwClockIfc).
    /// @param [in] ts time to set
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t setTime(const Timestamp_t &ts) = 0;

    /// @brief Get Receive Timestamp
    ///
    /// Get a receive timestamp for the last received PTP packet. The source port id
    /// \a pId and the sequence identifier \a seqId, as well as the
    /// PTP message type \a msgType may be used to
    /// find the corresponding receive timestamp \a ts for a PTP packet. If available,
    /// store subNs into \a subNs in scaledNs format.
    /// @param [in,out] ts holds receive timestamp after successful call
    /// @param [in,out] subNs holds subnanoseconds (if available) after successful call
    /// @param [in] pId source port ID
    /// @param [in] seqId sequence number
    /// @param [in] msgType PTP message type
    /// @return ENoErr on success, EErr otherwise.
    virtual int8_t getRecvTimestamp(Timestamp_t &ts, TimeInterval_t &subNs,
                                    const PortId_t *pId, const uint16_t seqId,
                                    const uint8_t msgType) = 0;

    /// @brief Get Send Timestamp
    ///
    /// Return the send timestamp \a ts of the most recently transmitted
    /// PTP packet according to the destination port id \a pId, sequence
    /// identifier \a seqId, and messagw type \a msgType of the packet. If available,
    /// store subNs into \a subNs in scaledNs format.
    /// @param [in,out] ts holds receive timestamp after successful call
    /// @param [in,out] subNs holds subnanoseconds (if available) after successful call
    /// @param [in] pId destination port ID
    /// @param [in] seqId sequence number
    /// @param [in] msgType PTP message type
    /// @return ENoErr on success, EErr otherwise.
    virtual int8_t getSendTimestamp(Timestamp_t &ts, TimeInterval_t &subNs,
                                    const PortId_t *pId, const uint16_t seqId,
                                    const uint8_t msgType) = 0;

    /// @brief Update Master-to-Slave delay for synchronization
    ///
    /// The PTP stack calls this function every time it receives a Sync message
    /// (if it is a OneStep clock) or a FollowUp message (in case of a
    /// TwoStep clock) and the clock is in the Slave state.
    /// \a t1 denotes the send timestamp of the sync message whereas \a t2 denotes
    /// the ingress timestamp of the receving node.
    /// The current master-to-slave delay may be calculated with the following
    /// equation: m2s = t2 - t1. Additionally, the timestamps may be used to calculate
    /// the current rate error.\a ival specifies the current update interval (sync interval).
    /// @param [in] t1 egress timestamp
    /// @param [in] t2 ingress timestamp
    /// @param [in] ival current sync interval
    /// @param [in] port specify port in a multiport setup
    virtual void updateM2SDelay(const Timestamp_t &t1,
                                const Timestamp_t &t2,
                                int8_t ival,
                                uint16_t port = 1) {
      (void)t1;
      (void)t2;
      (void)ival;
      (void)port;
    }

    /// @brief Synchronization
    ///
    /// The PTP stack calls this function every adjust interval if
    /// it is in Slave or Uncalibrated state. \a dlyMechanism signals
    /// if peerDelay, E2E Delay or no delay mechanism is used for
    /// synchroniziation.
    /// @param [in] dlyMechanism specify delay meachnism to be used
    /// @param [in] port specify port in a multiport setup
    /// @param [in] adjInterval the current adjust interval
    virtual void synchronize(uint8_t dlyMechanism, uint16_t port = 1, int8_t adjInterval = 0) = 0;

    /// @brief Update Slave-to-Master delay for synchronization
    ///
    /// The PTP stack calls this function every time it gets a valid
    /// Delay Response message from a previously sent Delay Request.
    /// It is not neccessary to re-implement this function if absolute
    /// time offsets are out of scope (syntonization only). The current
    /// slave to master delay may be calculated using the following
    /// equation: s2m = t4 - t3. <br>
    /// \a ival specifies the current update interval.
    /// @param [in] t3 egress timestamp of dlyReq message
    /// @param [in] t4 ingress timestamp of dlyReq message
    /// @param [in] ival current update interval
    /// @param [in] port specify port in a multiport setup
    virtual void updateS2MDelay(const Timestamp_t &t3,
                                const Timestamp_t &t4,
                                int8_t ival,
                                uint16_t port = 1) = 0;

    /// @brief Update Path Delay
    ///
    /// This function is used for P2P one-step mode.
    /// The PTP stack calls this function every time it gets a valid
    /// one step PDelay Response message from a previous sent PDelay Request.
    /// It is not neccessary to re-implement this function if absolute
    /// time offsets are out of scope (syntonization only). \a ival specifies
    /// the current update interval. \a pDelay is a reference to the current
    /// path delay.
    /// @param [in] pDelay reference to current path delay
    /// @param [in] ival current update interval
    /// @param [in] port specify port in a multiport setup
    virtual void updatePathDelay(const TimeInterval_t &pDelay,
                                 int8_t ival,
                                 uint16_t port = 1) {
      (void)pDelay;
      (void)ival;
      (void)port;
    }

    /// @brief Update Path Delay
    ///
    /// This function is used for P2P two-step mode.
    /// The PTP stack calls this function every time it gets a valid
    /// PDelay Response and PDelay Response Follow-Up message for a
    /// previously sent PDelay Request message. \a t1, \a t2, \a t3
    /// and \a t4 contain the respective timestamps to calculate the
    /// the current path delay: <br>
    /// pDelay = ((t4 - t1) - (t3 -t2)) / 2 <br>
    /// \a ival specifies the current update interval.
    /// @param [in] t1
    /// @param [in] t2
    /// @param [in] t3
    /// @param [in] t4
    /// @param [in] ival current update interval
    /// @param [in] port specify port in a multiport setup
    virtual void updatePathDelay(const Timestamp_t &t1,
                                 const Timestamp_t &t2,
                                 const Timestamp_t &t3,
                                 const Timestamp_t &t4,
                                 int8_t ival,
                                 uint16_t port = 1) {
      (void)t1;
      (void)t2;
      (void)t3;
      (void)t4;
      (void)ival;
      (void)port;
    }

    /// @brief Get Neighbor Rate Ratio
    ///
    /// This function retrieves the rate ratio between \a port and the attached
    /// link partner. It may be calculated by using subsequent pDelay messages.
    /// Currently, this function is only required for the 802.1as profile
    /// (equivalent to neighborRateRatio in 10.2.4.6 of 802.1as).
    /// @param [in,out] rateRatio
    /// @param [in] port specify port in a multi-port setup
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getNeighborRateRatio(int64_t &rateRatio, uint16_t port = 1) {
      (void)rateRatio;
      (void)port;
      return EErr;
    }

    /// @brief Get Master Rate Ratio
    ///
    /// This function retrives the rate ratio between \a port and the Master.
    /// Currently, this function is only required for the 802.1as profile
    /// @param [in,out] rateRatio
    /// @param [in] port specify port in a multi-port setup
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getMasterRateRatio(int64_t &rateRatio, uint16_t port = 1) {
      (void)rateRatio;
      (void)port;
      return EErr;
    }

    /// @brief Reset Path Delay
    ///
    /// The PTP stack calls this function whenever a StateChange event occurs
    /// and the current master changes. It can be used to reset filters working
    /// on the path delay.
    /// @param [in] port specify port in a multi-port setup
    virtual void resetPathDelay(uint16_t port = 1) {
      (void)port;
    }

    /// @brief Check if clock is calibrated
    ///
    /// This function is called by the PTP Stack to check if the clock is already
    /// calibrated. Only if a clock is calibrated, the PTP Stack will change from
    /// Uncalibrated state to Slave state. If true is returned, the state change
    /// will be performed.  Otherwise, nothing is done. \a dlyMechanism defines
    /// if peerDelay, E2E Delay or no delay mechanism is used for
    /// synchroniziation.
    /// @param [in] dlyMechanism dely mechanism used for synchronization
    /// @param [in] port specify port in a multi-port setup
    /// @return true if clock is calibrated, false otherwise
    virtual bool isCalibrated(uint8_t dlyMechanism, uint16_t port = 1) {
      (void)dlyMechanism;
      (void)port;
      return true;
    };

    /// @brief Get current Offset
    ///
    /// Get the current offset. This value may be some kind of an average or
    /// filtered value of the offset between master and slave.
    /// @param [in,out] offset
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getOffset(TimeInterval_t &offset) = 0;

    /// @brief Get PathDelay
    ///
    /// Get the mean path delay for port \a port. This value may be some
    /// kind of an average or filtered value of the path delay between master
    /// and slave.
    /// @param [in,out] pathDelay
    /// @param [in] port specify port in a multiport setup
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getPathDelay(TimeInterval_t &pathDelay, uint16_t port = 1) = 0;

    /// @brief Management Interface
    ///
    /// Enumeration for different management requests. They are used by
    /// the PTP Stack to get information from the clock or indicate
    /// certain events to the clock. Not all manage types must be implemented.
    /// If it is not implemented, unknown, or triggers an error, EErr shall be
    /// returned, else ENoErr.
    typedef enum {
      /// manageHWInfo
      ///
      /// Requests a string containing information
      /// about the hardware. \a buf contains a char pointer to the memory. Data
      /// must not be longer than 64 bytes and NULL terminated.
      manageHWInfo,
      /// manageLeap59
      ///
      /// Indicates an imminent leap seconds event. The day will be one second
      /// shorter and thus, the utc offset decreased by one. \buf holds a pointer
      /// to the time of the jump in the format of \a Timestamp_t.
      manageLeap59,
      /// manageLeap61
      ///
      /// Indicates an imminent leap seconds event. The day will be one second
      /// longer and thus, the utc offset increased by one. \buf holds a pointer
      /// to the time of the jump in the format of \a Timestamp_t.
      manageLeap61,
      /// manageLeapClockAdjust
      ///
      /// Enable and/or disable clock adjustment during leap second event.
      manageLeapClockAdjust,
      /// manageClockInitServos
      ///
      /// Reset PI controller servo settings.
      manageClockInitServos,
      /// manageMasterChange
      ///
      /// Indicates a master change. This can happen in the Slave state when
      /// BMCA detects a better Master. This manage type indicates such an event.
      /// Due to a different Master, the PathDelay may change dramatically because
      /// of a different physical position of the new master. The clock may deal
      /// with this situation in an adequat way.The PTP Stack will change from Slave
      /// to Uncalibrated and back to Slave in such an event.
      manageMasterChange,
      /// manageMasterLost
      ///
      /// Indicates that the connection to the master was lost. Is called
      /// everytime the PTP Stack leaves the Slave state.
      manageMasterLost,
      /// manageMasterSelected
      ///
      /// This mange type signals that a master clock has been selected. The
      /// PTP Stack changes from Uncalibrated State to Slave state.
      manageMasterSelected,
      /// manageReset
      ///
      /// Indicates a remote reset request. This manage type is called
      /// on every receipt of a INITIALIZE managment message with
      /// argument 1.
      manageReset,
      /// manageSetDomain
      ///
      /// Indicates the current domain. \a buf contains
      /// uint16_t with the domain number. This will be called on
      /// every domain change during runtime.
      manageSetDomain,
      /// manageStateChangeToMaster
      ///
      /// Indicates a change to the master state from any other state. The clock
      /// may go to some stable holdover mode or external synchronization mode.
      manageStateChangeToMaster,
      /// manageStateChangeToSlave
      ///
      /// Indicates a change to the slave state from any other state. Actually,
      /// a change will always be from Uncalibrated state (according to the
      /// state machine in Fig.23 in IEEE1588-2008).
      manageStateChangeToSlave,
      /// manageStateChangeToUncalibrated
      ///
      /// Indicates a change to the uncalibrated state from any other state. Acutally,
      /// a chang will always be from Uncalibrated.
      manageStateChangeToUncalibrated,
      /// manageStateChangeFromMaster
      ///
      /// Indicates a change from the master state to any other state. The clock
      /// may perform implementation specific actions.
      manageStateChangeFromMaster,
      /// manageStateChangeFromSlave
      ///
      /// Indicates a change from the slave state to any other state. The clock
      /// may perform implementation specific actions.
      manageStateChangeFromSlave,
      /// manageSyncLost
      ///
      /// Required! Mangement of this signal is strongly recommended for proper
      /// operation.
      /// Indicates that the synchronization source is lost. A clock implementation
      /// may enter a holdover state and trigger appropriate actions in the hardware
      /// clock. Additionally, filters and parameters may be reset to restart
      /// synchronization as soon as a manageStartSync signal is received.
      manageSyncLost,
      /// manageSyncStart
      ///
      /// Required! Mangement of this signal is strongly recommended for proper
      /// operation.
      /// Indicates that the synchronization process should be started/restarted.
      /// This signal may be used to trigger servo initialization and other
      /// implementation specific procedures.
      manageSyncStart,
      /// manageUTCOffset
      ///
      /// Writes the current UTC offset. \a buf contains
      /// int16_t with the current UTC offset. This parameter may be used by a
      /// hardware implementation for application specific purposes.
      manageUTCOffs,
      /// manageVersionInfo
      ///
      /// Requests a string containing information
      /// about the version. Must not be longer
      /// than 64 bytes and NULL terminated.
      manageVersionInfo,
      //  syn1588 specific manage types
      manageConfigureTimestamper, ///< syn1588 specific
      manageGetReg,               ///< syn1588 specific
      manageSetReg,               ///< syn1588 specific
      manageUpdatePTPSyncStatusReg///< syn1588 specific
    } ManageType_t;

    /// @brief Manage the clock
    ///
    /// This function is used by the PTP Stack to indicate/request/manage certain
    /// actions/data to the clock. It provides a pointer \a buf to a data buffer of
    /// the size \a len. If \a len is 0, \a buf does not contain any data.
    ///  \a type indicates the mange event.
    /// @param [in] type
    /// @param [in,out] buf
    /// @param [in] len
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t manage(ManageType_t type, void *buf, size_t len) {
      (void)type;
      (void)buf;
      (void)len;
      return EErr;
    }

    /// Enumeration for the states of the Leap Seconds FSM
    typedef enum {
      ELeapIdle = 0,    ///< Idle state @hideinitializer
      ELeapPremonition, ///< Premonition state
      ELeapImminent,    ///< Leap Imminent state
      ELeapProcessed    ///< Leap processed state
    } ClockIfcLeapSecondFsmState_t;

    /// @brief Get leap second processing state of clock
    /// @return the current state of the Leap Seconds FSM
    virtual ClockIfcLeapSecondFsmState_t getLeapSecondFsmState() {
      return ELeapIdle;
    }

#ifdef _DYNAMIC_RECONFIGURATION_
/// @brief Clock Heartbeat
///
/// used for dynamic reconfiguration
    virtual int heartbeat() {
      return ENoErr;
    }
#endif

    /// Destructor
    virtual ~PTP_ClockIfc() {}
};

#endif
