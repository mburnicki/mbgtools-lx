/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#include "basictypes.h"
#if defined(WIN32)
#include <WinSock2.h>
#elif defined __NIOS__
#include <sys/time.h>
#endif

const uint32_t NSEC_PER_SEC = 1000000000;
const uint32_t USEC_PER_SEC = 1000000;
const uint32_t MSEC_PER_SEC = 1000;

int64_t CBoundariesInScaledNS[] = {
  TO_SCALED_NS(S_IN_NS(10 + 1)), // 10 s + 1 (over 10 s)
  TO_SCALED_NS(S_IN_NS(10)),     // 10 s
  TO_SCALED_NS(S_IN_NS(1)),      // 1 s
  TO_SCALED_NS(MS_IN_NS(250)),   // 250 ms
  TO_SCALED_NS(MS_IN_NS(100)),   // 100 ms
  TO_SCALED_NS(MS_IN_NS(25)),    // 25 ms
  TO_SCALED_NS(MS_IN_NS(10)),    // 10 ms
  TO_SCALED_NS(US_IN_NS(2500)),  // 2.5 ms
  TO_SCALED_NS(US_IN_NS(1000)),  // 1 ms
  TO_SCALED_NS(US_IN_NS(250)),   // 250 us
  TO_SCALED_NS(US_IN_NS(100)),   // 100 us
  TO_SCALED_NS(US_IN_NS(25)),    // 25 us
  TO_SCALED_NS(US_IN_NS(10)),    // 10 us
  TO_SCALED_NS(2500l),           // 2.5 us
  TO_SCALED_NS(1000l),           // 1 us
  TO_SCALED_NS(250l),            // 250 ns
  TO_SCALED_NS(100l),            // 100 ns
  TO_SCALED_NS(25l),             // 25 ns
  TO_SCALED_NS(10l),             // 10 ns
  0                              // EMaxBoundary has to be 0
};

struct timeval operator+(struct timeval t1, struct timeval t2) {
  struct timeval res;
  res.tv_usec = t1.tv_usec + t2.tv_usec;
  if (res.tv_usec >= 1000000) {
    res.tv_usec -= 1000000;
    t2.tv_sec++;
  }
  res.tv_sec = t1.tv_sec + t2.tv_sec;
  return res;
}

struct timeval operator-(struct timeval t1, struct timeval t2) {
  struct timeval res;
  res.tv_usec = t1.tv_usec - t2.tv_usec;
  if (res.tv_usec < 0) {
    res.tv_usec += 1000000;
    t2.tv_sec++;
  }
  res.tv_sec = t1.tv_sec - t2.tv_sec;
  if (res.tv_sec < 0) {
    res.tv_sec = 0;
    res.tv_usec = 0;
  }
  return res;
}
