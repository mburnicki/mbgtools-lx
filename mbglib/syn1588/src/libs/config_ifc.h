/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/// @file
/// This file contains the definitions for class PTP_ConfigIfc

#if !defined INC_CONFIG_IFC
#define INC_CONFIG_IFC

#include "basictypes.h"
#include "datasets.h"

#if !defined PTP_MAX_NUM_PORTS
/// max number of PTP ports currently supported
#  define PTP_MAX_NUM_PORTS 64
#endif // !defined PTP_MAX_NUM_PORTS

#if !defined PTP_UC_NUM_REQ_CONN
/// max number of configureable unicast grantors (size of grantor table)
#  define PTP_UC_NUM_REQ_CONN 4
#endif // !defined PTP_UC_NUM_REQ_CONN
#if !defined PTP_TWOSTEP
#  define PTP_TWOSTEP true
#endif // !defined PTP_TWOSTEP
#if !defined PTP_SYNCINTERVAL
#  define PTP_SYNCINTERVAL 0
#endif // !defined PTP_SYNCINTERVAL
#if !defined PTP_DLYREQINTERVALOFFS
#  define PTP_DLYREQINTERVALOFFS 0
#endif // !defined PTP_DLYREQINTERVALOFFS
#if !defined PTP_ANNOUNCEINTERVAL
#  define PTP_ANNOUNCEINTERVAL 1
#endif // !defined PTP_ANNOUNCEINTERVAL
#if !defined PTP_ANNOUNCERECEPTTOUT
#  define PTP_ANNOUNCERECEPTTOUT 3
#endif // !defined PTP_ANNOUNCERECEPTTOUT
#if !defined PTP_ADJINTERVAL
#  define PTP_ADJINTERVAL 0
#endif // !defined PTP_ADJINTERVAL
#if defined PTP_SLAVEONLY
#  undef PTP_CLKCLASS
#  define PTP_CLKCLASS PTP_ConfigIfc::clkClassSlave
#endif // defined PTP_SLAVEONLY
#if !defined PTP_CLKCLASS
#  define PTP_CLKCLASS PTP_ConfigIfc::clkClassDefault
#endif // !defined PTP_CLKCLASS
#if !defined PTP_DOMAIN
#  define PTP_DOMAIN 0
#endif // !defined PTP_DOMAIN
#if !defined PTP_NWPROTO
#  define PTP_NWPROTO 1
#endif // !defined PTP_NWPROTO
#if !defined PTP_PRIORITY1
#  define PTP_PRIORITY1 128
#endif // !defined PTP_PRIORITY1
#if !defined PTP_PRIORITY2
#  define PTP_PRIORITY2 128
#endif // !defined PTP_PRIORITY2
#if !defined PTP_UC_DURATION
// Do not use a duration of 0 here!
#  define PTP_UC_DURATION 60
#endif // !defined PTP_UC_DURATION
// Maximum number of connections (connection array size)
// Can be artifically limited with configurable parameter
#if !defined PTP_UC_NUM_CONN
#  define PTP_UC_NUM_CONN  16
#endif // !defined PTP_UC_NUM_CONN
// Maximum number of messages per second
#if !defined PTP_UC_NUM_MSG
// 32 sync, 32 dly resp, 8 ann, * 256 conns
#  define PTP_UC_NUM_MSG 18432
#endif // !defined PTP_UC_NUM_MSG

#define C37_238_GMID 3
#define C37_238_2011_GMINAC 0
#define C37_238_2011_NWINAC 0
#define C37_238_2017_TOTINAC 0

#define PTP_G8275_1_LOCAL_PRIO 128

#if !defined PTP_802_1AS_PDELAY_THRESHOLD
#  define PTP_802_1AS_PDELAY_THRESHOLD TO_SCALED_NS(800)
#endif // !defined PTP_802_1AS_PDELAY_THRESHOLD

#if defined NO_MULTICAST && defined NO_UNICAST
#  error NO_MULTICAST and NO_UNICAST defined at the same time!
#endif // defined NO_MULTICAST && defined NO_UNICAST

#if defined _C37_238_2011_PROFILE_ && !defined ALT_TIME_OFFSET
#  error Power Profile requires Alt time offset
#endif // defined _C37_238_2011_PROFILE_ && !defined ALT_TIME_OFFSET

#if defined(_DYNAMIC_RECONFIGURATION_) && !defined(_SHARED_MEM_)
#  error dynamic port configuration is only possible when using the shared memory
#endif // defined(_DYNAMIC_RECONFIGURATION_) && !defined(_SHARED_MEM_)

#if defined NO_UNICAST && defined _UNICAST_INFO_DATA_
#  error using _UNICAST_INFO_DATA_ while not using unicast
#endif // defined NO_UNICAST && defined _UNICAST_INFO_DATA_

#if !defined USERDESCRIPTION
#  define USERDESCRIPTION "IEEE1588_clock"
#endif // !defined USERDESCRIPTION

/// @{
/// @name PTP profile identifiers
const uint8_t CPtpProfileIdDefault[]     = { 0x00, 0x1B, 0x19, 0x00, 0x01, 0x00 };
const uint8_t CPtpProfileIdDefaultV21[]  = { 0x00, 0x1B, 0x19, 0x01, 0x01, 0x00 };
const uint8_t CPtpProfileIdDefaultP[]    = { 0x00, 0x1B, 0x19, 0x00, 0x02, 0x00 };
const uint8_t CPtpProfileIdDefaultPv21[] = { 0x00, 0x1B, 0x19, 0x02, 0x01, 0x00 };
#if defined _802_1AS_PROFILE_
const uint8_t CPtpProfileId8021AS[]   = { 0x00, 0x80, 0xC2, 0x00, 0x01, 0x00 };
#endif // defined _802_1AS_PROFILE_
#if defined _TELECOM_PROFILE_
const uint8_t CPtpProfileIdTelecom[]  = { 0x00, 0x19, 0xA7, 0x00, 0x01, 0x00 };
#endif // defined _TELECOM_PROFILE_
#if defined _G8275_1_PROFILE_
const uint8_t CPtpProfileIdG8275_1[]  = { 0x00, 0x19, 0xA7, 0x01, 0x02, 0x01 };
#endif // defined _G8275_1_PROFILE_
#if defined _G8275_2_PROFILE_
const uint8_t CPtpProfileIdG8275_2[]  = { 0x00, 0x19, 0xA7, 0x02, 0x01, 0x00 };
#endif // defined _G8275_2_PROFILE_
#if defined _SMPTE_PROFILE_
const uint8_t CPtpProfileIdSmpte[]    = { 0x68, 0x97, 0xE8, 0x00, 0x01, 0x00 };
#endif // defined _SMPTE_PROFILE_
#if defined _C37_238_2011_PROFILE_ || defined _C37_238_2017_PROFILE_
const uint8_t CPtpProfileIdPower[]    = { 0x1C, 0x12, 0x9D, 0x00, 0x00, 0x00 };
#endif // defined _C37_238_2011_PROFILE_ || defined _C37_238_2017_PROFILE_

const uint8_t CPtpOUI[]               = {0xFF, 0xFF, 0xFF};
/// @}

/// @brief PTP Configuration interface structure
///
/// This structure holds all PTP parameters used by the PTP engine.
/// On initialization, data from this structure is used to initialize
/// PTP datasets and to define certain modes of operation like the used delay
/// mechanism or the used network protocol.
/// It is recommended that the structure is filled with the desired parameters
/// before PTP Engine is constructed.
/// Additionally, this structure contains constants for defined values of
/// all parameters.
/// A class may be derived from PTP_ConfigIfc to implement initialization of
/// the parameter structure,
/// e.g. a class parsing commandline parameters and setting configuration
/// parameters accordingly.
/// @ingroup interfaces
struct PTP_ConfigIfc {

  /// Constant for End-to-End Delay Measurement
  static const uint8_t    CE2E                  = 0x01;

  /// Constant for Peer-to-Peer Delay Measurement
  static const uint8_t    CP2P                  = 0x02;

  /// Constant for Delay Measurement used by eSync && lSync
  static const uint8_t    CSync                 = 0x03;

  static const uint8_t    CNoDly                = 0xFE; ///< Constant for No Delay Measurement
  static const uint8_t    CLeap61               = 0x01; ///< Leap 61 Mask for flags
  static const uint8_t    CLeap59               = 0x02; ///< Leap 59 Mask for flags

  /// Current UTC offset valid Mask for flags
  static const uint8_t    CCurrUtcOffsValid     = 0x04;

  static const uint8_t    CPtpTimescale         = 0x08; ///< PTP Timescale Mask for flags
  static const uint8_t    CTimeTraceable        = 0x10; ///< Time traceable Mask for flags
  static const uint8_t    CFreqTraceable        = 0x20; ///< Frequency traceable Mask for flags
  static const int16_t    CUtcOffset            = 37;   ///< = TAI-UTC, value is from 31.12.2016
  static const uint8_t    CNwModeUC             = 0x1;  ///< Constant for Unicast operation
  static const uint8_t    CNwModeMC             = 0x2;  ///< Constant for Multicast operation
  static const uint8_t    CNwModeMCUC           = 0x3;  ///< Constant for using MC and UC simultaniously
  static const uint8_t    CNwModeH              = 0x4;  ///< Constant for using Hybrid mode (MC down, UC up)

  static const uint16_t   CUCMinDuration        = 10;   ///< min duration of UC grants
  static const uint16_t   CUCDefaultDuration    = 300;  ///< default duration of UC grants
  static const uint16_t   CUCMaxDuration        = 1000; ///< max duration of UC grants

  /// max number messages for UC grants that a master can handle.
  /// (32 sync, 32 dly resp, 8 ann) * 256 conns
  static const uint32_t   CUCMaxMsg             = 18432;

  /// default PTP domain for default profile
  static const uint8_t    CPtpDomainDefault     = 0;
  /// start of PTP domain for default profile (inclusive)
  static const uint8_t    CPtpDomainMin         = 0;
  /// end of PTP domain for default profile (inclusive)
  static const uint8_t    CPtpDomainMax         = 255;

  /// Minimum allowed value for syncInterval
  static const int8_t     minSyncInterval       = -8;
  /// Maximum allowed value for syncInterval
  static const int8_t     maxSyncInterval       = 8;

  /// Minimum allowed value for dlyReqIntervalOffs
  static const int8_t     minDlyReqIntervalOffs = 0;

  /// Maximum allowed value for dlyReqIntervalOffs
  static const int8_t     maxDlyReqIntervalOffs = 5;

  /// Minimum allowed value for pdlyReqInterval
  static const int8_t     minPdlyReqInterval    = -8;

  /// Maximum allowed value for pdlyReqInterval
  static const int8_t     maxPdlyReqInterval    = 8;

  /// Minimum allowed value for announceInterval
  static const int8_t     minAnnounceInterval   = -7;

  /// Maximum allowed value for announceInterval
  static const int8_t     maxAnnounceInterval   = 4;

  /// Minimum allowed value for nnounceRecptTout
  static const uint8_t    minAnnounceRecptTout  = 2;

  /// Maximum allowed value for announceRecptTout
  static const uint8_t    maxAnnounceRecptTout  = 10;

  /// Minimum allowed value for adjust interval
  static const int8_t     CMinAdjInterval       = -8;

  /// Maximum allowed value for adjust interval
  static const int8_t     CMaxAdjInterval       = 8;

  /// Minimum period allowed for displaying stats
  static const uint8_t    minStatPeriod         = 8;

  /// Layer 2 (pure Ethernet) protocol (Annex F of IEEE1588)
  static const uint16_t   CNwProtoL2            = 3;

  static const uint16_t   CNwProtoL3IPv4        = 1;  ///< UDP/IPv4 Protocol (Annex D of IEEE1588)
#ifndef NO_IPV6
  static const uint16_t   CNwProtoL3IPv6        = 2;  ///< UDP/IPv6 Protocol (Annex E of IEEE1588)
#endif // NO_IPV6

  /// @{
  /// @name Clock class specification according to IEEE1588-2008 table 5

  static const uint8_t    clkClassM_Ext         = 6;  ///< Master on external reference

  /// Master on external reference (in holdover)
  static const uint8_t    clkClassM_ExtHold     = 7;

  /// Master on application specific reference
  static const uint8_t    clkClassM_Appl        = 13;

  /// Master on application specific reference (in holdover)
  static const uint8_t    clkClassM_ApplHold    = 14;

  /// Master on external reference (degredation alternative A)
  static const uint8_t    clkClassM_ExtDegA     = 52;

  /// Master on application specific reference (degredation alternative A)
  static const uint8_t    clkClassM_ApplDegA    = 58;

  /// Master on external reference (degredation alternative B, may be slave)
  static const uint8_t    clkClassM_ExtDegB     = 187;

  /// Master on application specific reference(degredation alternative B, may be slave)
  static const uint8_t    clkClassM_ApplDegB    = 193;

  static const uint8_t    clkClassDefault       = 248; ///< Default clock class, master and slave
  static const uint8_t    clkClassSlave         = 255; ///< Slave only
  /// @}

  /// @{
  /// @name Clock accuracies according to IEEE1588-2008 table 6
  static const uint8_t    clkAccuracy25ns       = 0x20; ///< time accuracy is within 25 ns
  static const uint8_t    clkAccuracy100ns      = 0x21; ///< time accuracy is within 100 ns
  static const uint8_t    clkAccuracy250ns      = 0x22; ///< time accuracy is within 250 ns
  static const uint8_t    clkAccuracy1us        = 0x23; ///< time accuracy is within 1 us
  static const uint8_t    clkAccuracy2_5us      = 0x24; ///< time accuracy is within 2.5 us
  static const uint8_t    clkAccuracy10us       = 0x25; ///< time accuracy is within 10 us
  static const uint8_t    clkAccuracy25us       = 0x26; ///< time accuracy is within 25 us
  static const uint8_t    clkAccuracy100us      = 0x27; ///< time accuracy is within 100 us
  static const uint8_t    clkAccuracy250us      = 0x28; ///< time accuracy is within 250 us
  static const uint8_t    clkAccuracy1ms        = 0x29; ///< time accuracy is within 1 ms
  static const uint8_t    clkAccuracy2_5ms      = 0x2A; ///< time accuracy is within 2.5 ms
  static const uint8_t    clkAccuracy10ms       = 0x2B; ///< time accuracy is within 10 ms
  static const uint8_t    clkAccuracy25ms       = 0x2C; ///< time accuracy is within 25 ms
  static const uint8_t    clkAccuracy100ms      = 0x2D; ///< time accuracy is within 100 ms
  static const uint8_t    clkAccuracy250ms      = 0x2E; ///< time accuracy is within 250 ms
  static const uint8_t    clkAccuracy1s         = 0x2f; ///< time accuracy is within 1 s
  static const uint8_t    clkAccuracy10s        = 0x30; ///< time accuracy is within 10 s
  static const uint8_t    clkAccuracyPoor       = 0x31; ///< time accuracy is more than 10 s
  static const uint8_t    clkAccuracyUnknown    = 0xFE; ///< time accuracy unknown
  /// @}

  /// @{
  /// @name Time source according to IEEE1588-2008 table 7
  static const uint8_t    timeSrcAtomic         = 0x10; ///< Atomic Clock time source
  static const uint8_t    timeSrcGPS            = 0x20; ///< GPS time source
  static const uint8_t    timeSrcTerRadio       = 0x30; ///< Terrestrial Radio time source
  static const uint8_t    timeSrcPTP            = 0x40; ///< PTP time source
  static const uint8_t    timeSrcNTP            = 0x50; ///< NTP time source
  static const uint8_t    timeSrcHandSet        = 0x60; ///< hand set time source
  static const uint8_t    timeSrcOther          = 0x90; ///< other time source
  static const uint8_t    timeSrcIntXO          = 0xA0; ///< internal oscillator
  /// @}

  /// @{
  /// @name Supported PTP profiles

  /// No specific PTP profile
  static const uint8_t CPtpProfileNone          = 0x00;
  /// PTP default profile, IEEE1588-2008 Annex J
  static const uint8_t CPtpProfileDefault       = 0x01;
#if defined _C37_238_2011_PROFILE_
  /// PTP power profile, C37.238
  static const uint8_t CPtpProfileC37_238_2011  = 0x02;
#endif // defined _C37_238_2011_PROFILE_
#if defined _TELECOM_PROFILE_
  /// PTP telecom profile, G.8265.1/Y.1365.1
  static const uint8_t CPtpProfileTelecom       = 0x03;
#endif // defined _TELECOM_PROFILE_
#if defined _SMPTE_PROFILE_
  /// PTP SMPTE profile, ST 2059-2:201X
  static const uint8_t CPtpProfileSmpte         = 0x04;
#endif // defined _SMPTE_PROFILE_
#if defined _G8275_1_PROFILE_
  /// PTP telecom 2 profile, G.8275.1/Y.1367.1
  static const uint8_t CPtpProfileG8275_1       = 0x05;
#endif // defined _G8275_1_PROFILE_
#if defined _802_1AS_PROFILE_
  /// PTP profile described in IEEE Std 802.1AS-2011
  static const uint8_t CPtpProfile802_1as       = 0x07;
#endif // defined _802_1AS_PROFILE_
#if defined _G8275_2_PROFILE_
  /// PTP telecom profile, G.8275.2
  static const uint8_t CPtpProfileG8275_2       = 0x08;
#endif // defined _G8275_2_PROFILE_
#if defined _C37_238_2017_PROFILE_
  /// PTP power profile, C37.238-2017
  static const uint8_t CPtpProfileC37_238_2017  = 0x09;
#endif // defined _C37_238_2017_PROFILE_
  /// @}

  /// @{
  /// @name PTP telecom profile specifics
#if defined(_G8275_1_PROFILE_) || defined(_G8275_2_PROFILE_)
  //clockClass: see Table 2 of G8275.1 v2.1 or Table 2 of G8275.2 v1.1
  /// T-GM connected to a PRTC in locked mode
  static const uint8_t CClkClassG8275_1_2_TGMlockedPRTC              = 6;
  /// T-GM in holdover, within holdover specification
  static const uint8_t CClkClassG8275_1_2_TGMholdOver                = 7;
  /// T-BC in holdover, within holdover specification
  static const uint8_t CClkClassG8275_1_2_TBCholdOver                = 135;
  /// T-GM in holdover, out of holdover specification,
  /// traceable to Category 1 frequency source
  static const uint8_t CClkClassG8275_1_2_TGMholdOverOutSpecFreqCat1 = 140;
  /// T-GM in holdover, out of holdover specification,
  /// traceable to Category 2 frequency source
  static const uint8_t CClkClassG8275_1_2_TGMholdOverOutSpecFreqCat2 = 150;
  /// T-GM in holdover, out of holdover specification,
  /// traceable to Category 3 frequency source
  static const uint8_t CClkClassG8275_1_2_TGMholdOverOutSpecFreqCat3 = 160;
  /// T-BC in holdover, out of holdover specification
  static const uint8_t CClkClassG8275_1_2_TBCholdOverOutSpec         = 165;
  /// T-GM or T-BC without time reference since start-up
  static const uint8_t CClkClassG8275_1_2_TGMTBCnoRef                = 248;

  //clockAccuracy:
  // see chapter 6.3.5 of G8275.1 v2.1 or 6.7.6 of G8275.2 v1.1
  /// T-GM connected to locked ePRTC
  static const uint8_t CClkAccuracyG8275_1_2_TGMePRTC = 0x20;
  /// T-GM connected to locked PRTC
  static const uint8_t CClkAccuracyG8275_1_2_TGMPRTC  = 0x21;
  /// all other clocks
  static const uint8_t CClkAccuracyG8275_1_2_Other    = 0xFE;

  //offsetScaledLogVariance:
  // see chapter 6.3.5 of G8275.1 v2.1 or 6.7.7 of G8275.2 v1.1
  /// for an ePRTC, corresponding TDEV = 10 ns
  static const uint16_t COffsetScaledLogVarianceG8275_1_2_ePRTC = 0x4B32;
  /// for an PRTC, corresponding TDEV = 30 ns
  static const uint16_t COffsetScaledLogVarianceG8275_1_2_PRTC  = 0x4E5D;
  /// for all other cases
  static const uint16_t COffsetScaledLogVarianceG8275_1_2_Other = 0xFFFF;

  //allowed PTP domain range
  /// default PTP domain for G8275.1
  static const uint8_t CPtpDomainG8275_1default = 24;
  /// start of PTP domain for G8275.1 (inclusive)
  static const uint8_t CPtpDomainG8275_1start   = 24;
  /// end of PTP domain for G8275.1 (inclusive)
  static const uint8_t CPtpDomainG8275_1end     = 43;

  /// default PTP domain for G8275.2
  static const uint8_t CPtpDomainG8275_2default = 44;
  /// start of PTP domain for G8275.1 (inclusive)
  static const uint8_t CPtpDomainG8275_2start   = 44;
  /// end of PTP domain for G8275.1 (inclusive)
  static const uint8_t CPtpDomainG8275_2end     = 63;

  //message interval ranges
  ///min announce interval for G8275.2
  static const  int8_t CMinAnnounceInterval_G8275_2 = -3;
  ///max announce interval for G8275.2
  static const uint8_t CMaxAnnounceInterval_G8275_2 = 0;
  ///min sync interval for G8275.2
  static const  int8_t CMinSyncInterval_G8275_2     = -7;
  ///max sync interval for G8275.2
  static const uint8_t CMaxSyncInterval_G8275_2     = 0;

  //default values
  ///default (static) priority1
  static const uint8_t  CPrio1G8275_1_2             = 128;
  /// min duration of UC grants
  static const uint16_t CUCMinDurationG8275_2       = 60;
#endif // defined _G8275_1_PROFILE_ || defined _G8275_2_PROFILE_
  /// @}

  /// @{
  /// @anchor param
  /// @name Parameter types supported by Oregano specific "PARAM" management message
  enum ParamType_e {
    EParamKey           = 0x0010,
    EParamLogLevel      = 0x0011,

#if defined SYNC_OUTPUT
    EParamSyncOut       = 0x0012,
#endif // defined SYNC_OUTPUT

#if defined SYNC_INPUT
    EParamSyncIn        = 0x0013,
    EParamSyncInOff     = 0x0014,
#endif // defined SYNC_INPUT

#if defined CFG_SERIAL_BAUD
    EParamBaud          = 0x0015,
#endif // defined CFG_SERIAL_BAUD

    EParamDHCP          = 0x0020,
    EParamIPAddr        = 0x0021,
    EParamSubnet        = 0x0022,
    EParamGateway       = 0x0023,
    EParamNWMode        = 0x0024,
    EParamVLANId        = 0x0025,
    EParamNWProto       = 0x0026,
    EParamDSF           = 0x0027,
    EParamVLAN          = 0x0028,

#if !defined NO_IPV6
    EParamIPv6Addr      = 0x0029,
    EParamIPv6Prefix    = 0x002A,
    EParamIPv6Gateway   = 0x002B,
    EParamIPv6FixedAddr = 0x002C,
#endif // !defined NO_IPV6

#if !defined NO_UNICAST
    EParamGrantor0      = 0x0030,
    EParamGrantor1      = 0x0031,
#endif // !defined NO_UNICAST

    EParamAdjIVal       = 0x0040,
    EParamTwoStep       = 0x0041,
    EParamDlyReqIVal    = 0x0042,
    EParamVariance      = 0x0043,
    EParamClass         = 0x0044,
#ifdef _USE_PTP_SECURITY_
    EParamSecurity      = 0x0045,
#endif
    EParamDlyAsym       = 0x0046,
    EParamPIK           = 0x0047,
    EParamPIT           = 0x0048,
    EParamIIRSMS        = 0x0049,
    EParamIIRSPath      = 0x004A,
    EParamIIRAP         = 0x004B,
    EParamIIRAG         = 0x004C,
    EParamTSPlaus       = 0x004D,

#if defined(_SHARED_MEM_TO_CONFIG_) || defined (CFG_ESYNC)
    EParamVSync         = 0x004F,
    EParamESync         = 0x0050,
#endif // defined(_SHARED_MEM_TO_CONFIG_) || defined (CFG_ESYNC)

#if !defined(_NO_LUCKY_PACKET_FILTER_)
    EParamFilterDepth   = 0x0053,
#endif // !defined(_NO_LUCKY_PACKET_FILTER_)

    // we use this as last because the previous are conditional and we do not
    // need a comma at the end of the last entry.
    EParamBoundary      = 0x004E
  };
  /// @}

#if defined(USE_DEPRECATED)
  /// @{
  /// @anchor param_dep
  /// @name Deprecated Parameter types previously supported by Oregano specific "PARAM" management message
  static const uint16_t PARAM_KEY           = (uint16_t)EParamKey;
  static const uint16_t PARAM_LOGLEVEL      = (uint16_t)EParamLogLevel;
  static const uint16_t PARAM_SYNCOUT       = 0x0012;
  static const uint16_t PARAM_SYNCIN        = 0x0013;
  static const uint16_t PARAM_SYNCINOFF     = 0x0014;
  static const uint16_t PARAM_BAUD          = 0x0015;
  static const uint16_t PARAM_DHCP          = (uint16_t)EParamDHCP;
  static const uint16_t PARAM_IPADDR        = (uint16_t)EParamIPAddr;
  static const uint16_t PARAM_SUBNET        = (uint16_t)EParamSubnet;
  static const uint16_t PARAM_GATEWAY       = (uint16_t)EParamGateway;
  static const uint16_t PARAM_NWMODE        = (uint16_t)EParamNWMode;
  static const uint16_t PARAM_VLANID        = (uint16_t)EParamVLANId;
  static const uint16_t PARAM_NWPROTO       = (uint16_t)EParamNWProto;
  static const uint16_t PARAM_DSF           = (uint16_t)EParamDSF;
  static const uint16_t PARAM_VLAN          = (uint16_t)EParamVLAN;
#ifndef NO_IPV6
  static const uint16_t PARAM_IPV6ADDR      = 0x0029;
  static const uint16_t PARAM_IPV6PREFIX    = 0x002A;
  static const uint16_t PARAM_IPV6GATEWAY   = 0x002B;
  static const uint16_t PARAM_IPV6FIXEDADDR = 0x002C;
#endif
  static const uint16_t PARAM_GRANTOR0      = 0x0030;
  static const uint16_t PARAM_GRANTOR1      = 0x0031;
  static const uint16_t PARAM_ADJIVAL       = (uint16_t)EParamAdjIVal;
  static const uint16_t PARAM_TWOSTEP       = (uint16_t)EParamTwoStep;
  static const uint16_t PARAM_DLYREQIVAL    = (uint16_t)EParamDlyReqIVal;
  static const uint16_t PARAM_VARIANCE      = (uint16_t)EParamVariance;
  static const uint16_t PARAM_CLASS         = (uint16_t)EParamClass;
  static const uint16_t PARAM_SECURITY      = (uint16_t)EParamSecurity;
  static const uint16_t PARAM_DLYASYM       = (uint16_t)EParamDlyAsym;
  static const uint16_t PARAM_PIK           = (uint16_t)EParamPIK;
  static const uint16_t PARAM_PIT           = (uint16_t)EParamPIT;
  static const uint16_t PARAM_IIRSMS        = (uint16_t)EParamIIRSMS;
  static const uint16_t PARAM_IIRSPATH      = (uint16_t)EParamIIRSPath;
  static const uint16_t PARAM_IIRAP         = (uint16_t)EParamIIRAP;
  static const uint16_t PARAM_IIRAG         = (uint16_t)EParamIIRAG;
  static const uint16_t PARAM_TSPLAUS       = (uint16_t)EParamTSPlaus;
  static const uint16_t PARAM_BOUNDARY      = (uint16_t)EParamBoundary;
  static const uint16_t PARAM_VSYNC         = 0x004F;
  static const uint16_t PARAM_ESYNC         = 0x0050;
  static const uint16_t PARAM_FILTERDEPTH   = 0x0053;
  /// @}
#endif // defined(USE_DEPRECATED)

  ///@{
  ///@name Slave Monitoring
#ifdef _PTP_SLAVE_MONITORING_
  static const uint8_t CSlaveMonitoringMinMsgPerTLV = 1;  ///< Slave Monitoring default number of data points per TLV
  static const uint8_t CSlaveMonitoringMinMsgIval   = 1;  ///< Slave Monitoring default data collection interval
  static const uint8_t CSlaveMonitoringMaxMsgPerTLV = 20; ///< Slave Monitoring max number of data pointer per TLV
  static const uint8_t CSlaveMonitoringMaxMsgIval   = 20; ///< Slave Monitoring max data collection interval
  static const uint8_t CSlaveMonitoringRxTimingEn   = 0x1;
  static const uint8_t CSlaveMonitoringRxCompEn     = 0x2;
  static const uint8_t CSlaveMonitoringTxTimingEn   = 0x4;
#endif
  ///@}

  bool                    twoStep;              ///< Is a TwoStep clock (FollowUp capable)

  uint8_t                 prio1;                ///< Clock priority1
  uint8_t                 prio2;                ///< Clock priority2
  uint8_t                 domain;               ///< Domain
  uint8_t                 domainMin;            ///< min. Domain
  uint8_t                 domainMax;            ///< max. Domain
  uint16_t                sdoid;                ///< sdoid id for PTPv2.1
  int8_t                  announceInterval;     ///< Announce message interval
  int8_t                  syncInterval;         ///< Sync message interval
  int8_t                  adjInterval;          ///< Clock adjust interval

  /// DlyReq message interval offset (relative to sync interval)
  int8_t                  dlyReqIntervalOffs;

  int8_t                  pdlyReqInterval;      ///< PDlyReq message interval


  uint8_t                 dlyMechanism;         ///< last item of len zero indicates end of list

  uint8_t                 clkAccuracy;          ///< Clock accuracy (see 7.6.2.5)
  uint8_t                 clkClass;             ///< Clock class (see 7.6.2.4)
  uint16_t                clkVariance;          ///< Clock variance (see 7.6.3)
  bool                    masterOnly ;          ///< Master only  for PTPV2.1 (IEEE1588-2019 see 8.2.15.5.2)
#ifdef _USE_PTP_SECURITY_
  bool                    enableSecurity;       ///< Enable Security extension
  bool                    allowMutableFields;   ///< allow changing of mutable fields by TCs when PTP Security is active
#endif
  bool                    enableManagement;     ///< Enable management

  /// Announce Receipt Timeout (in Announce intervals)
  uint8_t                 announceRecptTout;

  TimeInterval_t          dlyAsymmetry;         ///< Path delay asmmetry
  uint8_t                 flags;                ///< Time Properties Flags
  uint8_t                 timeSrc;              ///< Time Source
  int16_t                 currUTCOffs;          ///< Current UTC Offset

  // Acceptable Master Table Datasets
  static AcceptableMasterTableDataset_t *accMasterTbl;      ///< Acceptable master table (per clock)
  AcceptableMasterPortDataset_t          accMasterTblPort;  ///< Acceptable master port (per port)

#if !defined NO_UNICAST
  /// Unicast Port Address List for Grantors,
  /// last item of len zero indicates end of list
  PortAddress_t           ucPortAddr[PTP_UC_NUM_REQ_CONN + 1];


  uint32_t                ucDuration;            ///< Duration of a requesting unicast connection
  uint32_t                ucMinDuration;         ///< min duration of a UC connection
  uint16_t                ucMaxConnections;      ///< Maximum number of unicast connections
  uint32_t                ucMaxMessages;         ///< Maximum number of unicast messages per second
#endif // !defined NO_UNICAST

  uint16_t                port;                  ///< Port Number
  uint8_t                 nwMode;                ///< Network Mode (UC..Unicast, MC..Multicast)
  uint8_t                 ptpProfile;            ///< PTP Profile
  uint8_t                 ptpProfileId[6];       ///< PTP profile ID
  uint8_t                 ptpVersion;            ///< PTP version used
  uint8_t                 ptpMinorVersion;       ///< PTP minor version used
  uint8_t                 stat_period;           ///< period for displaying statistics
  bool                    statLogEnabled;        ///< statistic logging enabled
  int8_t                  user_description[129]; ///< user description of the clock
  uint8_t                 manufacturerId[3];     ///< OUI of the manufacturer
  int8_t                  *revisionData;         ///< hardware and software revision
  int8_t                  *productDesc;          ///< product description
  bool                    swmode;                ///< software mode enabled
  bool                    v1HwCompatibility;     ///< V1 HW compatibility, event messages are padded

  uint8_t                 syncRecptTout;         ///< Sync Receipt Timeout (in Sync intervals)

  /// Power Profile configuration
#if defined _C37_238_2011_PROFILE_ || defined _C37_238_2017_PROFILE_
  uint16_t                gmId;                  ///< grandmasterID, 0x0003-0x00FE
#endif // defined _C37_238_2011_PROFILE_ || defined _C37_238_2017_PROFILE_
#if defined _C37_238_2011_PROFILE_
  uint32_t                gmTimeInaccuracy;      ///< grandmaster Time inaccuracy
  uint32_t                nwTimeInaccuracy;      ///< network Time inaccuracy
#endif // defined _C37_238_2011_PROFILE_
#if defined _C37_238_2017_PROFILE_
  uint32_t                c37_238_2017DataReserved; ///< reserved field as specified by standard fro compatibility with C37.238-2011
  uint32_t                totalTimeInaccuracy;   ///< total Time inaccuracy
#endif // defined _C37_238_2017_PROFILE_
#if defined _SMPTE_PROFILE_
  SyncMetadata_t          syncMetadata;          ///< synchronization metadata for SMPTE profile
  bool                    overruleSyncMetadata;  ///< allows customer to overrule the syncMetadata with any value (also all-zeros)
#endif // defined _SMPTE_PROFILE_
#if defined(_G8275_1_PROFILE_) || defined(_G8275_2_PROFILE_)
  G8275_1_2ProfileData_t  g8275_1_2Data;         ///< data for telecom 2 profile (G.8275.1)
#endif // defined(_G8275_1_PROFILE_) || defined(_G8275_2_PROFILE_)
#if defined _802_1AS_PROFILE_
  bool                    transport802_1as;      ///< 802.1as transport specific field in PTP header

  uint16_t                timeBaseIndicator;       ///< time base indicator to be used as GM
  // (802.1as)
  TimeInterval_t          neighborPropDelayThresh; ///< path delay threhold for 802.1as
  bool                    isAsBridge;              ///< Determines if engine port is a 802.1as
  // bridge
  bool                    hasSyncForwardingCap;    ///< Determines if hardware support for fowarding
  // Syncs is available
#endif // defined _802_1AS_PROFILE_

  bool                    usePktgen;          ///< Determines if packet generator is used
#if defined _PATH_TRACE_
  bool                    usePathTrace;       ///< Determines if path trace feature is used
#endif // defined _PATH_TRACE_
#if !defined PTP_NO_TRANSPARENT_CLOCK
  bool                    isTransparentClock; ///< Determines if engine port is a transparent clock
#endif // !defined PTP_NO_TRANSPARENT_CLOCK
  bool                    isBoundaryClock;    ///< Determines if engine port is a boundary clock
  bool                    isControllerClock;  ///< Determines if engine is a unicast controller
#ifdef _PTP_MONITORING_
  bool                    usePtpMonitoring;   ///< Enable PTP Monitoring support (no offical part of PTPv2)
#endif // _PTP_MONITORING_
#ifdef _SYSTEM_INFO_
  bool                    showSystemInfo;
#endif // _SYSTEM_INFO_
#ifdef _DYNAMIC_RECONFIGURATION_
  bool                    changed;            ///< flag to indicate that the config has been changed
#endif
#ifdef _PTP_SLAVE_MONITORING_
  SlaveMonitoringPortDataset_t slaveMonitoringDs;
  bool                    slaveMonitoringEnabled;
#endif

  /// @brief Default Constructor
  ///
  /// Sets all parameters to their respective default values.
  PTP_ConfigIfc() {
    prio1                  = PTP_PRIORITY1;
    prio2                  = PTP_PRIORITY2;
    twoStep                = PTP_TWOSTEP;
    domain                 = PTP_DOMAIN;
    sdoid                  = 0x0000; 
    domainMin              = CPtpDomainMin;
    domainMax              = CPtpDomainMax;
    syncInterval           = PTP_SYNCINTERVAL;
    adjInterval            = PTP_ADJINTERVAL;
    announceInterval       = PTP_ANNOUNCEINTERVAL;
    dlyReqIntervalOffs     = PTP_DLYREQINTERVALOFFS;
    pdlyReqInterval        = 0;
    dlyMechanism           = CE2E;
    clkAccuracy            = clkAccuracy100us;  // 100 us
    clkClass               = PTP_CLKCLASS;
    clkVariance            = 0xFFFF;
    masterOnly             = false ;
#ifdef _USE_PTP_SECURITY_
    enableSecurity         = false;
    allowMutableFields     = false;
#endif
    enableManagement       = true;
    dlyAsymmetry.scaledNs  = 0;
    announceRecptTout      = PTP_ANNOUNCERECEPTTOUT;  // 2 .. 10
    flags                  = 0 | CPtpTimescale;
    timeSrc                = timeSrcIntXO;  // internal oscillator
    currUTCOffs            = CUtcOffset;
#if !defined NO_MULTICAST
    nwMode                 = CNwModeMC; // multicast mode, default
#else // !defined NO_MULTICAST
    nwMode                 = CNwModeUC; // unicast, if multicast is disabled
#endif // !defined NO_MULTICAST
#if !defined NO_UNICAST
    // Unicast settings
    for (uint8_t i = 0; i < PTP_UC_NUM_REQ_CONN + 1; i++) {
      ucPortAddr[i].nwProto = 0;
      ucPortAddr[i].len     = 0;
      memset(ucPortAddr[i].addr, 0x00, 16);
    }
#   if defined PTP_UC_MASTER0
    ucPortAddr[0].addr[0] = PTP_UC_MASTER0;
    ucPortAddr[0].addr[1] = PTP_UC_MASTER1;
    ucPortAddr[0].addr[2] = PTP_UC_MASTER2;
    ucPortAddr[0].addr[3] = PTP_UC_MASTER3;
    ucPortAddr[0].nwProto = PTP_ConfigIfc::CNwProtoL3IPv4;
    ucPortAddr[0].len     = 4;
#   endif // defined PTP_UC_MASTER0
#   if defined PTP_UC_MASTER4
    // operate in Ethernet mode
    ucPortAddr[0].addr[4] = PTP_UC_MASTER4;
    ucPortAddr[0].addr[5] = PTP_UC_MASTER5;
    ucPortAddr[0].nwProto = PTP_ConfigIfc::CNwProtoL2;
    ucPortAddr[0].len     = 6;
#   endif // defined PTP_UC_MASTER4
    ucDuration        = PTP_ConfigIfc::CUCDefaultDuration;
    ucMinDuration     = PTP_ConfigIfc::CUCMinDuration;
    ucMaxConnections  = PTP_UC_NUM_CONN;
    ucMaxMessages     = PTP_ConfigIfc::CUCMaxMsg;
#endif // !defined NO_UNICAST
    ptpProfile        = CPtpProfileNone;
    PTP_MEMCPY(ptpProfileId, CPtpProfileIdDefault, 6);
    ptpVersion         = PTP_VERSION_NUMBER;
    ptpMinorVersion = 0;
    port               = 1;
    stat_period        = 0;
    statLogEnabled     = false;
    swmode             = false;
    v1HwCompatibility  = false;
#if defined _802_1AS_PROFILE_
    transport802_1as   = false;
#endif // defined _802_1AS_PROFILE_

#if defined _C37_238_2011_PROFILE_ || defined _C37_238_2017_PROFILE_
    gmId               = C37_238_GMID;
#endif // defined _C37_238_2011_PROFILE_ || defined _C37_238_2017_PROFILE_
#if defined _C37_238_2011_PROFILE_
    gmTimeInaccuracy   = C37_238_2011_GMINAC;
    nwTimeInaccuracy   = C37_238_2011_NWINAC;
#endif // defined _C37_238_2011_PROFILE_
#if defined _C37_238_2017_PROFILE_
    c37_238_2017DataReserved = 0;
    totalTimeInaccuracy = C37_238_2017_TOTINAC;
#endif // defined _C37_238_2017_PROFILE_

#if defined _SMPTE_PROFILE_
    syncMetadata.defaultSystemFrameRateNum   = 25;
    syncMetadata.defaultSystemFrameRateDenum = 1;
    syncMetadata.masterLockingStatus         = 0;
    syncMetadata.timeAddressFlags            = 0;
    syncMetadata.currentLocalOffset          = 0;
    syncMetadata.jumpSeconds                 = 0;
    PTP_MEMSET(syncMetadata.timeOfNextJump, 0, 6);
    PTP_MEMSET(syncMetadata.timeOfNextJam,  0, 6);
    PTP_MEMSET(syncMetadata.timeOfPreviousJam,  0, 6);
    syncMetadata.previousJamLocalOffset      = 0;
    syncMetadata.daylightSaving              = 0;
    syncMetadata.leapSecondJump              = 0;
#endif // defined _SMPTE_PROFILE_

#if defined(_G8275_1_PROFILE_) || defined(_G8275_2_PROFILE_)
    g8275_1_2Data.portMasterOnly       = false;
    g8275_1_2Data.portLocalPriority    = PTP_G8275_1_LOCAL_PRIO;
    g8275_1_2Data.defaultLocalPriority = PTP_G8275_1_LOCAL_PRIO;
#endif // defined(_G8275_1_PROFILE_) || defined(_G8275_2_PROFILE_)
    syncRecptTout        = 3;
#if defined _802_1AS_PROFILE_
    timeBaseIndicator    = 0;
    neighborPropDelayThresh.scaledNs = PTP_802_1AS_PDELAY_THRESHOLD;
    isAsBridge           = false;
    hasSyncForwardingCap = false;
#endif // defined _802_1AS_PROFILE_

    usePktgen           = false;
#if defined _PATH_TRACE_
    usePathTrace        = false;
#endif // defined _PATH_TRACE_
#if !defined PTP_NO_TRANSPARENT_CLOCK
    isTransparentClock  = false;
#endif // !defined PTP_NO_TRANSPARENT_CLOCK
    isBoundaryClock     = false;
    isControllerClock   = false;
#if defined _PTP_MONITORING_
    usePtpMonitoring    = true;
#endif // defined _PTP_MONITORING_
#if defined _SYSTEM_INFO_
    showSystemInfo      = false;
#endif // defined _SYSTEM_INFO_

    // set default user description,
    // can be overwritten with description from flash
    PTP_MEMCPY((char *)user_description,
               USERDESCRIPTION,
               sizeof(USERDESCRIPTION));

    for (uint8_t i = 0; 3 > i; ++i) {
      manufacturerId[i] = CPtpOUI[i];
    }
    revisionData = NULL;
    productDesc  = NULL;
#ifdef _DYNAMIC_RECONFIGURATION_
    changed      = true;
#endif
#ifdef _PTP_SLAVE_MONITORING_
    slaveMonitoringEnabled = false;
#endif
  }

  /// Action Ids for saving configuration data
  enum NvActSave_e {
    ESaveConfig      = 0, ///< @hideinitializer
    ESaveErase       = 1, ///< @hideinitializer
    ESaveProg        = 2, ///< @hideinitializer
    ESaveParam       = 3, ///< @hideinitializer
    ESaveConfigErase = 4  ///< @hideinitializer
  };

  /// Action Ids for reading data
  enum NvActRead_e {
    ERead       = 0,  ///< @hideinitializer
    EReadConfig = 1,  ///< @hideinitializer
    EReadParam  = 2   ///< @hideinitializer
  };

  /// @brief Save non-volatile memory handler
  ///
  /// This funtion is called from PTP whenever a saveNonVolatile
  /// management message is received. Its contents are implementation-specific.
  /// Format: 1st byte nvActionId, then 4 bytes nvAddrLen, then optional data.
  /// This function may be used to trigger saving of the current
  /// activated configuration.
  /// @param [in] nvActionId
  /// @param [in] nvAddrLen
  /// @param [in] nvData (optional)
  /// @param [in] nvDataLen
  /// @return ENoErr on success, EErr otherwise
  virtual int8_t saveNonVolatile(uint8_t nvActionId,
                                 uint32_t nvAddrLen,
                                 uint8_t *nvData,
                                 uint16_t *nvDataLen) {
    (void)nvActionId;
    (void)nvAddrLen;
    (void)nvData;
    (void)nvDataLen;
    return EErr;
  }

  /// @brief Read non-volatile memory handler
  ///
  /// Triggers implementation specific actions.
  /// @param [in] nvActionId
  /// @param [in] nvAddrLen
  /// @param [in,out] nvData
  /// @param [in,out] nvDataLen
  /// @return ENoErr on success, EErr otherwise
  virtual int8_t readNonVolatile(uint8_t nvActionId,
                                 uint32_t nvAddrLen,
                                 uint8_t *nvData,
                                 uint16_t *nvDataLen) {
    (void)nvActionId;
    (void)nvAddrLen;
    (void)nvData;
    (void)nvDataLen;
    return EErr;
  }

#ifdef _DYNAMIC_RECONFIGURATION_
  /// @brief Config Heartbeat
  ///
  /// used for dynamic reconfiguration
  virtual int heartbeat(uint32_t tout) {
    (void)tout;
    return ENoErr;
  }
#endif

#if defined PTP_CONFIG_DEBUG_DUMP
  void dump() {}
#endif // defined PTP_CONFIG_DEBUG_DUMP

  /// Destructor
  virtual ~PTP_ConfigIfc() {}
};

#endif // !defined INC_CONFIG_IFC
