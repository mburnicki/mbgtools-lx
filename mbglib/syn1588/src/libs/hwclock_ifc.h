/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/// @file
/// This file contains the implementation of the classes PTP_ConfigHwClk
/// and PTP_HwClockIfc

#ifndef INC_HWCLOCK_IFC
#define INC_HWCLOCK_IFC

#include "basictypes.h"
#include "clock_ifc.h"
#include "config_ifc.h"

/// @brief HW Clock Configuration
///
/// This structure contains the configuration for a PTP HW clock. The parameters
/// may be used by a hardware clock implementation to configure the hardware clock accordingly.
struct PTP_ConfigHwClk {

  /// @brief Vlan Id enable mask.
  /// This mask is used to signal vlan activation. Vlan id is only 12 bit
  static const uint16_t CEnableVlanMask = 0xF000;

  uint16_t nwProto;         ///< Network Protocol
  uint8_t  nwMode;          ///< Network mode (MC, UC,...)
  uint8_t  domain;          ///< PTP domain
  uint8_t  index;           ///< Index of the desired hardware instance
  uint16_t vlanId;          ///< VLAN identifier
  uint32_t usedTimestamper; ///< type of timestamper to be used
  bool     userSelectedTimestamper; ///< Flag to signal that a user selected a timestamper
  bool     twoStep;         ///< 2 step flag
  bool     initTimestamper; ///< flag to init the timestamper
  bool     initRate;        ///< flag to init rate
  bool     isP2P;           ///< flag to init P2P mode

  /// Default Constructor
  PTP_ConfigHwClk() {
    nwProto         = PTP_ConfigIfc::CNwProtoL3IPv4;
    nwMode          = PTP_ConfigIfc::CNwModeMC;
    domain          = PTP_DOMAIN;
    index           = 0;
    vlanId          = 0;
    usedTimestamper = 0;
    userSelectedTimestamper = false;
    twoStep         = PTP_TWOSTEP;
    initTimestamper = true;
    initRate        = false;
    isP2P           = false;
  }
};

/// @brief HW Clock Interface Class
///
/// This class defines an interface (hardware abstraction layer) for a hardware
/// clock implementation. A \a PTP_ClockIfc class implementation may use this
/// interface to access time information. This way a single clock class may be
/// used with different hardware clock implementations without recoding effort.
/// A HW clock has to implement all functions defined in this interface by abstracting
/// the actual handling of the hardware clock.
/// @ingroup interfaces
class PTP_HwClockIfc {

  public:

    /// Clock adjustment type
    typedef enum {
      EAdjustAnalog,    ///< use analog clock adjustment
      EAdjustDigital,   ///< use digital clock adjustment
      EAdjustInit,      ///<
      EAdjustLimit      ///<
    } ClockAdjustMode_t;

    /// Destructor
    virtual ~PTP_HwClockIfc() {}

    /// @brief Initialization function
    /// @param [in] cfg a HW clock configuration structure
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t init(PTP_ConfigHwClk *cfg) {
      (void)cfg;
      return EErr;
    }

    /// @brief Get the actual time
    ///
    /// Returns the actual timestamp in the Timestamp_t structure \a ts.
    /// The timestamp is based on TAI
    /// @param [in,out] ts
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getTime(Timestamp_t &ts) = 0;

    /// @brief Set the clock time
    ///
    /// Set the clock time to \a ts.
    /// The timestamp is based on TAI
    /// @param [in] ts
    /// @return ENoErr on success, EErr otherweise
    virtual int8_t setTime(const Timestamp_t &ts) = 0;

    /// @brief Get a Timestamp
    ///
    /// If \a recv is true, a receive timestamp shall be fetched, otherwise a
    /// send timestamp. The source port id \a pId, the sequence identifier
    /// \a seqId and the message type \a type are used to find the corresponding
    /// timestamp \a ts of a PTP packet.
    /// The timestamp is based on TAI.
    /// @param [in] recv set true for receive timestamp, false for send timestamp
    /// @param [in,out] ts holds timestamp after successful call
    /// @param [in,out] subNs holds sub-nanoseconds, if applicable
    /// @param [in] pId source port id
    /// @param [in] seqId sequence id
    /// @param [in] type message type
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getTimestamp(bool recv, Timestamp_t &ts, TimeInterval_t &subNs,
                                const PortId_t *pId, const uint16_t seqId,
                                const uint8_t type) = 0;

    /// @brief Update Path Delay
    ///
    /// Implement this function if you need to do something hardware specific with
    /// the PeerToPeer delay.
    /// For Example: You need to write the peer delay into a hardware register of
    /// a 1-Step transparent clock.
    /// @param [in] offset
    virtual void updatePathDelay(const TimeInterval_t &offset) {
      (void)offset;
    }

    /// @brief Adjust the Clock rate
    ///
    /// Adjust the clock rate by \a drift in scaled nanoseconds per second (nanoseconds << 16).
    /// \a mode specifies which adjusting mode shall be used (should be only used if there are
    /// different modes supported by the hardware clock). \a ival contains the current adjust
    /// interval.
    /// @param [in] drift the drift to set
    /// @param [in] mode a clock adjust mode
    /// @param [in] ival current adjust interval
    /// @param [in] rateLimitOverride flag to override rate limitations
    /// @see ClockAdjustMode_t
    virtual void adjustRate(const TimeInterval_t &drift, uint8_t mode, int8_t ival, bool rateLimitOverride = false) = 0;

    /// @brief Compensate offset
    ///
    /// Add/subtract time to compensate \a offset. This function is used to compensate
    /// a large offset with a single operation.
    /// @param [in] offset the offset to compensate
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t compensateOffset(const TimeInterval_t &offset) {
      (void)offset;
      return EErr;
    }

    /// @brief Get the clock drift
    ///
    /// Return the current drift of the clock. Usually, the value depends on the
    /// value of the clock's current rate. In most implementations this value depends on
    /// the clock's frequency as well. The drift is used to initialize the PI controller
    /// to improve the startup behavior of the clock. If there is no value available \a
    /// drift should be set to 0.
    /// @param [in,out] drift holds current clock drift after successful call
    /// @param [in] mode
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getDrift(TimeInterval_t &drift, uint8_t mode) {
      (void)mode;
      drift.scaledNs = 0;
      return ENoErr;
    }


    /// @brief Manage the clock
    ///
    /// This function is used by the PTP Stack to indicate/request/manage certain
    /// actions/data to the clock. It provides a pointer \a buf to a data buffer of
    /// the size \a len. If \a len is 0, \a buf does not contain any data.
    /// \a type indicates the manage event. \a cfg contains the configuration of the
    /// hardware clock.
    /// @param [in] cfg HW Clock configuration
    /// @param [in] type clock management type
    /// @param [in] buf additional data
    /// @param [in] len length of \a buf
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t manage(PTP_ConfigHwClk* cfg, PTP_ClockIfc::ManageType_t type, void *buf, size_t len) = 0;

    /// @brief Get Clock Identifier
    ///
    /// This function is called by the stack to obtain its unique
    /// identifier, which is an 8 byte octet (IEEE EUI-64 address).
    /// @param [in,out] id holds clock id after successful call
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getId(uint8_t *id) = 0;

    /// @brief Get HW info
    ///
    /// Get a string with HW information. <br>The string length must not exceed
    /// 64 bytes and it has to be null-terminated. This adheres to the specification of
    /// PTP_String in IEEE1588-2008
    /// @param [in,out] string holds HW info after successful call
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getHWInfo(char *string) = 0;

    /// @brief Get version info
    ///
    /// Get a string with HW version information. <br>The string length must not exceed
    /// 64 bytes and it has to be null-terminated. This adheres to the specification of
    /// PTP_String in IEEE1588-2008
    /// @param [in,out] string holds HW version info after successful call
    /// @return ENoErr on success, EErr otherwise
    virtual int8_t getVersionInfo(char *string) = 0;

    ///@{
    /// @name Hardware clock capability flags
    /// @anchor HwCap

    /// dedicated 1588 hardware support
    static const uint16_t CHasHardwareSupport   = 0x01;
    /// support for one step mode (sync messages only)
    static const uint16_t CHasOneStepSupport    = 0x02;
    /// equipped with high quality oscillator
    static const uint16_t CHasHQOscillator      = 0x04;
    /// support for one step peer-to-peer meachanism
    static const uint16_t CHasOneStepP2PSupport = 0x08;
    /// support for compensate offset functionality
    static const uint16_t CHasCompensateOffsetSupport = 0x10;
    /// support for subtract offset functionality
    static const uint16_t CHasSubtractOffsetSupport = 0x20;
    /// support for system internal leapsecond processing (Linux kernel leap second support)
    static const uint16_t CHasSystemLeapsecondSupport = 0x40;
    /// support for network packet delivered timestamps (SO_TIMESTAMPING for Linux, no support atm for windows)
    static const uint16_t CHasPacketAttachedTimestamp = 0x80;
    ///@}

    /// @brief Get capabilities
    ///
    /// Get the capabilities of the clock defined by hardware clock capabilities.
    /// @return clock capabilities as bitmask
    /// @see @ref HwCap "HW Clock capability flags"
    virtual uint16_t getCapabilities() = 0;
};

#endif
