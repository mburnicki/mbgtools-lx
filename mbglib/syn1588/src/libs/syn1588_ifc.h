/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

///
/// @file
/// This file contains the class definiton for Syn1588Ifc. It contains
/// the basic register access functions as well as the register addresses. The
/// header file is used for all platforms whereby the .cpp files is platform
/// specific.
///

#ifndef INC_SYN1588_IFC
#define INC_SYN1588_IFC

#include "basictypes.h"
#include "log_ifc.h"

#include <stddef.h>

/// This enum describes which kind of hardware this
/// Instance of the Syn1588Ifc represents.
enum PTP_HwType {
  Unknown_NIC = 0,  ///< unknown NIC @hideinitializer
  PCI_NIC,          ///< syn1588 NIC version 1.0
  PCIe_NIC,         ///< syn1588 NIC version 1.5
  VIP_board2_1,     ///< VIP 2.1
  IEEE1588_NIC,     ///< PTP-capable NIC
  PCIe_NIC2_0,      ///< syn1588 NIC version 2.0
  PCIe_NIC2_1,      ///< syn1588 NIC version 2.1
  VIP_board3_0,     ///< VIP 3
  IP_Core,          ///< IP core
  Dual_NIC          ///< syn1588 Dual NIC
};

#define PTP_HW_TYPES \
  { "unknown NIC", \
    "syn1588(R) PCI NIC", \
    "syn1588(R) PCIe NIC Revision 1.5", \
    "syn1588(R) VIP Revision 2.1", \
    "IEEE1588 NIC", \
    "syn1588(R) PCIe NIC Revision 2", \
    "syn1588(R) PCIe NIC Revision 2.1", \
    "syn1588(R) VIP Revision 3", \
    "syn1588(R) IP Core", \
    "syn1588(R) Dual NIC", \
  }

static const uint32_t CLOCKID_H_PCI      = 0x006035FF;
static const uint32_t CLOCKID_H_PCIE15_0 = 0x0004A3FF;
static const uint32_t CLOCKID_H_PCIE15_1 = 0x001EC0FF;
static const uint32_t CLOCKID_H_PCIE21   = 0x8CA5A1FF;

// constants for build numbers that enable certain register
#if defined __NIOS__ || defined __SYN1588_IP_CORE__ // NIC, ...
static const uint16_t MAC_CAP_BUILD_VER = 0;
static const uint16_t AUX_REG_BUILD_VER = 0;
static const uint16_t I2C_REG_BUILD_VER = 0;
static const uint16_t CAP_REG_BUILD_VER = 0;
#else // NIC
static const uint16_t MAC_CAP_BUILD_VER = 804;
static const uint16_t AUX_REG_BUILD_VER = 808;
static const uint16_t I2C_REG_BUILD_VER = 789;
static const uint16_t CAP_REG_BUILD_VER = 622;
#endif

// forward declaration
class Syn1588Impl;

/// Interface Class for Syn1588 Clock

/// This class defines the low-level interface to the Syn1588 Clock.
/// @ingroup syn1588API
class Syn1588Ifc {

  private:
    Syn1588Impl *m_syn1588Impl;
    PTP_LogIfc  *m_log;   // logging pointer
    //optional, not necessarily necessary:
    int32_t     m_buildnum;
    uint32_t    m_cap1;
    uint32_t    m_macCap0;

    /// Check function
    /// @param addr register address to check for accessibility
    Return_e check(const uint32_t addr);

    // static cariable to prevent logging the notice about minimum requirements of the driver
    // multiple times
    static bool m_alreadyLogged;

  public:
    /// Constructor
    Syn1588Ifc(PTP_LogIfc *log, const uint8_t card);

    /// Destructor
    ~Syn1588Ifc();

    /// Returns true if Syn1588 NIC has been found.
    bool present();

    /// Return the HW type
    /// see PTP_HwType
    uint8_t getType();

    /// Return the device file handle
    SYN1588_DEV_HANDLE getHandle();

    /// Raw register read
    /// @param [in] addr register address
    /// @param [in,out] err optional, holds ENoErr/EErr after call
    /// @return register value
    uint32_t readRegRaw(const uint32_t addr, Return_e * const err = NULL);

    /// Register read
    /// @param [in] addr register address
    /// @param [in,out] err optional, holds ENoErr/EErr after call
    /// @return register value
    uint32_t readReg(const uint32_t addr, Return_e * const err = NULL);

    /// Register write
    /// @param addr register address
    /// @param data register value
    /// @return ENoErr on success, EErr on failure
    Return_e writeReg(const uint32_t addr, const uint32_t data);

    /// Set register bits according to \a mask
    /// @param addr register address
    /// @param mask bitmask with bits to set
    /// @return ENoErr on success, EErr on failure
    Return_e setBit(const uint32_t addr, const uint32_t mask);

    /// clear register bits according to \a mask
    /// @param addr register address
    /// @param mask bitmask with bits to clear
    /// @return ENoErr on success, EErr on failure
    Return_e clearBit(const uint32_t addr, const uint32_t mask);

    /// Register Event (for select processing)
    /// @param event bitmask of interrupt bits to set
    /// @return ENoErr on success, EErr on failure
    Return_e registerEvent(uint32_t event);

    /// Disable a registered event
    /// @param [in] event
    /// @return ENoErr on success, EErr on failure
    Return_e disableEvent(uint32_t event);

    /// Wait for an event
    /// @param timeout Time in milliseconds for blocking wait for an interrupt
    /// @param [in,out] err optional, holds ENoErr/EErr after call
    /// @returns bitmask of set interrupt bits
    uint32_t waitForEvent(uint32_t timeout, Return_e * const err = NULL);

    /// signals if the underlying system can provide timestamps with a packet
    bool canProvidePacketAttachedTimestamp(void) const;

    static const uint8_t CMaxCards             = 15;     ///< Determines the maximum numbers of syn1588 cards

    ///@{
    ///@name syn1588 clock M register addresses
    ///
    static const uint16_t  SYN1588_VER         = 0x000;  ///< Syn1588(R) CSC version register R
    static const uint16_t  TSFIFO0RX           = 0x010;  ///< receive Timestamp FIFO0 data <31:00> R
    static const uint16_t  TSFIFO0TX           = 0x018;  ///< send Timestamp FIFO0 data <31:00> R
    static const uint16_t  TSFIFO1RX           = 0x01C;  ///< receive Timestamp FIFO1 data <31:00> R
    static const uint16_t  TSFIFO1TX           = 0x020;  ///< send Timestamp FIFO1 data <31:00> R
    static const uint16_t  IRIGB_IN_SECONDS    = 0x024;  ///< 32 bit seconds decoded from the IRIG-B input data stream
    static const uint16_t  IRIGB_IN_STATUS     = 0x028;  ///< holds the QoS information of the decoded IRIG-B signal
    static const uint16_t  TSFIFO0_STATUS      = 0x030;  ///< Timestamp FIFO counter and full/empty flags R
    static const uint16_t  TSFIFO1_STATUS      = 0x034;  ///< Timestamp FIFO counter and full/empty flags R
    static const uint16_t  AUXILIARY           = 0x038;  ///< Auxiliary register to select external clock, set DDR for HD mode
    static const uint16_t  PTP_SYNC_STATUS     = 0x03C;  ///< The PTP Sync Status register is used by the syn1588 ptp stack to sync the current status to the kernel
    static const uint16_t  IRSRC               = 0x040;  ///< Interrupt Source R/W
    static const uint16_t  IREN                = 0x044;  ///< Enables various interrupts R/W
    static const uint16_t  TIMECTRL            = 0x048;  ///< Controls operation of the local clock R/W
    static const uint16_t  EVENTCTRL           = 0x04C;  ///< Control of events R/W
    static const uint16_t  SHDWSTEPPU_L        = 0x050;  ///< Future step size (pure phase), lower 32 bit R/W
    static const uint16_t  SHDWSTEPPU_H        = 0x054;  ///< Future step size (pure phase), upper 32 bit R/W
    static const uint16_t  ONESTEPCTRL         = 0x058;  ///< Allows to set different modes for one step timestamper R/W
    static const uint16_t  SHADOW_TEN_FIELD_ID = 0x05C;  ///< Shadow-register: Load value of the Ten Field Sequence ID counter R/W
    static const uint16_t  TS_PHY_DELAY_TX     = 0x064;  ///< PHY delay correction transmit, R/W
    static const uint16_t  TS_PHY_DELAY_RX     = 0x068;  ///< PHY delay correction receive, R/W
    static const uint16_t  CAPABILITIES1       = 0x06C;  ///< Indicates implemented hardware features, R
    static const uint16_t  CAPABILITIES2       = 0x070;  ///< Reserved for future use, R
    static const uint16_t  CAPABILITIES3       = 0x074;  ///< Reserved for future use, R
    static const uint16_t  LSYNC_SYNC_STATUS   = 0x080;  ///< The lSync Sync Status register is used by the lsync utility to sync the current status to the kernel
    static const uint16_t  SHDWTIME_NS         = 0x084;  ///< Future contents of the clock, nanoseconds, W
    static const uint16_t  SHDWTIME_SEC        = 0x088;  ///< Future contents of the clock, seconds, W
    static const uint16_t  SDI_DELTA_NEXT_ALIGN = 0x08C; ///< SDI extended signal generation R/W
    static const uint16_t  TIME_L              = 0x090;  ///< Current time, sub nanoseconds,  32 bit R   -> deprecated
    static const uint16_t  TIME_NS             = 0x094;  ///< Current time, nanoseconds,      32 bit R
    static const uint16_t  TIME_SEC            = 0x098;  ///< Current time, seconds,          32 bit R
    static const uint16_t  SHADOW_AUDIO_TIME   = 0x09C;  ///< Shadow-register: Load value of Audio Time counter R/W
    static const uint16_t  AUDIO_DELTA_NEXT_ALIGN = 0x0A0; ///< Audio signal generation R/W
    static const uint16_t  EVENTTIME0_L        = 0x0A4;  ///< Time of last event 0, lower 32 bit R
    static const uint16_t  EVENTTIME0_H        = 0x0A8;  ///< Time of last event 0, upper 32 bit R
    static const uint16_t  EVENTTIME1_L        = 0x0B0;  ///< Time of last event 1, lower 32 bit R
    static const uint16_t  EVENTTIME1_H        = 0x0B4;  ///< Time of last event 1, upper 32 bit R
    static const uint16_t  FRAC_NUM            = 0x0B8;  ///< fractional counter numerator for high precision period
    static const uint16_t  FRAC_DENUM          = 0x0BC;  ///< fractional counter denumerator for high precision period
    static const uint16_t  FRAME_COUNT         = 0x0C0;  ///< H-Sync and F-Sync configuration
    static const uint16_t  SHD_SDITIME27M      = 0x0C4;  ///< Shadow-register: Load value of 27 MHz SDI_TIME counter
    static const uint16_t  GENLOCK_CTRL        = 0x0C8;  ///< Genlock control register
    static const uint16_t  SHD_SDITIME90k      = 0x0CC;  ///< Shadow-register: Load value of 90 kHz SDI_TIME counter
    static const uint16_t  TRIGTIME0_L         = 0x0D0;  ///< Time to trigger event 0, first 32 bit R/W
    static const uint16_t  TRIGTIME0_H         = 0x0D4;  ///< Time to trigger event 0, upper 32 bit R/W
    static const uint16_t  TRIGTIME1_L         = 0x0D8;  ///< Time to trigger event 1, first 32 bit R/W
    static const uint16_t  TRIGTIME1_H         = 0x0DC;  ///< Time to trigger event 1, upper 32 bit R/W
    static const uint16_t  PERIODTIME0_L       = 0x0F0;  ///< Period of timer, lower 32 bit R/W
    static const uint16_t  PERIODTIME0_H       = 0x0F4;  ///< Period of timer, upper 32 bit R/W
    static const uint16_t  PERIODTIME1_L       = 0x100;  ///< Period of timer, lower 32 bit R/W
    static const uint16_t  PERIODTIME1_H       = 0x104;  ///< Period of timer, upper 32 bit R/W
    static const uint16_t  LEAP_SEC            = 0x108;  ///< Current leap seconds (8 bit) R (current value / W (future value)
    static const uint16_t  LEAP_APPLYTIME      = 0x10C;  ///< Leap seconds apply time (32 bit seconds) R/W
    static const uint16_t  OSZPWM_CTR          = 0x1E0;  ///< PWM output control register R/W
    static const uint16_t  CLOCKID_L           = 0x1E4;  ///< Low word of the 64 bit EUI R
    static const uint16_t  CLOCKID_H           = 0x1E8;  ///< High word of the 64 bit EUI R
    static const uint16_t  HWBUILD             = 0x1F0;  ///< Virtual copy of MAC_ETH_BUILD        -> deprecated, use virtual register
    static const uint16_t  IOMATRIX            = 0x200;  ///< I/O interconnection definition R/W
    static const uint16_t  CLKFREQ             = 0x204;  ///< Clock frequency
    static const uint16_t  MIITS0_RXCRTL       = 0x214;  ///< MII Timestamper 0 RX Control
    static const uint16_t  MIITS0_TXCRTL       = 0x208;  ///< MII Timestamper 0 TX Control
    static const uint16_t  MIITS1_RXCRTL       = 0x20C;  ///< MII Timestamper 1 RX Control
    static const uint16_t  MIITS1_TXCRTL       = 0x210;  ///< MII Timestamper 1 TX Control
    static const uint16_t  TIME2_NS            = 0x220;  ///< Current time, 32 bit nanoseconds R
    static const uint16_t  TIME2_SEC           = 0x224;  ///< Current time, 32 bit seconds R
    static const uint16_t  PERIODTIME2_L       = 0x254;  ///< Period of timer, lower 32 bit R/W
    static const uint16_t  PERIODTIME2_H       = 0x258;  ///< Period of timer, upper 32 bit R/W
    static const uint16_t  PERIODTIME3_L       = 0x25C;  ///< Period of timer, lower 32 bit R/W
    static const uint16_t  PERIODTIME3_H       = 0x260;  ///< Period of timer, upper 32 bit R/W
    static const uint16_t  SHADOW_SDI_LTC_TIME_ADDR = 0x264; ///< shadow for the SDI LTC Time Address value
    static const uint16_t  SHADOW_SDI_LTC_BIN_GROUP = 0x268; ///< shadow for the SDI LTC Binary Group value
    static const uint16_t  SDI_LTC_CONTROL     = 0x26C;  ///< control register of the SDI LTC signal generator
    static const uint16_t  SDI_LTC_TELEGRAM    = 0x270;  ///< interface to the telegram snapshot of SDI signal and counter values
    static const uint16_t  MIITS0_RXMEM        = 0x400;  ///< MII Timestamper 0 RX Pattern/Mask Memory
    static const uint16_t  MIITS0_TXMEM        = 0x600;  ///< MII Timestamper 0 TX Pattern/Mask Memory

    /// MII Timestamper 1 RX Pattern/Mask Memory
    /// This overlaps with Event16 registers for VMS NIC
    static const uint16_t  MIITS1_RXMEM        = 0x800;
    static const uint16_t  MIITS1_TXMEM        = 0xA00;  ///< MII Timestamper 1 TX Pattern/Mask Memory

    ///@{
    ///@name Pktgen registers for 32 bit interface
    /// TODO: Add base address offset to the addresse according to hardware implementation.
    /// Must not be used yet!
#ifdef _USE_PKTGEN_
    /// Location definition of packet genertor V2 registers and memories
    static const uint32_t PKTGEN_V2_BASE               = 0x3F000;  ///< Base address of channel 0 registers
    static const uint16_t PKTGEN_V2_CHANNEL0_OFFSET    = 0x00000;  ///< Offset for channel 0
    static const uint16_t PKTGEN_V2_CHANNEL1_OFFSET    = 0x08000;  ///< Offset for channel 1 (identical address map)
    static const uint16_t PKTGEN_V2_CHANNEL_RAM_OFFSET = 0x01000;  ///< Base address of channel 0 memory
    static const uint32_t PKTGEN_V2_SLAVE_OFFSET       = 0x10000;  ///< slave list
    /// Offset addresses for the channel registers
    /// address is calculated with BASE + CHANNEL_OFFSET + register offset(address)
    static const uint16_t PKTGEN_V2_PKT_VERSION            = 0x0000; ///< Packet generator version register
    static const uint16_t PKTGEN_V2_PKT_CAPABILITY_0       = 0x0004; ///< Capability 0 register
    static const uint16_t PKTGEN_V2_PKT_CAPABILITY_1       = 0x0008; ///< Capability 0 register
    static const uint16_t PKTGEN_V2_PKT_CAPABILITY_2       = 0x000C; ///< Capability 0 register
    static const uint16_t PKTGEN_V2_PKT_CAPABILITY_3       = 0x0010; ///< Capability 0 register
    static const uint16_t PKTGEN_V2_IMPL_SLAVE_LIST_NUM    = 0x0014; ///< Implemented slave list length
    static const uint16_t PKTGEN_V2_CHANNEL_BOUNDARY       = 0x0018; ///< Slave list boundary of the two channels
    static const uint16_t PKTGEN_V2_PKT_MODE               = 0x001C; ///< Configuration of packet generator
    static const uint16_t PKTGEN_V2_MAX_SYNC_ANNOUNCE_RATE = 0x0020; ///< Fastest sync / announce rate in the slave list
    static const uint16_t PKTGEN_V2_SLAVE_LIST_NUM         = 0x0024; ///< Actual number of slave list entries of this channel
    static const uint16_t PKTGEN_V2_SYNC_ANNOUNCE_LEN      = 0x0028; ///< Length of sync and announce packets
    static const uint16_t PKTGEN_V2_DLYRESP_LEN            = 0x002C; ///< Length of delay response or NTP packets
    static const uint16_t PKTGEN_V2_DLYREQ_FILT_LEN        = 0x0030; ///< Length of the filtering of delay request packets
    static const uint16_t PKTGEN_V2_IP_OFFSETS             = 0x0034; ///< IP_HDR_OFF, IP_CHECK_OFF, IP_DEST_OFF, IP_SRC_OFF
    static const uint16_t PKTGEN_V2_UDP_OFFSETS            = 0x0038; ///< UDP_HDR_OFF, UDP_CHECK_OFF, UDP_LEN_OFF
    static const uint16_t PKTGEN_V2_PTP_OFFSETS            = 0x003C; ///< PTP_PORTID_OFF, PTP_SEQID_OFF, DLYRESP_REQID_OFF, DLYRESP_RX_TS_OFF
    static const uint16_t PKTGEN_V2_MIXED_OFFSETS          = 0x0040; ///< PTP_CORR_OFF = NTP_START_OFF, NTP_TX_TS_OFF
    static const uint16_t PKTGEN_V2_NTP_OFFSETS            = 0x0044; ///< NTP_REFERENCE_TS_OFF, NTP_ROOT_DISPER_OFF, NTP_REQU_EOP_OFF, NTP_ORIGIN_TS_OFF
    static const uint16_t PKTGEN_V2_NTP_LI_STRATUM         = 0x0048; ///< NTP leap seconds and stratum field
    static const uint16_t PKTGEN_V2_NTP_ROOT_DISPERSION    = 0x004C; ///< NTP root dispersion
    // command registers
    static const uint16_t PKTGEN_V2_CLEAR_PKT_COUNTERS     = 0x0100; ///< Clear all statistic counters
    static const uint16_t PKTGEN_V2_SWITCH_TEMPL_PAGES     = 0x0108; ///< Switch page of the templates
    static const uint16_t PKTGEN_V2_CLEAR_DLYREQ_FIFO      = 0x010C; ///< Clear delay request FIFO
    // statistic counters, status
    static const uint16_t PKTGEN_V2_PKT_STATUS             = 0x0200; ///< TBD
    static const uint16_t PKTGEN_V2_PKT_SYNC_CNT           = 0x0204; ///< Statistic counter: sync packets
    static const uint16_t PKTGEN_V2_PKT_ANNOUNCE_CNT       = 0x0208; ///< Statistic counter: announce packets
    static const uint16_t PKTGEN_V2_PKT_DLYREQ_CNT         = 0x020C; ///< Statistic counter: delay request packets
    static const uint16_t PKTGEN_V2_PKT_DLYRESP_CNT        = 0x0210; ///< Statistic counter: delay response packets
    static const uint16_t PKTGEN_V2_PKT_DLYREQ_DROP_CNT    = 0x0214; ///< Statistic counter: dropped delay request packets
    static const uint16_t PKTGEN_V2_PKT_CPU_DROP_CNT       = 0x0218; ///< Statistic counter: dropped CPU packets
    // RAMs
    // address is calculated with BASE + CHANNEL_OFFSET + RAM_OFFSET + location
    static const uint16_t PKTGEN_V2_SYNC_TMPL              = 0x0000; ///< Sync template
    static const uint16_t PKTGEN_V2_DLYRESP_TMPL           = 0x0800; ///< Delay response template
    static const uint16_t PKTGEN_V2_ANNOUNCE_TMPL          = 0x1000; ///< Announce template
    static const uint16_t PKTGEN_V2_PATTERN_MASK_DLYREQ    = 0x2000; ///< Delay request filter pattern and mask
    static const uint16_t PKTGEN_V2_VLAN_RAM               = 0x3000; ///< VLAN ID to source port id cross reference table

    // PKTGEN V2 bits
    // For register PKT_MODE
    static const uint32_t PKTGEN_V2_MODE_VLAN_EN_BIT       = 0x00000100; ///< global vlan enable
    static const uint32_t PKTGEN_V2_MODE_PKT_NTP_EN        = 0x00000080; ///< NTP mode enable (EN_DLYRESP also needed)
    static const uint32_t PKTGEN_V2_MODE_SA_MC_BIT         = 0x00000040; ///< enable multicast sync and announce
    static const uint32_t PKTGEN_V2_MODE_NTP_MC_BIT        = 0x00000020; ///< enable NTP multicast
    static const uint32_t PKTGEN_V2_MODE_EN_DLYRESP        = 0x00000010; ///< enable delay response generator
    static const uint32_t PKTGEN_V2_MODE_EN_ANN            = 0x00000008; ///< enable announce generator
    static const uint32_t PKTGEN_V2_MODE_EN_SYNC           = 0x00000004; ///< enable sync generator
    static const uint32_t PKTGEN_V2_MODE_PF_BIT            = 0x00000003; ///< packet format bits
    static const uint32_t PKTGEN_V2_MODE_PF_IPV4           = 0x00000000; ///< setting for IPV4
    static const uint32_t PKTGEN_V2_MODE_PF_IPV6           = 0x00000001; ///< setting for IPV6
    static const uint32_t PKTGEN_V2_MODE_PF_LAYER2         = 0x00000002; ///< setting for Layer2
    // For register PKTGEN_V2_CLEAR_DLYREQ_FIFO
    static const uint16_t PKTGEN_V2_CLEAR_DLYREQ_BIT       = 0x0001; ///< clear delay request fifo
    // For register PKTGEN_V2_SWITCH_TEMPL_PAGES
    static const uint32_t PKTGEN_V2_SWITCH_DLY_TEMPL_BIT  = 1 << 16; ///< switch delay template
    static const uint32_t PKTGEN_V2_SWITCH_ANN_TEMPL_BIT  = 1 << 8; ///< switch announce template
    static const uint32_t PKTGEN_V2_SWITCH_SYNC_TEMPL_BIT = 1 << 0; ///< switch sync template

#endif
    ///@} End of Pktgen registers

    ///@{
    ///@name Virtual Registers

    /// @{
    /// @name Common virtual registers
    static const uint16_t VR_CLK_FREQ     = 0x1000; ///< Syn1588(R) CSC frequency [Hz]
    static const uint16_t VR_ETH_VERSION  = 0x1004; ///< Version ID of the Ethernet MAC
    static const uint16_t VR_ETH_BUILD    = 0x1008; ///< Build ID of the design
    static const uint16_t VR_PCI_REVISION = 0x100C; ///< PCI Revision of PCIe NIC

    /// @}

    /// @{
    /// @name MAC Timestamper registers
    /// @brief The following registers are implemented in the driver.

    static const uint16_t VR_ETH_TSFIFORX      = 0x1010; ///< MAC RX Timestamper FIFO data
    static const uint16_t VR_ETH_TSFIFOTX      = 0x1014; ///< MAC TX Timestamper FIFO data
    static const uint16_t VR_ETH_TSFIFO_STATUS = 0x1018; ///< MAC Timestamp FIFO counter and full/empty flags R

    /// @} End of MAC Timestamper registers

    ///@{
    /// @name Redirected MAC TS Registers
    /// @brief The following registers are redirected in the driver to the appropriate TS registers in the MAC.

    static const uint16_t VR_ETH_TSTXCTRL      = 0x1020; ///< MAC Timestamper RX Control
    static const uint16_t VR_ETH_TSRXCTRL      = 0x1024; ///< MAC Timestamper TX Control
    static const uint16_t VR_ETH_PHY_DELAY_TX  = 0x1028; ///< Correction values for compensating PHY transmit delay
    static const uint16_t VR_ETH_PHY_DELAY_RX  = 0x102C; ///< Correction values for compensating PHY receive delay
    static const uint16_t VR_ETH_TIMECTRL      = 0x1030; ///< Controls operation of the MAC timestamper
    static const uint16_t VR_ETH_ONESTEPCTRL   = 0x1034; ///< Allows to set different modes for MAC one step timestamper
    static const uint16_t VR_ETH_TS_RXMEM      = 0x1100; ///< Base address of MAC Timestamper RX Pattern/Mask Memory
    static const uint16_t VR_ETH_TS_TXMEM      = 0x1300; ///< Base address of MAC Timestamper TX Pattern/Mask Memory
    /// redirected registers for the 2nd MAC TS unit
    static const uint16_t VR_ETH_TS2_TXCTRL      = 0x1040; ///< 2nd MAC Timestamper RX Control
    static const uint16_t VR_ETH_TS2_RXCTRL      = 0x1044; ///< 2nd MAC Timestamper TX Control
    //static const uint16_t VR_ETH_PHY_DELAY_TX  = 0x1028; ///< Correction values for compensating PHY transmit delay
    //static const uint16_t VR_ETH_PHY_DELAY_RX  = 0x102C; ///< Correction values for compensating PHY receive delay
    //static const uint16_t VR_ETH_TIMECTRL      = 0x1030; ///< Controls operation of the MAC timestamper
    static const uint16_t VR_ETH_TS2_ONESTEPCTRL = 0x1054; ///< Allows to set different modes for 2nd MAC one step timestamper
    static const uint16_t VR_ETH_TS2_RXMEM       = 0x1200; ///< Base address of 2nd MAC Timestamper RX Pattern/Mask Memory
    static const uint16_t VR_ETH_TS2_TXMEM       = 0x1400; ///< Base address of 2nd MAC Timestamper TX Pattern/Mask Memory

    static const uint32_t ETH_VERSION_XO_MASK = 0x00004000; ///< Mask to identify OCXO/TCXO adapter

    static const uint16_t TS_RXMEM_SIZE       = 0x100;      ///< RX Timestamper memory size
    static const uint16_t TS_TXMEM_SIZE       = 0x100;      ///< TX Timestamper memory size

    ///@} End of redirected MAC TS registers

    ///@{
    /// @name MAC error counter
    /// @brief The following registers a redirected to the MAC to get access to the
    /// error counters.

    static const uint16_t VR_ETH_RXERR         = 0x1500; ///< Virtual copy of MAC_ETH_CNT_RXERR
    static const uint16_t VR_ETH_FCSERR        = 0x1504; ///< Virtual copy of MAC_ETH_CNT_FCSERR
    static const uint16_t VR_ETH_MCASTERR      = 0x1508; ///< Virtual copy of MAC_ETH_CNT_MCASTERR
    static const uint16_t VR_ETH_LCOLLERR      = 0x150C; ///< Virtual copy of MAC_ETH_CNT_LCOLLERR
    static const uint16_t VR_ETH_LENERR        = 0x1510; ///< Virtual copy of MAC_ETH_CNT_LENERR
    static const uint16_t VR_ETH_OVERERR       = 0x1514; ///< Virtual copy of MAC_ETH_CNT_OVERERR
    static const uint16_t VR_ETH_FRAMEERR      = 0x1518; ///< Virtual copy of MAC_ETH_CNT_FRAMEERR
    static const uint16_t VR_ETH_CNT_RX        = 0x151C; ///< Virtual copy of MAC_ETH_CNT_CNT_RX
    static const uint16_t VR_ETH_CNT_TX        = 0x1520; ///< Virtual copy of MAC_ETH_CNT_CNT_TX
    static const uint16_t VR_ETH_CNT_VLANERR   = 0x1524; ///< Virtual copy of MAC_CNT_VLANERR
    static const uint16_t VR_ETH_CNT_VLAN      = 0x1528; ///< Virtual copy of MAC_CNT_VLAN
    static const uint16_t VR_ETH_CNT_BROADCAST = 0x152C; ///< Virtual copy of MAC_CNT_BROADCAST
    static const uint16_t VR_ETH_CNT_MULTICAST = 0x1530; ///< Virtual copy of MAC_CNT_MULTICAST
    static const uint16_t VR_ETH_CNT_RX_TS     = 0x1534; ///< Virtual copy of MAC_CNT_RX_TS
    static const uint16_t VR_ETH_CNT_TX_TS     = 0x1538; ///< Virtual copy of MAC_CNT_TX_TS
    static const uint16_t VR_ETH_CNT_RX_TS2    = 0x153C; ///< Virtual copy of MAC_CNT_RX_TS2
    static const uint16_t VR_ETH_CNT_TX_TS2    = 0x1540; ///< Virtual copy of MAC_CNT_TX_TS2

    ///@} End of redirected MAC error counters

    ///@{
    /// @name MAC Capability registers
    static const uint16_t VR_ETH_CAPABILITIES_0 = 0x1544; ///< Virtual copy of MAC_CAPABILITIES_0
    static const uint16_t VR_ETH_MIICOMMAND     = 0x1548; ///< Virtual copy of MAC_MIICOMMAND
    static const uint16_t VR_ETH_SFP_EEPROM     = 0x154C; ///< Virtual copy of MAC_SFP_EEPROM
    static const uint16_t VR_IFG_CTRL           = 0x155C; ///< Virtual copy of MAC_IFG_CTRL

    ///@} End of redirected MAC capability registers

    ///@cond INTERNAL
    ///@{
    /// @name Customer-specific reserved registers
    /// @note The following registers are used by customers and are therefore reserved.
    static const uint16_t VR_AST_AUXR       = 0x1800; ///< MBG
    static const uint16_t VR_TSU_SLOT_ID    = 0x1804; ///< MBG
    static const uint16_t VR_TSU_PHY_INFO   = 0x1808; ///< MBG
    static const uint16_t VR_AST_RESET_CNTL = 0x180C; ///< MBG
    static const uint16_t VR_AST_MUX_CNTL   = 0x1810; ///< MBG

    ///@} End of redirected registers
    ///@endcond

    ///@{
    /// @name lSync registers
    /// @brief virtual registers for lSync synchronization
    static const uint16_t VR_SYSTS_L  = 0x1600; ///< Nanoseconds of system timestamp
    static const uint16_t VR_SYSTS_H  = 0x1604; ///< Seconds of system timestamp
    static const uint16_t VR_HWTS_L   = 0x1608; ///< Nanoseconds of hardware timestamp
    static const uint16_t VR_HWTS_H   = 0x160C; ///< Seconds of hardware timestamp
    static const uint16_t VR_DELTASYS = 0x1610; ///< Clock cycles to draw system timestamp
    static const uint16_t VR_DELTAHW  = 0x1614; ///< Clock cycles to draw hardware timestamp

    ///@} End of lSyncRegisters


#ifdef _USE_PKTGEN_
    ///@{
    ///@name VIP PKT GEN registers
    /// The following register addresses are used for accessing
    /// the XRAM interface of the packet generator integrated in
    /// the syn1588 VIP.

    static const uint16_t  VIP_PKTGEN_CTRL        = 0x1A00;  ///< Virtual copy of VIP XRAM register PKT_GEN_CTRL (8 bit)
    static const uint16_t  VIP_PKTGEN_SL_RAM_LINE = 0x1A04;  ///< Virtual copy of VIP XRAM register PKT_GEN_SL_RAM_LINE (16 bit)
    static const uint16_t  VIP_PKTGEN_DATA        = 0x1A08;  ///< Virtual copy of VIP XRAM register PKT_GEN_DATA (8 bit)
    static const uint16_t  VIP_PKTGEN_SYNC_SEQID  = 0x1A0C;  ///< Virtual copy of VIP XRAM register PKT_GEN_SYNC_SEQID (16 bit)
    static const uint16_t  VIP_PKTGEN_ANN_SEQID   = 0x1A10;  ///< Virtual copy of VIP XRAM register PKT_GEN_ANN_SEQID (16 bit)
    static const uint16_t  VIP_PKTGEN_MODE        = 0x1A14;  ///< Virtual copy of VIP XRAM register PKT_GEN_PKT_MODE (8 bit)
    static const uint16_t  VIP_PKTGEN_MAX_RATE    = 0x1A18;  ///< Virtual copy of VIP XRAM register PKT_GEN_MAX_SYNC_RATE(8 bit)
    static const uint16_t  VIP_PKTGEN_SLAVE_LIST  = 0x1A1C;  ///< Virtual copy of VIP XRAM register PKT_GEN_SLAVE_LIST (16 bit)
    static const uint16_t  VIP_PKTGEN_ANN_LEN     = 0x1A20;  ///< Virtual copy of VIP XRAM register PKT_GEN_ANN_LEN (8 bit)
    static const uint16_t  VIP_PKTGEN_STATUS      = 0x1A24;  ///< Virtual copy of VIP XRAM register PKT_GEN_STATUS (8 bit)

    ///@} End of VIP PKT GEN registers
#endif

    ///@} End of Virtual Registers

    ///@{
    ///@name SPI registers

    static const uint16_t SPI_PLL_CTRL            = 0x2000; ///< SPI controller PLL control
    static const uint16_t SPI_PLL_DATA            = 0x2004; ///< SPI controller PLL data
    static const uint16_t SPI_CONFIG_CTRL         = 0x2040; ///< SPI controller Config control
    static const uint16_t SPI_CONFIG_DATA         = 0x2044; ///< SPI controller Config data
    static const uint16_t SPI_DAC_CTRL            = 0x2080; ///< SPI controller DAC control
    static const uint16_t SPI_DAC_DATA            = 0x2084; ///< SPI controller DAC data
    static const uint32_t SPI_RESET_DISABLE       = 0x20000; ///< Write to SPI_PLL_CTRL to disable SPI controller reset

    ///@} End of SPI Registers

    ///@{
    ///@name PLL Sequencer registers

    static const uint16_t PLL_SEQ_BASE            = 0x2008; ///< PLL Sequencer base address
    static const uint16_t PLL_SEQ_DATA            = 0x200C; ///< PLL Sequencer data address
    #define               PLL_SEQ_LENGTH            255     ///< number of internal registers of PLL sequencer

    static const uint32_t PLL_SEQ_RD              = 0x1 << 24;  ///< read command
    static const uint32_t PLL_SEQ_WR              = 0x2 << 24;  ///< write command
    static const uint32_t PLL_SEQ_RESET           = 0x1 << 19;  ///< reset bit (write to base address)
    static const uint32_t PLL_SEQ_START           = 0x1 << 16;  ///< start bit (write to base address)
    static const uint32_t PLL_SEQ_BUSY            = 0x1 << 17;  ///< busy flag (read from base address)
    static const uint32_t PLL_SEQ_ERROR           = 0x1 << 18;  ///< error flag (read from base address)

    ///@} End of PLL Sequencer registers

    ///@{
    ///@name I2C EEPROM registers
    ///@todo Attention: this overlaps with the external TS mem range, IFF both rev 2.1+ NIC and 10G TS are used together
    static const uint16_t I2C_EEPROM_0_MAC_0      = 0x2100; ///< I2C EEPROM: MAC address upper  four bytes (8|9|10|11)
    static const uint16_t I2C_EEPROM_1_MAC_1      = 0x2104; ///< I2C EEPROM: MAC address middle four bytes (4|5|6|7)
    static const uint16_t I2C_EEPROM_2_MAC_2      = 0x2108; ///< I2C EEPROM: MAC address lower  four bytes (0|1|2|3)
    static const uint16_t I2C_EEPROM_3_BOARD_REV  = 0x210C; ///< I2C EEPROM: Board revision (major|minor|subminor|subsubminor)
    static const uint16_t I2C_EEPROM_4_SER_NUM_0  = 0x2110; ///< I2C EEPROM: serial number (3|2|1|0)
    static const uint16_t I2C_EEPROM_5_SER_NUM_1  = 0x2114; ///< I2C EEPROM: serial number (7|6|5|4)
    static const uint16_t I2C_EEPROM_6_SER_NUM_2  = 0x2118; ///< I2C EEPROM: serial number (11|10|9|8)
    static const uint16_t I2C_EEPROM_7_SER_NUM_3  = 0x211C; ///< I2C EEPROM: serial number (15|14|13|12)
    static const uint16_t I2C_EEPROM_8_BOARD_CAP_0 = 0x2120; ///< I2C EEPROM: Board capability (3|2|1|0)
    static const uint16_t I2C_EEPROM_9_BOARD_CAP_1 = 0x2124; ///< I2C EEPROM: Board capability (7|6|5|4)

    ///@} End of I2C EEPROM Registers

    ///@{
    ///@name I2C Video DAC registers

    static const uint16_t I2C_VIDEO_PRE_LO        = 0x20C0; ///< I2C VIDEO DAC Prescaler low
    static const uint16_t I2C_VIDEO_PRE_HI        = 0x20C4; ///< I2C VIDEO DAC Prescaler high
    static const uint16_t I2C_VIDEO_CTRL          = 0x20C8; ///< I2C VIDEO DAC Control
    static const uint16_t I2C_VIDEO_TX_RX         = 0x20CC; ///< I2C VIDEO DAC Transmit/Receive
    static const uint16_t I2C_VIDEO_CMD_STATUS    = 0x20D0; ///< I2C VIDEO DAC Command/Status
    static const uint16_t I2C_VIDEO_ADRR_H        = 0x20FF; ///< I2C VIDEO DAC LAST ADDRESS
    // Commands to the Video DAC
    // VIDEO DAC ADV7391
    static const uint16_t I2C_VIDEO_SLAVE_ADDR_W  = 0x0054; ///< I2C VIDEO DAC SLAVE ADDRESS while Write
    static const uint16_t I2C_VIDEO_SLAVE_ADDR_R  = 0x0055; ///< I2C VIDEO DAC SLAVE ADDRESS while Read
    static const uint16_t I2C_VIDEO_WRITE         = 0x0010; ///< I2C VIDEO DAC Write to Slave
    static const uint16_t I2C_VIDEO_READ          = 0x0020; ///< I2C VIDEO DAC Read from Slave
    static const uint16_t I2C_VIDEO_START         = 0x0090; ///< I2C VIDEO DAC START CMD to Slave
    static const uint16_t I2C_VIDEO_STOP          = 0x0040; ///< I2C VIDEO DAC STOP CMD to Slave
    static const uint16_t I2C_VIDEO_RD_NACK_STO   = 0x0068; ///< I2C VIDEO DAC Send RD+NACK+STOP to Slave
    static const uint16_t I2C_VIDEO_NACK_STO      = 0x0048; ///< I2C VIDEO DAC Send NACK+STOP to Slave
    // Flags for STATUS REG
    static const uint32_t I2C_VIDEO_RX_NACK       = 0x00000080; ///< I2C VIDEO DAC NOT Reveived ACK Bit Set
    static const uint32_t I2C_VIDEO_RX_ACK        = 0x00000000; ///< I2C VIDEO DAC Reveived ACK Bit Set
    static const uint32_t I2C_VIDEO_BUSY          = 0x00000040; ///< I2C VIDEO DAC BUSY
    static const uint32_t I2C_VIDEO_TIP           = 0x00000002; ///< I2C VIDEO DAC Transfer in Progress
    static const uint32_t I2C_VIDEO_IF            = 0x00000001; ///< I2C VIDEO DAC Interrupt Flag-Byte received, AL

    ///@} End of I2C Video DAC Registers

#ifdef _SYN1588_EXT_TS_REGS_
    ///@{
    ///@name Extended Timestamper registers
    /// These timestampers are not part of the default syn1588 Clock_M IP core.
    /// They require multiple network interfaces (syn1588 Clock_MX).

    static const uint16_t MIITS2_RXMEM            = 0x2100; ///< Generic Receive Timestamper 2 mask/pattern memory
    static const uint16_t MIITS2_TXMEM            = 0x2200; ///< Generic Transmit Timestamper 2 mask/pattern memory
    static const uint16_t MIITS3_RXMEM            = 0x2300; ///< Generic Receive Timestamper 3 mask/pattern memory
    static const uint16_t MIITS3_TXMEM            = 0x2400; ///< Generic Transmit Timestamper 3 mask/pattern memory
    static const uint16_t MIITS4_RXMEM            = 0x2500; ///< Generic Receive Timestamper 4 mask/pattern memory
    static const uint16_t MIITS4_TXMEM            = 0x2600; ///< Generic Transmit Timestamper 4 mask/pattern memory
    static const uint16_t MIITS5_RXMEM            = 0x2700; ///< Generic Receive Timestamper 5 mask/pattern memory
    static const uint16_t MIITS5_TXMEM            = 0x2800; ///< Generic Transmit Timestamper 5 mask/pattern memory

    static const uint16_t MIITS2_TXCTRL           = 0x2900; ///< Generic Transmit Timestamper 2 control register
    static const uint16_t MIITS2_RXCTRL           = 0x2904; ///< Generic Receive Timestamper 2 control register
    static const uint16_t MIITS2_FIFO_TX          = 0x2908; ///< Transmit timestamp FIFO 2 data
    static const uint16_t MIITS2_FIFO_RX          = 0x290C; ///< Receive timestamp FIFO 2 data
    static const uint16_t MIITS2_FIFO_STATUS      = 0x2910; ///< TSFIFO 2 counter and full/empty flags for RX & TX
    static const uint16_t MIITS2_PHY_DELAY_TX     = 0x2914; ///< Correction values for compensating PHY 2 transmit delay
    static const uint16_t MIITS2_PHY_DELAY_RX     = 0x2918; ///< Correction values for compensating PHY 2 receive delay
    static const uint16_t MIITS2_SPEEDSEL         = 0x291C; ///< Link speed selection of PHY2/Timestamper 2
    static const uint16_t MIITS2_ONESTEPCTRL      = 0x2920; ///< Control the 1-step timestamping function for Timestamper 2

    static const uint16_t MIITS3_TXCTRL           = 0x2940; ///< Generic Transmit Timestamper 3 control register
    static const uint16_t MIITS3_RXCTRL           = 0x2944; ///< Generic Receive Timestamper 3 control register
    static const uint16_t MIITS3_FIFO_TX          = 0x2948; ///< Transmit timestamp FIFO 3 data
    static const uint16_t MIITS3_FIFO_RX          = 0x294C; ///< Receive timestamp FIFO 3 data
    static const uint16_t MIITS3_FIFO_STATUS      = 0x2950; ///< TSFIFO 3 counter and full/empty flags for RX & TX
    static const uint16_t MIITS3_PHY_DELAY_TX     = 0x2954; ///< Correction values for compensating PHY 3 transmit delay
    static const uint16_t MIITS3_PHY_DELAY_RX     = 0x2958; ///< Correction values for compensating PHY 3 receive delay
    static const uint16_t MIITS3_SPEEDSEL         = 0x295C; ///< Link speed selection of PHY2/Timestamper 3
    static const uint16_t MIITS3_ONESTEPCTRL      = 0x2960; ///< Control the 1-step timestamping function for Timestamper 3

    static const uint16_t MIITS4_TXCTRL           = 0x2980; ///< Generic Transmit Timestamper 4 control register
    static const uint16_t MIITS4_RXCTRL           = 0x2984; ///< Generic Receive Timestamper 4 control register
    static const uint16_t MIITS4_FIFO_TX          = 0x2988; ///< Transmit timestamp FIFO 4 data
    static const uint16_t MIITS4_FIFO_RX          = 0x298C; ///< Receive timestamp FIFO 4 data
    static const uint16_t MIITS4_FIFO_STATUS      = 0x2990; ///< TSFIFO 4 counter and full/empty flags for RX & TX
    static const uint16_t MIITS4_PHY_DELAY_TX     = 0x2994; ///< Correction values for compensating PHY 4 transmit delay
    static const uint16_t MIITS4_PHY_DELAY_RX     = 0x2998; ///< Correction values for compensating PHY 4 receive delay
    static const uint16_t MIITS4_SPEEDSEL         = 0x299C; ///< Link speed selection of PHY2/Timestamper 4
    static const uint16_t MIITS4_ONESTEPCTRL      = 0x29A0; ///< Control the 1-step timestamping function for Timestamper 4

    static const uint16_t MIITS5_TXCTRL           = 0x29C0; ///< Generic Transmit Timestamper 5 control register
    static const uint16_t MIITS5_RXCTRL           = 0x29C4; ///< Generic Receive Timestamper 5 control register
    static const uint16_t MIITS5_FIFO_TX          = 0x29C8; ///< Transmit timestamp FIFO 5 data
    static const uint16_t MIITS5_FIFO_RX          = 0x29CC; ///< Receive timestamp FIFO 5 data
    static const uint16_t MIITS5_FIFO_STATUS      = 0x29D0; ///< TSFIFO 5 counter and full/empty flags for RX & TX
    static const uint16_t MIITS5_PHY_DELAY_TX     = 0x29D4; ///< Correction values for compensating PHY 5 transmit delay
    static const uint16_t MIITS5_PHY_DELAY_RX     = 0x29D8; ///< Correction values for compensating PHY 5 receive delay
    static const uint16_t MIITS5_SPEEDSEL         = 0x29DC; ///< Link speed selection of PHY2/Timestamper 5
    static const uint16_t MIITS5_ONESTEPCTRL      = 0x29E0; ///< Control the 1-step timestamping function for Timestamper 5

    ///@} End of Extended Timestamper registers
#endif

    ///@} End of Registers

    ///@{
    ///@name Constants for Event representation
    static const uint32_t EVENT_IN0 = 0x0400;     ///< Event 0 Input Interrupt
    static const uint32_t EVENT_IN1 = 0x0800;     ///< Event 1 Input Interrupt
    static const uint32_t PERIOD0   = 0x01000000; ///< Periodtimer 0 Interrupt
    static const uint32_t PERIOD1   = 0x02000000; ///< Periodtimer 1 Interrupt

    ///@}

    ///@{
    ///@name Time Control Register bits
    static const uint32_t TICB_LSHDWREG  =     0x00000001; ///< immediate load of all registers with shadow value
    static const uint32_t TICB_EN1STEP   =     0x00000002; ///< enable 1 step timestamper
    static const uint32_t TCIB_VLAN      =     0x00000004; ///< enable VLAN (only necessary for 1 step)
    static const uint32_t TCIB_IPV6      =     0x00000008; ///< set to 1 for IPv6, else IPv4 (only necessary for 1 step)
    static const uint32_t TCIB_NOTXTS    =     0x00000010; ///< Disable activation of timestamped packet flag in TX BD word
    static const uint32_t TCIB_NTPTS     =     0x00010000; ///< Enable (1) NTP timestamp format for all timestampers. Otherwise PTP format (0).
    static const uint32_t TCIB_STOPCLOCK =     0x10000000; ///< 1 enables the stop clock function for a number of clock cycles given in SHDWSTEPP_NS.
    static const uint32_t TICB_LSTEP     =     0x20000000; ///< load pure phase step size register with shadow value
    static const uint32_t TICB_ADDTIME   =     0x40000000; ///< add time in SHDWSTEPP_NS (interpreted as unsigned 30 bit) to current time
    static const uint32_t TICB_LTIME     =     0x80000000; ///< load time register with shadow value

    ///@}

    ///@{
    ///@name Event Control Register bits
    static const uint32_t IRQB_EVENT0       = 0x00000400; ///< event 0 interrupt bit
    static const uint32_t EVCB_EVENT0       = 0x00000001; ///< event 0 control bit
    static const uint32_t EVCB_EVENT1       = 0x00000002; ///< event 1 control bit
    static const uint32_t EVCB_TRIGGER0     = 0x00000004; ///< trigger 0 control bit
    static const uint32_t EVCB_TRIGGER1     = 0x00000008; ///< trigger 1 control bit
    static const uint32_t EVCB_PERIOD0      = 0x00000010; ///< period 0 control bit
    static const uint32_t EVCB_PERIOD1      = 0x00000020; ///< period 1 control bit
    static const uint32_t EVCB_PERIOD0_OUT  = 0x00000040; ///< period 0 output control bit
    static const uint32_t EVCB_PERIOD1_OUT  = 0x00000080; ///< period 1 output control bit
    static const uint32_t EVCB_PERIOD0_INIT = 0x00000100; ///< initial period 0 value
    static const uint32_t EVCB_PERIOD0_SET  = 0x00000200; ///< set period 0
    static const uint32_t EVCB_PERIOD1_INIT = 0x00000400; ///< initial period 1 value
    static const uint32_t EVCB_PERIOD1_SET  = 0x00000800; ///< set period 1
    static const uint32_t EVCB_IRIGB        = 0x00001000; ///< IRIG-B control bit
    static const uint32_t EVCB_PER0_FROM_TRIG0 = 0x18000; ///< start PER0 with TRIG0
    static const uint32_t EVCB_PER0_FROM_TRIG1 = 0x08000; ///< start PER0 with TRIG1
    static const uint32_t EVCB_PER1_FROM_TRIG0 = 0x60000; ///< start PER1 with TRIG0
    static const uint32_t EVCB_PER1_FROM_TRIG1 = 0x20000; ///< start PER1 with TRIG1
    static const uint32_t EVCB_PERIOD2      = 0x00080000; ///< period 2 control bit
    static const uint32_t EVCB_PERIOD3      = 0x00100000; ///< period 3 control bit

    ///@}

    ///@{
    ///@name Trigger Time register bits/masks
    static const uint32_t TRGT_SECOND_FIELD_BITMASK = 0x000FFFFF; ///< size of the bitfield reserved to set the trigger point [s]
    static const uint32_t TRGT_SET_TO_0a            = 0x00000000; ///< set trigger output to '0' at trigger point
    static const uint32_t TRGT_SET_TO_1             = 0x00100000; ///< set trigger output to '1' at trigger point
    static const uint32_t TRGT_SET_TO_TOGGLE        = 0x00200000; ///< set trigger output to toggle at trigger point
    static const uint32_t TRGT_SET_TO_0b            = 0x00300000; ///< set trigger output to '0' at trigger point

    ///@}

    ///@{
    ///@name FIFO Status bits
    static const uint32_t RXFEMPTY = 0x00000001; ///< rxfifo empty status bit
    static const uint32_t RXFFULL  = 0x00000002; ///< rxfifo full status bit
    static const uint32_t TXFEMPTY = 0x00010000; ///< txfifo empty status bit
    static const uint32_t TXFFULL  = 0x00020000; ///< txfifo full status bit
    static uint32_t RXFCNT(const uint32_t value) { return ((value) & 0x0000FFFC) >> 2; }
    static uint32_t TXFCNT(const uint32_t value) { return ((value) & 0xFFFC0000) >> 18; }

    ///@}

    ///@{
    ///@name Interrupt Enable bits for syn1588 Clock M timestampers
    static const uint8_t IREN_MIITS0_RX = 0x01; ///< enable miits 0 rx interrupt bit
    static const uint8_t IREN_MIITS0_TX = 0x04; ///< enable miits 0 tx interrupt bit
    static const uint8_t IREN_MIITS1_RX = 0x10; ///< enable miits 1 rx interrupt bit
    static const uint8_t IREN_MIITS1_TX = 0x40; ///< enable miits 1 tx interrupt bit

    ///@}

    ///@{
    ///@name Genlock/SDI specific configuration bits
    static const uint32_t FRAC_CTRL           =    0x80000000; ///< controls fractional period generation properties
    static const uint32_t GCTRL_PREC0         =    0x80000000; ///< Genlock control: Enable Precision Period0
    static const uint32_t GCTRL_PREC1         =    0x40000000; ///< Genlock control: Enable Precision Period1
    static const uint32_t GCTRL_HSYNC         =    0x20000000; ///< Genlock control: Enable HSync Output at Period_o(0)
    static const uint32_t GCTRL_PRE_EV0       =    0x10000000; ///< Genlock control: Use Genlock Predivider(Bit6:0) for Event0 input
    static const uint32_t GCTRL_PRE_EV0_TRIG1 =    0x08000000; ///< Genlock control: Start Genlock Predivider(Bit6:0) for Event0 input with trigger1
    static const uint32_t GCTRL_SDIENBL       =    0x04000000; ///< Genlock control: Start SDI Counter
    ///@}

    ///@{
    ///@name Extended SDI signal specific configuration bits
    static const uint32_t SDNA_BITMASK_DELTA_SDI_NSEC = 0x3FFFFFF; ///< mask for the DELTA_SDI_NSEC bitfield
    static const uint32_t SDNA_BIT_SDI_EXTENDED_GEN_ACTIVE = 0x40000000; ///< bit set by HW when the extended SDI signal generator is active
    static const uint32_t SDNA_BIT_ENABLE_SDI_EXTENDED_GEN = 0x80000000; ///< bit to enable(1)/disable(0)) the extended SDI signal generator
    ///@}

    ///@{
    ///@name Audio specific configuration bits
    static const uint32_t ADNA_BITMASK_DELTA_AUDIO_NSEC = 0xFFFFFF; ///< mask for the DELTA_AUDIO_NSEC bitfield
    static const uint32_t ADNA_BIT_ENABLE_AUDIO_ALIGNMENT = 0x80000000; ///< bit to enable the Audio signal generator
    ///@}

    ///@{
    ///@name SDI LTC specific configuration bits
    static const uint32_t LTCC_BIT_ENABLE_SDILTC    = 0x00000001; ///< bit to enable the SDI LTC signal generator
    static const uint32_t LTCC_BIT_FRCOUNT_MAX_30   = 0x00000000; ///< value to set frame counter maximum for 30 Hz
    static const uint32_t LTCC_BIT_FRCOUNT_MAX_25   = 0x00000004; ///< value to set frame counter maximum for 25 Hz
    static const uint32_t LTCC_BIT_FRCOUNT_MAX_24   = 0x00000008; ///< value to set frame counter maximum for 24 Hz
    static const uint32_t LTCC_BIT_COUNT_FRAMEPAIRS = 0x00000010; ///< count frame pairs (1) or frames (0)
    ///@}

    ///@{
    ///@name Auxiliary register configuration bits
    static const uint32_t AUX_CLK_SEL      =  0x00000001;  ///< Select local TCXO/OCXO or external clock
    static const uint32_t AUX_DDR_HD       =  0x00000002;  ///< Enable DDR when in HD Video mode
    static const uint32_t AUX_EXTCLK_SRC   =  0x00000004;  ///< Select external clock source (external input or PHY)
    static const uint32_t AUX_PHYCLK_SRC   =  0x00000008;  ///< Select the base clock for the PHY (local or syn1588)
    static const uint32_t AUX_CLK_STATUS   =  0x00010000;  ///< State of clock mux (local or external clock selected)
    ///@}

    ///@{
    ///@name PTP_SYNC_STATUS register content
    static const uint32_t LSS_BIT_LSYNC_RUNNING           = 0x40000000; ///< bit signalling that lSync is active
    ///@}

    ///@{
    ///@name PTP_SYNC_STATUS register content
    static const uint32_t PSS_BITMASK_UTC_OFFSET          = 0x000000FF; ///< bitfield mask for UTC Offset in [s]
    static const uint32_t PSS_BIT_UTC_OFFSET_VALID        = 0x00000100; ///< bit signalling of UTC Offset is valid or not
    static const uint32_t PSS_BIT_LEAP59                  = 0x00000200; ///< bit signalling an upcoming negative leap second
    static const uint32_t PSS_BIT_LEAP61                  = 0x00000400; ///< bit signalling an upcoming positive leap second
    static const uint32_t PSS_BITMASK_PTP_STATE           = 0x00007800; ///< bitfield mask for the field representing the PTP State
    static const uint32_t PSS_SHIFT_PTP_STATE             = 11;         ///< shift offset for the field representing the PTP State
    static const uint32_t PSS_BIT_PTP_INSYNC              = 0x00008000; ///< bit signalling that the PTP Stack is in-sync
    static const uint32_t PSS_BITMASK_PTP_INSYNC_BND_CODE = 0x003F0000; ///< bitfield mask for the code indicating the insync boundary, see ::PTP_STATE_RANGE_TABLE_INIT
    static const uint32_t PSS_SHIFT_PTP_INSYNC_BND_SHIFT  = 16;         ///< shift offset for the code indicating the insync boundary
    static const uint32_t PSS_BIT_PTP_TIMESCALE           = 0x00400000; ///< bit signalling the current PTP Timescale ('1' = PTP, '0' = ARB)
    static const uint32_t PSS_BIT_LSYNC_RUNNING           = LSS_BIT_LSYNC_RUNNING; ///< only writeable via LSYNC STATUS register
    static const uint32_t PSS_BIT_VALID                   = 0x80000000; ///< bit signalling that the register content is valid
    ///@}

    ///@{
    ///@name syn1588 Capability bits.
    /// \a Not the same as PTP_ClockIfc capabilities!

    static const uint32_t CAP1_MIITS0    =    0x00000001; ///< MIITS0 implemented
    static const uint32_t CAP1_MIITS1    =    0x00000002; ///< MIITS1 implemented
    static const uint32_t CAP1_ULTIMATE  =    0x00000004; ///< Ultimate timestamper implemented
    static const uint32_t CAP1_MEASURE   =    0x00000008; ///< event 16 for measurement implemented
    static const uint32_t CAP1_EVENT0    =    0x00000010; ///< event 0 implemented
    static const uint32_t CAP1_EVENT1    =    0x00000020; ///< event 1 implemented
    static const uint32_t CAP1_TRIGGER0  =    0x00000040; ///< trigger 0 implemented
    static const uint32_t CAP1_TRIGGER1  =    0x00000080; ///< trigger 1 implemented
    static const uint32_t CAP1_PERIOD0   =    0x00000100; ///< period 0 implemented
    static const uint32_t CAP1_PERIOD1   =    0x00000200; ///< period 1 implemented
    static const uint32_t CAP1_TIME2     =    0x00000400; ///< time 2 registers implemented
    static const uint32_t CAP1_IRIGB     =    0x00000800; ///< IRIG-B output implemented
    static const uint32_t CAP1_PROG1STEP =    0x00001000; ///< programmable one step implemented
    static const uint32_t CAP1_PERIODSET =    0x00002000; ///< PERIOD set and initial value is implemented
    static const uint32_t CAP1_NTPMODE   =    0x00004000; ///< NTP timestamp mode is implemented
    static const uint32_t CAP1_LEAPAPPLY =    0x00008000; ///< leap apply time is implemented
    static const uint32_t CAP1_DUTYCYCLE1=    0x00010000; ///< Duty cycle function for PERIODTIME0 is implemented
    static const uint32_t CAP1_PKTGEN    =    0x00020000; ///< Packetgenerator is implemented
    static const uint32_t CAP1_PKTGENFMT =    0x00040000; ///< Packetgenerator: Slave List Format (12/14 Bytes per line:0, 24/26 Bytes per line: 1)
    static const uint32_t CAP1_GENLOCK   =    0x00080000; ///< Precision Period and Genlock for video applications is implemented
    static const uint32_t CAP1_IRIGBIN   =    0x00100000; ///< IRIG-B input implemented
    static const uint32_t CAP1_PKTGENEXT =    0x00200000; ///< Extended VLAN support in Packetgenerator
    static const uint32_t CAP1_STOPCLOCK =    0x00400000; ///< stop clock for given number of cycles function is implemented (for subtracting offsets)
    static const uint32_t CAP1_HSNETWORKINDIRECT = 0x00800000;  ///< Indirect addressing mode for high-speed network interfaces is implemented
    static const uint32_t CAP1_PERIOD2 = 0x01000000;  ///< Period Light 2 is implemented
    static const uint32_t CAP1_PERIOD3 = 0x02000000;  ///< Period Light 3 is implemented
    static const uint32_t CAP1_EXTTIME = 0x04000000;  ///< ExTime function is implemented
    static const uint32_t CAP1_PTP_SYNC_STATUS = 0x08000000;  ///< PTP_Sync_Status register is implemented
    static const uint32_t CAP1_SDI_EXTENDED_SUPPORT = 0x10000000;  ///< SDI Next Alignment Point implemented
    static const uint32_t CAP1_AUDIO_COUNTER = 0x20000000;  ///< Audio Counter and Audio Next Alignment Point implemented
    static const uint32_t CAP1_SDI_LTC_TIMECODE = 0x40000000;  ///< SDI_LTC_Timecode implemented

#ifdef _SYN1588_EXT_TS_REGS_
    static const uint32_t CAP2_EXT_TS_MIITS2_1G   = 0x00000001; ///< MIITS2 is implemented with 1 Gbit/100 MBit
    static const uint32_t CAP2_EXT_TS_MIITS3_1G   = 0x00000002; ///< MIITS3 is implemented with 1 Gbit/100 MBit
    static const uint32_t CAP2_EXT_TS_MIITS4_1G   = 0x00000004; ///< MIITS4 is implemented with 1 Gbit/100 MBit
    static const uint32_t CAP2_EXT_TS_MIITS5_1G   = 0x00000008; ///< MIITS5 is implemented with 1 Gbit/100 MBit
    static const uint32_t CAP2_EXT_TS_MIITS2_10G  = 0x00000010; ///< MIITS2 is implemented with 10 GBit
    static const uint32_t CAP2_EXT_TS_MIITS3_10G  = 0x00000020; ///< MIITS3 is implemented with 10 GBit
    static const uint32_t CAP2_EXT_TS_MIITS4_10G  = 0x00000040; ///< MIITS4 is implemented with 10 GBit
    static const uint32_t CAP2_EXT_TS_MIITS5_10G  = 0x00000080; ///< MIITS5 is implemented with 10 GBit
    static const uint32_t CAP2_EXT_TS_MIITS2_25G  = 0x00000100; ///< MIITS2 is implemented with 25 GBit
    static const uint32_t CAP2_EXT_TS_MIITS3_25G  = 0x00000200; ///< MIITS3 is implemented with 25 GBit
    static const uint32_t CAP2_EXT_TS_MIITS4_25G  = 0x00000400; ///< MIITS4 is implemented with 25 GBit
    static const uint32_t CAP2_EXT_TS_MIITS5_25G  = 0x00000800; ///< MIITS5 is implemented with 25 GBit
    static const uint32_t CAP2_EXT_TS_MIITS2_40G  = 0x00001000; ///< MIITS2 is implemented with 40 GBit
    static const uint32_t CAP2_EXT_TS_MIITS3_40G  = 0x00002000; ///< MIITS3 is implemented with 40 GBit
    static const uint32_t CAP2_EXT_TS_MIITS4_40G  = 0x00004000; ///< MIITS4 is implemented with 40 GBit
    static const uint32_t CAP2_EXT_TS_MIITS5_40G  = 0x00008000; ///< MIITS5 is implemented with 40 GBit
    static const uint32_t CAP2_EXT_TS_MIITS2_100G = 0x00010000; ///< MIITS2 is implemented with 100 GBit
    static const uint32_t CAP2_EXT_TS_MIITS3_100G = 0x00020000; ///< MIITS3 is implemented with 100 GBit
    static const uint32_t CAP2_EXT_TS_MIITS4_100G = 0x00040000; ///< MIITS4 is implemented with 100 GBit
    static const uint32_t CAP2_EXT_TS_MIITS5_100G = 0x00080000; ///< MIITS5 is implemented with 100 GBit

    static const uint32_t CAP2_EXT_TS_MIITS2_PRESENT =  CAP2_EXT_TS_MIITS2_1G
                                                      | CAP2_EXT_TS_MIITS2_10G
                                                      | CAP2_EXT_TS_MIITS2_25G
                                                      | CAP2_EXT_TS_MIITS2_40G
                                                      | CAP2_EXT_TS_MIITS2_100G;
    static const uint32_t CAP2_EXT_TS_MIITS3_PRESENT =  CAP2_EXT_TS_MIITS3_1G
                                                      | CAP2_EXT_TS_MIITS3_10G
                                                      | CAP2_EXT_TS_MIITS3_25G
                                                      | CAP2_EXT_TS_MIITS3_40G
                                                      | CAP2_EXT_TS_MIITS3_100G;
    static const uint32_t CAP2_EXT_TS_MIITS4_PRESENT =  CAP2_EXT_TS_MIITS4_1G
                                                      | CAP2_EXT_TS_MIITS4_10G
                                                      | CAP2_EXT_TS_MIITS4_25G
                                                      | CAP2_EXT_TS_MIITS4_40G
                                                      | CAP2_EXT_TS_MIITS4_100G;
    static const uint32_t CAP2_EXT_TS_MIITS5_PRESENT =  CAP2_EXT_TS_MIITS5_1G
                                                      | CAP2_EXT_TS_MIITS5_10G
                                                      | CAP2_EXT_TS_MIITS5_25G
                                                      | CAP2_EXT_TS_MIITS5_40G
                                                      | CAP2_EXT_TS_MIITS5_100G;

    static const uint32_t MIITS_SPEEDSEL_100 = 0x01; ///< Network interface speed is 100 MBit
    static const uint32_t MIITS_SPEEDSEL_1G  = 0x02; ///< Network interface speed is 1 GBit
    static const uint32_t MIITS_SPEEDSEL_10G = 0x04; ///< Network interface speed is 10 GBit

    static const uint32_t MIITS_SPEEDSEL_1STEPCAPABLE = MIITS_SPEEDSEL_100
                                                      | MIITS_SPEEDSEL_1G
                                                      | MIITS_SPEEDSEL_10G;
                                                      ///< this is currently hardcoded,
                                                      ///< new FPGAs may make higher speed 1Step units possible
#endif //_SYN1588_EXT_TS_REGS_
    ///@}
    ///@name PCI NIC board capability bits.
    static const uint32_t BOARDCAP_EXTENDED_SDI_LTC_AUDIO_SUPPORT = 0x00000001; ///< Extended SDI, SDI-LTC and Audio signal generation supported by the hardware

    /// @{

    /// @name IOMATRIX Source definitions
    ///@name IOMATRIX Register Bits and Values

    /// @{
    /// @name IOMATRIX Source definitions
    static const uint8_t IOM_SRC_DIS       = 0x0; ///< Source disabled
    static const uint8_t IOM_SRC_SMA0      = 0x1; ///< X4
    static const uint8_t IOM_SRC_SMA1      = 0x4; ///< X7
    static const uint8_t IOM_SRC_SMA2      = 0x3; ///< X6
    static const uint8_t IOM_SRC_SMA3      = 0x2; ///< X5
    static const uint8_t IOM_SRC_PER0      = 0x5; ///< period0_o signal of syn1588 Clock_M
    static const uint8_t IOM_SRC_PER1      = 0x6; ///< period1_o signal of syn1588 Clock_M
    static const uint8_t IOM_SRC_TRG0      = 0x7; ///< trigger0_o signal of syn1588 Clock_M
    static const uint8_t IOM_SRC_TRG1      = 0x8; ///< trigger1_o signal of syn1588 Clock_M
    static const uint8_t IOM_SRC_1PPS      = 0x9; ///< 1pps_o signal of syn1588 Clock_M
    static const uint8_t IOM_SRC_IRIGB     = 0xA; ///< IRIG-B signal of syn1588 Clock_M
    static const uint8_t IOM_SRC_1PPSIRIG  = 0xB; ///< decoded 1PPS signal from IRIG-B input unit
    static const uint8_t IOM_SRC_GPS       = 0xE; ///< 1pps signal of GPS receiver on VIP board
    static const uint8_t IOM_SRC_FRAMESYNC = 0xF; ///< framesync output
    static const uint8_t IOM_SRC_MSK       = 0xF;
    /// @} End of IOMATRIX Source definitions

    /// @{
    /// @name IOMATRIX Destination definitions
    static const uint8_t IOM_DST_SMA0      = 0;   ///< X4
    static const uint8_t IOM_DST_SMA1      = 4;   ///< X7
    static const uint8_t IOM_DST_SMA2      = 8;   ///< X6
    static const uint8_t IOM_DST_SMA3      = 12;  ///< X5
    static const uint8_t IOM_DST_EVT0      = 16;  ///< Event0
    static const uint8_t IOM_DST_EVT1      = 20;  ///< Event1
    static const uint8_t IOM_DST_LVL0      = 24;  ///<
    static const uint8_t IOM_DST_LVL1      = 25;  ///<
    static const uint8_t IOM_DST_LVL2      = 26;  ///<
    static const uint8_t IOM_DST_LVL3      = 27;  ///<
    static const uint8_t IOM_DST_IRIGBIN   = 28;  ///< Source definition for IRIG-B input unit
    static const uint8_t IOM_DST_INVALID   = 255;
    /// @} End of IOATRIX Destination definitions

    ///@} End of IOMATRIX Register Bits and Values

    ///@{
    ///@name IRIGB IN Status Register Bits
    static const uint32_t IRIGB_IN_STAT_DETECT = 0x00000001; ///< 1 if IRIG-B input data stream detected
    static const uint32_t IRIGB_IN_STAT_LOCKED = 0x00000002; ///< 1 if locked to the IRIG-B input data stream
    static const uint32_t IRIGB_IN_STAT_VALID  = 0x00000004; ///< 1 if IRIG-B input data is valid

    ///@}

    ///@{
    ///@name MAC_CAPABILTIES_0 Register bits
    static const uint32_t MAC_CAP0_RX_TIMESTAMPER      = 0x00000001; ///< RX_TIMESTAMPER implemented
    static const uint32_t MAC_CAP0_TX_TIMESTAMPER      = 0x00000002; ///< TX_TIMESTAMPER implemented
    static const uint32_t MAC_CAP0_1STEP_TIMESTAMPER   = 0x00000004; ///< 1-step TIMESTAMPER implemented
    static const uint32_t MAC_CAP0_STAT_COUNTER_LENGTH = 0x00000008; ///< length of statistic counters, 0 = 16bit, 1 = 32bit
    static const uint32_t MAC_CAP0_VLAN_NUM            = 0x00000010; ///< 0 = one VLAN ID, 1 = 4 VLAN IDs
    static const uint32_t MAC_CAP0_VLAN_GROUP_ENABLE   = 0x00000020; ///< VLAN group enable implemented
    static const uint32_t MAC_CAP0_ADD_STAT_COUNTER    = 0x00000040; ///< additional statistic counters for MAC implemented
    static const uint32_t MAC_CAP0_SFP_TRANSCEIVER     = 0x00000080; ///< SFP transceiver module identification implemented
    static const uint32_t MAC_CAP0_10G_MAC             = 0x00000100; ///< 10GBit MAC
    static const uint32_t MAC_CAP0_100_1000_MAC        = 0x00000200; ///< 100/1000 MBit MAC
    static const uint32_t MAC_CAP0_25G_MAC             = 0x00000400; ///< 25GBit MAC
    static const uint32_t MAC_CAP0_FEC_ENABLED         = 0x00000800; ///< FEC function available
  //static const uint32_t MAC_CAP0_RESERVED            = 0x00001000; ///< reserved
    static const uint32_t MAC_CAP0_JUMBO_PACKET_ENABLED= 0x00002000; ///< Jumbo packet support enabled
    static const uint32_t MAC_CAP0_AXI_VAR_BURST       = 0x00004000; ///< AXI type interface with variable burst length
    static const uint32_t MAC_CAP0_MAC_TS2             = 0x00008000; ///< Second timestamper TS2 is implemented (RX+TX+1-Step)
    static const uint32_t MAC_CAP0_FIBER_IFC_STATUS    = 0x00010000; ///< Fiber interface status implemented (only for 10/25 Gbit MAC)
  //static const uint32_t MAC_CAP0_SECURITY_ENABLED    = 0x00020000; ///< reserved (hardware security enabled)

    ///@}

    ///@{
    ///@name MAC_CAPABILTIES_1 Register bits
    static const uint32_t MAC_CAP1_PROGRAMMABLE_IFG    = 0x00000001; ///< Programmable inter-frame gap implemented
    ///@}

    ///@{
    ///@name MAC_MIICOMMAND Register bits
    static const uint8_t MAC_MIICMD_PHY_PRESENT        = 0x08; ///< VIP only
    static const uint8_t MAC_MIICMD_SFP_IFC_PRESENT    = 0x10; ///< SFP ifc detected
    static const uint8_t MAC_MIICMD_NO_SFP_TRANSCEIVER = 0x20; ///< bit set if no SFP transceiver present
    static const uint8_t MAC_MIICMD_SFP_BASEX          = 0x40; ///< 100/1000BASE-X SFP transceiver present
    static const uint8_t MAC_MIICMD_SFP_BASET          = 0x80; ///< 100/1000BASE-T SFP transceiver present

    ///@}

    ///@{
    ///@name SMA ports
    static const uint8_t SMA_PORT_X4 = 0; ///< SMA port 0 (X4)
    static const uint8_t SMA_PORT_X5 = 3; ///< SMA port 3 (X5)
    static const uint8_t SMA_PORT_X6 = 2; ///< SMA port 2 (X6)
    static const uint8_t SMA_PORT_X7 = 1; ///< SMA port 1 (X7)

    ///@}
};
#endif
