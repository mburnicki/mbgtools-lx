/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2021
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

#ifndef INC_DATATYPES
#define INC_DATATYPES

#include "basictypes.h"
#include "ptptime.h"
#include "msgx.h"
#include "basicfunctions.h"

#ifndef PTP_VERSION_NUMBER
#  define PTP_VERSION_NUMBER 2
#endif
#ifndef PTP_VERSION_NUMBER_2_1
#  define PTP_VERSION_NUMBER_2_1 (2 | (1 << 4)) // major version = lower nibble; minor = upper nibble
#endif

#pragma pack(1)

/// PTP Port Identity structure

/// This structure defines the port identity and some
/// useful basic operations on them (compare, convert_nw, ...)

struct PTP_PortId : public PortId_t {

  //static const uint8_t CAllClockId[] = "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF";

  /// Constructor
  PTP_PortId(const char *id = "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF",
             const uint16_t p = 0xFFFF) {
    PTP_MEMCPY(clkId, id, sizeof(clkId));
    port = p;
  }

  explicit PTP_PortId(const PortId_t &pId) {
    *this = pId;
  }

  /// Copy Assignment operator
  PTP_PortId &operator=(const PortId_t &rhs) {
    PTP_MEMCPY(clkId, rhs.clkId, sizeof(clkId));
    port = rhs.port;
    return *this;
  }

  // PortId comparation functions, according to ordering of clock and
  // portId values (see 7.5.2.4)

  /// Compare to Clock Id
  int cmp(const uint8_t *cid) const {
    int c = PTP_MEMCMP(this->clkId, cid, sizeof(clkId));
    return (c ? c : +1);
  }

  /// Compare to Port Id
  int cmp(const PTP_PortId &pid) const {
    int c = PTP_MEMCMP(this->clkId, pid.clkId, sizeof(clkId));
    return (c ? c : (int)((uint32_t)port - (uint32_t)pid.port));
  }

  /// Equal operator: Clock Id is the same
  bool operator ==(const uint8_t *cid) const {
    return PTP_MEMCMP(clkId, cid, sizeof(clkId)) == 0;
  }
  /// Equal operator: Port Id is the same
  bool operator ==(const PTP_PortId &pid) const {
    return (this->cmp(pid) == 0);
  }
  /// Inequality operator: Clock Id is not the same
  bool operator !=(const uint8_t *cid) const {
    return !(*this == cid);
  }
  /// Inequality operator: Port Id is not the same
  bool operator !=(const PTP_PortId &pid) const {
    return (this->cmp(pid) != 0);
  }

  /// Matching: true if all fields are equal or are Broadcast (FF)
  bool matches(const PTP_PortId &pid) const {
     return (*this == pid.clkId || *this == PTP_PortId().clkId) &&
            (this->port == pid.port || this->port == 0xFFFF);
  }
  /// Convert to and from network byte order
  void convert_nw() {
    port = N2H_16(port);
  }
}__PACKME__;

/// PTP Clock Quality structure
struct PTP_ClockQuality : ClockQuality_t {

  /// Convert to and from network byte order
  void convert_nw() {
    clkVariance = N2H_16(clkVariance);
  }

  bool operator !=(const PTP_ClockQuality &clkQual) const {
    if (clkClass    != clkQual.clkClass) return true;
    if (clkAccuracy != clkQual.clkAccuracy) return true;
    if (clkVariance != clkQual.clkVariance) return true;
    return false;
  }

}__PACKME__;

/// Acceptable Master
struct PTP_AcceptableMaster : public AcceptableMaster_t {

  void convert_nw() {
    PTP_PortId(portId).convert_nw();
  }

  PTP_AcceptableMaster &operator =(const PTP_AcceptableMaster &m) {
    PTP_MEMCPY(this, &m, sizeof(PTP_AcceptableMaster));
    return *this;
  }

} __PACKME__;

/// PTP Port Address structure
struct PTP_PortAddress : public PortAddress_t {

  /// Convert to and from network byte order
  void convert_nw() {
    nwProto = N2H_16(nwProto);
    len     = N2H_16(len);
  }
} __PACKME__;

/// this structure holds sdoid id's major and minor parts. //IEEE1588-2019 7.1.1
class SdoId {
  private:
    uint8_t      majorSdoid ; //4 msb of 12 bit
    uint8_t      minorSdoid ; //8 lsb of 12 bit
    uint16_t     wholeSdoid ; //12

  public:
    explicit SdoId(uint16_t raw){
      wholeSdoid =  (raw & 0x0FFF);
      majorSdoid =  (wholeSdoid & 0x0F00) >> 8;
      minorSdoid =  (wholeSdoid & 0x00FF);
    };

    SdoId(){
      wholeSdoid =  0;
      majorSdoid =  0;
      minorSdoid =  0;
    };
    
    uint8_t  majorId() {return majorSdoid;};
    uint8_t  minorId() {return minorSdoid;};
    uint16_t wholeId() {return wholeSdoid;};

}__PACKME__;

/// PTP State enumeration
const PTP_State SInitializing  = 1; ///< Initializing state
const PTP_State SFaulty        = 2; ///< Faulty state
const PTP_State SDisabled      = 3; ///< Disabled state
const PTP_State SListening     = 4; ///< Listening state
const PTP_State SPreMaster     = 5; ///< Pre-Master state
const PTP_State SMaster        = 6; ///< Master state
const PTP_State SPassive       = 7; ///< Passive state
const PTP_State SUncalibrated  = 8; ///< Uncalibrated state
const PTP_State SSlave         = 9; ///< Slave state
const PTP_State SMaxStateArray = 10;

#define PTP_STATE_DEF \
             { "", "Initializing", "Faulty", "Disabled", "Listening", "PreMaster", "Master", \
               "Passive", "Uncalibrated", "Slave" }

/// PTP State String representation
extern const char *PTP_StateStr[];

/// PTP Message type enumeration
enum {
  TSync,
  TDlyReq,
  TPDlyReq,
  TPDlyResp,
  TFollowUp = 8,
  TDlyResp,
  TPDlyRespFup,
  TAnnounce,
  TSignalling,
  TManagement,
  TMaxMsgArray
};

#define PTP_MSG_DEF { \
"Sync", \
"DlyReq", \
"PDlyReq", \
"PDlyResp", \
"x", \
"x", \
"x", \
"x", \
"FollowUp", \
"DlyResp", \
"PDlyRespFup", \
"Announce", \
"Signalling", \
"Management"}

/// String representation for message type
extern const char *PTP_MsgStr[];

#pragma pack()

#endif
