/******************************************************************************
 *           _____          ___________        _____    __    __ ___
 *          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
 *         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
 *        /  \___/ /          / /           /  \___/ /    \  / / __/
 *       / /\_____/          / /           / /\_____/      \/ /___/
 *      / /                 / /           / /
 *     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
 *
 ******************************************************************************
 *
 *                  Oregano Systems - Design & Consulting GesmbH
 *
 *         Web: http://oregano.at              @: support@oregano.at
 *
 ******************************************************************************
 *
 *                            Copyright (c) 2022
 *
 ******************************************************************************
 *
 *                             Confidential data.
 *
 * This source code, netlists, documentation is confidential and must not be
 * published nor made available to third parties. Usage of this source code,
 * netlists, documentation is subject to restrictions and conditions of a
 * license agreement.
 *
 * ALL RIGHTS RESERVED.
 * Oregano Systems does not assume any liability
 * arising out of the application or use of any product described or shown
 * herein nor does it convey any license under its patents, copyrights, or any
 * rights of others.
 *
 * Oregano Systems reserves the right to make changes, at any time, in
 * order to improve reliability, function or design. Oregano Systems will not
 * assume responsibility for the use of any circuitry described herein.
 *
 * All trademarks used are the property of their respective owners.
 *
 *****************************************************************************/

/*
 * THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
 * AS SOURCE CODE
 * */

#ifndef INC_BASICTYPES
#define INC_BASICTYPES

/// @defgroup datatypes "PTP Datatypes"
/// @brief Datatypes used internally by the PTP library

#if !defined( __cplusplus )
  #define inline __inline
#endif
#ifdef _WIN32
  // On Windows, the name of a shared memory and other
  // shared objects (e.g. semaphores) has to start with
  // a specific prefix, if the object is to be shared
  // between different namespaces, e.g. services
  // and applications.
  #define SHM_GLOBAL_PREFIX            "Global\\"
#endif
/*
 * Compiler specific includes and packing/alignment macros
 *
 * */
#define __PACKME__
#if defined WIN32
#elif defined __NIOS__
#  include <stddef.h>
#  undef __PACKME__
#  define __PACKME__ __attribute__ ((packed))
#  define __ALIGN__
#elif defined __linux__
#  include "sys/types.h"
#  if defined __arm__
#    undef __PACKME__
#    define __PACKME__ __attribute__ ((packed))
#    define __ALIGN__
#  endif
#endif
#if defined WIN32
#  define __FALLTHROUGH__
#elif defined __linux__
// not yet in code because not supported by older versions of gcc
#  define __FALLTHROUGH__ __attribute__ ((fallthrough))
#elif defined CPP17COMPLIANT
#  define __FALLTHROUGH__ [[fallthrough]]
#endif

#include <stdint.h>


// A generic, OS-specific handle type.
#ifdef _WIN32
  #include <winsock2.h>
  #include <windows.h>
  typedef HANDLE SYN1588_DEV_HANDLE;
#else  // Linux / POSIX
  typedef int SYN1588_DEV_HANDLE;
#endif


#ifndef UINT32_MAX
#  define UINT32_MAX (uint32_t(-1))
#endif
#ifndef INT64_MAX
#  define INT64_MAX  (int64_t(~(1llu << 63)))
#endif
#ifndef UINT8_MAX
#  define UINT8_MAX (uint8_t(-1))
#endif

// define a handy 32bit "boolean" for determining the current compilation arch
#include <limits.h>
#if ((ULONG_MAX) == (UINT_MAX))
#  define _IS_32_BIT_
#endif

// Defines for converting ns to scaled ns and the other way around
inline int64_t TO_SCALED_NS(int64_t ns) { return ns << 16; }
inline int64_t FROM_SCALED_NS(int64_t scaledNs) { return scaledNs >> 16; }

#ifdef __cplusplus
inline int32_t PRINT_SCALED_NS(int64_t scaledNs) {
  return static_cast<int32_t>((scaledNs >> 16) & static_cast<int32_t>(0xFFFFFFFFu));
}
inline uint16_t PRINT_SUB_NS(int64_t scaledNs) {
  return static_cast<uint16_t>((((scaledNs) & 0xFFFFL) * 100) >> 16);
}
inline uint64_t TO_SCALED_RATIO(double ratio) {
  return static_cast<uint64_t>(((ratio) - (1.0)) *
                    ((static_cast<uint64_t>(1) << (41))) );
}
#else
inline int32_t PRINT_SCALED_NS(int64_t scaledNs) {
  return (int32_t)((scaledNs >> 16) & (int32_t)0xFFFFFFFF);
}
inline uint16_t PRINT_SUB_NS(int64_t scaledNs) {
  return (uint16_t)((((scaledNs) & 0xFFFFL) * 100) >> 16);
}
inline uint64_t TO_SCALED_RATIO(double ratio) {
  return (uint64_t)(((ratio) - (1.0)) *
                    ((uint64_t)(1) << (41)));
}
#endif //__cplusplus

#define FROM_SCALED_RATIO(x) ((double)(x) / (1ULL << 41) + 1.0)
#define PRINT_CLKID(x)       x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7]
#define PRINT_IPV4(x)        x[0], x[1], x[2], x[3]
#define PRINT_IPV6(x)        x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], \
  x[8], x[9], x[10], x[11], x[12], x[13], x[14], x[15]

#define CMP_RET(a, b, aret, bret) \
  if ((a) > (b)) return aret; \
  if ((a) < (b)) return bret

#if !defined( NSEC_PER_SEC )
extern const uint32_t NSEC_PER_SEC; ///< nanoseconds per second
extern const uint32_t USEC_PER_SEC; ///< microseconds per second
extern const uint32_t MSEC_PER_SEC; ///< milliseconds per second
#endif


#define S_IN_NS(x)  (x * (int64_t)NSEC_PER_SEC)
#define MS_IN_NS(x) (x * ((int64_t)NSEC_PER_SEC / 1000))
#define US_IN_NS(x) (x * ((int64_t)NSEC_PER_SEC / 1000000))

#pragma pack(1)

/// Storage for values from the PTP_SYNC_STATUS register.
typedef uint32_t SYN1588_PTP_SYNC_STATUS;

/// Timestamp structure as defined in IEEE 1588-2008 5.3.3
/// @ingroup datatypes
struct Timestamp_t {
  uint16_t   sec_h; ///< Seconds (47:32)
  uint32_t   sec_l; ///< Seconds (31:0)
  uint32_t   nsec;  ///< Nanoseconds
} __PACKME__;

/// Timestamp plus sync status
/// @ingroup datatypes
struct TimestampStatus_t {
  struct Timestamp_t ts;  ///< Time stamp, PTP scale (TAI) or arbitrary.
  uint32_t sync_status;   ///< Value from the ::PTP_SYNC_STATUS register.
} __PACKME__;

/// Time Interval structure as defined in IEEE 1588-2008 5.3.2
/// @ingroup datatypes
struct TimeInterval_t {
  int64_t   scaledNs; ///< Scaled Nanoseconds (ns shifted left by 16)
} __PACKME__;

/// Port Identifier structure as defined in IEEE1588-2008 5.3.5. It identifies
/// a PTP port.
/// @ingroup datatypes
struct PortId_t {
  uint8_t    clkId[8]; ///< EUI-64 Clock Identity
  uint16_t   port;     ///< Port number
} __PACKME__;

/// Port Address structure as defined in IEEE 1588-2008 5.3.6. It represents
/// the protocol address of a PTP port. This structure can be used for all
/// kind of protocols (e.g. IPv4, IPv6, layer 2).
/// @ingroup datatypes
struct PortAddress_t {
  uint16_t   nwProto;  ///< Network Protocol Identifier
  uint16_t   len;      ///< Address Length
  uint8_t    addr[16]; ///< Address
} __PACKME__;

/// Clock Quality structure as defined in IEEE 1588-2008 5.3.8. It contains
/// information about the actual quality level of a PTP node.
/// @ingroup datatypes
struct ClockQuality_t {
  uint8_t    clkClass;    ///< Clock Class
  uint8_t    clkAccuracy; ///< Clock Accuracy
  uint16_t   clkVariance; ///< Clock Variance
} __PACKME__;

/// Acceptable Master structure
/// holds a PortId && alternate priority of an acceptable master
/// as defined in IEEE1588-2017 Draft
struct AcceptableMaster_t {
  struct PortId_t   portId;   ///< port ID
  uint8_t           altPrio1; ///< alternative priority1
} __PACKME__;

/// The PTP State is nothing else than an unsigned integer.
/// @ingroup datatypes
typedef uint8_t PTP_State;

/// @brief Return values
///
/// This enumeration should be used as return values of functions throughout
/// the whole PTP Stack to signal if a function returned successfully or not.
/// @ingroup datatypes
enum Return_e {EErr, ENoErr};

/// @brief Boundaries used in the PTP-applications
///
/// These boundaries should be used when boundaries are needed.
/// @note This enumeration shall be sorted from the biggest to the smallest value.
/// @ingroup datatypes
enum Boundaries_e {
  EOver10s = 0,
  EWithin10s,
  EWithin1s,
  EWithin250ms,
  EWithin100ms,
  EWithin25ms,
  EWithin10ms,
  EWithin2_5ms,
  EWithin1ms,
  EWithin250us,
  EWithin100us,
  EWithin25us,
  EWithin10us,
  EWithin2_5us,
  EWithin1us,
  EWithin250ns,
  EWithin100ns,
  EWithin50ns,
  EWithin25ns,
  EWithin10ns,
  EMaxBoundary
};


/// @brief An insync range boundary code indicating out-of-bounds.
///
/// This insync range boundary code is the maximum possible value
/// that can be stored in the ::PSS_BITMASK_PTP_INSYNC_BND_CODE
/// of the ::PTP_SYNC_STATUS word.
/// It should be used to indicate that the boundary is out of
/// the well-defined limits, similar to ::EOver10s.
///
/// @see ::PTP_STATE_RANGE_TABLE_INIT
#define PTP_STATE_RANGE_MAX_CODE  0x3F

/// @brief Codes for the ::PSS_BITMASK_PTP_INSYNC_BND_CODE field
///
/// An initializer for a table to convert the enum values in ::Boundaries_e
/// to a code for the ::PSS_BITMASK_PTP_INSYNC_BND_CODE field in the
/// ::PTP_SYNC_STATUS register.
///
/// Using these codes instead of the index values from ::Boundaries_e makes
/// it easier to keep the meaning of the ::PSS_BITMASK_PTP_INSYNC_BND_CODE
/// field consistent, even if new values are inserted in the middle of the enum,
/// as long as this initializer is updated accordingly.
///
/// Of the 6 available bits, the 2 LSBs are used to define a kind of mantissa,
/// in steps of 0.1, 0.25. 0.50, 0.75 associated to the 4 possible binary values.
/// The 4 MSBs are interpreted as an exponent to base 10, to which a constant
/// offset has to be applied to map the resulting values to a range that is
/// useful for PTP, which is here 1 ps for code 0x00 up to 5000 s for
/// the highest code 0x3E. In addition there is code 0x3F, which would map
/// to 7500 s, but is used here as an exception to indicate that the maximum
/// range is exceeded, like ::EOver10s.
///
/// @see ::PSS_BITMASK_PTP_INSYNC_BND_CODE
/// @see ::PTP_STATE_RANGE_MAX_CODE
#define PTP_STATE_RANGE_TABLE_INIT          \
{                                           \
  PTP_STATE_RANGE_MAX_CODE,  /* EOver10s */ \
  0x34,      /* EWithin10s   */             \
  0x30,      /* EWithin1s    */             \
  0x2D,      /* EWithin250ms */             \
  0x2C,      /* EWithin100ms */             \
  0x29,      /* EWithin25ms  */             \
  0x28,      /* EWithin10ms  */             \
  0x25,      /* EWithin2_5ms */             \
  0x24,      /* EWithin1ms   */             \
  0x21,      /* EWithin250us */             \
  0x20,      /* EWithin100us */             \
  0x1D,      /* EWithin25us  */             \
  0x1C,      /* EWithin10us  */             \
  0x19,      /* EWithin2_5us */             \
  0x18,      /* EWithin1us   */             \
  0x15,      /* EWithin250ns */             \
  0x14,      /* EWithin100ns */             \
  0x12,      /* EWithin50ns  */             \
  0x11,      /* EWithin25ns  */             \
  0x10       /* EWithin10ns  */             \
}


extern int64_t CBoundariesInScaledNS[];

#ifndef __cplusplus
// For C only programs, we have to define a few
// datatypes here instead
typedef struct Timestamp_t    Timestamp_t;
typedef struct TimeInterval_t TimeInterval_t;
typedef struct PortId_t       PortId_t;
typedef struct PortAddress_t  PortAddress_t;
#else
#ifdef _WIN32
struct timeval;
#endif

struct timeval operator+(struct timeval t1, struct timeval t2);
struct timeval operator-(struct timeval t1, struct timeval t2);

#endif // __cplusplus

#pragma pack()

#endif
