/******************************************************************************
*           _____          ___________        _____    __    __ ___
*          / ___ \        /____  ___ /       / ___ \   \ \  / //__ \
*         / / |_\ \           / /           / / \_\ \   \ \/ / __/ /
*        /  \___/ /          / /           /  \___/ /    \  / / __/
*       / /\_____/          / /           / /\_____/      \/ /___/
*      / /                 / /           / /
*     /_/r e c i s i o n  /_/i m e      /_/r o t o c o l
*
******************************************************************************
*
*                  Oregano Systems - Design & Consulting GesmbH
*
*         Web: http://oregano.at              @: support@oregano.at
*
******************************************************************************
*
*                            Copyright (c) 2021
*
******************************************************************************
*
*                             Confidential data.
*
* This source code, netlists, documentation is confidential and must not be
* published nor made available to third parties. Usage of this source code,
* netlists, documentation is subject to restrictions and conditions of a
* license agreement.
*
* ALL RIGHTS RESERVED.
* Oregano Systems does not assume any liability
* arising out of the application or use of any product described or shown
* herein nor does it convey any license under its patents, copyrights, or any
* rights of others.
*
* Oregano Systems reserves the right to make changes, at any time, in
* order to improve reliability, function or design. Oregano Systems will not
* assume responsibility for the use of any circuitry described herein.
*
* All trademarks used are the property of their respective owners.
*
*****************************************************************************/
#ifndef INC_BASICFUNCTIONS
#define INC_BASICFUNCTIONS
/*
* THIS FILE IS PART OF THE CUSTOMER API AND IS PROVIDED TO CUSTOMERS
* AS SOURCE CODE
* */

/// @file
/// This file contains macros to define certain basic functions which are
/// used throughout the syn1588(R) PTP library. Most of the functions are part
/// of the standard C library. However, some implementations may want to use
/// proprietary implementations of the functions. These implementations may 
/// be defined in this file. Note that the parameter list shall match with
/// the standard C library implementation.

#ifndef _NO_STD_CLIB_
#include <string.h>

#define PTP_MEMCPY  ::memcpy
#define PTP_MEMCMP  ::memcmp
#define PTP_MEMMOVE ::memmove
#define PTP_MEMSET  ::memset
#define PTP_STRCHR  ::strchr
#define PTP_STRCMP  ::strcmp
#define PTP_STRLEN  ::strlen
#define PTP_STRNCMP ::strncmp
#define PTP_STRNCPY ::strncpy
#define PTP_STRSTR  ::strstr

#else
// define proprietary function implementations here

#endif

#endif
