
/**************************************************************************
 *
 *  $Id: mbg_xmr_util.h 1.3 2022/12/21 15:24:03 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and prototypes for mbg_xmr_util.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_xmr_util.h $
 *  Revision 1.3  2022/12/21 15:24:03  martin.burnicki
 *  Removed an obsolete function parameter.
 *  Revision 1.2  2022/06/30 09:47:33  martin.burnicki
 *  Updated function prototypes.
 *  Revision 1.1  2022/06/24 12:54:36  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_XMR_UTIL_H
#define _MBG_XMR_UTIL_H


/* Other headers to be included */

#include <cfg_hlp.h>


#ifdef _MBG_XMR_UTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif

/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Print all XMR status info.
 *
 * @param[in]  p_axrs           Pointer to an XMR status info structure read from a device.
 * @param[in]  n_src            Number of XMR ref. sources, from ::ALL_XMULTI_REF_INFO::instances.
 * @param[in]  info             An optional header string to print, may be @a NULL.
 * @param[in]  indent_str       An optional indentation string, may be @a NULL.
 */
 void print_all_xmr_status( ALL_XMULTI_REF_STATUS *p_axrs, int n_src, const char *info, const char *indent_str ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_XMR_UTIL_H */
