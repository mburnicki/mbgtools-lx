
/**************************************************************************
 *
 *  $Id: ctrydttm.h 1.8 2022/12/21 15:48:53 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for ctrydttm.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: ctrydttm.h $
 *  Revision 1.8  2022/12/21 15:48:53  martin.burnicki
 *  Updated function prototypes.
 *  Revision 1.7  2018/12/11 16:01:48  martin
 *  Updated function prototypes.
 *  Revision 1.6  2018/01/02 16:15:06Z  martin
 *  Updated function prototypes.
 *  Revision 1.5  2015/08/31 10:00:46Z  martin
 *  Fixed DOS build.
 *  Revision 1.4  2015/08/27 16:28:25Z  martin
 *  Updated function prototypes.
 *  Revision 1.3  2000/09/14 15:13:45  MARTIN
 *  Updated function prototypes.
 *  Revision 1.2  2000/07/21 11:50:43  MARTIN
 *  Initial revision
 *
 **************************************************************************/

#ifndef _CTRYDTTM_H
#define _CTRYDTTM_H


/* Other headers to be included */

#include <ctry.h>

#include <stdlib.h>


#ifdef __cplusplus
extern "C" {
#endif

#ifdef _CTRYDTTM
 #define _ext
#else
 #define _ext extern
#endif


/* Start of header body */






/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 int snprint_02u( char *s, size_t max_len, unsigned int uc ) ;
 int snprint_04u( char *s, size_t max_len, unsigned int us ) ;
 int snprint_ctry_wday( char *s, size_t max_len, int wday, LANGUAGE language ) ;
 int snprint_ctry_dt_short( char *s, size_t max_len, uint mday, uint month ) ;
 int snprint_ctry_dt( char *s, size_t max_len, int mday, int month, int year ) ;
 int snprint_ctry_tm_short( char *s, size_t max_len, uchar hour, uchar minute ) ;
 int snprint_ctry_tm( char *s, size_t max_len, uchar hour, uchar minute, uchar second ) ;
 int snprint_ctry_tm_long( char *s, size_t max_len, uchar hour, uchar minute, uchar second, long frac, ushort frac_digits ) ;

/* ----- function prototypes end ----- */

/* End of header body */


#undef _ext

#ifdef __cplusplus
}
#endif


#endif  /* _CTRYDTTM_H */

