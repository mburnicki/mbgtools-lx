
/**************************************************************************
 *
 *  $Id: chk_time_info.h 1.7 2021/04/12 22:17:11 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for chk_time_info.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: chk_time_info.h $
 *  Revision 1.7  2021/04/12 22:17:11  martin
 *  Updated function prototypes.
 *  Revision 1.6  2021/04/07 18:09:28  martin
 *  Moved postprocessing of raw data pairs to a separate
 *  function, mbg_proc_chk_time_info().
 *  Revision 1.5  2021/03/22 17:37:29  martin
 *  Updated some comments.
 *  Revision 1.4  2019/02/08 10:30:36  martin
 *  Renamed some symbols.
 *  Added doxygen comments.
 *  Updated function prototypes.
 *  Revision 1.3  2017/07/05 09:01:46  martin
 *  Tiny cleanup.
 *  Updated function prototypes.
 *  Revision 1.2  2013/03/04 16:02:31  martin
 *  Updated function prototypes.
 *  Revision 1.1  2012/05/29 09:52:27  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _CHK_TIME_INFO_H
#define _CHK_TIME_INFO_H


/* Other headers to be included */

#include <mbgdevio.h>


#ifdef _CHK_TIME_INFO
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if 0 && defined( _USE_PACK )  // use default alignment
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif

#ifdef __cplusplus
extern "C" {
#endif


#if !defined( MAX_CYCLES_FILTER_ENTRIES )
  #define MAX_CYCLES_FILTER_ENTRIES    32
#endif



/**
 * @brief A structure to keep filter data for cycles values.
 *
 * Used to keep track of the filter state.
 */
typedef struct
{
  MBG_PC_CYCLES cyc[MAX_CYCLES_FILTER_ENTRIES];  ///< The last recent values to be filtered.
  MBG_PC_CYCLES sum;                             ///< The current sum of the cycles values.
  int entries;                                   ///< The number of cycles values already saved.
  int index;                                     ///< Index of the "current" cycles value.

} CYCLES_FILTER_DATA;



/**
 * @brief A structure to store the results of the ::mbg_chk_time_info routine
 *
 * Used to keep track of the filter state.
 */
typedef struct
{
  MBG_TIME_INFO_HRT hrti;         ///< System time, device time, and cycles read from the driver.

  MBG_PC_CYCLES ltcy_cyc;         ///< Computed latency of the call, in [cycles].
  MBG_PC_CYCLES exec_cyc;         ///< Computed execution time of the call, in [cycles].
  MBG_PC_CYCLES exec_cyc_limit;   ///< Execution time limit, in [cycles], computed based on usual execution time.

  double ltcy_sec;                ///< Computed latency of the call, in [s]. Only available if cycles frequency is known.
  double exec_sec;                ///< Computed execution time of the call, in [s]. Only available if cycles frequency is known.
  double exec_sec_limit;          ///< Execution time limit, in [s]. Only available if cycles frequency is known.

  double d_sys;                   ///< System timestamp in floating point format, [s].
  double d_ref;                   ///< Raw reference timestamp in floating point format, [s].
  double d_ref_comp;              ///< Compensated reference timestamp in floating point format, [s].

} MBG_CHK_TIME_INFO;



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Process raw system timestamp / reference timestamp pairs.
 *
 * Calculates and fills some fields in a ::MBG_CHK_TIME_INFO structure
 * to simplify further processing by the calling application.
 *
 * @param[in,out]  p_cti     Pointer to a ::MBG_CHK_TIME_INFO structure to be handled.
 * @param[in,out]  p_filter  Optional address of a ::CYCLES_FILTER_DATA instance
 *                           associated with the time stamps. If this parameter is
 *                           not @a NULL, the execution limit is also updated.
 *
 * @see ::mbg_chk_time_info
 */
 void mbg_proc_chk_time_info( MBG_CHK_TIME_INFO *p_cti, CYCLES_FILTER_DATA *p_filter ) ;

 /**
 * @brief Read and evaluate a system timestamp / reference timestamp pair.
 *
 * The device timestamp is considered as reference timestamp.
 *
 * Usually the @p fast_ts_only flag should be 0, in which case a ::PCPS_HR_TIME
 * is read from the device in conjunction with the system timestamp.
 *
 * If the @p fast_ts_only flag is not 0, only a ::PCPS_TIME_STAMP instead of a
 * ::PCPS_HR_TIME is read in conjunction with the system timestamp. In this case
 * the call executes faster, but this is only supported if the device supports
 * memory mapped timestamps (see ::chk_fast_tstamp_supp). Also, the returned data
 * doesn't include the device status information in this case.
 *
 * Once the device system timestamp / reference timestamp pair has been read
 * successfully, ::mbg_proc_chk_time_info is called to evaluate the associated
 * cycles values to simplify further processing by the calling application.
 *
 * @param[in]      dh        Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]     p_cti     Pointer to a ::MBG_CHK_TIME_INFO structure to be written.
 * @param[in,out]  p_filter  Optional address of a ::CYCLES_FILTER_DATA instance
 *                           associated with the device that is referenced by @p dh.
 *                           If this parameter is not @a NULL, the execution limit is
 *                           automatically updated.
 * @param[in]  fast_ts_only  A flag indicating that only a ::PCPS_TIME_STAMP
 *                           instead of a ::PCPS_HR_TIME is to be read
 *                           in conjunction with the system timestamp,
 *                           usually 0.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_proc_chk_time_info
 * @see ::snprint_chk_time_info
 * @see ::mbg_get_time_info_hrt
 * @see ::mbg_get_time_info_tstamp
 */
 int mbg_chk_time_info( MBG_DEV_HANDLE dh, MBG_CHK_TIME_INFO *p_cti, CYCLES_FILTER_DATA *p_filter, int fast_ts_only ) ;

 /**
 * @brief Print a device name into a string buffer.
 *
 * Format according to ::snprint_chk_time_info.
 *
 * @param[out] s         The string buffer to be filled.
 * @param[in]  max_len   Size of the output buffer for 0-terminated string.
 * @param[in]  dev_name  Pointer to a ::MBG_CHK_TIME_INFO to be evaluated.
 *
 * @return Length of the string in the buffer.
 *
 * @see ::mbg_chk_time_info
 */
 int snprint_chk_time_dev_name( char *s, size_t max_len, const char *dev_name ) ;

 /**
 * @brief Print info from a ::MBG_CHK_TIME_INFO structure into a string buffer.
 *
 * @param[out] s            The string buffer to be filled.
 * @param[in]  max_len      Size of the output buffer for 0-terminated string.
 * @param[in]  p_cti        Pointer to a ::MBG_CHK_TIME_INFO to be evaluated.
 * @param[in]  p_dev        Pointer to a device info, optional, may be @a NULL.
 * @param[in]  frac_digits  Number of fractional digits to be printed, e.g. 9 for nanoseconds.
 * @param[in]  print_raw    If not 0, raw values are also printed.
 *
 * @return Length of the string in the buffer.
 *
 * @see ::mbg_chk_time_info
 */
 int snprint_chk_time_info( char *s, size_t max_len, const MBG_CHK_TIME_INFO *p_cti, const PCPS_DEV *p_dev, int frac_digits, int print_raw ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT

#endif  /* _CHK_TIME_INFO_H */

