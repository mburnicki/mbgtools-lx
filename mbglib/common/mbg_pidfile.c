
/**************************************************************************
 *
 *  $Id: mbg_pidfile.c 1.7 2021/03/26 10:28:01 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Functions to handle a PID file.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_pidfile.c $
 *  Revision 1.7  2021/03/26 10:28:01  martin
 *  New function mbg_pidfile_register_cleanup() that registers
 *  mbg_pidfile_close_and_remove() as atexit function.
 *  Should be called by the application, if required.
 *  Do logging via a function pointer that defaults to syslog()
 *  but can be changed to point to a different function.
 *  Updated lots of comments.
 *  Revision 1.6  2021/03/14 22:36:55  martin
 *  Fixed a clang compiler warning.
 *  Revision 1.5  2014/07/16 15:19:55  martin
 *  Revision 1.4  2013/08/01 16:17:01  martin
 *  Revision 1.3  2013/07/30 15:30:49  martin
 *  Revision 1.2  2013/07/24 15:29:38  martin
 *  Revision 1.1  2013/07/23 16:10:18  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _MBG_PIDFILE
  #include <mbg_pidfile.h>
#undef _MBG_PIDFILE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/file.h>


// PID file handling
//
// If we want to make sure that no other instance of the same program
// is running, neither as daemon nor in the foreground only, we want
// to try to open and lock the PID file *before* the program forks.
// On the other hand, if the program forks, we need to write our PID
// to the PID file *after* the fork.
//
// So the best approach is to open and lock the PID file before the
// fork, and write the PID to it after the fork, and hence we need to
// make sure that the lock is persistant across the fork.
//
// The flopen() call opens and locks a PID file in one step and thus
// avoids a race condition. It is usually implemented using open()
// and flock(), so it is persistant across forks. So this call usually
// meets our requirements, but is not supported on all systems.
//
// If flopen() is not supported, we have to call open() first and
// then some locking function, which can be flock() or lockf().
//
// flock() *can* be implemented on top of fcntl(), but usually isn't.
// lockf() *is* usually implemented on top of fcntl(), but may be not.
// fcntl() usually works on NFS mounts, other implementations usually don't.
// fcntl() usually isn't persistant across forks, but flock() is.
//
// PID files are usually not located inside the local file system,
// not on NFS mounts, hence flopen() is the first choice, and using
// open() plus flock() is the second best choice.

#if !defined( HAVE_FLOPEN )
  // flopen() is usually supported on *BSD.
  #if defined( MBG_TGT_BSD )
    #define HAVE_FLOPEN
  #endif
#endif

#if HAVE_FLOPEN
  #include <libutil.h>
#else
  #define USE_FLOCK           1
#endif


// Global file name of the PID file.
static const char *glb_pid_fn;

// We don't make this static to be able to change this externally,
// though this is not recommended.
mode_t pid_file_mode = 0640;


/*HDR*/
/**
 * @brief Close and remove a PID file.
 *
 * Should be called immediately before a daemon terminates,
 * and can be registered as @a atexit function.
 *
 * @see ::mbg_pidfile_open_and_lock
 * @see ::mbg_pidfile_write_own_pid
 * @see ::mbg_pidfile_register_cleanup
 */
void mbg_pidfile_close_and_remove( void )
{
  int rc;
  pid_t pid = getpid();

  #if defined( DEBUG )
    pidfile_log_fnc( LOG_INFO, "%s called, pid %i", __func__, pid );
  #endif

  if ( glb_pid_fd >= 0 )
  {
    rc = close( glb_pid_fd );

    if ( rc == -1 )
    {
      pidfile_log_fnc( LOG_WARNING, "Process %i failed to close PID file %s (fd %i): %s",
                       pid, glb_pid_fn, glb_pid_fd, strerror( errno ) );
    }
    else
    {
      #if defined( DEBUG )
        pidfile_log_fnc( LOG_INFO, "Process %i closed PID file %s (fd %i)",
                         pid, glb_pid_fn, glb_pid_fd );
      #endif

      glb_pid_fd = -1;
    }
  }


  if ( glb_pid_fn )
  {
    rc = unlink( glb_pid_fn );

    if ( rc == -1 )
    {
      pidfile_log_fnc( LOG_WARNING, "Process %i failed to remove PID file %s: %s",
                       pid, glb_pid_fn, strerror( errno ) );
    }
    else
    {
      #if defined( DEBUG )
        pidfile_log_fnc( LOG_INFO, "Process %i removed PID file %s",
                         pid, glb_pid_fn );
      #endif

      glb_pid_fn = NULL;
    }
  }

}  // mbg_pidfile_close_and_remove



/*HDR*/
/**
 * @brief Register a cleanup function that is called on exit.
 *
 * Should be called immediately before ::mbg_pidfile_write_own_pid is called.
 *
 * @see ::mbg_pidfile_close_and_remove
 */
/*HDR*/
void mbg_pidfile_register_cleanup( void )
{
  #if defined( DEBUG )
    pidfile_log_fnc( LOG_INFO, "Process %i registers exit function %s",
                     getpid(),  "mbg_pidfile_close_and_remove" );
  #endif

  atexit( mbg_pidfile_close_and_remove );

}  // mbg_pidfile_register_cleanup



/*HDR*/
/**
 * @brief Try to open and lock a PID file.
 *
 * @note This function can be called before or after a program forks,
 * which is useful if only a single instance of a program may be
 * running, either as daemon or in the foreground.
 *
 * @param[in]  pid_fn  File name of the PID file, including path.
 *
 * @return The file descriptor of the PID file, or -1 on error.
 *
 * @see ::mbg_pidfile_close_and_remove
 * @see ::mbg_pidfile_write_own_pid
 */
int mbg_pidfile_open_and_lock( const char *pid_fn )
{
  pid_t pid = getpid();
  int flags = O_RDWR | O_CREAT;

  #if 1 && defined( O_CLOEXEC )
    flags |= O_CLOEXEC;  // TODO check if this is appropriate
  #endif

#if HAVE_FLOPEN
  // This is the preferred method to open and lock a PID file.
  // flopen() is usually available on *BSD. It does open() and flock()
  // in one step and thus avoids a race condition between opening and locking.

  // If flags includes O_NONBLOCK and the file is already locked,
  // flopen() will fail and set errno to EWOULDBLOCK.
  flags |= O_NONBLOCK;

  glb_pid_fd = flopen( pid_fn, flags, pid_file_mode );

  if ( glb_pid_fd < 0 )        // Failed to open PID file.
  {
    pidfile_log_fnc( LOG_ERR, "Process %i failed to open PID file %s: %s",
                     pid, pid_fn, strerror( errno ) );
    goto out_err;
  }

  #if defined( DEBUG )
    pidfile_log_fnc( LOG_INFO, "Process %i successfully opened and locked PID file %s (fd %i)",
                     pid, pid_fn, glb_pid_fd );
  #endif

#else  // flopen() is not supported.

  glb_pid_fd = open( pid_fn, flags, pid_file_mode );

  if ( glb_pid_fd < 0 )        // Failed to open PID file.
  {
    pidfile_log_fnc( LOG_ERR, "Process %i failed to open PID file %s: %s",
                     pid, pid_fn, strerror( errno ) );
    goto out_err;
  }

  #if defined( DEBUG )
    pidfile_log_fnc( LOG_INFO, "Process %i successfully opened PID file %s (fd %i)",
                     pid, pid_fn, glb_pid_fd );
  #endif

  #if USE_FLOCK
    // This is the preferred method to lock a PID file, if flopen()
    // is not supported. This may not work correctly if the file
    // to be locked is located on an NFS mount, but for a PID file
    // this shouldn't be the case.

    flags = LOCK_EX;   // Get an exclusive lock.
    flags |= LOCK_NB;  // Non-blocking, return error if would block.

    if ( flock( glb_pid_fd, flags ) < 0 )  // Failed to get a lock on PID file.
    {
      #if defined( DEBUG )
        pidfile_log_fnc( LOG_WARNING, "Process %i failed to get an flock() lock on PID file %s (fd %i): %s",
                         pid, pid_fn, glb_pid_fd, strerror( errno ) );
      #else
        pidfile_log_fnc( LOG_WARNING, "Failed to get a lock on PID file %s", pid_fn );
      #endif
      goto out_err_close;
    }

    #if defined( DEBUG )
      pidfile_log_fnc( LOG_INFO, "Process %i got an flock() lock on PID file %s (fd %i)",
                       pid, pid_fn, glb_pid_fd );
    #endif

  #else
    // lockf() locks are usually (but not necessarily) implemented on top
    // of fcntl(), but are *not* preserved across forks.
    // If implemented on top of fnctl(), locking works also on NFS mounts,
    // but locks are *not* preserved across forks.

    // Try to lock the file using F_TLOCK which never blocks but returns
    // an error if the file is already locked.
    if ( lockf( glb_pid_fd, F_TLOCK, 0 ) < 0 )  // Failed to get a lock on PID file.
    {
      #if defined( DEBUG )
        pidfile_log_fnc( LOG_WARNING, "Process %i failed to get a lockf() lock on PID file %s (fd %i): %s",
                         pid, pid_fn, glb_pid_fd, strerror( errno ) );
      #else
        pidfile_log_fnc( LOG_WARNING, "Failed to get a lock on PID file %s", pid_fn );
      #endif
      goto out_err_close;
    }

    #if defined( DEBUG )
      pidfile_log_fnc( LOG_INFO, "Process %i got a lockf() lock on PID file %s (fd %i)",
                       pid, pid_fn, glb_pid_fd );
    #endif

  #endif

#endif

  // Save PID file name.
  glb_pid_fn = pid_fn;

  goto out;  // Done.


out_err_close:
  close( glb_pid_fd );

out_err:
  glb_pid_fd = -1;

out:
  return glb_pid_fd;

}  // mbg_pidfile_open_and_lock



/*HDR*/
/**
 * @brief Write own process ID to our PID file.
 *
 * @note If a program runs as daemon, i.e. forks, this function
 * has to be called <b>after</b> the fork in order to write the
 * PID of the forked process to the PID file.
 *
 * @return The number of byte written to the PID file,
 *         or a negative number on error.
 *
 * @see ::mbg_pidfile_close_and_remove
 * @see ::mbg_pidfile_open_and_lock
 */
int mbg_pidfile_write_own_pid( void )
{
  char tmp_str[64];
  pid_t pid = getpid();

  // Write a single line with PID to PID file.
  int n = snprintf( tmp_str, sizeof( tmp_str ), "%i\n", pid );
  int rc = write( glb_pid_fd, tmp_str, n );

  if ( rc < 0 )
    pidfile_log_fnc( LOG_ERR, "Failed to write own pid %i to PID file %s (fd %i): %s",
                     pid, glb_pid_fn, glb_pid_fd, strerror( errno ) );
  else
  {
    if ( rc != n )
    {
      pidfile_log_fnc( LOG_WARNING, "Could only write %i of %i chars of own PID %i to PID file %s (fd %i)",
                       rc, n, pid, glb_pid_fn, glb_pid_fd );
      rc = -1;
    }
    else
    {
      #if defined( DEBUG )
        pidfile_log_fnc( LOG_INFO, "Wrote own PID %i to PID file %s (fd %i): %i bytes",
                         pid, glb_pid_fn, glb_pid_fd, n );
      #endif
    }
  }

  return rc;

}  // mbg_pidfile_write_own_pid

