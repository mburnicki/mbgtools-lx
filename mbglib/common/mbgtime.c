
/**************************************************************************
 *
 *  $Id: mbgtime.c 1.15 2022/12/21 15:27:29 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Functions related to date and time.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgtime.c $
 *  Revision 1.15  2022/12/21 15:27:29  martin.burnicki
 *  Quieted a potential compiler warning.
 *  Revision 1.14  2019/11/27 11:02:57  martin
 *  Renamed function n_days() to n_days_since_year_0()
 *  to make clearer what the function returns, and provided
 *  a backward-compatible inline function n_days().
 *  Updated some doxygen comments.
 *  Revision 1.13  2019/09/27 14:44:54  martin
 *  New function find_past_gps_wn_lsf_from_table().
 *  Changed parameters of n_days() to int to avoid compiler warnings.
 *  Revision 1.12  2019/09/19 16:35:14  martin
 *  Preliminary changes.
 *  Revision 1.11  2019/07/23 07:28:49  martin
 *  Exclude old sprint_...() functions from build in non-firmware projects.
 *  Revision 1.10  2019/06/26 10:02:12  martin
 *  New function day_of_week_sun06() which returns 0 for Sunday.
 *  Removed duplicate code in day_of_week().
 *  Updated doxygen comments.
 *  Revision 1.9  2018/11/21 12:02:18  martin
 *  Functions nano_time_to_double() and double_to_nano_time()
 *  were moved to module nanotime.c.
 *  Functions err_tm() and tm_to_wsec() now accept a pointer
 *  to a const structure.
 *  Function clear_time() also clears the TM_GPS::frac field.
 *  Added some casts to avoid compiler warnings.
 *  Removed obsolete inclusion of mystd.h.
 *  Added doxygen comments.
 *  Revision 1.8  2009/03/13 09:27:41  martin
 *  Include mystd.h here rather than in mbgtime.h.
 *  Revision 1.7  2006/08/25 09:32:12Z  martin
 *  Added new functions nano_time_to_double() and double_to_nano_time().
 *  Revision 1.6  2005/01/03 10:17:40Z  martin
 *  Moved functions days_to_years() and n_days() here.
 *  Parameters of n_days() have changed.
 *  Revision 1.5  2002/09/06 07:49:42Z  martin
 *  Turned name of included header file to lower case.
 *  New file header.
 *
 **************************************************************************/

#define _MBGTIME
 #include <mbgtime.h>
#undef _MBGTIME

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#ifdef _C166_FAULT
 #define MOD_CORR( x )  labs( x )
#else
 #define MOD_CORR( x )   x
#endif



/*HDR*/
/**
 * @brief Set a timeout object to specified interval
 *
 * @param[out]  t         The timeout object
 * @param[in]   clk       The current time, in clock_t ticks
 * @param[in]   interval  The interval until expiration, in clock_t ticks
 */
void set_timeout( TIMEOUT *t, clock_t clk, clock_t interval )
{
  t->start = clk;
  t->stop = ( clk + interval ) & MASK_CLOCK_T;
  t->is_set = 1;

}  // set_timeout



/*HDR*/
/**
 * @brief Stretch a timeout specified in given timeout object
 *
 * @param[in,out]  t         The timeout object
 * @param[in]      interval  The interval until expiration, in clock_t ticks
 */
void stretch_timeout( TIMEOUT *t, clock_t interval )
{
  t->stop = ( t->stop + interval ) & MASK_CLOCK_T;

}  // stretch_timeout



/*HDR*/
/**
 * @brief Check if a timeout object has expired
 *
 * @param[in]  t    The timeout object
 * @param[in]  clk  The current time, in clock_t ticks
 *
 * @return 1 if timeout expired, else 0
 */
bit check_timeout( TIMEOUT *t, clock_t clk )
{
  static bit b1;
  static bit b2;
  static bit b3;

  if ( !t->is_set )
    return 0;

  b1 = clk > t->stop;
  b2 = clk < t->start;

  if ( t->stop >= t->start )
    b3 = b1 | b2;
  else
    b3 = b1 & b2;

  if ( b3 )
    t->is_set = 0;

  return b3;

}  // check_timeout



/*HDR*/
/**
 * @brief Check if a ::TM_GPS structure contains a valid date and time
 *
 * @param[in]  tm   The date/time structure to be checked
 *
 * @return 0 if date/time is valid, else a negative number indicating
 *         which field was found invalid
 */
int err_tm( const TM_GPS *tm )
{
  if ( (ushort) tm->sec > 60 )  // also accept leap second
    return -2;

  if ( (ushort) tm->min > 59 )
    return -3;

  if ( (ushort) tm->hour > 59 )
    return -4;


  if ( (ushort) tm->wday > 6 )
    return -5;

  if ( (ushort) tm->yday > 366 )  // also accept leap year
    return -6;


  if ( tm->mday < 1 || tm->mday > 31 )
    return -7;

  if ( tm->month < 1 || tm->month > 12 )
    return -8;

  if ( tm->year < 0 || tm->year > 9999 )
    return -9;

  return 0;  // no error

}  // err_tm



/*HDR*/
/**
 * @brief Set the time in a ::TM_GPS structure to 00:00:00.000
 *
 * @param[in,out]  tm   The date/time structure to be set
 *
 * @return Pointer to the ::TM_GPS structure that has been passed
 */
TM_GPS *clear_time( TM_GPS *tm )
{
  tm->frac = tm->sec = tm->min = tm->hour = 0;

  return tm;

}  // clear_time



/*HDR*/
/**
 * @brief Convert second-of-week to day-of-week and time-of-day
 *
 * @param[in]   wsec  The second-of-week number to be converted.
 *                    Must not be negative.
 * @param[out]  tm    Address of a ::TM_GPS structure which takes
 *                    the computed results. Updates the fields
 *                    ::TM_GPS::hour, ::TM_GPS::min, ::TM_GPS::sec,
 *                    and ::TM_GPS::wday in the range 0..6, with
 *                    0 = Sunday.
 *
 * @return  Pointer to the ::TM_GPS structure that has been passed
 *
 * @see ::tm_to_wsec
 * @see ::day_of_week_sun06
 */
TM_GPS *wsec_to_tm( long wsec, TM_GPS *tm )
{
  ldiv_t ldt;
  div_t dt;


  ldt = ldiv( wsec, SECS_PER_DAY );
  tm->wday = (int8_t) ldt.quot;

  ldt.rem = MOD_CORR( ldt.rem );
  ldt = ldiv( ldt.rem, SECS_PER_HOUR );
  tm->hour = (int8_t) ldt.quot;

  ldt.rem = MOD_CORR( ldt.rem );
  dt = div( (int) ldt.rem, SECS_PER_MIN );
  tm->min = (int8_t) dt.quot;
  tm->sec = (int8_t) dt.rem;

  return tm;

}  // wsec_to_tm



/*HDR*/
/**
 * @brief Compute second-of-week from day-of-week and time-of-day
 *
 * @todo Specify input / output ranges
 *
 * @param[in]  tm  Address of a ::TM_GPS structure providing day-of-week and time-of-day
 *
 * @return The computed second-of-week number
 *
 * @see ::wsec_to_tm
 */
long tm_to_wsec( const TM_GPS *tm )
{
  long l;

  l = (long) tm->wday * SECS_PER_DAY
    + (long) tm->hour * SECS_PER_HOUR
    + (long) tm->min * SECS_PER_MIN
    + (long) tm->sec;

  return l;

}  // tm_to_wsec



/*HDR*/
/**
 * @brief Check if a specific year is a leap year
 *
 * @param[in]  y  The full year number
 *
 * @return  != 0 if the year is a leap year, else 0
 */
int is_leap_year( int y )
{
  return ( ( ( ( y % 4 ) == 0 ) && ( ( y % 100 ) != 0 ) ) || ( ( y % 400 ) == 0 ) );

}  // is_leap_year



/*HDR*/
/**
 * @brief Compute the day-of-year from a given date
 *
 * @param[in]  day    The day-of-month
 * @param[in]  month  The month
 * @param[in]  year   The full year number
 *
 * @return The computed day-of-year
 */
int day_of_year( int day, int month, int year )
{
  register const char *t;

  t = days_of_month[ is_leap_year( year ) ];

  while ( --month )
    day += *t++;

  return day;

}  // day_of_year



/*HDR*/
/**
 * @brief Compute a date from a given year and day-of-year
 *
 * @param[in]   year     The full year number
 * @param[in]   day_num  Number of days from the beginning of that year, may be negative
 * @param[out]  tm       Address of a ::TM_GPS structure which takes the computed results
 */
void date_of_year( int year, int day_num, TM_GPS *tm )
{
  register int i;
  register const char *t;

  while ( day_num < 1 )
    day_num += day_of_year( 31, 12, --year );

  t = days_of_month[is_leap_year( year )];


  for ( i = 1; i < 13 && day_num > *t ; i++ )
    day_num -= *t++;

  if ( i > 12 )
    date_of_year( ++year, day_num, tm );
  else
  {
    tm->mday = (int8_t) day_num;
    tm->month = (int8_t) i;
    tm->year = (int16_t) year;
  }

}  // date_of_year



/*HDR*/
/**
 * @brief Compute day-of-week for a given date.
 *
 * ATTENTION: The computed day-of-week is in the range 0..6,
 * with 0 = Monday (!).
 *
 * In most cases the function ::day_of_week_sun06 is
 * more suitable for applications.
 *
 * @param[in]  day    The day-of-month, 0..31
 * @param[in]  month  The month, 1..12
 * @param[in]  year   The full year number
 *
 * @return The computed day-of-week, 0..6, 0 = Monday (!)
 *
 * @see ::day_of_week_sun06
 * @see ::n_days_since_year_0
 */
int day_of_week( int day, int month, int year )
{
  long l = n_days_since_year_0( day, month, year );

  return (int) ( l % DAYS_PER_WEEK );

}  // day_of_week



/*HDR*/
/**
 * @brief Compute day-of-week for a given date.
 *
 * The computed day-of-week is in the range 0..6,
 * with 0 = Sunday, as expected by most applications.
 *
 * @param[in]  day    The day-of-month, 0..31
 * @param[in]  month  The month, 1..12
 * @param[in]  year   The full year number
 *
 * @return The computed day-of-week, 0..6, with 0 = Sunday.
 *
 * @see ::n_days_since_year_0
 * @see ::wsec_to_tm
 */
int day_of_week_sun06( int day, int month, int year )
{
  long l = n_days_since_year_0( day, month, year ) + 1;

  return (int) ( l % DAYS_PER_WEEK );

}  // day_of_week_sun06



/*HDR*/
/**
 * @brief Update a year number by a number of days, accounting for leap years
 *
 * @param[in]  day_num  The number of days to evaluate
 * @param[in]  year     The year number to start with
 *
 * @return The computed year number
 */
int days_to_years( long *day_num, int year )
{
  for (;;)
  {
    int yearday = is_leap_year( year ) ? 366 : 365;

    if ( *day_num < yearday )
      return year;

    *day_num -= yearday;

    year++;
  }

}  // days_to_years



/*HDR*/
/**
 * @brief Compute number of days after Jan 1, 0000 for a given date
 *
 * @param[in]  mday   The day-of-month, [1..31]
 * @param[in]  month  The month, [1..12].
 * @param[in]  year   The full year number, starting from year 0.
 *
 * @return The computed number of days
 *
 * @see ::day_of_week
 */
long n_days_since_year_0( int mday, int month, int year )
{
  long l;
  long lyear = year - 1;

  l = lyear * 365L  // days until beginning of year, 365 days per year
    + lyear / 4L    // plus number of leap days
    - lyear / 100L  // except years modulo 100 are no leap years
    + lyear / 400L  // but years modulo 400 are leap years anyway
    + (long) day_of_year( mday, month, year ) - 1L;   // plus number of days until specified date

  return l;

}  // n_days_since_year_0



/*HDR*/
/**
 * @brief Search a table of known past leap second dates for a specific week and day number.
 *
 * Optionally we return the latest week number we
 * have found in the table, so an application can
 * start there searching there for future potential
 * leap second dates.
 *
 * @ingroup group_true_gps_wn_fncs
 */
int find_past_gps_wn_lsf_from_table( GPS_WNUM *p_wn, GPS_DNUM dn_t, int srch_all, GPS_WNUM *p_wn_last )
{
  static const LS_TABLE_ENTRY_GPS tbl[] = KNOWN_LEAP_SECOND_INFO_GPS;

  GPS_WNUM wn_srch;
  GPS_DNUM dn_srch;
  const LS_TABLE_ENTRY_GPS *p;
  int n_found = 0;
  GPS_WNUM wn_last = 0;

  #if DEBUG_WNLSF
    printf( "WNlsf %i (0x%04X), DNt %i (tbl):", *p_wn, *p_wn, dn_t );
  #endif

  // While the table entries are normalized (like 1930|0),
  // the wn|dn pair to be looked up may not be normalized
  // (like 1929|7), so we might miss a matching table entry
  // unless we also normalize the search pattern.
  wn_srch = *p_wn;
  dn_srch = dn_t;
  normalize_gps_wn_dn( &wn_srch, &dn_srch );

  // We only use the 8 bit truncated week number
  // to search the table.
  wn_srch &= 0xFF;

  for ( p = tbl; p->wn || p->dn || p->gps_tai_offs; p++ )
  {
    wn_last = p->wn;

    #if DEBUG_WNLSF
      printf( "\n  wn %i (0x%04X), dn %i", p->wn, p->wn, p->dn );
    #endif

    if ( p->dn != dn_srch )
      continue;

    if ( ( p->wn & 0xFF ) != wn_srch )
      continue;

    // Found a matching wn|dn pair.

    #if DEBUG_WNLSF
      printf( "  MATCH" );
    #endif

    n_found++;

    // The day number is explicit, so we only have to
    // update the week number provided by the caller.

    // If the caller has searched for day number 7,
    // we have to adjust the full week number
    // from the table accordingly.
    *p_wn = (GPS_WNUM) ( ( dn_t == 7 ) ? ( p->wn - 1 ) : p->wn );

    // If the first search result is sufficient
    // for the application, we can stop searching.
    if ( !srch_all )
      break;
  }

  #if DEBUG_WNLSF
    printf( "\n" );
    printf( "  found %i, last wn %i (0x%04X)\n", n_found, wn_last, wn_last );
  #endif

  // Optionally we return the latest week number we
  // have found in the table, so an application can
  // start there searching for future potential
  // leap second dates.
  if ( p_wn_last )
    *p_wn_last = wn_last;

  return n_found;

}  // find_past_gps_wn_lsf_from_table



#if _IS_MBG_FIRMWARE

// Old sprint_...() functions used by existing firmware.
// Should be replaced by snprint_...() variants implemented
// using snprint_safe(), etc.
// Similar for

/*HDR*/
/**
 * @brief Print time with hours, minutes, seconds to a string
 *
 * @param[out]  s   Address of a string buffer to be filled
 * @param[in]   tm  Address of a ::TM_GPS structure providing date and time
 */
int sprint_time( char *s, const TM_GPS *tm )
{
  return sprintf( s, time_fmt, tm->hour, tm->min, tm->sec );

}  // sprint_time


/*HDR*/
/**
 * @brief Print time with hours, minutes to a string
 *
 * @param[out]  s   Address of a string buffer to be filled
 * @param[in]   tm  Address of a ::TM_GPS structure providing date and time
 */
int sprint_short_time( char *s, const TM_GPS *tm )
{
  return sprintf( s, short_time_fmt, tm->hour, tm->min );

}  // sprint_short_time


/*HDR*/
/**
 * @brief Print date to a string
 *
 * @param[out]  s   Address of a string buffer to be filled
 * @param[in]   tm  Address of a ::TM_GPS structure providing date and time
 */
int sprint_date( char *s, const TM_GPS *tm )
{
  return sprintf( s, date_fmt, tm->mday, tm->month, tm->year );

}  // sprint_date


/*HDR*/
/**
 * @brief Print day-of-week and date to a string
 *
 * @param[out]  s   Address of a string buffer to be filled
 * @param[in]   tm  Address of a ::TM_GPS structure providing date and time
 */
int sprint_day_date( char *s, const TM_GPS *tm )
{
  return sprintf( s, day_date_fmt, day_name_eng[(uchar) tm->wday], tm->mday, tm->month, tm->year );

}  // sprint_day_date


/*HDR*/
/**
 * @brief Print day-of-week, date and time to a string
 *
 * @param[out]  s   Address of a string buffer to be filled
 * @param[in]   tm  Address of a ::TM_GPS structure providing date and time
 */
int sprint_tm( char *s, const TM_GPS *tm )
{
  return sprintf( s, "%s, %2i.%02i.%04i  %2i:%02i:%02i",
                  day_name_eng[(uchar)tm->wday],
                  tm->mday, tm->month, tm->year,
                  tm->hour, tm->min, tm->sec );

}  // sprint_tm

#endif  // _IS_MBG_FIRMWARE



/*HDR*/
/**
 * @brief Extract a time from a string
 *
 * @param[in]   s   A time string in format hh:mm:ss
 * @param[out]  tm  Address of a ::TM_GPS structure which takes the extracted time
 */
void sscan_time( const char *s, TM_GPS *tm )
{
  tm->hour = (int8_t) atoi( &s[0] );
  tm->min = (int8_t) atoi( &s[3] );
  tm->sec = (int8_t) atoi( &s[6] );

}  // sscan_time



/*HDR*/
/**
 * @brief Extract a date from a string
 *
 * @param[in]   s   A date string in format dd.mm. or dd.mm.yyyy
 * @param[out]  tm  Address of a ::TM_GPS structure which takes the extracted date
 */
void sscan_date( char *s, TM_GPS *tm )
{
  tm->mday = (int8_t) atoi( &s[0] );
  tm->month = (int8_t) atoi( &s[3] );

  if ( _isdigit( s[6] ) )
    tm->year = (int16_t) atoi( &s[6] );

}  // sscan_date


