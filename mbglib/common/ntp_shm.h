
/**************************************************************************
 *
 *  $Id: ntp_shm.h 1.4 2021/03/23 20:12:46 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for ntp_shm.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: ntp_shm.h $
 *  Revision 1.4  2021/03/23 20:12:46  martin
 *  Call the logging function via a function pointer that defaults to
 *  syslog() but can be changed to point to an application-specific function.
 *  Also updated some comments.
 *  Revision 1.3  2017/07/05 16:52:25  martin
 *  Defined MAX_SHM_UNIT_OFFSET.
 *  Doxygen changes and fixes.
 *  Updated function prototypes.
 *  Revision 1.2  2013/01/02 10:23:41  daniel
 *  Added nsec support
 *  Revision 1.1  2012/05/29 09:54:15  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _NTP_SHM_H
#define _NTP_SHM_H


/* Other headers to be included */

#include <mbg_syslog.h>

#include <syslog.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#ifdef _NTP_SHM
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if 0 && defined( _USE_PACK )  // Use default alignment.
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup group_ntp_defs NTP interface definitions
 *
 * @note These definitions have been copied from the NTP source code (http://www.ntp.org)
 *
 * @{ */

/**
 * @brief Structure of NTP's shared memory segment.
 */
struct shmTime
{
  /**
   * @brief Mode of the SHM segment.
   *
   * Determines how the SHM segment is to be evaluated.
   *
   * mode 0: If @p valid is set, use the values, then clear @p valid.
   *
   * mode 1: If @p valid is set, and @p count before and after read
   *         of the values is equal, use values, then clear @p valid.
   */
  int    mode;

  int    count;                  ///< Incremented whenever the structure is evaluated or updated.
  time_t clockTimeStampSec;      ///< Seconds from external clock.
  int    clockTimeStampUSec;     ///< Microseconds from external clock.
  time_t receiveTimeStampSec;    ///< Seconds from internal clock, when external value was received.
  int    receiveTimeStampUSec;   ///< Microsseconds from internal clock, when external value was received.
  int    leap;                   ///< See @ref NTP_LEAP_BITS.
  int    precision;
  int    nsamples;
  int    valid;
  unsigned clockTimeStampNSec;    ///< Nanoseconds from external clock.
  unsigned receiveTimeStampNSec;  ///< Nanoseconds from internal clock, when external value was received.
  int    dummy[8];
};

/**
 * @defgroup NTP_LEAP_BITS NTP Leap Bits
 *
 * @note This describes the NTP leap bits.
 *
 * @{ */

#define LEAP_NOWARNING  0x0  ///< Normal, no leap second warning.
#define LEAP_ADDSECOND  0x1  ///< Last minute of day has 61 seconds.
#define LEAP_DELSECOND  0x2  ///< Last minute of day has 59 seconds.
#define LEAP_NOTINSYNC  0x3  ///< Clock is not synchronized.

/** @} defgroup NTP_LEAP_BITS */



/**
 * @brief The number of units (refclocks) supported by the SMH segment.
 */
#define MAX_SHM_REFCLOCKS    4


/**
 * @brief Max. Number of SHM unit offset.
 */
#define MAX_SHM_UNIT_OFFSET  128


/**
 * @brief Basic SHM unit identifier (unit 0).
 */
#define NTPD_BASE    0x4e545030    ///< "NTP0"

/** @} defgroup group_ntp_defs */



/**
 * @brief Pointer to the log function used by @a ntp_shm.
 *
 * Defaults to @a syslog, but can be changed to a different function
 * that is called in the same way.
 */
_ext MBG_SYSLOG_FNC *ntp_shm_log_fnc
#if defined( _DO_INIT )
  = syslog
#endif
;



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 struct shmTime *getShmTime( int unit ) ;
 int ntpshm_init( struct shmTime **shmTime, int n_units, int n_unit0 ) ;

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT

#endif  /* _NTP_SHM_H */

