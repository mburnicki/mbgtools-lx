
/**************************************************************************
 *
 *  $Id: cmp_time_util.h 1.6 2022/12/21 14:49:56 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for cmp_time_util.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: cmp_time_util.h $
 *  Revision 1.6  2022/12/21 14:49:56  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameters from a function.
 *  Revision 1.5  2021/03/22 17:37:32  martin
 *  Updated some comments.
 *  Revision 1.4  2021/03/12 14:20:20  martin
 *  Use the new global variable pc_cycles_frequency.
 *  Revision 1.3  2020/11/04 17:17:37  martin
 *  Added some functions to support checking the continuity of the timestamps
 *  and status sequentially read from a device.
 *  Revision 1.2  2018/08/08 08:23:28  martin
 *  Moved some functions and structures to more convenient files.
 *  Fixed Windows build.
 *  Updated function prototypes.
 *  Revision 1.1  2013/07/25 10:42:59  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _CMP_TIME_UTIL_H
#define _CMP_TIME_UTIL_H


/* Other headers to be included */

#include <mbgdevio.h>
#include <deviohlp.h>

#ifdef _CMP_TIME_UTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if 0 && defined( _USE_PACK )  // use default alignment
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif


#ifdef __cplusplus
extern "C" {
#endif


#if defined( MBG_TGT_WIN32 )
  #define snprintf _snprintf
#endif



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Check if both devices support fast HR timestamps.
 *
 * @param[in]  dh1          Handle of the first device.
 * @param[in]  dh2          Handle of the second device.
 * @param[in]  err_msg_fnc  Pointer to a function to be called in case of error.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int chk_fast_tstamp_supp( MBG_DEV_HANDLE dh1, MBG_DEV_HANDLE dh2, MBG_ERR_MSG_FNC err_msg_fnc ) ;

 /**
 * @brief Read timestamps and cycles from both devices.
 *
 * @param[in]   dh1        Handle of the first device.
 * @param[out]  p_htc1     Timestamp and cycles read from the first device.
 * @param[in]   dh2        Handle of the second device.
 * @param[out]  p_htc2     Timestamp and cycles read from the first device.
 * @param[in]   read_fast  Flag indicating that fast timestamps should be read.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int get_htc_timestamps( MBG_DEV_HANDLE dh1, PCPS_HR_TIME_CYCLES *p_htc1, MBG_DEV_HANDLE dh2, PCPS_HR_TIME_CYCLES *p_htc2, int read_fast ) ;

 /**
 * @brief Compute the delta cycles and time difference.
 *
 * @param[in]   p_htc        Timestamp/cycles t from device under test.
 * @param[in]   p_htc_ref    Timestamp/cycles t_ref from reference device.
 * @param[out]  p_delta_ts   Pointer to save time difference (t - tref), can be @a NULL.
 * @param[out]  p_delta_cyc  Pointer to save delta cycles (t - tref), can be @a NULL.
 *
 * @return The difference between delta_t and delta_cycles.
 */
 double get_htc_delta( const PCPS_HR_TIME_CYCLES *p_htc, const PCPS_HR_TIME_CYCLES *p_htc_ref, double *p_delta_ts, double *p_delta_cyc ) ;

 /**
 * @brief Print a HR time stamp, and optionally a status and time difference.
 *
 * @param[in]  p_ts  Pointer to the time stamp.
 * @param[in]  p_st  Pointer to the status code, optional, can be @a NULL.
 * @param[in]  p_d   Pointer to the variable with time difference in microseconds, can be @a NULL.
 *
 * @see ::mbg_check_continuity
 */
 void mbg_show_delta_exceeded( const PCPS_TIME_STAMP *p_ts, const PCPS_TIME_STATUS_X *p_st, const double *p_d ) ;

 /**
 * @brief Check continuity of 2 subsequent time stamps and status codes.
 *
 * Compares the current timestamp to the previous timestamp,
 * and optionally, the current sttus to the previous status.
 *
 * Calls ::mbg_show_delta_exceeded if the time difference
 * is less than 0, or exceeeds @p max_allowed_delta_us,
 * or if the status has changed.
 *
 * @param[in]  p_ts                  Pointer to the current time stamp.
 * @param[in]  p_prv_ts              Pointer to the previous time stamp.
 * @param[in]  p_st                  Pointer to the current status code, optional, can be @a NULL.
 * @param[in]  p_prv_st              Pointer to the previous status code, optional, can be @a NULL.
 * @param[in]  max_allowed_delta_us  Maximum allowed time difference, in microseconds.
 *
 * @see ::mbg_show_delta_exceeded
 */
 void mbg_check_continuity( const PCPS_TIME_STAMP *p_ts, const PCPS_TIME_STAMP *p_prv_ts, const PCPS_TIME_STATUS_X *p_st, const PCPS_TIME_STATUS_X *p_prv_st, double max_allowed_delta_us ) ;

 /**
 * @brief Print %UTC date and time from a ::PCPS_TIME_STAMP structure to a string.
 *
 * This function is mostly similar to ::mbg_snprint_hr_tstamp, but optionally
 * appends the raw timestamp instead of inserting it at the beginning.
 *
 * @param[out]  s          Address of a string buffer to be filled.
 * @param[in]   max_len    Size of the string buffer.
 * @param[in]   p          Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]   print_raw  Flag indicating if a raw timestamp (hex) is to be appended.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_snprint_hr_tstamp
 * @see ::mbg_snprint_hr_time_loc
 */
 int mbg_snprint_hr_tstamp_ext( char *s, int max_len, const PCPS_TIME_STAMP *p, int print_raw ) ;

 /**
 * @brief Print local date and time from a ::PCPS_TIME_STAMP structure to a string.
 *
 * This function is mostly similar to ::mbg_snprint_hr_tstamp, but optionally
 * appends the raw timestamp instead of inserting it at the beginning.
 *
 * @param[out]  s          Address of a string buffer to be filled.
 * @param[in]   max_len    Size of the string buffer.
 * @param[in]   p          Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_snprint_hr_tstamp
 * @see ::mbg_snprint_hr_tstamp_ext
 */
 int mbg_snprint_hr_time_loc( char *s, size_t max_len, const PCPS_HR_TIME *p ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT

#endif  /* _CMP_TIME_UTIL_H */

