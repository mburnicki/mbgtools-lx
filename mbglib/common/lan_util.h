
/**************************************************************************
 *
 *  $Id: lan_util.h 1.11 2022/08/26 13:55:30 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for lan_util.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: lan_util.h $
 *  Revision 1.11  2022/08/26 13:55:30  martin.burnicki
 *  Changed the return type of some snprint_... functions from size_t to int.
 *  Revision 1.10  2021/03/22 18:07:28  martin
 *  Updated a bunch of comments.
 *  Revision 1.9  2018/02/28 16:55:31  martin
 *  Updated function prototypes.
 *  Revision 1.8  2017/10/05 08:20:40  daniel
 *  Introduced check_port_status() function
 *  Revision 1.7  2017/07/05 14:10:58  martin
 *  Some definitions used with IPv6 added by hannes.
 *  Removed definitions of old MBG_LU_... return codes.
 *  Standard MBG_RETURN_CODES are now used instead.
 *  Updated function prototypes.
 *  Revision 1.6  2014/09/24 08:32:58  martin
 *  Fixed a POSIX header file dependency.
 *  Revision 1.5  2013/10/02 07:20:36  martin
 *  Updated function prototypes.
 *  Revision 1.4  2013/02/19 15:15:53  martin
 *  Added some inline functions.
 *  Redefined return codes as named enum.
 *  Updated function prototypes.
 *  Revision 1.3  2012/10/02 18:24:29  martin
 *  Added some macros to simpliy conversion to string.
 *  Revision 1.2  2012/03/09 08:51:44  martin
 *  Updated function prototypes.
 *  Revision 1.1  2011/03/04 10:01:32  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _LAN_UTIL_H
#define _LAN_UTIL_H


/* Other headers to be included */

#include <mbg_tgt.h>
#include <gpsdefs.h>   // For some Meinberg data structures.

#include <stdlib.h>

#if defined( MBG_TGT_POSIX )
  #include <sys/types.h>
  #include <sys/socket.h>
  #include <net/if.h>
  #include <ifaddrs.h>  // *Must* be included *after* net/if.h.
#else
  // A dummy declaration to prevent from warnings due to usage
  // of this type with function prototypes.
  struct ifreq
  {
    int dummy;
  };
#endif


#if defined( IFHWADDRLEN )  // Usually defined in net/if.h.
  #if ( IFHWADDRLEN != 6 )
    #error Warning: IFHWADDRLEN is not 6!
  #endif
#endif



#ifdef _LAN_UTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if defined( _USE_PACK )
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif

#ifdef __cplusplus
extern "C" {
#endif


#if !defined( MAC_SEP_CHAR )
  #define MAC_SEP_CHAR      ':'   ///< Character used to separate octets of a MAC ID.
#endif

#if !defined( MAC_SEP_CHAR_ALT )
  #define MAC_SEP_CHAR_ALT  '-'   ///< Alternate character used to separate octets of a MAC ID.
#endif


#define MAX_IP4_BITS  ( 8 * (int) sizeof( IP4_ADDR ) )

#define IP4_MSB_MASK  ( 1UL << ( MAX_IP4_BITS - 1 ) )

#define MIN_IP4_CIDR_NETMASK_BITS  0
#define MAX_IP4_CIDR_NETMASK_BITS  MAX_IP4_BITS


#define IP6_MSB_MASK ( 1UL << ( 8 - 1 ) )

#define MIN_IP6_CIDR_NETMASK_BITS  0
#define MAX_IP6_CIDR_NETMASK_BITS  IP6_ADDR_BITS



/**
 * @brief Compute an IP4 net mask according to the number of CIDR netmask bits.
 *
 * E.g. the 24 bits mentioned in "172.16.3.250/24" result in 0xFFFFFF00,
 * corresponding to 255.255.255.0 in dotted quad notation.
 *
 * @param[in]  netmask_bits  Number of netmask bits from CIDR notation.
 *
 * @return  The IPv4 net mask.
 *
 * @see ::get_ip4_net_mask_bits
 */
static __mbg_inline
IP4_ADDR ip4_net_mask_from_cidr( int netmask_bits )
{
  return (IP4_ADDR) ~( ( 1UL << ( MAX_IP4_BITS - netmask_bits ) ) - 1 );

}  // ip4_net_mask_from_cidr



/**
 * @brief Determine the broadcast address for an IP4 address plus net mask.
 *
 * E.g. IP 0xAC1003FA, net mask 0xFFFFFF00 yields broadcast addr 0xAC1003FF.
 * In dotted quad notation, IP 172.16.3.250 with net mask 255.255.255.0
 * results in broadcast address 172.16.3.255.
 *
 * @param[in]  p_addr  The full IP4 address.
 * @param[in]  p_mask  The IP4 net mask.
 *
 * @return  The determined IP4 broadcast address.
 */
static __mbg_inline
IP4_ADDR ip4_broad_addr_from_addr( const IP4_ADDR *p_addr, const IP4_ADDR *p_mask )
{
  return *p_addr | ~(*p_mask);

}  // ip4_broad_addr_from_addr



/**
 * @brief Determine the network part of an IP4 address based on the net mask.
 *
 * E.g. IP 0xAC1003FA, net mask 0xFFFFFF00 yields network part 0xAC100300.
 * In dotted quad notation, IP 172.16.3.250 with net mask 255.255.255.0
 * results in network part 172.16.3.0.
 *
 * @param[in]  p_addr  The full IP4 address.
 * @param[in]  p_mask  The IP4 net mask.
 *
 * @return  The network part of the IP4 address.
 */
static __mbg_inline
IP4_ADDR ip4_net_part_from_addr( const IP4_ADDR *p_addr, const IP4_ADDR *p_mask )
{
  return *p_addr & *p_mask;

}  // ip4_net_part_from_addr



/**
 * @brief Check if two IP4 addresses have the same network part.
 *
 * @param[in]  p_addr1  The first IP4 address to check.
 * @param[in]  p_addr2  The second IP4 address to check.
 * @param[in]  p_mask   The IP4 net mask.
 *
 * @return  @a true, if the network parts are matching, else @a false.
 */
static __mbg_inline
int ip4_net_part_matches( const IP4_ADDR *p_addr1, const IP4_ADDR *p_addr2,
                          const IP4_ADDR *p_mask )
{
  return ip4_net_part_from_addr( p_addr1, p_mask )
      == ip4_net_part_from_addr( p_addr2, p_mask );

}  // ip4_net_part_matches



#define _ip4_addr_to_str( _s, _a ) \
  snprint_ip4_addr( _s, sizeof( _s ), _a, NULL )

#define _mac_addr_to_str( _s, _a ) \
  snprint_mac_addr( _s, sizeof( _s ), _a )



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Count the number of sequential bits in an IPv4 net mask.
 *
 * Counting starts from MSB, i.e. for 0xC0 and 0xC1 the results.
 * are both 2 since only the 2 MSBs are sequentially set.
 *
 * @param[in]  p_mask  The IPv4 net mask to be evaluated.
 *
 * @return The number of sequential MSB bits set in @p *p_mask.
 *
 * @see ::ip4_net_mask_from_cidr
 */
 int get_ip4_net_mask_bits( const IP4_ADDR *p_mask ) ;

 /**
 * @brief Print an IPv4 address to a dotted quad formatted string.
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv4 address to be evaluated.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip4_cidr_addr
 * @see ::str_to_ip4_addr
 * @see ::cidr_str_to_ip4_addr_and_net_mask
 */
 int snprint_ip4_addr( char *s, size_t max_len, const IP4_ADDR *p_addr, const char *info ) ;

 /**
 * @brief Print an IPv4 address plus net mask in CIDR notation to a string.
 *
 * The printed CIDR string is something like "172.16.3.250/24"
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv4 address to be evaluated.
 * @param[in]  p_mask   The associated IPv4 net mask.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip4_addr
 * @see ::str_to_ip4_addr
 * @see ::cidr_str_to_ip4_addr_and_net_mask
 */
 int snprint_ip4_cidr_addr( char *s, size_t max_len, const IP4_ADDR *p_addr, const IP4_ADDR *p_mask, const char *info ) ;

 /**
 * @brief Convert a string to an ::IP4_ADDR.
 *
 * If the output parameter is specified as @a NULL, this function can be used
 * to check if the IPv4 address string is formally correct.
 *
 * @param[out] p_addr  Pointer to an ::IP4_ADDR variable to be filled, or @a NULL.
 * @param[in]  s       An IPv4 address string to be converted.
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip4_addr
 * @see ::snprint_ip4_cidr_addr
 * @see ::cidr_str_to_ip4_addr_and_net_mask
 */
 int str_to_ip4_addr( IP4_ADDR *p_addr, const char *s ) ;

 /**
 * @brief Convert a string in CIDR notation to an ::IP4_ADDR and net mask.
 *
 * If the output parameters are specified as @a NULL, this function
 * can be used to check if the CIDR string is formally correct.
 *
 * @param[out] p_addr    Pointer to an ::IP4_ADDR variable to be filled with
 *                       the IPv4 address, or @a NULL.
 * @param[out] p_mask    Pointer to an ::IP4_ADDR variable to be filled with
 *                       the IPv4 net mask, or @a NULL.
 * @param[in]  cidr_str  The string to be converted, in CIDR format, e.g. "172.16.3.250/24".
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip4_addr
 * @see ::snprint_ip4_cidr_addr
 * @see ::str_to_ip4_addr
 */
 int cidr_str_to_ip4_addr_and_net_mask( IP4_ADDR *p_addr, IP4_ADDR *p_mask, const char *cidr_str ) ;

 /**
 * @brief Count the number of sequential bits in an IPv6 net mask.
 *
 * Counting starts from MSB, i.e. for 0xC0 and 0xC1 the results
 * are both 2 since only the 2 MSBs are sequentially set.
 *
 * @param[in]  p_mask  The IPv6 net mask to be evaluated.
 *
 * @return The number of sequential MSB bits set in @p *p_mask.
 *
 * @see ::ip6_net_mask_from_cidr
 */
 int get_ip6_net_mask_bits( const IP6_ADDR *p_mask ) ;

 /**
 * @brief Print an IPv6 address in optimized format to a string.
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv6 address to be evaluated.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip6_cidr_addr
 * @see ::snprint_ip6_cidr_mask_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
 int snprint_ip6_addr( char *s, size_t max_len, const IP6_ADDR *p_addr, const char *info ) ;

 /**
 * @brief Print an IPv6 address plus net mask to string in CIDR notation.
 *
 * The printed CIDR string is something like "2001:0DB8:0:CD30:EF45::/64".
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv6 address to be evaluated.
 * @param[in]  p_mask   The associated IPv6 net mask.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_mask_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
 int snprint_ip6_cidr_addr( char *s, size_t max_len, const IP6_ADDR *p_addr, const IP6_ADDR *p_mask, const char *info ) ;

 /**
 * @brief Print an IPv6 address plus number of net mask bits to string in CIDR notation.
 *
 * The printed CIDR string is something like "2001:0DB8:0:CD30:EF45::/64".
 *
 * @param[out] s               The string buffer to be filled.
 * @param[in]  max_len         Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr          The IPv6 address to be evaluated.
 * @param[in]  cidr_mask_bits  The CIDR number of bits specifying the IPv6 net mask.
 * @param[in]  info            An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
 int snprint_ip6_cidr_mask_addr( char *s, size_t max_len, const IP6_ADDR *p_addr, const int cidr_mask_bits, const char* info ) ;

 /**
 * @brief Convert a string to an ::IP6_ADDR.
 *
 * If the output parameter is specified as @a NULL, this function
 * can be used to check if the string is formally correct.
 *
 * On success ::IP6_ADDR variable contains the IPv6 address
 * in little endian byte order.
 *
 * @param[out] p_addr  Pointer to the ::IP6_ADDR variable, or @a NULL.
 * @param[in]  s       A string containing an IPv6 address.
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_addr
 * @see ::snprint_ip6_cidr_mask_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
 int str_to_ip6_addr( IP6_ADDR *p_addr, const char *s ) ;

 /**
 * @brief Convert a string in CIDR notation to an ::IP6_ADDR and net mask.
 *
 * If the output parameters are specified as @a NULL, this function
 * can be used to check if the CIDR string is formally correct.
 *
 * @param[out] p_addr  Pointer to an ::IP6_ADDR variable to be filled up
 *                     with the IPv6 address, or @a NULL.
 * @param[out] p_mask  Pointer to an ::IP6_ADDR variable to be filled up
                       with the net mask bits, or @a NULL.
 * @param[in] cidr_str The string to be converted, in CIDR format, e.g. "2001:0DB8:0:CD30::/64".
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip4_addr
 * @see ::snprint_ip4_cidr_addr
 * @see ::str_to_ip4_addr
 */
 int cidr_str_to_ip6_addr_and_net_mask( IP6_ADDR *p_addr, IP6_ADDR *p_mask, const char *cidr_str ) ;

 /**
 * @brief Convert a string in CIDR notation to an ::IP6_ADDR and net mask bits.
 *
 * If the output parameters are specified as @a NULL, this function
 * can be used to check if the CIDR string is formally correct.
 *
 * @param[out] p_addr    Pointer to an ::IP6_ADDR variable for the IPv6 address, or @a NULL.
 * @param[out] p_cidr    Pointer to an int variable for the net mask bits, or @a NULL.
 * @param[in]  cidr_str  The string to be converted, in CIDR format, e.g. "2001:0DB8:0:CD30::/64".
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_addr
 * @see ::str_to_ip6_addr
 */
 int cidr_str_to_ip6_addr_and_cidr_bits( IP6_ADDR *p_addr, int *p_cidr, const char *cidr_str ) ;

 /**
 * @brief Compute an IPv6 net mask according to the number of CIDR netmask bits.
 *
 * E.g. the 64 bits mentioned in "2001:0DB8:0:CD30::/64" result in 2^64,
 * corresponding to FFFF:FFFF:FFFF:FFFF:: in IPv6 notation.
 *
 * @param[out] p_mask        Pointer to an ::IP6_ADDR variable for the IPv6 netmask.
 * @param[in]  netmask_bits  Number of netmask bits from CIDR notation.
 *
 * @see ::get_ip6_net_mask_bits
 */
 void ip6_net_mask_from_cidr( IP6_ADDR *p_mask, int netmask_bits ) ;

 /**
 * @brief Determine the network part of an IPv6 address based on the net mask.
 *
 * E.g. IP "2001:0DB8:0:CD30::", net mask "FFFF:FFFF::" yields network part "2001:0DB8::".
 *
 * @param[out] p_net_part  The extracted network part of the IPv6 address.
 * @param[in]  p_addr      The IPv6 address to be evaluated.
 * @param[in]  p_mask      The associated IPv6 net mask.
 */
 void ip6_net_part_from_addr( IP6_ADDR *p_net_part, const IP6_ADDR *p_addr, const IP6_ADDR *p_mask ) ;

 /**
 * @brief Print a MAC ID or similar array of octets to a string.
 *
 * @param[out] s           The string buffer to be filled.
 * @param[in]  max_len     Maximum length of the string, i.e. size of the buffer.
 * @param[in]  octets      An array of octets.
 * @param[in]  num_octets  The number of octets to be printed from the array.
 * @param[in]  sep         The separator printed between the bytes, or 0.
 * @param[in]  info        An optional string which is prepended to the output, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_mac_addr
 * @see ::str_to_octets
 * @see ::octets_are_all_zero
 */
 int snprint_octets( char *s, size_t max_len, const uint8_t *octets, int num_octets, char sep, const char *info ) ;

 /**
 * @brief Print a ::PTP_CLOCK_ID to a string.
 *
 * @todo Maybe this function should be moved to a different module.
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Maximum length of the string, i.e. size of the buffer.
 * @param[in]  p        The ::PTP_CLOCK_ID to be printed.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_octets
 */
 int snprint_ptp_clock_id( char *s, size_t max_len, const PTP_CLOCK_ID *p ) ;

 /**
 * @brief Print a MAC address to a string.
 *
 * @param[out] s           The string buffer to be filled.
 * @param[in]  max_len     Maximum length of the string, i.e. size of the buffer.
 * @param[in]  p_mac_addr  The MAC address to be printed.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_octets
 * @see ::str_to_octets
 * @see ::octets_are_all_zero
 */
 int snprint_mac_addr( char *s, size_t max_len, const MBG_MAC_ADDR *p_mac_addr ) ;

 /**
 * @brief Set a MAC ID or a similar array of octets from a string.
 *
 * @param[out] octets      An array of octets to be set up.
 * @param[in]  num_octets  The number of octets that can be stored.
 * @param[in]  s           The string to be converted.
 *
 * @return  The overall number of octets decoded from the string.
 *
 * @see ::snprint_octets
 * @see ::snprint_mac_addr
 * @see ::octets_are_all_zero
 */
 int str_to_octets( uint8_t *octets, int num_octets, const char *s ) ;

 /**
 * @brief Check if an array of octets is all zero.
 *
 * @param[in]  octets      Pointer to the array of octets.
 * @param[in]  num_octets  Number of octets.
 *
 * @return  @a true if all bytes are 0, else @a false.
 *
 * @see ::snprint_octets
 * @see ::snprint_mac_addr
 * @see ::str_to_octets
 */
 bool octets_are_all_zero( const uint8_t *octets, int num_octets ) ;

 /**
 * @brief Check if a MAC address is all zero.
 *
 * @param[in]  p_addr  Pointer to a MAC address to be checked.
 *
 * @return  @a true if all bytes of the MAC address are 0, else @a false.
 *
 * @see ::octets_are_all_zero
 */
 bool mac_addr_is_all_zero( const MBG_MAC_ADDR *p_addr ) ;

 /**
 * @brief Do a SIOCGxxx IOCTL call to read specific information from a LAN interface.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[in]  ioctl_code  One of the predefined system SIOCGxxx IOCTL codes.
 * @param[out] p_ifreq     Pointer to a request buffer
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int do_siocg_ioctl( const char *if_name, int ioctl_code, struct ifreq *p_ifreq ) ;

 /**
 * @brief Retrieve the index of a specific network interface.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_intf_idx  Pointer to a variable to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_intf_idx is set to -1.
 */
 int get_port_intf_idx( const char *if_name, int *p_intf_idx ) ;

 /**
 * @brief Retrieve the MAC address of a network interface.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_mac_addr  Pointer to the MAC address buffer to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, the MAC address @p *p_mac_addr is set to all 0.
 */
 int get_port_mac_addr( const char *if_name, MBG_MAC_ADDR *p_mac_addr ) ;

 /**
 * @brief Check the link state of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 *
 * @return  1 if link detected on port,<br>
 *          0 if no link detected on port,<br>
 *          one of the (negative) @ref MBG_ERROR_CODES in case of an error.
 */
 int check_port_link( const char *if_name ) ;

 /**
 * @brief Check the state of a network interface.
 *
 * @param[in]   if_name   Name of the interface to check.
 * @param[out]  p_speed   Optional pointer to a variable to take up the link speed, may be @a NULL.
 * @param[out]  p_duplex  Optional pointer to a variable to take up the duplex state, may be @a NULL.
 *
 * @return 1 if link detected on port,<br>
 *         0 if no link detected on port,<br>
 *         one of the (negative) @ref MBG_ERROR_CODES in case of an error.
 *         In case of error, @p *p_duplex is set to -1.
 */
 int check_port_status( const char *if_name, int *p_speed, int *p_duplex ) ;

 /**
 * @brief Retrieve the IPv4 address of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p_addr   Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
 int get_port_ip4_addr( const char *if_name, IP4_ADDR *p_addr ) ;

 /**
 * @brief Retrieve the IPv4 net mask of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p_addr   Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
 int get_port_ip4_netmask( const char *if_name, IP4_ADDR *p_addr ) ;

 /**
 * @brief Retrieve the IPv4 broadcast address of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p_addr   Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
 int get_port_ip4_broad_addr( const char *if_name, IP4_ADDR *p_addr ) ;

 /**
 * @brief Retrieve the IPv4 gateway (default route).
 *
 * @param[out] p_addr  Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 */
 int get_ip4_gateway( IP4_ADDR *p_addr ) ;

 /**
 * @brief Retrieve the IPv4 address of a network interface as string.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_addr_buf  Pointer to the string buffer to be filled up.
 * @param[in]  buf_size    Size of the string buffer.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, a string "0.0.0.0" is generated.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
 int get_port_ip4_addr_str( const char *if_name, char *p_addr_buf, int buf_size ) ;

 /**
 * @brief Retrieve the IPv4 net mask of a network interface as string.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_addr_buf  Pointer to the string buffer to be filled up.
 * @param[in]  buf_size    Size of the string buffer.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, a string "0.0.0.0" is generated.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
 int get_port_ip4_netmask_str( const char *if_name, char *p_addr_buf, int buf_size ) ;

 /**
 * @brief Retrieve the IPv4 broadcast address of a network interface as string.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_addr_buf  Pointer to the string buffer to be filled up.
 * @param[in]  buf_size    Size of the string buffer.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, a string "0.0.0.0" is generated.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_specific_port_ip4_addr
 */
 int get_port_ip4_broad_addr_str( const char *if_name, char *p_addr_buf, int buf_size ) ;

 /**
 * @brief Retrieve the current IPv4 settings of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p        Pointer to a ::IP4_SETTINGS structure to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
 int get_port_ip4_settings( const char *if_name, IP4_SETTINGS *p ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT

#endif  /* _LAN_UTIL_H */

