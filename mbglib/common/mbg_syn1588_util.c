
/**************************************************************************
 *
 *  $Id: mbg_syn1588_util.c 1.3 2022/12/21 15:21:56 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Utility functions for the handling of SYN1588 PTP NICs.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_syn1588_util.c $
 *  Revision 1.3  2022/12/21 15:21:56  martin.burnicki
 *  Quieted a potential compiler warning.
 *  Revision 1.2  2022/07/06 15:09:08  martin.burnicki
 *  Support SYN1588  insync range boundary code instead of the original enum.
 *  Revision 1.1  2022/05/31 13:41:42  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#define _MBG_SYN1588_UTIL
  #include <mbg_syn1588_util.h>
#undef _MBG_SYN1588_UTIL

#include <str_util.h>



/**
 * @brief Insync boundary code indicating out-of-bounds.
 *
 * This insync boundary code is the maximum possible value
 * that can be stored in the ::PTP_SYNC_STATUS word.
 * It should be used to indicate that the boundary is out of
 * the well-defined limits, similar to ::EOver10s.
 */
#define PTP_STATE_RANGE_MAX_CODE  0x3F


static const char *ptp_state_strs[N_SYN1588_PTP_STATES] = SYN1588_PTP_STATE_STRS;


/*HDR*/
/**
 * @brief Retrieve a string representing the state of the PTP stack.
 *
 * @param[in]  state  The state of the PTP stack, see ::SYN1588_PTP_STATES.
 *
 * @return One of the strings defined in ::SYN1588_PTP_STATE_STRS.
 */
const char *mbg_syn1588_chk_get_ptp_state_str( uint state )
{
  if ( state < N_SYN1588_PTP_STATES )
    return ptp_state_strs[state];

  return "unknown";

}  // mbg_syn1588_chk_get_ptp_state_str



/*HDR*/
/**
 * @brief Write the decoded value of an insync range boundary code to a string buffer.
 *
 * @param[out]  s          Pointer to the output buffer.
 * @param[in]   max_len    Size of the output buffer.
 * @param[in]   code       The insync range boundary code, see ::PTP_STATE_RANGE_TABLE_INIT.
 * @param[in]   check_max  If @a true, ::PTP_STATE_RANGE_MAX_CODE causes special string.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
int mbg_syn1588_snprint_insync_boundary_code( char *s, size_t max_len,
                                              uint code, bool check_max )
{
  #define N_PREFIXES 4

  static const char *unit_prefixes[N_PREFIXES] =
  {
    "p",
    "n",
    "u",
    "m"
  };

  div_t dt;
  int range_idx;
  int range_mul;
  #if DEBUG_INSYNC_BOUNDARY
    int range_mul_raw;
  #endif
  long m;
  long l;
  int i;
  int n = 0;

  (void) n;  // Avoid warning "never used".

  if ( check_max && ( code >= PTP_STATE_RANGE_MAX_CODE ) )
    return snprintf_safe( s, max_len, "(out of bounds)" );

  dt = div( code, 12 );

  range_idx = dt.quot;
  range_mul = dt.rem / 4;

  if ( dt.quot > N_PREFIXES )
    range_mul += ( dt.quot - 2 );  // TODO

  #if DEBUG_INSYNC_BOUNDARY
    range_mul_raw = range_mul;
  #endif

  if ( ( dt.rem % 4 ) == 0 )
    range_mul--;

  #if DEBUG_INSYNC_BOUNDARY
    printf( "%2i", dt.quot );
    printf( " %2i", dt.rem );
    printf( " %2i", range_mul_raw );
    printf( " %2i", range_mul );
  #endif

  m = syn1588_insync_boundary_mantissa( code );

  // Take care of scale range boundaries.
  l = ( m == 0 ) ? 100 : ( m * 25 );

  for ( i = 0; i < range_mul; i++ )
    l *= 10;

  for ( i = range_mul; i < 0; i++ )
    l /= 10;

  #if OMIT_INSYNC_BOUNDARY_FRAC
    n = snprintf_safe( s, max_len, "%li %ss", l, ( range_idx < N_PREFIXES ) ?
                       unit_prefixes[range_idx] : "" );
  #else
  {
    ldiv_t ldt = ldiv( l, 10 );

    n = snprintf_safe( s, max_len, "%li", ldt.quot );

    #if !PRINT_INSYNC_BOUNDARY_FRAC
      if ( ldt.rem )
    #endif
        n += snprintf_safe( &s[n], max_len - n, ".%li", ldt.rem );

    n += snprintf_safe( &s[n], max_len - n, " %ss", ( range_idx < N_PREFIXES ) ?
                        unit_prefixes[range_idx] : "" );
  }
  #endif

  return n;

}  // mbg_syn1588_snprint_insync_boundary_code


