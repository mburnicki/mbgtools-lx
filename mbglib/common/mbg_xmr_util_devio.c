
/**************************************************************************
 *
 *  $Id: mbg_xmr_util_devio.c 1.3 2022/12/21 15:25:08 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Meinberg functions to show XMR info for bus level devices.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_xmr_util_devio.c $
 *  Revision 1.3  2022/12/21 15:25:08  martin.burnicki
 *  Removed an obsolete function parameter.
 *  Quieted some potential compiler warnings.
 *  Revision 1.2  2022/06/30 09:47:10  martin.burnicki
 *  Support force-reading XMR info and status, which is useful for debugging
 *  but can be dangerous if the device generally doesn't support XMR.
 *  Added some doxygen comments.
 *  Revision 1.1  2022/06/24 12:54:31  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#define _MBG_XMR_UTIL_DEVIO
  #include <mbg_xmr_util_devio.h>
#undef _MBG_XMR_UTIL_DEVIO

#include <mbg_xmr_util.h>

#include <stdio.h>



/*HDR*/
/**
 * @brief Force-read all XMR info into a newly or re-allocated ::ALL_XMULTI_REF_INFO.
 *
 * @note This variant of ::mbg_get_all_xmulti_ref_info function bypasses the check
 * whether XMR is supported by the device, and accesses the device in any case.
 * Calling this function with a device that generally doesn't support the XMR feature
 * may cause the device and even the whole computer to hang.
 *
 * An ::ALL_XMULTI_REF_INFO and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_INFO_IDX and ::XMR_EXT_SRC_INFO_IDX will be allocated and need
 * to be freed by calling ::free_all_xmulti_ref_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_XMULTI_REF_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::free_all_xmulti_ref_info
 */
int mbg_get_all_xmulti_ref_info_forced( MBG_DEV_HANDLE dh, ALL_XMULTI_REF_INFO **p )
{
  ALL_XMULTI_REF_INFO *xmr_info = *p;
  uint32_t data_sz;
  int rc;

  if ( xmr_info == NULL )
  {
    xmr_info = (ALL_XMULTI_REF_INFO *) calloc( 1, sizeof( *xmr_info ) );

    if ( xmr_info == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  // First, get the XMULTI_REF_INSTANCES to check how many sources are supported.
  rc = _mbgdevio_gen_read_gps( dh, PC_GPS_XMR_INSTANCES, &xmr_info->instances,
                               sizeof( xmr_info->instances ) );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  data_sz = xmr_info->instances.n_xmr_settings * sizeof( *xmr_info->infos );

  if ( xmr_info->infos == NULL )
  {
    xmr_info->infos = (XMULTI_REF_INFO_IDX *) calloc( 1, data_sz );

    if ( xmr_info->infos == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_XMR_INFO, xmr_info->infos, data_sz );

  if ( mbg_rc_is_error( rc ) )
    goto out;

  if ( mbg_rc_is_success( chk_dev_xmulti_ref_supp_ext_src_info( xmr_info ) ) )
  {
    // TODO
    // XMR_EXT_SRC_INFO_IDX can not yet be read from bus devices.
    // Therefore, remove the feature bit from this card.
    // Has to be changed as soon as low level functions are ready
    // TODO
    xmr_info->instances.flags &= ~XMRIF_MSK_EXT_SRC_INFO_SUPP;
  }

out:
  if ( mbg_rc_is_error( rc ) )
  {
    free_all_xmulti_ref_info( xmr_info );
    xmr_info = NULL;
  }

  *p = xmr_info;

  return rc;

}  // mbg_get_all_xmulti_ref_info_forced



/*HDR*/
/**
 * @brief Force-read all XMR status info into a newly or re-allocated ::ALL_XMULTI_REF_STATUS.
 *
 * @note This variant of ::mbg_get_all_xmulti_ref_status function bypasses the check
 * whether XMR is supported by the device, and accesses the device in any case.
 * Calling this function with a device that generally doesn't support the XMR feature
 * may cause the device and even the whole computer to hang.
 *
 * An ::ALL_XMULTI_REF_STATUS and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_STATUS_IDX will be allocated and need to be freed by calling
 * ::free_all_xmulti_ref_status.
 *
 * @param[in]   dh    Valid handle to a Meinberg device.
 * @param[in]   info  Pointer to the appropriate info structure.
 * @param[out]  p     Pointer to a pointer of ::ALL_XMULTI_REF_STATUS.
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::free_all_xmulti_ref_status
 */
int mbg_get_all_xmulti_ref_status_forced( MBG_DEV_HANDLE dh, const ALL_XMULTI_REF_INFO *info, ALL_XMULTI_REF_STATUS **p )
{
  ALL_XMULTI_REF_STATUS *xmr_status = *p;
  uint32_t data_sz;
  int rc;

  if ( info == NULL )
    return MBG_ERR_INV_PARM;

  if ( xmr_status == NULL )
  {
    xmr_status = (ALL_XMULTI_REF_STATUS *) calloc( 1, sizeof( *xmr_status ) );

    if ( xmr_status == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  data_sz = info->instances.n_xmr_settings * sizeof( *xmr_status->status );

  if ( xmr_status->status == NULL )
  {
    xmr_status->status = (XMULTI_REF_STATUS_IDX *) calloc( 1, data_sz );

    if ( xmr_status->status == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_XMR_STATUS, xmr_status->status, data_sz );

  if ( mbg_rc_is_error( rc ) )
    goto out;

  #if 0
    // TODO Some more detailed status info could be read here,
    // if supported by bus level devices, using code similar to this:

    if ( mbg_rc_is_success( chk_dev_xmulti_ref_supp_...() ) )
    {
      ...
    }
  #endif

out:
  if ( mbg_rc_is_error( rc ) )
  {
    free_all_xmulti_ref_status( xmr_status );
    xmr_status = NULL;
  }

  *p = xmr_status;

  return rc;

}  // mbg_get_all_xmulti_ref_status_forced



/*HDR*/
/**
 * @brief Read and show all XMR status info.
 *
 * @note If @p force is true, the device is accessed to read the XMR data
 * even if the feature flag is not set. Use this very carefully only:
 * Doing so with a device that generally doesn't support the XMR feature
 * may cause the device and even the whole computer to hang!

 * @param[in]  dh               Valid handle to a Meinberg device.
 * @param[in]  info             An optional header string to print, may be @a NULL.
 * @param[in]  indent_str       An optional indentation string, may be @a NULL.
 * @param[in]  force            If @a true, force-read XMR data even if the feature flag is not set.
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::print_all_xmr_status
 */
void show_all_xmr_status( MBG_DEV_HANDLE dh, const char *info, const char *indent_str,
                          bool force )
{
  ALL_XMULTI_REF_INFO *p_axri = NULL;
  ALL_XMULTI_REF_STATUS *p_axrs = NULL;

  int rc = force ? mbg_get_all_xmulti_ref_info_forced( dh, &p_axri ) :
           mbg_get_all_xmulti_ref_info( dh, &p_axri );

  if ( mbg_rc_is_error( rc ) )
  {
    fprintf( stderr, "Failed to read all XMR info: %s\n", mbg_strerror( rc ) );
    goto done;
  }

  rc = force ? mbg_get_all_xmulti_ref_status_forced( dh, p_axri, &p_axrs )
             : mbg_get_all_xmulti_ref_status( dh, p_axri, &p_axrs );

  if ( mbg_rc_is_error( rc ) )
  {
    fprintf( stderr, "Failed to read all XMR status: %s\n", mbg_strerror( rc ) );
    goto done;
  }


  print_all_xmr_status( p_axrs, p_axri->instances.n_xmr_settings,
                        info, indent_str );

done:
  if ( p_axri )
    free_all_xmulti_ref_info( p_axri );

  if ( p_axrs )
    free_all_xmulti_ref_status( p_axrs );

}  // show_all_xmr_status


