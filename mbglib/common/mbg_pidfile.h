
/**************************************************************************
 *
 *  $Id: mbg_pidfile.h 1.6 2021/03/26 10:25:56 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for mbg_daemonize.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_pidfile.h $
 *  Revision 1.6  2021/03/26 10:25:56  martin
 *  New global function pointer pidfile_log_fnc that initially points
 *  to syslog() and is used for all logging done by the module.
 *  Can be changed to point to a different function.
 *  Updated function prototypes and some comments.
 *  Revision 1.5  2017/05/10 15:21:37  martin
 *  Tiny cleanup.
 *  Revision 1.4  2014/07/16 15:19:55  martin
 *  Revision 1.3  2013/07/30 15:30:49  martin
 *  Revision 1.2  2013/07/24 15:29:38  martin
 *  Revision 1.1  2013/07/23 16:10:19  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_PIDFILE_H
#define _MBG_PIDFILE_H


/* Other headers to be included */

#include <mbg_syslog.h>

#include <syslog.h>


#ifdef _MBG_PIDFILE
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if 0 && defined( _USE_PACK )  // Use default alignment.
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define DEFAULT_PIDFILE_DIR  "/var/run/"

_ext int glb_pid_fd
#if defined( _DO_INIT )
 = -1
#endif
;



/**
 * @brief Pointer to the log function used by @a mbg_pidfile.
 *
 * Defaults to @a syslog, but can be changed to a different function
 * that is called in the same way.
 */
_ext MBG_SYSLOG_FNC *pidfile_log_fnc
#if defined( _DO_INIT )
  = syslog
#endif
;



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Close and remove a PID file.
 *
 * Should be called immediately before a daemon terminates,
 * and can be registered as @a atexit function.
 *
 * @see ::mbg_pidfile_open_and_lock
 * @see ::mbg_pidfile_write_own_pid
 * @see ::mbg_pidfile_register_cleanup
 */
 void mbg_pidfile_close_and_remove( void ) ;

 /**
 * @brief Register a cleanup function that is called on exit.
 *
 * Should be called immediately before ::mbg_pidfile_write_own_pid is called.
 *
 * @see ::mbg_pidfile_close_and_remove
 */
 /*HDR*/
 void mbg_pidfile_register_cleanup( void ) ;

 /**
 * @brief Try to open and lock a PID file.
 *
 * @note This function can be called before or after a program forks,
 * which is useful if only a single instance of a program may be
 * running, either as daemon or in the foreground.
 *
 * @param[in]  pid_fn  File name of the PID file, including path.
 *
 * @return The file descriptor of the PID file, or -1 on error.
 *
 * @see ::mbg_pidfile_close_and_remove
 * @see ::mbg_pidfile_write_own_pid
 */
 int mbg_pidfile_open_and_lock( const char *pid_fn ) ;

 /**
 * @brief Write own process ID to our PID file.
 *
 * @note If a program runs as daemon, i.e. forks, this function
 * has to be called <b>after</b> the fork in order to write the
 * PID of the forked process to the PID file.
 *
 * @return The number of byte written to the PID file,
 *         or a negative number on error.
 *
 * @see ::mbg_pidfile_close_and_remove
 * @see ::mbg_pidfile_open_and_lock
 */
 int mbg_pidfile_write_own_pid( void ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT

#endif  /* _MBG_PIDFILE_H */
