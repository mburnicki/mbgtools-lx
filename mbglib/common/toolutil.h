
/**************************************************************************
 *
 *  $Id: toolutil.h 1.20 2022/12/21 15:39:02 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and prototypes for toolutil.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: toolutil.h $
 *  Revision 1.20  2022/12/21 15:39:02  martin.burnicki
 *  MBG_DEV_HANDLER_FNC no longer takes a (PCPS_DEV *) parameter,
 *  so functions of this type must call feature check functions instead of
 *  using feature check macros.
 *  Revision 1.19  2021/11/15 17:09:16  martin.burnicki
 *  Updated function prototypes.
 *  Revision 1.18  2021/11/08 20:26:20  martin.burnicki
 *  Updated comment of a function prototype.
 *  Revision 1.17  2021/05/03 16:14:57  martin
 *  Provide default definition for SUPP_SERIAL.
 *  Revision 1.16  2021/04/29 14:46:45  martin
 *  Moved an inline function to mbgutil.h.
 *  Revision 1.15  2021/04/29 13:08:22  martin
 *  Moved common definitions for year limit to mbgutil.h.
 *  Revision 1.14  2021/04/12 22:03:47  martin
 *  Basic support for serial devices.
 *  Changed type of mbg_exp_year_limit to int.
 *  Revision 1.13  2021/03/22 11:25:11  martin
 *  Updated a bunch of comments.
 *  Revision 1.12  2021/03/12 10:51:33  martin
 *  Updated some doxygen comments.
 *  Revision 1.11  2020/11/04 16:59:12  martin
 *  Moved inline function delta_timestamps() to the header file
 *  and renamed it to delta_timestamp_us() because the returned
 *  value is in microseconds.
 *  Revision 1.10  2020/10/20 10:46:05  martin
 *  Updated function prototypes.
 *  Revision 1.9  2020/02/27 13:58:53  martin
 *  Updated function prototypes.
 *  Revision 1.8  2019/03/13 09:44:57  martin
 *  Moved predefined program exit codes to mbgerror.h.
 *  Revision 1.7  2018/12/14 13:14:52  martin
 *  Updated function prototypes.
 *  Revision 1.6  2018/11/15 12:21:10  martin
 *  Updated function prototypes.
 *  Revision 1.5  2018/06/25 13:47:45  martin
 *  Updated function prototypes.
 *  Revision 1.4  2017/07/05 07:38:06  martin
 *  Support Windows target.
 *  Defined function type MBG_DEV_HANDLER_FNC.
 *  Defined some OS-specific strings e.g. for example
 *  file and device names used in help messages.
 *  Check for MBG_TGT_POSIX instead of MBG_TGT_UNIX.
 *  Doxygen comments.
 *  Updated function prototypes.
 *  Revision 1.3  2012/10/15 09:36:22  martin
 *  Use common way to handle version information.
 *  Added string table with PZF state names.
 *  Updated function prototypes.
 *  Revision 1.2  2009/06/19 12:11:35  martin
 *  Updated function prototypes.
 *  Revision 1.1  2008/12/17 10:45:14  martin
 *  Initial revision.
 *  Revision 1.1  2008/12/15 08:35:08  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _TOOLUTIL_H
#define _TOOLUTIL_H


#if !defined( SUPP_SERIAL )
  #define SUPP_SERIAL 0
#endif


/* Other headers to be included */

#include <mbgdevio.h>
#include <mbgversion.h>

#if defined( MBG_TGT_POSIX )

  #include <unistd.h>

#elif defined( MBG_TGT_WIN32 )

  #include <io.h>
  #include <fcntl.h>
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <wingetopt.h>

  #define open          _open
  #define snprintf      _snprintf
  #define sleep( _x )   Sleep( (_x) * 1000 )
  #define usleep( _x )  Sleep( (_x) / 1000 )

#endif



#ifdef _TOOLUTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


enum CHK_DEV_FLAGS
{
  CHK_DEV_ALL_DEVICES = 0x0001,   ///< Call callback for all devices.
  CHK_DEV_WITHOUT_DEV = 0x0002    ///< Call callback once if no device found.
};



/**
 * @brief Masks affecting device options to be printed.
 *
 * This refers to example device names specified on the
 * command line. Used with ::mbg_print_device_options.
 *
 * @see ::mbg_print_device_options
 */
enum DEV_OPT_PRINT_MASKS
{
  DEV_OPT_PRINT_BUS_LEVEL = 0x0001,
  DEV_OPT_PRINT_SERIAL    = 0x0002
};



#if MBG_TGT_HAS_DEV_FN
  #define EXAMPLE_DEV_FN_1      MBGCLOCK_DEV_FN_BASE "0"
  #define EXAMPLE_DEV_FN_1_TCR  EXAMPLE_DEV_FN_1
  #define EXAMPLE_DEV_FN_2      MBGCLOCK_DEV_FN_BASE "3"
#endif

#define EXAMPLE_DEV_NAME_1      "gps180pex"
#define EXAMPLE_DEV_NAME_1_TCR  "tcr167pci"
#define EXAMPLE_DEV_NAME_2      "tcr170pex_027911002000"


#if defined( MBG_TGT_POSIX )
  #define ROOT_PRIVILEGES_STR        "with root privileges"
#else
  #define ROOT_PRIVILEGES_STR        "as administrator"
#endif


_ext bool must_print_usage;

_ext const char str_empty[]
#ifdef _DO_INIT
 = ""
#endif
;


_ext const char *pzf_corr_state_name[N_PZF_CORR_STATE]
#ifdef _DO_INIT
 = PZF_CORR_STATE_NAMES_ENG
#endif
;



/**
 * @brief The type of functions to be called to handle a device in a specific way.
 */
typedef int MBG_DEV_HANDLER_FNC( MBG_DEV_HANDLE );



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Print the program version to a string buffer.
 *
 * @param[out]  s        Address of a string buffer to be filled.
 * @param[in]   max_len  Size of the string buffer.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
 int mbg_program_version_str( char *s, size_t max_len ) ;

 /**
 * @brief Print some program info to a string buffer.
 *
 * @param[out]  s           Address of a string buffer to be filled.
 * @param[in]   max_len     Size of the string buffer.
 * @param[in]   pname       The program name.
 * @param[in]   first_year  First copyright year.
 * @param[in]   last_year   Last copyright year.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
 int mbg_program_info_str( char *s, size_t max_len, const char *pname, int first_year, int last_year ) ;

 /**
 * @brief Print program info to console.
 *
 * @param[in]  pname       The program name.
 * @param[in]  first_year  First copyright year.
 * @param[in]  last_year   Last copyright year.
 */
 void mbg_print_program_info( const char *pname, int first_year, int last_year ) ;

 /**
 * @brief Check if the program runs with admin rights.
 *
 * @return  @a true if runs with admin rights, else @a false.
 */
 bool mbg_has_admin_rights( void ) ;

 /**
 * @brief Print a warning if program runs without admin rights.
 *
 * Yet, this is only relevant for the detection of SYN1588 devices.
 */
 void mbg_chk_print_no_admin_rights( void ) ;

 /**
 * @brief Print a warning if no device has been found.
 *
 * If SYN1588 support is enabled, also possibly print
 * a warning if running without admin rights, because
 * on some systems, SYN1588 devices may not be detected
 * without admin rights.
 */
 void mbg_print_no_device_found( void ) ;

 /**
 * @brief Print usage intro to console.
 *
 * @param[in]  pname      The program name.
 * @param[in]  print_dev  Print device parameters, or not.
 * @param[in]  info       An optional additional info string, may be @a NULL.
 */
 void mbg_print_usage_intro( const char *pname, bool print_dev, const char *info ) ;

 /**
 * @brief Print info on a single program option / argument.
 *
 * @param[in]  opt_name      The option name, optional, may be @a NULL.
 * @param[in]  opt_info_fmt  The option info format string, optional, may be @a NULL.
 * @param[in]  ...           Variable argument list according to the @p fmt format string.
 */
 __attribute__( ( format( printf, 2, 3 ) ) ) void mbg_print_opt_info( const char *opt_name, const char *opt_info_fmt, ... ) ;

 /**
 * @brief Print info on common program help arguments.
 *
 * Lists program parameters causing printing
 * of help / usage information.
 */
 void mbg_print_help_options( void ) ;

 /**
 * @brief Print common info on how to specify devices on the command line.
 *
 * @param[in]  mask  Bit mask to control for which kind of devices
 *                   information shall be displayed.
 *                   See ::DEV_OPT_PRINT_MASKS.
 *
 * @see ::DEV_OPT_PRINT_MASKS
 */
 void mbg_print_device_options( int mask ) ;

 /**
 * @brief Print usage info whether SYN1588 support has been compiled in.
 */
 void mbg_print_usage_syn1588_support( void ) ;

 /**
 * @brief Print usage outro, including device options and info on SYN1588 support .
 *
 * @param[in]  mask  Bit mask to control for which kind of devices
 *                   information shall be displayed.
 *                   See ::DEV_OPT_PRINT_MASKS.
 *
 * @param[in]  print_syn1588_info  Controls whether info on SYN1588 support
 *                                 is to be printed, or not.
 */
 void mbg_print_usage_outro( int mask, bool print_syn1588_info ) ;

 /**
 * @brief Print program info and default usage information.
 *
 * @param[in]  pname      The program name.
 * @param[in]  prog_info  An optional additional info string, may be @a NULL.
 */
 void mbg_print_default_usage( const char *pname, const char *prog_info ) ;

 /**
 * @brief Retrieve and print some common device info.
 *
 * @param[in]   dh        Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   dev_name  A device name string to be printed.
 * @param[out]  p_dev     Pointer to a device info structure to be read from the device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_get_show_dev_info( MBG_DEV_HANDLE dh, const char *dev_name, PCPS_DEV *p_dev ) ;

 /**
 * @brief Print device info and take some action on a specific device.
 *
 * @param[in]   dh        Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   dev_name  A device name string to be printed.
 * @param[in]   fnc       Pointer to a callback function that actually takes the action.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_handle_device( MBG_DEV_HANDLE dh, const char *dev_name, MBG_DEV_HANDLER_FNC *fnc ) ;

 /**
 * @brief Get the number of devices actually present.
 *
 * Do a real search only when called for the first time.
 *
 * @return The number of devices currently present.
 */
 int chk_get_num_devices( void ) ;

 /**
 * @brief Try to open a device using a parameter string or device index.
 *
 * @param[out]  p_dh           Address of a device handle variable to be set on success.
 * @param[in]   dev_param_str  An optional device specification string. This can be:
 *                             - A device file name, if the operating system supports this. See ::MBG_DEV_FN.
 *                             - A device model name like "GPS180PEX", optionally with the serial number
 *                               appended after an underscore, as in "GPS180PEX_029511026220".
 *                               See ::MBG_DEV_NAME.
 *                             - A device index number as string, e.g. "0" or "3".
 *                             - @a NULL, in which case @p dev_idx has to be a valid device index number.
 * @param[in]   dev_idx               A device index number which is used if @p dev_param_str is @a NULL.
 * @param[out]  dev_name_buffer       A string buffer to which a device name can be written, see ::MBG_DEV_FN.
 * @param[in]   dev_name_buffer_size  The size of the @p dev_name_buffer buffer.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_open_device_by_param
 */
 int mbg_open_device_by_param( MBG_DEV_HANDLE *p_dh, const char *dev_param_str, int dev_idx, char *dev_name_buffer, size_t dev_name_buffer_size ) ;

 /**
 * @brief Try to open a device and print a message in case of error.
 *
 * Call ::mbg_open_device_by_param to try to open a device,
 * and print an error message to stderr if the call fails.
 *
 * @param[out]  p_dh           Address of a device handle variable to be set on success.
 * @param[in]   dev_param_str  An optional device specification string. This can be:
 *                             - A device file name, if the operating system supports this. See ::MBG_DEV_FN.
 *                             - A device model name like "GPS180PEX", optionally with the serial number
 *                               appended after an underscore, as in "GPS180PEX_029511026220".
 *                               See ::MBG_DEV_NAME.
 *                             - A device index number as string, e.g. "0" or "3".
 *                             - @a NULL, in which case @p dev_idx has to be a valid device index number.
 * @param[in]   dev_idx               A device index number which is used if @p dev_param_str is @a NULL.
 * @param[out]  dev_name_buffer       A string buffer to which a device name can be written, see ::MBG_DEV_FN.
 * @param[in]   dev_name_buffer_size  The size of the @p dev_name_buffer buffer.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_open_device_by_param
 */
 int mbg_open_device_by_param_chk( MBG_DEV_HANDLE *p_dh, const char *dev_param_str, int dev_idx, char *dev_name_buffer, size_t dev_name_buffer_size ) ;

 /**
 * @brief Main action handler that can be called by utility programs.
 *
 * This function checks the command line parameters passed to the program.
 * Those which have not been evaluated before are interpreted as device specifiers.
 *
 * Device specifiers can be device file names like "/dev/mbgclock0" on target
 * platforms that support such device names, see ::MBG_DEV_FN,
 * or device type names like "GPS180PEX" which can optionally have a serial
 * number appended, like "GPS180PEX_029511026220", to be able to distinguish
 * between several devices of the same type (see ::MBG_DEV_NAME),
 * or just an index number as required for the ::mbg_open_device call.
 *
 * For each of the specified devices, the callback function @p fnc is called
 * to take some specific action on the device.
 *
 * If no device has been specified in the argument list, ::mbg_find_devices is
 * called to set up a device list, and depending on whether ::CHK_DEV_ALL_DEVICES
 * is set in @p chk_dev_flags, the callback function @p fnc is either called
 * for every device from the list, or only for the first one.
 *
 * @param[in]  argc     Number of parameters of the program argument list.
 * @param[in]  argv     Array of program argument strings.
 * @param[in]  optind   Number of the command line arguments that have already been handled.
 * @param[in]  fnc      Pointer to a callback function that actually takes an action on each specified device.
 * @param[in]  chk_dev_flags    Bit mask controlling the behavior of the function, see ::CHK_DEV_FLAGS.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_handle_devices( int argc, char *argv[], int optind, MBG_DEV_HANDLER_FNC *fnc, int chk_dev_flags ) ;

 /**
 * @brief Print date and time from a ::PCPS_TIME structure to a string.
 *
 * @param[out]  s        Address of a string buffer to be filled.
 * @param[in]   max_len  Size of the string buffer.
 * @param[in]   p        Pointer to a ::PCPS_TIME structure to be evaluated.
 * @param[in]   verbose  Increase verbosity of the output:<br>
 *                       > 0: append %UTC offset and status<br>
 *                       > 1: append signal value.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
 int mbg_snprint_date_time( char *s, size_t max_len, const PCPS_TIME *p, int verbose ) ;

 /**
 * @brief Print date and time from a ::PCPS_TIME_STAMP structure to a string.
 *
 * @param[out]  s         Address of a string buffer to be filled.
 * @param[in]   max_len   Size of the string buffer.
 * @param[in]   p         Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]   utc_offs  A local time offset added to the time stamp before converted to date and time.
 * @param[in]   show_raw  Flag indicating if a raw timestamp (hex) is to be printed, too.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
 int mbg_snprint_hr_tstamp( char *s, size_t max_len, const PCPS_TIME_STAMP *p, long utc_offs, int show_raw ) ;

 /**
 * @brief Print date and time from a ::PCPS_HR_TIME structure to a string.
 *
 * Converts the %UTC timestamp first to local time according to
 * the ::PCPS_HR_TIME::utc_offs value.
 *
 * @param[out]  s         Address of a string buffer to be filled.
 * @param[in]   max_len   Size of the string buffer.
 * @param[in]   p         Pointer to a ::PCPS_HR_TIME structure to be evaluated.
 * @param[in]   show_raw  Flag indicating if a raw timestamp (hex) is to be printed, too.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
 int mbg_snprint_hr_time( char *s, size_t max_len, const PCPS_HR_TIME *p, int show_raw ) ;

 /**
 * @brief Print date and time from a ::PCPS_TIME_STAMP structure.
 *
 * First the HR timestamp passed by parameter @p p_ts is printed.
 *
 * If the parameter @p p_prv_ts is not @a NULL, it should specify
 * an earlier timestamp, and the elapsed time since @p p_ts
 * is appended.
 *
 * Finally the latency value is printed in microseconds, unless
 * parameter @p no_latency is != 0.
 *
 * @param[in]  p_ts         Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]  hns_latency  A latency number in hectonanoseconds, i.e. 100 ns units.
 * @param[in]  p_prv_ts     Pointer to a ::PCPS_TIME_STAMP structure to be evaluated, may be @a NULL.
 * @param[in]  no_latency   A flag indicating if printing the latency should be suppressed.
 * @param[in]  show_raw     Flag indicating if a raw timestamp (hex) is to be printed, too.
 *
 * @see ::mbg_print_hr_time
 */
 void mbg_print_hr_timestamp( PCPS_TIME_STAMP *p_ts, int32_t hns_latency, PCPS_TIME_STAMP *p_prv_ts, int no_latency, int show_raw ) ;

 /**
 * @brief Print date and time from a ::PCPS_HR_TIME structure.
 *
 * First the HR timestamp passed by parameter @p p_ht is printed.
 *
 * If the parameter @p p_prv_ts is not @a NULL, it should specify
 * an earlier timestamp, and the elapsed time since @p p_ht
 * is appended.
 *
 * Next the latency value is printed in microseconds, unless
 * parameter @p no_latency is != 0.
 *
 * @param[in]  p_ht         Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]  hns_latency  A latency number in hectonanoseconds, i.e. 100 ns units.
 * @param[in]  p_prv_ts     Pointer to a ::PCPS_TIME_STAMP structure to be evaluated, may be @a NULL.
 * @param[in]  no_latency   A flag indicating if printing the latency should be suppressed.
 * @param[in]  show_raw     Flag indicating if a raw timestamp (hex) is to be printed, too.
 * @param[in]  verbose      Increase verbosity of the output:<br>
 *                           > 0: append status code<br>
 *                           > 1: append signal value.
 *
 * @see ::mbg_print_hr_timestamp
 */
 void mbg_print_hr_time( PCPS_HR_TIME *p_ht, int32_t hns_latency, PCPS_TIME_STAMP *p_prv_ts, int no_latency, int show_raw, int verbose ) ;

 /**
 * @brief Print PZF correlation info for a device which supports this.
 *
 * @param[in]  p_ci            Pointer to a ::CORR_INFO structure that has been read before.
 * @param[in]  show_corr_step  A flag indicating if correlation step indicators are to be appended.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_show_pzf_corr_info( const CORR_INFO *p_ci, int show_corr_step ) ;

 /**
 * @brief Retrieve a ::MBG_GNSS_MODE_INFO structure from a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_GNSS_MODE_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_get_gps_gnss_mode_info_chk( MBG_DEV_HANDLE dh, MBG_GNSS_MODE_INFO *p ) ;


/* ----- function prototypes end ----- */


#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _TOOLUTIL_H */

