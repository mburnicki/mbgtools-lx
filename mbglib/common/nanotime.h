
/**************************************************************************
 *
 *  $Id: nanotime.h 1.5 2022/06/23 14:48:30 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for nanotime.c
 *
 * -----------------------------------------------------------------------
 *  $Log: nanotime.h $
 *  Revision 1.5  2022/06/23 14:48:30  martin.burnicki
 *  Changed the return value of some snprintf_ functions from size_t to int.
 *  Revision 1.4  2019/08/19 11:10:02  martin
 *  Updated doxygen comments.
 *  Revision 1.3  2018/11/21 11:27:26  martin
 *  Include stdlib.h to provide size_t.
 *  Revision 1.2  2018/07/16 12:40:55  martin
 *  Include gpsdefs.h.
 *  Updated function prototypes.
 *  Revision 1.1  2017/06/12 08:49:09  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _NANOTIME_H
#define _NANOTIME_H

/* Other headers to be included */

#include <words.h>  // implicitly includes mbg_tgt.h, if required,
                    // and defines some basic structures
#include <gpsdefs.h>

#include <stdlib.h>

#ifdef _NANOTIME
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif




/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Convert a ::NANO_TIME time to double
 *
 * @param[in]  p  Address of a ::NANO_TIME structure to be converted
 *
 * @return The computed number of seconds with fractions, as double
 *
 * @see ::double_to_nano_time
 */
 double nano_time_to_double( const NANO_TIME *p ) ;

 /**
 * @brief Setup a ::NANO_TIME structure from a time provided as double
 *
 * @param[out]  p  Address of a ::NANO_TIME structure to be set up
 * @param[in]   d  The time to be converted, in seconds with fractions, as double
 *
 * @see ::nano_time_to_double
 */
 void double_to_nano_time( NANO_TIME *p, double d ) ;

 /**
 * @brief Print nano time into string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  nt       The ::NANO_TIME to be printed
 */
 int snprint_nano_time( char *s, size_t max_len, const NANO_TIME *nt ) ;

 /**
 * @brief Normalize a ::NANO_TIME_64 time
 *
 * After normalization, the following can be assumed:<br>
 * - nano_secs is in the range [-10^9 + 1, 10^9 - 1]<br>
 * - if secs is not 0, secs and nano_secs have the same sign
 *
 * @param[in,out] nt  The ::NANO_TIME_64 to be normalized
 */
 void normalize_nano_time_64( NANO_TIME_64 *nt ) ;

 /**
 * @brief Convert a ::NANO_TIME_64 time to double
 *
 * @param[in]  p  Address of a ::NANO_TIME_64 structure to be converted
 *
 * @return The computed number of seconds with fractions, as double
 *
 * @see ::double_to_nano_time_64
 */
 double nano_time_64_to_double( const NANO_TIME_64 *p ) ;

 /**
 * @brief Setup a ::NANO_TIME_64 structure from a time as double
 *
 * @param[out]  p  Address of a ::NANO_TIME_64 structure to be set up
 * @param[in]   d  The time to be converted, in seconds with fractions, as double
 *
 * @see ::nano_time_64_to_double
 */
 void double_to_nano_time_64( NANO_TIME_64 *p, double d ) ;

 /**
 * @brief Print a normalized ::NANO_TIME_64 into a string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  nt       The ::NANO_TIME_64 to be printed
 */
 int snprint_nano_time_64( char *s, size_t max_len, const NANO_TIME_64 *nt ) ;

 /**
 * @brief Set up a ::NANO_TIME_64 structure from a string with a time in seconds and fractions
 *
 * @param[in]   s  A string with a time in seconds, with fractions separated by decimal point
 * @param[out]  p  Address of a ::NANO_TIME_64 structure to be set up
 */
 void str_s_to_nano_time_64( const char *s, NANO_TIME_64 *p ) ;

 /**
 * @brief Set up a ::NANO_TIME_64 structure from a string with a time in milliseconds and fractions
 *
 * @param[in]   s  A string with a time in milliseconds, with fractions separated by decimal point
 * @param[out]  p  Address of a ::NANO_TIME_64 structure to be set up
 */
 void str_ms_to_nano_time_64( const char *s, NANO_TIME_64 *p ) ;

 /**
 * @brief Convert ::NANO_TIME_64 to ::TM_GPS
 *
 * @param[out] tm_gps  The ::TM_GPS to be filled
 * @param[in]  nt      The ::NANO_TIME_64 to be converted
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @see ::tm_gps_to_nano_time_64
 */
 int nano_time_64_to_tm_gps( TM_GPS *tm_gps, const NANO_TIME_64 *nt ) ;

 /**
 * @brief Convert ::TM_GPS to ::NANO_TIME_64
 *
 * @param[out] nt      The ::NANO_TIME_64 to be filled
 * @param[in]  tm      The ::TM_GPS to be converted
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @see ::nano_time_64_to_tm_gps
 */
 int tm_gps_to_nano_time_64( NANO_TIME_64 *nt, const TM_GPS *tm ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _NANOTIME_H */
