
/**************************************************************************
 *
 *  $Id: mbg_pps.c 1.1 2021/12/01 19:00:25 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  PPS support functions.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_pps.c $
 *  Revision 1.1  2021/12/01 19:00:25  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#define _MBG_PPS
 #include <mbg_pps.h>
#undef _MBG_PPS

#include <str_util.h>
#include <mbgerror.h>

#if defined( MBG_TGT_LINUX )
  #include <dirent.h>
  #include <stdio.h>
  #include <sys/stat.h>
  #include <fcntl.h>
  #include <syslog.h>
#endif



static /*HDR*/
/**
 * @brief Log a PPS-specific error.
 *
 * @param[in]  s         A specific error message.
 * @param[in]  p_pps_di  Pointer to a ::PPS_DEV_INFO structure providing the name, etc.
 * @param[in]  rc        The return code with the type of error.
 */
void mbg_pps_log_error( const char *s, const PPS_DEV_INFO *p_pps_di, int rc )
{
  mbg_pps_log_fnc( LOG_ERR, "%s \"%s\": %s", s, p_pps_di->dev_name,
                   mbg_strerror( rc ) );

}  // mbg_pps_log_error



/*HDR*/
/**
 * @brief Initialize a @ref PPS_DEV_INFO structure.
 *
 * Generate the device file name from the PPS device index @p pps_idx.
 *
 * Open the PPS device and create a handle that can be used
 * with further API calls.
 *
 * Check if the preferred @p mode is supported, and set it.
 *
 * @param[out]  p_pps_di  Pointer to the structure to be initialized.
 * @param[in]   pps_idx   Index of the PPS device, used to create the
 *                        PPS device file name.
 * @param[in]   mode      PPS mode flags like PPS_CAPTUREASSERT,
 *                        defined in timepps.h.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_pps_init( PPS_DEV_INFO *p_pps_di, int pps_idx, int mode )
{
  pps_params_t params = { 0 };
  int rc;

  // Set up the device file name.
  snprintf_safe( p_pps_di->dev_name, sizeof( p_pps_di->dev_name ),
                 PPS_DEVICE_PATH_FMT, pps_idx );

  #if DEBUG_PPS
    mbg_pps_log_fnc( LOG_INFO, "Trying to open PPS device \"%s\"",
                     p_pps_di->dev_name );
  #endif

  // Try to open the PPS device with the generated
  // device file name.
  p_pps_di->fd = open( p_pps_di->dev_name, O_RDWR );

  if ( p_pps_di->fd < 0 )
  {
    rc = mbg_get_last_error( NULL );
    mbg_pps_log_error( "Failed to open PPS device", p_pps_di, rc );
    return rc;
  }


  // Create the PPS handle.
  rc = time_pps_create( p_pps_di->fd, &p_pps_di->handle );

  if ( rc < 0 )
  {
    rc = mbg_get_last_error( NULL );
    mbg_pps_log_error( "Failed to create a PPS source from device", p_pps_di, rc );
    return rc;
  }


  #if DEBUG_PPS
    mbg_pps_log_fnc( LOG_INFO, "PPS device \"%s\"", p_pps_di->dev_name );
  #endif

  // Detect supported features.
  rc = time_pps_getcap( p_pps_di->handle, &p_pps_di->supp_modes );

  if ( rc < 0 )
  {
    rc = mbg_get_last_error( NULL );
    mbg_pps_log_error( "Failed to get PPS capabilities for", p_pps_di, rc );
    return rc;
  }


  // Check if the selected mode is supported.
  if ( ( p_pps_di->supp_modes & mode ) == mode )
  {
    // Get current parameters.
    rc = time_pps_getparams( p_pps_di->handle, &params );

    if ( rc < 0 )
    {
      rc = mbg_get_last_error( NULL );
      mbg_pps_log_error( "Failed to get PPS parameters for", p_pps_di, rc );
      return rc;
    }


    // Set the selected mode.
    params.mode = mode;

    rc = time_pps_setparams( p_pps_di->handle, &params );

    if ( rc < 0 )
    {
      rc = mbg_get_last_error( NULL );
      mbg_pps_log_error( "Failed to set PPS parameters for", p_pps_di, rc );
      return rc;
    }
  }
  else
  {
    mbg_pps_log_fnc( LOG_ERR, "Selected PPS mode not supported for \"%s\"",
                     p_pps_di->dev_name );

    return MBG_ERR_NOT_SUPP_ON_OS;
  }

  return MBG_SUCCESS;

}  // mbg_pps_init



/**
 * @brief Try to find a PPS device attached to a specific device.
 *
 * @param[in]  dev_name  A string with the name of the device for which
 *                       an attached PPS device is to be found.
 *
 * @see ::DEFAULT_PPS_DEVICE_NAME
 *
 * @return On success, the index number of the PPS source (>= 0),<br>
 *         ::MBG_ERR_NO_ENTITY if no entry was found, or<br>
 *         ::MBG_ERR_NOT_SUPP_ON_OS if the function is not supported
 *         on the target platform
 */
/*HDR*/
int mbg_pps_find_attached_pps( const char *dev_name )
{
  #if defined( MBG_TGT_LINUX )
    // The SYSFS directory name for the PPS class.
    static const char pps_dir_name[] = "/sys/class/pps/";

    // The name of the pseudo file that contains the name
    // of the device file a PPS source is associated with.
    static const char pps_path_name[] = "/path";

    int rc = MBG_ERR_NO_ENTITY;
    DIR *p_dir = opendir( pps_dir_name );

    if ( p_dir )
    {
      struct dirent *dir;

      while ( ( dir = readdir( p_dir ) ) )
      {
        char pps_src_name[PATH_MAX];
        char fn[PATH_MAX];
        size_t max_len = sizeof( fn );
        FILE *fp;
        int n;
        int idx;

        // Skip directory entries.
        if ( dir->d_type == DT_DIR )
          continue;

        // Compose the full path name to the file
        // we are going to open.
        n = sn_cpy_str_safe( fn, max_len, pps_dir_name );
        n += sn_cpy_str_safe( &fn[n], max_len - n, dir->d_name );
        n += sn_cpy_str_safe( &fn[n], max_len - n, pps_path_name );
        (void) n;  // Avoid warning "never used" or so.

        pps_src_name[0] = 0;  // Make destination string empty.

        fp = fopen( fn, "r" );

        if ( fp )
        {
          char *cp = fgets( pps_src_name, sizeof( pps_src_name ), fp );
          fclose ( fp );

          if ( cp )  // A string was read, potentially ending with newline.
            trim_whitespace( pps_src_name );
        }

        #if DEBUG_PPS
          mbg_pps_log_fnc( LOG_INFO, "Found %s%s attached to %s",
                           pps_dir_name, dir->d_name, pps_src_name );
        #endif

        if ( strcmp( pps_src_name, dev_name ) )  // Strings differ.
          continue;

        // Found the entry we were looking for.
        // Now extract the index number from the name of the entry,
        // which is something like "pps0".
        n = sscanf( dir->d_name, "pps%i", &idx );

        if ( n == 1 )  // One conversion succeeded, result in idx.
        {
          rc = MBG_SUCCESS + idx;
          break;
        }
      }
    }

    if ( p_dir )
      closedir( p_dir );

    return rc;
  #else
    return MBG_ERR_NOT_SUPP_ON_OS;
  #endif

}  // mbg_pps_find_attached_pps



/*HDR*/
/**
 * @brief Attach a PPS source to a serial device.
 *
 * This may require root privileges.
 *
 * @param[in]  sdev  Parameters of an opened serial device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_pps_attach_to_serial( const MBGSERIO_DEV *sdev )
{
  int ldisc = 18;  // Predefined constant for the PPS line discipline.

  int sys_rc = ioctl( sdev->port_handle, TIOCSETD, &ldisc );

  if ( sys_rc < 0 )
    return mbg_get_last_error( NULL );

  return MBG_SUCCESS;

}  // mbg_pps_attach_to_serial



/*HDR*/
/**
 * @brief Set up a @ref PPS_DEV_INFO structure for a serial port.
 *
 * First looks if a PPS source is already attached to the
 * specified serial port.
 *
 * If it is not, tries to attach a PPS source and tries to find
 * the newly attached PPS source.
 *
 * @param[out]  p_pps_di  Pointer to the structure to be set up.
 * @param[in]   dev_name  Device file name of the serial port.
 * @param[in]   sdev      Parameters of an opened serial device with name @p dev_name.
 * @param[in]   mode      PPS mode flags like PPS_CAPTUREASSERT,
 *                        defined in timepps.h.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_pps_setup_pps_for_serial( PPS_DEV_INFO *p_pps_di, const char *dev_name,
                                  const MBGSERIO_DEV *sdev, int mode )
{
  // First see if there's already a PPS source that has been
  // attached to dev_name, e.g. by running "ldattach" on Linux.
  int rc = mbg_pps_find_attached_pps( dev_name );

  // If no PPS source has been found, we try to attach a source,
  // similar to what "ldattach" on Linux does.
  // This may require root privileges.
  if ( mbg_rc_is_error( rc ) )
  {
    rc = mbg_pps_attach_to_serial( sdev );

    if ( mbg_rc_is_error( rc ) )
    {
      mbg_pps_log_fnc( LOG_ERR, "Failed to attach PPS to %s: %s",
                       dev_name, mbg_strerror( rc ) );
    }
    else
    {
      mbg_pps_log_fnc( LOG_INFO, "Successfully attached PPS to %s",
                       dev_name );
      rc = mbg_pps_find_attached_pps( dev_name );
    }
  }

  if ( mbg_rc_is_success( rc ) )
    rc = mbg_pps_init( p_pps_di, rc, mode );  // rc contains the PPS index number

  return rc;

}  // mbg_pps_setup_pps_for_serial



/*HDR*/
/**
 * @brief Fetch a PPS time stamp from the kernel.
 *
 * The time stamp is returned via @p p_t_pps. If no
 * valid time stamp can be retrieved, the variable
 * pointed to by @p p_t_pps is set to all 0.
 *
 * @param[out]     p_t_pps   Address of a ::MBG_SYS_TIME structure to take the time stamp.
 * @param[in,out]  p_pps_di  Address of a ::PPS_DEV_INFO structure that contains the PPS info
 *                           and stores the data retrieved from the kernel.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_pps_fetch_ts( MBG_SYS_TIME *p_t_pps, PPS_DEV_INFO *p_pps_di )
{
  struct timespec pps_timeout = { 0 };
  const struct timespec *p_ts;
  pps_info_t *p_pps_info;
  int sys_rc;
  int rc = MBG_ERR_NO_DATA;

  if ( p_pps_di->handle < 1 )
  {
    rc = MBG_ERR_INV_HANDLE;
    goto fail;
  }


  p_pps_info = &p_pps_di->info;

  sys_rc = time_pps_fetch( p_pps_di->handle, PPS_TSFMT_TSPEC, p_pps_info, &pps_timeout );

  if ( sys_rc < 0 )
  {
    rc = mbg_get_last_error( NULL );
    goto fail;
  }


  // Right now we only expect "struct timespec" format.
  if ( !( p_pps_info->current_mode & PPS_TSFMT_TSPEC ) )
    goto fail;


  if ( p_pps_info->current_mode & PPS_CAPTUREASSERT )
    p_ts = &p_pps_info->assert_tu.tspec;
  else
    if ( p_pps_info->current_mode & PPS_CAPTURECLEAR )
      p_ts = &p_pps_info->clear_tu.tspec;
    else
      goto fail;

  _mbg_sys_time_from_timespec( p_t_pps, p_ts );

  return MBG_SUCCESS;

fail:
  memset( p_t_pps, 0, sizeof ( *p_t_pps ) );
  return rc;

}  // mbg_pps_fetch_ts

