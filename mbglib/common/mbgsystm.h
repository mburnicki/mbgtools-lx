
/**************************************************************************
 *
 *  $Id: mbgsystm.h 1.13 2021/11/29 14:32:13 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Generic functions and definitions to deal with the computer's
 *    system time, and prototypes for mbgsystm.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgsystm.h $
 *  Revision 1.13  2021/11/29 14:32:13  martin.burnicki
 *  Commonly use new macros _mbg_sys_time_from_timespec() and
 *  _mbg_sys_time_from_timeval() which are based on the new macros
 *  _nano_time_64_from_timespec() and _nano_time_64_from_timeval()
 *  defined in words.h.
 *  New macro _mbg_sys_timestamp_is_empty().
 *  Revision 1.12  2021/05/31 09:51:09  thomas-b
 *  Fixed missing include for POSIX systems (we need mbgtime for NSEC_PER_SEC)
 *  Revision 1.11  2021/05/11 19:04:58  martin
 *  Changed mbg_sleep_sec() to mbg_sleep_msec(), which expects milliseconds,
 *  and provided a compatible replacement function mbg_sleep_sec(), which
 *  in fact just calls mbg_sleep_msec() accordingly.
 *  Revision 1.10  2021/04/29 10:13:14  martin
 *  Fixed build on DOS.
 *  Revision 1.9  2021/04/08 16:41:05  martin
 *  New inline functions mbg_sys_time_normalize() and mbg_sys_time_add_ns().
 *  Revision 1.8  2021/04/08 14:24:25  martin
 *  Updated a bunch of comments.
 *  Revision 1.7  2021/03/12 12:32:37  martin
 *  Updated some comments.
 *  Revision 1.6  2020/03/31 08:16:38  martin
 *  Fix for Linux kernel 5.6 which doesn't support getnstimeofday() anymore.
 *  Revision 1.5  2019/02/08 11:05:48  martin
 *  Commonly use int64_t for MBG_SYS_TIME on Windows, rather than
 *  FILETIME in user space and LARGE_INTEGER in kernel space.
 *  Also use a function pointer to read the Windows system time
 *  depending on whether the precise time API calls are supported
 *  by the running Windows system, or not.
 *  Revision 1.4  2018/07/16 12:32:58  martin
 *  Fixed do_div() for 32 bit ARM Linux kernel.
 *  Include another header file on NetBSD.
 *  Revision 1.3  2017/07/26 14:26:17  martin
 *  Fixed build for NetBSD.
 *  Revision 1.2  2017/07/04 12:26:57  martin
 *  More detailed control of inclusion of other headers.
 *  Made mbg_delta_sys_time_ms() as inline function here.
 *  Updated function prototypes.
 *  Cleanup.
 *  Revision 1.1  2015/09/15 13:21:00Z  martin
 *  Initial revision.
 *  Moved existing code from different modules here.
 *
 **************************************************************************/

#ifndef _MBGSYSTM_H
#define _MBGSYSTM_H


/* Other headers to be included */

#include <mbg_tgt.h>
#include <words.h>
#include <mbgtime.h>

#if defined( MBG_TGT_POSIX )

  #if defined( MBG_TGT_KERNEL )

    #if defined( MBG_TGT_LINUX )

      // If true, getnstimeofday() has been deprecated and removed,
      // and ktime_get_real_ts64() can be used instead.
      #define LINUX_KERNEL_HAS_KT64            ( LINUX_VERSION_CODE >= KERNEL_VERSION( 5, 6, 0 ) )

      // If true, getnstimeofday() is supported and has not yet been deprecated.
      #define LINUX_KERNEL_HAS_GETNSTIMEOFDAY  ( ( LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 22 ) ) && !LINUX_KERNEL_HAS_KT64 )

      #define LINUX_KERNEL_HAS_MSLEEP          ( LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 16 ) )
      #define LINUX_KERNEL_HAS_KTIME_H         ( LINUX_VERSION_CODE >= KERNEL_VERSION( 2, 6, 16 ) )

      #if !LINUX_KERNEL_HAS_MSLEEP
        #include <linux/wait.h>
        #include <linux/sched.h>
      #endif

      // We need the prototype for getnstimeofday(). In newer kernels
      // (e.g. 4.x) this is available via linux/ktime.h, which in turn
      // includes linux/timekeeping.h, which declares the prototype.
      #if LINUX_KERNEL_HAS_KTIME_H
        #include <linux/ktime.h>
      #endif

      // In older kernel versions the prototype for getnstimeofday()
      // is declared in linux/time.h, so we include that one anyway.
      #include <linux/time.h>

      #include <linux/delay.h>
      #include <linux/jiffies.h>

    #elif defined( MBG_TGT_BSD )

      #if defined( MBG_TGT_FREEBSD )
        #include <sys/libkern.h>
      #endif

      #if defined( MBG_TGT_NETBSD )
        #include <sys/param.h>   // mstohz
        #include <sys/kernel.h>  // hz
        #include <sys/proc.h>    // kpause
      #endif

      #include <sys/time.h>

    #endif

  #else  // POSIX user space.

    #include <time.h>

    #if defined( MBG_TGT_LINUX )
      #include <sys/sysinfo.h>
    #endif

  #endif

#elif defined( MBG_TGT_WIN32 )

  #if defined( MBG_TGT_KERNEL )
    #include <mbg_w32.h>   // ke_query_system_time_fnc
  #else
    #include <timeutil.h>  // gstaft_fnc
  #endif

#elif defined( MBG_TGT_DOS )

  #include <dos.h>

#endif


#ifdef _MBGSYSTM
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


/*
 * Generic types to hold system timestamps and values for the system uptime.
 */
#if defined( MBG_TGT_POSIX )

  typedef NANO_TIME_64 MBG_SYS_TIME;
  typedef int64_t MBG_SYS_UPTIME;    // [s]

  #define _mbg_sys_timestamp_is_empty( _p ) \
    _nano_time_64_zero( _p )

  #define _mbg_sys_time_from_timespec( _p, _p_ts ) \
    _nano_time_64_from_timespec( _p, _p_ts )

  #define _mbg_sys_time_from_timeval( _p, _p_ts ) \
    _nano_time_64_from_timeval( _p, _p_ts )

#elif defined( MBG_TGT_WIN32 )

  typedef int64_t MBG_SYS_TIME;      // Number of 100ns units.
  typedef int64_t MBG_SYS_UPTIME;    // [s]

#elif defined( MBG_TGT_OS2 )

  typedef uint32_t MBG_SYS_TIME;   //## dummy
  typedef long MBG_SYS_UPTIME;     //## dummy

#elif defined( MBG_TGT_DOS )

  typedef uint32_t MBG_SYS_TIME;   //## dummy
  typedef long MBG_SYS_UPTIME;     //## dummy

#else // Other target OSs which access the hardware directly.

  typedef uint32_t MBG_SYS_TIME;   //## dummy
  typedef long MBG_SYS_UPTIME;     //## dummy

#endif

#if !defined( _mbg_sys_timestamp_is_empty )
  #define _mbg_sys_timestamp_is_empty( _p ) \
    ( *(_p) == 0 )
#endif

// MBG_SYS_TIME is always read in native machine endianess,
// so no need to convert endianess.
#define _mbg_swab_mbg_sys_time( _p ) \
  _nop_macro_fnc()


/**
 * @brief Read the current system time as ::MBG_SYS_TIME.
 *
 * Implementation depends strongly on the target system.
 *
 * On systems that may not support this, ::MBG_SYS_TIME is
 * just some signed integer type which is set to 0.
 *
 * @param[out]  p  Address of an ::MBG_SYS_TIME variable to take the result.
 */
static __mbg_inline
void mbg_get_sys_time( MBG_SYS_TIME *p )
{
  #if defined( MBG_TGT_POSIX )

    #if defined( MBG_TGT_KERNEL )   // Kernel space functions even differ for POSIX systems.

      #if defined( MBG_TGT_LINUX )  // Linux kernel space.

        #if LINUX_KERNEL_HAS_KT64
        {
          struct timespec64 ts;

          ktime_get_real_ts64( &ts );
          _mbg_sys_time_from_timespec( p, &ts );
        }
        #elif LINUX_KERNEL_HAS_GETNSTIMEOFDAY
        {
          struct timespec ts;

          getnstimeofday( &ts );
          _mbg_sys_time_from_timespec( p, &ts );
        }
        #else
        {
          // getnstimeofday() is *not* supported.
          struct timeval tv;

          do_gettimeofday( &tv );
          _mbg_sys_time_from_timeval( p, &tv );
        }
        #endif

      #elif defined( MBG_TGT_BSD )  // BSD kernel space.
      {
        struct timespec ts;

        nanotime( &ts );
        _mbg_sys_time_from_timespec( p, &ts );
      }
      #endif

    #else  // POSIX user space.
    {
      struct timespec ts;

      #if defined( CLOCK_REALTIME_PRECISE )  // At least available in FreeBSD.
        clock_gettime( CLOCK_REALTIME_PRECISE, &ts );
      #else
        clock_gettime( CLOCK_REALTIME, &ts );
      #endif

      _mbg_sys_time_from_timespec( p, &ts );
    }
    #endif

  #elif defined( MBG_TGT_WIN32 )

    #if defined( MBG_TGT_KERNEL )  // Windows kernel space.
      ke_query_system_time_fnc( (LARGE_INTEGER *) p );
    #else                          // Windows user space.
    {
      gstaft_fnc( (FILETIME *) p );
    }
    #endif

  #else

    *p = 0;  // Dummy.

  #endif

}  // mbg_get_sys_time



/**
 * @brief Read the current system uptime as ::MBG_SYS_UPTIME, in seconds.
 *
 * Implementation depends strongly on the target system.
 *
 * On systems that may not support this, @p *p is set to -1.
 *
 * @param[out]  p  Address of an ::MBG_SYS_UPTIME variable to take the result.
 */
static __mbg_inline
void mbg_get_sys_uptime( MBG_SYS_UPTIME *p )
{
  #if defined( MBG_TGT_WIN32 )

    #if defined( MBG_TGT_KERNEL )  // Windows kernel space.

      ULONGLONG time_increment  = KeQueryTimeIncrement();
      LARGE_INTEGER tick_count;

      KeQueryTickCount( &tick_count );

      // Multiplication by time_increment yields HNS units,
      // but we need seconds.
      *p = ( tick_count.QuadPart * time_increment ) / HNS_PER_SEC;

    #else                          // Windows user space.

      DWORD tickCount;
      DWORD timeAdjustment;
      DWORD timeIncrement;
      BOOL timeAdjustmentDisabled;

      if ( !GetSystemTimeAdjustment( &timeAdjustment, &timeIncrement, &timeAdjustmentDisabled ) )
        *p = -1;  // Failed.

      // ATTENTION: This is compatible with older Windows versions, but
      // the returned tick count wraps around to zero after 49.7 days.
      // A new GetTickCount64() call is available on Windows Vista and newer,
      // but the function call had to be imported dynamically because otherwise
      // programs refused to start on pre-Vista versions due to undefined DLL symbol.
      tickCount = GetTickCount();

      *p = ( ( (MBG_SYS_UPTIME) tickCount ) * timeIncrement ) / HNS_PER_SEC;

    #endif

  #elif defined( MBG_TGT_LINUX )

    #if defined( MBG_TGT_KERNEL )
    // TODO getrawmonotonic() can possibly be used for this in newer kernels.
    {
      // In kernel mode, using a simple 64 bit division may result
      // in a linker error due to a missing symbol __udivdi3, so we
      // use the specific inline function do_div().
      // Also, the jiffies counter is not set to 0 at startup but to
      // a defined initialization value we need to account for.
      uint64_t tmp = get_jiffies_64() - INITIAL_JIFFIES;
      do_div( tmp, HZ );
      *p = tmp;
    }
    #else
    {
      struct sysinfo si;
      int rc = sysinfo( &si );
      *p = ( rc == 0 ) ? si.uptime : -1;
    }
    #endif

  #elif defined( MBG_TGT_BSD )

    #if defined( MBG_TGT_KERNEL )
    {
      struct timespec ts;
      #if 0  //##+++++++
      {
        struct bintime bt;

        binuptime( &bt );
        #if defined( DEBUG )
          printf( "binuptime: %lli.%09lli\n",
                  (long long) bt.sec,
                  (long long) bt.frac );
        #endif
      }
      #endif

      nanouptime( &ts );
      #if defined( DEBUG )
        printf( "nanouptime: %lli.%09lli\n",
                (long long) ts.tv_sec,
                (long long) ts.tv_nsec );
      #endif
      *p = ts.tv_sec;
    }
    #elif defined( MBG_TGT_FREEBSD )
    {
      struct timespec ts;
      // CLOCK_UPTIME_FAST is specific to FreeBSD.
      int rc = clock_gettime( CLOCK_UPTIME_FAST, &ts );
      *p = ( rc == 0 ) ? ts.tv_sec : -1;
    }
    #else  // MBG_TGT_NETBSD, ...

      *p = -1;  // TODO Needs to be implemented.

    #endif

  #else

    *p = -1;  // Not supported.

  #endif

}  // mbg_get_sys_uptime



/**
 * @brief Compute delta between two ::MBG_SYS_TIME times, in milliseconds.
 *
 * Implementation depends strongly on the target system.
 *
 * On systems that may not support this, 0 is returned.
 *
 * @param[in]  t2  The time to be subtracted from.
 * @param[in]  t1  The time to be subtracted.
 *
 * @return  The time difference in [milliseconds].
 */
static __mbg_inline
long mbg_delta_sys_time_ms( const MBG_SYS_TIME *t2, const MBG_SYS_TIME *t1 )
{
  #if defined( MBG_TGT_POSIX )

    long dt = ( t2->secs - t1->secs ) * 1000;

    #if defined ( MBG_TGT_LINUX ) && defined( MBG_TGT_KERNEL )
      uint64_t tmp64 = t2->nano_secs - t1->nano_secs;
      do_div( tmp64, 1000000 );
      dt += tmp64;
    #else
      dt += ( t2->nano_secs - t1->nano_secs ) / 1000000;
    #endif

    return dt;

  #elif defined( MBG_TGT_WIN32 )

    return (long) ( ( *t2 - *t1 ) / HNS_PER_MS );

  #else

    return 0;

  #endif

}  // mbg_delta_sys_time_ms



/**
 * @brief Sleep for a specific number of milliseconds.
 *
 * Implementation depends strongly on the target system.
 *
 * @param[in]  msecs  The number of milliseconds to sleep.
 */
static __mbg_inline
void mbg_sleep_msec( ulong msecs )
{
  #if defined( MBG_TGT_POSIX )

    #if defined( MBG_TGT_KERNEL )   // Kernel space functions even differ for POSIX systems.

      #if defined( MBG_TGT_LINUX )  // Linux kernel space.

        // msleep is not available in older kernels, so we
        // use this only if it is really supported.
        #if LINUX_KERNEL_HAS_MSLEEP
          msleep( msecs );
        #else
        {
          DECLARE_WAIT_QUEUE_HEAD( tmp_wait );
          // Timeout unit is [jiffies].
          wait_event_interruptible_timeout( tmp_wait, 0, ( ( msecs * HZ ) / 1000 ) + 1 );
        }
        #endif

      #elif defined( MBG_TGT_FREEBSD )

        struct timeval tv = { 0 };
        int ticks;
        tv.tv_sec = msecs / 1000;
        tv.tv_usec = ( msecs % 1000 ) * 1000;
        ticks = tvtohz( &tv );

        #if defined( DEBUG )
          printf( "pause: %lli.%06lli (%i ticks)\n",
                  (long long) tv.tv_sec,
                  (long long) tv.tv_usec,
                  ticks );
        #endif

        pause( "pause", ticks );

      #elif defined( MBG_TGT_NETBSD )

        int timeo = mstohz( msecs );

        #if defined( DEBUG )
          printf( "kpause: %i ms (%i ticks)\n", msecs, timeo );
        #endif

        kpause( "pause", 1, timeo, NULL );

      #endif

    #else  // POSIX user space.

      // Depending on the implementation, usleep is only guaranteed to
      // work for intervals less than 1000000 us, i.e. less than 1 s.
      // So if the interval is 1 s or more, we call sleep() instead.
      //
      // TODO On newer POSIX systems, we should prefer nanosleep()
      // over usleep(), but we'd need to find a safe way to determine
      // if nanosleep() is supported by the target system.

      if ( msecs < 1000 )
        usleep( msecs * 1000 );
      else
        sleep( msecs / 1000 );

    #endif

  #elif defined( MBG_TGT_WIN32 )

    #if defined( MBG_TGT_KERNEL )  // Windows kernel space.

      LARGE_INTEGER delay;

      // We need to pass a negative value to KeDelayExecutionThread()
      // because the given time is a relative time interval, not absolute
      // time. See the API docs for KeDelayExecutionThread().
      delay.QuadPart = - ( (LONGLONG) msecs * HNS_PER_SEC / 1000 );

      KeDelayExecutionThread( KernelMode, FALSE, &delay );

    #else                          // Windows user space.

      // Sleep() expects milliseconds.
      Sleep( msecs );

    #endif

  #elif defined( MBG_TGT_DOS )

    delay( (unsigned) msecs );

  #else

    // This needs to be implemented for the target OS,
    // so this code should result in a linker error.
    do_sleep_msec( msecs );

  #endif

}  // mbg_sleep_msec



/**
 * @brief Sleep for a specific number of seconds.
 *
 * @param[in]  secs  The number of seconds to sleep.
 */
static __mbg_inline
void mbg_sleep_sec( long secs )
{
  mbg_sleep_msec( secs * 1000 );

}  // mbg_sleep_sec



/**
 * @brief Normalize a system timestamp in ::MBG_SYS_TIME format.
 *
 * Implementation depends on the target system.
 *
 * It's assumed that the basic system timestamp is always positive,
 * but an offset has been added or subtracted without accounting
 * for an overflow or underflow.
 *
 * @param[in]  p  Pointer to the time stamp to be normalized.
 */
static __mbg_inline
void mbg_sys_time_normalize( MBG_SYS_TIME *p )
{
  #if defined( MBG_TGT_POSIX )

    while ( p->nano_secs >= NSEC_PER_SEC )
    {
      p->nano_secs -= NSEC_PER_SEC;
      p->secs++;
    }

    while ( p->nano_secs < 0 )
    {
      p->nano_secs += NSEC_PER_SEC;
      p->secs--;
    }

  #elif defined( MBG_TGT_WIN32 )

    // Nothing to do for FILETIME.
    // Avoid warnings "never used".
    (void) p;

  #elif defined( MBG_TGT_DOS )

    // Nothing to do here.
    // This isn't really supported.
    (void) p;

  #else

    #error mbg_sys_time_normalize() needs to be implemented for this target.

  #endif

}  // mbg_sys_time_normalize



/**
 * @brief Add an amount of nanoseconds to a system timestamp in ::MBG_SYS_TIME format.
 *
 * Implementation depends on the target system.
 *
 * It's assumed that the basic system timestamp is always positive.
 *
 * @param[in]  p   Pointer to the time stamp to which to add an offset.
 * @param[in]  ns  A number of nanoseconds to be added to @p *p.
 */
static __mbg_inline
void mbg_sys_time_add_ns( MBG_SYS_TIME *p, long ns )
{
  #if defined( MBG_TGT_POSIX )

    p->nano_secs += ns;
    mbg_sys_time_normalize( p );

  #elif defined( MBG_TGT_WIN32 )

    // FILETIME has 100 ns (HNS) units, so we
    // have to scale it to nanoseconds before
    // the offset is applied, and scale the result
    // back to HNS.
    // TODO Should we also do some rounding
    // to the nearest HNS count?
    *p = ( ( (*p) * 100 ) + ns ) / 100;

  #elif defined( MBG_TGT_DOS )

    // Nothing to do.
    // We don't support nanoseconds anyway,

  #else

    #error mbg_sys_time_add_ns() needs to be implemented for this target.

  #endif

}  // mbg_sys_time_add_ns



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

/* (no header definitions found) */

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif

/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBGSYSTM_H */
