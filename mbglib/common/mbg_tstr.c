
/**************************************************************************
 *
 *  $Id: mbg_tstr.c 1.4 2021/12/01 10:24:09 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Support functions for serial time strings.
 *
 *  See also pcpststr.c for time string support with bus-level devices.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_tstr.c $
 *  Revision 1.4  2021/12/01 10:24:09  martin.burnicki
 *  New functions mbg_tstr_setup_tstr_buffer() and
 *  mbg_tstr_setup_rcv_delay().
 *  Added and updated some doxygen comments.
 *  Revision 1.3  2021/04/21 10:59:50  martin
 *  Huge cleanup and refactoring.
 *  Revision 1.2  2011/04/18 09:41:00  martin
 *  Changes for a real timestring decoder support.
 *  Cleanup.
 *  Not yet thread-safe because of static local variables.
 *  Revision 1.1  2007/11/05 10:47:52  martin
 *  Initial revision
 *
 **************************************************************************/

#define _MBG_TSTR
 #include <mbg_tstr.h>
#undef _MBG_TSTR

#include <mbgerror.h>
#include <mbgmktm.h>
#include <cnv_wday.h>
#include <str_util.h>
#include <evalutil.h>

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#if defined( MBG_TGT_WIN32 )
  #include <timeutil.h>
#elif defined( MBG_TGT_POSIX )
  #include <sys/time.h>
#endif


#define DEBUG_TSTR_DATA  0  // FIXME TODO
#define DEBUG_EVAL       0  // FIXME TODO



// The year limit used to convert 2 digit year numbers
// (year of the century) to full 4 digit year numbers.
// Needs to be defined by the application.
extern int mbg_exp_year_limit;



static /*HDR*/
/**
 * @brief Do a formal format check of a received time string.
 *
 * @param[in]  buffer          The buffer with the received string.
 * @param[in]  bytes_received  The number of received bytes in the @p buffer.
 * @param[in]  p_spec          Pointer to the specs of the expected time string.
 *
 * @return  ::MBG_SUCCESS if the string format is OK,<br>
 *          ::MBG_ERR_STR_LEN if the string lenght doesn't match, or<br>
 *          ::MBG_ERR_DATA_FMT if the received string isn't appropriate for the template.
 */
int check_timestr_fmt( const char *buffer, size_t bytes_received, const MBG_TSTR_SPEC *p_spec )
{
  size_t i;

  #if defined( DEBUG )
    if ( p_spec->str_template && p_spec->str_length )
    {
      long l = (long) strlen( p_spec->str_template );

      if (  l != p_spec->str_length )
        fprintf( stderr, "Warning: spec str len %li doesn't match template len %li for %s string!\n",
                 p_spec->str_length, l, p_spec->name );
    }

  #endif

  // Only if the string has a given length (i.e. p->str_length != 0),
  // we check if the number of receivved bytes matches the expected
  // length of the string.
  if ( p_spec->str_length && ( bytes_received != p_spec->str_length ) )
    return MBG_ERR_STR_LEN;

  for ( i = 0; i < bytes_received; i++ )
  {
    if ( ( p_spec->str_template[i] != p_spec->skip_char )
      && ( buffer[i] != p_spec->str_template[i] ) )
      return MBG_ERR_DATA_FMT;
  }

  return MBG_SUCCESS;

}  // check_timestr_fmt



static /*HDR*/
int asc_to_bin( char c )
{
  return c - '0';

}  // asc_to_bin



static /*HDR*/
int int_from_str2( const char *s )
{
  return asc_to_bin( s[0] ) * 10 + asc_to_bin( s[1] );

}  // int_from_str2



/*HDR*/
/**
 * @brief Print a time string to a string.
 *
 * Replace some control characters (e.g. STX, ETX, CR, LF)
 * by readable tags, e.g. "<stx>".
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  ts       The time string to be printed.
 *
 * @return Length of the string in the buffer.
 */
int snprint_time_str( char *s, size_t max_len, const char *ts )
{
  const char *cp;
  int n = 0;

  for ( cp = ts; *cp; cp++ )
  {
    switch ( *cp )
    {
      case STX_CHAR:
        n += snprintf_safe( &s[n], max_len - n, "<stx>" );
        break;

      case ETX_CHAR:
        n += snprintf_safe( &s[n], max_len - n, "<etx>" );
        break;

      case '\r':
        n += snprintf_safe( &s[n], max_len - n, "<cr>" );
        break;  // do nothing

      case '\n':
        n += snprintf_safe( &s[n], max_len - n, "<lf>" );
        break;

      default:
        n += snprintf_safe( &s[n], max_len - n, "%c", *cp );
    }
  }

  return n;

}  // snprint_time_str



/*HDR*/
/**
 * @brief Clear an @ref MBG_TSTR_DATA structure.
 *
 * @param[out]  p_data  Address of the structure to be cleared.
 */
void mbg_tstr_clear_tstr_data( MBG_TSTR_DATA *p_data )
{
  memset( p_data, 0, sizeof( *p_data ) );

}  // mbg_tstr_clear_tstr_data



static __mbg_inline /*HDR*/
void decode_date_default( struct tm *p_tm, const char *s )
{
  p_tm->tm_mday = int_from_str2( &s[0] );
  p_tm->tm_mon = int_from_str2( &s[3] );
  p_tm->tm_year = int_from_str2( &s[6] );

}  // decode_date_default



static __mbg_inline /*HDR*/
void decode_wday_default( struct tm *p_tm, const char *s )
{
  p_tm->tm_wday = asc_to_bin( s[0] );

}  // decode_wday_default



static __mbg_inline /*HDR*/
void decode_time_default( struct tm *p_tm, const char *s )
{
  p_tm->tm_hour = int_from_str2( &s[0] );
  p_tm->tm_min = int_from_str2( &s[3] );
  p_tm->tm_sec = int_from_str2( &s[6] );

}  // decode_time_default



static /*HDR*/
/**
 * @brief Decode status characters used with some time string formats.
 *
 * Some time string formats use the same status characters, so
 * we can use the same function to check and decode the status.
 *
 * @param[out]  p_data   The time string data to be updated.
 * @param[in]   s        Start of the status characters part of the time string.
 * @param[in]   seconds  Seconds count, used to determine a leap second status.
 *
 * @return  ::MBG_SUCCESS if decoded successfully, else ::MBG_ERR_INV_PARM.
 */
int decode_status_mbg_std( MBG_TSTR_DATA *p_data, const char *s, int seconds )
{
  // First check if all status chars are valid.

  if ( s[0] != '#' && s[0] != ' ' )
    goto fail;

  if ( s[1] != '*' && s[1] != ' ' )
    goto fail;

  if ( s[2] != 'U' && s[2] != 'S' && s[2] != ' ' )
    goto fail;

  if ( s[3] != '!' && s[3] != 'A' && s[3] != ' ' )
    goto fail;


  // All status chars are valid, so evaluate them.

  if ( s[0] == ' ' && s[1] == ' ' )
    p_data->status |= TSTR_ST_IS_SYNC;

  if ( s[2] == 'U' )  // Time is UTC.
    p_data->utc_offs = 0;
  else
    if ( s[2] == 'S' )  // DST is in effect.
    {
      // For the "Meinberg Standard" time string
      // this usually means CEST, i.e. UTC+2h.
      p_data->utc_offs = 2 * SECS_PER_HOUR;
      p_data->status |= TSTR_ST_DL_ENB;
    }
    else
    {
      // For the "Meinberg Standard" time string
      // this usually means CET, i.e. UTC+1h.
      p_data->utc_offs = SECS_PER_HOUR;
    }

  p_data->status |= TSTR_ST_UTC_OFFS_VALID;

  // Check fo announcement of a leap second
  // or a change in DST.
  if ( s[3] == 'A' )
    p_data->status |= TSTR_ST_LS_ANN;
  else
    if ( s[3] == '!' )
      p_data->status |= TSTR_ST_DL_ANN;

  // Check if the current string indicates
  // an inserted leap second.
  if ( seconds == 60 )
    p_data->status |= TSTR_ST_LS_ENB;

  return MBG_SUCCESS;

fail:
  return MBG_ERR_INV_PARM;

}  // decode_status_mbg_std



/*HDR*/
/**
 * @brief Decode date, day-of-week and time from some time string formats.
 *
 * Some time string formats use the same format for date, day-of-week,
 * and time, so we can use the same function to check and decode these.
 *
 * @param[out]  p_tm  Address of a <em>struct tm</em> to take the decoded data.
 * @param[in]   s     Start of the relevant part of a time string.
 *
 * @return  ::MBG_SUCCESS.
 */
int decode_date_time_wday_mbg_std( struct tm *p_tm, const char *s )
{
  decode_date_default( p_tm, &s[0] );
  decode_wday_default( p_tm, &s[11] );
  decode_time_default( p_tm, &s[15] );

  return MBG_SUCCESS;

}  // decode_date_time_wday_mbg_std



/*HDR*/
/**
 * @brief Decode the contents of a "Meinberg Standard" time string.
 *
 * @param[in,out]  p_tsri  Address of the ::MBG_TSTR_RCV_INFO stucture to be evaluated.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_tstr_decode_mbg_std( MBG_TSTR_RCV_INFO *p_tsri )
{
  const MBG_TSTR_SPEC *p_spec = p_tsri->tstr_spec;
  struct tm *p_tm;
  int rc = MBG_SUCCESS;

  // Only if the string has a given length (i.e. p_spec->str_length != 0),
  // we check if the number of received bytes matches the expected
  // length of the string.
  if ( p_spec->str_length && ( p_tsri->bytes_received != p_spec->str_length ) )
    return MBG_ERR_STR_LEN;

  p_tm = &p_tsri->tstr_data.tm;

  decode_date_time_wday_mbg_std( p_tm, &p_tsri->buffer[3] );

  // *p_tm should now contain valid time and date values,
  // with 2-digit year number, which differs from what
  // mktime() expects.
  if ( !is_valid_time( p_tm->tm_hour, p_tm->tm_min, p_tm->tm_sec ) ||
       !is_valid_short_date( p_tm->tm_mday, p_tm->tm_mon ) ||
       !is_valid_short_year( p_tm->tm_year ) ||
       !is_valid_wday_1_7( p_tm->tm_wday ) )
    return MBG_ERR_INV_PARM;


  // FIXME TODO TSTR_ST_INV_TIME

  // Evaluate the status characters of the time string.
  // Also pass the seconds count to see if we have an
  // inserted leap second.
  rc = decode_status_mbg_std( &p_tsri->tstr_data, &p_tsri->buffer[27], p_tm->tm_sec );

  if ( mbg_rc_is_error( rc ) )
    return rc;


  // Everything looks good, so we adjust the values of struct tm
  // to the numbers expected by mktime().
  p_tm->tm_mon--;
  p_tm->tm_wday = _wday_mon17_to_sun06( p_tm->tm_wday );
  p_tm->tm_year = mbg_exp_year( p_tm->tm_year, mbg_exp_year_limit ) - 1900;

  return rc;

}  // mbg_tstr_decode_mbg_std

MBG_TSTR_DECODE_FNC mbg_tstr_decode_mbg_std;



static /*HDR*/
/**
 * @brief Decode the %UTC offset part of the "Uni Erlangen" string format.
 *
 * @param[out]  p_data   The time string data to be updated.
 * @param[in]   s        Start of the %UTC offset part of the time string.
 *
 * @return  ::MBG_SUCCESS if decoded successfully, else ::MBG_ERR_INV_PARM.
 */
int decode_utc_offs_uni_erl( MBG_TSTR_DATA *p_data, const char *s )
{
  p_data->utc_offs = time_offs_secs_from_string( s );
  p_data->status |= TSTR_ST_UTC_OFFS_VALID;

  return MBG_SUCCESS;

}  // decode_utc_offs_uni_erl



static /*HDR*/
/**
 * @brief Decode status characters used with the "Uni Erlangen" format.
 *
 * @param[out]  p_data   The time string data to be updated.
 * @param[in]   s        Start of the status characters part of the time string.
 * @param[in]   seconds  Seconds count, used to determine a leap second status.
 *
 * @return  ::MBG_SUCCESS if decoded successfully, else ::MBG_ERR_INV_PARM.
 */
int decode_status_uni_erl( MBG_TSTR_DATA *p_data, const char *s, int seconds )
{
  // First check if all status chars are valid.

  if ( s[0] != '#' && s[0] != ' ' )
    goto fail;

  if ( s[1] != '*' && s[1] != ' ' )
    goto fail;

  if ( s[2] != 'S' && s[2] != ' ' )
    goto fail;

  if ( s[3] != '!' && s[3] != ' ' )
    goto fail;

  if ( s[4] != 'A' && s[4] != ' ' )
    goto fail;

  // s[5] is always ' ' and checked via the template.

  if ( s[6] != 'L' && s[6] != ' ' )
    goto fail;


  // All status chars are valid, so evaluate them.

  if ( s[0] == ' ' && s[1] == ' ' )
    p_data->status |= TSTR_ST_IS_SYNC;

  if ( s[2] == 'S' )  // DST is in effect.
    p_data->status |= TSTR_ST_DL_ENB;

  if ( s[3] == '!' )
    p_data->status |= TSTR_ST_DL_ANN;

  if ( s[4] == 'A' )
    p_data->status |= TSTR_ST_LS_ANN;

  // Plausifility check for inserted leap second.
  if ( ( ( s[6] == 'L' ) && ( seconds != 60 ) ) ||
       ( ( s[6] == ' ' ) && ( seconds == 60 ) ) )
    fprintf( stderr, "Warning: Leap second status char '%c' disagrees with seconds count %i!",
             s[6], seconds );

  if ( ( s[6] == 'L' ) || ( seconds == 60 ) )
    p_data->status |= TSTR_ST_DL_ENB;

  return MBG_SUCCESS;

fail:
  return MBG_ERR_INV_PARM;

}  // decode_status_uni_erl



/*HDR*/
/**
 * @brief Decode the contents of a "Uni Erlangen" time string.
 *
 * @param[in,out]  p_tsri  Address of the ::MBG_TSTR_RCV_INFO stucture to be evaluated.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_tstr_decode_uni_erl( MBG_TSTR_RCV_INFO *p_tsri )
{
  const MBG_TSTR_SPEC *p_spec = p_tsri->tstr_spec;
  struct tm *p_tm;
  int rc = MBG_SUCCESS;

  // Only if the string has a given length (i.e. p_spec->str_length != 0),
  // we check if the number of received bytes matches the expected
  // length of the string.
  if ( p_spec->str_length && ( p_tsri->bytes_received != p_spec->str_length ) )
    return MBG_ERR_STR_LEN;

  p_tm = &p_tsri->tstr_data.tm;

  decode_date_default( p_tm, &p_tsri->buffer[1] );
  decode_wday_default( p_tm, &p_tsri->buffer[11] );
  decode_time_default( p_tm, &p_tsri->buffer[14] );

  // *p_tm should now contain valid time and date values,
  // with 2-digit year number, which differs from what
  // mktime() expects.
  if ( !is_valid_time( p_tm->tm_hour, p_tm->tm_min, p_tm->tm_sec ) ||
       !is_valid_short_date( p_tm->tm_mday, p_tm->tm_mon ) ||
       !is_valid_short_year( p_tm->tm_year ) ||
       !is_valid_wday_1_7( p_tm->tm_wday ) )
    return MBG_ERR_INV_PARM;

  rc = decode_utc_offs_uni_erl( &p_tsri->tstr_data, &p_tsri->buffer[24] );

  if ( mbg_rc_is_error( rc ) )
    return rc;


  // FIXME TODO TSTR_ST_INV_TIME

  // Evaluate the status characters of the time string.
  // Also pass the seconds count to see if we have an
  // inserted leap second.
  rc = decode_status_uni_erl( &p_tsri->tstr_data, &p_tsri->buffer[32], p_tm->tm_sec );

  if ( mbg_rc_is_error( rc ) )
    return rc;


  // Everything looks good, so we adjust the values of struct tm
  // to the numbers expected by mktime().
  p_tm->tm_mon--;
  p_tm->tm_wday = _wday_mon17_to_sun06( p_tm->tm_wday );
  p_tm->tm_year = mbg_exp_year( p_tm->tm_year, mbg_exp_year_limit ) - 1900;

  return rc;

}  // mbg_tstr_decode_uni_erl

MBG_TSTR_DECODE_FNC mbg_tstr_decode_uni_erl;



/*HDR*/
/**
 * @brief Get the number of supported time string formats.
 *
 * @return The number of non-NULL entries in ::tstr_spec_table.
 */
unsigned int mbg_tstr_get_fmt_cnt( void )
{
  unsigned int cnt;

  for ( cnt = 0; tstr_spec_table[cnt]; cnt++ );

  return cnt;

}  // mbg_tstr_get_fmt_cnt



/*HDR*/
/**
 * @brief Set up the receive info structure for a specific time string.
 *
 * Check if the index @p tstr_idx is in range, store it and allocate
 * (or re-allocate) a receive buffer of @p rcv_buffer_size bytes.
 *
 * @param[in,out]  p_tsri           Address of the ::MBG_TSTR_RCV_INFO stucture to be updated.
 * @param[in]      tstr_idx         Index of the time string spec in ::tstr_spec_table.
 * @param[in]      rcv_buffer_size  Size of the receive buffer to be allocated.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_tstr_setup_tstr_buffer( MBG_TSTR_RCV_INFO *p_tsri, unsigned int tstr_idx,
                                size_t rcv_buffer_size )
{
  unsigned int n_tstr_fmt = mbg_tstr_get_fmt_cnt();

  if ( tstr_idx >= n_tstr_fmt )  // Index out of range.
    return MBG_ERR_INV_PARM;

  p_tsri->tstr_spec = tstr_spec_table[tstr_idx];

  // If a buffer has already been allocated, free it.
  if ( p_tsri->buffer )
    free( p_tsri->buffer );

  // Allocate a buffer for the time string.
  p_tsri->buffer_size = rcv_buffer_size;
  p_tsri->buffer = malloc( p_tsri->buffer_size );

  if ( p_tsri->buffer == NULL )
    return mbg_get_last_error( NULL );

  // This isn't required, but useful for debugging.
  memset( p_tsri->buffer, 0xFF, p_tsri->buffer_size );

  return MBG_SUCCESS;

}  // mbg_tstr_setup_tstr_buffer



/*HDR*/
/**
 * @brief Set up the expected transmission time for the receive case.
 *
 * The determined time depends on the receive FIFO threshold of the serial port,
 * the baud rate, framing, etc., and can be used determine the point in time
 * when the transmission was started.
 *
 * @param[in,out]  p_tsri      Address of the ::MBG_TSTR_RCV_INFO stucture to be updated.
 * @param[in]      sdev        Pointer to the serial device info.
 * @param[in]      n_adj_bits  The number of additional serial bits to be accounted for
 *                             when computing the full transmission time.
 * @return  ::MBG_SUCCESS.
 */
int mbg_tstr_setup_rcv_delay( MBG_TSTR_RCV_INFO *p_tsri, const MBGSERIO_DEV *sdev,
                              int n_adj_bits )
{
  // Set up the expected transmission delay.
  //
  // The rcv_fifo_threshold value used here only makes sense if the on-time
  // character is the first character of the time string.
  //
  // Things are probably more complex if the on-time character is e.g.
  // a CR near the end of the time string, and rcv_fifo_threshold is not 1.
  p_tsri->rcv_delay_ns = mbgserio_get_duration_rcv( &sdev->sp,
                                  sdev->rcv_fifo_threshold, n_adj_bits );

  return MBG_SUCCESS;

}  // mbg_tstr_setup_rcv_delay



/*HDR*/
/**
 * @brief Register a receive callback function.
 *
 * The registered callback function is called to notifiy the application
 * when a time string has been successfully received, or when an error occured.
 *
 * @param[in,out]  p_tsri  Address of the ::MBG_TSTR_RCV_INFO stucture to be updated.
 * @param[in]      fnc     The callback function to be registered.
 *
 * @return  ::MBG_SUCCESS.
 */
int mbg_tstr_set_rcv_callback( MBG_TSTR_RCV_INFO *p_tsri, MBG_TSTR_RCV_CALLBACK_FNC *fnc )
{
  p_tsri->callback = fnc;

  return MBG_SUCCESS;

}  // mbg_tstr_set_rcv_callback



/*HDR*/
/**
 * @brief Receive characters of a time string.
 *
 * This function should be called for every character that
 * has been been received, to check if it matches the
 * expected time string format.
 *
 * @param[in]      c       A received character.
 * @param[in,out]  p_tsri  Address of a time string receive info structure.
 *
 * @return  ::MBG_SUCCESS if the character was accepted,<br>
 *          ::MBG_ERR_RANGE if the character was out of range, not accepted,<br>
 *          ::MBG_ERR_OVERFLOW if the receive buffer overflows,<br>
 *          ::MBG_ERR_STR_LEN if the string lenght doesn't match, or<br>
 *          ::MBG_ERR_DATA_FMT if the received string isn't appropriate for the template.
 */
int mbg_tstr_receive( unsigned char c, MBG_TSTR_RCV_INFO *p_tsri )
{
  const MBG_TSTR_SPEC *p_spec = p_tsri->tstr_spec;
  int rc = MBG_SUCCESS;

  if ( p_spec )
  {
    if ( c == p_spec->ontime_char )
      mbg_get_sys_time( &p_tsri->ts_rcv_raw );

    if ( c == p_spec->first_char )
    {
      p_tsri->bytes_received = 0;
      p_tsri->first_char_received = true;
    }
  }

  if ( !p_tsri->first_char_received )
  {
    // The received character doesn't belong to the expected
    // time string. So if an info string has been specified,
    // we just dump it.
    if ( p_tsri->info )
    {
      printf( "%s 0x%02X", p_tsri->info, c );

      if ( ( c >= ' ' ) && ( c < '\x7F' ) )
        printf( " '%c'", c );

      printf( "\n" );
    }

    rc = MBG_ERR_RANGE;  // TODO
    goto out;
  }

  if ( p_tsri->bytes_received >= p_tsri->buffer_size - 1 )
  {
    printf( " ** Warning: Buffer Overflow!\n" );
    rc = MBG_ERR_OVERFLOW;
    goto callback_out;
  }

  p_tsri->buffer[p_tsri->bytes_received] = c;
  p_tsri->bytes_received++;
  p_tsri->buffer[p_tsri->bytes_received] = 0;

  if ( p_spec && ( c == p_spec->term_char ) )
  {
    // The received character matched the termination
    // character of the expected time string.
    // Evaluate the time string.
    rc = check_timestr_fmt( p_tsri->buffer, p_tsri->bytes_received, p_spec );

    // Re-initialize reception in any case.
    p_tsri->first_char_received = false;

    if ( mbg_rc_is_error( rc ) )
    {
      // TODO Maybe check if string matches one of the other formats.
      printf( "\tInvalid Timestring!\n" );
      rc = MBG_ERR_DATA_FMT;
      goto callback_out;
    }

    // The formal string format matches what we expected.

    // Set up the compensated receive time stamp which should
    // be appropriate for the beginning of the transmission of
    // the on-time character at the beginning of the time string.
    p_tsri->ts_rcv_comp = p_tsri->ts_rcv_raw;
    mbg_sys_time_add_ns( &p_tsri->ts_rcv_comp, -p_tsri->rcv_delay_ns );

    mbg_tstr_clear_tstr_data( &p_tsri->tstr_data );

    // Call the decode function that should have
    // been specified for the string format.
    if ( p_spec->decode_fnc )
    {
      rc = p_spec->decode_fnc( p_tsri );

      if ( mbg_rc_is_success( rc ) )
        rc = mbg_mktime64_from_tm( &p_tsri->tstr_data.t64, &p_tsri->tstr_data.tm );
    }

    goto callback_out;
  }

  goto out;


callback_out:
  // Now call the callback function to notify the application,
  // if such a function has been registered.
  if ( p_tsri->callback )
    rc = p_tsri->callback( p_tsri, rc );

out:
  return rc;

}  // mbg_tstr_receive

