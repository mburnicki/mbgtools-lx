
/**************************************************************************
 *
 *  $Id: mbg_syn1588_io.h 1.6 2022/06/01 20:00:34 martin.burnicki REL_M $
 *  $Name:  $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for mbg_syn1588_io.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_syn1588_io.h $
 *  Revision 1.6  2022/06/01 20:00:34  martin.burnicki
 *  Removed inappropriate function attributes from some functions.
 *  Revision 1.5  2022/05/25 09:53:52Z  martin.burnicki
 *  Updated function prototypes.
 *  Removed obsolete code.
 *  Revision 1.4  2021/12/01 19:46:24  martin.burnicki
 *  New functions mbg_syn1588_get_time_info_hrt()
 *  and mbg_syn1588_get_time_info_tstamp().
 *  Revision 1.3  2021/11/08 21:16:08  martin.burnicki
 *  Cleaned up device handling.
 *  Updated function prototypes.
 *  Revision 1.2  2021/09/30 14:00:41  martin
 *  Collapsed development branch.
 *  Function chk_if_syn1588_type() can be called to see if a device handle
 *  refers to a SYN1588, or not. This function does not scan the list of
 *  SYN1588 devices if no such device has been detected anyway, and thus
 *  avoids unnecessarily increased execution time.
 *  Revision 1.1  2019/05/03 10:37:48  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_SYN1588_IO_H
#define _MBG_SYN1588_IO_H

/* Other headers to be included */

#include <mbgdevio.h>

#if defined( MBG_TGT_WIN32 )
  #include <mbgsvctl.h>
#endif

#if SUPP_SYN1588_USR_SPC_CPP
  #include <syn1588_c_api.h>
#endif

#if SUPP_SYN1588_USR_SPC
  #include <nanotime.h>
  #include <mbgutil.h>
#endif

#include <stdio.h>

#ifdef _MBG_SYN1588_IO
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif

#if !defined( SUPP_SYN1588_USR_SPC_CPP )
  #if SUPP_SYN1588_USR_SPC
    #define SUPP_SYN1588_USR_SPC_CPP     1  // 1 or 0, as appropriate
  #else
    #define SUPP_SYN1588_USR_SPC_CPP     0  // always 0
  #endif
#endif



#if SUPP_SYN1588_USR_SPC

#define MBG_SYN1588_TYPE_NAME  "SYN1588"

_ext const char mbg_syn1588_type_name[]
#ifdef _DO_INIT
  = MBG_SYN1588_TYPE_NAME
#endif
;

#endif  // SUPP_SYN1588_USR_SPC



#if !defined ( MBG_TGT_WIN32 )

/**
 * @brief The number of SYN1588 devices found in the system.
 *
 * Will be set by functions like mbg_find_devices(), and can
 * be used for a fast check to see if those devices have to be
 * accounted for.
 *
 * On Windows, this variable is exported by the mbgsvctl.dll,
 * so it is defined elsewhere.
 *
 * For non-Windows systems, the variable is defined here.
 */
_ext int syn1588_devices_found;

#endif  // !defined ( MBG_TGT_WIN32 )


/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Retrieve the device index associated with a device handle.
 *
 * @param[in]  dh  The device handle to be checked.
 *
 * @return  On success, a valid device index, else ::SYN1588_INVALID_DEV_IDX.
 *
 * @see ::is_mbg_syn1588_type
 */
 SYN1588_DEV_IDX mbg_syn1588_get_dev_idx( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device handle is associated with a SYN1588 device.
 *
 * This function checks if a specified handle can be found in a
 * list of device handles associated with SYN1588 devices.
 *
 * It may take some time to scan the the list, so the function
 * ::chk_if_syn1588_type should be used preferably, if possible,
 * because that function scans the list only if any SYN1588 device
 * has been detected.
 *
 * @param[in]  dh  The device handle to be checked.
 *
 * @return true, if the device is a SYN1588 type, else false.
 *
 * @see ::chk_if_syn1588_type
 */
 bool is_mbg_syn1588_type( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a string is a valid SYN1588 pseudo device file name.
 *
 * @param[in]  s  The string to be checked.
 *
 * @return ::MBG_SUCCESS if @p s is a valid device file name, else ::MBG_ERR_INV_PARM.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
 int mbg_syn1588_chk_dev_fn_is_valid( const char *s ) ;

 int mbg_syn1588_dev_fn_from_dev_idx( char *s, int max_len, int dev_idx ) ;
 void mbg_syn1588_close_device( MBG_DEV_HANDLE *p_dh ) ;
 int mbg_syn1588_open_device( SYN1588_DEV_IDX dev_idx, MBG_DEV_HANDLE *p_dh ) ;
 int mbg_syn1588_open_device_by_dev_fn( const char *dev_fn, MBG_DEV_HANDLE *p_dh ) ;
 int mbg_syn1588_find_pnp_device( SYN1588_DEV_IDX dev_idx, MBG_DEV_FN *p_dev_fn ) ;
 int mbg_syn1588_find_devices( int max_devs, MBG_DEV_FN dev_fn_array[] ) ;
 int mbg_syn1588_get_device_info( MBG_DEV_HANDLE dh, PCPS_DEV *p ) ;
 int mbg_syn1588_setup_receiver_info( MBG_DEV_HANDLE dh, RECEIVER_INFO *p ) ;
 /**
 * @brief Retrieve the content of a SYN1588 NIC's PTP_SYNC_STATUS register.
 *
 * @note The value returned via @p p is set to 0 if the status is not
 *       available, or an error occurred.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle.
 * @param[out]  p   Pointer to a ::SYN1588_PTP_SYNC_STATUS variable to be filled.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_syn1588_get_ptp_sync_status( MBG_DEV_HANDLE dh, SYN1588_PTP_SYNC_STATUS *p ) ;

 int mbg_syn1588_get_fast_hr_timestamp( MBG_DEV_HANDLE dh, PCPS_TIME_STAMP *p ) ;
 int mbg_syn1588_get_fast_hr_timestamp_cycles( MBG_DEV_HANDLE dh, PCPS_TIME_STAMP_CYCLES *p ) ;
 int mbg_syn1588_get_hr_time( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p ) ;
 int mbg_syn1588_get_hr_time_cycles( MBG_DEV_HANDLE dh, PCPS_HR_TIME_CYCLES *p ) ;
 int mbg_syn1588_get_time( MBG_DEV_HANDLE dh, PCPS_TIME *p ) ;
 int mbg_syn1588_get_time_cycles( MBG_DEV_HANDLE dh, PCPS_TIME_CYCLES *p ) ;
 /**
 * @brief Read both system time and associated device time from the kernel driver.
 *
 * @todo This info as well as the implementation for SYN1588 devices
 * needs to be fixed.
 *
 * The kernel driver reads the current system time plus a HR time structure
 * from a device immediately after each other. The returned info structure also
 * contains some cycles counts to be able to determine the execution times
 * required to read those time stamps.
 *
 * The advantage of this call compared to ::mbg_get_time_info_tstamp is
 * that this call also returns the status of the device. On the other hand, reading
 * the HR time from the device may block e.g. if another application accesses
 * the device.
 *
 * This call makes a ::mbg_get_hr_time_cycles call internally so the API call
 * ::mbg_chk_dev_has_hr_time can be used to check whether this call is supported
 * by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::MBG_TIME_INFO_HRT structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_time_info_tstamp
 */
 int mbg_syn1588_get_time_info_hrt( MBG_DEV_HANDLE dh, MBG_TIME_INFO_HRT *p ) ;

 /**
 * @brief Read both system time and associated device timestamp from the kernel driver.
 *
 * @todo This info as well as the implementation for SYN1588 devices
 * needs to be fixed.
 *
 * This call is similar to ::mbg_get_time_info_hrt except that a
 * ::mbg_get_fast_hr_timestamp_cycles call is made internally, so
 * the API call ::mbg_chk_dev_has_fast_hr_timestamp can be used to check whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_TIME_INFO_TSTAMP structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_time_info_hrt
 */
 int mbg_syn1588_get_time_info_tstamp( MBG_DEV_HANDLE dh, MBG_TIME_INFO_TSTAMP *p ) ;

 void mbg_syn1588_tests( MBG_DEV_HANDLE dh ) ;

/* ----- function prototypes end ----- */


/**
 * @brief Check if a device handle refers to a SYN1588 type
 *
 * To keep execution time as short as possible, this inline function
 * first checks if any SYN1588 type has been found at all, and only
 * if this is the case it checks if the device handle is in the list
 * of handles referring to SYN1588 devices.
 *
 * @param[in]  dh  The device handle to be checked.
 *
 * @return true, if the device is a SYN1588 type, else false.
 *
 * @see ::is_mbg_syn1588_type
 */
static __mbg_inline /*HDR*/
bool chk_if_syn1588_type( MBG_DEV_HANDLE dh )
{
  return ( syn1588_devices_found != 0 ) && is_mbg_syn1588_type( dh );

}  // chk_if_syn1588_type


#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_SYN1588_IO_H */
