
/**************************************************************************
 *
 *  $Id: mbgserio.h 1.13 2023/05/09 10:00:41 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for mbgserio.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgserio.h $
 *  Revision 1.13  2023/05/09 10:00:41  martin
 *  Fixed a doxygen comment.
 *  Revision 1.12  2021/12/01 10:19:10  martin.burnicki
 *  Make a parameter of mbgserio_get_duration_rcv() const.
 *  Revision 1.11  2021/05/04 20:43:25  martin
 *  New field MBGSERIO_DEV::rcv_fifo_threshold, for the receive FIFO
 *  threshold, and structure MBGSERIO_SPEED_PARMS. These are useful
 *  to determine the transmission delay of a received time string.
 *  Removed some trailing spaces.
 *  Updated some comments and function prototypes.
 *  Revision 1.10  2021/04/07 16:32:11  martin
 *  Updated a bunch of comments, and renamed some local variables.
 *  Revision 1.9  2018/11/29 16:03:58  martin
 *  Removed obsolete conditional _USE_MBG_TSTR stuff.
 *  Revision 1.8  2017/07/06 07:41:00Z  martin
 *  Renamed SERIAL_IO_STATUS to MBGSERIO_DEV,
 *  and renamed some fields of the structure.
 *  Defined default device names for serial ports
 *  depending on the target system.
 *  Check preprocessor symbol _USE_MBG_TSTR instead
 *  of _USE_CHK_TSTR.
 *  Check for MBG_TGT_POSIX instead of MBG_TGT_UNIX.
 *  Updated function prototypes.
 *  Revision 1.7  2013/02/01 16:10:45  martin
 *  Got rid of _mbg_open/close/read/write() macros.
 *  Functions are now used instead.
 *  Updated function prototypes.
 *  Revision 1.6  2011/08/23 10:15:25Z  martin
 *  Updated function prototypes.
 *  Revision 1.5  2011/08/04 09:48:55  martin
 *  Support flushing output.
 *  Re-ordered some definitions.
 *  Revision 1.4  2009/09/01 10:54:29  martin
 *  Include mbg_tmo.h for the new portable timeout functions.
 *  Added symbols for return codes in case of an error.
 *  Code cleanup.
 *  Revision 1.3  2009/04/01 14:17:31  martin
 *  Cleanup for CVI.
 *  Revision 1.2  2008/09/04 15:11:36Z  martin
 *  Preliminary support for device lists.
 *  Updated function prototypes.
 *  Revision 1.1  2007/11/12 16:48:02  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBGSERIO_H
#define _MBGSERIO_H


/* Other headers to be included */

#include <mbg_tgt.h>
#include <mbg_tmo.h>
#include <mbgerror.h>

#include <stdlib.h>
#include <string.h>

#if defined( MBG_TGT_POSIX )
  #include <termios.h>
#endif

#if !defined( _USE_SELECT_FOR_SERIAL_IO )
  #if defined( MBG_TGT_POSIX )
    #define _USE_SELECT_FOR_SERIAL_IO  1
  #else
    #define _USE_SELECT_FOR_SERIAL_IO  0
  #endif
#endif


#ifdef _MBGSERIO
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif

#if !defined( DEFAULT_SERIAL_DEVICE_NAME )
  // For applications that support 2 serial ports, we also
  // define default names for the second port to be used.
  #if defined( MBG_TGT_WIN32 ) || defined( MBG_TGT_DOS )
    #define DEFAULT_SERIAL_DEVICE_NAME   "COM1"
    #define DEFAULT_SERIAL_DEVICE_NAME_2 "COM2"
  #elif defined( MBG_TGT_LINUX )
    #define DEFAULT_SERIAL_DEVICE_NAME   "/dev/ttyS0"
    #define DEFAULT_SERIAL_DEVICE_NAME_2 "/dev/ttyS1"
  #elif defined( MBG_TGT_FREEBSD )
    // Call-in ports are named /dev/ttyuN where N is the port number, starting from 0.
    // Generally, the call-in port is used for terminals. Call-in ports require that
    // the serial line assert the "Data Carrier Detect" signal to work correctly.
    // Call-out ports are named /dev/cuauN on FreeBSD versions 8.X and later and
    // /dev/cuadN on FreeBSD versions 7.X and earlier. Call-out ports are usually not
    // used for terminals, but are used for modems. The call-out port can be used if the
    // serial cable or the terminal does not support the "Data Carrier Detect" signal.
    #if ( __FreeBSD__ > 7 )
      #define DEFAULT_SERIAL_DEVICE_NAME   "/dev/cuau0"
      #define DEFAULT_SERIAL_DEVICE_NAME_2 "/dev/cuau1"
    #else
      #define DEFAULT_SERIAL_DEVICE_NAME   "/dev/cuad0"
      #define DEFAULT_SERIAL_DEVICE_NAME_2 "/dev/cuad1"
    #endif
  #elif defined( MBG_TGT_NETBSD )
    // The ttyXX devices are traditional dial-in devices;
    // the dtyXX devices are used for dial-out. See tty(4).
    #define DEFAULT_SERIAL_DEVICE_NAME   "/dev/dty00"
    #define DEFAULT_SERIAL_DEVICE_NAME_2 "/dev/dty01"
  #else
    #error DEFAULT_SERIAL_DEVICE_NAME needs to be defined for this target.
  #endif
#endif


/*
 * The following macros control parts of the build process.
 * The default values are suitable for most cases but can be
 * overridden by global definitions, if required.
 */

#if _IS_MBG_FIRMWARE

  // This handle type in not used by the firmware.
  // However, we define it to avoid build errors.
  typedef int MBG_HANDLE;

#else

  #if defined( MBG_TGT_CVI )

    #include <rs232.h>

  #elif defined( MBG_TGT_WIN32 )

    #include <io.h>

  #elif defined( MBG_TGT_DOS )

    #if defined( _USE_V24TOOLS )
      #include <v24tools.h>
    #endif

  #endif

#endif



typedef struct _MBG_STR_LIST
{
  char *s;
  struct _MBG_STR_LIST *next;

} MBG_STR_LIST;



/**
 * @brief Serial parameters required to compute the transmission time.
 *
 * @see ::mbgserio_get_duration_xmt
 * @see ::mbgserio_get_duration_rcv
 */
typedef struct
{
  uint32_t baud_rate;  ///< Baud rate (0 if unknown).
  int data_bits;       ///< Number of data bits (7 or 8, 0 if unknown).
  int parity_bits;     ///< Number of parity bits (0 or 1).
  int stop_bits;       ///< Number of stop bits (1 or 2, 0 if unknown).

} MBGSERIO_SPEED_PARMS;



/**
 * @brief Control structure for a serial device.
 *
 * Should be considered opaque.
 */
typedef struct
{
  MBG_PORT_HANDLE port_handle;  ///< The handle that will be used for the device.

  MBGSERIO_SPEED_PARMS sp;      ///< Can be used to compute the transmission time.

  /// @brief The receive FIFO threshold of the UART.
  ///
  /// The UART generates a receive event (e.g. an IRQ) after
  /// at least this number of bytes has been received. This may
  /// affect the timing. 0 if unknown or can't be determined.
  int rcv_fifo_threshold;

  ulong poll_timeout;           ///< The default timeout when waiting for data, [ms].

  #if defined( MBG_TGT_WIN32 )
    DCB org_dcb;
    int org_dcb_has_been_read;

    COMMTIMEOUTS org_commtimeouts;        ///< Original timeout configuration when the port was opened.
    int org_commtimeouts_have_been_read;  ///< Flag indicating that @p org_commtimeouts has been read.

    COMMPROP comm_prop;                   ///< Configuration settings supported by the driver (r/o).
    int comm_prop_have_been_read;         ///< Flag indicating that @p comm_prop has been read.
  #endif

  #if defined( MBG_TGT_POSIX )
    struct termios org_tio;     ///< Original port settings when the port was opened.
    int org_tio_has_been_read;  ///< Flag indicating that @p org_tio has been read.
  #endif

} MBGSERIO_DEV;



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Return the base name of the device name of a serial port.
 *
 * In order to leave the original device name string unchanged,
 * a buffer is allocated with a duplicate of the original string,
 * and a pointer to the basename component in that buffer is returned.
 *
 * After usage, the allocated buffer whose address is saved in
 * @p *pp_dup_port_name should be freed by calling ::mbgserio_free_port_basename.
 *
 * @param[in]  pp_dup_port_name  Address of a <em>char *</em> that takes the address
 *                               of the allocated string buffer.
 * @param[in]  port_name         The original device name string.
 *
 * @return  A pointer to the basename which is part of an allocated copy of the original string.
 *
 * @see ::mbgserio_free_port_basename
 */
 _NO_MBG_API_ATTR char * _MBG_API mbgserio_get_port_basename( char **pp_dup_port_name, const char *port_name ) ;

 /**
 * @brief Free a buffer that has been allocated by ::mbgserio_get_port_basename.
 *
 * This function should be called whenever a string buffer allocated
 * by an earlier ::mbgserio_get_port_basename call isn't needed anymore.
 *
 * @param[in]  pp_dup_port_name  Address of a <em>char *</em> with the address
 *                               of the allocated string buffer.
 *
 * @see ::mbgserio_get_port_basename
 */
 _NO_MBG_API_ATTR void _MBG_API mbgserio_free_port_basename( char **pp_dup_port_name ) ;

 /**
 * @brief Determine the threshold of a serial RX FIFO buffer.
 *
 * This is useful to be able to exactly compensate the delay
 * of a received serial data string.
 *
 * @note This may not be supported on each target system.
 *
 * @param[in]  port_name  The device name of the serial port.
 *
 * @return  The number of bytes in the FIFO buffer after which
 *          a receive event (e.g. IRQ) is generated,
 *          or 0 if the real threshold can't be determined.
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_get_rx_fifo_threshold( const char *port_name ) ;

 /**
 * @brief Close a serial port specified by a ::MBGSERIO_DEV structure.
 *
 * After the port has been closed, the buffer that had been allocated
 * for the ::MBGSERIO_DEV structure is freed, and the pointer to that
 * buffer is set to @a NULL.
 *
 * @param[in,out]  pp_sdev  Address of a pointer to the control structure including the port handle.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_close( MBGSERIO_DEV **pp_sdev ) ;

 /**
 * @brief Open a serial port and set up a ::MBGSERIO_DEV structure.
 *
 * @param[in,out]  pp_sdev   Address of a pointer to a ::MBGSERIO_DEV device control structure
 *                           allocated and set up by this call. Set to @a NULL on error.
 * @param[in]      dev_name  Device name of the port to be opened.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_open( MBGSERIO_DEV **pp_sdev, const char *dev_name ) ;

 /**
 * @brief Set the transmission parameters of a serial port.
 *
 * @param[in,out]  sdev       Control structure of the device.
 * @param[in]      baud_rate  One of the well-known baud rates, e.g. 19200.
 * @param[in]      framing    A short string providing the framing, e.g. "8N1".
 *
 * @return One of the @ref MBG_RETURN_CODES.
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_set_parms( MBGSERIO_DEV *sdev, uint32_t baud_rate, const char *framing ) ;

 /**
 * @brief Read from a serial port.
 *
 * @param[in,out]  sdev    Control structure of the device.
 * @param[out]     buffer  Buffer to take the bytes read.
 * @param[in]      count   Size of @p buffer.
 *
 * @return On Success, the (positive) number of byte read,
 *         or one of the (negative) @ref MBG_ERROR_CODES on error.
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_read( MBGSERIO_DEV *sdev, void *buffer, unsigned int count ) ;

 /**
 * @brief Write to a serial port.
 *
 * @param[in,out]  sdev    Control structure of the device.
 * @param[in]      buffer  Buffer with the bytes to write.
 * @param[in]      count   Number of bytes in @p buffer to write.
 *
 * @return On Success, the (positive) number of byte written,
 *         or one of the (negative) @ref MBG_ERROR_CODES on error.
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_write( MBGSERIO_DEV *sdev, const void *buffer, unsigned int count ) ;

 /**
 * @brief Flush the transmit buffer of a serial port.
 *
 * @param[in,out]  sdev  Control structure of the device.
 */
 _NO_MBG_API_ATTR void _MBG_API mbgserio_flush_tx( MBGSERIO_DEV *sdev ) ;

 /**
 * @brief Get the current poll timeout count, in milliseconds.
 *
 * Used for timeout in ::mbgserio_read_wait.
 *
 * @param[in]  sdev  Control structure of the device.
 *
 * @return  The current timeout value, in milliseconds.
 *
 * @see ::mbgserio_set_dev_poll_timeout
 * @see ::mbgserio_read_wait
 */
 ulong mbgserio_get_dev_poll_timeout( const MBGSERIO_DEV *sdev ) ;

 /**
 * @brief Set a new poll timeout, in milliseconds.
 *
 * Used for timeout in ::mbgserio_read_wait.
 *
 * @param[in,out]  sdev         Control structure of the device.
 * @param[in]      new_timeout  New timeout, in milliseconds.
 *
 * @return  The previous timeout value, in milliseconds.
 *
 * @see ::mbgserio_get_dev_poll_timeout
 * @see ::mbgserio_read_wait
 */
 ulong mbgserio_set_dev_poll_timeout( MBGSERIO_DEV *sdev, ulong new_timeout ) ;

 /**
 * @brief Wait for data and read from a serial port.
 *
 * @param[in,out]  sdev    Control structure of the device.
 * @param[out]     buffer  Buffer to take the bytes read.
 * @param[in]      count   Size of @p buffer.
 *
 * @return On Success, the (positive) number of byte read,
 *         or one of the (negative) @ref MBG_ERROR_CODES on error.
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_read_wait( MBGSERIO_DEV *sdev, void *buffer, unsigned int count ) ;

 /**
 * @brief Compute the time required to transmit a number of bytes, in nanoseconds.
 *
 * Depends on the baud rate and the effective number of
 * bits per byte, i.e. data / parity / stop bits.
 * Assuming the transmission is without gaps.
 *
 * If 2 stobits are used, this differs from ::mbgserio_get_duration_rcv.
 * See ::mbgserio_get_duration_rcv.
 *
 * @param[in]  p_sp     The relevant parameters of the transmission.
 * @param[in]  n_bytes  The number of bytes to transmit.
 *
 * @return  The the computed duration, in nanoseconds.
 *
 * @see ::mbgserio_get_duration_rcv
 */
 _NO_MBG_API_ATTR int32_t _MBG_API mbgserio_get_duration_xmt( MBGSERIO_SPEED_PARMS *p_sp, int n_bytes ) ;

 /**
 * @brief Compute the receive delay for a number of bytes, in nanoseconds.
 *
 * Depends on the baud rate and the effective number of
 * bits per byte, i.e. data / parity / stop bits.
 * Assuming the transmission is without gaps.
 *
 * Differs from ::mbgserio_get_duration_xmt in case there are
 * 2 stop bits. This is because because UARTS usually flag
 * reception of a byte after the first stop bit, even if
 * 2 stop bits are used for transmission.
 *
 * @param[in]  p_sp     The relevant parameters of the transmission.
 * @param[in]  n_bytes  The number of bytes to receive.
 * @param[in]  n_bits   A number of additional bits to account for.
 *
 * @return  The the computed duration, in nanoseconds.
 *
 * @see ::mbgserio_get_duration_xmt
 */
 _NO_MBG_API_ATTR int32_t _MBG_API mbgserio_get_duration_rcv( const MBGSERIO_SPEED_PARMS *p_sp, int n_bytes, int n_bits ) ;

 /**
 * @brief Set up a string list with names of available serial ports.
 *
 * @param[out]  list      Address of the header of a list to be set up.
 * @param[in]   max_devs  Maximum number of list entries to create.
 *
 * @return  The number of entries in the list.
 *
 * @see ::mbgserio_free_str_list
 */
 _NO_MBG_API_ATTR int _MBG_API mbgserio_setup_port_str_list( MBG_STR_LIST **list, int max_devs ) ;

 /**
 * @brief Free a string list that has been allocated before.
 *
 * @param[out]  list  Address of the string list to be freed.
 *
 * @see ::mbgserio_setup_port_str_list
 */
 _NO_MBG_API_ATTR void _MBG_API mbgserio_free_str_list( MBG_STR_LIST *list ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif

/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBGSERIO_H */
