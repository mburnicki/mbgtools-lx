
/**************************************************************************
 *
 *  $Id: irig_cal.h 1.5 2021/03/22 17:57:48 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and inline functions to calibrate IRIG inputs.
 *
 * -----------------------------------------------------------------------
 *  $Log: irig_cal.h $
 *  Revision 1.5  2021/03/22 17:57:48  martin
 *  Updated a bunch of comments.
 *  Revision 1.4  2018/03/21 14:58:06  martin
 *  Fixed a compiler warning.
 *  Revision 1.3  2015/08/31 09:35:30Z  martin
 *  Quieted a compiler warning.
 *  Revision 1.2  2012/01/17 09:58:36Z  martin
 *  Added new function mbg_icode_rx_to_group_idx().
 *  Do a pausibility check in mbg_get_cal_rec_irig_rx_num_rec().
 *  Revision 1.1  2011/11/23 17:50:02  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _IRIG_CAL_H
#define _IRIG_CAL_H


/* Other headers to be included */

#include <mbgdevio.h>
#include <use_pack.h>


#ifdef _IRIG_CAL
 #define _ext
#else
 #define _ext extern
#endif


/* Start of header body */

#if defined( _USE_PACK )   // Set byte alignment.
  #pragma pack( 1 )
  #define _USING_BYTE_ALIGNMENT
#endif


/**
 * @brief Get the IRIG calibration group index for a given IRIG RX code.
 *
 * There are calibration data records available for different groups of IRIG RX
 * codes with similar characteristics, e.g. DCLS or modulated, etc. This function
 * tries to determine the group index for a given IRIG RX code.
 *
 * @param[in]  icode_rx  A given IRIG RX code index.
 *
 * @return  The group index [0..N_IRIG_RX_COMP-1], or -1 if the group could not be determined.
 */
static __mbg_inline
int mbg_icode_rx_to_group_idx( int icode_rx )
{
  ulong icode_mask = 1UL << icode_rx;

  if ( icode_mask & MSK_ICODE_RX_DC )
  {
    // For DCLS codes check the bit rate.

    if ( icode_mask & MSK_ICODE_RX_100BPS )
      return IRIG_RX_COMP_B0;

    if ( icode_mask & MSK_ICODE_RX_1000BPS )
      return IRIG_RX_COMP_A0;

    if ( icode_mask & MSK_ICODE_RX_10000BPS )
      return IRIG_RX_COMP_G0;

    return -1;  // Unknown DLCS code.
  }


  // For modulated codes check the carrier frequency.

  if ( icode_mask & MSK_ICODE_RX_1KHZ )
    return IRIG_RX_COMP_B1;

  if ( icode_mask & MSK_ICODE_RX_10KHZ )
    return IRIG_RX_COMP_A1;

  if ( icode_mask & MSK_ICODE_RX_100KHZ )
    return IRIG_RX_COMP_G1;

  return -1;  // Unknown modulated code.

}  // mbg_icode_rx_to_group_idx



/**
 * @brief Get generic I/O support info for a certain data type.
 *
 * @param[in]  dh    Valid handle to a Meinberg device.
 * @param[in]  type  One of the enumerated generic I/O data types.
 * @param[out] p     Pointer to a ::GEN_IO_INFO variable to be filled up.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_cal_rec_irig_rx_num_rec
 * @see ::mbg_get_cal_rec_irig_rx_comp
 * @see ::mbg_set_cal_rec_irig_rx_comp
 */
static __mbg_inline
int mbg_get_gen_io_info( MBG_DEV_HANDLE dh, GEN_IO_INFO_TYPE type, GEN_IO_INFO *p )
{
  int rc;

  _mbg_swab_gen_io_info_type( &type );

  rc = mbg_generic_io( dh, PCPS_GEN_IO_GET_INFO, &type, sizeof( type ), p, sizeof( *p ) );

  if ( rc == MBG_SUCCESS )
    _mbg_swab_gen_io_info( p );

  return rc;

}  // mbg_get_gen_io_info



/**
 * @brief Get supported number of IRIG RX calibration records.
 *
 * @param[in]  dh         Valid handle to a Meinberg device.
 * @param[out] p_num_rec  Pointer to an @a int to be filled up.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_gen_io_info
 * @see ::mbg_get_cal_rec_irig_rx_comp
 * @see ::mbg_set_cal_rec_irig_rx_comp
 */
static __mbg_inline
int mbg_get_cal_rec_irig_rx_num_rec( MBG_DEV_HANDLE dh, int *p_num_rec )
{
  GEN_IO_INFO tmp = { 0 };
  int rc = mbg_get_gen_io_info( dh, PCPS_GEN_IO_CAL_REC_IRIG_RX_COMP, &tmp );

  if ( rc == MBG_SUCCESS )
  {
    _mbg_swab_gen_io_info( &tmp );
    *p_num_rec = ( tmp.type == PCPS_GEN_IO_CAL_REC_IRIG_RX_COMP ) ? tmp.num : 0;
  }

  return rc;

}  // mbg_get_cal_rec_irig_rx_num_rec



/**
 * @brief Retrieve IRIG RX calibration record with specific index.
 *
 * @param[in]  dh   Valid handle to a Meinberg device.
 * @param[in]  idx  Index of the calibration record to be retrieved.
 * @param[out] p    Pointer to a ::CAL_REC_IRIG_RX_COMP variable to be filled up.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_gen_io_info
 * @see ::mbg_get_cal_rec_irig_rx_num_rec
 * @see ::mbg_set_cal_rec_irig_rx_comp
 */
static __mbg_inline
int mbg_get_cal_rec_irig_rx_comp( MBG_DEV_HANDLE dh, uint16_t idx, CAL_REC_IRIG_RX_COMP *p )
{
  CAL_REC_HDR hdr;
  int rc;

  hdr.type = CAL_REC_TYPE_IRIG_RX_COMP;
  hdr.idx = idx;

  _mbg_swab_cal_rec_hdr( &hdr );

  rc =  mbg_generic_io( dh, PCPS_GEN_IO_CAL_REC_IRIG_RX_COMP,
                        &hdr, sizeof( hdr ), p, sizeof( *p ) );

  if ( rc == MBG_SUCCESS )
    _mbg_swab_cal_rec_irig_rx_comp( p );

  return rc;

}  // mbg_get_cal_rec_irig_rx_comp



/**
 * @brief Write an IRIG RX calibration record to a device.
 *
 * @param[in]  dh  Valid handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::CAL_REC_IRIG_RX_COMP variable to be written.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_gen_io_info
 * @see ::mbg_get_cal_rec_irig_rx_num_rec
 * @see ::mbg_get_cal_rec_irig_rx_comp
 */
static __mbg_inline
int mbg_set_cal_rec_irig_rx_comp( MBG_DEV_HANDLE dh, const CAL_REC_IRIG_RX_COMP *p )
{
  int rc;

  #if defined( MBG_ARCH_BIG_ENDIAN )
    CAL_REC_IRIG_RX_COMP tmp = *p;
    _mbg_swab_cal_rec_irig_rx_comp( &tmp );
    p = &tmp;
  #endif

  rc = mbg_generic_io( dh, PCPS_GEN_IO_CAL_REC_IRIG_RX_COMP,
                       p, sizeof( *p ), NULL, 0 );

  return rc;

}  // mbg_set_cal_rec_irig_rx_comp



#ifdef __cplusplus
extern "C" {
#endif

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

/* (no header definitions found) */

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif

#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext

#endif  /* _IRIG_CAL_H */

