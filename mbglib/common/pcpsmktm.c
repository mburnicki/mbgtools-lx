
/**************************************************************************
 *
 *  $Id: pcpsmktm.c 1.6 2019/09/27 12:17:19 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Function to convert PCPS_TIME to Unix time (seconds since 1970)
 *
 * -----------------------------------------------------------------------
 *  $Log: pcpsmktm.c $
 *  Revision 1.6  2019/09/27 12:17:19  martin
 *  Changed pcps_mktime() to pcps_mktime64() with calling convention
 *  similar to mbg_mktime64().
 *  Revision 1.5  2017/07/05 08:05:11  martin
 *  Let pcps_mktime() return a 'time_t' instead of 'long', and made
 *  the PCPS_TIME pointer parameter 'const'.
 *  Added doxygen comments.
 *  Revision 1.4  2006/12/14 15:27:49  martin
 *  Include time.h.
 *  Revision 1.3  2006/08/22 09:10:03  martin
 *  Renamed function totalsec() to mbg_mktime() and moved it
 *  to a separate file mbgmktm.c.
 *  Revision 1.2  2001/08/14 11:58:08  MARTIN
 *  Included sys/time.h for time_t definition.
 *  Revision 1.1  2001/02/02 15:30:09  MARTIN
 *
 **************************************************************************/

#define _PCPSMKTM
 #include <pcpsmktm.h>
#undef _PCPSMKTM

#include <mbgmktm.h>


/*HDR*/
/**
 * @brief Compute a linear ::MBG_TIME64_T value from ::PCPS_TIME.
 *
 * This function basically works like the POSIX @a mktime function but
 * expects the date and time to be converted in ::PCPS_TIME format.
 *
 * Unlike POSIX @a mktime, which applies a local time offset specified
 * as environment variable that is read by the runtime library, this
 * function applies the %UTC offset from ::PCPS_TIME::offs_utc when
 * converting local time to %UTC.
 *
 * Unlike @a mktime, this function expects the address of a variable
 * of an ::MBG_TIME64_T to take the computed timestamp, and returns
 * a result code indicating if the conversion was successful, or not.
 * So @a -1 can be a valid timestamp, refering to the time
 * one second before the epoch.
 *
 * @note Since ::PCPS_TIME::year actually just contains the year
 * of the century, the year number needs to be extended before a
 * timestamp can be calculated.
 *
 * @param[out] p_t64  Address of a variable to take the result on success,
 *                    i.e. seconds since 1970-01-01 (POSIX @a time_t format,
 *                    but always 64 bits).
 *
 * @param[in]  p_tm   Pointer to a ::PCPS_TIME type providing the date
 *                    and time to be converted.
 *
 * @param[in]  year_lim  The year limit used when converting a 2 digit year number
 *                       to a full 4 digit year number. See ::mbg_exp_year.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int pcps_mktime64( MBG_TIME64_T *p_t64, const PCPS_TIME *p_tm, int year_lim )
{
  MBG_TIME64_T t64 = 0;

  int year = mbg_exp_year( p_tm->year, year_lim );  // Expand the 2 digit year number.

  int rc = mbg_mktime64( &t64, year - 1900, p_tm->month - 1, p_tm->mday,
                         p_tm->hour, p_tm->min, p_tm->sec );

  // PCPS_TIME::offs_utc contains a local time, so we
  // still have to compensate the local time offset
  // to yield %UTC.
  if ( mbg_rc_is_success( rc ) )
    t64 -= p_tm->offs_utc * SECS_PER_HOUR;

  *p_t64 = t64;

  return rc;

}  // pcps_mktime64



