
/**************************************************************************
 *
 *  $Id: mbgioctl.c 1.10 2021/09/13 13:39:09 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Utility functions to be used with IOCTLs.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgioctl.c $
 *  Revision 1.10  2021/09/13 13:39:09  martin
 *  Removed obsolete function mbgioctl_rc_to_mbg_errno().
 *  Revision 1.9  2021/03/16 12:20:53Z  martin
 *  Updated some comments.
 *  Revision 1.8  2021/03/12 12:32:53  martin
 *  Updated some comments.
 *  Revision 1.7  2020/06/18 13:09:07  martin
 *  Always build with mbgioctl_get_name().
 *  Revision 1.6  2019/04/03 12:59:43  martin
 *  Fixed preprocessor conditionals using unknown symbols.
 *  Revision 1.5  2018/11/22 14:57:43  martin
 *  Check new preprocessor symbol MBG_TGT_USE_IOCTL to exclude
 *  code from build on targets that don't use IOCTL calls.
 *  Revision 1.4  2018/11/05 13:33:26  martin
 *  Avoid signed/unsigned mismatch by using appropriate function parameter.
 *  Revision 1.3  2018/07/05 08:57:05Z  martin
 *  New function mbgioctl_rc_to_mbg_errno().
 *  New function mbgioctl_get_name().
 *  Updated file header comment.
 *  Revision 1.2  2003/03/18 09:30:05  martin
 *  Changed names from ioctlw32 to mbgioctl.
 *  Revision 1.1  2002/02/19 13:51:08Z  MARTIN
 *  Initial revision
 *
 **************************************************************************/

#include <mbg_tgt.h>

// The code below is only required for target systems
// where IOCTLs are used to access a kernel driver, so
// we exclude it from build on systems where the physical
// devices are accessed directly from user space.

#if MBG_TGT_USE_IOCTL

#define _MBGIOCTL
 #include <mbgioctl.h>
#undef _MBGIOCTL

#include <mbgerror.h>


/*HDR*/
const char *mbgioctl_get_name( long code )
{
  static const MBG_CODE_NAME_TABLE_ENTRY tbl[] = IOCTL_CODES_TABLE;

  const MBG_CODE_NAME_TABLE_ENTRY *p = tbl;

  for ( p = tbl; p->name; p++ )
  {
    if ( p->code == code )
      return p->name;
  }

  return "UNKNOWN_IOCTL";

}  // mbgioctl_get_name


#endif  // MBG_TGT_USE_IOCTL

