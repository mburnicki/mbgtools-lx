
/**************************************************************************
 *
 *  $Id: mbgdevio.h 1.67 2023/12/05 20:21:36 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes used with Meinberg device driver I/O.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgdevio.h $
 *  Revision 1.67  2023/12/05 20:21:36  martin.burnicki
 *  New functions mbg_set_and_check_gps_ant_cable_len
 *  and mbg_set_and_check_tr_distance.
 *  Revision 1.66  2023/02/23 10:52:32  martin.burnicki
 *  Updated a doxygen comment.
 *  Revision 1.65  2022/12/21 15:26:48  martin.burnicki
 *  Quieted a potential compiler warning.
 *  Revision 1.64  2022/08/23 16:20:24  martin.burnicki
 *  Updated function prototypes.
 *  Revision 1.63  2022/06/02 08:20:23  martin.burnicki
 *  Updated version code to 3.14.
 *  Revision 1.62  2022/05/31 14:00:10Z  martin.burnicki
 *  Added non-automatic prototypes for SYN1588 mutex functions on Windows.
 *  Revision 1.61  2022/04/27 15:04:20  martin.burnicki
 *  New inline functions mbg_create_dev_name() and
 *  mbg_create_dev_name_from_dev_info().
 *  Updated function prototypes.
 *  Revision 1.60  2022/02/10 13:56:19  martin.burnicki
 *  Fixed some doxygen comments.
 *  Revision 1.59  2021/11/08 21:31:51  martin.burnicki
 *  Updated function prototypes.
 *  Revision 1.58  2021/11/08 18:00:06  martin.burnicki
 *  Changed MBG_DEV_FN to a structure.
 *  Revision 1.57  2021/10/05 09:11:45  martin
 *  Account for new definitions _MBG_API_ATTR_EXPORT and _MBG_API_ATTR_IMPORT.
 *  Revision 1.56  2021/05/04 21:12:16  martin
 *  Updated function prototypes.
 *  Revision 1.55  2021/04/29 15:05:54  martin
 *  Support reading device CPU info.
 *  Revision 1.54  2021/04/29 14:46:11  martin
 *  Moved some inline functions to mbgutil.h.
 *  Revision 1.53  2021/04/29 14:01:33  martin
 *  Renamed global variable pc_cycles_frequency to mbg_pc_cycles_frequency.
 *  Use attribute _MBG_API_ATTR with all declarations of global variables.
 *  Revision 1.52  2021/04/13 15:00:22  martin
 *  Moved some SYN1588-specific definitions elsewhere.
 *  Revision 1.51  2021/03/22 18:09:27  martin
 *  Updated a bunch of comments.
 *  Revision 1.50  2021/03/12 14:18:07  martin
 *  Made variable pc_cycles_frequency global.
 *  mbg_init_pc_cycles_frequency() now provides a return code.
 *  Updated a bunch of comments.
 *  Revision 1.49  2020/10/20 10:37:09  martin
 *  Added some definitions to conditionally support SYN1588.
 *  Revision 1.48  2020/02/21 15:39:46  martin
 *  Updated function prototypes.
 *  Revision 1.47  2018/11/22 14:27:05  martin
 *  Fixed pointer size problems in a mixed 32/64 bit Linux environment.
 *  Removed obolete preprocessor symbol MBG_USE_KERNEL_DRIVER
 *  and use new global symbol MBG_TGT_USE_IOCTL instead.
 *  Revision 1.46  2018/11/14 12:24:40  martin
 *  Renamed a preprocessor symbol to avoid a name clash.
 *  Revision 1.45  2018/09/21 08:59:17  martin
 *  New version code 0x0308, compatibility version still 0x0210.
 *  Account for a renamed library function.
 *  Updated function prototypes.
 *  Revision 1.44  2018/08/07 15:09:25Z  martin
 *  New version code 0x0370, compatibility version still 0x0210.
 *  Revision 1.43  2018/07/05 08:50:48  martin
 *  Added missing linkage attribute for mbg_open_device().
 *  Reworked handling of IOCTL return codes.
 *  Modified test if cpu_set_t and associated definitions are available.
 *  Use standard errno conversion functions.
 *  Removed usage of obsolete symbol MBGDEVIO_SIMPLE.
 *  Updated function prototypes.
 *  Revision 1.42  2017/07/05 15:20:32  martin
 *  New MBGDEVIO_VERSION 0x0400, compatibility version unchanged.
 *  Account for MBG_ERROR_CODES returned by the ioctl functions.
 *  New type MBG_DEV_FN used for device file names, formerly often
 *  referred to as 'hardware ID'.
 *  New type MBG_DEV_NAME used for unique device names, composed of
 *  device type name and serial number appended after an underscore.
 *  Renamed and reworked list handling for device lists.
 *  Use functions from new module timeutil.
 *  windows.h is now included in mbg_tgt.h.
 *  Check for MBG_TGT_POSIX instead of MBG_TGT_UNIX.
 *  Account for PCPS_HRT_BIN_FRAC_SCALE renamed to MBG_FRAC32_UNITS_PER_SEC.
 *  Fixed macro definition syntax to avoid clang compiler warnings.
 *  Exclude mbg_chk_tstamp64_leap_sec() for BC builds for now.
 *  Lots of new doxygen comments and groups.
 *  Updated function prototypes.
 *  Revision 1.41  2013/09/26 08:54:54  martin
 *  Defined thread function type MBG_THREAD_FNC.
 *  Defined check-if-supported function type MBG_CHK_SUPP_FNC.
 *  Updated function prototypes.
 *  Revision 1.40  2012/10/02 18:40:30Z  martin
 *  There are some g++ versions which fail to compile source code using
 *  the macros provided by Linux to define IOCTL codes. If only the API
 *  functions are called by an application then the IOCTL codes aren't
 *  required anyway, so we just avoid inclusion of mbgioctl.h.
 *  Updated doxygen comments.
 *  Updated function prototypes.
 *  Support on-board event logs.
 *  Fixed a bug which caused a crash when generic I/O calls
 *  were used on Windows.
 *  Changes for QNX.
 *  Workaround to make mbgmon (BC) build on Windows.
 *  Cleaned up CPU set support on Linux.
 *  Moved some macros here so they can be used by other modules.
 *  Support reading CORR_INFO, and reading/writing TR_DISTANCE.
 *  Cleaned up handling of pragma pack().
 *  Cleaned up inclusion of header files.
 *  Moved mutex definitions to new mbgmutex.h.
 *  Renamed mutex stuff to critical sections.
 *  Updated function prototypes to support PTP unicast configuration
 *  Made xhrt leap second check an inline function.
 *  Fixed macro to avoid compiler warning.
 *  Revision 1.39  2009/12/15 15:34:59Z  daniel
 *  Support reading the raw IRIG data bits for firmware versions
 *  which support this feature.
 *  Revision 1.38.1.2  2009/12/10 09:58:53Z  daniel
 *  Revision 1.38.1.1  2009/12/10 09:45:29Z  daniel
 *  Revision 1.38  2009/09/29 15:06:26Z  martin
 *  Updated function prototypes.
 *  Revision 1.37  2009/08/12 14:31:51  daniel
 *  New version code 306, compatibility version still 210.
 *  Revision 1.36  2009/06/19 12:20:31Z  martin
 *  Updated function prototypes.
 *  Revision 1.35  2009/06/09 08:57:09  daniel
 *  New version code 305, compatibility version still 210.
 *  Revision 1.34  2009/06/08 18:20:14Z  daniel
 *  Updated function prototypes.
 *  Fixes for ARM target.
 *  Revision 1.33  2009/03/19 15:36:26  martin
 *  New version code 304, compatibility version still 210.
 *  Moved some inline functions dealing with MBG_PC_CYCLES
 *  from mbgdevio.h to pcpsdev.h.
 *  Include mbg_arch.h here.
 *  Removed unused doxygen comment.
 *  Updated function prototypes.
 *  Revision 1.32  2008/12/17 10:43:30Z  martin
 *  New version code 303, compatibility version still 210.
 *  Increased MBG_MAX_DEVICES from 5 to 8.
 *  Added macros to read the time stamp counter (cycles), and
 *  added an inline rdtscll() call for user space Linux.
 *  Added some inline functions to deal with cycles and timestamps.
 *  Generic support for threads and process/thread affinity controlled
 *  by symbol MBGDEVIO_USE_THREAD_API.
 *  New preprocessor symbol MBGDEVIO_HAVE_THREAD_AFFINITY.
 *  Support extrapolated time stamps controlled
 *  by symbol MBGDEVIO_XHRT_API.
 *  Removed definition of MBG_TGT_SUPP_MMAP.
 *  Updated function prototypes and doxygen comments.
 *  Revision 1.31  2008/02/26 16:57:38Z  martin
 *  Updated function prototypes and doxygen comments.
 *  Revision 1.30  2008/02/04 13:33:15Z  martin
 *  New preprocessor symbol MBG_TGT_SUPP_MMAP.
 *  Revision 1.29  2008/01/31 08:55:39Z  daniel
 *  Renamed functions related to mapped memory support
 *  Revision 1.28  2008/01/31 08:36:22Z  martin
 *  Picked up changes from 1.24.1.1:
 *  Added default preprocessor symbol MBGDEVIO_SIMPLE.
 *  Revision 1.27  2008/01/17 15:56:37Z  daniel
 *  New version code 302, compatibility version still 210.
 *  Added structure MBG_MAPPED_MEM_INFO.
 *  Updated function prototypes.
 *  Revision 1.26  2007/10/16 10:11:42Z  daniel
 *  New version code 301, compatibility version still 210.
 *  Revision 1.25  2007/09/26 14:10:34Z  martin
 *  New version code 300, compatibility version still 210.
 *  Added MBG_MAX_DEVICES.
 *  Added enum SELECTION_MODE.
 *  Added structures MBG_DEVICE_LIST and MBG_DEVICE_NAME_LIST.
 *  Updated function prototypes.
 *  Revision 1.24  2007/03/22 10:14:16Z  martin
 *  New version code 219, compatibility version still 210.
 *  Revision 1.23  2007/03/02 10:18:10Z  martin
 *  Updated function prototypes due to renamed data structures.
 *  Use new definitions of generic handle types.
 *  Preliminary support for *BSD.
 *  Revision 1.22  2006/08/09 13:47:29  martin
 *  New version code 218, compatibility version still 210.
 *  Revision 1.21  2006/06/08 12:30:22Z  martin
 *  New version code 217, compatibility version still 210.
 *  Revision 1.20  2006/05/02 13:14:27Z  martin
 *  New version code 216, compatibility version still 210.
 *  Updated function prototypes.
 *  Revision 1.19  2006/01/11 12:14:53Z  martin
 *  New version code 215, compatibility version still 210.
 *  Revision 1.18  2005/12/15 09:38:39Z  martin
 *  New version 214, compatibility version still 210.
 *  Revision 1.17  2005/06/02 11:54:40Z  martin
 *  Updated function prototypes.
 *  Revision 1.16  2005/02/16 15:13:00Z  martin
 *  New MBGDEVIO_VERSION 0x0212.
 *  Updated function prototypes.
 *  Revision 1.15  2005/01/14 10:22:44Z  martin
 *  Updated function prototypes.
 *  Revision 1.14  2004/12/09 11:24:00Z  martin
 *  Support configuration of on-board frequency synthesizer.
 *  Revision 1.13  2004/11/09 14:13:00Z  martin
 *  Updated function prototypes.
 *  Revision 1.12  2004/08/17 11:13:46Z  martin
 *  Account for renamed symbols.
 *  Revision 1.11  2004/04/14 09:34:23Z  martin
 *  New definition MBGDEVIO_COMPAT_VERSION.
 *  Pack structures 1 byte aligned.
 *  Revision 1.10  2003/12/22 15:35:10Z  martin
 *  New revision 2.03.
 *  Moved some definitions to pcpsdev.h.
 *  New structures to read device time together with associated
 *  PC high resolution timer cycles.
 *  Updated function prototypes.
 *  Revision 1.9  2003/06/19 08:50:05Z  martin
 *  Definition of MBGDEVIO_VERSION number to allow DLL
 *  API version checking.
 *  Replaced some defines by typedefs.
 *  Renamed USE_DOS_TSR to MBG_USE_DOS_TSR.
 *  New preprocessor symbol MBG_USE_KERNEL_DRIVER which
 *  is defined only for targets which use IOCTLs.
 *  Don't include pcps_dos.h here.
 *  Updated function prototypes.
 *  Revision 1.8  2003/05/16 08:44:26  MARTIN
 *  Cleaned up inclusion of headers.
 *  Removed obsolete definitions.
 *  Changes for direct access targets.
 *  Revision 1.7  2003/04/25 10:16:00  martin
 *  Updated inclusion of headers.
 *  Made prototypes available for all targets.
 *  Revision 1.6  2003/04/15 19:38:05Z  martin
 *  Updated function prototypes.
 *  Revision 1.5  2003/04/09 13:44:53Z  martin
 *  Use new common IOCTL codes from mbgioctl.h.
 *  Updated function prototypes.
 *  Revision 1.4  2002/09/06 11:06:35Z  martin
 *  Updated function prototypes for Win32 API..
 *  Win32 compatibility macros to use old APIs with new functions.
 *  Support targets OS/2 and NetWare.
 *  Revision 1.3  2002/02/28 10:08:54Z  MARTIN
 *  Syntax cleanup for Win32.
 *  Revision 1.2  2002/02/26 14:40:47  MARTIN
 *  Source code cleanup.
 *  Changes for DOS with and without TSR.
 *  Revision 1.1  2002/02/19 13:48:21  MARTIN
 *  Initial revision
 *
 **************************************************************************/

#ifndef _MBGDEVIO_H
#define _MBGDEVIO_H


/* Other headers to be included */

#include <mbg_tgt.h>
#include <mbg_arch.h>
#include <cfg_hlp.h>
#include <mbgutil.h>
#include <timeutil.h>
#include <mbgmutex.h>
#include <mbgerror.h>
#include <mbggeo.h>
#include <pcpsdev.h>
#include <pci_asic.h>
#include <use_pack.h>

#include <time.h>
#include <stdio.h>
#include <errno.h>


#define MBGDEVIO_VERSION         0x0314

#define MBGDEVIO_COMPAT_VERSION  0x0210


#if !defined( SUPP_SYN1588_USR_SPC )
  #define SUPP_SYN1588_USR_SPC        0
#endif


#if defined( MBG_TGT_WIN32 )

  #if !defined( MBGDEVIO_XHRT_API )
    #define MBGDEVIO_XHRT_API  1
  #endif

  #if !defined( MBGDEVIO_USE_THREAD_API )
    #define MBGDEVIO_USE_THREAD_API  1
  #endif

  #if !defined( MBGDEVIO_HAVE_THREAD_AFFINITY )
    #define MBGDEVIO_HAVE_THREAD_AFFINITY  1
  #endif

  #define MBGDEVIO_RET_VAL  DWORD

#elif defined( MBG_TGT_LINUX )

  #if !defined( MBGDEVIO_XHRT_API )
    #define MBGDEVIO_XHRT_API  1
  #endif

  // Thread support on Linux depends strongly on
  // the versions of some libraries, so the symbols
  // MBGDEVIO_USE_THREAD_API and MBGDEVIO_HAVE_THREAD_AFFINITY
  // should be set in the project's Makefile, depending on the
  // target envionment. Otherwise thread support is disabled
  // as per default.

  #include <sys/ioctl.h>
  #include <fcntl.h>
  #include <sched.h>

  #if MBGDEVIO_USE_THREAD_API
    #include <pthread.h>
  #endif

#elif defined( MBG_TGT_BSD )

  #include <sys/ioctl.h>
  #include <fcntl.h>

#elif defined( MBG_TGT_OS2 )

#elif defined( MBG_TGT_QNX_NTO )

  #include <sys/types.h>
  #include <sys/stat.h>
  #include <fcntl.h>

  #include <pcpsdrvr.h>

#elif defined( MBG_TGT_DOS )

  #if !defined( MBG_USE_DOS_TSR )
    #define MBG_USE_DOS_TSR  1
  #endif

  #include <pcpsdrvr.h>

#else // Other target OSs which access the hardware directly.

  #include <pcpsdrvr.h>

#endif


#if MBG_TGT_USE_IOCTL

  #include <mbgioctl.h>

  #include <stdlib.h>
  #include <string.h>

#endif


#if !defined( MBGDEVIO_XHRT_API )
  #define MBGDEVIO_XHRT_API  0
#endif

#if !defined( MBGDEVIO_USE_THREAD_API )
  #define MBGDEVIO_USE_THREAD_API  0
#endif

#if !defined( MBGDEVIO_HAVE_THREAD_AFFINITY )
  #define MBGDEVIO_HAVE_THREAD_AFFINITY  0
#endif

#if ( 0 && defined( DEBUG ) )
  #define DEBUG_IOCTL_CALLS  1
#else
  #define DEBUG_IOCTL_CALLS  0
#endif


#ifdef _MBGDEVIO
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif

#if defined( MBGDEVIO_STATIC_LIB )
  // We don't build/use a DLL.
  #define _MBG_API_ATTR_VAR
#else
  // We do build/use a DLL.
  #ifdef _MBGDEVIO
    #define _MBG_API_ATTR_VAR _MBG_API_ATTR_EXPORT
  #else
    #define _MBG_API_ATTR_VAR _MBG_API_ATTR_IMPORT
  #endif
#endif


/* Start of header body */

#if defined( _USE_PACK )
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif


#ifdef __cplusplus
extern "C" {
#endif

#if MBG_TGT_USE_IOCTL   // TODO Should we use a different symbol to control this?

  typedef MBG_HANDLE MBG_DEV_HANDLE;

  #define MBG_INVALID_DEV_HANDLE  MBG_INVALID_HANDLE

#else // Other target OSs which access the hardware directly.

  typedef PCPS_DDEV *MBG_DEV_HANDLE;

  #define MBG_INVALID_DEV_HANDLE  NULL

#endif


#if !defined( MBGDEVIO_RET_VAL )
  #define MBGDEVIO_RET_VAL   int
#endif


#if !defined( _mbgdevio_cnv_ret_val )
  #define _mbgdevio_cnv_ret_val( _v )  (_v)
#endif


/**
 * @brief A structure containing a string that can take a device file name.
 *
 * The format of the device file name depends on the type of
 * operating system.
 *
 * On Linux and similar systems this is something like
 * a regular file name with an index number appended,
 * e.g. "/dev/mbgclock0". See ::MBGCLOCK_DEV_FN_BASE.
 *
 * On Windows this is a complex string composed of GUIDs,
 * bus-specific vendor and device ID numbers, etc., which can't
 * easily be handled manually.
 *
 * The string has been encapsulated in a structure to avoid
 * confusion when using arrays of device name strings.
 */
typedef struct
{
  char s[260];  // Conforming to MAX_PATH on Windows.
} MBG_DEV_FN;


#if MBG_TGT_HAS_DEV_FN_BASE

#define MBGCLOCK_DEV_FN_BASE  "/dev/mbgclock"
#define MBGCLOCK_DEV_FN_FMT   MBGCLOCK_DEV_FN_BASE "%i"

_ext _MBG_API_ATTR_VAR const char mbg_dev_fn_base[]
#ifdef _DO_INIT
  = MBGCLOCK_DEV_FN_BASE
#endif
;

_ext _MBG_API_ATTR_VAR const char mbg_dev_fn_fmt[]
#ifdef _DO_INIT
  = MBGCLOCK_DEV_FN_FMT
#endif
;

#endif  // MBG_TGT_HAS_DEV_FN_BASE



/**
 * @brief Clock frequency of the PC's cycles counter, in [Hz].
 *
 * ::mbg_init_pc_cycles_frequency can be called to determine
 * the frequency and set up this variable.
 *
 * If the cycles frequency value is 0, the target platform
 * may not support this, or the value couldn't be determined.
 * In this case it's not possible to convert a number of cycles
 * to an associated time interval.
 */
_ext _MBG_API_ATTR_VAR MBG_PC_CYCLES_FREQUENCY mbg_pc_cycles_frequency;



// Definitions specific to the target OS.

#if defined( MBG_TGT_LINUX )

  #define MBG_PROCESS_ID              pid_t
  #define _mbg_get_current_process()  0

  // __cpu_set_t_defined used to be defined by GNU's glibc.
  // However, at least with glibc-2.26 this doesn't seem to be
  // the case anymore, so we test alternatively if CPU_SETSIZE
  // is defined.
  #if defined( __cpu_set_t_defined ) || defined( CPU_SETSIZE )
    #define MBG_CPU_SET               cpu_set_t
    #define MBG_CPU_SET_SIZE          CPU_SETSIZE
    #define _mbg_cpu_clear( _ps )     CPU_ZERO( (_ps) )
    #define _mbg_cpu_set( _i, _ps )   CPU_SET( (_i), (_ps) )
    #define _mbg_cpu_isset( _i, _ps ) CPU_ISSET( (_i), (_ps) )
  #endif

  #if MBGDEVIO_USE_THREAD_API
    #define MBG_THREAD_ID             pthread_t
    #define _mbg_get_current_thread() 0
    #define MBG_THREAD_FNC_ATTR       // empty
    #define MBG_THREAD_FNC_RET_VAL    void *
    #define _mbg_thread_exit( _v )    return (void *) (_v)
  #endif

#elif defined( MBG_TGT_WIN32 )

  #define MBG_PROCESS_ID              HANDLE
  #define _mbg_get_current_process()  GetCurrentProcess()

  #define MBG_CPU_SET                 DWORD_PTR  // Attention: this is not used as pointer!
  #define MBG_CPU_SET_SIZE            ( sizeof( MBG_CPU_SET ) * 8 )

  #define MBG_THREAD_ID               HANDLE
  #define _mbg_get_current_thread()   GetCurrentThread()
  #define MBG_THREAD_FNC_ATTR         WINAPI
  #define MBG_THREAD_FNC_RET_VAL      DWORD
  #define _mbg_thread_exit( _v )      ExitThread( _v ); return (_v)

#endif  // target specific


#if !defined( MBG_TGT_WIN32 )
  #define FILETIME    int  // Just a dummy to avoid build errors.
#endif

#if !defined( MBG_PROCESS_ID )
  #define MBG_PROCESS_ID              int
#endif

#if !defined( _mbg_get_current_process )
  #define _mbg_get_current_process()  0
#endif

#if !defined( MBG_CPU_SET )
  #define MBG_CPU_SET                 int
#endif

#if !defined( MBG_CPU_SET_SIZE )
  #define MBG_CPU_SET_SIZE            ( sizeof( MBG_CPU_SET ) * 8 )
#endif

#if !defined( _mbg_cpu_clear )
  #define _mbg_cpu_clear( _ps )       ( *(_ps) = 0 )
#endif

#if !defined( _mbg_cpu_set )
  #define _mbg_cpu_set( _i, _ps )     ( *(_ps) |= ( (MBG_CPU_SET) 1UL << (_i) ) )
#endif

#if !defined( _mbg_cpu_isset )
  #define _mbg_cpu_isset( _i, _ps )   ( *(_ps) & ( (MBG_CPU_SET) 1UL << (_i) ) )
#endif


#if !defined( MBG_THREAD_ID )
  #define MBG_THREAD_ID               int
#endif

#if !defined( _mbg_get_current_thread )
  #define _mbg_get_current_thread()   0
#endif

#if !defined( MBG_THREAD_FNC_ATTR )
  #define MBG_THREAD_FNC_ATTR         // empty
#endif

#if !defined( MBG_THREAD_FNC_RET_VAL )
  #define MBG_THREAD_FNC_RET_VAL      void
#endif

#if !defined( _mbg_thread_exit )
  #define _mbg_thread_exit( _v )      _nop_macro_fnc()
#endif



/**
 * @brief A generic thread info structure.
 */
typedef struct
{
  MBG_THREAD_ID thread_id;
  #if defined( MBG_TGT_WIN32 )
    HANDLE exit_request;
  #endif

} MBG_THREAD_INFO;



/**
 * @brief A generic type of a thread function.
 */
typedef MBG_THREAD_FNC_RET_VAL MBG_THREAD_FNC_ATTR MBG_THREAD_FNC( void * );



/**
 * @brief A structure holding a time stamp / cycles count pair.
 *
 * This can be used to extrapolate the current time from the current
 * system cycles count.
 *
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * @see @ref mbg_xhrt_poll_group
 */
typedef struct
{
  PCPS_HR_TIME_CYCLES htc;    ///< Cycles count associated with the time stamp.
  uint64_t pcps_hr_tstamp64;  ///< Time stamp read from a device.

} MBG_XHRT_VARS;



/**
 * @brief A status structure provided by the time polling thread function.
 *
 * This can be used to get information from the poll thread function
 * and extrapolate the current time from the current system cycles count.
 *
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * @see @ref mbg_xhrt_poll_group
 */
typedef struct
{
  MBG_XHRT_VARS vars;
  MBG_XHRT_VARS prv_vars;
  MBG_PC_CYCLES_FREQUENCY freq_hz;
  int ioctl_status;
  int sleep_ms;
  MBG_CRIT_SECT crit_sect;
  MBG_DEV_HANDLE dh;

} MBG_XHRT_INFO;



/**
 * @brief A structure used to control a poll thread function.
 *
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see @ref mbg_xhrt_poll_group
 */
typedef struct
{
  MBG_XHRT_INFO xhrt_info;
  MBG_THREAD_INFO ti;

} MBG_POLL_THREAD_INFO;



/**
 * @brief Device match modes.
 *
 * Match modes to determine how to proceed if a device with
 * particular model type and serial number has not been detected.
 */
enum MBG_MATCH_MODES
{
  MBG_MATCH_ANY,      ///< Use the next device available on the system..
  MBG_MATCH_MODEL,    ///< Use the next available device of the same type
  MBG_MATCH_EXACTLY,  ///< Use only exactly the excat matching device, otherwise fail.
  N_MBG_MATCH_MODES   ///< Nnumber of known modes.
};



/**
 * @brief An entry for a list of device file names.
 *
 * Applications should consider this type as opaque,
 * and must not rely on the type, number, and order of
 * the structure members.
 */
typedef struct _MBG_DEV_FN_LIST_ENTRY MBG_DEV_FN_LIST_ENTRY;


/**
 * @brief The structure behind the ::MBG_DEV_FN_LIST_ENTRY type.
 *
 * Applications should consider this structure as opaque,
 * and must not rely on the type, number, and order of
 * the structure members.
 *
 * The declaration is still in the header file because
 * it is still required by some Windows-specific code.
 */
struct _MBG_DEV_FN_LIST_ENTRY
{
  char *dev_fn_ptr;  ///< Address of an OS-specific device file name
                     ///< that can be used with an OS-specific open call.
  MBG_DEV_FN_LIST_ENTRY *next;
};



/**
 * @brief An entry for a list of device names.
 *
 * Applications should consider this type as opaque,
 * and must not rely on the type, number, and order of
 * the structure members.
 *
 * @see ::mbg_find_devices_with_names
 * @see ::mbg_free_device_name_list
 */
typedef struct _MBG_DEV_NAME_LIST_ENTRY MBG_DEV_NAME_LIST_ENTRY;


/**
 * @brief The structure behind the ::MBG_DEV_NAME_LIST_ENTRY type.
 *
 * Applications should actually consider this structure as opaque,
 * and must not rely on the type, number, and order of
 * the structure members.
 *
 * The declaration is still in the header file because
 * it is still used by some Windows-specific code.
 */
struct _MBG_DEV_NAME_LIST_ENTRY
{
  char dev_name[40];      ///< Readable device name (Should be ::MBG_DEV_NAME, which is smaller, though)
  MBG_DEV_NAME_LIST_ENTRY *next;
};



/**
 * @defgroup mbgdevio_open_fncs API functions that can be used to open a device
 *
 * Each of these functions can be used to open a device to obtain a valid
 * ::MBG_DEV_HANDLE handle which can be used with subsequent API calls to
 * access the particular device.
 *
 * If the device fails to be opened then each of these functions returns
 * ::MBG_INVALID_DEV_HANDLE, and an OS-specific "last error" code is set
 * accordingly. The caller can then use ::mbg_get_last_error immediately
 * to retrieve the OS-specific error code, convert it, and return the
 * associated Meinberg-specific error code. See @ref MBG_ERROR_CODES.
 *
 * Operating systems maintain a list of devices that are currently
 * present in a system. The function ::mbg_open_device expects a number
 * that is used as an index to this list. If only a single supported device
 * is present then index 0 can always be used to open that device.
 *
 * The function ::mbg_find_devices can be used to retrieve the number of
 * devices statically present in the system, and the highest index number
 * that can be used is the number of devices - 1.
 *
 * In a plug-and-play system the device list maintained by the operating
 * system may change over time, for example if a USB device is plugged in
 * or out during operation. In this case index numbers may change, and may
 * not always refer to the same device.
 *
 * So the function ::mbg_open_device_by_name can be used instead to open
 * a specific device identified by model name and optionally serial number.
 * See ::MBG_DEV_NAME for the format of such device name.
 *
 * If a list of unique device names is required e.g. to set up a
 * device selection dialog in a GUI application then the function
 * ::mbg_find_devices_with_names can be used to allocate and set up
 * such a list. When the list has been evaluated and isn't needed
 * anymore then ::mbg_free_device_name_list should be called to free
 * the allocated memory.
 *
 * Inside the operating system the device is always identified by a
 * device file name which can be used internally with an OS-specific
 * "open" function, e.g. "open" on POSIX systems, or ""CreateFile"
 * on Windows.
 *
 * While the device file name is a complex string on Windows,
 * on Linux and similar systems this is just a device node name
 * like "/dev/mbgclock0" which can easily be used to specify a
 * particular device. See ::MBG_DEV_FN.
 *
 * On such systems the function ::mbg_open_device_by_dev_fn can be used
 * to open a device specified by a device file name.
 *
 * @see ::mbg_open_device
 * @see ::mbg_find_devices
 * @see ::mbg_open_device_by_name
 * @see ::mbg_find_devices_with_names
 * @see ::mbg_open_device_by_dev_fn
 * @see ::mbg_close_device
 */


/**
 * @defgroup mbgdevio_hr_time_fncs API functions reading high resolution time, plus status
 *
 * These functions return a high resolution time, including status flags and
 * local time offset according to the time zone configuration on the device.
 *
 * The API call ::mbg_chk_dev_has_hr_time checks if these functions are supported
 * by a device.
 *
 * Due to the extended information returned to the caller these function
 * require interaction with the on-board microcontroller, and thus take
 * longer to execute than the @ref mbgdevio_fast_timestamp_fncs.
 *
 * <b>Note:</b> These API calls provides better accuracy and higher resolution
 * than the the standard functions in @ref mbgdevio_legacy_time_fncs.
 *
 * The ::mbg_get_hr_time function doesn't account for the latency
 * which is introduced when accessing the board.
 *
 * The ::mbg_get_hr_time_cycles and ::mbg_get_hr_time_comp calls
 * provide ways to account for and/or compensate the latency.
 *
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 * @see @ref mbgdevio_fast_timestamp_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */


/**
 * @defgroup mbgdevio_fast_timestamp_fncs Fast API functions reading high resolution timestamps, but no status
 *
 * These functions are fast, but return only the %UTC time. No status flags,
 * no time zone offset. The time stamps are read from a latched counter chain
 * in the PCI chip set, so this is only supported by devices the chip set
 * of which supports this.
 *
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp_cycles
 * @see ::mbg_get_fast_hr_timestamp_comp
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */


/**
 * @defgroup mbgdevio_legacy_time_fncs Legacy API functions to read the time
 *
 * These functions are supported by all devices, but return time only
 * as a ::PCPS_TIME structure, which provides local time according to the
 * local time zone configured on the device, and with 10 ms resolution only.
 *
 * <b>Note:</b> The @ref mbgdevio_hr_time_fncs or @ref mbgdevio_fast_timestamp_fncs
 * should be used preferably, if the device supports these.
 *
 * @see ::mbg_get_time
 * @see ::mbg_get_time_cycles
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_fast_timestamp_fncs
 */


/**
 * @defgroup mbgdevio_chk_supp_fncs mbgdevio functions used to check if a particular feature is supported
 * @ingroup chk_supp_fncs
 *
 * These functions can be used to check if a device supports a particular feature.
 *
 * Whether a specific feature is supported or not, depends on the device type,
 * and may depend on the firmware version of the device.
 *
 * The low level device probe routine (e.g. in the kernel driver) determines
 * if a particular feature is indeed supported, or not, and these functions
 * just query the results of the probe routine.
 *
 * ::MBG_SUCCESS is returned if the requested feature is supported,
 * ::MBG_ERR_NOT_SUPP_BY_DEV if the feature is not supported, or one of the other
 * @ref MBG_ERROR_CODES is returned if an error occurred when trying to determine
 * if the feature is supported, e.g. if an IOCTL call failed for some reason.
 *
 * @see ::MBG_CHK_SUPP_FNC
 */


/**
 * @defgroup mbgdevio_chk_supp_fncs_deprecated Deprecated mbgdevio functions to check if a particular feature is supported
 * @ingroup chk_supp_fncs
 *
 * @deprecated These functions are deprecated. Use the @ref mbgdevio_chk_supp_fncs preferably.
 *
 * These are the original functions that were introduced to check if a device supports
 * a particular feature. The functions are deprecated, but will be kept for compatibility.
 *
 * The functions return ::MBG_SUCCESS if the information could be retrieved with out error,
 * and an error code otherwise, e.g. if the IOCTL call itself failed.
 *
 * Only in case of success an integer variable the address of which has to be passed to the
 * function is set to 1 or 0 depending on whether the requested feature is supported, or not.
 * So the calling application always has to check 2 values after such a function has been called.
 *
 * To make applications simpler the @ref mbgdevio_chk_supp_fncs have been introduced instead,
 * which can be used in a much easier way.
 *
 * @see @ref mbgdevio_chk_supp_fncs
 */



/**
 * @defgroup mbg_xhrt_poll_group Extrapolation of high resolution time stamps
 *
 * ::TODO
 */



/**
 * @brief The type of functions to check if a feature is supported.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see @ref mbgdevio_chk_supp_fncs
 */
typedef int _MBG_API MBG_CHK_SUPP_FNC( MBG_DEV_HANDLE dh );




/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Get the version number of the precompiled DLL/shared object library.
 *
 * If this library is used as a DLL/shared object library, the version
 * number can be checked to see if the header files which are actually used
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API function are called
 * in the correct way.
 *
 * @return The version number.
 *
 * @see ::mbgdevio_check_version
 * @see ::MBGDEVIO_VERSION defined in mbgdevio.h
 */
 _MBG_API_ATTR int _MBG_API mbgdevio_get_version( void ) ;

 /**
 * @brief Check if the DLL/shared library is compatible with a given version.
 *
 * If this library is used as a DLL/shared object library, the version
 * number can be checked to see if the header files which are actually used
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API functions are called
 * in the correct way.
 *
 * @param[in] header_version  Version number to be checked, should be ::MBGDEVIO_VERSION
 *                            from the mbgdevio.h file version used to build the application
 *
 * @return ::MBG_SUCCESS if compatible, else ::MBG_ERR_LIB_NOT_COMPATIBLE.
 *
 * @see ::mbgdevio_get_version
 * @see ::MBGDEVIO_VERSION defined in mbgdevio.h
 */
 _MBG_API_ATTR int _MBG_API mbgdevio_check_version( int header_version ) ;

 /**
 * @brief Check if support for SYN1588 devices is available.
 *
 * @return ::MBG_SUCCESS if SYN1588 support is available,<br>
 *         ::MBG_ERR_NOT_SUPP_BY_DRVR if driver has been built without SYN1588 support, or<br>
 *         ::MBG_ERR_NOT_SUPP_ON_OS if SYN1588 devices are not at all supported on the OS.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_if_syn1588_supported( void ) ;

 /**
 * @brief Check if a device supports the ::RECEIVER_INFO structure and related calls.
 *
 * Very old devices may not provide a ::RECEIVER_INFO structure.
 * The ::mbg_setup_receiver_info call should be used preferably to set up
 * a ::RECEIVER_INFO for a device. That function uses this call to determine
 * whether a ::RECEIVER_INFO can be read directly from a device, or sets up
 * a default structure using default values depending on the device type.
 *
 * @note This function should be preferred over ::mbg_dev_has_receiver_info,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_get_gps_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_receiver_info( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports large configuration data structures.
 *
 * Such structures have been introduced with the first Meinberg GPS receivers.
 * Mostly all configuration structures are large data structures, and mostly all
 * current devices support this, but some old devices may not.
 *
 * @note This function should be preferred over ::mbg_dev_has_gps_data,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_gps_data( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_generic_io API call.
 *
 * <b>Warning</b>: This call is for debugging purposes and internal use only!
 *
 * @note This function should be preferred over ::mbg_dev_has_generic_io,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_generic_io
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_generic_io( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_asic_version API call.
 *
 * Whether ::mbg_get_asic_version is supported, or not, depends on
 * the bus interface chip assembled on the device.
 *
 * @note This function should be preferred over ::mbg_dev_has_asic_version,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_asic_version
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_asic_version( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_asic_features call.
 *
 * Whether ::mbg_get_asic_features is supported, or not, depends on
 * the bus interface chip assembled on the device.
 *
 * @note This function should be preferred over ::mbg_dev_has_asic_features,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_asic_features
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_asic_features( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides eXtended Multi Ref (XMR) inputs.
 *
 * Devices providing XMR inputs can receive or decode different timing
 * signals in parallel, and the supported sources can be prioritized.
 *
 * @note This function should be preferred over ::mbg_dev_has_xmr,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_xmr( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if the device is connected via the ISA bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the ISA bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_isa( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if the device is connected via the MCA bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the MCA bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_mca( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if the device is connected via the PCI bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the PCI bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_pci( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if the device is connected via the PCI Express bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the PCI Express bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_pci_express( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if the device is connected via the USB bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the USB bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_usb( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a GPS receiver.
 *
 * The function also returns ::MBG_SUCCESS for GNSS receivers
 * which can track GPS satellites beside other GNSS systems.
 *
 * A pure GPS receiver is not considered a GNSS receiver, but
 * each of Meinberg's GNSS receivers is also a GPS receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_gps,
 * which has been deprecated.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_gps( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a GNSS receiver.
 *
 * This is usually the case if a device supports reception of
 * different satellite systems, e.g. GPS, Glonass, Galileo, etc.
 *
 * A pure GPS receiver is not considered a GNSS receiver, but
 * each of Meinberg's GNSS receivers is also a GPS receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_gnss,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_set_gps_gnss_mode_settings
 * @see ::mbg_get_gps_all_gnss_sat_info
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_gnss( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a DCF77 receiver.
 *
 * Beside standard DCF77 receivers which receive the legacy AM signal,
 * there are also PZF receivers which can decode the pseudo-random phase
 * modulation and thus yield a higher accuracy. See ::mbg_chk_dev_has_pzf.
 *
 * @note This function should be preferred over ::mbg_dev_is_dcf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_chk_dev_is_lwr
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_dcf( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports demodulation of the DCF77 PZF code.
 *
 * Beside the enhanced PZF correlation receivers which decode the DCF77's
 * pseudo-random phase modulation to yield a better accuracy, there are also
 * legacy DCF77 receivers which just decode the standard AM signal.
 * See ::mbg_chk_dev_is_dcf.
 *
 * @note This function should be preferred over ::mbg_dev_has_pzf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_corr_info
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_chk_dev_is_dcf
 * @see ::mbg_chk_dev_is_lwr
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_pzf( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a MSF receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_msf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_lwr
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_msf( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a WWVB receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_wwvb,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_lwr
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_wwvb( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a JJY receiver.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_lwr
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_jjy( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is any long wave signal receiver.
 *
 * Long wave receivers include e.g. DCF77, MSF, WWVB, or JJY.
 *
 * @note This function should be preferred over ::mbg_dev_is_lwr,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_dcf
 * @see ::mbg_chk_dev_is_msf
 * @see ::mbg_chk_dev_is_wwvb
 * @see ::mbg_chk_dev_is_jjy
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_lwr( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a time code receiver (TCR, IRIG or similar).
 *
 * @note This function should be preferred over ::mbg_dev_is_irig_rx,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_rx_info
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_has_irig
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_tcr( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device is a SYN1588 card.
 *
 * @note This function always fails if the driver package
 * has been built without support for SYN1588 cards.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle.
 *
 * @return ::MBG_SUCCESS if the device is a SYN1588 card,
 *         ::MBG_ERR_NO_DEV if is is not,<br>
 *         ::MBG_ERR_NOT_SUPP_BY_DRVR if driver has been built without SYN1588 support, or<br>
 *         ::MBG_ERR_NOT_SUPP_ON_OS if SYN1588 devices are not at all supported on the OS.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_is_syn1588_type( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports simple LAN interface API calls.
 *
 * @note This function should be preferred over ::mbg_dev_has_lan_intf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_state
 * @see ::mbg_get_ip4_settings
 * @see ::mbg_set_ip4_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_lan_intf( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports PTP configuration/status calls.
 *
 * @note This function should be preferred over ::mbg_dev_has_ptp,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_state
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ptp( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports PTP unicast feature/configuration.
 *
 * Not all devices which support PTP do also support PTP Unicast. This API
 * call checks if PTP Unicast is supported in addition to the standard
 * PTP multicast.
 *
 * @note This function should be preferred over ::mbg_dev_has_ptp_unicast,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_get_ptp_state
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ptp_unicast( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the mbg_get_hr_time... functions.
 *
 * @note This function should be preferred over ::mbg_dev_has_hr_time,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_hr_time( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the mbg_get_fast_hr_timestamp... calls.
 *
 * @note This function should be preferred over ::mbg_dev_has_fast_hr_timestamp,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_fast_hr_timestamp_cycles
 * @see ::mbg_get_fast_hr_timestamp_comp
 * @see ::mbg_get_fast_hr_timestamp
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_fast_hr_timestamp( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports configurable time scales.
 *
 * By default the devices return %UTC and/or local time. However, some devices
 * can be configured to return raw GPS time or TAI instead.
 *
 * @note This function should be preferred over ::mbg_dev_has_time_scale,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_time_scale_info
 * @see ::mbg_set_time_scale_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_time_scale( MBG_DEV_HANDLE dh ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Check if a device supports setting an event time.
 *
 * This feature is only supported by some special custom firmware
 * to preset a %UTC time at which the device is to generate an output signal.
 *
 * @note This function should be preferred over ::mbg_dev_has_event_time,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_set_event_time
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_event_time( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_ucap_entries and ::mbg_get_ucap_event calls.
 *
 * If the device doesn't support this but is a GPS card, it provides
 * a time capture FIFO buffer and the obsolete ::mbg_get_gps_ucap call
 * can be used to retrieve entries from the FIFO buffer.
 *
 * @note This function should be preferred over ::mbg_dev_has_ucap,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 * @see ::mbg_clr_ucap_buff
 * @see ::mbg_get_gps_ucap
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ucap( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_clr_ucap_buff call.
 *
 * The ::mbg_clr_ucap_buff call can be used to clear the on-board
 * time capture FIFO buffer of the device, but the call may not
 * be supported by some older GPS devices.
 *
 * @note This function should be preferred over ::mbg_dev_can_clr_ucap_buff,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_clr_ucap_buff
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_can_clr_ucap_buff( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports timezone configuration using the ::TZDL structure.
 *
 * @note This function should be preferred over ::mbg_dev_has_tzdl,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gps_tzdl
 * @see ::mbg_set_gps_tzdl
 * @see ::mbg_chk_dev_has_tz
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tzdl( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZDL structure.
 *
 * @note This function should be preferred over ::mbg_dev_has_pcps_tzdl,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_pcps_tzdl
 * @see ::mbg_set_pcps_tzdl
 * @see ::mbg_chk_dev_has_tz
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_pcps_tzdl( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZCODE type.
 *
 * @note This function should be preferred over ::mbg_dev_has_tzcode,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_tzcode
 * @see ::mbg_set_tzcode
 * @see ::mbg_chk_dev_has_tz
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tzcode( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports any kind of timezone configuration.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @note This function should be preferred over ::mbg_dev_has_tz,
 * which has been deprecated.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_tzdl
 * @see ::mbg_chk_dev_has_pcps_tzdl
 * @see ::mbg_chk_dev_has_tzcode
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tz( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides either an IRIG input or output.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig_tx
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides a configurable IRIG output.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_tx_info
 * @see ::mbg_set_irig_tx_settings
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig_tx( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_irig_ctrl_bits call.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig_ctrl_bits,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_ctrl_bits
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig_ctrl_bits( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_raw_irig_data call.
 *
 * @note This function should be preferred over ::mbg_dev_has_raw_irig_data,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_raw_irig_data
 * @see ::mbg_get_raw_irig_data_on_sec_change
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_raw_irig_data( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_irig_time call.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig_time,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_time
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig_time( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides the level of its inputs signal.
 *
 * This is useful to display the signal level of e.g. an IRIG or similar
 * time code, or a long wave signal.
 *
 * @note This function should be preferred over ::mbg_dev_has_signal,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_signal( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides a modulation signal.
 *
 * Modulation signals are e.g. the second marks provided by a long wave receiver.
 *
 * @note This function should be preferred over ::mbg_dev_has_mod,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_mod( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the "last sync time".
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_sync_time( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides the receiver position.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_rcvr_pos( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides the status of buffered variables.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_bvar_stat( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_ttm_time API call.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ttm_time( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides one or more serial ports.
 *
 * @note This function should only be used for legacy devices that
 * don't provide a ::RECEIVER_INFO structure.
 * Otherwise ::RECEIVER_INFO::n_com_ports should be checked to see
 * if and how many serial ports are provided by the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_serial( MBG_DEV_HANDLE dh ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Check if a device supports higher baud rates than usual.
 *
 * Check if a device provides a serial output that supports
 * higher baud rates than older ones, i.e. ::DEFAULT_BAUD_RATES_DCF_HS
 * rather than ::DEFAULT_BAUD_RATES_DCF.
 *
 * The call ::mbg_get_serial_settings takes care of this, so applications
 * which use that call as suggested don't need to use this call directly.
 *
 * @note This function should be preferred over ::mbg_dev_has_serial_hs,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_serial_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_serial_hs( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides a programmable frequency synthesizer.
 *
 * @note This function should be preferred over ::mbg_dev_has_synth,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_synth
 * @see ::mbg_set_synth
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_synth( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides GPIO signal inputs and/or outputs.
 *
 * @note This function should be preferred over ::mbg_dev_has_gpio,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_gpio( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports configuration of antenna cable length.
 *
 * @note This function should be preferred over ::mbg_dev_has_cab_len,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gps_ant_cable_len
 * @see ::mbg_set_gps_ant_cable_len
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_cab_len( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides a configurable ref time offset.
 *
 * This may be required to convert the received time to %UTC, if the input
 * signal doesn't specify this (e.g. with most IRIG code formats).
 *
 * @note This function should be preferred over ::mbg_dev_has_ref_offs,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_ref_offs
 * @see ::mbg_set_ref_offs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ref_offs( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::MBG_OPT_INFO/::MBG_OPT_SETTINGS.
 *
 * These structures contain optional settings, controlled by flags.
 * See ::MBG_OPT_SETTINGS and associated definitions.
 *
 * @note This function should be preferred over ::mbg_dev_has_opt_flags,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_opt_info
 * @see ::mbg_set_opt_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_opt_flags( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports reading/writing of %UTC parameters.
 *
 * This API call checks if a device supports reading/writing a GPS %UTC
 * parameter set via the PC bus. Reading/writing these parameters via the
 * serial port using the Meinberg binary data protocol is supported by all
 * Meinberg GPS and GNSS devices.
 *
 * The %UTC parameter set is usually received from the satellites' broadcasts
 * and contains the current time offset between GPS time and %UTC, plus information
 * on a pending leap second event.
 *
 * It may be useful to overwrite them to do some tests, or for applications
 * where the device is freewheeling anyway.
 *
 * @note This function should be preferred over ::mbg_dev_has_utc_parm,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_utc_parm
 * @see ::mbg_set_utc_parm
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_utc_parm( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports reading correlation info.
 *
 * The PZF phase modulation decoded by DCF77 PZF receivers is decoded
 * using a correlation approach, and optionally there's an API call
 * supported which can be used to retrieve the correlation value
 * and thus determine the quality of the input signal.
 *
 * @note This function should be preferred over ::mbg_dev_has_corr_info,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_get_corr_info
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_corr_info( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports configurable distance from transmitter.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, mostly with long wave receivers.
 *
 * @note This function should be preferred over ::mbg_dev_has_tr_distance,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_get_tr_distance
 * @see ::mbg_set_tr_distance
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tr_distance( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides a debug status word to be read.
 *
 * @note This function should be preferred over ::mbg_dev_has_debug_status,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_debug_status
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_debug_status( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device provides an on-board event log.
 *
 * @note This function should be preferred over ::mbg_dev_has_evt_log,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_first_evt_log_entry
 * @see ::mbg_get_next_evt_log_entry
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_evt_log( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports bus level DAC control commands.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_has_dac_ctrl( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Check if a device supports the ::RECEIVER_INFO structure and related calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_receiver_info preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_receiver_info
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_receiver_info" ) _MBG_API mbg_dev_has_receiver_info( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports large configuration data structures.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_gps_data preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_gps_data
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_gps_data" ) _MBG_API mbg_dev_has_gps_data( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_generic_io API call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_generic_io preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_generic_io
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_generic_io" ) _MBG_API mbg_dev_has_generic_io( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_asic_version call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_asic_version preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_asic_version
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_asic_version" ) _MBG_API mbg_dev_has_asic_version( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_asic_features call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_asic_features preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_asic_features
 * @see ::mbg_get_asic_features
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_asic_features" ) _MBG_API mbg_dev_has_asic_features( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides eXtended Multi Ref (XMR) inputs.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_xmr preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_xmr
 * @see @ref group_multi_ref_ext
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_xmr" ) _MBG_API mbg_dev_has_xmr( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports GNSS configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_gnss preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_gnss
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_gnss" ) _MBG_API mbg_dev_is_gnss( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device is a GPS receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_gps preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_gps
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_gps" ) _MBG_API mbg_dev_is_gps( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device is a DCF77 receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_dcf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_dcf
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_dcf" ) _MBG_API mbg_dev_is_dcf( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports demodulation of the DCF77 PZF code.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_pzf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_pzf
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_pzf" ) _MBG_API mbg_dev_has_pzf( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device is a MSF receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_msf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_msf
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_msf" ) _MBG_API mbg_dev_is_msf( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device is a WWVB receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_wwvb preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_wwvb
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_wwvb" ) _MBG_API mbg_dev_is_wwvb( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device is any long wave signal receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_lwr preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_lwr
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_lwr" ) _MBG_API mbg_dev_is_lwr( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides a configurable IRIG input.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_tcr preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_tcr
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_tcr" ) _MBG_API mbg_dev_is_irig_rx( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports simple LAN interface API calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_lan_intf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_lan_intf
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_lan_intf" ) _MBG_API mbg_dev_has_lan_intf( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports PTP configuration/status calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ptp preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ptp
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ptp" ) _MBG_API mbg_dev_has_ptp( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports PTP unicast feature/configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ptp_unicast preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ptp_unicast" ) _MBG_API mbg_dev_has_ptp_unicast( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the HR_TIME functions.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_hr_time preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_hr_time
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_hr_time" ) _MBG_API mbg_dev_has_hr_time( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the mbg_get_fast_hr_timestamp_...() calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_fast_hr_timestamp preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_fast_hr_timestamp" ) _MBG_API mbg_dev_has_fast_hr_timestamp( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports configurable time scales.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_time_scale preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_time_scale
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_time_scale" ) _MBG_API mbg_dev_has_time_scale( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports setting an event time.
 *
 * @note This is only supported by some customized devices
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_event_time preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_event_time
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_event_time" ) _MBG_API mbg_dev_has_event_time( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_ucap_entries and ::mbg_get_ucap_event calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ucap preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ucap
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ucap" ) _MBG_API mbg_dev_has_ucap( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_clr_ucap_buff call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_can_clr_ucap_buff preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_can_clr_ucap_buff
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_can_clr_ucap_buff" ) _MBG_API mbg_dev_can_clr_ucap_buff( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports timezone configuration using the ::TZDL structure.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tzdl preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tzdl
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tzdl" ) _MBG_API mbg_dev_has_tzdl( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZDL structure.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_pcps_tzdl preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_pcps_tzdl
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_pcps_tzdl" ) _MBG_API mbg_dev_has_pcps_tzdl( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZCODE type.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tzcode preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tzcode
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tzcode" ) _MBG_API mbg_dev_has_tzcode( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports any kind of timezone configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tz preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tz
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tz" ) _MBG_API mbg_dev_has_tz( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides either an IRIG input or output.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig" ) _MBG_API mbg_dev_has_irig( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides a configurable IRIG output.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig_tx preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig_tx
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig_tx" ) _MBG_API mbg_dev_has_irig_tx( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_irig_ctrl_bits call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig_ctrl_bits preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig_ctrl_bits
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig_ctrl_bits" ) _MBG_API mbg_dev_has_irig_ctrl_bits( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_raw_irig_data call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_raw_irig_data preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_raw_irig_data
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_raw_irig_data" ) _MBG_API mbg_dev_has_raw_irig_data( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::mbg_get_irig_time call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig_time preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig_time
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig_time" ) _MBG_API mbg_dev_has_irig_time( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides the level of its inputs signal.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_signal preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_signal
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_signal" ) _MBG_API mbg_dev_has_signal( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides a modulation signal.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_mod preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_mod
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_mod" ) _MBG_API mbg_dev_has_mod( MBG_DEV_HANDLE dh, int *p ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Check if a device supports higher baud rates than usual.
 *
 * Check if a device provides a serial output that supports
 * higher baud rates than older ones, i.e. ::DEFAULT_BAUD_RATES_DCF_HS
 * rather than ::DEFAULT_BAUD_RATES_DCF.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_serial_hs preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_serial_hs
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_serial_hs" ) _MBG_API mbg_dev_has_serial_hs( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides a programmable frequency synthesizer.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_synth preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_synth
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_synth" ) _MBG_API mbg_dev_has_synth( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides GPIO signal inputs and/or outputs.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_gpio preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_gpio
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_gpio" ) _MBG_API mbg_dev_has_gpio( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports configuration of antenna cable length.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_cab_len preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_cab_len
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_cab_len" ) _MBG_API mbg_dev_has_cab_len( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides a configurable ref time offset.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ref_offs preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ref_offs
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ref_offs" ) _MBG_API mbg_dev_has_ref_offs( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports the ::MBG_OPT_INFO/::MBG_OPT_SETTINGS.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_opt_flags preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_opt_flags
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_opt_flags" ) _MBG_API mbg_dev_has_opt_flags( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device support reading/writing of ::UTC parameters.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_utc_parm preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_utc_parm
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_utc_parm" ) _MBG_API mbg_dev_has_utc_parm( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports reading correlation info.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_corr_info preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_corr_info
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_corr_info" ) _MBG_API mbg_dev_has_corr_info( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device supports configurable distance from transmitter.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tr_distance preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tr_distance
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tr_distance" ) _MBG_API mbg_dev_has_tr_distance( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides a debug status word to be read.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_debug_status preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_debug_status
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_debug_status" ) _MBG_API mbg_dev_has_debug_status( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a device provides an on-board event log.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_evt_log preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_evt_log
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_evt_log" ) _MBG_API mbg_dev_has_evt_log( MBG_DEV_HANDLE dh, int *p ) ;

 /**
 * @brief Check if a string is a valid device file name.
 *
 * @param[in]  s  The string to be checked.
 *
 * @return ::MBG_SUCCESS if @p s is a valid device file name, else ::MBG_ERR_INV_PARM.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
 _MBG_API_ATTR int _MBG_API mbg_chk_dev_fn_is_valid( const char *s ) ;

 /**
 * @brief Create a device file name associated with an index number.
 *
 * Create a system-specific device file name string, e.g. @a /dev/mbgclock0
 * on Linux and similar systems, which can be used with the @a open call
 * on systems which support this, and is also appropriate for informational
 * messages, e.g. when referring to a specific device.
 *
 * On Windows, a complex hardware ID string is required to open the device,
 * and the Windows-specific function mbg_svc_get_device_path can be used
 * to get the hardware ID string associated with a specific device index.
 *
 * However, the Windows hardware ID string is not appropriate for
 * informational messages referring to a device, so this function
 * may return an empty string on Windows, or a pseudo device name
 * for devices like SYN1588 PCIe NIC devices.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   dev_idx  The device index number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_dev_info_from_dev_idx
 * @see mbg_svc_get_device_path on Windows
 */
 _MBG_API_ATTR int _MBG_API mbg_dev_fn_from_dev_idx( char *s, int max_len, int dev_idx ) ;

 /**
 * @brief Create a device info string associated with an index number.
 *
 * This consists of a generic string such as "Device #0", followed
 * by an associated device file name (e.g. "/dev /mbgclock0"),
 * if appropriate.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   dev_idx  The device index number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
 _MBG_API_ATTR int _MBG_API mbg_dev_info_from_dev_idx( char *s, int max_len, int dev_idx ) ;

 /**
 * @brief Open a device by index number.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  dev_idx  Device index number, starting from 0.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 */
 _MBG_API_ATTR MBG_DEV_HANDLE _MBG_API mbg_open_device( int dev_idx ) ;

 /**
 * @brief Open a device specified by a device file name.
 *
 * The format the device file name depends on the operating system.
 * see ::MBG_DEV_FN.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  dev_fn  The device file name.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 * @see ::MBG_DEV_FN
 */
 _MBG_API_ATTR MBG_DEV_HANDLE _MBG_API mbg_open_device_by_dev_fn( const char *dev_fn ) ;

 /**
 * @brief Open a device specified by a device file name.
 *
 * @deprecated This function is deprecated, use
 * ::mbg_open_device_by_dev_fn preferably.
 *
 * The format the device file name depends on the operating system.
 * see ::MBG_DEV_FN.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  dev_fn  The device file name, see ::MBG_DEV_FN.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 * @see ::MBG_DEV_FN
 */
 _MBG_API_ATTR MBG_DEV_HANDLE _DEPRECATED_BY( "mbg_open_device_by_dev_fn" ) _MBG_API mbg_open_device_by_hw_id( const char *dev_fn ) ;

 /**
 * @brief Get the number of devices installed on the computer.
 *
 * The function ::mbg_find_devices_with_names should probably
 * be used preferably. See @ref mbgdevio_open_fncs.
 *
 * @return The number of devices found.
 *
 * @see ::mbg_find_devices_with_names
 * @see ::mbg_open_device
 * @see @ref mbgdevio_open_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_find_devices( void ) ;

 /**
 * @brief Allocate memory and set up a list of installed and supported devices.
 *
 * Allocate and fill a list with the names of Meinberg devices currently
 * present in the system.
 *
 * This can be used e.g. to populate a device selection dialog
 * in a configuration program.
 *
 * When the list is not used anymore it can be freed by calling
 * ::mbg_free_device_name_list.
 *
 * @param[in]  p_list       Pointer to a linked list to be allocated.
 * @param[in]  max_devices  Maximum number of devices to be searched for
 *                          (must not exceed ::N_SUPP_DEV_BUS).
 *
 * @return The number of present devices.
 *
 * @see ::mbg_free_device_name_list
 * @see ::mbg_find_devices
 * @see ::MBG_DEV_NAME
 */
 _MBG_API_ATTR int _MBG_API mbg_find_devices_with_names( MBG_DEV_NAME_LIST_ENTRY **p_list, int max_devices ) ;

 /**
 * @brief Free the memory allocated for a list of ::MBG_DEV_NAME_LIST_ENTRY entries.
 *
 * The list may have been set up and allocated before
 * by ::mbg_find_devices_with_names.
 *
 * @param[in,out]  list  Linked list of ::MBG_DEV_NAME_LIST_ENTRY entries.
 *
 * @see ::mbg_find_devices_with_names
 */
 _MBG_API_ATTR void _MBG_API mbg_free_device_name_list( MBG_DEV_NAME_LIST_ENTRY *list ) ;

 /**
 * @brief Split a string in ::MBG_DEV_NAME format its components.
 *
 * The components are the 'type name' and 'serial number' strings.
 *
 * See ::MBG_DEV_NAME for the possible formats of a device name.
 * If no @p dev_name is passed, the destination buffers are set
 * to empty strings.
 *
 * One of the complementary functions ::mbg_create_dev_name or
 * ::mbg_create_dev_name_from_dev_info can be used to generate
 * a device name string.
 *
 * @param[in]   dev_name        The string in ::MBG_DEV_NAME format to be split up.
 * @param[out]  type_name       Output buffer for the type name part, e.g. a ::PCPS_CLOCK_NAME.
 * @param[in]   type_name_size  Size of the output buffer for the type name string.
 * @param[out]  sernum          Output buffer for the sernum part, e.g. a ::PCPS_SN_STR.
 * @param[in]   sernum_size     Size of the output buffer for the sernum string.
 *
 * @see ::mbg_create_dev_name
 * @see ::mbg_create_dev_name_from_dev_info
 * @see ::MBG_DEV_NAME
 * @see ::MBG_DEV_NAME_FMT
 */
 _MBG_API_ATTR void _MBG_API mbg_split_dev_name( const char *dev_name, char *type_name, size_t type_name_size, char *sernum, size_t sernum_size ) ;

 /**
 * @brief Return a handle to a device with a particular device name.
 *
 * See ::MBG_DEV_NAME for the possible formats of a device name.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  srch_name       String in ::MBG_DEV_NAME format of a device to be opened.
 * @param[in]  selection_mode  One of the ::MBG_MATCH_MODES.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 * @see ::MBG_DEV_NAME
 * @see ::MBG_MATCH_MODES
 */
 _MBG_API_ATTR MBG_DEV_HANDLE _MBG_API mbg_open_device_by_name( const char *srch_name, int selection_mode ) ;

 /**
 * @brief Close a device handle and set the handle value to ::MBG_INVALID_DEV_HANDLE.
 *
 * @param[in,out] p_dh  Pointer to a Meinberg device handle.
 *
 * @see @ref mbgdevio_open_fncs
 */
 _MBG_API_ATTR void _MBG_API mbg_close_device( MBG_DEV_HANDLE *p_dh ) ;

 /**
 * @brief Read information about the driver handling a given device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::PCPS_DRVR_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 _MBG_API_ATTR int _MBG_API mbg_get_drvr_info( MBG_DEV_HANDLE dh, PCPS_DRVR_INFO *p ) ;

 /**
 * @brief Read detailed device information.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Address of a ::PCPS_DEV structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 _MBG_API_ATTR int _MBG_API mbg_get_device_info( MBG_DEV_HANDLE dh, PCPS_DEV *p ) ;

 /**
 * @brief Read CPU type info of the device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Address of a ::PCPS_CPU_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 _MBG_API_ATTR int _MBG_API mbg_get_pcps_cpu_info( MBG_DEV_HANDLE dh, PCPS_CPU_INFO *p ) ;

 /**
 * @brief Read the current state of the on-board ::PCPS_STATUS_PORT.
 *
 * This function is useful to read the status port of the device which
 * also includes the ::PCPS_ST_MOD bit, which reflects the time code
 * modulation of long wave receivers.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::PCPS_STATUS_PORT value to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see @ref group_status_port "bitmask"  //### TODO check syntax
 */
 _MBG_API_ATTR int _MBG_API mbg_get_status_port( MBG_DEV_HANDLE dh, PCPS_STATUS_PORT *p ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Generic read function.
 *
 * Writes a command code to a device and reads a number
 * of replied data to a generic buffer.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   cmd   Can be any @ref PCPS_CMD_CODES "command code" supported by the device.
 * @param[out]  p     Pointer to a buffer to be filled up.
 * @param[in]   size  Size of the output buffer.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_generic_write
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_write_gps
 * @see ::mbg_generic_io
 */
 _MBG_API_ATTR int _MBG_API mbg_generic_read( MBG_DEV_HANDLE dh, int cmd, void *p, int size ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Generic read GPS function.
 *
 * Writes a GPS command code to a device and reads a number
 * of data bytes back into a generic buffer.
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   cmd   One of the ::PCPS_CMD_CODES supported by the device.
 * @param[out]  p     Pointer to a buffer to be filled up.
 * @param[in]   size  Size of the buffer, has to match the expected data size associated with @p cmd.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gps_data
 * @see ::mbg_generic_write_gps
 * @see ::mbg_generic_read
 * @see ::mbg_generic_write
 * @see ::mbg_generic_io
 */
 _MBG_API_ATTR int _MBG_API mbg_generic_read_gps( MBG_DEV_HANDLE dh, int cmd, void *p, int size ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Generic write function.
 *
 * Writes a command code plus an associated number
 * of data bytes to a device.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]  dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  cmd   One of the ::PCPS_CMD_CODES supported by the device.
 * @param[in]  p     Pointer to a buffer of data to be written.
 * @param[in]  size  Size of the buffer, has to match the expected data size associated with @p cmd.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_generic_read
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_write_gps
 * @see ::mbg_generic_io
 */
 _MBG_API_ATTR int _MBG_API mbg_generic_write( MBG_DEV_HANDLE dh, int cmd, const void *p, int size ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Generic write GPS function.
 *
 * Writes a GPS command code plus an associated number
 * of data bytes to a device.
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]  dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  cmd   One of the ::PCPS_CMD_CODES supported by the device.
 * @param[in]  p     Pointer to a buffer of data to be written.
 * @param[in]  size  Size of the buffer, has to match the expected data size associated with @p cmd.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gps_data
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_read
 * @see ::mbg_generic_write
 * @see ::mbg_generic_io
 */
 _MBG_API_ATTR int _MBG_API mbg_generic_write_gps( MBG_DEV_HANDLE dh, int cmd, const void *p, int size ) ;

 /* (Intentionally excluded from Doxygen)
 * @brief Write and/or read generic data to/from a device.
 *
 * The function ::mbg_chk_dev_has_generic_io checks
 * whether this call is supported by a device.
 *
 * <b>Warning</b>: This call is for debugging purposes and internal use only!
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   type    The type of data to be read/written, see @ref PCPS_CMD_CODES.
 * @param[in]   in_p    Pointer to an input buffer with the data to be written.
 * @param[in]   in_sz   Size of the input buffer, has to match the expected data size associated with @p type.
 * @param[out]  out_p   Pointer to an output buffer to be filled up.
 * @param[in]   out_sz  Size of the output buffer, has to match the expected data size associated with @p type.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_generic_io
 * @see ::mbg_generic_read
 * @see ::mbg_generic_write
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_write_gps
 */
 _MBG_API_ATTR int _MBG_API mbg_generic_io( MBG_DEV_HANDLE dh, int type, const void *in_p, int in_sz, void *out_p, int out_sz ) ;

 /**
 * @brief Read a ::PCPS_TIME structure returning the current date/time/status.
 *
 * The returned time is local time according to the zone setting onboard
 * the device, with a resolution of 10 ms (i.e. 10ths of seconds) only.
 *
 * This call is supported by every device manufactured by Meinberg.
 * However, for higher accuracy and resolution the @ref mbgdevio_hr_time_fncs or
 * the @ref mbgdevio_fast_timestamp_fncs group of calls should be used preferably,
 * if supported by the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_legacy_time_fncs
 * @see ::mbg_get_hr_time
 * @see ::mbg_set_time
 * @see ::mbg_get_sync_time
 */
 _MBG_API_ATTR int _MBG_API mbg_get_time( MBG_DEV_HANDLE dh, PCPS_TIME *p ) ;

 /**
 * @brief Set the clock onboard the device to a given date and time.
 *
 * The macro ::_pcps_can_set_time checks whether
 * this call is supported by a device.
 *
 * @todo Provide an API function replacing ::_pcps_can_set_time.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_STIME structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_time
 */
 _MBG_API_ATTR int _MBG_API mbg_set_time( MBG_DEV_HANDLE dh, const PCPS_STIME *p ) ;

 /**
 * @brief Read the time when the device has last recently synchronized.
 *
 * Fills a ::PCPS_TIME structure with the date/time/status reporting
 * when the device was synchronized the last time to its time source,
 * e.g. the DCF77 signal, the GPS satellites, or similar.
 *
 * The API call ::mbg_chk_dev_has_sync_time checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> If that information is not available on the device,
 * the value of the returned ::PCPS_TIME::sec field is set to 0xFF.
 * The macro ::_pcps_time_is_read can be used to check whether the
 * returned information is valid, or "not set".
 *
 * @todo Provide an API function replacing ::_pcps_has_sync_time.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_sync_time
 * @see ::mbg_get_time
 */
 _MBG_API_ATTR int _MBG_API mbg_get_sync_time( MBG_DEV_HANDLE dh, PCPS_TIME *p ) ;

 /**
 * @brief Wait until the next second change, then return current time.
 *
 * Returns time in a ::PCPS_TIME structure similar to ::mbg_get_time.
 *
 * <b>Note:</b> This API call is supported on Windows only.
 * The call blocks until the kernel driver detects a second change
 * reported by the device. The accuracy of this call is limited
 * to a few milliseconds.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_legacy_time_fncs
 * @see ::mbg_get_time
 */
 _MBG_API_ATTR int _MBG_API mbg_get_time_sec_change( MBG_DEV_HANDLE dh, PCPS_TIME *p ) ;

 /**
 * @brief Read the current high resolution time, including status.
 *
 * Fills up a ::PCPS_HR_TIME (High Resolution time) structure containing
 * the current %UTC time (seconds since 1970), %UTC offset, and status
 * (see @ref PCPS_TIME_STATUS_FLAGS).
 *
 * The API call ::mbg_chk_dev_has_hr_time checks whether
 * this call is supported by a device.
 *
 * For details see @ref ::mbgdevio_hr_time_fncs
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_HR_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_fast_timestamp_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_get_hr_time( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p ) ;

 /* (Intentionally excluded from Doxygen )
 * @brief Write a high resolution time stamp ::PCPS_TIME_STAMP to a device.
 *
 * Used to configure a %UTC time when the device shall generate an event.
 * The API call ::mbg_chk_dev_has_event_time checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This is only supported by devices running some special firmware.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_TIME_STAMP structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_event_time
 */
 _MBG_API_ATTR int _MBG_API mbg_set_event_time( MBG_DEV_HANDLE dh, const PCPS_TIME_STAMP *p ) ;

 /**
 * @brief Read the serial port configuration from an old type of device.
 *
 * @deprecated Direct usage of this function is deprecated. The generic
 * API function ::mbg_get_serial_settings should be used instead
 * which fully supports the capabilities of current devices.
 *
 * The API call ::mbg_chk_dev_has_serial checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_SERIAL structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_get_serial( MBG_DEV_HANDLE dh, PCPS_SERIAL *p ) ;

 /**
 * @brief Write the serial port configuration to an old type of device.
 *
 * @deprecated Direct usage of this function is deprecated. The generic
 * API function ::mbg_save_serial_settings should be used instead
 * which fully supports the capabilities of current devices.
 *
 * The API call ::mbg_chk_dev_has_serial checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_SERIAL structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_set_serial( MBG_DEV_HANDLE dh, const PCPS_SERIAL *p ) ;

 /**
 * @brief Read time zone/daylight saving configuration code from a device.
 *
 * The APIs using ::PCPS_TZCODE are only supported by some simpler devices
 * and allow just a very basic configuration.
 *
 * The API call ::mbg_chk_dev_has_tzcode checks whether
 * this call is supported by a device.
 *
 * Other devices may support the ::mbg_get_pcps_tzdl or ::mbg_get_gps_tzdl
 * calls instead which allow for a more detailed configuration of the
 * time zone and daylight saving settings.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TZCODE structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzcode
 * @see ::mbg_set_tzcode
 * @see ::mbg_get_pcps_tzdl
 * @see ::mbg_get_gps_tzdl
 */
 _MBG_API_ATTR int _MBG_API mbg_get_tzcode( MBG_DEV_HANDLE dh, PCPS_TZCODE *p ) ;

 /**
 * @brief Write time zone/daylight saving configuration code to a device.
 *
 * The APIs using ::PCPS_TZCODE are only supported by some simpler devices
 * and allow only a very basic configuration.
 *
 * The API call ::mbg_chk_dev_has_tzcode checks whether
 * this call is supported by a device.
 *
 * Other devices may support the ::mbg_set_pcps_tzdl or ::mbg_set_gps_tzdl
 * calls instead which allow for a more detailed configuration of the
 * time zone and daylight saving settings.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_TZCODE structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzcode
 * @see ::mbg_get_tzcode
 * @see ::mbg_set_pcps_tzdl
 * @see ::mbg_set_gps_tzdl
 */
 _MBG_API_ATTR int _MBG_API mbg_set_tzcode( MBG_DEV_HANDLE dh, const PCPS_TZCODE *p ) ;

 /**
 * @brief Read time zone/daylight saving parameters from a device.
 *
 * This function fills up a ::PCPS_TZDL structure which supports a more
 * detailed configuration of time zone and daylight saving than the ::PCPS_TZCODE
 * type.
 *
 * The API call ::mbg_chk_dev_has_pcps_tzdl checks whether
 * this call is supported by a device.
 *
 * Other devices may support the ::mbg_get_tzcode or ::mbg_get_gps_tzdl
 * calls instead.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TZDL structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pcps_tzdl
 * @see ::mbg_set_pcps_tzdl
 * @see ::mbg_get_tzcode
 * @see ::mbg_get_gps_tzdl
 */
 _MBG_API_ATTR int _MBG_API mbg_get_pcps_tzdl( MBG_DEV_HANDLE dh, PCPS_TZDL *p ) ;

 /**
 * @brief Write time zone/daylight saving parameters to a device.
 *
 * This function passes a ::PCPS_TZDL structure to a device which supports
 * a more detailed configuration of time zone and daylight saving than the
 * ::PCPS_TZCODE type.
 *
 * The API call ::mbg_chk_dev_has_pcps_tzdl checks whether
 * this call is supported by a device.
 * Other devices may support the ::mbg_set_tzcode or ::mbg_set_gps_tzdl
 * calls instead.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_TZDL structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pcps_tzdl
 * @see ::mbg_get_pcps_tzdl
 * @see ::mbg_set_tzcode
 * @see ::mbg_set_gps_tzdl
 */
 _MBG_API_ATTR int _MBG_API mbg_set_pcps_tzdl( MBG_DEV_HANDLE dh, const PCPS_TZDL *p ) ;

 /**
 * @brief Read the %UTC offset configuration of the reference time from a device.
 *
 * This parameter is used to specify the %UTC offset of an incoming
 * reference time signal if a kind of time signal e.g. an IRIG input
 * signal) does not provide this information.
 *
 * The API call ::mbg_chk_dev_has_ref_offs checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_REF_OFFS value to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ref_offs
 * @see ::mbg_set_ref_offs
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ref_offs( MBG_DEV_HANDLE dh, MBG_REF_OFFS *p ) ;

 /**
 * @brief Write the %UTC offset configuration of the reference time to a device.
 *
 * This parameter is used to specify the %UTC offset of an incoming
 * reference time signal if a kind of time signal e.g. an IRIG input
 * signal) does not provide this information.
 *
 * The API call ::mbg_chk_dev_has_ref_offs checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::MBG_REF_OFFS value to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ref_offs
 * @see ::mbg_get_ref_offs
 */
 _MBG_API_ATTR int _MBG_API mbg_set_ref_offs( MBG_DEV_HANDLE dh, const MBG_REF_OFFS *p ) ;

 /**
 * @brief Read a ::MBG_OPT_INFO structure containing optional settings, controlled by flags.
 *
 * The ::MBG_OPT_INFO structure contains a mask of supported flags plus the current
 * settings of those flags.
 *
 * The API call ::mbg_chk_dev_has_opt_flags checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_OPT_INFO structure to be filled up
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_opt_flags
 * @see ::mbg_set_opt_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_get_opt_info( MBG_DEV_HANDLE dh, MBG_OPT_INFO *p ) ;

 /**
 * @brief Write a ::MBG_OPT_SETTINGS structure containing optional device settings.
 *
 * The API call ::mbg_chk_dev_has_opt_flags checks whether
 * this call is supported by a device.
 *
 * ::mbg_get_opt_info should be called first to check which of
 * the specified flags is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_OPT_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_opt_flags
 * @see ::mbg_get_opt_info
 */
 _MBG_API_ATTR int _MBG_API mbg_set_opt_settings( MBG_DEV_HANDLE dh, const MBG_OPT_SETTINGS *p ) ;

 /**
 * @brief Read the current IRIG input settings plus capabilities.
 *
 * @deprecated Calling this function directly is deprecated. The function
 * ::mbg_get_all_irig_rx_info should be used instead which also reads some
 * other associated parameters affecting the behaviour of the IRIG input.
 *
 * The API call ::mbg_chk_dev_is_tcr checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Address of an ::IRIG_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_all_irig_rx_info
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
 _MBG_API_ATTR int _MBG_API mbg_get_irig_rx_info( MBG_DEV_HANDLE dh, IRIG_INFO *p ) ;

 /**
 * @brief Write an ::IRIG_SETTINGS structure to a device to configure an IRIG input.
 *
 * @deprecated Calling this function directly is deprecated. The function
 * ::mbg_save_all_irig_rx_settings should be used instead which also writes some
 * other associated parameters affecting the behaviour of the IRIG input.
 *
 * The API call ::mbg_chk_dev_is_tcr checks whether
 * this call is supported by a device.
 * ::mbg_get_irig_rx_info should be called first to determine
 * the possible settings supported by the input of the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::IRIG_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_all_irig_rx_settings
 * @see ::mbg_get_irig_rx_info
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
 _MBG_API_ATTR int _MBG_API mbg_set_irig_rx_settings( MBG_DEV_HANDLE dh, const IRIG_SETTINGS *p ) ;

 /**
 * @brief Read all IRIG input configuration information from a device.
 *
 * @param[in]  dh           Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p_obs        Obsolete pointer kept for compatibility, should be @a NULL.
 * @param[in]  p_irig_info  Pointer to a ::IRIG_SETTINGS structure to be read.
 * @param[in]  p_ref_offs   Pointer to a ::MBG_REF_OFFS structure to be read.
 * @param[in]  p_opt_info   Pointer to a ::MBG_OPT_SETTINGS structure to be read.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_all_irig_rx_settings
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_set_ref_offs
 * @see ::mbg_set_opt_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_get_all_irig_rx_info( MBG_DEV_HANDLE dh, const void *p_obs, IRIG_INFO *p_irig_info, MBG_REF_OFFS *p_ref_offs, MBG_OPT_INFO *p_opt_info ) ;

 /**
 * @brief Write all IRIG input configuration settings to a device.
 *
 * The API call ::mbg_chk_dev_is_tcr checks whether
 * this call is supported by a device.
 * ::mbg_get_all_irig_rx_info should be called before to determine
 * the possible settings supported by the IRIG input.
 *
 * @param[in]   dh              Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   p_obs           Obsolete pointer kept for compatibility, should be @a NULL.
 * @param[out]  p_irig_settings Pointer to a ::IRIG_SETTINGS structure to be written.
 * @param[out]  p_ref_offs      Pointer to a ::MBG_REF_OFFS structure to be written.
 * @param[out]  p_opt_settings  Pointer to a ::MBG_OPT_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_all_irig_rx_info
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_set_ref_offs
 * @see ::mbg_set_opt_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_save_all_irig_rx_settings( MBG_DEV_HANDLE dh, const void *p_obs, const IRIG_SETTINGS *p_irig_settings, const MBG_REF_OFFS *p_ref_offs, const MBG_OPT_SETTINGS *p_opt_settings ) ;

 /**
 * @brief Read the control function bits received from an incoming IRIG signal.
 *
 * This function fills an ::MBG_IRIG_CTRL_BITS structure with the control function
 * bits decoded from the incoming IRIG signal.
 *
 * The meaning of these bits depends on the selected time code format.
 *
 * In some IRIG formats these bits provide some well-known information which can
 * also be evaluated by the device. For example, in IEEE 1344 or IEEE C37.118 code
 * the control function bits are used to provide the year number, %UTC offset,
 * DST status, leap second warning, etc.
 *
 * For most IRIG code formats, however, these bits are reserved, i.e. not used
 * at all, or application defined, depending on the configuration of the IRIG
 * generator providing the IRIG signal.
 *
 * In the latter case the application has to evaluate the received control function
 * bits and can use this function to retrieve these bits from the receiver device.
 *
 * The API call ::mbg_chk_dev_has_irig_ctrl_bits checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_IRIG_CTRL_BITS type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_irig_ctrl_bits
 */
 _MBG_API_ATTR int _MBG_API mbg_get_irig_ctrl_bits( MBG_DEV_HANDLE dh, MBG_IRIG_CTRL_BITS *p ) ;

 /**
 * @brief Read raw IRIG data from an IRIG receiver.
 *
 * This function reads an ::MBG_RAW_IRIG_DATA structure with the raw data bits received
 * from the incoming IRIG signal. This enables an application itself to decode the
 * information provided by the IRIG signal.
 *
 * The API call ::mbg_chk_dev_has_raw_irig_data checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_RAW_IRIG_DATA type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_raw_irig_data
 * @see ::mbg_get_raw_irig_data_on_sec_change
 */
 _MBG_API_ATTR int _MBG_API mbg_get_raw_irig_data( MBG_DEV_HANDLE dh, MBG_RAW_IRIG_DATA *p ) ;

 /**
 * @brief Wait for second changeover then read raw IRIG data from an IRIG receiver.
 *
 * This function waits until the second of the on-board time rolls over,
 * and then reads the last recent raw IRIG data from the device.
 *
 * The API call ::mbg_chk_dev_has_raw_irig_data checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> The ::mbg_get_time_sec_change function called
 * by this function is supported on Windows only, so this function
 * can also be used on Windows only.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_RAW_IRIG_DATA type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_raw_irig_data
 * @see ::mbg_get_raw_irig_data
 * @see ::mbg_get_time_sec_change
 */
 _MBG_API_ATTR int _MBG_API mbg_get_raw_irig_data_on_sec_change( MBG_DEV_HANDLE dh, MBG_RAW_IRIG_DATA *p ) ;

 /**
 * @brief Read the IRIG time and day-of-year number from an IRIG receiver.
 *
 * Reads a ::PCPS_IRIG_TIME structure with the raw IRIG day-of-year number
 * and time decoded from the latest IRIG input frame. If the configured IRIG code
 * also contains the year number, the year number is also returned, otherwise
 * the returned year number is 0xFF.
 *
 * The API call ::mbg_chk_dev_has_irig_time checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_IRIG_TIME type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_irig_time
 */
 _MBG_API_ATTR int _MBG_API mbg_get_irig_time( MBG_DEV_HANDLE dh, PCPS_IRIG_TIME *p ) ;

 /**
 * @brief Clear the on-board time capture FIFO buffer.
 *
 * The API call ::mbg_chk_dev_can_clr_ucap_buff checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_can_clr_ucap_buff
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 */
 _MBG_API_ATTR int _MBG_API mbg_clr_ucap_buff( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Read information on the event capture buffer.
 *
 * Reads a ::PCPS_UCAP_ENTRIES structure with the number of user capture
 * events actually stored in the FIFO buffer, and the maximum number of
 * events that can be held by the buffer.
 *
 * The API call ::mbg_chk_dev_has_ucap checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_UCAP_ENTRIES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ucap
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ucap_entries( MBG_DEV_HANDLE dh, PCPS_UCAP_ENTRIES *p ) ;

 /**
 * @brief Retrieve a single time capture event from the on-board FIFO buffer.
 *
 * The capture event is returned in a ::PCPS_HR_TIME structure. The oldest entry
 * in the FIFO is retrieved and then removed from the FIFO.
 *
 * If no capture event is available in the FIFO buffer, both the seconds
 * and the fractions of the returned timestamp are 0.
 *
 * The API call ::mbg_chk_dev_has_ucap checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This call is very much faster than the older ::mbg_get_gps_ucap
 * call which is obsolete but still supported for compatibility with
 * older devices.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_HR_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ucap
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_clr_ucap_buff
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ucap_event( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p ) ;

 /**
 * @brief Read the time zone/daylight saving parameters.
 *
 * This function returns the time zone/daylight saving parameters
 * in a ::TZDL structure.
 *
 * The API call ::mbg_chk_dev_has_tzdl checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> In spite of the function name this call may also be
 * supported by non-GPS devices. Other devices may support the ::mbg_get_tzcode
 * or ::mbg_get_pcps_tzdl calls instead.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TZDL structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzdl
 * @see ::mbg_set_gps_tzdl
 * @see ::mbg_get_tzcode
 * @see ::mbg_get_pcps_tzdl
 * @see @ref group_tzdl
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_tzdl( MBG_DEV_HANDLE dh, TZDL *p ) ;

 /**
 * @brief Write the time zone/daylight saving parameters.
 *
 * This function writes the time zone/daylight saving parameters
 * in a ::TZDL structure to a device.
 *
 * The API call ::mbg_chk_dev_has_tzdl checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> In spite of the function name this call may also be
 * supported by non-GPS devices. Other devices may support the ::mbg_set_tzcode
 * or ::mbg_set_pcps_tzdl calls instead.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::TZDL structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzdl
 * @see ::mbg_get_gps_tzdl
 * @see ::mbg_set_tzcode
 * @see ::mbg_set_pcps_tzdl
 * @see @ref group_tzdl
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_tzdl( MBG_DEV_HANDLE dh, const TZDL *p ) ;

 /**
 * @brief Retrieve the software revision of a GPS receiver.
 *
 * @deprecated This function is deprecated.
 *
 * This function is deprecated, but still supported
 * for compatibility with older GPS devices. Normally
 * the software revision is part of the ::RECEIVER_INFO
 * structure. See ::mbg_setup_receiver_info which takes
 * care of the different options.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::SW_REV structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_chk_dev_is_gps
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_sw_rev( MBG_DEV_HANDLE dh, SW_REV *p ) ;

 /**
 * @brief Retrieve the status of the battery buffered GPS variables.
 *
 * GPS receivers require some navigational data set to be available
 * to be able to decode position and time accurately. This data set
 * is transmitted periodically by the satellites, so it can
 * simply be collected if it's not available.
 *
 * The ::BVAR_STAT type reports which parts of the data set are
 * available in the receiver, and which are not.
 *
 * If the available data set is not complete, the receiver
 * stays in COLD BOOT mode until all data have been received
 * and thus all data sets are valid.
 *
 * The API call ::mbg_chk_dev_has_bvar_stat checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::BVAR_STAT structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_bvar_stat
 * @see ::BVAR_FLAGS
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_bvar_stat( MBG_DEV_HANDLE dh, BVAR_STAT *p ) ;

 /**
 * @brief Read the current on-board time using a ::TTM structure.
 *
 * The API call ::mbg_chk_dev_has_ttm_time checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This API call is very slow, so ::mbg_get_hr_time or
 * ::mbg_get_fast_hr_timestamp or associated calls should be used preferably.
 *
 * <b>Note:</b> This function should be preferred over ::mbg_get_gps_time,
 * which has been deprecated.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TTM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ttm_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_fast_hr_timestamp
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ttm_time( MBG_DEV_HANDLE dh, TTM *p ) ;

 /**
 * @brief Read the current on-board time using a ::TTM structure.
 *
 * The API call ::mbg_chk_dev_has_ttm_time checks whether
 * this call is supported by a device.
 *
 * @deprecated This function is deprecated, use ::mbg_get_ttm_time preferably.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TTM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ttm_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_fast_hr_timestamp
 */
 _MBG_API_ATTR int _DEPRECATED_BY( "mbg_get_ttm_time" ) _MBG_API mbg_get_gps_time( MBG_DEV_HANDLE dh, TTM *p ) ;

 /**
 * @brief Set the time on a GPS receiver device.
 *
 * Write a ::TTM structure to a GPS receiver in order to set
 * the on-board date and time. Date and time must be local time
 * according to the time zone configuration (::TZDL) onboard
 * the device.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::TTM structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_time( MBG_DEV_HANDLE dh, const TTM *p ) ;

 /**
 * @brief Read a ::PORT_PARM structure with the serial port configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_get_serial_settings preferably.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This function is deprecated since it is only
 * supported by a certain class of devices and can handle only
 * up to 2 serial ports. The generic function ::mbg_get_serial_settings
 * should be used instead.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PORT_PARM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_port_parm( MBG_DEV_HANDLE dh, PORT_PARM *p ) ;

 /**
 * @brief Write a ::PORT_PARM structure to configure the on-board serial ports.
 *
 * @deprecated This function is deprecated, use ::mbg_save_serial_settings preferably.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This function is deprecated since it is only
 * supported by a certain class of devices and can handle only
 * up to 2 ports. The generic function ::mbg_save_serial_settings
 * should be used instead.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PORT_PARM structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_port_parm( MBG_DEV_HANDLE dh, const PORT_PARM *p ) ;

 /**
 * @brief Read an ::ANT_INFO structure to retrieve an extended GPS antenna status.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> Normally the current antenna connection status can also be
 * determined by evaluation of the ::PCPS_TIME::signal or ::PCPS_HR_TIME::signal
 * fields. The "disconnected" status reported by ::ANT_INFO disappears only if
 * the antenna has been reconnected <b>and</b> the receiver has synchronized
 * to the GPS satellites again.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::ANT_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_ant_info( MBG_DEV_HANDLE dh, ANT_INFO *p ) ;

 /**
 * @brief Read a time capture event from the on-board FIFO buffer using a ::TTM structure.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This call is pretty slow and has been obsoleted by
 * ::mbg_get_ucap_event which should be used preferably, if supported
 * by the device. Anyway, this call is still supported for compatibility
 * with older devices which don't support ::mbg_get_ucap_event.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TTM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 * @see ::mbg_clr_ucap_buff
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_ucap( MBG_DEV_HANDLE dh, TTM *p ) ;

 /**
 * @brief Read the ::ENABLE_FLAGS structure controlling when outputs are to be enabled.
 *
 * The ::ENABLE_FLAGS structure controls whether certain signal outputs
 * are to be enabled immediately after the device was powered up, or only
 * after the device has synchronized to its input signal.
 *
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Note:</b> Not all of the input signals specified for the
 * ::ENABLE_FLAGS structure can be modified individually.
 * See ::ENABLE_FLAGS_CODES.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::ENABLE_FLAGS structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::ENABLE_FLAGS
 * @see ::ENABLE_FLAGS_CODES
 * @see ::mbg_set_gps_enable_flags
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_enable_flags( MBG_DEV_HANDLE dh, ENABLE_FLAGS *p ) ;

 /**
 * @brief Write an ::ENABLE_FLAGS structure to configure when outputs shall be enabled.
 *
 * The ::ENABLE_FLAGS structure controls whether certain signal outputs
 * are to be enabled immediately after the device was powered up, or only
 * after the device has synchronized to its input signal.
 *
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Note:</b> Not all of the input signals specified for the
 * ::ENABLE_FLAGS structure can be modified individually.
 * See ::ENABLE_FLAGS_CODES.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p Pointer to a ENABLE_FLAGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::ENABLE_FLAGS
 * @see ::ENABLE_FLAGS_CODES
 * @see ::mbg_get_gps_enable_flags
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_enable_flags( MBG_DEV_HANDLE dh, const ENABLE_FLAGS *p ) ;

 /**
 * @brief Read the extended GPS receiver status from a device.
 *
 * The ::STAT_INFO structure reports the status of the GPS receiver,
 * including mode of operation and number of visible/usable satellites.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::STAT_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::STAT_INFO
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_stat_info( MBG_DEV_HANDLE dh, STAT_INFO *p ) ;

 /**
 * @brief Send one of the ::PC_GPS_COMMANDS to a GPS receiver device.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::GPS_CMD.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::PC_GPS_COMMANDS
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_cmd( MBG_DEV_HANDLE dh, const GPS_CMD *p ) ;

 /**
 * @brief Read the current geographic position from a GPS/GNSS device.
 *
 * The returned ::POS structure contains the current position in
 * ECEF (Earth Centered, Earth Fixed) kartesian coordinates, and in
 * geographic coordinates with different formats, using the WGS84
 * geographic datum.
 *
 * The API call ::mbg_chk_dev_has_rcvr_pos checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::POS structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pos_xyz
 * @see ::mbg_set_gps_pos_lla
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_pos( MBG_DEV_HANDLE dh, POS *p ) ;

 /**
 * @brief Set the GPS/GNSS receiver position using ::XYZ coordinates.
 *
 * The structure ::XYZ must specify the new position in ECEF
 * (Earth Centered, Earth Fixed) kartesian coordinates.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Position in ::XYZ format to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pos_lla
 * @see ::mbg_get_gps_pos
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_pos_xyz( MBG_DEV_HANDLE dh, const XYZ p ) ;

 /**
 * @brief Set the GPS/GNSS receiver position using ::LLA coordinates.
 *
 * The structure ::LLA must specify the new position as longitude,
 * latitude, and altitude, using the WGS84 geographic datum.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Position in ::LLA format to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pos_xyz
 * @see ::mbg_get_gps_pos
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_pos_lla( MBG_DEV_HANDLE dh, const LLA p ) ;

 /**
 * @brief Read the configured antenna cable length from a device.
 *
 * The antenna cable length parameter is used by GPS/GNSS receivers
 * to compensate the propagation delay of the RF signal over the antenna
 * cable, which is about 5 ns/m.
 *
 * The API call ::mbg_chk_dev_has_cab_len checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::ANT_CABLE_LEN structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_cab_len
 * @see ::mbg_set_gps_ant_cable_len
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_ant_cable_len( MBG_DEV_HANDLE dh, ANT_CABLE_LEN *p ) ;

 /**
 * @brief Write the GPS antenna cable length configuration to a device.
 *
 * The antenna cable length parameter is used by GPS/GNSS receivers
 * to compensate the propagation delay of the RF signal over the antenna
 * cable, which is about 5 ns/m.
 *
 * The API call ::mbg_chk_dev_has_cab_len checks whether
 * this call is supported by a device.
 *
 * @note Unfortunately, the maximum value accepted by a device
 * may vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value.
 * The function ::mbg_set_and_check_gps_ant_cable_len checks
 * whether the new value was accepted by the device, and therefore
 * should be used preferably.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::ANT_CABLE_LEN structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_cab_len
 * @see ::mbg_get_gps_ant_cable_len
 * @see ::mbg_set_and_check_gps_ant_cable_len
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_ant_cable_len( MBG_DEV_HANDLE dh, const ANT_CABLE_LEN *p ) ;

 /**
 * @brief Write and check the GPS antenna cable length.
 *
 * The antenna cable length parameter is used by GPS/GNSS receivers
 * to compensate the propagation delay of the RF signal over the antenna
 * cable, which is about 5 ns/m.
 *
 * The API call ::mbg_chk_dev_has_cab_len checks whether
 * this call is supported by a device.
 *
 * Unfortunately, the maximum value accepted by a device may
 * vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value:
 *
 * - Some devices truncate the new value to the maximum value that
 *   is hard-coded in the firmware, so that the effective value
 *   corresponds to the maximum value, but neither matches the
 *   original value, nor the desired new value. This case can only
 *   be determined if the optional parameter @p p_org_val is provided.
 *
 * - Some devices simply don't accept and store a value that is
 *   out of range, so the original value is left unchanged.
 *
 * @param[in]      dh         Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in,out]  p_new_val  Pointer to a ::ANT_CABLE_LEN variable to be written.
 * @param[in]      p_org_val  Optional pointer to a ::ANT_CABLE_LEN variable with
 *                            the original value read previously, may be @a NULL.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *         If the return code is ::MBG_ERR_CFG or ::MBG_ERR_CFG_TRUNC,
 *         the re-read value is stored in @p p_new_val for convenience.
 *
 * @see ::mbg_chk_dev_has_cab_len
 * @see ::mbg_set_gps_ant_cable_len
 * @see ::mbg_get_gps_ant_cable_len
 */
 _MBG_API_ATTR int _MBG_API mbg_set_and_check_gps_ant_cable_len( MBG_DEV_HANDLE dh, ANT_CABLE_LEN *p_new_val, const ANT_CABLE_LEN *p_org_val ) ;

 /**
 * @brief Read the ::RECEIVER_INFO structure from a device.
 *
 * The API call ::mbg_chk_dev_has_receiver_info checks
 * whether this call is supported by a device.
 *
 * <b>Note:</b> Applications should call ::mbg_setup_receiver_info
 * preferably, which also sets up a basic ::RECEIVER_INFO structure
 * for devices which don't provide that structure by themselves.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::RECEIVER_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_chk_dev_has_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_receiver_info( MBG_DEV_HANDLE dh, RECEIVER_INFO *p ) ;

 /**
 * @brief Read a ::STR_TYPE_INFO_IDX array of supported string types.
 *
 * A valid ::RECEIVER_INFO associated with the device
 * has to be passed to this function.
 *
 * <b>Note:</b> The function ::mbg_get_serial_settings should be used preferably
 * to get retrieve the current port settings and configuration options.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  stii  Pointer to a an array of string type information to be filled up.
 * @param[in]   p_ri  Pointer to the ::RECEIVER_INFO associated with the device. TODO Make this obsolete!
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 * @see ::mbg_get_gps_all_port_info
 * @see ::mbg_setup_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_str_type_info( MBG_DEV_HANDLE dh, STR_TYPE_INFO_IDX stii[], const RECEIVER_INFO *p_ri ) ;

 /**
 * @brief Read a ::PORT_INFO_IDX array of supported serial port configurations.
 *
 * A valid ::RECEIVER_INFO associated with the device
 * has to be passed to this function.
 *
 * <b>Note:</b> The function ::mbg_get_serial_settings should be used preferably
 * to get retrieve the current port settings and configuration options.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  pii   Pointer to a an array of port configuration information to be filled up.
 * @param[in]   p_ri  Pointer to the ::RECEIVER_INFO associated with the device. TODO Make this obsolete!
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 * @see ::mbg_get_gps_all_str_type_info
 * @see ::mbg_setup_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_port_info( MBG_DEV_HANDLE dh, PORT_INFO_IDX pii[], const RECEIVER_INFO *p_ri ) ;

 /**
 * @brief Write the configuration for a single serial port to a device.
 *
 * The ::PORT_SETTINGS_IDX structure contains both the ::PORT_SETTINGS
 * and the port index value. Except for the parameter types this call is
 * equivalent to ::mbg_set_gps_port_settings.
 *
 * The API call ::mbg_chk_dev_has_receiver_info checks
 * whether this call is supported by a device.
 *
 * <b>Note:</b> The function ::mbg_save_serial_settings should be used preferably
 * to write new port configuration to the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PORT_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 * @see ::mbg_set_gps_port_settings
 * @see ::mbg_chk_dev_has_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_port_settings_idx( MBG_DEV_HANDLE dh, const PORT_SETTINGS_IDX *p ) ;

 /**
 * @brief Write the configuration for a single serial port to a device.
 *
 * The ::PORT_SETTINGS structure does not contain the port index, so the
 * the port index must be given separately. Except for the parameter types
 * this call is equivalent to ::mbg_set_gps_port_settings_idx.
 *
 * The API call ::mbg_chk_dev_has_receiver_info checks
 * whether this call is supported by a device.
 *
 * <b>Note:</b> The function ::mbg_save_serial_settings should be used preferably
 * to write new port configuration to the device.
 *
 * @param[in]  dh   Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p    Pointer to a ::PORT_SETTINGS structure to be written.
 * @param[in]  idx  Index of the serial port to be configured (starting from 0).
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 * @see ::mbg_set_gps_port_settings_idx
 * @see ::mbg_chk_dev_has_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_port_settings( MBG_DEV_HANDLE dh, const PORT_SETTINGS *p, int idx ) ;

 /**
 * @brief Set up a ::RECEIVER_INFO structure for a device.
 *
 * If the device supports the ::RECEIVER_INFO structure, the structure
 * is read from the device, otherwise a structure is set up using
 * default values depending on the device type.
 *
 * Optionally, the function ::mbg_get_device_info may have been called
 * before, and the returned ::PCPS_DEV structure can be passed to this
 * function.
 *
 * If a @a NULL pointer is passed instead, the device info is retrieved
 * directly from the device, using the device handle.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   p_obs  Obsolete pointer kept for compatibility, should be @a NULL.
 * @param[out]  p      Pointer to a ::RECEIVER_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_device_info
 * @see ::mbg_chk_dev_has_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_setup_receiver_info( MBG_DEV_HANDLE dh, const void *p_obs, RECEIVER_INFO *p ) ;

 /**
 * @brief Read the version code of the on-board PCI/PCIe interface ASIC.
 *
 * The API call ::mbg_chk_dev_has_asic_version checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCI_ASIC_VERSION type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_asic_version
 */
 _MBG_API_ATTR int _MBG_API mbg_get_asic_version( MBG_DEV_HANDLE dh, PCI_ASIC_VERSION *p ) ;

 /**
 * @brief Read the features of the on-board PCI/PCIe interface ASIC.
 *
 * The API call ::mbg_chk_dev_has_asic_features checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCI_ASIC_FEATURES type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_asic_features
 */
 _MBG_API_ATTR int _MBG_API mbg_get_asic_features( MBG_DEV_HANDLE dh, PCI_ASIC_FEATURES *p ) ;

 /**
 * @brief Read the current time scale settings and which time scales are supported.
 *
 * The ::MBG_TIME_SCALE_INFO structure tells which time scale settings are supported
 * by a device, and which time scale is currently configured.
 *
 * The API call ::mbg_chk_dev_has_time_scale checks whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_has_time_scale.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_TIME_SCALE_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_time_scale_settings
 * @see ::mbg_chk_dev_has_time_scale
 */
 _MBG_API_ATTR int _MBG_API mbg_get_time_scale_info( MBG_DEV_HANDLE dh, MBG_TIME_SCALE_INFO *p ) ;

 /**
 * @brief Write the time scale configuration to a device.
 *
 * The ::MBG_TIME_SCALE_SETTINGS structure determines which time scale
 * is to be used for the time stamps which can be read from a device.
 *
 * The API call ::mbg_chk_dev_has_time_scale checks whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_has_time_scale.
 *
 * The function ::mbg_get_time_scale_info should have been called before
 * in order to determine which time scales are supported by the card.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::MBG_TIME_SCALE_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_time_scale_info
 * @see ::mbg_chk_dev_has_time_scale
 */
 _MBG_API_ATTR int _MBG_API mbg_set_time_scale_settings( MBG_DEV_HANDLE dh, const MBG_TIME_SCALE_SETTINGS *p ) ;

 /**
 * @brief Read a ::UTC parameter structure from a device.
 *
 * The API call ::mbg_chk_dev_has_utc_parm checks whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_has_utc_parm.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::UTC structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_utc_parm
 * @see ::mbg_set_utc_parm
 */
 _MBG_API_ATTR int _MBG_API mbg_get_utc_parm( MBG_DEV_HANDLE dh, UTC *p ) ;

 /**
 * @brief Write a ::UTC parameter structure to a device.
 *
 * This should only be done for testing, or if a device is operated
 * in always freewheeling mode. If the receiver is tracking any satellites,
 * the settings written to the device are overwritten by the parameters
 * broadcast by the satellites.
 *
 * The API call ::mbg_chk_dev_has_utc_parm checks whether
 * this call is supported by a device.
 *
 * See also the notes for mbg_chk_dev_has_utc_parm.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a valid ::UTC structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_utc_parm
 * @see ::mbg_get_utc_parm
 */
 _MBG_API_ATTR int _MBG_API mbg_set_utc_parm( MBG_DEV_HANDLE dh, const UTC *p ) ;

 /**
 * @brief Read the current time plus the associated PC cycles from a device.
 *
 * The ::PCPS_TIME_CYCLES structure contains a ::PCPS_TIME structure
 * and a PC cycles counter value which can be used to compensate the latency
 * of the call, i.e. the program execution time until the time stamp has actually
 * been read from the device.
 *
 * This call is supported for any card, similar to ::mbg_get_time. However,
 * the ::mbg_get_hr_time_cycles call should be used preferably if supported by
 * the device since that call provides much better accuracy than this one.
 *
 * The cycles counter value corresponds to a value returned by @a QueryPerformanceCounter
 * on Windows, and @a get_cycles on Linux. On operating systems or targets which don't
 * provide a cycles counter the returned cycles value is always 0.
 *
 * Applications should first pick up their own cycles counter value and then call
 * this function. The difference of the cycles counter values corresponds to the
 * latency of the call in units of the cycles counter clock frequency, e.g. as reported
 * by @a QueryPerformanceFrequency on Windows.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TIME_CYCLES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_legacy_time_fncs
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_time
 */
 _MBG_API_ATTR int _MBG_API mbg_get_time_cycles( MBG_DEV_HANDLE dh, PCPS_TIME_CYCLES *p ) ;

 /**
 * @brief Read the current high resolution time plus the associated PC cycles from a device.
 *
 * The returned ::PCPS_HR_TIME_CYCLES structure contains a ::PCPS_HR_TIME
 * structure and a PC cycles counter value which can be used to compensate
 * the latency of the call, i.e. the program execution time until the time stamp
 * has actually been read from the device.
 *
 * The API call ::mbg_chk_dev_has_hr_time checks whether
 * this call is supported by the device.
 *
 * For details see @ref ::mbgdevio_hr_time_fncs
 *
 * The cycles counter value corresponds to a value returned by @a QueryPerformanceCounter
 * on Windows, and @a get_cycles on Linux. On operating systems or targets which don't
 * provide a cycles counter the returned cycles value is always 0.
 *
 * Applications should first pick up their own cycles counter value and then call
 * this function. The difference of the cycles counter values corresponds to the
 * latency of the call in units of the cycles counter clock frequency, e.g.task as reported
 * by @a QueryPerformanceFrequency on Windows.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_HR_TIME_CYCLES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_hr_time_fncs
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_fast_timestamp_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_get_hr_time_cycles( MBG_DEV_HANDLE dh, PCPS_HR_TIME_CYCLES *p ) ;

 /**
 * @brief Read the current high resolution time, and compensate the latency of the call.
 *
 * Read a ::PCPS_HR_TIME structure plus cycles counter value, and correct the
 * time stamp for the latency of the call as described for ::mbg_get_hr_time_cycles,
 * then return the compensated time stamp, and optionally the latency.
 *
 * The API call ::mbg_chk_dev_has_hr_time checks whether
 * this call is supported by the device.
 *
 * For details see @ref ::mbgdevio_hr_time_fncs.
 *
 * The cycles counter value corresponds to a value returned by @a QueryPerformanceCounter
 * on Windows, and @a get_cycles on Linux. On operating systems or targets which don't
 * provide a cycles counter the returned cycles value is always 0.
 *
 * Applications should first pick up their own cycles counter value and then call
 * this function. The difference of the cycles counter values corresponds to the
 * latency of the call in units of the cycles counter clock frequency, e.g. as reported
 * by @a QueryPerformanceFrequency on Windows.
 *
 * @param[in]  dh           Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p            Pointer to a ::PCPS_HR_TIME structure to be filled up.
 * @param[out] hns_latency  Optional pointer to an @a int32_t value to return
 *                          the latency in 100ns units, or @a NULL, if not used.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_hr_time_fncs
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_fast_timestamp_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_get_hr_time_comp( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p, int32_t *hns_latency ) ;

 /**
 * @brief Read the current IRIG output settings plus the supported settings.
 *
 * The returned ::IRIG_INFO structure contains the configuration of an IRIG output
 * plus the possible settings supported by that output.
 *
 * The API call ::mbg_chk_dev_has_irig_tx checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::IRIG_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_irig_tx_settings
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
 _MBG_API_ATTR int _MBG_API mbg_get_irig_tx_info( MBG_DEV_HANDLE dh, IRIG_INFO *p ) ;

 /**
 * @brief Write an ::IRIG_SETTINGS structure to a device to configure the IRIG output.
 *
 * The API call ::mbg_chk_dev_has_irig_tx checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to an ::IRIG_INFO structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_irig_tx_info
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
 _MBG_API_ATTR int _MBG_API mbg_set_irig_tx_settings( MBG_DEV_HANDLE dh, const IRIG_SETTINGS *p ) ;

 /**
 * @brief Read the current frequency synthesizer settings from a device.
 *
 * Read a ::SYNTH structure containing the configuration of an optional
 * on-board programmable frequency synthesizer.
 *
 * The API call ::mbg_chk_dev_has_synth checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::SYNTH structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_synth
 * @see ::mbg_set_synth
 * @see ::mbg_get_synth_state
 * @see @ref group_synth
 */
 _MBG_API_ATTR int _MBG_API mbg_get_synth( MBG_DEV_HANDLE dh, SYNTH *p ) ;

 /**
 * @brief Write frequency synthesizer configuration settings to a device.
 *
 * Write a ::SYNTH structure containing the configuration of an optional
 * on-board programmable frequency synthesizer.
 *
 * The API call ::mbg_chk_dev_has_synth checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::SYNTH structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_synth
 * @see ::mbg_get_synth
 * @see ::mbg_get_synth_state
 * @see @ref group_synth
 */
 _MBG_API_ATTR int _MBG_API mbg_set_synth( MBG_DEV_HANDLE dh, const SYNTH *p ) ;

 /**
 * @brief Read the current status of the on-board frequency synthesizer.
 *
 * The API call ::mbg_chk_dev_has_synth checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::SYNTH_STATE structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_synth
 * @see ::mbg_get_synth
 * @see ::mbg_set_synth
 * @see @ref group_synth
 */
 _MBG_API_ATTR int _MBG_API mbg_get_synth_state( MBG_DEV_HANDLE dh, SYNTH_STATE *p ) ;

 /**
 * @brief Read a high resolution ::PCPS_TIME_STAMP_CYCLES structure via memory mapped access.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TIME_STAMP_CYCLES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_fast_timestamp_fncs
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp_comp
 * @see ::mbg_get_fast_hr_timestamp
 * @see @ref mbgdevio_fast_timestamp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_get_fast_hr_timestamp_cycles( MBG_DEV_HANDLE dh, PCPS_TIME_STAMP_CYCLES *p ) ;

 /**
 * @brief Read a high resolution timestamp and compensate the latency of the call.
 *
 * The retrieved ::PCPS_TIME_STAMP is read from memory mapped registers,
 * before it is returned, it is compensated for the latency of the call.
 *
 * @param[in]   dh           Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p            Pointer to a ::PCPS_TIME_STAMP structure to be filled up.
 * @param[out]  hns_latency  Optionally receives the latency in hectonanoseconds. TODO Check if hns.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_fast_timestamp_fncs
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp_cycles
 * @see ::mbg_get_fast_hr_timestamp
 * @see @ref mbgdevio_fast_timestamp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_get_fast_hr_timestamp_comp( MBG_DEV_HANDLE dh, PCPS_TIME_STAMP *p, int32_t *hns_latency ) ;

 /**
 * @brief Read a high resolution ::PCPS_TIME_STAMP structure via memory mapped access.
 *
 * This function does not return or evaluate a cycles count, so the latency
 * of the call can not be determined. However, depending on the timer hardware
 * used as cycles counter it may take quite some time to read the cycles count
 * on some hardware architectures, so this call can be used to yield lower
 * latencies, under the restriction to be unable to determine the exact latency.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TIME_STAMP structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_fast_timestamp_fncs
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp_comp
 * @see ::mbg_get_fast_hr_timestamp_cycles
 * @see @ref mbgdevio_fast_timestamp_fncs
 */
 _MBG_API_ATTR int _MBG_API mbg_get_fast_hr_timestamp( MBG_DEV_HANDLE dh, PCPS_TIME_STAMP *p ) ;

 /**
 * @brief Read current configuraton and features provided by the programmable pulse outputs.
 *
 * Read a ::POUT_INFO_IDX array of current settings and configuration
 * options of the programmable pulse outputs of a device.
 *
 * A valid ::RECEIVER_INFO associated with the device
 * has to be passed to this function.
 *
 * The function should only be called if the ::RECEIVER_INFO::n_prg_out
 * field (i.e. the number of programmable outputs of the device) is not 0.
 *
 * The array passed to this function to receive the returned data
 * must be able to hold at least ::RECEIVER_INFO::n_prg_out elements.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  pii   Pointer to a an array of ::POUT_INFO_IDX structures to be filled up.
 * @param[in]   p_ri  Pointer to the ::RECEIVER_INFO associated with the device. TODO Make this obsolete!
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pout_settings_idx
 * @see ::mbg_set_gps_pout_settings
 * @see ::mbg_setup_receiver_info
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_pout_info( MBG_DEV_HANDLE dh, POUT_INFO_IDX pii[], const RECEIVER_INFO *p_ri ) ;

 /**
 * @brief Write the configuration for a single programmable pulse output.
 *
 * The ::POUT_SETTINGS_IDX structure contains both the ::POUT_SETTINGS
 * and the output index value. Except for the parameter types this call
 * is equivalent to ::mbg_set_gps_pout_settings.
 *
 * The function should only be called if the ::RECEIVER_INFO::n_prg_out field
 * (i.e. the number of programmable outputs of the device) is not 0, and the
 * output index value must be in the range 0..::RECEIVER_INFO::n_prg_out.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::POUT_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_all_pout_info
 * @see ::mbg_set_gps_pout_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_pout_settings_idx( MBG_DEV_HANDLE dh, const POUT_SETTINGS_IDX *p ) ;

 /**
 * @brief Write the configuration for a single programmable pulse output.
 *
 * The ::POUT_SETTINGS structure does not contain the index of the
 * programmable output to be configured, so the index must explicitly
 * be passed to this function. Except for the parameter types this call
 * is equivalent to ::mbg_set_gps_pout_settings_idx.
 *
 * The function should only be called if the ::RECEIVER_INFO::n_prg_out field
 * (i.e. the number of programmable outputs of the device) is not 0, and the
 * output index value must be in the range 0..::RECEIVER_INFO::n_prg_out.
 *
 * @param[in]  dh   Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p    Pointer to a ::POUT_SETTINGS structure to be written.
 * @param[in]  idx  Index of the programmable pulse output to be configured (starting from 0 ).
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_all_pout_info
 * @see ::mbg_set_gps_pout_settings_idx
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_pout_settings( MBG_DEV_HANDLE dh, const POUT_SETTINGS *p, int idx ) ;

 /**
 * @brief Read the IRQ status of a device.
 *
 * IRQ status information includes flags indicating whether IRQs are
 * actually enabled, and whether IRQ support by a device is possibly
 * unsafe due to the firmware and interface chip version.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_IRQ_STAT_INFO variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see @ref PCPS_IRQ_STAT_INFO_DEFS
 */
 _MBG_API_ATTR int _MBG_API mbg_get_irq_stat_info( MBG_DEV_HANDLE dh, PCPS_IRQ_STAT_INFO *p ) ;

 /**
 * @brief Read LAN interface information from a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::LAN_IF_INFO variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_ip4_state
 * @see ::mbg_get_ip4_settings
 * @see ::mbg_set_ip4_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_get_lan_if_info( MBG_DEV_HANDLE dh, LAN_IF_INFO *p ) ;

 /**
 * @brief Read LAN IPv4 state from a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::IP4_SETTINGS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_settings
 * @see ::mbg_set_ip4_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ip4_state( MBG_DEV_HANDLE dh, IP4_SETTINGS *p ) ;

 /**
 * @brief Read LAN IPv4 settings from a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::IP4_SETTINGS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_state
 * @see ::mbg_set_ip4_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ip4_settings( MBG_DEV_HANDLE dh, IP4_SETTINGS *p ) ;

 /**
 * @brief Write LAN IPv4 settings to a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   ::IP4_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_settings
 */
 _MBG_API_ATTR int _MBG_API mbg_set_ip4_settings( MBG_DEV_HANDLE dh, const IP4_SETTINGS *p ) ;

 /**
 * @brief Read PTP/IEEE1588 status from a device.
 *
 * The API call ::mbg_chk_dev_has_ptp checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PTP_STATE variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ptp_state( MBG_DEV_HANDLE dh, PTP_STATE *p ) ;

 /**
 * @brief Read PTP/IEEE1588 config info and current settings from a device.
 *
 * The API call ::mbg_chk_dev_has_ptp checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PTP_CFG_INFO variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_state
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ptp_cfg_info( MBG_DEV_HANDLE dh, PTP_CFG_INFO *p ) ;

 /**
 * @brief Write PTP/IEEE1588 configuration settings to a device.
 *
 * The API call ::mbg_chk_dev_has_ptp checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   ::PTP_CFG_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_state
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
 _MBG_API_ATTR int _MBG_API mbg_set_ptp_cfg_settings( MBG_DEV_HANDLE dh, const PTP_CFG_SETTINGS *p ) ;

 /**
 * @brief Read PTP/IEEE1588 unicast master configuration limits from a device.
 *
 * The API call ::mbg_chk_dev_has_ptp_unicast checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PTP_UC_MASTER_CFG_LIMITS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp_unicast
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_get_ptp_state
 */
 _MBG_API_ATTR int _MBG_API mbg_get_ptp_uc_master_cfg_limits( MBG_DEV_HANDLE dh, PTP_UC_MASTER_CFG_LIMITS *p ) ;

 /**
 * @brief Read PTP Unicast master settings and configuration options.
 *
 * The array passed to this function to receive the returned data
 * must be able to hold at least ::PTP_UC_MASTER_CFG_LIMITS::n_supp_master elements.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  pii     Pointer to a an array of ::PTP_UC_MASTER_INFO_IDX structures to be filled up.
 * @param[in]   p_umsl  Pointer to a ::PTP_UC_MASTER_CFG_LIMITS structure returned by ::mbg_get_ptp_uc_master_cfg_limits.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp_unicast
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_get_ptp_state
 */
 _MBG_API_ATTR int _MBG_API mbg_get_all_ptp_uc_master_info( MBG_DEV_HANDLE dh, PTP_UC_MASTER_INFO_IDX pii[], const PTP_UC_MASTER_CFG_LIMITS *p_umsl ) ;

 /**
 * @brief Write PTP/IEEE1588 unicast configuration settings to a device.
 *
 * The API call ::mbg_chk_dev_has_ptp_unicast checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   ::PTP_UC_MASTER_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp_unicast
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_get_ptp_state
 */
 _MBG_API_ATTR int _MBG_API mbg_set_ptp_uc_master_settings_idx( MBG_DEV_HANDLE dh, const PTP_UC_MASTER_SETTINGS_IDX *p ) ;

 /**
 * @brief Read both system time and associated device time from the kernel driver.
 *
 * The kernel driver reads the current system time plus a HR time structure
 * from a device immediately after each other. The returned info structure also
 * contains some cycles counts to be able to determine the execution times
 * required to read those time stamps.
 *
 * The advantage of this call compared to ::mbg_get_time_info_tstamp is
 * that this call also returns the status of the device. On the other hand, reading
 * the HR time from the device may block e.g. if another application accesses
 * the device.
 *
 * This call makes a ::mbg_get_hr_time_cycles call internally so the API call
 * ::mbg_chk_dev_has_hr_time can be used to check whether this call is supported
 * by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::MBG_TIME_INFO_HRT structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_time_info_tstamp
 */
 _MBG_API_ATTR int _MBG_API mbg_get_time_info_hrt( MBG_DEV_HANDLE dh, MBG_TIME_INFO_HRT *p ) ;

 /**
 * @brief Read both system time and associated device timestamp from the kernel driver.
 *
 * This call is similar to ::mbg_get_time_info_hrt except that a
 * ::mbg_get_fast_hr_timestamp_cycles call is made internally, so
 * the API call ::mbg_chk_dev_has_fast_hr_timestamp can be used to check whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_TIME_INFO_TSTAMP structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_time_info_hrt
 */
 _MBG_API_ATTR int _MBG_API mbg_get_time_info_tstamp( MBG_DEV_HANDLE dh, MBG_TIME_INFO_TSTAMP *p ) ;

 /**
 * @brief Read PZF correlation info from a device.
 *
 * The API call ::mbg_chk_dev_has_corr_info checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::CORR_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_chk_dev_has_corr_info
 */
 _MBG_API_ATTR int _MBG_API mbg_get_corr_info( MBG_DEV_HANDLE dh, CORR_INFO *p ) ;

 /**
 * @brief Read configurable "distance from transmitter" parameter from a device.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, mostly with long wave receivers.
 *
 * The API call ::mbg_chk_dev_has_tr_distance checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TR_DISTANCE variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_set_tr_distance
 */
 _MBG_API_ATTR int _MBG_API mbg_get_tr_distance( MBG_DEV_HANDLE dh, TR_DISTANCE *p ) ;

 /**
 * @brief Write configurable "distance from transmitter" parameter to a device.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, mostly with long wave receivers.
 *
 * The API call ::mbg_chk_dev_has_tr_distance checks whether
 * this call is supported by a device.
 *
 * @note Unfortunately, the maximum value accepted by a device
 * may vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value.
 * The function ::mbg_set_and_check_tr_distance checks whether
 * the new value was accepted by the device, and therefore
 * should be used preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::TR_DISTANCE variable to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_get_tr_distance
 * @see ::mbg_set_and_check_tr_distance
 */
 _MBG_API_ATTR int _MBG_API mbg_set_tr_distance( MBG_DEV_HANDLE dh, const TR_DISTANCE *p ) ;

 /**
 * @brief Write and check the configurable "distance from transmitter" parameter.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, usually in long wave receivers.
 *
 * The API call ::mbg_chk_dev_has_tr_distance checks whether
 * this call is supported by a device.
 *
 * Unfortunately, the maximum value accepted by a device may
 * vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value:
 *
 * - Some devices truncate the new value to the maximum value that
 *   is hard-coded in the firmware, so that the effective value
 *   corresponds to the maximum value, but neither matches the
 *   original value, nor the desired new value. This case can only
 *   be determined if the optional parameter @p p_org_val is provided.
 *
 * - Some devices simply don't accept and store a value that is
 *   out of range, so the original value is left unchanged.
 *
 * @param[in]      dh         Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in,out]  p_new_val  Pointer to a ::TR_DISTANCE variable to be written.
 * @param[in]      p_org_val  Optional pointer to a ::TR_DISTANCE variable with
 *                            the original value read previously, may be @a NULL.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *         If the return code is ::MBG_ERR_CFG or ::MBG_ERR_CFG_TRUNC,
 *         the re-read value is stored in @p p_new_val for convenience.
 *
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_set_tr_distance
 * @see ::mbg_get_tr_distance
 */
 _MBG_API_ATTR int _MBG_API mbg_set_and_check_tr_distance( MBG_DEV_HANDLE dh, TR_DISTANCE *p_new_val, const TR_DISTANCE *p_org_val ) ;

 /**
 * @brief Read a debug status word from a device.
 *
 * This is mainly supported by IRIG timecode receivers, and the status
 * word is intended to provide more detailed information why a device
 * might not synchronize to an incoming timecode signal.
 *
 * See ::MBG_DEBUG_STATUS and related definitions for details.
 *
 * The API call ::mbg_chk_dev_has_debug_status checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_DEBUG_STATUS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_debug_status
 */
 _MBG_API_ATTR int _MBG_API mbg_get_debug_status( MBG_DEV_HANDLE dh, MBG_DEBUG_STATUS *p ) ;

 /**
 * @brief Clear the event log buffer onboard a device.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_first_evt_log_entry
 * @see ::mbg_get_next_evt_log_entry
 */
 _MBG_API_ATTR int _MBG_API mbg_clr_evt_log( MBG_DEV_HANDLE dh ) ;

 /**
 * @brief Read details about the event log buffer onboard a device.
 *
 * The returned structure ::MBG_NUM_EVT_LOG_ENTRIES indicates how many
 * event log entries can be stored on the device, and how many entries
 * are actually stored.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_NUM_EVT_LOG_ENTRIES variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_first_evt_log_entry
 * @see ::mbg_get_next_evt_log_entry
 */
 _MBG_API_ATTR int _MBG_API mbg_get_num_evt_log_entries( MBG_DEV_HANDLE dh, MBG_NUM_EVT_LOG_ENTRIES *p ) ;

 /**
 * @brief Read the first (oldest) event log entry from a device.
 *
 * @note Subsequent reads should be made using ::mbg_get_next_evt_log_entry.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * If no (more) event log entry is available onboard the device,
 * the returned MBG_EVT_LOG_ENTRY::code is MBG_EVT_ID_NONE.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_EVT_LOG_ENTRY variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_next_evt_log_entry
 */
 _MBG_API_ATTR int _MBG_API mbg_get_first_evt_log_entry( MBG_DEV_HANDLE dh, MBG_EVT_LOG_ENTRY *p ) ;

 /**
 * @brief Read the next event log entry from a device.
 *
 * @note The first read should be made using ::mbg_get_first_evt_log_entry
 * to set the on-board read index to the oldest entry.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * If no (more) event log entry is available onboard the device,
 * the returned MBG_EVT_LOG_ENTRY::code is MBG_EVT_ID_NONE.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_EVT_LOG_ENTRY variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_first_evt_log_entry
 */
 _MBG_API_ATTR int _MBG_API mbg_get_next_evt_log_entry( MBG_DEV_HANDLE dh, MBG_EVT_LOG_ENTRY *p ) ;

 /**
 * @brief Read the current GNSS mode info including current settings.
 *
 * The ::MBG_GNSS_MODE_INFO structure tells which GNSS systems are supported
 * by a device, and also includes the settings currently in effect.
 *
 * The API call ::mbg_chk_dev_is_gnss can be used to check whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_is_gnss.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p_mi  Pointer to a ::MBG_GNSS_MODE_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_gnss_mode_settings
 * @see ::mbg_get_gps_all_gnss_sat_info
 * @see ::mbg_chk_dev_is_gnss
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_gnss_mode_info( MBG_DEV_HANDLE dh, MBG_GNSS_MODE_INFO *p_mi ) ;

 /**
 * @brief Write the GNSS mode configuration to a device.
 *
 * The ::MBG_GNSS_MODE_SETTINGS structure determines the GNSS settings
 * for a device, e.g. which satellite systems have to be used.
 *
 * The function ::mbg_get_gps_gnss_mode_info should have been called before
 * to determine which GNSS settings are supported by the device.
 *
 * The API call ::mbg_chk_dev_is_gnss can be used to check whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_is_gnss.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p_ms  Pointer to a ::MBG_GNSS_MODE_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_get_gps_all_gnss_sat_info
 * @see ::mbg_chk_dev_is_gnss
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_gnss_mode_settings( MBG_DEV_HANDLE dh, const MBG_GNSS_MODE_SETTINGS *p_ms ) ;

 /**
 * @brief Read a ::GNSS_SAT_INFO_IDX array of satellite status information.
 *
 * The function ::mbg_get_gps_gnss_mode_info must have been called before,
 * and the returned ::MBG_GNSS_MODE_INFO structure be passed to this function.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  gsii  Pointer to a an array of satellite info structures to be filled up.
 * @param[in]   p_mi  Pointer to a ::MBG_GNSS_MODE_INFO structure returned by ::mbg_get_gps_gnss_mode_info.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_set_gps_gnss_mode_settings
 * @see ::mbg_chk_dev_is_gnss
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_gnss_sat_info( MBG_DEV_HANDLE dh, GNSS_SAT_INFO_IDX gsii[], const MBG_GNSS_MODE_INFO *p_mi ) ;

 /**
 * @brief Read common GPIO configuration limits.
 *
 * The API call ::mbg_chk_dev_has_gpio checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::MBG_GPIO_CFG_LIMITS structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gpio_cfg_limits( MBG_DEV_HANDLE dh, MBG_GPIO_CFG_LIMITS *p ) ;

 /**
 * @brief Get all GPIO settings and capabilities.
 *
 * The API call ::mbg_chk_dev_has_gpio checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  gii    An array of ::MBG_GPIO_STATUS_IDX structures to be filled up.
 * @param[in]   p_gcl  Pointer to a ::MBG_GPIO_CFG_LIMITS structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_gpio_info( MBG_DEV_HANDLE dh, MBG_GPIO_INFO_IDX gii[], const MBG_GPIO_CFG_LIMITS *p_gcl ) ;

 /**
 * @brief Write the configuration for a single GPIO port to a device.
 *
 * The ::MBG_GPIO_SETTINGS_IDX structure contains both the ::MBG_GPIO_SETTINGS
 * and the port index value.
 *
 * The API call ::mbg_chk_dev_has_gpio checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_GPIO_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_gpio_settings_idx( MBG_DEV_HANDLE dh, const MBG_GPIO_SETTINGS_IDX *p ) ;

 /**
 * @brief Read the status of all GPIO signal ports.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  gsi    An array of ::MBG_GPIO_STATUS_IDX structures to be filled up.
 * @param[in]   p_gcl  Pointer to a ::MBG_GPIO_CFG_LIMITS structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_gpio_status( MBG_DEV_HANDLE dh, MBG_GPIO_STATUS_IDX gsi[], const MBG_GPIO_CFG_LIMITS *p_gcl ) ;

 /**
 * @brief Read ::XMULTI_REF_INSTANCES.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::XMULTI_REF_INSTANCES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
 _MBG_API_ATTR int _MBG_API mbg_get_xmr_instances( MBG_DEV_HANDLE dh, XMULTI_REF_INSTANCES *p ) ;

 /**
 * @brief Read the status of all XMR sources.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  xmrsi   An array of ::XMULTI_REF_STATUS_IDX structures to be filled up.
 * @param[in]   p_xmri  Pointer to a ::XMULTI_REF_INSTANCES structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_xmr_status( MBG_DEV_HANDLE dh, XMULTI_REF_STATUS_IDX xmrsi[], const XMULTI_REF_INSTANCES *p_xmri ) ;

 /**
 * @brief Read all XMR settings and capabilities.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  xmrii   An array of ::XMULTI_REF_INFO_IDX structures to be filled up.
 * @param[in]   p_xmri  Pointer to a ::XMULTI_REF_INSTANCES structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
 _MBG_API_ATTR int _MBG_API mbg_get_gps_all_xmr_info( MBG_DEV_HANDLE dh, XMULTI_REF_INFO_IDX xmrii[], const XMULTI_REF_INSTANCES *p_xmri ) ;

 /**
 * @brief Write a single XMR setting to a device.
 *
 * The ::XMULTI_REF_SETTINGS_IDX structure contains both the ::XMULTI_REF_SETTINGS
 * and the index value.
 *
 * The API call ::mbg_chk_dev_has_xmr checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   p   Pointer to a ::XMULTI_REF_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_get_xmr_holdover_status
 */
 _MBG_API_ATTR int _MBG_API mbg_set_gps_xmr_settings_idx( MBG_DEV_HANDLE dh, const XMULTI_REF_SETTINGS_IDX *p ) ;

 /**
 * @brief Read the current XMR holdover interval from a device.
 *
 * The API call ::mbg_chk_dev_has_xmr checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p       Pointer to a ::XMR_HOLDOVER_INTV structure to be filled up.
 * @param[in]   p_xmri  Pointer to a ::XMULTI_REF_INSTANCES structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 */
 _MBG_API_ATTR int _MBG_API mbg_get_xmr_holdover_status( MBG_DEV_HANDLE dh, XMR_HOLDOVER_STATUS *p, const XMULTI_REF_INSTANCES *p_xmri ) ;

 /**
 * @brief Read the CPU affinity of a process.
 *
 * This means on which of the available CPUs or CPU cores
 * a process may be executed.
 *
 * @param[in]   pid  The process ID.
 * @param[out]  p    Pointer to a ::MBG_CPU_SET variable which contains a mask of CPUs.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_process_affinity
 * @see ::mbg_set_current_process_affinity_to_cpu
 */
 _MBG_API_ATTR int _MBG_API mbg_get_process_affinity( MBG_PROCESS_ID pid, MBG_CPU_SET *p ) ;

 /**
 * @brief Set the CPU affinity of a process.
 *
 * This determines on which of the available CPUs
 * or CPU cores the process is allowed to be executed.
 *
 * @param[in]   pid  The process ID.
 * @param[out]  p    Pointer to a ::MBG_CPU_SET variable which contains a mask of CPUs.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_process_affinity
 * @see ::mbg_set_current_process_affinity_to_cpu
 */
 _MBG_API_ATTR int _MBG_API mbg_set_process_affinity( MBG_PROCESS_ID pid, MBG_CPU_SET *p ) ;

 /**
 * @brief Set the CPU affinity of a process for a single CPU only.
 *
 * This means the process may only be executed on that single CPU.
 *
 * @param[in]  cpu_num  The number of the CPU.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_process_affinity
 * @see ::mbg_set_process_affinity
 */
 _MBG_API_ATTR int _MBG_API mbg_set_current_process_affinity_to_cpu( int cpu_num ) ;

 /**
 * @brief Create a new execution thread for the current process.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in]  p_ti  Pointer to a ::MBG_THREAD_INFO structure to be filled up.
 * @param[in]  fnc   The name of the thread function to be started.
 * @param[in]  arg   A generic argument passed to the thread function.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_stop
 * @see ::mbg_thread_sleep_interruptible
 * @see ::mbg_thread_set_affinity
 */
 _MBG_API_ATTR int _MBG_API mbg_thread_create( MBG_THREAD_INFO *p_ti, MBG_THREAD_FNC fnc, void *arg ) ;

 /**
 * @brief Stop a thread which has been created by ::mbg_thread_create.
 *
 * Wait until the thread has finished and release all resources.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p_ti  Pointer to a ::MBG_THREAD_INFO structure associated with the thread.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_create
 * @see ::mbg_thread_sleep_interruptible
 * @see ::mbg_thread_set_affinity
 */
 _MBG_API_ATTR int _MBG_API mbg_thread_stop( MBG_THREAD_INFO *p_ti ) ;

 /**
 * @brief Let the current thread sleep for a certain interval.
 *
 * The sleep is interrupted if a signal is received indicating
 * the thread should terminate.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p_ti      Pointer to a ::MBG_THREAD_INFO structure associated with the thread.
 * @param[in]      sleep_ms  The number of milliseconds to sleep.
 *
 * @return ::MBG_SUCCESS   if the sleep interval has expired normally,<br>
 *         ::MBG_ERR_INTR  if a signal to terminate has been received,<br>
 *         or one of the other @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_create
 * @see ::mbg_thread_stop
 * @see ::mbg_thread_set_affinity
 */
 _MBG_API_ATTR int _MBG_API mbg_thread_sleep_interruptible( MBG_THREAD_INFO *p_ti, ulong sleep_ms ) ;

 /**
 * @brief Set the CPU affinity of a single thread.
 *
 * This determines on which of the available CPUs the thread
 * is allowed to be executed.
 *
 * This function is only implemented for targets that support thread affinity.
 *
 * @param[in,out]  p_ti  Pointer to a ::MBG_THREAD_INFO structure associated with the thread.
 * @param[in]      p     Pointer to a ::MBG_CPU_SET variable which contains a mask of CPUs.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_create
 * @see ::mbg_thread_stop
 * @see ::mbg_thread_sleep_interruptible
 */
 _MBG_API_ATTR int _MBG_API mbg_thread_set_affinity( MBG_THREAD_INFO *p_ti, MBG_CPU_SET *p ) ;

 /**
 * @brief Set up a ::MBG_POLL_THREAD_INFO structure and start a new thread.
 *
 * The new thread runs a function which periodically reads
 * a time stamp / cycles pair from a device.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p_pti     Pointer to a ::MBG_POLL_THREAD_INFO structure.
 * @param[in]      dh the    Handle of the device to be polled.
 * @param[in]      freq_hz   The initial cycles frequency, if known, in Hz.
 * @param[in]      sleep_ms  the sleep interval for the poll thread function in ms.
 *                           If this parameter is 0, the default sleep interval is used.
 *
 * @return ::MBG_SUCCESS on success,<br>
 *         ::MBG_ERR_NOT_SUPP_BY_DEV if the device to poll does not support HR time,<br>
 *         else the result of ::mbg_thread_create.
 *
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 */
 _MBG_API_ATTR int _MBG_API mbg_xhrt_poll_thread_create( MBG_POLL_THREAD_INFO *p_pti, MBG_DEV_HANDLE dh, MBG_PC_CYCLES_FREQUENCY freq_hz, int sleep_ms ) ;

 /**
 * @brief Stop a polling thread started by ::mbg_xhrt_poll_thread_create.
 *
 * This call also releases all associated resources.
 *
 * @param[in,out]  p_pti  Pointer to a ::MBG_POLL_THREAD_INFO structure.
 *
 * @return The result of ::mbg_thread_stop.
 *
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 */
 _MBG_API_ATTR int _MBG_API mbg_xhrt_poll_thread_stop( MBG_POLL_THREAD_INFO *p_pti ) ;

 /**
 * @brief Retrieve an extrapolated time stamp in ::PCPS_HR_TIME format.
 *
 * The time stamp is extrapolated using the system's current cycles counter value
 * and a time stamp plus associated cycles counter value saved by the polling thread.
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p      Pointer to a ::MBG_XHRT_INFO structure used to retrieve data from the polling thread.
 * @param[out]     p_hrt  Pointer to a ::PCPS_HR_TIME structure to be filled up.
 *
 * @return The return code from the API call used by the polling thread
 *         to read the time from the device, usually ::mbg_get_hr_time_cycles,
 *         so ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbg_xhrt_poll_group
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 * @see @ref mbg_xhrt_poll_group
 */
 _MBG_API_ATTR int _MBG_API mbg_get_xhrt_time_as_pcps_hr_time( MBG_XHRT_INFO *p, PCPS_HR_TIME *p_hrt ) ;

 /**
 * @brief Retrieve an extrapolated time stamp in 'FILETIME' format.
 *
 * The time stamp is extrapolated using the system's current cycles counter value
 * and a time stamp plus associated cycles counter value saved by the polling thread.
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * Since @a FILETIME is a Windows-specific type, this function is only
 * implemented on Windows.
 *
 * @param[in,out]  p     Pointer to a ::MBG_XHRT_INFO structure used to retrieve data from the polling thread.
 * @param[out]     p_ft  Pointer to a FILETIME structure to be filled up.
 *
 * @return The return code from the API call used by the polling thread
 *         to read the time from the device, usually ::mbg_get_hr_time_cycles,
 *         so ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbg_xhrt_poll_group
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_cycles_frequency
 * @see @ref mbg_xhrt_poll_group
 */
 _MBG_API_ATTR int _MBG_API mbg_get_xhrt_time_as_filetime( MBG_XHRT_INFO *p, FILETIME *p_ft ) ;

 /**
 * @brief Retrieve the frequency of the system's cycles counter.
 *
 * The frequency is determined by the device polling thread.
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p          Pointer to a ::MBG_XHRT_INFO structure used to retrieve data from the polling thread.
 * @param[out]     p_freq_hz  Pointer to a ::MBG_PC_CYCLES_FREQUENCY variable in which the frequency is returned.
 *
 * @return The return code from the API call used by the polling thread
 *         to read the time from the device, usually ::mbg_get_hr_time_cycles,
 *         so ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbg_xhrt_poll_group
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see @ref mbg_xhrt_poll_group
 */
 _MBG_API_ATTR int _MBG_API mbg_get_xhrt_cycles_frequency( MBG_XHRT_INFO *p, MBG_PC_CYCLES_FREQUENCY *p_freq_hz ) ;

 /**
 * @brief Retrieve the system's default cycles counter frequency from the kernel driver.
 *
 * This API call can be used on systems that don't provide this information in user space.
 *
 * @param[in]   dh  Handle of the device to which the IOCTL call is sent.
 * @param[out]  p   Pointer of a ::MBG_PC_CYCLES_FREQUENCY variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_default_cycles_frequency
 */
 _MBG_API_ATTR int _MBG_API mbg_get_default_cycles_frequency_from_dev( MBG_DEV_HANDLE dh, MBG_PC_CYCLES_FREQUENCY *p ) ;

 /**
 * @brief Retrieve the system's default cycles counter frequency.
 *
 * @note This may not be supported on all target platforms, in which case the
 * returned frequency is 0 and the ::mbg_get_default_cycles_frequency_from_dev
 * call should be used instead.
 *
 * @return The default cycles counter frequency in Hz, or 0 if the value is not available.
 *
 * @see ::mbg_get_default_cycles_frequency_from_dev
 */
 _MBG_API_ATTR MBG_PC_CYCLES_FREQUENCY _MBG_API mbg_get_default_cycles_frequency( void ) ;

 /**
 * @brief Retrieve a string with the SYN1588 type name.
 *
 * @return  The type name string, usually "SYN1588".
 */
 _MBG_API_ATTR const char * _MBG_API mbg_get_syn1588_type_name( void ) ;

 /**
 * @brief Retrieve the content of a SYN1588 NIC's PTP_SYNC_STATUS register.
 *
 * @note The variable addressed by @p p is unchanged if an error occurred.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle.
 * @param[out]  p   Pointer to a uint32_t variable to be filled.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 _MBG_API_ATTR int _MBG_API mbg_get_syn1588_ptp_sync_status( MBG_DEV_HANDLE dh, uint32_t *p ) ;


/* ----- function prototypes end ----- */


#if defined( MBG_TGT_WIN32 )
  // These functions are only required / used on Windows, so we exclude
  // them from the automatically generated prototypes above.
  void mbgdevio_syn1588_get_lock_win32( void );
  void mbgdevio_syn1588_release_lock_win32( void );
#endif



/*HDR*/
/**
 * @brief Create a string with a unique device name.
 *
 * The device name is composed of the type name of the device,
 * and its serial number which is appended after an underscore '_'.
 * So the string buffer is typically an ::MBG_DEV_NAME.
 * See ::MBG_DEV_NAME for the possible formats of a device name.
 *
 * A more specific version of this function is
 * ::mbg_create_dev_name_from_dev_info.
 *
 * The complementary function ::mbg_split_dev_name can be used
 * to split an ::MBG_DEV_NAME into its components.
 *
 * @param[out]  s         Pointer to the output buffer for the string.
 * @param[in]   max_len   Size of the output buffer.
 * @param[in]   type_str  Type name string.
 * @param[in]   sn_str    Serial number string.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_create_dev_name_from_dev_info
 * @see ::mbg_split_dev_name
 * @see ::MBG_DEV_NAME
 */
static __mbg_inline
int mbg_create_dev_name( char *s, size_t max_len,
                         const char *type_str, const char *sn_str )
{
  int n = mbg_snprintf( s, max_len, MBG_DEV_NAME_FMT, type_str, sn_str );

  return n;

}  // mbg_create_dev_name



/*HDR*/
/**
 * @brief Create a string with a unique device name from a PCPS_DEV structure.
 *
 * The device name is composed of the type name of the device,
 * and its serial number which is appended after an underscore '_'.
 * So the string buffer is typically an ::MBG_DEV_NAME.
 * See ::MBG_DEV_NAME for the possible formats of a device name.
 *
 * A more versatile version of this function is ::mbg_create_dev_name.
 *
 * The complementary function ::mbg_split_dev_name can be used
 * to split an ::MBG_DEV_NAME into its components.
 *
 * @param[out]  s        Pointer to the output buffer for the string.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   p_dev    Pointer to a ::PCPS_DEV structure providing the required information.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_create_dev_name
 * @see ::mbg_split_dev_name
 * @see ::MBG_DEV_NAME
 */
static __mbg_inline
int mbg_create_dev_name_from_dev_info( char *s, size_t max_len,
                                       const PCPS_DEV *p_dev )
{
  int n = mbg_create_dev_name( s, max_len, _pcps_type_name( p_dev ),
                               _pcps_sernum( p_dev ) );

  return n;

}  // mbg_create_dev_name_from_dev_info



#if defined( MBG_TGT_WIN32 )

  /**
   * @brief Implementation of the IOCTL handler for Windows.
   *
   * In contrast to the POSIX variant, this IOCTL handler expects
   * distinct input and output buffers.
   *
   * @param[in]   dh          Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
   * @param[in]   ioctl_code  IOCTL code to be passed to the kernel driver.
   * @param[in]   in_p        Pointer to an input buffer with the data to be passed to the kernel driver.
   * @param[in]   in_sz       Size of the input buffer, according to the value of @p ioctl_code.
   * @param[out]  out_p       Pointer to an output buffer to receive data from the kernel driver.
   * @param[in]   out_sz      Size of the output buffer, according to the value of @p ioctl_code.
   *
   * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
   */
static __mbg_inline
MBGDEVIO_RET_VAL do_mbg_ioctl_win32( MBG_DEV_HANDLE dh, int ioctl_code,
                                     const void *in_p, int in_sz, void *out_p, int out_sz )
{
  DWORD ReturnedLength = 0;

  if ( !DeviceIoControl( dh, ioctl_code,
                         (LPVOID) in_p, in_sz, out_p, out_sz,
                         &ReturnedLength,
                         NULL
                       ) )
  {
    // An error occurred.
    // Save the system error code, and convert it to an associated
    // error code of the Meinberg API.
    DWORD last_error = GetLastError();
    int rc = mbg_win32_sys_err_to_mbg( last_error, NULL );

    #if 0  //### TODO FIXME
      // We can't call mbgsvctl_log_mbgdevio_error() here (for now).
      // Is is defined in mbgsvctl.h, and including mbgsvctl.h here,
      // or copying the prototype here results in DLL import/export
      // mismatch errors.

      // Do not report a USB device timeout error.
      if ( rc != MBG_ERR_USB_ACCESS )
        mbgsvctl_log_mbgdevio_error( ioctl_code, rc );
    #endif

    return rc;
  }

  return MBG_SUCCESS;

}  // do_mbg_ioctl_win32

  #define _do_mbg_ioctl( _dh, _ioctl, _p, _in_sz, _out_sz ) \
    do_mbg_ioctl_win32( _dh, _ioctl, (LPVOID) _p, _in_sz, (LPVOID) _p, _out_sz )

#elif defined( MBG_HAS_POSIX_IOCTL )

  /**
   * @brief Implementation of the IOCTL handler for POSIX systems.
   *
   * @param[in]  dh          Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
   * @param[in]  ioctl_code  IOCTL code to be passed to the kernel driver.
   * @param[in,out]  p       Pointer to an input or output buffer, depending on @p ioctl_code.
   *
   * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
   */
  static __mbg_inline
  MBGDEVIO_RET_VAL do_mbg_ioctl_posix( MBG_DEV_HANDLE dh, unsigned long ioctl_code, const void *p )
  {
    int sys_rc = ioctl( dh, ioctl_code, (void *) p );

    // In case of an error, ioctl returns -1, and errno has been set
    // to a *positive* POSIX error code as specified in errno.h.
    if ( sys_rc == -1 )
    {
      // Save the system error code, and convert it to an associated
      // error code of the Meinberg API.
      int sys_errno = errno;
      int rc = mbg_posix_errno_to_mbg( sys_errno, NULL );

      #if DEBUG_IOCTL_CALLS
        fprintf( stderr, "IOCTL %s (0x%08lX) error: %s (errno: %i) --> %s (rc: %i)\n",
                 mbgioctl_get_name( ioctl_code ), (ulong) ioctl_code,
                 strerror( sys_errno ), sys_errno,
                 mbg_strerror( rc ), rc );
      #endif

      return rc;
    }

    return MBG_SUCCESS;

  }  // do_mbg_ioctl_posix

  #define _do_mbg_ioctl( _dh, _ioctl, _p, _in_sz, _out_sz ) \
    do_mbg_ioctl_posix( _dh, _ioctl, _p )

#endif



// The code below depends on whether the target device is accessed via
// IOCTLs to a device driver, or the hardware is accessed directly.

#if MBG_TGT_USE_IOCTL

  /**
   * @brief Send a generic IOCTL command to the driver.
   *
   * @param[in]  dh     Valid handle to a Meinberg device.
   * @param[in]  info   Additional information for the kernel driver depending on
   *                    the IOCTL code, i.e. the low level function to be called:
   *                      - One of the PCPS_... commands with IOCTL_PCPS_GENERIC_{READ|WRITE}.
   *                      - One of the PC_GPS_... commands with IOCTL_PCPS_GENERIC_{READ|WRITE}_GPS.
   *                      - One of the PCPS_GEN_IO_... enumeration codes with ::IOCTL_PCPS_GENERIC_IO.
   * @param[in]  ioctl_code One of the IOCTL_GENERIC_... codes telling the kernel driver
   *                    which low level function to use, e.g. normal read or write,
   *                    large (GPS) data read or write, or generic I/O.
   * @param[in]  in_p   Pointer to an input buffer passed to the driver, can be @a NULL.
   * @param[in]  in_sz  Size of the input buffer, can be 0 if no buffer is used.
   * @param[out] out_p  Pointer to an output buffer passed to the driver, can be @a NULL.
   * @param[in]  out_sz Size of the output buffer, can be 0 if no buffer is used.
   *
   * @return ::MBG_SUCCESS or error code returned by device I/O control function.
   */
  static __mbg_inline
  int mbgdevio_do_gen_io( MBG_DEV_HANDLE dh, int info, unsigned int ioctl_code,
                          const void *in_p, int in_sz,
                          void *out_p, int out_sz )
  {
    MBGDEVIO_RET_VAL rc;

    // Generic IOCTL calls always need to pass some info beside
    // the I/O buffers down to the driver, which usually is
    // the command code for the device.
    // Thus we must always use one of the control structures
    // IOCTL_GENERIC_REQ or IOCTL_GENERIC_BUFFER, whichever
    // is appropriate for the target OS.

    #if USE_IOCTL_GENERIC_REQ

      IOCTL_GENERIC_REQ req = { 0 };

      req.in_p = (uintptr_t) in_p;
      req.out_p = (uintptr_t) out_p;
      req.in_sz = in_sz;
      req.out_sz = out_sz;
      req.info = info;

      #if DEBUG_IOCTL_CALLS
        printf( "inp:  %p --> 0x%" PRIx64 " (%lu)\n", in_p, req.in_p, (ulong) req.in_sz );
        printf( "outp: %p --> 0x%" PRIx64 " (%lu)\n", out_p, req.out_p, (ulong) req.out_sz );
      #endif

      rc = _do_mbg_ioctl( dh, ioctl_code, &req, sizeof( req ), 0 );

    #else

      IOCTL_GENERIC_BUFFER *p_buff;
      int buff_size = sizeof( p_buff->ctl )
                    + ( ( in_sz > out_sz ) ? in_sz : out_sz );

      p_buff = (IOCTL_GENERIC_BUFFER *) malloc( buff_size );

      if ( p_buff == NULL )
        return MBG_ERR_NO_MEM;

      p_buff->ctl.info = info;
      p_buff->ctl.data_size_in = in_sz;
      p_buff->ctl.data_size_out = out_sz;

      if ( in_p )
        memcpy( p_buff->data, in_p, in_sz );

      rc = _do_mbg_ioctl( dh, ioctl_code, p_buff,
                          sizeof( IOCTL_GENERIC_CTL ) + in_sz,
                          sizeof( IOCTL_GENERIC_CTL ) + out_sz );

      if ( out_p && ( rc == MBG_SUCCESS ) )
        memcpy( out_p, p_buff->data, out_sz );

      free( p_buff );

    #endif

    return _mbgdevio_cnv_ret_val( rc );

  }  // mbgdevio_do_gen_io



  #define _do_mbgdevio_read( _dh, _cmd, _ioctl, _p, _sz ) \
    _do_mbg_ioctl( _dh, _ioctl, _p, 0, _sz )

  #define _do_mbgdevio_write( _dh, _cmd, _ioctl, _p, _sz ) \
    _do_mbg_ioctl( _dh, _ioctl, _p, _sz, 0 )

  #define _do_mbgdevio_read_gps      _do_mbgdevio_read

  #define _do_mbgdevio_write_gps     _do_mbgdevio_write



  #define _mbgdevio_read_var( _dh, _cmd, _ioctl, _p ) \
    _do_mbgdevio_read( _dh, _cmd, _ioctl, _p, sizeof( *(_p) ) )

  #define _mbgdevio_write_var( _dh, _cmd, _ioctl, _p ) \
    _do_mbgdevio_write( _dh, _cmd, _ioctl, _p, sizeof( *(_p) ) )

  #define _mbgdevio_write_cmd( _dh, _cmd, _ioctl ) \
    _do_mbgdevio_write( _dh, _cmd, _ioctl, NULL, 0 )

  #define _mbgdevio_read_gps        _do_mbgdevio_read
  #define _mbgdevio_read_gps_var    _mbgdevio_read_var

  #define _mbgdevio_write_gps       _do_mbgdevio_write
  #define _mbgdevio_write_gps_var   _mbgdevio_write_var


  #define _mbgdevio_gen_read( _dh, _cmd, _p, _sz ) \
    mbgdevio_do_gen_io( _dh, _cmd, IOCTL_PCPS_GENERIC_READ, NULL, 0, _p, _sz )

  #define _mbgdevio_gen_write( _dh, _cmd, _p, _sz ) \
    mbgdevio_do_gen_io( _dh, _cmd, IOCTL_PCPS_GENERIC_WRITE, _p, _sz, NULL, 0 )

  #define _mbgdevio_gen_io( _dh, _type, _in_p, _in_sz, _out_p, _out_sz ) \
    mbgdevio_do_gen_io( _dh, _type, IOCTL_PCPS_GENERIC_IO, _in_p, _in_sz, _out_p, _out_sz )

  #define _mbgdevio_gen_read_gps( _dh, _cmd, _p, _sz ) \
    mbgdevio_do_gen_io( _dh, _cmd, IOCTL_PCPS_GENERIC_READ_GPS, NULL, 0, _p, _sz )

  #define _mbgdevio_gen_write_gps( _dh, _cmd, _p, _sz ) \
    mbgdevio_do_gen_io( _dh, _cmd, IOCTL_PCPS_GENERIC_WRITE_GPS, _p, _sz, NULL, 0 )


#else  // !MBG_TGT_USE_IOCTL, accessing hardware device directly

  #define _mbgdevio_chk_cond( _cond )  \
  {                                    \
    if ( !(_cond) )                    \
      return MBG_ERR_NOT_SUPP_BY_DEV;  \
  }

  #define _mbgdevio_read( _dh, _cmd, _ioctl, _p, _sz ) \
    pcps_read_safe( _dh, _cmd, _p, _sz )

  #define _mbgdevio_write( _dh, _cmd, _ioctl, _p, _sz ) \
    pcps_write_safe( _dh, _cmd, _p, _sz )

  #define _do_mbgdevio_read_gps( _dh, _cmd, _ioctl, _p, _sz ) \
    pcps_read_gps_safe( _dh, _cmd, _p, _sz )

  #define _mbgdevio_write_gps( _dh, _cmd, _ioctl, _p, _sz ) \
    pcps_write_gps_safe( _dh, _cmd, _p, _sz )



//##+++++++++++++++++++
  #define _mbgdevio_read_var( _dh, _cmd, _ioctl, _p ) \
    _pcps_read_var_safe( _dh, _cmd, *(_p) )

  #define _mbgdevio_write_var( _dh, _cmd, _ioctl, _p ) \
    _pcps_write_var_safe( _dh, _cmd, *(_p) )

  #define _mbgdevio_write_cmd( _dh, _cmd, _ioctl ) \
    _pcps_write_byte_safe( _dh, _cmd );

  #define _mbgdevio_read_gps_var( _dh, _cmd, _ioctl, _p ) \
    _pcps_read_gps_var_safe( _dh, _cmd, *(_p) )

  #define _mbgdevio_write_gps_var( _dh, _cmd, _ioctl, _p ) \
    _pcps_write_gps_var_safe( _dh, _cmd, *(_p) )


  #define _mbgdevio_gen_read( _dh, _cmd, _p, _sz ) \
    _mbgdevio_read( _dh, _cmd, -1, _p, _sz )

  #define _mbgdevio_gen_write( _dh, _cmd, _p, _sz ) \
    _mbgdevio_write( _dh, _cmd, -1, _p, _sz )

  #define _mbgdevio_gen_io( _dh, _type, _in_p, _in_sz, _out_p, _out_sz ) \
    pcps_generic_io( _dh, _type, _in_p, _in_sz, _out_p, _out_sz );

  #define _mbgdevio_gen_read_gps( _dh, _cmd, _p, _sz ) \
    _do_mbgdevio_read_gps( _dh, _cmd, -1, _p, _sz )

  #define _mbgdevio_gen_write_gps( _dh, _cmd, _p, _sz ) \
    _mbgdevio_write_gps( _dh, _cmd, -1, _p, _sz )

#endif  // !MBG_TGT_USE_IOCTL



#define _mbg_generic_read_var( _dh, _cmd, _s )  \
  mbg_generic_read( _dh, _cmd, &(_s), sizeof( (_s) ) )

#define _mbg_generic_write_var( _dh, _cmd, _s )  \
  mbg_generic_write( _dh, _cmd, &(_s), sizeof( (_s) ) )

#define _mbg_generic_read_gps_var( _dh, _cmd, _s )  \
  mbg_generic_read_gps( _dh, _cmd, &(_s), sizeof( (_s) ) )

#define _mbg_generic_write_gps_var( _dh, _cmd, _s )  \
  mbg_generic_write_gps( _dh, _cmd, &(_s), sizeof( (_s) ) )



#define _mbgdevio_gen_read_var( _dh, _cmd, _s ) \
  _mbgdevio_gen_read( _dh, _cmd, &(_s), sizeof( (_s) ) )

#define _mbgdevio_gen_write_var( _dh, _cmd, _s ) \
  _mbgdevio_gen_write( _dh, _cmd, &(_s), sizeof( (_s) ) )

#define _mbgdevio_gen_read_gps_var( _dh, _cmd, _s ) \
  _mbgdevio_gen_read_gps( _dh, _cmd, &(_s), sizeof( (_s) ) )

#define _mbgdevio_gen_write_gps_var( _dh, _cmd, _s ) \
  _mbgdevio_gen_write_gps( _dh, _cmd, &(_s), sizeof( (_s) ) )



#if defined( MBG_TGT_WIN32 ) || defined( MBG_TGT_POSIX )

// NOTE
// For some reason, the code below causes an internal compiler error
// with Borland C++Builder 5.0 release builds. Since we don't need
// this function in the BC5 projects anyway we simply exclude it
// from build.

#if !defined( __BORLANDC__ )

static __mbg_inline
void mbg_chk_tstamp64_leap_sec( uint64_t *tstamp64, PCPS_TIME_STATUS_X *status )
{
  if ( *status & ( PCPS_LS_ANN | PCPS_LS_ENB ) )
  {
    struct tm tm = { 0 };
    time_t t = cvt_to_time_t( (uint32_t) ( (*tstamp64) >> 32 ) );
    int rc = mbg_gmtime( &tm, &t );

    if ( mbg_rc_is_success( rc ) )
    {
      // Handle leap second and status.
      if ( tm.tm_hour == 0 && tm.tm_min == 0 && tm.tm_sec == 0 )
      {
        if ( *status & PCPS_LS_ANN )
        {
          // Set leap second enabled flag on rollover to the leap second and clear announce flag.
          *status &= ~PCPS_LS_ANN;
          *status |= PCPS_LS_ENB;

          // Decrement interpolated second to avoid automated overflow during the leap second.
          // Second 59 appears for the second time.
          *tstamp64 -= MBG_FRAC32_UNITS_PER_SEC;
        }
        else
          if ( *status & PCPS_LS_ENB ) // Clear bits when leap second expires and 0:00:00 UTC is reached.
            *status &= ~( PCPS_LS_ANN | PCPS_LS_ENB );
      }
    }
  }

}  // mbg_chk_tstamp64_leap_sec

#endif  // !defined( __BORLANDC__ )

#endif  // defined( MBG_TGT_WIN32 ) || defined( MBG_TGT_POSIX )



static __mbg_inline
int mbg_init_pc_cycles_frequency( MBG_DEV_HANDLE dh )
{
  if ( mbg_pc_cycles_frequency == 0 )
    return mbg_get_default_cycles_frequency_from_dev( dh, &mbg_pc_cycles_frequency );

  return MBG_SUCCESS;

}  // mbg_init_pc_cycles_frequency



#ifdef __cplusplus
}
#endif

#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT
#undef _MBG_API_ATTR_VAR

#endif  /* _MBGDEVIO_H */

