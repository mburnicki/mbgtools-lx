
/**************************************************************************
 *
 *  $Id: mbgmktm.h 1.5 2019/09/27 14:42:16 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for mbgmktm.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgmktm.h $
 *  Revision 1.5  2019/09/27 14:42:16  martin
 *  Updated function prototypes, and cleaned up code.
 *  Revision 1.4  2019/08/26 11:29:55  martin
 *  New inline functions mbg_mktime64_from_tm_gps()
 *  and mbg_mktime_from_tm_gps().
 *  Revision 1.3  2019/08/21 11:07:14  martin
 *  ATTENTION: Calling conventions have changed!
 *  Renamed mbg_mktime() to mbg_mktime64(), which always computes a
 *  64 bit timestamp and expects the address of a 64 bit variable to
 *  take the result. Unlike mktime(), this function now provides a
 *  proper return code indicating if the conversion was successful,
 *  or not.
 *  Also, the 'day' parameter now has to be in the same range as for
 *  POSIX mktime(), i.e. 1..31, instead of 0..30 as in the previous
 *  implementation of mbg_mktime. This also has to be taken into
 *  account wherever this function is called.
 *  The header file provides an inline function mbg_mktime() which
 *  is similar to mbg_mktime64() but expects the address of a POSIX
 *  time_t for the result, which can be 32 bit only on some systems,
 *  so the mbg_mktime64() function should be used preferably, if
 *  64 bit timestamps are used anyway.
 *  There are also new inline functions mbg_mktime64_from_tm() and
 *  mbg_mktime_from_tm(), which call the associated generic functions
 *  but expect a standard POSIX struct tm to provide the date and
 *  time to be converted.
 *  Revision 1.2  2017/07/05 10:02:42  martin
 *  Include time.h.
 *  Updated function prototypes.
 *  Revision 1.1  2006/08/22 08:57:15  martin
 *  Former function totalsec() moved here from pcpsmktm.c.
 *
 **************************************************************************/

#ifndef _MBGMKTM_H
#define _MBGMKTM_H


/* Other headers to be included */

#include <mbgtime.h>
#include <timeutil.h>
#include <mbgerror.h>

#include <time.h>


#ifdef _MBGMKTM
 #define _ext
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup mbg_mktime_fncs Meinberg functions to replace POSIX mktime
 *
 * These functions work like the POSIX @a mktime function but don't apply
 * a local time offset which depends on some timezone setting.
 *
 * The basic function ::mbg_mktime64 expects the address of a variable
 * of type ::MBG_TIME64_T, which is always 64 bits wide, even on systems
 * where @a time_t is only 32 bits wide.
 *
 * Also, the basic function does not expect a structure, but a set of variables,
 * which makes it more versatile. The expected values are in the same ranges
 * as the for the members of the POSIX <em>struct tm</em> type used by @a mktime.
 *
 * Another difference is that unlike @a mktime, which simply returns
 * <em>(time_t) -1</em> in case of error, these functions provides a
 * return code indicating if the calulation succeeded or not,
 * and @a -1 can be a valid timestamp indicating the last second
 * before the epoch.
 *
 * There are also some variants, implemented as inline functions.
 *
 * The ::mbg_mktime function can be called with the address of a generic
 * @a time_t for the calculated time stamp. On systems with a 64 bit @a time_t
 * the result is the same as for ::mbg_mktime64, but on systems where
 * @a time_t is only 32 bits wide, the returned value is truncated
 * and suffers from the <b>Y2038 problem</b>.
 *
 * The variants ::mbg_mktime64_from_tm and ::mbg_mktime_from_tm are
 * similar to ::mbg_mktime64 and ::mbg_mktime, but really expect a
 * pointer to a <em>struct tm</em> as input, like the original @a mktime.
 *
 * @see ::mbg_mktime64
 * @see ::mbg_mktime
 * @see ::mbg_mktime64_from_tm
 * @see ::mbg_mktime_from_tm
 */



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Compute a linear ::MBG_TIME64_T value from broken down date and time
 *
 * This function basically works like the POSIX @a mktime function but
 * doesn't apply a local time offset which depends on some timezone setting.
 *
 * Unlike @a mktime, it expects the address of a variable of type
 * ::MBG_TIME64_T, which is always 64 bits wide, even on systems
 * where @a time_t is only 32 bits wide.
 *
 * Also, it does not expect a structure, but a set of variables, which
 * makes it more versatile. The expected values are in the same ranges
 * as the members of the POSIX <em>struct tm</em> type used by @a mktime.
 *
 * Another difference is that unlike POSIX @a mktime, which simply returns
 * <em>(time_t) -1</em> in case of error, this function provides a
 * return code indicating if the calulation succeeded or not,
 * and @a -1 can be a valid timestamp indicating a time associated with
 * one second before the epoch.
 *
 * There are also some variants, implemented as inline functions.
 * See @ref mbg_mktime_fncs.
 *
 * @param[out]  p_t64  Address of a variable to take the result on success,
 *                     i.e. seconds since 1970-01-01 (POSIX @a time_t format,
 *                     but always 64 bits).
 * @param[in]   year   Years since 1900, i.e current year number - 1900.
 * @param[in]   month  Months since January, 0..11
 * @param[in]   day    Day of the month, 1..31
 * @param[in]   hour   0..23
 * @param[in]   min    0..59
 * @param[in]   sec    0..59, or 60 if inserted leap second
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
 int mbg_mktime64( MBG_TIME64_T *p_t64, int year, int month, int day, int hour, int min, int sec ) ;


/* ----- function prototypes end ----- */


static __mbg_inline /*HDR*/
/**
 * @brief Compute a linear time_t value from broken down date and time.
 *
 * This function is a variant of ::mbg_mktime64, but expects
 * the address of a @a time_t variable for the result.
 *
 * On systems with a 64 bit @a time_t the result is the same as for
 * ::mbg_mktime64, but on systems where @a time_t is only 32 bits wide,
 * the returned value is truncated and suffers from the <b>Y2038 problem</b>.
 *
 * Use ::mbg_mktime64 preferably to compute a timestamp
 * which is always 64 bits wide.
 *
 * @param[out]  p_t    Address of a @a time_t variable to receive the calculated
 *                     timestamp on success, i.e. the seconds since 1970-01-01
 *                     (POSIX time_t format, but truncated to 32 bits on systems
 *                     with 32 bit @a time_t).
 *
 * @param[in]   year   Years since 1900, i.e current year number - 1900.
 * @param[in]   month  Months since January, 0..11
 * @param[in]   day    Day of the month, 1..31
 * @param[in]   hour   0..23
 * @param[in]   min    0..59
 * @param[in]   sec    0..59, or 60 if leap second
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int mbg_mktime( time_t *p_t, int year, int month, int day,
                int hour, int min, int sec )
{
  MBG_TIME64_T t64;

  int rc = mbg_mktime64( &t64, year, month, day, hour, min, sec );

  // On success we still check if the result is in a valid range
  // and convert the result, if required.
  if ( mbg_rc_is_success( rc ) )
    rc = mbg_trnc_time64_t_to_time_t( p_t, &t64 );

  return rc;

}  // mbg_mktime



static __mbg_inline /*HDR*/
/**
 * @brief Compute a 64 bit ::MBG_TIME64_T value from a struct tm.
 *
 * This function is just a shortcut for ::mbg_mktime64 which takes
 * a POSIX-style <em>struct tm</em> as input, and calls ::mbg_mktime64
 * accordingly.
 *
 * @param[out]  p_t    Address of an ::MBG_TIME64_T variable to receive the
 *                     calculated timestamp on success, i.e. seconds since
 *                     1970-01-01 (POSIX @a time_t format, but always 64 bits).
 *
 * @param[in]   p_tm   Pointer to a POSIX <em>struct tm</em> providing the date
 *                     and time to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int mbg_mktime64_from_tm( MBG_TIME64_T *p_t, const struct tm *p_tm )
{
  return mbg_mktime64( p_t, p_tm->tm_year, p_tm->tm_mon, p_tm->tm_mday,
                       p_tm->tm_hour, p_tm->tm_min, p_tm->tm_sec );

}  // mbg_mktime64_from_tm



static __mbg_inline /*HDR*/
/**
 * @brief Compute a POSIX time_t value from a struct tm.
 *
 * This function is just a shortcut for ::mbg_mktime which takes
 * a POSIX-style <em>struct tm</em>, and calls ::mbg_mktime accordingly.
 *
 * Use ::mbg_mktime64_from_tm preferably to compute a timestamp
 * which is always 64 bits wide.
 *
 * @param[out]  p_t    Address of a @a time_t variable to receive the calculated
 *                     timestamp on success, i.e. the seconds since 1970-01-01
 *                     (POSIX time_t format, but truncated to 32 bits on systems
 *                     with 32 bit @a time_t).
 *
 * @param[in]   p_tm   Pointer to a POSIX <em>struct tm</em> providing the date
 *                     and time to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int mbg_mktime_from_tm( time_t *p_t, const struct tm *p_tm )
{
  return mbg_mktime( p_t, p_tm->tm_year, p_tm->tm_mon, p_tm->tm_mday,
                     p_tm->tm_hour, p_tm->tm_min, p_tm->tm_sec );

}  // mbg_mktime_from_tm



static __mbg_inline /*HDR*/
/**
 * @brief Compute a 64 bit ::MBG_TIME64_T value from ::TM_GPS.
 *
 * This function is just a shortcut for ::mbg_mktime64 which takes a
 * ::TM_GPS structure as input, and calls ::mbg_mktime64 accordingly.
 *
 * @param[out]  p_t    Address of an ::MBG_TIME64_T variable to receive the
 *                     calculated timestamp on success, i.e. seconds since
 *                     1970-01-01 (POSIX @a time_t format, but always 64 bits).
 *
 * @param[in]   p_tm   Pointer to a ::TM_GPS structure providing the date
 *                     and time to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int mbg_mktime64_from_tm_gps( MBG_TIME64_T *p_t, const TM_GPS *p_tm )
{
  return mbg_mktime64( p_t, p_tm->year - 1900, p_tm->month - 1, p_tm->mday,
                       p_tm->hour, p_tm->min, p_tm->sec );

}  // mbg_mktime64_from_tm_gps



static __mbg_inline /*HDR*/
/**
 * @brief Compute a POSIX time_t value from ::TM_GPS.
 *
 * This function is just a shortcut for ::mbg_mktime which takes a
 * ::TM_GPS structure as input, and calls ::mbg_mktime accordingly.
 *
 * Use ::mbg_mktime64_from_tm_gps preferably to compute a timestamp
 * which is always 64 bits wide.
 *
 * @param[out]  p_t    Address of a @a time_t variable to receive the calculated
 *                     timestamp on success, i.e. the seconds since 1970-01-01
 *                     (POSIX time_t format, but truncated to 32 bits on systems
 *                     with 32 bit @a time_t).
 *
 * @param[in]   p_tm   Pointer to a ::TM_GPS structure providing the date
 *                     and time to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int mbg_mktime_from_tm_gps( time_t *p_t, const TM_GPS *p_tm )
{
  return mbg_mktime( p_t, p_tm->year - 1900, p_tm->month - 1, p_tm->mday,
                     p_tm->hour, p_tm->min, p_tm->sec );

}  // mbg_mktime_from_tm_gps



#ifdef __cplusplus
}
#endif

/* End of header body */

#undef _ext

#endif  /* _MBGMKTM_H */
