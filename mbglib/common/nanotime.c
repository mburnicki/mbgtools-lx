
/**************************************************************************
 *
 *  $Id: nanotime.c 1.12 2022/09/02 12:41:38 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Meinberg Library module providing functions to handle NANO_TIME
 *    and NANO_TIME_64.
 *
 * -----------------------------------------------------------------------
 *  $Log: nanotime.c $
 *  Revision 1.12  2022/09/02 12:41:38  martin.burnicki
 *  Fixed some compiler warnings.
 *  Revision 1.11  2022/06/23 14:48:23Z  martin.burnicki
 *  Changed the return value of some snprintf_ functions from size_t to int.
 *  Revision 1.10  2021/10/20 07:27:18  thomas-b
 *  Fixed conversion between fractions of TM_GPS and ns in NANO_TIME_64
 *  Revision 1.9  2019/08/21 10:34:55  thomas-b
 *  Call mbg_mktime64 in conversion from and to NANO_TIME_64, because secs value is int64_t, anyway
 *  Fixed function call; it does not expect mday - 1 any longer
 *  Revision 1.8  2019/08/19 10:23:46  martin
 *  Updated nano_time_64_to_tm_gps() and tm_gps_to_nano_time_64().
 *  ATTENTION: tm_gps_to_nano_time_64() now returns MBG_SUCCESS
 *  on success, or or some negative Meinberg error code, as usual, whereas
 *  the previous previous version of the function returned 1 on success and
 *  0 in case of error, so the conditions to check the return code have to be inverted!
 *  Revision 1.7  2019/07/08 12:46:16  thomas-b
 *  Added conversion of the fractions (ns) in tm_gps_to_nano_time_64 and vice versa
 *  Revision 1.6  2018/12/11 11:55:06  martin
 *  Fixed compiler warnings on Windows.
 *  Revision 1.5  2018/11/21 11:24:19Z  martin
 *  Exclude non-essential header files and functions from firmware builds.
 *  Moved inclusion of stdlib.h to the header file.
 *  Revision 1.4  2018/09/20 09:46:57  martin
 *  Fixed build for some targets.
 *  Moved version dependencies to mbg:tgt.h.
 *  Revision 1.3  2018/07/16 12:39:42Z  martin
 *  Functions nano_time_64_to_tm_gps() and tm_gps_to_nano_time_64()
 *  were moved here.
 *  Account for renamed library symbol NSEC_PER_SEC.
 *  Even VS2010 doesn't provide strtoll().
 *  Revision 1.2  2017/06/12 13:54:04  martin
 *  Fixed build under Windows.
 *  Revision 1.1  2017/06/12 08:49:09Z  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _NANOTIME
 #include <nanotime.h>
#undef _NANOTIME

#include <mbgtime.h>

#if !_IS_MBG_FIRMWARE
  #include <mbgerror.h>
  #include <mbgmktm.h>
  #include <str_util.h>
  #include <timeutil.h>
#endif



/*HDR*/
/**
 * @brief Convert a ::NANO_TIME time to double
 *
 * @param[in]  p  Address of a ::NANO_TIME structure to be converted
 *
 * @return The computed number of seconds with fractions, as double
 *
 * @see ::double_to_nano_time
 */
double nano_time_to_double( const NANO_TIME *p )
{
  double d = p->secs;

  d += ( (double) p->nano_secs ) / 1e9;

  return d;

}  // nano_time_to_double



/*HDR*/
/**
 * @brief Setup a ::NANO_TIME structure from a time provided as double
 *
 * @param[out]  p  Address of a ::NANO_TIME structure to be set up
 * @param[in]   d  The time to be converted, in seconds with fractions, as double
 *
 * @see ::nano_time_to_double
 */
void double_to_nano_time( NANO_TIME *p, double d )
{
  p->secs = (int32_t) d;

  d -= (double) p->secs;

  p->nano_secs = (int32_t) ( d * 1e9 );

}  // double_to_nano_time



#if !_IS_MBG_FIRMWARE

/*HDR*/
/**
 * @brief Print nano time into string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  nt       The ::NANO_TIME to be printed
 */
int snprint_nano_time( char *s, size_t max_len, const NANO_TIME *nt )
{
  int n = snprintf_safe( s, max_len, "%c%li.%09li",
                         _nano_time_negative( nt ) ? '-' : '+',
                         labs( (long) nt->secs ),
                         labs( (long) nt->nano_secs ) );
  return n;

}  // snprint_nano_time

#endif



#if !_IS_MBG_FIRMWARE && !defined( MBG_TGT_MISSING_64_BIT_TYPES )

/*HDR*/
/**
 * @brief Normalize a ::NANO_TIME_64 time
 *
 * After normalization, the following can be assumed:<br>
 * - nano_secs is in the range [-10^9 + 1, 10^9 - 1]<br>
 * - if secs is not 0, secs and nano_secs have the same sign
 *
 * @param[in,out] nt  The ::NANO_TIME_64 to be normalized
 */
void normalize_nano_time_64( NANO_TIME_64 *nt )
{
  int64_t additional_secs;

  // Step 1: Make sure nano seconds are in the interval [-10^9 + 1, 10^9 - 1]
  additional_secs = nt->nano_secs / NSEC_PER_SEC;
  nt->nano_secs -= additional_secs * NSEC_PER_SEC;
  nt->secs += additional_secs;

  // Step 2: Make sure seconds and nanoseconds have same sign if seconds is not 0
  if ( nt->secs > 0 && nt->nano_secs < 0 )
  {
    nt->secs -= 1;
    nt->nano_secs += NSEC_PER_SEC;
  }
  else if ( nt->secs < 0 && nt->nano_secs > 0 )
  {
    nt->secs += 1;
    nt->nano_secs -= NSEC_PER_SEC;
  }

}  // normalize_nano_time_64



/*HDR*/
/**
 * @brief Convert a ::NANO_TIME_64 time to double
 *
 * @param[in]  p  Address of a ::NANO_TIME_64 structure to be converted
 *
 * @return The computed number of seconds with fractions, as double
 *
 * @see ::double_to_nano_time_64
 */
double nano_time_64_to_double( const NANO_TIME_64 *p )
{
  double d = (double) p->secs;

  d += ( (double) p->nano_secs ) / 1e9;

  return d;

}  // nano_time_64_to_double



/*HDR*/
/**
 * @brief Setup a ::NANO_TIME_64 structure from a time as double
 *
 * @param[out]  p  Address of a ::NANO_TIME_64 structure to be set up
 * @param[in]   d  The time to be converted, in seconds with fractions, as double
 *
 * @see ::nano_time_64_to_double
 */
void double_to_nano_time_64( NANO_TIME_64 *p, double d )
{
  p->secs = (int32_t) d;

  d -= (double) p->secs;

  p->nano_secs = (int32_t) ( d * 1e9 );

}  // double_to_nano_time_64



/*HDR*/
/**
 * @brief Print a normalized ::NANO_TIME_64 into a string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  nt       The ::NANO_TIME_64 to be printed
 */
int snprint_nano_time_64( char *s, size_t max_len, const NANO_TIME_64 *nt )
{
  int n = snprintf_safe( s, max_len, "%c%" PRId64 ".%09" PRId64,
                         _nano_time_negative( nt ) ? '-' : '+',
                         _abs64( nt->secs ),
                         _abs64( nt->nano_secs ) );
  return n;

}  // snprint_nano_time_64



static __mbg_inline /*HDR*/
/**
 * @brief Generic function to divide ::NANO_TIME_64 value
 *
 * @param[in,out]  p    Address of a ::NANO_TIME_64 structure to be divided
 * @param[in]      div  The number by which to divide
 * @param[in]      mul  The complementary number to div, i.e. mul = 1000000000 / div
 */
void div_nano_time_64( NANO_TIME_64 *p, long div, long mul )
{
  #if defined( MBG_TGT_MISSING_LLDIV_T )
    //### TODO: Test this code
    int64_t rem = p->secs % div;
    p->secs /= div;
    p->nano_secs = ( rem * mul ) + ( p->nano_secs / div );
  #else
    lldiv_t lldt = lldiv( p->secs, div );
    p->secs = lldt.quot;
    p->nano_secs = ( lldt.rem * mul ) + ( p->nano_secs / div );
  #endif

}  // div_nano_time_64



#if !defined( MBG_TGT_MISSING_STRTOLL )

static __mbg_inline /*HDR*/
/**
 * @brief Generic function to set up a ::NANO_TIME_64 structure from a string
 *
 * @param[in]   s            A string with a time in seconds, with fractions separated by decimal point
 * @param[out]  p            Address of a ::NANO_TIME_64 structure to be set up
 * @param[in]   base         The base of the conversion, i.e. 10 for a string with decimal numbers, or 16 for a hex string
 * @param[in]   frac_digits  The number of fractions required to represent 1 nanosecond, e.g.
 *                           9 for a decimal string like 0.000000001, or
 *                           TODO for a hex string like 0x00000000.00000000
 */
void str_to_nano_time_64( const char *s, NANO_TIME_64 *p, int base, int frac_digits )
{
  char *cp = NULL;

  p->secs = strtoll( s, &cp, base );

  if ( *cp == '.' )  // fractions may follow
  {
    char *ep = NULL;

    cp++;  // skip '.'

    p->nano_secs = strtoll( cp, &ep, base );

    if ( p->nano_secs )  // only if fractions not 0
    {
      int l = (int) ( ep - cp );

      while ( l < frac_digits )
      {
        p->nano_secs *= base;
        l++;
      }

      while ( l > frac_digits )
      {
        p->nano_secs /= base;
        l--;
      }

      if ( p->secs < 0 )
        p->nano_secs = -p->nano_secs;
    }
  }

}  // str_to_nano_time_64



/*HDR*/
/**
 * @brief Set up a ::NANO_TIME_64 structure from a string with a time in seconds and fractions
 *
 * @param[in]   s  A string with a time in seconds, with fractions separated by decimal point
 * @param[out]  p  Address of a ::NANO_TIME_64 structure to be set up
 */
void str_s_to_nano_time_64( const char *s, NANO_TIME_64 *p )
{
  str_to_nano_time_64( s, p, 10, 9 );

}  // str_s_to_nano_time_64



/*HDR*/
/**
 * @brief Set up a ::NANO_TIME_64 structure from a string with a time in milliseconds and fractions
 *
 * @param[in]   s  A string with a time in milliseconds, with fractions separated by decimal point
 * @param[out]  p  Address of a ::NANO_TIME_64 structure to be set up
 */
void str_ms_to_nano_time_64( const char *s, NANO_TIME_64 *p )
{
  // First do the same conversion as for a string representing seconds
  str_s_to_nano_time_64( s, p );

  // Now we have the value in milliseconds
  // Divide by 1000 to get seconds
  div_nano_time_64( p, 1000, 1000000UL );

}  // str_ms_to_nano_time_64

#endif  // !defined( MBG_TGT_MISSING_STRTOLL )



/*HDR*/
/**
 * @brief Convert ::NANO_TIME_64 to ::TM_GPS
 *
 * @param[out] tm_gps  The ::TM_GPS to be filled
 * @param[in]  nt      The ::NANO_TIME_64 to be converted
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @see ::tm_gps_to_nano_time_64
 */
int nano_time_64_to_tm_gps( TM_GPS *tm_gps, const NANO_TIME_64 *nt )
{
  struct tm tm = { 0 };
  int rc = mbg_gmtime64( &tm, &nt->secs );

  if ( mbg_rc_is_success( rc ) )
  {
    tm_gps->year = tm.tm_year + 1900;
    tm_gps->month = tm.tm_mon + 1;
    tm_gps->mday = tm.tm_mday;
    tm_gps->yday = tm.tm_yday;
    tm_gps->wday = tm.tm_wday;
    tm_gps->hour = tm.tm_hour;
    tm_gps->min = tm.tm_min;
    tm_gps->sec = tm.tm_sec;

    // divide nanoseconds by 100 to get fractions
    // fractions should have 100ns resolution due to 10 MHz clock tick
    tm_gps->frac = (int32_t)(nt->nano_secs / 100);

    tm_gps->offs_from_utc = 0;
  }

  return rc;

}  // nano_time_64_to_tm_gps



/*HDR*/
/**
 * @brief Convert ::TM_GPS to ::NANO_TIME_64
 *
 * @param[out] nt      The ::NANO_TIME_64 to be filled
 * @param[in]  tm      The ::TM_GPS to be converted
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @see ::nano_time_64_to_tm_gps
 */
int tm_gps_to_nano_time_64( NANO_TIME_64 *nt, const TM_GPS *tm )
{
  int rc;

  rc = mbg_mktime64( &nt->secs, tm->year - 1900, tm->month - 1, tm->mday, tm->hour, tm->min, tm->sec );

  // multiply fractions by 100 to get nanoseconds
  // fractions should have 100ns resolution due to 10 MHz clock tick
  if ( mbg_rc_is_success( rc ) )
    nt->nano_secs = (int64_t)tm->frac * 100;

  return rc;

}  // tm_gps_to_nano_time_64

#endif  // !_IS_MBG_FIRMWARE && !defined( MBG_TGT_MISSING_64_BIT_TYPES )


