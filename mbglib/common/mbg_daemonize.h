
/**************************************************************************
 *
 *  $Id: mbg_daemonize.h 1.4 2021/03/26 09:57:14 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for mbg_daemonize.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_daemonize.h $
 *  Revision 1.4  2021/03/26 09:57:14  martin
 *  New global function pointer mbg_daemonize_log_fnc that initially
 *  points to syslog() and is used for all logging done by the module.
 *  Can be changed to point to a different function.
 *  Updated function prototypes and some comments.
 *  Revision 1.3  2017/05/10 15:21:37  martin
 *  Tiny cleanup.
 *  Revision 1.2  2013/07/30 15:30:49  martin
 *  Revision 1.1  2013/07/23 16:10:18  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_DAEMONIZE_H
#define _MBG_DAEMONIZE_H


/* Other headers to be included */

#include <mbg_syslog.h>
#include <mbg_signal.h>

#include <syslog.h>


#ifdef _MBG_DAEMONIZE
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if 0 && defined( _USE_PACK )  // Use default alignment.
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define DEFAULT_RUNNING_DIR  "/"


/**
 * @brief Pointer to the log function used by @a mbg_daemonize.
 *
 * Defaults to @a syslog, but can be changed to a different function
 * that is called in the same way.
 */
_ext MBG_SYSLOG_FNC *mbg_daemonize_log_fnc
#if defined( _DO_INIT )
  = syslog
#endif
;



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 void mbg_daemonize( const char *running_dir, MBG_REG_SIG_HANDLER_FNC *reg_sig_handler_func ) ;

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT

#endif  /* _MBG_DAEMONIZE_H */
