
/**************************************************************************
 *
 *  $Id: deviohlp.c 1.11 2022/08/24 16:25:29 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Device configuration helper functions. This is an extension to
 *    mbgdevio.c, providing useful functions to simplify reading/writing
 *    complex device configuration structure sets.
 *
 *  Warning:
 *    These functions should not be implemented in a DLL / shared library
 *    since the parameter sizes might vary with different versions
 *    of the API calls, which would make different versions of
 *    precompiled libraries incompatible to each other.
 *
 * -----------------------------------------------------------------------
 *  $Log: deviohlp.c $
 *  Revision 1.11  2022/08/24 16:25:29  martin.burnicki
 *  Removed obsolete function parameters.
 *  Revision 1.10  2022/06/30 09:09:59  martin.burnicki
 *  Prepare for variants of some functions, and some style cleanup.
 *  Revision 1.9  2021/03/22 17:57:27  martin
 *  Updated a bunch of comments.
 *  Revision 1.8  2020/02/27 13:37:09  martin
 *  Use some feature check functions instead of macros. This obsoletes
 *  the PCPS_DEV pointers that are expected by some functions.
 *  Account for renamed library symbol.
 *  Revision 1.7  2018/09/19 16:54:12  martin
 *  Account for renamed global variables.
 *  Revision 1.6  2018/07/05 08:23:51Z  martin
 *  Account for a renamed function in cfg_hlp.c.
 *  Added some comments.
 *  Revision 1.5  2018/04/24 16:10:46  martin
 *  Use predefined symbols to print ASIC version.
 *  Revision 1.4  2018/01/10 11:49:17  martin
 *  Removed usage of obsolete symbol MBGDEVIO_SIMPLE.
 *  Revision 1.3  2017/07/05 13:47:44  martin
 *  Many configuration functions provided by thomas-b,
 *  philipp, and martin.
 *  Revision 1.2  2012/10/15 13:48:35Z  martin
 *  Added functions mbg_get_all_ptp_cfg_info(), mbg_save_all_ptp_cfg_info().
 *  Account for renamed structure.
 *  Updated doxygen comments.
 *  Revision 1.1  2011/08/03 15:37:00Z  martin
 *  Initial revision with functions moved here from mbgdevio.
 *
 **************************************************************************/

#define _DEVIOHLP
  #include <deviohlp.h>
#undef _DEVIOHLP

#include <str_util.h>
#include <lan_util.h>
#include <stdio.h>


#define DEFAULT_BAUD_RATES_DCF \
(                              \
  MBG_PORT_HAS_300   |         \
  MBG_PORT_HAS_600   |         \
  MBG_PORT_HAS_1200  |         \
  MBG_PORT_HAS_2400  |         \
  MBG_PORT_HAS_4800  |         \
  MBG_PORT_HAS_9600            \
)


#define DEFAULT_BAUD_RATES_DCF_HS \
(                                 \
  DEFAULT_BAUD_RATES_DCF |        \
  MBG_PORT_HAS_19200     |        \
  MBG_PORT_HAS_38400              \
)


#define DEFAULT_FRAMINGS_DCF \
(                            \
  MBG_PORT_HAS_7E2 |         \
  MBG_PORT_HAS_8N1 |         \
  MBG_PORT_HAS_8N2 |         \
  MBG_PORT_HAS_8E1           \
)


/*HDR*/
int chk_feat_supp( MBG_DEV_HANDLE dh, MBG_CHK_SUPP_FNC *chk_supp_fnc,
                   MBG_ERR_MSG_FNC *err_msg_fnc, const char *not_supp_msg )
{
  const char *cp = NULL;
  int rc = MBG_SUCCESS;  // assume option is supported

  if ( chk_supp_fnc == NULL )  // no check function specified
    goto out;


  // A check function has been specified, so we must call it to check if
  // the specific parameter/command is supported by this particular device.

  if ( dh == MBG_INVALID_DEV_HANDLE )
  {
    cp = "Tried checking feature support with invalid device handle.";
    rc = MBG_ERR_INV_HANDLE;
    goto out;
  }

  rc = chk_supp_fnc( dh );

  if ( mbg_rc_is_success( rc ) )
    goto out;    // return with success


  if ( rc == MBG_ERR_NOT_SUPP_BY_DEV )
  {
    cp = not_supp_msg;
    goto fail;
  }


  cp = "Failed to check if supported";  // TODO Pproper error msg.

fail:
  if ( err_msg_fnc && cp )
    err_msg_fnc( cp );

out:
  return rc;

}  // chk_feat_supp



/*HDR*/
/**
 * @brief Read or setup all GNSS status information.
 *
 * This function should be called preferably to get a summary of
 * the GNSS status from GNSS receivers (GPS, GLONASS, ...).
 *
 * The function ::mbg_get_device_info must have been called before, and
 * the returned ::PCPS_DEV structure has to be passed to this function.
 *
 * If the device supports this, the low level GNSS API functions
 * are called directly to collect the status information. If the device
 * doesn't support the GNSS API but is a pure GPS receiver, the GPS
 * API functions are called and the GNSS data structures are filled up
 * accordingly, so the calling application can always evaluate the
 * GNSS data structures in ::ALL_GNSS_INFO.
 *
 * If neither GPS nor another GNSS system is supported, this function
 * returns the ::MBG_ERR_NOT_SUPP_BY_DEV error.
 *
 * @param[in]  dh     Valid handle to a Meinberg device.
 * @param[out] p_agi  Pointer to an ::ALL_GNSS_INFO to be filled.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_get_gps_stat_info
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_get_gps_all_gnss_sat_info
 */
int mbg_chk_get_all_gnss_info( MBG_DEV_HANDLE dh, ALL_GNSS_INFO *p_agi )
{
  int rc = MBG_ERR_UNSPEC;

  memset( p_agi, 0, sizeof( *p_agi ) );

  // First we check if the device is a GPS receiver,
  // which includes GNSS receivers.
  rc = mbg_chk_dev_is_gps( dh );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  // Read the STAT_INFO structure which is supported by all GPS
  // and GNSS receivers.
  rc = mbg_get_gps_stat_info( dh, &p_agi->stat_info );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  // Check if the device is a GNSS receiver, i.e. can track satellites
  // from different systems (GPS, GLONASS, Beidou, ...).
  rc = mbg_chk_dev_is_gnss( dh );

  if ( mbg_rc_is_success( rc ) )
  {
    // The device is a GNSS receiver, i.e. it can track satellites
    // from different systems (GPS, GLONASS, Beidou, ...).
    // Read some specific GNSS information.
    rc = mbg_get_gps_gnss_mode_info( dh, &p_agi->gnss_mode_info );

    if ( mbg_rc_is_error( rc ) )
      goto out;


    rc = chk_set_n_gnss_supp( p_agi );

    if ( mbg_rc_is_error( rc ) )
      goto out;


    // If the the device supports the current API, we can simply
    // retrieve status information for each individual GNSS system.
    rc = mbg_get_gps_all_gnss_sat_info( dh, p_agi->gnss_sat_info_idx, &p_agi->gnss_mode_info );
    goto out;
  }


  // If we get here, the device is a pure GPS receiver
  // which neither supports additional GNSS systems nor the
  // associated data structures, so we set up all GNSS
  // data structures for GPS only.
  rc = setup_gps_only_gnss_mode_info_from_stat_info( p_agi );

out:
  return rc;

}  // mbg_chk_get_all_gnss_info



/*HDR*/
/**
 * @brief Read all serial port settings and supported configuration parameters.
 *
 * The functions ::mbg_get_device_info and ::mbg_setup_receiver_info
 * must have been called before, and the returned ::PCPS_DEV and
 * ::RECEIVER_INFO structures have to be passed to this function.
 *
 * The complementary function ::mbg_save_serial_settings should be used
 * to write the modified serial port configuration back to the device.
 *
 * @param[in]  dh       Valid handle to a Meinberg device.
 * @param[out] p_rpcfg  Pointer to a ::RECEIVER_PORT_CFG structure to be filled up.
 * @param[in]  p_ri     Pointer to a valid ::RECEIVER_INFO structure.
 *
 * @return ::MBG_SUCCESS, or error code returned by device I/O control function.
 *
 * @see ::mbg_get_device_info
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_save_serial_settings
 */
int mbg_get_serial_settings( MBG_DEV_HANDLE dh,
                             RECEIVER_PORT_CFG *p_rpcfg,
                             const RECEIVER_INFO *p_ri )
{
  int rc;

  memset( p_rpcfg, 0, sizeof( *p_rpcfg ) );

  if ( mbg_rc_is_success( mbg_chk_dev_has_receiver_info( dh ) ) )
  {
    // The device provides a RECEIVER_INFO, so we can simply read all
    // serial port configuration and supported string types directly.

    rc = mbg_get_gps_all_port_info( dh, p_rpcfg->pii, p_ri );

    if ( mbg_rc_is_error( rc ) )
      goto out;

    rc = mbg_get_gps_all_str_type_info( dh, p_rpcfg->stii, p_ri );

    if ( mbg_rc_is_error( rc ) )
      goto out;
  }
  else
  {
    // The device doesn't support a RECEIVER_INFO, so this is
    // an old GPS or non-GPS device.

    if ( mbg_rc_is_success( mbg_chk_dev_is_gps( dh ) ) )
    {
      // Read the serial port configuration from an old GPS device using
      // a legacy API call and set up current structures accordingly.

      rc = mbg_get_gps_port_parm( dh, &p_rpcfg->tmp_pp );

      if ( mbg_rc_is_error( rc ) )
        goto out;

      rc = setup_port_info_from_port_parm( p_rpcfg->pii, &p_rpcfg->tmp_pp, p_ri );

      if ( mbg_rc_is_error( rc ) )
        goto out;

      // Also set up default string type information.
      rc = setup_default_str_type_info_idx( p_rpcfg->stii, p_ri );
    }
    else
    {
      // Not all legacy non-GPS devices have an onbord serial port.
      if ( mbg_rc_is_success( mbg_chk_dev_has_serial( dh ) ) )
      {
        // Read the serial port configuration from an old non-GPS device using
        // a legacy API call and set up current structures accordingly.

        PCPS_SERIAL ser_code;

        rc = mbg_get_serial( dh, &ser_code );

        if ( mbg_rc_is_error( rc ) )
          goto out;

        port_info_from_pcps_serial( p_rpcfg->pii, ser_code,
                                    mbg_rc_is_success( mbg_chk_dev_has_serial_hs( dh ) ) ?
                                      DEFAULT_BAUD_RATES_DCF_HS :
                                      DEFAULT_BAUD_RATES_DCF
                                 );
        // Also set up default string type information.
        rc = setup_default_str_type_info_idx( p_rpcfg->stii, p_ri );
      }
    }

  }

  rc = MBG_SUCCESS;

out:
  return rc;

}  // mbg_get_serial_settings



/*HDR*/
/**
 * @brief Write the configuration settings for a single serial port to a device.
 *
 * Modifications to the serial port configuration should be made only
 * after ::mbg_get_serial_settings had been called to read all serial port
 * settings and supported configuration parameters.
 * This function has finally to be called once for every serial port
 * the configuration of which has been modified.
 *
 * As also required by ::mbg_get_serial_settings, the functions
 * ::mbg_get_device_info and ::mbg_setup_receiver_info must have been
 * called before, and the returned ::PCPS_DEV and ::RECEIVER_INFO structures
 * have to be passed to this function.
 *
 * @param[in]  dh        Valid handle to a Meinberg device.
 * @param[in]  p_rpcfg   Pointer to a valid ::RECEIVER_PORT_CFG structure.
 * @param[in]  port_num  Index of the serial port to be saved.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_serial_settings
 * @see ::mbg_get_device_info
 * @see ::mbg_setup_receiver_info
 */
int mbg_save_serial_settings( MBG_DEV_HANDLE dh, RECEIVER_PORT_CFG *p_rpcfg,
                              int port_num )
{
  int rc;

  if ( mbg_rc_is_success( mbg_chk_dev_has_receiver_info( dh ) ) )
  {
    // The device provides a ::RECEIVER_INFO, so we can simply write
    // the configuration for the specified serial port directly.
    rc = mbg_set_gps_port_settings( dh, &p_rpcfg->pii[port_num].port_info.port_settings, port_num );
  }
  else
  {
    // This is a legacy device which doesn't provide a ::RECEIVER_INFO.
    if ( mbg_rc_is_success( mbg_chk_dev_is_gps( dh ) ) )
    {
      // If the legacy device is a GPS receiver, we have to set up
      // an appropriate ::PORT_PARM structure and write that one
      // to the device.
      port_parm_from_port_settings( &p_rpcfg->tmp_pp, port_num,
                          &p_rpcfg->pii[port_num].port_info.port_settings, 1 );

      rc = mbg_set_gps_port_parm( dh, &p_rpcfg->tmp_pp );
    }
    else
    {
      // If the legacy device is not a GPS receiver, we have
      // to set up a ::PCPS_SERIAL configuration byte and write
      // that one to the device.
      PCPS_SERIAL ser_code;

      pcps_serial_from_port_info( &ser_code, p_rpcfg->pii );

      rc = mbg_set_serial( dh, &ser_code );
    }
  }

  return rc;

}  // mbg_save_serial_settings



/*HDR*/
/**
 * @brief Read all network configuration into an ::ALL_NET_CFG_INFO structure.
 *
 * Reads the network configuration of a device via the LAN_IP4 API and
 * translates the structures into NET_CFG structures.
 *
 * As soon as available, this function should make use of the NET_CFG API.
 *
 * An ::ALL_NET_CFG_INFO and the appropriate number of ::MBG_NET_INTF_LINK_INFO_IDX,
 * ::MBG_NET_INTF_ADDR_INFO_IDX, ::MBG_IP_ADDR_IDX, ::MBG_NET_NAME_IDX and
 * ::MBG_NET_INTF_ROUTE_INFO_IDX will be allocated and need to be freed later
 * by calling ::free_all_net_cfg_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_NET_CFG_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_settings
 * @see ::free_all_net_cfg_info
 */
int mbg_get_all_net_cfg_info( MBG_DEV_HANDLE dh, ALL_NET_CFG_INFO **p )
{
  ALL_NET_CFG_INFO *net_cfg_info = *p;
  int rc;

  if ( net_cfg_info == NULL )
  {
    net_cfg_info = ( ALL_NET_CFG_INFO * )calloc( 1, sizeof( *net_cfg_info ) );
    if ( net_cfg_info == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  // TODO Use NET_CFG API as soon as it's available for PCI/PCIe devices.
  rc = mbg_chk_dev_has_lan_intf( dh );

  if ( mbg_rc_is_success( rc ) )
  {
    LAN_IF_INFO lan_if_info;
    IP4_SETTINGS ip4_settings;

    if ( net_cfg_info->glb_cfg_info.n_supp_intf_link != 0 )
      goto out;

    rc = mbg_get_lan_if_info( dh, &lan_if_info );

    if ( mbg_rc_is_error( rc ) )
      goto out;

    if ( net_cfg_info->link_infos == NULL )
    {
      net_cfg_info->link_infos = ( MBG_NET_INTF_LINK_INFO_IDX * )calloc( 1, sizeof( *net_cfg_info->link_infos ) );
      if ( net_cfg_info->link_infos == NULL )
      {
        rc = MBG_ERR_NO_MEM;
        goto out;
      }
    }

    net_cfg_info->glb_cfg_info.glb_settings.num_intf_link = 1;

    memset( &net_cfg_info->link_infos[0], 0, sizeof( net_cfg_info->link_infos[0] ) );

    net_cfg_info->link_infos[0].info.supp_states = MBG_NET_INTF_LINK_STATE_MASK_UP;
    net_cfg_info->link_infos[0].info.supp_types = MBG_NET_INTF_LINK_TYPE_MASK_PHYS | MBG_NET_INTF_LINK_TYPE_MASK_VLAN;
    net_cfg_info->link_infos[0].info.supp_speed_modes = MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_FULL | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_FULL;
    net_cfg_info->link_infos[0].info.supp_port_types = MBG_NET_INTF_LINK_PORT_TYPE_MASK_TP;

    snprintf_safe( net_cfg_info->link_infos[0].info.link_settings.name, sizeof( net_cfg_info->link_infos[0].info.link_settings.name ), "lan0" );
    net_cfg_info->link_infos[0].info.link_settings.mac_addr = lan_if_info.mac_addr;
    net_cfg_info->link_infos[0].info.link_settings.if_index = 0;
    net_cfg_info->link_infos[0].info.link_settings.type = MBG_NET_INTF_LINK_TYPE_PHYS;
    net_cfg_info->link_infos[0].info.link_settings.port_type = MBG_NET_INTF_LINK_PORT_TYPE_TP;

    if ( net_cfg_info->glb_cfg_info.n_supp_intf_addr != 0 )
      goto out;

    rc = mbg_get_ip4_settings( dh, &ip4_settings );

    if ( mbg_rc_is_error( rc ) )
      goto out;

    if( ip4_settings.flags & IP4_MSK_LINK )
    {
      net_cfg_info->link_infos->info.link_settings.states |= MBG_NET_INTF_LINK_STATE_MASK_UP;
      net_cfg_info->link_infos->info.link_settings.states |= MBG_NET_INTF_LINK_STATE_MASK_LOWER_UP;
    }

    if ( net_cfg_info->addr_infos == NULL )
    {
      net_cfg_info->addr_infos = ( MBG_NET_INTF_ADDR_INFO_IDX * )calloc( 1, sizeof( *net_cfg_info->addr_infos ) );
      if ( net_cfg_info->addr_infos == NULL )
      {
        rc = MBG_ERR_NO_MEM;
        goto out;
      }
    }

    net_cfg_info->glb_cfg_info.glb_settings.num_intf_addr = 1;

    memset( &net_cfg_info->addr_infos[0], 0, sizeof( net_cfg_info->addr_infos[0] ) );

    net_cfg_info->addr_infos[0].info.supp_flags = MBG_NET_INTF_ADDR_MASK_DHCP4;

    snprintf_safe( net_cfg_info->addr_infos[0].info.addr_settings.label, sizeof( net_cfg_info->addr_infos[0].info.addr_settings.label ), "lan0:0" );

    if ( ip4_settings.flags & IP4_MSK_VLAN )
    {
      net_cfg_info->link_infos = ( MBG_NET_INTF_LINK_INFO_IDX * )realloc( net_cfg_info->link_infos, 2 * sizeof( *net_cfg_info->link_infos ) );
      if ( net_cfg_info->link_infos == NULL )
      {
        rc = MBG_ERR_NO_MEM;
        goto out;
      }

      net_cfg_info->glb_cfg_info.glb_settings.num_intf_link = 2;

      memset(&net_cfg_info->link_infos[1], 0, sizeof(net_cfg_info->link_infos[1]));

      net_cfg_info->link_infos[1].idx = 1;
      net_cfg_info->link_infos[1].info.supp_states = MBG_NET_INTF_LINK_STATE_MASK_UP;
      net_cfg_info->link_infos[1].info.supp_types = MBG_NET_INTF_LINK_TYPE_MASK_VLAN;
      net_cfg_info->link_infos[1].info.supp_speed_modes = MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_FULL | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_FULL;
      net_cfg_info->link_infos[1].info.supp_port_types = MBG_NET_INTF_LINK_PORT_TYPE_MASK_TP;

      snprintf_safe( net_cfg_info->link_infos[1].info.link_settings.name, sizeof( net_cfg_info->link_infos[1].info.link_settings.name ), "vlan0" );
      net_cfg_info->link_infos[1].info.link_settings.mac_addr = net_cfg_info->link_infos[0].info.link_settings.mac_addr;
      net_cfg_info->link_infos[1].info.link_settings.if_index = 1;
      net_cfg_info->link_infos[1].info.link_settings.ass_if_index = 0;
      net_cfg_info->link_infos[1].info.link_settings.states = net_cfg_info->link_infos[0].info.link_settings.states;
      net_cfg_info->link_infos[1].info.link_settings.type = MBG_NET_INTF_LINK_TYPE_VLAN;
      net_cfg_info->link_infos[1].info.link_settings.vlan_cfg = ip4_settings.vlan_cfg;

      net_cfg_info->addr_infos[0].info.addr_settings.ass_if_index = 1;
    }

    if ( ip4_settings.flags & IP4_MSK_DHCP )
      net_cfg_info->addr_infos[0].info.addr_settings.flags |= MBG_NET_INTF_ADDR_MASK_DHCP4;

    net_cfg_info->addr_infos[0].info.addr_settings.ip.type = MBG_IP_ADDR_TYPE_IP4;
    net_cfg_info->addr_infos[0].info.addr_settings.ip.u_addr.ip4_addr = ip4_settings.ip_addr;

    net_cfg_info->addr_infos[0].info.addr_settings.broadcast.type = MBG_IP_ADDR_TYPE_IP4;
    net_cfg_info->addr_infos[0].info.addr_settings.broadcast.u_addr.ip4_addr = ip4_settings.broad_addr;

    net_cfg_info->addr_infos[0].info.addr_settings.prefix_bits = get_ip4_net_mask_bits(&ip4_settings.netmask);

    if ( net_cfg_info->glb_cfg_info.n_supp_intf_route == 0 )
    {
      if ( net_cfg_info->route_infos == NULL )
      {
        net_cfg_info->route_infos = ( MBG_NET_INTF_ROUTE_INFO_IDX * )calloc( 1, sizeof( *net_cfg_info->route_infos ) );
        if ( net_cfg_info->route_infos == NULL )
        {
          rc = MBG_ERR_NO_MEM;
          goto out;
        }
      }

      net_cfg_info->glb_cfg_info.glb_settings.num_intf_route = 1;

      memset( &net_cfg_info->route_infos[0], 0, sizeof( net_cfg_info->route_infos[0] ) );

      net_cfg_info->route_infos[0].info.route_settings.type = MBG_NET_INTF_ROUTE_TYPE_DEFAULT_GATEWAY;
      net_cfg_info->route_infos[0].info.route_settings.gateway.type = MBG_IP_ADDR_TYPE_IP4;
      net_cfg_info->route_infos[0].info.route_settings.gateway.u_addr.ip4_addr = ip4_settings.gateway;
      if ( ip4_settings.gateway != 0 )
      {
        if(net_cfg_info->glb_cfg_info.glb_settings.num_intf_link == 2)
          net_cfg_info->route_infos[0].info.route_settings.ass_if_index = 1;
      }
      else net_cfg_info->route_infos[0].info.route_settings.ass_if_index = (uint32_t)-1;

    }

  }

out:
  if ( mbg_rc_is_error( rc ) )
  {
    free_all_net_cfg_info( net_cfg_info );
    net_cfg_info = NULL;
  }

  *p = net_cfg_info;

  return rc;

}  // mbg_get_all_net_cfg_info



/*HDR*/
/**
 * @brief Write all network settings to a device.
 *
 * The complementary function ::mbg_get_all_net_cfg_info should
 * have been used to read the original network settings and
 * supported configuration parameters.
 *
 * The appropriate settings are translated into LAN_IP4 structures
 * and send to the device using the appropriate API functions.
 *
 * As soon as available, this function should make use of the NET_CFG API.
 *
 * @param[in]  dh  Valid handle to a Meinberg device.
 * @param[in]  p   Pointer to a pointer of ::ALL_NET_CFG_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_set_ip4_settings
 */
int mbg_save_all_net_cfg_info( MBG_DEV_HANDLE dh, ALL_NET_CFG_INFO *p )
{
  // TODO Use NET_CFG API as soon as it's available for PCI/PCIe devices.
  int rc = mbg_chk_dev_has_lan_intf( dh );

  if ( mbg_rc_is_success( rc ) )
  {
    IP4_SETTINGS ip4_settings;
    memset( &ip4_settings, 0, sizeof( ip4_settings ) );

    if ( p->addr_infos[0].info.addr_settings.ip.type == MBG_IP_ADDR_TYPE_IP4 )
      ip4_settings.ip_addr = p->addr_infos[0].info.addr_settings.ip.u_addr.ip4_addr;

    ip4_settings.netmask = ip4_net_mask_from_cidr( p->addr_infos[0].info.addr_settings.prefix_bits );
    ip4_settings.broad_addr = ip4_broad_addr_from_addr( &ip4_settings.ip_addr, &ip4_settings.netmask );

    if ( p->route_infos[0].info.route_settings.gateway.type == MBG_IP_ADDR_TYPE_IP4 )
      ip4_settings.gateway = p->route_infos[0].info.route_settings.gateway.u_addr.ip4_addr;

    if ( ( p->glb_cfg_info.glb_settings.num_intf_link > 1 ) && ( p->addr_infos[0].info.addr_settings.ass_if_index == 1 ) )
    {
      ip4_settings.flags |= IP4_MSK_VLAN;
      ip4_settings.vlan_cfg = p->link_infos[1].info.link_settings.vlan_cfg;
    }

    if ( ( p->addr_infos[0].info.addr_settings.flags & MBG_NET_INTF_ADDR_MASK_DHCP4 ) == MBG_NET_INTF_ADDR_MASK_DHCP4 )
      ip4_settings.flags |= IP4_MSK_DHCP;

    rc = mbg_set_ip4_settings( dh, &ip4_settings );
  }

  return rc;

}  // mbg_save_all_net_cfg_info



/*HDR*/
/**
 * @brief Read current network status into an ::ALL_NET_STATUS_INFO structure.
 *
 * Reads the network status of a device via the LAN_IP4 API and
 * translates the structures into NET_CFG structures.
 *
 * As soon as available, this function should make use of the NET_CFG API.
 *
 * An ::ALL_NET_STATUS_INFO and the appropriate number of ::MBG_NET_INTF_LINK_INFO_IDX,
 * ::MBG_NET_INTF_ADDR_INFO_IDX, ::MBG_IP_ADDR_IDX, ::MBG_NET_NAME_IDX and
 * ::MBG_NET_INTF_ROUTE_INFO_IDX will be allocated and need to be freed later
 * by calling ::free_all_net_status_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_NET_STATUS_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_state
 * @see ::free_all_net_status_info
 */
int mbg_get_all_net_status_info( MBG_DEV_HANDLE dh, ALL_NET_STATUS_INFO **p )
{
  ALL_NET_STATUS_INFO *net_status_info = *p;
  int rc;

  if ( net_status_info == NULL )
  {
    net_status_info = ( ALL_NET_STATUS_INFO * )calloc( 1, sizeof( *net_status_info ) );
    if ( net_status_info == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  // TODO Use NET_CFG API as soon as it's available for PCI/PCIe devices.
  rc = mbg_chk_dev_has_lan_intf( dh );

  if ( mbg_rc_is_success( rc ) )
  {
    LAN_IF_INFO lan_if_info;
    IP4_SETTINGS ip4_state;

    if ( net_status_info->glb_cfg_info.n_supp_intf_link != 0 )
      goto out;

    rc = mbg_get_lan_if_info( dh, &lan_if_info );

    if ( mbg_rc_is_error( rc ) )
      goto out;

    if ( net_status_info->link_infos == NULL )
    {
      net_status_info->link_infos = ( MBG_NET_INTF_LINK_INFO_IDX * )calloc( 1, sizeof( *net_status_info->link_infos ) );
      if ( net_status_info->link_infos == NULL )
      {
        rc = MBG_ERR_NO_MEM;
        goto out;
      }
    }

    net_status_info->glb_cfg_info.glb_settings.num_intf_addr = 1;

    memset( &net_status_info->link_infos[0], 0, sizeof( net_status_info->link_infos[0] ) );

    net_status_info->link_infos[0].info.supp_states = MBG_NET_INTF_LINK_STATE_MASK_UP;
    net_status_info->link_infos[0].info.supp_types = MBG_NET_INTF_LINK_TYPE_MASK_PHYS | MBG_NET_INTF_LINK_TYPE_MASK_VLAN;
    net_status_info->link_infos[0].info.supp_speed_modes = MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_FULL | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_FULL;
    net_status_info->link_infos[0].info.supp_port_types = MBG_NET_INTF_LINK_PORT_TYPE_MASK_TP;

    snprintf_safe( net_status_info->link_infos[0].info.link_settings.name, sizeof( net_status_info->link_infos[0].info.link_settings.name ), "lan0" );
    net_status_info->link_infos[0].info.link_settings.mac_addr = lan_if_info.mac_addr;
    net_status_info->link_infos[0].info.link_settings.if_index = 0;
    net_status_info->link_infos[0].info.link_settings.type = MBG_NET_INTF_LINK_TYPE_PHYS;
    net_status_info->link_infos[0].info.link_settings.port_type = MBG_NET_INTF_LINK_PORT_TYPE_TP;

    if ( net_status_info->glb_cfg_info.n_supp_intf_addr != 0 )
      goto out;

    rc = mbg_get_ip4_state( dh, &ip4_state );

    if ( mbg_rc_is_error( rc ) )
      goto out;

    if( ip4_state.flags & IP4_MSK_LINK )
    {
      net_status_info->link_infos[0].info.link_settings.states |= MBG_NET_INTF_LINK_STATE_MASK_UP;
      net_status_info->link_infos[0].info.link_settings.states |= MBG_NET_INTF_LINK_STATE_MASK_LOWER_UP;
    }

    if ( net_status_info->addr_infos == NULL )
    {
      net_status_info->addr_infos = ( MBG_NET_INTF_ADDR_INFO_IDX * )realloc( net_status_info->addr_infos, sizeof( *net_status_info->addr_infos ) );
      if ( net_status_info->addr_infos == NULL )
      {
        rc = MBG_ERR_NO_MEM;
        goto out;
      }
    }

    net_status_info->glb_cfg_info.glb_settings.num_intf_addr = 1;

    memset( &net_status_info->addr_infos[0], 0, sizeof( net_status_info->addr_infos[0] ) );

    net_status_info->addr_infos[0].info.supp_flags = MBG_NET_INTF_ADDR_MASK_DHCP4;

    snprintf_safe( net_status_info->addr_infos[0].info.addr_settings.label, sizeof( net_status_info->addr_infos[0].info.addr_settings.label ), "lan0:0" );

    if ( ip4_state.flags & IP4_MSK_VLAN )
    {
      net_status_info->link_infos = ( MBG_NET_INTF_LINK_INFO_IDX * )realloc( net_status_info->link_infos, 2 * sizeof( *net_status_info->link_infos ) );
      if ( net_status_info->link_infos == NULL )
      {
        rc = MBG_ERR_NO_MEM;
        goto out;
      }

      net_status_info->glb_cfg_info.glb_settings.num_intf_link = 2;

      memset(&net_status_info->link_infos[1], 0, sizeof(net_status_info->link_infos[1]));

      net_status_info->link_infos[1].idx = 1;
      net_status_info->link_infos[1].info.supp_states = MBG_NET_INTF_LINK_STATE_MASK_UP;
      net_status_info->link_infos[1].info.supp_types = MBG_NET_INTF_LINK_TYPE_MASK_VLAN;
      net_status_info->link_infos[1].info.supp_speed_modes = MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_10_T_FULL | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_HALF | MBG_NET_INTF_LINK_SPEED_MODE_MASK_100_T_FULL;
      net_status_info->link_infos[1].info.supp_port_types = MBG_NET_INTF_LINK_PORT_TYPE_MASK_TP;

      snprintf_safe( net_status_info->link_infos[1].info.link_settings.name, sizeof( net_status_info->link_infos[1].info.link_settings.name ), "vlan0" );
      net_status_info->link_infos[1].info.link_settings.mac_addr = net_status_info->link_infos[0].info.link_settings.mac_addr;
      net_status_info->link_infos[1].info.link_settings.if_index = 1;
      net_status_info->link_infos[1].info.link_settings.ass_if_index = 0;
      net_status_info->link_infos[1].info.link_settings.states = net_status_info->link_infos[0].info.link_settings.states;
      net_status_info->link_infos[1].info.link_settings.type = MBG_NET_INTF_LINK_TYPE_VLAN;
      net_status_info->link_infos[1].info.link_settings.vlan_cfg = ip4_state.vlan_cfg;

      net_status_info->addr_infos[0].info.addr_settings.ass_if_index = 1;
    }

    if ( ip4_state.flags & IP4_MSK_DHCP )
      net_status_info->addr_infos[0].info.addr_settings.flags |= MBG_NET_INTF_ADDR_MASK_DHCP4;

    net_status_info->addr_infos[0].info.addr_settings.ip.type = MBG_IP_ADDR_TYPE_IP4;
    net_status_info->addr_infos[0].info.addr_settings.ip.u_addr.ip4_addr = ip4_state.ip_addr;

    net_status_info->addr_infos[0].info.addr_settings.broadcast.type = MBG_IP_ADDR_TYPE_IP4;
    net_status_info->addr_infos[0].info.addr_settings.broadcast.u_addr.ip4_addr = ip4_state.broad_addr;

    net_status_info->addr_infos[0].info.addr_settings.prefix_bits = get_ip4_net_mask_bits(&ip4_state.netmask);

    if ( net_status_info->glb_cfg_info.n_supp_intf_route == 0 )
    {
      if ( net_status_info->route_infos == NULL )
      {
        net_status_info->route_infos = ( MBG_NET_INTF_ROUTE_INFO_IDX * )calloc( 1, sizeof( *net_status_info->route_infos ) );
        if ( net_status_info->route_infos == NULL )
        {
          rc = MBG_ERR_NO_MEM;
          goto out;
        }
      }

      net_status_info->glb_cfg_info.glb_settings.num_intf_route = 1;

      memset( &net_status_info->route_infos[0], 0, sizeof( net_status_info->route_infos[0] ) );

      net_status_info->route_infos[0].info.route_settings.type = MBG_NET_INTF_ROUTE_TYPE_DEFAULT_GATEWAY;
      net_status_info->route_infos[0].info.route_settings.gateway.type = MBG_IP_ADDR_TYPE_IP4;
      net_status_info->route_infos[0].info.route_settings.gateway.u_addr.ip4_addr = ip4_state.gateway;
      if ( ip4_state.gateway != 0 )
      {
        if(net_status_info->glb_cfg_info.glb_settings.num_intf_link == 2)
          net_status_info->route_infos[0].info.route_settings.ass_if_index = 1;
      }
      else net_status_info->route_infos[0].info.route_settings.ass_if_index = (uint32_t)-1;

    }

  }

out:
  if ( mbg_rc_is_error( rc ) )
  {
    free_all_net_status_info( net_status_info );
    net_status_info = NULL;
  }

  *p = net_status_info;

  return rc;

}  // mbg_get_all_net_status_info



/*HDR*/
/**
 * @brief Read all PTP settings and supported configuration parameters.
 *
 * The complementary function ::mbg_save_all_ptp_cfg_info should
 * be used to write the modified configuration back to the device.
 *
 * @param[in]  dh  Valid handle to a Meinberg device.
 * @param[out] p   Address of an ::ALL_PTP_CFG_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_save_all_ptp_cfg_info
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
int mbg_get_all_ptp_cfg_info( MBG_DEV_HANDLE dh, ALL_PTP_CFG_INFO *p )
{
  int rc = MBG_SUCCESS;

  memset( p, 0, sizeof( *p ) );

  rc = mbg_get_ptp_cfg_info( dh, &p->ptp_cfg_info );

  if ( rc < 0 )
    return rc;

  if ( p->ptp_cfg_info.supp_flags & PTP_CFG_MSK_SUPPORT_PTP_UNICAST )
  {
    rc = mbg_get_ptp_uc_master_cfg_limits( dh, &p->ptp_uc_master_cfg_limits );

    if ( rc < 0 )
      return rc;

    if ( p->ptp_uc_master_cfg_limits.n_supp_master > MAX_PARM_PTP_UC_MASTER )
    {
      // The number of PTP unicast masters supported by this device
      // exceeds the number of unicast masters supporterd by this driver.
      return MBG_ERR_N_UC_MSTR_EXCEEDS_SUPP;
    }

    rc = mbg_get_all_ptp_uc_master_info( dh, p->all_ptp_uc_master_info_idx,
                                         &p->ptp_uc_master_cfg_limits );
    if ( rc < 0 )
      return rc;
  }

  return MBG_SUCCESS;

}  // mbg_get_all_ptp_cfg_info



/*HDR*/
/**
 * @brief Write all PTP settings to a device.
 *
 * The complementary function ::mbg_get_all_ptp_cfg_info should
 * have been used to read the original PTP settings and supported
 * configuration parameters.
 *
 * @param[in] dh  Valid handle to a Meinberg device.
 * @param[in] p   Pointer to a valid ::ALL_PTP_CFG_INFO structure.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
int mbg_save_all_ptp_cfg_info( MBG_DEV_HANDLE dh, const ALL_PTP_CFG_INFO *p )
{
  int rc = MBG_SUCCESS;

  rc = mbg_set_ptp_cfg_settings( dh, &p->ptp_cfg_info.settings );

  if ( rc < 0 )
    return rc;


  if ( p->ptp_cfg_info.supp_flags & PTP_CFG_MSK_SUPPORT_PTP_UNICAST )
  {
    int i;

    for ( i = 0; i < p->ptp_uc_master_cfg_limits.n_supp_master; i++ )
    {
      PTP_UC_MASTER_SETTINGS_IDX s;

      memset( &s, 0, sizeof( s ) );

      s.idx = i;
      s.settings = p->all_ptp_uc_master_info_idx[i].info.settings;

      rc = mbg_set_ptp_uc_master_settings_idx( dh, &s );

      if ( rc < 0 )
        return rc;
    }
  }

  return MBG_SUCCESS;

}  // mbg_save_all_ptp_cfg_info



/*HDR*/
/**
 * @brief Read all XMR info into a newly or re-allocated ::ALL_XMULTI_REF_INFO.
 *
 * @note ::mbg_chk_dev_has_xmr should be called before using this function.
 *
 * An ::ALL_XMULTI_REF_INFO and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_INFO_IDX and ::XMR_EXT_SRC_INFO_IDX will be allocated and need
 * to be freed by calling ::free_all_xmulti_ref_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_XMULTI_REF_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::free_all_xmulti_ref_info
 */
int mbg_get_all_xmulti_ref_info( MBG_DEV_HANDLE dh, ALL_XMULTI_REF_INFO **p )
{
  ALL_XMULTI_REF_INFO *xmr_info = *p;
  size_t data_sz;
  int rc;

  if ( xmr_info == NULL )
  {
    xmr_info = (ALL_XMULTI_REF_INFO *) calloc( 1, sizeof( *xmr_info ) );

    if ( xmr_info == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  // First, get the XMULTI_REF_INSTANCES to check how many sources are supported.
  rc = mbg_get_xmr_instances( dh, &xmr_info->instances );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  data_sz = xmr_info->instances.n_xmr_settings * sizeof( *xmr_info->infos );

  if ( xmr_info->infos == NULL )
  {
    xmr_info->infos = (XMULTI_REF_INFO_IDX *) calloc( 1, data_sz );

    if ( xmr_info->infos == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  rc = mbg_get_gps_all_xmr_info( dh, xmr_info->infos, &xmr_info->instances );

  if ( mbg_rc_is_error( rc ) )
    goto out;

  if ( mbg_rc_is_success( chk_dev_xmulti_ref_supp_ext_src_info( xmr_info ) ) )
  {
    // TODO
    // XMR_EXT_SRC_INFO_IDX can not yet be read from bus devices.
    // Therefore, remove the feature bit from this card.
    // Has to be changed as soon as low level functions are ready
    // TODO
    xmr_info->instances.flags &= ~XMRIF_MSK_EXT_SRC_INFO_SUPP;
  }

out:
  if ( mbg_rc_is_error( rc ) )
  {
    free_all_xmulti_ref_info( xmr_info );
    xmr_info = NULL;
  }

  *p = xmr_info;

  return rc;

}  // mbg_get_all_xmulti_ref_info



/*HDR*/
/**
 * @brief Write all extended multi ref settings to a device.
 *
 * The complementary function ::mbg_get_all_xmulti_ref_info should
 * have been used to read the original extended multi ref info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::ALL_XMULTI_REF_INFO structure with all settings.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_set_gps_xmr_settings_idx
 */
_NO_MBG_API_ATTR int _MBG_API mbg_save_all_xmulti_ref_info( MBG_DEV_HANDLE dh,
                                                            ALL_XMULTI_REF_INFO *p )
{
  int rc = MBG_SUCCESS, i;
  XMULTI_REF_SETTINGS_IDX settings;

  for ( i = 0; ( i < p->instances.n_xmr_settings ) && mbg_rc_is_success( rc ); i++ )
  {
    settings.idx = i;
    settings.settings = p->infos[i].info.settings;
    rc = mbg_set_gps_xmr_settings_idx( dh, &settings );
  }

  // After all settings have been successully written, write
  // a dummy structure with index -1 to apply settings.
  if ( mbg_rc_is_success( rc ) )
  {
    memset( &settings, 0, sizeof( settings ) );
    settings.idx = -1;
    settings.settings.id.type = MULTI_REF_NONE;
    rc = mbg_set_gps_xmr_settings_idx( dh, &settings );
  }

  return rc;

}  // mbg_save_all_xmulti_ref_info



/*HDR*/
/**
 * @brief Read all XMR status info into a newly or re-allocated ::ALL_XMULTI_REF_STATUS.
 *
 * @note ::mbg_chk_dev_has_xmr should be called before using this function.
 *
 * An ::ALL_XMULTI_REF_STATUS and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_STATUS_IDX will be allocated and need to be freed by calling
 * ::free_all_xmulti_ref_status.
 *
 * @param[in]   dh    Valid handle to a Meinberg device.
 * @param[in]   info  Pointer to the appropriate info structure.
 * @param[out]  p     Pointer to a pointer of ::ALL_XMULTI_REF_STATUS.
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::free_all_xmulti_ref_status
 */
int mbg_get_all_xmulti_ref_status( MBG_DEV_HANDLE dh, const ALL_XMULTI_REF_INFO *info,
                                   ALL_XMULTI_REF_STATUS **p )
{
  ALL_XMULTI_REF_STATUS *xmr_status = *p;
  size_t data_sz;
  int rc;

  if ( info == NULL )
    return MBG_ERR_INV_PARM;

  if ( xmr_status == NULL )
  {
    xmr_status = (ALL_XMULTI_REF_STATUS *) calloc( 1, sizeof( *xmr_status ) );

    if ( xmr_status == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  data_sz = info->instances.n_xmr_settings * sizeof( *xmr_status->status );

  if ( xmr_status->status == NULL )
  {
    xmr_status->status = (XMULTI_REF_STATUS_IDX *) calloc( 1, data_sz );

    if ( xmr_status->status == NULL )
    {
      rc = MBG_ERR_NO_MEM;
      goto out;
    }
  }

  rc = mbg_get_gps_all_xmr_status( dh, xmr_status->status, &info->instances );

  if ( mbg_rc_is_error( rc ) )
    goto out;

  #if 0
    // TODO Some more detailed status info could be read here,
    // if supported by bus level devices, using code similar to this:

    if ( mbg_rc_is_success( chk_dev_xmulti_ref_supp_...() ) )
    {
      ...
    }
  #endif

out:
  if ( mbg_rc_is_error( rc ) )
  {
    free_all_xmulti_ref_status( xmr_status );
    xmr_status = NULL;
  }

  *p = xmr_status;

  return rc;

}  // mbg_get_all_xmulti_ref_status



/*HDR*/
/**
 * @brief Read all user capture information and store it into a newly allocated or reused ::ALL_UCAP_INFO.
 *
 * @note ::mbg_chk_dev_has_ucap should be called to check if this API is supported.
 *
 * The appropriate number of ::TTM structures will be allocated and needs to be freed
 * by calling ::free_all_ucap_info. Existing user captures will not be removed, so the
 * number of user captures can never decrease.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer to ::ALL_UCAP_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_ucap
 * @see ::free_all_ucap_info
 */
int mbg_get_all_ucap_info( MBG_DEV_HANDLE dh, ALL_UCAP_INFO **p )
{
  int rc;
  ALL_UCAP_INFO *ucap_info = *p;
  UCAP_ENTRY *new_entry, *old_entry;

  // If this function is called for the first time,
  // allocate a new ::ALL_UCAP_INFO structure.
  if ( ucap_info == NULL )
  {
    ucap_info = ( ALL_UCAP_INFO* )calloc( 1, sizeof( *ucap_info ) );
    if ( ucap_info == NULL )
    {
      *p = NULL;
      return MBG_ERR_NO_MEM;
    }
    mbg_klist_init(&ucap_info->list);
  }

  do
  {
    new_entry = calloc_ucap_entry();
    if ( !new_entry )
    {
      rc = MBG_ERR_NO_MEM;
      goto err_out;
    }

    rc = mbg_get_gps_ucap( dh, &new_entry->ttm );
    if ( mbg_rc_is_error( rc ) )
      goto err_out;

    if ( ( (uint8_t)new_entry->ttm.tm.sec == (uint8_t) 0xFF ) )
    {
      free ( new_entry );
      new_entry = NULL;
      break;
    }

    if ( ucap_info->num_ucaps < MAX_UCAP_ENTRIES )
    {
      mbg_klist_append_item(&ucap_info->list, &new_entry->head);
      ++ucap_info->num_ucaps;
    }
    else
    {
      old_entry = mbg_klist_first_entry(&ucap_info->list, UCAP_ENTRY, head);
      mbg_klist_delete_item(&old_entry->head);
      free(old_entry);
      mbg_klist_append_item(&ucap_info->list, &new_entry->head);
    }

  } while (1);

  rc = MBG_SUCCESS;

  goto success_out;

err_out:
  free_all_ucap_info( ucap_info );
  ucap_info = NULL;
  if ( new_entry )
    free( new_entry );

success_out:
  *p = ucap_info;

  return rc;

}  // mbg_get_all_ucap_info



#if 0 && defined( DEBUG )  // TODO

/*HDR*/
void test_gpio( MBG_DEV_HANDLE dh, int verbose )
{
  int b;
  int rc;
  unsigned int i;
  MBG_GPIO_CFG_LIMITS gpio_cfg_limits = { 0 };
  ALL_GPIO_INFO_IDX all_gpio_info_idx = { { 0 } };
  ALL_GPIO_STATUS_IDX all_gpio_status_idx = { { 0 } };

  rc = mbg_chk_dev_has_gpio( dh );

  printf( "\nGPIO chk supp: %i\n", rc );

  rc = mbg_dev_has_gpio( dh, &b );

  if ( mbg_rc_is_error( rc ) || !b )
  {
    printf( "GPIO not supported, rc: %i, b: %i\n", rc, b );
    return;
  }

  printf( "GPIO supported, rc: %i, b: %i\n", rc, b );

  printf( "\nChecking GPIO:\n" );

  rc = mbg_get_gpio_cfg_limits( dh, &gpio_cfg_limits );

  if (  mbg_rc_is_error( rc )  )
  {
    printf( "Failed to read GPIO limits, rc: %i\n", rc );
    return;
  }

  printf( "Number of GPIO ports: %i\n", gpio_cfg_limits.num_io );

  rc = mbg_get_gps_all_gpio_info( dh, all_gpio_info_idx, &gpio_cfg_limits );

  if (  mbg_rc_is_error( rc ) )
    printf( "Failed to read all GPIO info, rc: %i\n", rc );

  if ( !( gpio_cfg_limits.flags & MBG_GPIO_CFG_LIMIT_FLAG_MASK_STATUS_SUPP ) )
    printf( "GPIO status not supported (flag not set).\n" );
  else
  {
    rc = mbg_get_gps_all_gpio_status( dh, all_gpio_status_idx, &gpio_cfg_limits );

    if ( mbg_rc_is_error( rc ) )
      printf( "Failed to read all GPIO status, rc: %i\n", rc );
  }


  for ( i = 0; i < gpio_cfg_limits.num_io; i++ )
  {
    MBG_GPIO_INFO *p_i = &all_gpio_info_idx[i].info;
    MBG_GPIO_STATUS *p_st = &all_gpio_status_idx[i].status;
    int gpio_type = p_i->settings.type;

    printf( "GPIO %i: type 0x%02X (%s)", i,
            gpio_type, _get_gpio_type_name( gpio_type ) );

    switch ( gpio_type )
    {
      case MBG_GPIO_TYPE_FREQ_IN:         // variable frequency input, freq == 0 if input not used
      {
        MBG_GPIO_FREQ_IN_SETTINGS *p = &p_i->settings.u.freq_in;
#if 0
  MBG_GPIO_FREQ freq;  ///< frequency in range ::MBG_GPIO_FREQ_IN_SUPP::freq_min..::MBG_GPIO_FREQ_IN_SUPP::freq_max, or 0 if input is not used
  uint32_t csc_limit;  ///< max. cycle slip [1/1000 cycle units], see ::MBG_GPIO_FREQ_IN_SUPP::csc_limit_max
  uint32_t shape;      ///< selected signal shape, see ::MBG_GPIO_SIGNAL_SHAPES
  uint32_t reserved;   ///< reserved, currently always 0
  uint32_t flags;      ///< reserved, currently always 0
#endif

      } break;

      case MBG_GPIO_TYPE_FREQ_OUT:        // variable frequency output
      {
        MBG_GPIO_FREQ_OUT_SETTINGS *p = &p_i->settings.u.freq_out;

        printf( " %lu.%lu Hz %s, phase %li.%03li deg",
                (ulong) p->freq.hz, (ulong) p->freq.frac,
                _get_gpio_signal_shape_name( p->shape ),
                (long) p->milli_phase / 1000,
                labs( (long) p->milli_phase % 1000 ) );

      } break;

      case MBG_GPIO_TYPE_FIXED_FREQ_OUT:  // fixed frequency output
      {
        MBG_GPIO_FIXED_FREQ_OUT_SETTINGS *p = &p_i->settings.u.ff_out;

#if 0
typedef struct
{
  uint32_t freq_idx;  ///< fixed frequency index, see ::MBG_GPIO_FIXED_FREQS
  uint32_t shape;     ///< selected signal shape, see ::MBG_GPIO_SIGNAL_SHAPES
  uint32_t reserved;  ///< reserved, currently always 0
  uint32_t flags;     ///< reserved, currently always 0

} MBG_GPIO_FIXED_FREQ_OUT_SETTINGS;
#endif

        printf( " %s %s", _get_gpio_fixed_freq_str( p->freq_idx ),
                _get_gpio_signal_shape_name( p->shape ) );

      } break;

      case MBG_GPIO_TYPE_BITS_IN:         // framed data stream input
        break;

      case MBG_GPIO_TYPE_BITS_OUT:        // framed data stream output
        break;
    }

    printf( ", status 0x%02X (%s)",
            p_st->port_state, _get_gpio_port_state_name( p_st->port_state ) );

    printf( "\n" );
  }

#if 0
  rc = mbg_set_gps_gpio_settings_idx( dh, MBG_GPIO_SETTINGS_IDX *p )
#endif

}  // test_gpio



/*HDR*/
void test_xmr( MBG_DEV_HANDLE dh, int verbose )
{
  static const char *multi_ref_names[N_MULTI_REF] = DEFAULT_MULTI_REF_NAMES;

  XMULTI_REF_INSTANCES xmr_instances = { 0 };
  ALL_XMULTI_REF_INFO_IDX all_xmulti_ref_info_idx = { { 0 } };
  ALL_XMULTI_REF_STATUS_IDX all_xmulti_ref_status_idx = { { 0 } };
  XMR_HOLDOVER_STATUS xmr_holdover_status;

  int b;
  int rc;
  int i;

  rc = mbg_chk_dev_has_xmr( dh );

  printf( "\nXMR chk supp: %i\n", rc );

  rc = mbg_dev_has_xmr( dh, &b );

  if ( rc != MBG_SUCCESS || !b )
  {
    printf( "XMR not supported, rc: %i, b: %i\n", rc, b );
    return;
  }

  printf( "XMR supported, rc: %i, b: %i\n", rc, b );

  printf( "\nChecking XMR:\n" );

  rc = mbg_get_xmr_instances( dh, &xmr_instances );

  if ( rc != MBG_SUCCESS )
  {
    printf( "*** Failed to read XMR instances, rc: %i\n", rc );
    return;
  }

  printf( "Slot %u, supp. XMR instances/priority levels: %u, flags: 0x%08lX\n",
          xmr_instances.slot_id, xmr_instances.n_xmr_settings,
          (ulong) xmr_instances.flags );

  if ( verbose )
  {
//    XMRIF_MSK_MRF_NONE_SUPP        = ( 1UL << XMRIF_BIT_MRF_NONE_SUPP ),        ///< see ::XMRIF_BIT_MRF_NONE_SUPP
//##++++++++++++++++++++    XMRIF_MSK_HOLDOVER_STATUS_SUPP = ( 1UL << XMRIF_BIT_HOLDOVER_STATUS_SUPP )  ///< see ::XMRIF_BIT_HOLDOVER_STATUS_SUPP
  }


  for ( i = 0; i < MAX_N_MULTI_REF_TYPES; i++ )
  {
    int n_inst = xmr_instances.n_inst[i];

    if ( i < N_MULTI_REF )  // a known signal type
    {
      if ( verbose )
      {
        if ( n_inst )
          printf( "%u %s input%s supported\n", n_inst,
                  multi_ref_names[i], ( n_inst == 1 ) ? "" : "s" );
        else
          if ( verbose > 1 )
            printf( "No %s input supported\n", multi_ref_names[i] );
      }

      continue;
    }

    // If execution gets here, the signal type is unknown.
    // Print a warning if the device supports instances of this signal type.
    if ( n_inst )
      printf( "*** Warning: %u instances of unknown signal type idx %u supported!\n",
              n_inst, i );
  }

  rc = mbg_get_gps_all_xmr_info( dh, all_xmulti_ref_info_idx, &xmr_instances );

  if ( rc != MBG_SUCCESS )
    printf( "*** Failed to read all XMR info, rc: %i\n", rc );

  rc = mbg_get_gps_all_xmr_status( dh, all_xmulti_ref_status_idx, &xmr_instances );

  if ( rc != MBG_SUCCESS )
    printf( "*** Failed to read all XMR status, rc: %i\n", rc );

  for ( i = 0; i < xmr_instances.n_xmr_settings; i++ )
  {
    XMULTI_REF_INFO *p = &all_xmulti_ref_info_idx[i].info;

    printf( "XMR %i: %li\n", i,
            (long) p->settings.bias.secs );

#if 0
    XMULTI_REF_SETTINGS_IDX xmrsi;
    xmrsi.settings = p->settings;
    xmrsi.settings.precision.nano_secs = i + 40;
    xmrsi.settings.bias.secs = 0xA55ACDEF;
    xmrsi.settings.bias.nano_secs = 0x12345678;
    xmrsi.idx = i;

    rc = mbg_set_gps_xmr_settings_idx( dh, &xmrsi );

    if ( rc != MBG_SUCCESS )
      printf( "Failed to write XMR settings %i, rc: %i\n", i, rc );
#endif
  }

  if ( !( xmr_instances.flags & XMRIF_MSK_HOLDOVER_STATUS_SUPP ) )
    printf( "*** Warning: XMR holdover status not supported!\n" );
  else
  {
    rc = mbg_get_xmr_holdover_status( dh, &xmr_holdover_status, &xmr_instances );

    if ( rc != MBG_SUCCESS )
      printf( "*** Failed to read XMR holdover status, rc: %i\n", rc );
    else
    {
      printf( "XMR mode: %s, %s%s, %s%s, %s%s\n",
              _get_xmr_holdover_status_mode_name( xmr_holdover_status.mode ),
              ( xmr_holdover_status.flags & XMR_HLDOVR_MSK_IN_HOLDOVER ) ? "" : str_not_spc, "in holdover",
              ( xmr_holdover_status.flags & XMR_HLDOVR_MSK_TRANSITION_ENBD ) ? "" : str_not_spc, "transition enabled",
              ( xmr_holdover_status.flags & XMR_HLDOVR_MSK_IN_TRANSITION ) ? "" : str_not_spc, "in transition"
            );

//##+++++      print_xmr_prio( xmr_holdover_status.curr_prio, "Current",
#if 0
typedef struct
{
  uint8_t mode;                ///< XMR/holdover mode, see ::XMR_HOLDOVER_STATUS_MODES
  int8_t curr_prio;            ///< current priority level, 0..::XMULTI_REF_INSTANCES::n_xmr_settings, or ::XMR_PRIO_LVL_UNSPEC
  int8_t nxt_prio;             ///< next priority level after holdover, 0..::XMULTI_REF_INSTANCES::n_xmr_settings, or ::XMR_PRIO_LVL_UNSPEC
  uint8_t remote_watchdog;     ///< counts down in ::XMR_HLDOVR_PRE_AUTONOMOUS mode
  uint32_t reserved;           ///< reserved, don't use, currently 0
  XMR_HOLDOVER_INTV elapsed;   ///< elapsed time in holdover mode, only valid if ::XMR_HLDOVR_MSK_IN_HOLDOVER is set
  XMR_HOLDOVER_INTV interval;  ///< current holdover interval, only valid if ::XMR_HLDOVR_MSK_IN_HOLDOVER is set
  uint32_t flags;              ///< holdover status flags, see ::XMR_HOLDOVER_STATUS_FLAG_MASKS

} XMR_HOLDOVER_STATUS;
#endif

    }
  }

}  // test_xmr

#endif  // defined DEBUG



static const int pcps_to_mbg_framing_tbl[N_PCPS_FR_DCF] =
{
  MBG_FRAMING_8N1,
  MBG_FRAMING_7E2,
  MBG_FRAMING_8N2,
  MBG_FRAMING_8E1
};



/*HDR*/
void port_info_from_pcps_serial(
  PORT_INFO_IDX *p_pii,
  PCPS_SERIAL pcps_serial,
  uint32_t supp_baud_rates
)
{
  PCPS_SER_PACK ser_pack;
  PORT_INFO *p_pi;
  PORT_SETTINGS *p_ps;

  ser_pack.pack = pcps_serial;
  pcps_unpack_serial( &ser_pack );

  p_pi = &p_pii[0].port_info;
  p_ps = &p_pi->port_settings;

  p_ps->parm.baud_rate = mbg_baud_rates[ser_pack.baud];

  strncpy_safe( p_ps->parm.framing,  //### TODO
                mbg_framing_strs[pcps_to_mbg_framing_tbl[ser_pack.frame]],
                sizeof( p_ps->parm.framing ) );

  p_ps->parm.handshake = HS_NONE;

  p_ps->str_type = 0;
  p_ps->mode = ser_pack.mode;

  p_pi->supp_baud_rates = supp_baud_rates;
  p_pi->supp_framings = DEFAULT_FRAMINGS_DCF;
  p_pi->supp_str_types = DEFAULT_SUPP_STR_TYPES_DCF;

}  // port_info_from_pcps_serial



/*HDR*/
void pcps_serial_from_port_info(
  PCPS_SERIAL *p,
  const PORT_INFO_IDX *p_pii
)
{
  PCPS_SER_PACK ser_pack;
  const PORT_INFO *p_pi = &p_pii[0].port_info;
  const PORT_SETTINGS *p_ps = &p_pi->port_settings;
  int framing_idx = get_framing_idx( p_ps->parm.framing );
  int i;


  ser_pack.baud = get_baud_rate_idx( p_ps->parm.baud_rate );

  // Translate the common framing index to the corresponding
  // number used with the old PCPS_SERIAL parameter.
  // This should always return a valid result since the
  // framing index is expected to be selected from
  // supported framings.
  for ( i = 0; i < N_PCPS_FR_DCF; i++ )
    if ( pcps_to_mbg_framing_tbl[i] == framing_idx )
      break;

  ser_pack.frame = i;

  ser_pack.mode = p_ps->mode;

  pcps_pack_serial( &ser_pack );

  *p = ser_pack.pack;

}  // pcps_serial_from_port_info



/*HDR*/
/**
 * @brief Unpack a structure with serial port parameters.
 *
 * @param[in,out]  p  Address of a structure holding both the
 *                    packed and unpacked information.
 */
void pcps_unpack_serial( PCPS_SER_PACK *p )
{
  uint8_t pack = p->pack;

  p->baud = (uint8_t) ( pack & BITMASK( PCPS_BD_BITS ) );
  p->frame = (uint8_t) ( ( pack >> PCPS_FR_SHIFT ) & BITMASK( PCPS_FR_BITS ) );
  p->mode = (uint8_t) ( ( pack >> PCPS_MOD_SHIFT ) & BITMASK( PCPS_MOD_BITS ) );

}  // pcps_unpack_serial



/*HDR*/
/**
 * @brief Pack a structure with serial port parameters.
 *
 * @param[in,out]  p  Address of a structure holding both the
 *                    packed and unpacked information.
 */
void pcps_pack_serial( PCPS_SER_PACK *p )
{
  p->pack = (uint8_t) ( ( p->baud & BITMASK( PCPS_BD_BITS ) )
        | ( ( p->frame & BITMASK( PCPS_FR_BITS ) ) << PCPS_FR_SHIFT )
        | ( ( p->mode & BITMASK( PCPS_MOD_BITS ) ) << PCPS_MOD_SHIFT ) );

}  /* pcps_pack_serial */



/*HDR*/
/**
 * @brief Setup a list of ISA port addresses from a string.
 *
 * @param[in]   s          A string with port addresses, separated by comma.
 * @param[out]  port_vals  An array of port addresses to be filled.
 * @param[in]   n_vals     The max number of possible entries in @p port_vals.
 */
void pcps_setup_isa_ports( char *s,
                           int *port_vals,
                           int n_vals )
{
  ushort i;


  for ( i = 0; i < n_vals; i++ )
  {
    if ( *s == 0 )
      break;

    *port_vals++ = (uint16_t) strtoul( s, &s, 16 );

    if ( *s == ',' )
      s++;
  }

}  // pcps_setup_isa_ports



/*HDR*/
const char *setup_device_type_name( char *s, size_t max_len, MBG_DEV_HANDLE dh,
                                    const RECEIVER_INFO *p_ri )
{
  size_t n = sn_cpy_str_safe( s, max_len, p_ri->model_name );

  if ( mbg_rc_is_success( mbg_chk_dev_has_asic_version( dh ) ) )
  {
    PCI_ASIC_VERSION asic_version;

    if ( mbg_rc_is_success( mbg_get_asic_version( dh, &asic_version ) ) )
    {
      asic_version = _convert_asic_version_number( asic_version );  // TODO Do we need this?

      n += snprintf_safe( &s[n], max_len - n, " (PCI ASIC v" PCPS_ASIC_STR_FMT ")",
                          _pcps_asic_version_major( asic_version ),
                          _pcps_asic_version_minor( asic_version ) );
    }
  }

  return s;

}  // setup_device_type_name



/*HDR*/
const char *setup_asic_features( char *s, size_t max_len, MBG_DEV_HANDLE dh )
{
  size_t n = 0;

  if ( mbg_rc_is_success( mbg_chk_dev_has_asic_features( dh ) ) )
  {
    PCI_ASIC_FEATURES asic_features;

    if ( mbg_rc_is_success( mbg_get_asic_features( dh, &asic_features ) ) )
    {
      if ( asic_features & PCI_ASIC_HAS_MM_IO )
        n += sn_cpy_str_safe( &s[n], max_len - n, "Memory Mapped I/O" );

      //### if ( asic_features & PCI_ASIC_HAS_PGMB_IRQ )
      // (implement this as loop)
    }
  }

  if ( n == 0 )  // nothing else printed
    sn_cpy_str_safe( s, max_len, str_not_avail );

  return s;

}  // setup_asic_features


