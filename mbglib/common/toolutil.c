
/**************************************************************************
 *
 *  $Id: toolutil.c 1.26 2022/12/21 15:34:41 martin.burnicki REL_M $
 *
 *  Common functions that can be used with Meinberg command line
 *  utility programs.
 *
 * -----------------------------------------------------------------------
 *  $Log: toolutil.c $
 *  Revision 1.26  2022/12/21 15:34:41  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameter from some functions,
 *  and use feature check functions instead of macros.
 *  Removed some obsolete _int_from_size_t() stuff.
 *  Revision 1.25  2022/07/25 16:41:55  martin.burnicki
 *  Support getting full version string from git.
 *  Revision 1.24  2022/04/01 15:00:12  martin.burnicki
 *  Print the RECEIVER_INFO's model_name as device name
 *  in mbg_get_show_dev_info().
 *  Revision 1.23  2021/11/15 17:08:30  martin.burnicki
 *  Improved printing of usage information.
 *  Revision 1.22  2021/11/08 21:37:29  martin.burnicki
 *  SYN1588 devices are now enumerated after native Meinberg devices.
 *  Print build number of SYN1588 device instead of firmware version.
 *  Use new functions from mbgdevio to reduce dependies on conditional
 *  preprocessor symbols.
 *  Revision 1.21  2021/11/08 18:02:46  martin.burnicki
 *  Account for modified MBG_DEV_FN type.
 *  Revision 1.20  2021/05/04 20:45:53  martin
 *  Cleaned up conditional support for SYN1588.
 *  Revision 1.19  2021/04/29 13:09:18  martin
 *  Account for definitions that were moved to mbgutil.h.
 *  Revision 1.18  2021/04/12 22:04:08  martin
 *  Basic support for serial devices.
 *  Updated printing of usage information.
 *  Revision 1.17  2021/03/22 11:25:09  martin
 *  Updated a bunch of comments.
 *  Revision 1.16  2021/03/12 10:51:30  martin
 *  Updated some doxygen comments.
 *  Revision 1.15  2020/11/04 16:59:12  martin
 *  Moved inline function delta_timestamps() to the header file
 *  and renamed it to delta_timestamp_us() because the returned
 *  value is in microseconds.
 *  Revision 1.14  2020/10/20 10:45:17  martin
 *  New function mbg_has_admin_rights() that can be used to check
 *  if a program is running with admin rights, or not.
 *  New function mbg_chk_print_no_admin_rights() which conditionally
 *  prints a warning that SYN1588 devices may not be detected if running
 *  without admin rights.
 *  New function mbg_print_no_device_found() that can be called
 *  to print information that no device has been found. Also
 *  calls mbg_chk_print_no_admin_rights() to give a hint for
 *  a possible reason.
 *  Revision 1.13  2020/02/27 13:34:52  martin
 *  mbg_print_opt_info() now accepts printf-like format string as
 *  2nd argument, plus associated optional parameters.
 *  mbg_show_pzf_corr_info() now just prints the
 *  correlation info, but doesn't read it from the device.
 *  Removed inclusion of obsolete header pcpsutil.h.
 *  Account for a renamed function.
 *  Revision 1.12  2019/06/25 15:22:34  martin
 *  Print the latency value of a HR time at the end of a line for clearer output.
 *  Revision 1.11  2019/05/08 11:27:35  martin
 *  Use new symbol PCPS_IRQ_NUM_UNDEFINED.
 *  Revision 1.10  2019/02/08 10:34:47  martin
 *  Fix for the mingw build environment.
 *  Revision 1.9  2018/12/14 13:13:46  martin
 *  Cleaned up mbg_open_device_by_param(), removed obsolete
 *  parameters, and removed all printing by this function.
 *  New function mbg_open_device_by_param_chk() which actually
 *  calls mbg_open_device_by_param() and prints a message to stderr
 *  if the call failed.
 *  Updated some comments.
 *  Revision 1.8  2018/11/15 12:20:44  martin
 *  Individual MBG_MICRO_VERSION codes are now obsolete.
 *  Revision 1.7  2018/07/03 14:47:31  martin
 *  Print a message if an old card with 20 ms timing bug is detected.
 *  Revision 1.6  2018/06/25 13:45:26  martin
 *  Distinguish between MBG_TGT_HAS_DEV_FN and
 *  MBG_TGT_HAS_DEV_FN_BASE.
 *  Use some predefined string formats.
 *  Check parameters in mbg_open_device_by_param().
 *  Some string print functions now return int rather than
 *  size_t, like standard library functions.
 *  Use snprint_utc_offs() from timeutil.c.
 *  Revision 1.5  2017/07/05 07:32:38  martin
 *  Renamed mbg_check_devices() to mbg_handle_devices() and added
 *  a new parameter which controls if only one or all devices
 *  should be handled if no device has been specified.
 *  New function mbg_open_device_by_param() which can be used e.g. to
 *  check if a parameter string is an device file name (MBG_DEV_FN),
 *  or a device index number, or a device name in MBG_DEV_NAME format,
 *  and calls the appropriate open function to open the device.
 *  Made mbg_snprint_hr_tstamp() more versatile by passing a new
 *  optional UTC offset parameter.
 *  Account for PCPS_HRT_BIN_FRAC_SCALE renamed to MBG_FRAC32_UNITS_PER_SEC.
 *  Account for frac_sec_from_bin() obsoleted by bin_frac_32_to_dec_frac().
 *  Print device options with OS-specific device names.
 *  Print special firmware version info, if appropriate.
 *  Reworked error checking after opening device.
 *  mbg_print_program_info() which is usually called first after program
 *  start now makes stdout unbuffered if output is redirected.
 *  Use safe string functions from str_util.c.
 *  Support Windows and QNX Neutrino targets.
 *  Added doxygen comments.
 *  Revision 1.4  2012/10/15 09:33:48  martin
 *  Use common way to handle version information.
 *  Open device with O_RDWR flag.
 *  Fixed printing delta timestamps.
 *  Added function to print PCPS_TIME.
 *  Added function to show PZF correlation.
 *  Added function mbg_print_hr_time() which optionally prints hex status.
 *  Let the display functions for HR timestamps optionally show
 *  the raw (hex) timestamps.
 *  Revision 1.3  2009/06/19 12:12:14  martin
 *  Added function mbg_print_hr_timestamp().
 *  Revision 1.2  2009/02/18 09:15:55  martin
 *  Support TAI and GPS time scales in mbg_snprint_hr_time().
 *  Revision 1.1  2008/12/17 10:45:13  martin
 *  Initial revision.
 *  Revision 1.1  2008/12/15 08:35:07  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _TOOLUTIL
  #include <toolutil.h>
#undef _TOOLUTIL

// Include Meinberg headers.
#include <mbgutil.h>
#include <cfg_hlp.h>
#include <timeutil.h>
#include <str_util.h>

#if defined( MBG_TGT_WIN32 )
  #include <mbgsvctl.h>
#endif

#if SUPP_SERIAL
  #include <mbgserio.h>
#endif

// Include system headers.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>



#if defined( MBG_TGT_WIN32 ) && defined( _MSC_VER )
  // Required for MSC, but not for e.g. MinGW.
  // Must be defined *after* stdio has been included.
  #define isatty   _isatty
  #define fileno   _fileno
#endif



/*HDR*/
/**
 * @brief Print the program version to a string buffer.
 *
 * @param[out]  s        Address of a string buffer to be filled.
 * @param[in]   max_len  Size of the string buffer.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
int mbg_program_version_str( char *s, size_t max_len )
{
  int n = snprintf_safe( s, max_len, "%s", MBG_FULL_VERSION_STR );

  return n;

}  // mbg_program_version_str



/*HDR*/
/**
 * @brief Print some program info to a string buffer.
 *
 * @param[out]  s           Address of a string buffer to be filled.
 * @param[in]   max_len     Size of the string buffer.
 * @param[in]   pname       The program name.
 * @param[in]   first_year  First copyright year.
 * @param[in]   last_year   Last copyright year.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
int mbg_program_info_str( char *s, size_t max_len, const char *pname,
                          int first_year, int last_year )
{
  int n = 0;

  if ( last_year == 0 )
    last_year = MBG_CURRENT_COPYRIGHT_YEAR;

  n += snprintf_safe( &s[n], max_len - n, "%s v", pname );

  n += mbg_program_version_str( &s[n], max_len - n );

  n += snprintf_safe( &s[n], max_len - n, " copyright Meinberg " );

  if ( first_year != last_year )
    n += snprintf_safe( &s[n], max_len - n, "%04i-", first_year );

  n += snprintf_safe( &s[n], max_len - n, "%04i", last_year );

  return n;

}  // mbg_program_info_str



/*HDR*/
/**
 * @brief Print program info to console.
 *
 * @param[in]  pname       The program name.
 * @param[in]  first_year  First copyright year.
 * @param[in]  last_year   Last copyright year.
 */
void mbg_print_program_info( const char *pname, int first_year, int last_year )
{
  char ws[256];

  // If the output has been redirected, make stdout unbuffered, e.g.
  // to see the output immediately even though piped through 'tee'.
  if ( !isatty( fileno( stdout ) ) )
    setvbuf( stdout, NULL, _IONBF, 0 );

  mbg_program_info_str( ws, sizeof( ws ), pname, first_year, last_year );

  printf( "\n%s\n\n", ws );

}  // mbg_print_program_info



/*HDR*/
/**
 * @brief Check if the program runs with admin rights.
 *
 * @return  @a true if runs with admin rights, else @a false.
 */
bool mbg_has_admin_rights( void )
{
  #if defined( MBG_TGT_WIN32 )
    return mbg_svc_has_admin_rights();
  #elif defined( MBG_TGT_POSIX )
    return geteuid() == 0;
  #else
    #error mbg_has_admin_rights() needs to be implemented.
  #endif

}  // mbg_has_admin_rights



/*HDR*/
/**
 * @brief Print a warning if program runs without admin rights.
 *
 * Yet, this is only relevant for the detection of SYN1588 devices.
 */
void mbg_chk_print_no_admin_rights( void )
{
  #if defined( MBG_TGT_WIN32 )
    if ( mbg_rc_is_success( mbg_chk_if_syn1588_supported() ) )
      if ( !mbg_has_admin_rights() )
        printf( "\n"
                "Running without admin rights, so %s devices\n"
                "may not be visible, even though supported.\n",
                mbg_get_syn1588_type_name() );
  #endif

}  // mbg_chk_print_no_admin_rights



/*HDR*/
/**
 * @brief Print a warning if no device has been found.
 *
 * If SYN1588 support is enabled, also possibly print
 * a warning if running without admin rights, because
 * on some systems, SYN1588 devices may not be detected
 * without admin rights.
 */
void mbg_print_no_device_found( void )
{
  printf( "No device found.\n" );
  mbg_chk_print_no_admin_rights();
  printf( "\n" );

}  // mbg_print_no_device_found



/*HDR*/
/**
 * @brief Print usage intro to console.
 *
 * @param[in]  pname      The program name.
 * @param[in]  print_dev  Print device parameters, or not.
 * @param[in]  info       An optional additional info string, may be @a NULL.
 */
void mbg_print_usage_intro( const char *pname, bool print_dev, const char *info )
{
  printf( "Usage:  %s [[opt] [opt] ...]", pname );

  if ( print_dev )
    printf( " [[dev] [dev] ...]" );

  printf( "\n\n" );

  if ( info )
    printf( "%s\n\n", info );


}  // mbg_print_usage_intro



/*HDR*/
/**
 * @brief Print info on a single program option / argument.
 *
 * @param[in]  opt_name      The option name, optional, may be @a NULL.
 * @param[in]  opt_info_fmt  The option info format string, optional, may be @a NULL.
 * @param[in]  ...           Variable argument list according to the @p fmt format string.
 */
__attribute__( ( format( printf, 2, 3 ) ) )
void mbg_print_opt_info( const char *opt_name, const char *opt_info_fmt, ... )
{
  if ( opt_name == NULL )
    opt_name = str_empty;

  printf( "  %8s", opt_name );

  if ( opt_info_fmt )
  {
    va_list args;

    printf( "   " );

    va_start( args, opt_info_fmt );
    vprintf( opt_info_fmt, args );
    va_end( args );
  }

  printf( "\n" );

}  // mbg_print_opt_info



/*HDR*/
/**
 * @brief Print info on common program help arguments.
 *
 * Lists program parameters causing printing
 * of help / usage information.
 */
void mbg_print_help_options( void )
{
  puts( "where \"opt\" is one of the options:" );
  mbg_print_opt_info( "-? or -h", "Print this usage information" );

}  // mbg_print_help_options



/*HDR*/
/**
 * @brief Print common info on how to specify devices on the command line.
 *
 * @param[in]  mask  Bit mask to control for which kind of devices
 *                   information shall be displayed.
 *                   See ::DEV_OPT_PRINT_MASKS.
 *
 * @see ::DEV_OPT_PRINT_MASKS
 */
void mbg_print_device_options( int mask )
{
  #if SUPP_SERIAL
    int cmp_mask = DEV_OPT_PRINT_BUS_LEVEL | DEV_OPT_PRINT_SERIAL;
  #else
    int cmp_mask = DEV_OPT_PRINT_BUS_LEVEL;
  #endif

  if ( mask & cmp_mask )
  {
    printf( "\n" );

    puts( "where \"dev\" can be:" );
  }

  if ( mask & DEV_OPT_PRINT_BUS_LEVEL )
  {
    #if MBG_TGT_HAS_DEV_FN_BASE
      printf( "    A device file name, e.g. \"%s\" or \"%s\"\n",
              EXAMPLE_DEV_FN_1, EXAMPLE_DEV_FN_2 );
    #endif
      printf( "    A device type name, with or with S/N appended, e.g. \"%s\" or \"%s\"\n",
              EXAMPLE_DEV_NAME_1, EXAMPLE_DEV_NAME_2 );

      printf( "    A device index, e.g. \"0\", \"1\",\"2\", ...\n" );
  }

  #if SUPP_SERIAL
    if ( mask & DEV_OPT_PRINT_SERIAL )
    {
      printf( "    The name of a serial port, to which an external device\n");
      printf( "    is connected, e.g. %s\n", DEFAULT_SERIAL_DEVICE_NAME );
    }
  #endif

}  // mbg_print_device_options



/*HDR*/
/**
 * @brief Print usage info whether SYN1588 support has been compiled in.
 */
void mbg_print_usage_syn1588_support( void )
{
  bool syn1588_supported = mbg_rc_is_success( mbg_chk_if_syn1588_supported() );

  printf( "This program has been built %s support for SYN1588 cards.\n",
          syn1588_supported ? "with" : "WITHOUT" );
  puts( "" );

}  // mbg_print_usage_syn1588_support



/*HDR*/
/**
 * @brief Print usage outro, including device options and info on SYN1588 support .
 *
 * @param[in]  mask  Bit mask to control for which kind of devices
 *                   information shall be displayed.
 *                   See ::DEV_OPT_PRINT_MASKS.
 *
 * @param[in]  print_syn1588_info  Controls whether info on SYN1588 support
 *                                 is to be printed, or not.
 */
void mbg_print_usage_outro( int mask, bool print_syn1588_info )
{
  mbg_print_device_options( mask );
  puts( "" );

  if ( print_syn1588_info )
    mbg_print_usage_syn1588_support();

}  // mbg_print_usage_outro



/*HDR*/
/**
 * @brief Print program info and default usage information.
 *
 * @param[in]  pname      The program name.
 * @param[in]  prog_info  An optional additional info string, may be @a NULL.
 */
void mbg_print_default_usage( const char *pname, const char *prog_info )
{
  mbg_print_usage_intro( pname, true, prog_info );
  mbg_print_help_options();
  mbg_print_usage_outro( DEV_OPT_PRINT_BUS_LEVEL, true );

}  // mbg_print_default_usage



/*HDR*/
/**
 * @brief Retrieve and print some common device info.
 *
 * @param[in]   dh        Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   dev_name  A device name string to be printed.
 * @param[out]  p_dev     Pointer to a device info structure to be read from the device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_get_show_dev_info( MBG_DEV_HANDLE dh, const char *dev_name, PCPS_DEV *p_dev )
{
  RECEIVER_INFO ri;
  unsigned long port;
  int irq_num;
  int rc;

  if ( dev_name )
    printf( "%s:\n", dev_name );

  // Get information about the device.
  rc = mbg_get_device_info( dh, p_dev );

  if ( mbg_cond_err_msg( rc, "mbg_get_device_info" ) )
    goto out;

  rc = mbg_setup_receiver_info( dh, p_dev, &ri );

  if ( mbg_cond_err_msg( rc, "mbg_setup_receiver_info" ) )
    goto out;


  printf( "%s", ri.model_name );

  if ( strlen( _pcps_sernum( p_dev ) ) &&
       strcmp( _pcps_sernum( p_dev ), "N/A" ) )
    printf( " %s", _pcps_sernum( p_dev ) );

  if ( mbg_rc_is_success( mbg_chk_dev_is_syn1588_type( dh ) ) )
    printf( " (Build %u", _pcps_fw_rev_num( p_dev ) );
  else
  {
    printf( " (FW " PCPS_FW_STR_FMT,
            _pcps_fw_rev_num_major( _pcps_fw_rev_num( p_dev ) ),
            _pcps_fw_rev_num_minor( _pcps_fw_rev_num( p_dev ) ) );

    if ( chk_sw_rev_name( &ri.sw_rev, 0 ) )
      printf( " \"%s\"", ri.sw_rev.name );

    rc = mbg_chk_dev_has_asic_version( dh );

    if ( mbg_rc_is_success( rc ) )
    {
      PCI_ASIC_VERSION av;
      int rc = mbg_get_asic_version( dh, &av );

      if ( mbg_rc_is_success( rc ) )
      {
        av = _convert_asic_version_number( av );  // TODO Do we need this?

        printf( ", ASIC " PCPS_ASIC_STR_FMT,
                _pcps_asic_version_major( av ),
                _pcps_asic_version_minor( av )
              );
      }
    }
  }

  printf( ")" );

  port = _pcps_short_port_base( p_dev, 0 );

  if ( port )
    printf( " at port 0x%03lX", port );

  port = _pcps_short_port_base( p_dev, 1 );

  if ( port )
    printf( "/0x%03lX", port );

  irq_num = _pcps_irq_num( p_dev );

  if ( irq_num != PCPS_IRQ_NUM_UNDEFINED )
    printf( ", irq %i", irq_num );

  if ( _pcps_fw_has_20ms_bug( p_dev ) )
    printf( "\n** Warning: %s v" PCPS_FW_STR_FMT " has a 20 ms timing bug. Please upgrade the firmware!",
            _pcps_type_name( p_dev ),
            _pcps_fw_rev_num_major( _pcps_fw_rev_num( p_dev ) ),
            _pcps_fw_rev_num_minor( _pcps_fw_rev_num( p_dev ) ) );

  rc = MBG_SUCCESS;

out:
  puts( str_empty );
  return rc;

}  // mbg_get_show_dev_info



/*HDR*/
/**
 * @brief Print device info and take some action on a specific device.
 *
 * @param[in]   dh        Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   dev_name  A device name string to be printed.
 * @param[in]   fnc       Pointer to a callback function that actually takes the action.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_handle_device( MBG_DEV_HANDLE dh, const char *dev_name,
                       MBG_DEV_HANDLER_FNC *fnc )
{
  PCPS_DEV dev;
  int rc;

  rc = mbg_get_show_dev_info( dh, dev_name, &dev );

  if ( mbg_rc_is_success( rc ) )
    if ( fnc )
      rc = fnc( dh );

  mbg_close_device( &dh );

  puts( "" );

  return rc;

}  // mbg_handle_device



/*HDR*/
/**
 * @brief Get the number of devices actually present.
 *
 * Do a real search only when called for the first time.
 *
 * @return The number of devices currently present.
 */
int chk_get_num_devices( void )
{
  static int num_devices;
  static bool has_searched_devices;

  if ( !has_searched_devices )
  {
    num_devices = mbg_find_devices();
    has_searched_devices = true;
  }

  return num_devices;

}  // chk_get_num_devices



/*HDR*/
/**
 * @brief Try to open a device using a parameter string or device index.
 *
 * @param[out]  p_dh           Address of a device handle variable to be set on success.
 * @param[in]   dev_param_str  An optional device specification string. This can be:
 *                             - A device file name, if the operating system supports this. See ::MBG_DEV_FN.
 *                             - A device model name like "GPS180PEX", optionally with the serial number
 *                               appended after an underscore, as in "GPS180PEX_029511026220".
 *                               See ::MBG_DEV_NAME.
 *                             - A device index number as string, e.g. "0" or "3".
 *                             - @a NULL, in which case @p dev_idx has to be a valid device index number.
 * @param[in]   dev_idx               A device index number which is used if @p dev_param_str is @a NULL.
 * @param[out]  dev_name_buffer       A string buffer to which a device name can be written, see ::MBG_DEV_FN.
 * @param[in]   dev_name_buffer_size  The size of the @p dev_name_buffer buffer.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_open_device_by_param
 */
int mbg_open_device_by_param( MBG_DEV_HANDLE *p_dh, const char *dev_param_str, int dev_idx,
                              char *dev_name_buffer, size_t dev_name_buffer_size )
{
  MBG_DEV_HANDLE dh = MBG_INVALID_DEV_HANDLE;
  int devices_found = chk_get_num_devices();
  int rc = MBG_ERR_GENERIC;

  if ( dev_name_buffer == NULL || dev_name_buffer_size == 0 )
    return MBG_ERR_INV_PARM;

  dev_name_buffer[0] = 0;    // Make string empty.

  if ( dev_param_str )
  {
    // A device parameter string has been specified.
    char *endptr;
    long l;

    #if MBG_TGT_HAS_DEV_FN_BASE
      // Check if the device parameter represents a Meinberg device file name.
      if ( mbg_rc_is_success( mbg_chk_dev_fn_is_valid( dev_param_str ) ) )
      {
        sn_cpy_str_safe( dev_name_buffer, dev_name_buffer_size, dev_param_str );
        dh = mbg_open_device_by_dev_fn( dev_param_str );
        goto has_tried_to_open;
      }
    #endif  // MBG_TGT_HAS_DEV_FN_BASE


    // Check if the device parameter string is an index number.
    endptr = NULL;
    l = strtol( dev_param_str, &endptr, 0 );

    // If endptr now points past the string, the
    // whole string has been interpreted as number.
    if ( endptr == &dev_param_str[strlen( dev_param_str )] )
    {
      long l_max = (long) ( ( (unsigned int) -1 ) >> 1 );

      if ( l > l_max )
      {
        // The decoded number exceeds the maximum number
        // that can be passed to ::mbg_open_device.
        rc = MBG_ERR_RANGE;
        goto out;
      }

      // We use the decoded number as device index.
      dev_idx = (int) l;
      goto open_dev_with_idx;
    }


    // Finally assume the device name is a model name, optionally
    // with serial number appended.
    snprintf_safe( dev_name_buffer, dev_name_buffer_size, "%s", dev_param_str );
    dh = mbg_open_device_by_name( dev_param_str, MBG_MATCH_MODEL );
    goto has_tried_to_open;
  }


  // We get here directly if dev_param_str is NULL.

open_dev_with_idx:
  // Try to open the device with the index number which has been derived
  // from dev_param_str, or has been passed directly by the caller.
  // First check if the device index is in a valid range.

  if ( devices_found == 0 )        // No device found.
    return MBG_ERR_NO_DEV;

  if ( dev_idx >= devices_found )  // Device index exceeds max.
    return MBG_ERR_NO_DEV;

  mbg_dev_info_from_dev_idx( dev_name_buffer, (int) dev_name_buffer_size, dev_idx );
  dh = mbg_open_device( dev_idx );


has_tried_to_open:
  // We get here immediately after one of the "open" functions has been called,
  // so retrieve an error code to be returned if that function failed.
  rc = ( dh == MBG_INVALID_DEV_HANDLE ) ? mbg_get_last_error( NULL ) : MBG_SUCCESS;

out:
  // Save the returned handle, which can be MBG_INVALID_DEV_HANDLE.
  *p_dh = dh;

  return rc;

}  // mbg_open_device_by_param



/*HDR*/
/**
 * @brief Try to open a device and print a message in case of error.
 *
 * Call ::mbg_open_device_by_param to try to open a device,
 * and print an error message to stderr if the call fails.
 *
 * @param[out]  p_dh           Address of a device handle variable to be set on success.
 * @param[in]   dev_param_str  An optional device specification string. This can be:
 *                             - A device file name, if the operating system supports this. See ::MBG_DEV_FN.
 *                             - A device model name like "GPS180PEX", optionally with the serial number
 *                               appended after an underscore, as in "GPS180PEX_029511026220".
 *                               See ::MBG_DEV_NAME.
 *                             - A device index number as string, e.g. "0" or "3".
 *                             - @a NULL, in which case @p dev_idx has to be a valid device index number.
 * @param[in]   dev_idx               A device index number which is used if @p dev_param_str is @a NULL.
 * @param[out]  dev_name_buffer       A string buffer to which a device name can be written, see ::MBG_DEV_FN.
 * @param[in]   dev_name_buffer_size  The size of the @p dev_name_buffer buffer.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_open_device_by_param
 */
int mbg_open_device_by_param_chk( MBG_DEV_HANDLE *p_dh, const char *dev_param_str, int dev_idx,
                                  char *dev_name_buffer, size_t dev_name_buffer_size )
{
  int rc = mbg_open_device_by_param( p_dh, dev_param_str, dev_idx,
                                     dev_name_buffer, dev_name_buffer_size );
  if ( mbg_rc_is_error( rc ) )
  {
    char msg[256];
    int n = 0;
    n = snprintf_safe( msg, sizeof( msg ), "** Failed to open device" );

    if ( dev_name_buffer[0] )  // String not empty.
      n += snprintf_safe( &msg[n], sizeof( msg ) - n, " %s", dev_name_buffer );
    else
      if ( dev_param_str )     // Parameter string has been specified.
        n += snprintf_safe( &msg[n], sizeof( msg ) - n, " %s", dev_param_str );
      else                     // Device index number.
        n += snprintf_safe( &msg[n], sizeof( msg ) - n, " %i", dev_idx );

    n += snprintf_safe( &msg[n], sizeof( msg ) - n, ": %s", mbg_strerror( rc ) );
    fprintf( stderr, "%s\n\n", msg );
  }

  return rc;

}  // mbg_open_device_by_param_chk



/*HDR*/
/**
 * @brief Main action handler that can be called by utility programs.
 *
 * This function checks the command line parameters passed to the program.
 * Those which have not been evaluated before are interpreted as device specifiers.
 *
 * Device specifiers can be device file names like "/dev/mbgclock0" on target
 * platforms that support such device names, see ::MBG_DEV_FN,
 * or device type names like "GPS180PEX" which can optionally have a serial
 * number appended, like "GPS180PEX_029511026220", to be able to distinguish
 * between several devices of the same type (see ::MBG_DEV_NAME),
 * or just an index number as required for the ::mbg_open_device call.
 *
 * For each of the specified devices, the callback function @p fnc is called
 * to take some specific action on the device.
 *
 * If no device has been specified in the argument list, ::mbg_find_devices is
 * called to set up a device list, and depending on whether ::CHK_DEV_ALL_DEVICES
 * is set in @p chk_dev_flags, the callback function @p fnc is either called
 * for every device from the list, or only for the first one.
 *
 * @param[in]  argc     Number of parameters of the program argument list.
 * @param[in]  argv     Array of program argument strings.
 * @param[in]  optind   Number of the command line arguments that have already been handled.
 * @param[in]  fnc      Pointer to a callback function that actually takes an action on each specified device.
 * @param[in]  chk_dev_flags    Bit mask controlling the behavior of the function, see ::CHK_DEV_FLAGS.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_handle_devices( int argc, char *argv[], int optind,
                        MBG_DEV_HANDLER_FNC *fnc, int chk_dev_flags )
{
  MBG_DEV_FN tmp_dev_fn;
  MBG_DEV_HANDLE dh;
  int devices_specified = argc - optind;
  int devices_found = 0;
  const char *dev_param_str = NULL;
  int rc = MBG_ERR_NO_DEV;
  int i;

  if ( devices_specified )
  {
    // One or more device names have been specified
    // on the command line, so handle each device.
    for ( i = optind; i < argc; i++ )
    {
      dev_param_str = argv[i];

      rc = mbg_open_device_by_param_chk( &dh, dev_param_str, 0,
                                         tmp_dev_fn.s, sizeof( tmp_dev_fn.s ) );

      if ( mbg_rc_is_success( rc ) )
        rc = mbg_handle_device( dh, ( devices_specified > 1 ) ? tmp_dev_fn.s : NULL, fnc );

      // Don't continue if one of the specified devices failed.
      if ( mbg_rc_is_error( rc ) )
        break;
    }

    goto out;
  }


  // No device has been specified on the command line.

  if ( chk_dev_flags & CHK_DEV_WITHOUT_DEV )
  {
    // TODO Check if this branch is useful.
    // We have been called without a device to be used.
    // This may be useful e.g. if we just want to print
    // some general usage information.
    if ( fnc )
    {
      dh = MBG_INVALID_DEV_HANDLE;
      rc = fnc( dh );
    }
    else
    {
      // TODO Should we report an error if we have been called
      // without callback function and without device?
    }
    goto out;
  }


  devices_found = chk_get_num_devices();

  if ( devices_found == 0 )  // No device found.
  {
    // Don't continue without any device, unless
    // explicitly requested to do so.
    if ( !( chk_dev_flags & CHK_DEV_WITHOUT_DEV ) )
    {
      mbg_print_no_device_found();
      rc = MBG_ERR_NO_DEV;
      goto out;
    }

    // We may continue even if no device is available,
    // but must process the callback loop only once.
    devices_found = 1;
  }

  // Unless explicitly requested to handle all devices
  // we have found, we only handle the first one.
  if ( !( chk_dev_flags & CHK_DEV_ALL_DEVICES ) )
    devices_found = 1;

  for ( i = 0; i < devices_found; i++ )
  {
    rc = mbg_open_device_by_param_chk( &dh, NULL, i, tmp_dev_fn.s, sizeof( tmp_dev_fn.s ) );

    if ( mbg_rc_is_success( rc ) )
      rc = mbg_handle_device( dh, ( devices_found > 1 ) ? tmp_dev_fn.s : NULL, fnc );

    // If one of the unspecified devices failed,
    // we continue anyway and don't break here.
  }


out:
  return rc;

}  // mbg_handle_devices



/*HDR*/
/**
 * @brief Print date and time from a ::PCPS_TIME structure to a string.
 *
 * @param[out]  s        Address of a string buffer to be filled.
 * @param[in]   max_len  Size of the string buffer.
 * @param[in]   p        Pointer to a ::PCPS_TIME structure to be evaluated.
 * @param[in]   verbose  Increase verbosity of the output:<br>
 *                       > 0: append %UTC offset and status<br>
 *                       > 1: append signal value.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
int mbg_snprint_date_time( char *s, size_t max_len, const PCPS_TIME *p, int verbose )
{
  int n = 0;

  n += snprintf_safe( &s[n], max_len - n, "%04u-%02u-%02u %02u:%02u:%02u.%02u",
                      mbg_exp_year( p->year, mbg_exp_year_limit ), p->month, p->mday,
                      p->hour, p->min, p->sec, p->sec100 );

  if ( verbose > 0 )
    n += snprintf_safe( &s[n], max_len - n, " (UTC%+ih), st: %02Xh",
                        p->offs_utc, p->status );

  if ( verbose > 1 )
    n += snprintf_safe( &s[n], max_len - n, ", sig: %i", p->signal );

  return n;

}  // mbg_snprint_date_time



/*HDR*/
/**
 * @brief Print date and time from a ::PCPS_TIME_STAMP structure to a string.
 *
 * @param[out]  s         Address of a string buffer to be filled.
 * @param[in]   max_len   Size of the string buffer.
 * @param[in]   p         Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]   utc_offs  A local time offset added to the time stamp before converted to date and time.
 * @param[in]   show_raw  Flag indicating if a raw timestamp (hex) is to be printed, too.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
int mbg_snprint_hr_tstamp( char *s, size_t max_len, const PCPS_TIME_STAMP *p,
                           long utc_offs, int show_raw )
{
  long l;
  time_t t;
  struct tm tm = { 0 };
  int n = 0;
  int rc;

  if ( show_raw )
    n += snprintf_safe( &s[n], max_len - n, "raw: 0x%08lX.%08lX, ",
                        (ulong) p->sec, (ulong) p->frac );

  // We'll use the standard C library functions to convert the seconds
  // to broken-down calendar date and time.
  l = (long) p->sec + utc_offs;
  t = cvt_to_time_t( l );

  // Our time stamp may be UTC, or have been converted to local time.
  // Anyway, since we don't want to account for the system's time zone
  // settings, we always use the gmtime() function for conversion,
  // or our own wrapper, respectively:
  rc = mbg_gmtime( &tm, &t );

  if ( mbg_rc_is_success( rc ) )
    n += snprintf_safe( &s[n], max_len - n, "%04i-%02i-%02i %02i:%02i:%02i." PCPS_HRT_FRAC_SCALE_FMT,
                        tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
                        tm.tm_hour, tm.tm_min, tm.tm_sec,
                        (ulong) bin_frac_32_to_dec_frac( p->frac, PCPS_HRT_FRAC_SCALE ) );
  else
    n += snprint_gmtime_error( &s[n], max_len - n, rc, t, __func__ );

  return n;

}  // mbg_snprint_hr_tstamp



/*HDR*/
/**
 * @brief Print date and time from a ::PCPS_HR_TIME structure to a string.
 *
 * Converts the %UTC timestamp first to local time according to
 * the ::PCPS_HR_TIME::utc_offs value.
 *
 * @param[out]  s         Address of a string buffer to be filled.
 * @param[in]   max_len   Size of the string buffer.
 * @param[in]   p         Pointer to a ::PCPS_HR_TIME structure to be evaluated.
 * @param[in]   show_raw  Flag indicating if a raw timestamp (hex) is to be printed, too.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
int mbg_snprint_hr_time( char *s, size_t max_len, const PCPS_HR_TIME *p, int show_raw )
{
  char ws[80];
  PCPS_TIME_STAMP ts = p->tstamp;
  int n;
  const char *time_scale_name;
  const char *cp;

  // If the local time offset is not 0, add this to the time stamp
  // and set up a string telling the offset.
  if ( p->utc_offs )
  {
    snprint_utc_offs( ws, sizeof( ws ), NULL, p->utc_offs );
    cp = ws;
  }
  else
    cp = "";   // No local time offset.


  // Convert the local time stamp to calendar date and time.
  n = mbg_snprint_hr_tstamp( s, max_len, &ts, p->utc_offs, show_raw );

  // By default, the time stamp represents UTC plus an optional local time offset.
  time_scale_name = "UTC";

  // However, some device may have been configured to output TAI or GPS time.
  if ( p->status & PCPS_SCALE_TAI )
    time_scale_name = "TAI";      // Time stamp represents TAI.
  else
    if ( p->status & PCPS_SCALE_GPS )
      time_scale_name = "GPS";    // Time stamp represents GPS system time.

  n += snprintf_safe( &s[n], max_len - n, " %s%s", time_scale_name, cp );

  return n;

}  // mbg_snprint_hr_time



/*HDR*/
/**
 * @brief Print date and time from a ::PCPS_TIME_STAMP structure.
 *
 * First the HR timestamp passed by parameter @p p_ts is printed.
 *
 * If the parameter @p p_prv_ts is not @a NULL, it should specify
 * an earlier timestamp, and the elapsed time since @p p_ts
 * is appended.
 *
 * Finally the latency value is printed in microseconds, unless
 * parameter @p no_latency is != 0.
 *
 * @param[in]  p_ts         Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]  hns_latency  A latency number in hectonanoseconds, i.e. 100 ns units.
 * @param[in]  p_prv_ts     Pointer to a ::PCPS_TIME_STAMP structure to be evaluated, may be @a NULL.
 * @param[in]  no_latency   A flag indicating if printing the latency should be suppressed.
 * @param[in]  show_raw     Flag indicating if a raw timestamp (hex) is to be printed, too.
 *
 * @see ::mbg_print_hr_time
 */
void mbg_print_hr_timestamp( PCPS_TIME_STAMP *p_ts, int32_t hns_latency, PCPS_TIME_STAMP *p_prv_ts,
                             int no_latency, int show_raw )
{
  char ws[80];

  mbg_snprint_hr_tstamp( ws, sizeof( ws ), p_ts, 0, show_raw );
  printf( "HR time %s", ws );

  if ( p_prv_ts )
    printf( " (%+.1f us)", delta_timestamp_us( p_ts, p_prv_ts ) );

  if ( !no_latency )
    printf( ", latency: %.1f us", ( (double) hns_latency ) / 10 );

  puts( str_empty );

}  // mbg_print_hr_timestamp



/*HDR*/
/**
 * @brief Print date and time from a ::PCPS_HR_TIME structure.
 *
 * First the HR timestamp passed by parameter @p p_ht is printed.
 *
 * If the parameter @p p_prv_ts is not @a NULL, it should specify
 * an earlier timestamp, and the elapsed time since @p p_ht
 * is appended.
 *
 * Next the latency value is printed in microseconds, unless
 * parameter @p no_latency is != 0.
 *
 * @param[in]  p_ht         Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]  hns_latency  A latency number in hectonanoseconds, i.e. 100 ns units.
 * @param[in]  p_prv_ts     Pointer to a ::PCPS_TIME_STAMP structure to be evaluated, may be @a NULL.
 * @param[in]  no_latency   A flag indicating if printing the latency should be suppressed.
 * @param[in]  show_raw     Flag indicating if a raw timestamp (hex) is to be printed, too.
 * @param[in]  verbose      Increase verbosity of the output:<br>
 *                           > 0: append status code<br>
 *                           > 1: append signal value.
 *
 * @see ::mbg_print_hr_timestamp
 */
void mbg_print_hr_time( PCPS_HR_TIME *p_ht, int32_t hns_latency, PCPS_TIME_STAMP *p_prv_ts,
                        int no_latency, int show_raw, int verbose )
{
  char ws[80];

  mbg_snprint_hr_time( ws, sizeof( ws ), p_ht, show_raw );
  printf( "HR time %s", ws );

  if ( p_prv_ts )
    printf( " (%+.1f us)", delta_timestamp_us( &p_ht->tstamp, p_prv_ts ) );

  if ( verbose > 0 )
    printf( ", st: 0x%04lX", (ulong) p_ht->status );

  if ( verbose > 1 )
    printf( ", sig: %i", p_ht->signal );

  if ( !no_latency )
    printf( ", latency: %.1f us", ( (double) hns_latency ) / 10 );

  puts( "" );

}  // mbg_print_hr_time



/*HDR*/
/**
 * @brief Print PZF correlation info for a device which supports this.
 *
 * @param[in]  p_ci            Pointer to a ::CORR_INFO structure that has been read before.
 * @param[in]  show_corr_step  A flag indicating if correlation step indicators are to be appended.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_show_pzf_corr_info( const CORR_INFO *p_ci, int show_corr_step )
{
  char ws[80];
  const char *cp;

  if ( p_ci->status < N_PZF_CORR_STATE )
    cp = pzf_corr_state_name[p_ci->status];
  else
  {
    snprintf_safe( ws, sizeof( ws ), "unknown status code: 0x%02X", p_ci->status );
    cp = ws;
  }

  printf( "%s, PZF correlation: %u%%", cp, p_ci->val );

  if ( show_corr_step )
    if ( p_ci->corr_dir != ' ' )
      printf( " Shift: %c", p_ci->corr_dir );

  return MBG_SUCCESS;

}  // mbg_show_pzf_corr_info



/*HDR*/
/**
 * @brief Retrieve a ::MBG_GNSS_MODE_INFO structure from a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_GNSS_MODE_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_get_gps_gnss_mode_info_chk( MBG_DEV_HANDLE dh, MBG_GNSS_MODE_INFO *p )
{
  int rc = mbg_get_gps_gnss_mode_info( dh, p );
  mbg_cond_err_msg( rc, "mbg_get_gps_gnss_mode_info" );

  return rc;

}  // mbg_get_gps_gnss_mode_info_chk



