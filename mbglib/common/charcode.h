
/**************************************************************************
 *
 *  $Id: charcode.h 1.4 2016/08/10 12:25:41 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for pcpslstr.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: charcode.h $
 *  Revision 1.4  2016/08/10 12:25:41  martin
 *  Check for MBG_TGT_POSIX instead of MBG_TGT_UNIX.
 *  Revision 1.3  2015/11/11 14:55:59  martin
 *  Support Unicode and UTF-8 encodings.
 *  Revision 1.2  2014/04/24 14:13:17  martin
 *  Added new codes and conversion table initializers.
 *  Revision 1.1  2012/11/20 13:41:24  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _CHARCODE_H
#define _CHARCODE_H


/* Other headers to be included */

#include <mbg_tgt.h>

#if MBG_TGT_HAS_WCHAR_T
  #include <wchar.h>
#endif

#ifdef _CHARCODE
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


#if defined( MBG_TGT_LINUX )

  #define MBG_UML_UTF8

#elif defined( MBG_TGT_WIN32 ) \
   || defined( MBG_TGT_POSIX ) \
   || defined( MBG_TGT_QNX )

  #define MBG_UML_ANSI

#else

  #define MBG_UML_DOS

#endif



// upper case 'A' umlaut
#define ANSI_UC_A_UML     '\xC4'      // uppercase ANSI char
#define ANSI_US_A_UML     "\xC4"      // uppercase ANSI string literal

#define DOS_UC_A_UML      '\x8E'      // uppercase DOS char
#define DOS_US_A_UML      "\x8E"      // uppercase DOS string literal

#define UNICODE_US_A_UML  "\u00C4"    // uppercase Unicode string literal

#define UTF8_US_A_UML     "\xc3\x84"  // uppercase UTF-8 string literal



// upper case 'O' umlaut
#define ANSI_UC_O_UML     '\xD6'      // uppercase ANSI char
#define ANSI_US_O_UML     "\xD6"      // uppercase ANSI string literal

#define DOS_UC_O_UML      '\x99'      // uppercase DOS char
#define DOS_US_O_UML      "\x99"      // uppercase DOS string literal

#define UNICODE_US_O_UML  "\u00D6"    // uppercase Unicode string literal

#define UTF8_US_O_UML     "\xc3\x96"  // uppercase UTF-8 string literal



// upper case 'U' umlaut
#define ANSI_UC_U_UML     '\xDC'      // uppercase ANSI char
#define ANSI_US_U_UML     "\xDC"      // uppercase ANSI string literal

#define DOS_UC_U_UML      '\x9A'      // uppercase DOS char
#define DOS_US_U_UML      "\x9A"      // uppercase DOS string literal

#define UNICODE_US_U_UML  "\u00DC"    // uppercase Unicode string literal

#define UTF8_US_U_UML     "\xc3\x9c"  // uppercase UTF-8 string literal



// lower case 'a' umlaut
#define ANSI_LC_A_UML     '\xE4'      // lowercase ANSI char
#define ANSI_LS_A_UML     "\xE4"      // lowercase ANSI string literal

#define DOS_LC_A_UML      '\x84'      // lowercase DOS char
#define DOS_LS_A_UML      "\x84"      // lowercase DOS string literal

#define UNICODE_LS_A_UML  "\u00E4"    // lowercase Unicode string literal

#define UTF8_LS_A_UML     "\xc3\xa4"  // lowercase UTF-8 string literal



// lower case 'o' umlaut
#define ANSI_LC_O_UML     '\xF6'      // lowercase ANSI char
#define ANSI_LS_O_UML     "\xF6"      // lowercase ANSI string literal

#define DOS_LC_O_UML      '\x94'      // lowercase DOS char
#define DOS_LS_O_UML      "\x94"      // lowercase DOS string literal

#define UNICODE_LS_O_UML  "\u00F6"    // lowercase Unicode string literal

#define UTF8_LS_O_UML     "\xc3\xb6"  // lowercase UTF-8 string literal



// lower case 'u' umlaut
#define ANSI_LC_U_UML     '\xFC'      // lowercase ANSI char
#define ANSI_LS_U_UML     "\xFC"      // lowercase ANSI string literal

#define DOS_LC_U_UML      '\x81'      // lowercase DOS char
#define DOS_LS_U_UML      "\x81"      // lowercase DOS string literal

#define UNICODE_LS_U_UML  "\u00FC"    // lowercase Unicode string literal

#define UTF8_LS_U_UML     "\xc3\xbc"  // lowercase UTF-8 string literal



// 'sz' umlaut
#define ANSI_LC_SZ_UML    '\xDF'      // ANSI char
#define ANSI_LS_SZ_UML    "\xDF"      // ANSI string literal

#define DOS_LC_SZ_UML     '\xE1'      // DOS char
#define DOS_LS_SZ_UML     "\xE1"      // DOS string literal

#define UNICODE_LS_SZ_UML "\u00DF"    // Unicode string literal

#define UTF8_LS_SZ_UML    "\xc3\x9f"  // UTF-8 string literal



// degree character
#define ANSI_C_DEGREE     '\xB0'      // ANSI char
#define ANSI_S_DEGREE     "\xB0"      // ANSI string literal

#define DOS_C_DEGREE      '\xF8'      // DOS char
#define DOS_S_DEGREE      "\xF8"      // DOS string literal

#define UNICODE_S_DEGREE  "\u00E0"    // Unicode string liter

#define UTF8_S_DEGREE     "\xc2\xb0"  // UTF-8 string literal



// greek mu character (micro sign)
#define ANSI_C_MU         '\xB5'      // ANSI char
#define ANSI_S_MU         "\xB5"      // ANSI string literal

#define DOS_C_MU          '\xE6'      // DOS char
#define DOS_S_MU          "\xE6"      // DOS string literal

#define UNICODE_S_MU      "\u00B5"    // Unicode string liter

#define UTF8_S_MU         "\xc2\xb5"  // UTF-8 string literal



#if defined( MBG_UML_UTF8 )

  #define UCAE   UTF8_US_A_UML
  #define UCOE   UTF8_US_O_UML
  #define UCUE   UTF8_US_U_UML

  #define LCAE   UTF8_LS_A_UML
  #define LCOE   UTF8_LS_O_UML
  #define LCUE   UTF8_LS_U_UML

  #define LCSZ   UTF8_LS_SZ_UML
  #define DEG    UTF8_S_DEGREE
  #define MU     UTF8_S_MU

#elif defined( MBG_UML_ANSI )

  #define UCAE   ANSI_US_A_UML
  #define UCOE   ANSI_US_O_UML
  #define UCUE   ANSI_US_U_UML

  #define LCAE   ANSI_LS_A_UML
  #define LCOE   ANSI_LS_O_UML
  #define LCUE   ANSI_LS_U_UML

  #define LCSZ   ANSI_LS_SZ_UML
  #define DEG    ANSI_S_DEGREE
  #define MU     ANSI_S_MU

#elif defined( MBG_UML_DOS )

  #define UCAE   DOS_US_A_UML
  #define UCOE   DOS_US_O_UML
  #define UCUE   DOS_US_U_UML

  #define LCAE   DOS_LS_A_UML
  #define LCOE   DOS_LS_O_UML
  #define LCUE   DOS_LS_U_UML

  #define LCSZ   DOS_LS_SZ_UML
  #define DEG    DOS_S_DEGREE
  #define MU     DOS_S_MU

#else

  #error Need to define default encoding for umlauts.

#endif



// A string initializer which can be used to set up a string
// to check if all umlauts are displayed correctly.
#define UMLAUTS_STRING    UCAE UCOE UCUE  LCAE LCOE LCUE  LCSZ DEG MU


#define ANSI_UMLAUTS                           \
{                                              \
  ANSI_UC_A_UML, ANSI_UC_O_UML, ANSI_UC_U_UML, \
  ANSI_LC_A_UML, ANSI_LC_O_UML, ANSI_LC_U_UML, \
  ANSI_LC_SZ_UML, ANSI_C_DEGREE, ANSI_C_MU, 0  \
}


#define DOS_UMLAUTS                         \
{                                           \
  DOS_UC_A_UML, DOS_UC_O_UML, DOS_UC_U_UML, \
  DOS_LC_A_UML, DOS_LC_O_UML, DOS_LC_U_UML, \
  DOS_LC_SZ_UML, DOS_C_DEGREE, DOS_S_MU, 0  \
}


/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

/* (no header definitions found) */

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif

/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _CHARCODE_H */
