
/**************************************************************************
 *
 *  $Id: mbg_syslog.h 1.3 2021/04/07 17:49:04 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Provide a type-safe prototype for a syslog-like logging function.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_syslog.h $
 *  Revision 1.3  2021/04/07 17:49:04  martin
 *  Removed trailing spaces.
 *  Revision 1.2  2021/03/25 17:21:45  martin
 *  Updated some comments.
 *  Revision 1.1  2021/03/23 15:26:09  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_SYSLOG_H
#define _MBG_SYSLOG_H


/* Other headers to be included */

#ifdef _MBG_SYSLOG
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Type of a common log function like @a syslog.
 *
 * Can be used to define a function pointer that defaults to
 * @a syslog but can be changed to point to any special function
 * with <em>syslog</em>-like calling convention, e.g.:
 *
 * @code{.c}
   MBG_SYSLOG_FNC *my_log_fnc = syslog;

   my_log_fnc = alternate_log_function;
 * @endcode
 *
 * This allows for a very versatile logging feature.
 *
 * Using this function type makes sure that the compiler can
 * check that the variable function arguments are appropriate
 * for the given format string, as with @a printf.
 *
 * @param[in]  lvl  The log level, as with @a syslog.
 * @param[in]  fmt  Format string according to subsequent parameters.
 * @param[in]  ...  Variable argument list according to the @p fmt format string.
 */
typedef __attribute__( ( format( printf, 2, 3 ) ) )
   void MBG_SYSLOG_FNC( int lvl, const char *fmt, ... );



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

/* (no header definitions found) */

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_SYSLOG_H */
