
/**************************************************************************
 *
 *  $Id: cfg_hlp.c 1.5.2.2 2022/12/19 15:48:24 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Meinberg device configuration helper functions.
 *
 * -----------------------------------------------------------------------
 *  $Log: cfg_hlp.c $
 *  Revision 1.5.2.2  2022/12/19 15:48:24  martin.burnicki
 *  Removed obsolete _int_from_size_t() stuff.
 *  Revision 1.5.2.1  2022/12/15 16:02:41Z  martin.burnicki
 *  Fix potential compiler warnings, and optionally exclude code
 *  from build using preprocessor symbol OMIT_PTPV2.
 *  Revision 1.5  2019/09/27 14:16:40Z  martin
 *  Merged changes from 1.3.1.33:
 *  Support for next gen PTP config API added by thomas-b.
 *  XMR improvements by philipp.
 *  Sys ref API definitions by thomas-b.
 *  Network address conversion functions added by gregoire.diehl.
 *  New functions were added, and existing functions were improved.
 *  Use new types MBG_MSG_IDX and MBG_MSG_IDX_32.
 *  Lots of doxygen updates and fixes.
 *  Revision 1.4  2018/09/19 16:55:08Z  martin
 *  Account for renamed global variables.
 *  Revision 1.3  2018/07/05 10:28:07Z  martin
 *  Renamed setup_port_info_from_port_settings()
 *  to setup_port_info_from_port_parm().
 *  Added function chk_dev_receiver_info().
 *  Function free_all_firmware_info() added by thomas-b.
 *  Syslog option added to monitoring feature by philipp.
 *  I/O helper structures and API refuctored by philipp.
 *  Service feature and API added by philipp.
 *  Database feature support added by philipp.
 *  Function to free ALL_USER_INFO structure added by thomas-b.
 *  MBG_EVENT stuff integrated with common/general monitoring
 *  stuff by philipp.
 *  Monitoring events refactored and tainted config support
 *  added by philipp.
 *  Account for renamed library symbol NSEC_PER_SEC.
 *  Documented some conversion functions for legacy parameters.
 *  Doxygen fixes.
 *  Revision 1.2  2017/07/05 12:17:38  martin
 *  Support functions for TLV, IMS, GPIO, and
 *  mbg_snprint_revision() provided by philipp.
 *  Support functions for network configuration,
 *  NTP configuration, ALL_UCAP stuff, SNMP and
 *  MONITORING, as well as ALL_PTP_V2_COMMON_DATASETS
 *  and ALL_PTP_V1_COMMON_DATASETS provided by thomas-b.
 *  Support functions for xmulti_ref and IO PORT stuff
 *  provided by philipp and thomas-b.
 *  More common GNSS support.
 *  New functions alloc_dev_hw_id() and chk_free_dev_hw_id().
 *  Tried portable printing of int64_t types.
 *  Account for frac_sec_from_bin() obsoleted by
 *  bin_frac_32_to_dec_frac().
 *  Revision 1.1  2014/04/25 09:14:49  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _CFG_HLP
 #include <cfg_hlp.h>
#undef _CFG_HLP

#if defined( MBG_TGT_UNIX )
  #include <arpa/inet.h>
  #include <str_util.h>
  #include <math.h>
#endif

#include <mbgerror.h>
#include <timeutil.h>
#include <str_util.h>
#include <lan_util.h>
#include <myutil.h>
#include <mbgtime.h>

#if defined( _PRELIMINARY_CODE )
  #include <mbgmktm.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>



/*HDR*/
/**
 * @brief Check if a software revision name should be displayed.
 *
 * The software revision name is usually empty, except if the
 * firmware is a customized version, in which case the field
 * contains an identifier string.
 *
 * There are some standard firmware versions where this string
 * is not empty but padded with spaces, etc., so we try to
 * clean this up, so it can be displayed appropriately.
 *
 * @param[in,out]  p        The ::SW_REV name to check.
 * @param[in]      verbose  The app's verbosity level.
 *
 * @return  != 0 if SW name should be displayed.
 */
int chk_sw_rev_name( SW_REV *p, int verbose )
{
  if ( verbose > 1 )  // more than just verbose
    return 1;         // just return raw string

  trim_whitespace( p->name );

  // Some firmware versions have "CC_STANDARD" in their standard version,
  // which doesn't provide any valuable information.
  // We discard this by default.
  if ( strstr( p->name, "CC_STANDARD" ) )
    p->name[0] = 0;

  if ( verbose )
    return 1;  // calling app should display string, even if empty

  // calling app should display string only if not empty
  return strlen( p->name ) != 0;

}  // chk_sw_rev_name



/*HDR*/
/**
 * @brief Search a string table for a specific string.
 *
 * @param[in]  search     The string to be searched for in table @a str_table.
 * @param[in]  str_table  A table of strings like 'const char *strs[N_STRS]'.
 * @param[in]  n_entries  The number of strings in table @a str_table.
 *
 * @return  The index number of the string table entry matching the @a search string,
 *          or -1 if the @a search string could not be found.
 */
int get_str_idx( const char *search,
                 const char *str_table[],
                 int n_entries )
{
  int i;

  for ( i = 0; i < n_entries; i++ )
    if ( strcmp( search, str_table[i] ) == 0 )
      return i;

  return -1;

}  // get_str_idx



/*HDR*/
/**
 * @brief Search the ::mbg_baud_rates table for a specific baud rate.
 *
 * @param[in]  baud_rate  The baud_rate to be searched for.
 *
 * @return  The index number of the specific @a baud_rate in table ::mbg_baud_rates,
 *          or -1 if the specific @a baud_rate could not be found.
 */
int get_baud_rate_idx( BAUD_RATE baud_rate )
{
  int i;

  for ( i = 0; i < N_MBG_BAUD_RATES; i++ )
    if ( baud_rate == mbg_baud_rates[i] )
      return i;

  return -1;

}  // get_baud_rate_idx



/*HDR*/
/**
 * @brief Search the ::mbg_framing_strs table for a specific framing string.
 *
 * @param[in]  framing  The framing string to be searched for, e.g. "8N1".
 *
 * @return  The index number of the specific @a framing in table ::mbg_framing_strs,
 *          or -1 if the specific @a framing could not be found.
 */
int get_framing_idx( const char *framing )
{
  return get_str_idx( framing, mbg_framing_strs, N_MBG_FRAMINGS );

}  // get_framing_idx



/*HDR*/
/**
 * @brief Convert ::PORT_PARM::mode to ::PORT_SETTINGS::mode
 *
 * This function is used to evaluate the code from a @a mode field of a legacy
 * ::PORT_PARM structure and set up the appropriate fields in a ::PORT_SETTINGS
 * structure.
 *
 * @param[out]  p_ps         Pointer to a ::PORT_SETTINGS structure to be updated.
 * @param[in]   pp_mode      The mode code from a ::PORT_PARM structure, see ::LGCY_STR_MODES.
 * @param[in]   cap_str_idx  The capture string index number for the case that @a pp_mode is
 *                           a capture string mode. Usually 1 for legacy devices.
 *
 * @ingroup cfg_hlp_com_parm_cnv_fncs
 * @see ::port_parm_mode_from_port_settings (the complementary function)
 * @see ::port_settings_from_port_parm
 */
void port_settings_from_port_parm_mode( PORT_SETTINGS *p_ps, uint8_t pp_mode,
                                        int cap_str_idx )
{
  if ( pp_mode >= LGCY_STR_UCAP )
  {
    // If @a pp_mode is ::LGCY_STR_UCAP or greater
    // then the string is a capture string which
    // can have the mode ::STR_AUTO or ::STR_ON_REQ,
    // and the string type is a capture string, which
    // usually has index 1 for legacy devices.
    p_ps->str_type = cap_str_idx;
    p_ps->mode = ( pp_mode == LGCY_STR_UCAP ) ? STR_AUTO : STR_ON_REQ;
  }
  else
  {
    // If @a pp_mode is less than ::LGCY_STR_UCAP
    // then the string is the default time string,
    // the format of which depends on the firmware
    // version if the device is legacy.
    // The mode numbers are compatible in this case.
    p_ps->str_type = 0;
    p_ps->mode = pp_mode;
  }

}  // port_settings_from_port_parm_mode



/*HDR*/
/**
 * @brief Convert a ::PORT_SETTINGS::mode to a legacy ::PORT_PARM::mode
 *
 * This function is used to derive a a legacy ::PORT_PARM::mode value
 * from a ::PORT_SETTINGS structure.
 *
 * @param[out]  pp_mode      Pointer to a ::PORT_PARM::mode variable to be updated.
 * @param[in]   p_ps         Pointer to a ::PORT_SETTINGS structure to be evaluated.
 * @param[in]   cap_str_idx  The capture string index number for legacy devices, which is usually 1.
 *
 * @ingroup cfg_hlp_com_parm_cnv_fncs
 * @see ::port_settings_from_port_parm_mode (the complementary function)
 * @see ::port_parm_from_port_settings
 */
void port_parm_mode_from_port_settings( uint8_t *pp_mode, const PORT_SETTINGS *p_ps,
                                        int cap_str_idx )
{
  if ( p_ps->str_type == cap_str_idx )
  {
    // If the string type is a capture string
    // then convert the mode accordingly.
    *pp_mode = ( p_ps->mode == STR_ON_REQ ) ? LGCY_STR_UCAP_REQ : LGCY_STR_UCAP;
  }
  else
  {
    // Other string modes are compatible, so just copy the mode.
    *pp_mode = p_ps->mode;
  }

}  // port_parm_mode_from_port_settings



/*HDR*/
/**
 * @brief Set up a ::PORT_SETTINGS structure from a legacy ::PORT_PARM structure.
 *
 * @param[out]  p_ps         Pointer to a ::PORT_SETTINGS structure to be updated.
 * @param[in]   port_idx     Index number of the port settings to be converted.
 * @param[in]   p_pp         The ::PORT_PARM structure to be evaluated, contains settings for 2 ports.
 * @param[in]   cap_str_idx  The capture string index number for legacy devices, which is usually 1.
 *
 * @ingroup cfg_hlp_com_parm_cnv_fncs
 * @see ::port_parm_from_port_settings (the complementary function)
 * @see ::port_settings_from_port_parm_mode
 */
void port_settings_from_port_parm( PORT_SETTINGS *p_ps, int port_idx,
                                   const PORT_PARM *p_pp, int cap_str_idx )
{
  // The basic serial settings can simply be copied.
  p_ps->parm = p_pp->com[port_idx];

  // The string mode may need to be translated.
  port_settings_from_port_parm_mode( p_ps, p_pp->mode[port_idx],
                                     cap_str_idx );

}  // port_info_from_port_parm



/*HDR*/
/**
 * @brief Set up a a legacy ::PORT_PARM structure from a ::PORT_SETTINGS structure.
 *
 * @param[out]  p_pp         Pointer to a ::PORT_PARM structure to be updated.
 * @param[in]   port_idx     Index number of the port settings to be converted.
 * @param[out]  p_ps         Pointer to a ::PORT_SETTINGS structure to be updated.
 * @param[in]   cap_str_idx  The capture string index number for legacy devices, which is usually 1.
 *
 * @ingroup cfg_hlp_com_parm_cnv_fncs
 * @see ::port_settings_from_port_parm (the complementary function)
 * @see ::port_parm_mode_from_port_settings
 */
void port_parm_from_port_settings( PORT_PARM *p_pp, int port_idx,
                                   const PORT_SETTINGS *p_ps, int cap_str_idx )
{
  // The basic serial settings can simply be copied.
  p_pp->com[port_idx] = p_ps->parm;

  // The string mode may need to be translated.
  port_parm_mode_from_port_settings( &p_pp->mode[port_idx],
                                     p_ps, cap_str_idx );

}  // port_parm_from_port_settings



/*HDR*/
/**
 * @brief Check if all serial port capabilities reported by a device are supported by the current driver.
 *
 * @param[in]   p_pi               Address of a ::PORT_INFO structure to be checked.
 * @param[out]  str_type_info_idx  An array of ::STR_TYPE_INFO_IDX structures supported by the device
 * @param[in]   n_str_type         The number of entries in the @a str_type_info_idx array.
 *
 * @return  A bit mask of @ref MBG_COM_CFG_STATUS_MASKS flags indicating which type of parameter
 *          may not be fully supported. Should be 0 if everything is OK.
 *
 * @see ::is_valid_port_info
 * @see @ref MBG_COM_CFG_STATUS_MASKS
 */
uint32_t check_valid_port_info( const PORT_INFO *p_pi,
                                const STR_TYPE_INFO_IDX str_type_info_idx[],
                                int n_str_type )
{
  const PORT_SETTINGS *p_ps = &p_pi->port_settings;
  int idx;
  uint32_t flags = 0;


  if ( p_pi->supp_baud_rates & ~_mask( N_MBG_BAUD_RATES ) )
    flags |= MBG_PS_MSK_BAUD_RATE_OVR_SW;  // dev. supports more baud rates than driver

  idx = get_baud_rate_idx( p_ps->parm.baud_rate );

  if ( !_inrange( idx, 0, N_MBG_BAUD_RATES ) ||
       !_is_supported( idx, p_pi->supp_baud_rates ) )
    flags |= MBG_PS_MSK_BAUD_RATE;


  if ( p_pi->supp_framings & ~_mask( N_MBG_FRAMINGS ) )
    flags |= MBG_PS_MSK_FRAMING_OVR_SW;    // dev. supports more framings than driver

  idx = get_framing_idx( p_ps->parm.framing );

  if ( !_inrange( idx, 0, N_MBG_FRAMINGS ) ||
       !_is_supported( idx, p_pi->supp_framings ) )
    flags |= MBG_PS_MSK_FRAMING;


  if ( p_ps->parm.handshake >= N_COM_HS )
    flags |= MBG_PS_MSK_HS_OVR_SW;         // handshake index exceeds max.

  if ( p_ps->parm.handshake != HS_NONE )   // currently no device supports any handshake
    flags |= MBG_PS_MSK_HS;                // handshake mode not supp. by dev.


  if ( p_pi->supp_str_types & ~_mask( n_str_type ) )
    flags |= MBG_PS_MSK_STR_TYPE_OVR_SW;   // firmware error: more string types supported than reported

  idx = p_ps->str_type;

  if ( idx >= n_str_type )
    flags |= MBG_PS_MSK_STR_TYPE_OVR_DEV;  // string type index exceeds max.
  else
  {
    if ( !_is_supported( idx, p_pi->supp_str_types ) )
      flags |= MBG_PS_MSK_STR_TYPE;        // string type not supported by this port
    else
    {
      // Use the str_type index to get the supported output mode mask
      // from the string type info table. This is required to check
      // whether the selected mode is supported by the selected
      // string type.
      ulong supp_modes = str_type_info_idx[idx].str_type_info.supp_modes;

      if ( supp_modes & ~_mask( N_STR_MODE ) )
        flags |= MBG_PS_MSK_STR_MODE_OVR_SW;  // dev. supports more string modes than driver

      idx = p_ps->mode;

      if ( idx >= N_STR_MODE )                // mode is always >= 0
        flags |= MBG_PS_MSK_STR_MODE_OVR_SW;  // string mode index exceeds max.
      else
        if ( !_is_supported( idx, supp_modes ) )
          flags |= MBG_PS_MSK_STR_MODE;       // string mode not supp. by this string type and port
    }
  }


  if ( p_ps->flags != 0 )            /* currently always 0 */
    flags |= MBG_PS_MSK_FLAGS_OVR_SW | MBG_PS_MSK_FLAGS;


  return flags;

}  // check_valid_port_info



/*HDR*/
/**
 * @brief Check if the content of a ::PORT_INFO structure is known and valid.
 *
 * @param[in]  p_pi               Address of a ::PORT_INFO structure to be checked.
 * @param[in]  str_type_info_idx  An array of string type information.
 * @param[in]  n_str_type         The number of entries in the @a str_type_info_idx array.
 *
 * @return  @a true if the ::PORT_INFO structure contains valid data,
 *          else @a false.
 *
 * @see ::check_valid_port_info
 */
bool is_valid_port_info( const PORT_INFO *p_pi,
                         const STR_TYPE_INFO_IDX str_type_info_idx[],
                         int n_str_type )
{
  return check_valid_port_info( p_pi, str_type_info_idx, n_str_type ) == 0;

}  // valid_port_info



/*HDR*/
/**
 * @brief Setup an array of ::PORT_INFO_IDX structures from a ::PORT_PARM.
 *
 * Some legacy GPS receivers that don't provide a ::RECEIVER_INFO structure
 * also provide a ::PORT_PARM structure with the current serial port settings
 * only.
 *
 * This function sets up an array of ::PORT_INFO_IDX structures with the
 * associated settings, and fills up the remaining ::PORT_INFO fields with
 * the well-known supported settings, so applications can simply deal with
 * the current API structures.
 *
 * @param[out]  pii   Array with @a p_ri->n_com_ports ::PORT_INFO_IDX elements to be filled up.
 * @param[in]   p_pp  Pointer to a ::PORT_PARM structure providing the current COM port settings.
 * @param[in]   p_ri  Pointer to a ::RECEIVER_INFO structure providing the number of supported COM ports.
 *
 * @return  One of the @ref MBG_RETURN_CODES, but actually always ::MBG_SUCCESS.
 *
 * @ingroup cfg_hlp_com_parm_cnv_fncs
 * @see setup_default_str_type_info_idx
 */
int setup_port_info_from_port_parm( PORT_INFO_IDX pii[], const PORT_PARM *p_pp,
                                    const RECEIVER_INFO *p_ri )
{
  int i;

  for ( i = 0; i < p_ri->n_com_ports; i++ )
  {
    PORT_INFO_IDX *p_pii = &pii[i];
    PORT_INFO *p_pi = &p_pii->port_info;

    p_pii->idx = i;
    port_settings_from_port_parm( &p_pi->port_settings, i, p_pp, 1 );

    p_pi->supp_baud_rates = DEFAULT_GPS_BAUD_RATES_C166;
    p_pi->supp_framings = DEFAULT_GPS_FRAMINGS_C166;
    p_pi->supp_str_types = DEFAULT_SUPP_STR_TYPES_GPS;
  }

  return MBG_SUCCESS;

}  // setup_port_info_from_port_parm



/*HDR*/
/**
 * @brief Setup an array of ::STR_TYPE_INFO_IDX for well-known string types.
 *
 * Some legacy GPS receivers that don't provide a ::RECEIVER_INFO structure
 * also don't support the ::STR_TYPE_INFO structure to indicate which serial
 * string formats are supported.
 *
 * This function sets up an array of ::STR_TYPE_INFO_IDX structures with the
 * well-known supported string types, so applications can simply deal with
 * the current API structures.
 *
 * @param[out]  stii  Array with at least @a p_ri->n_str_type ::STR_TYPE_INFO_IDX elements to be filled up.
 * @param[in]   p_ri  Pointer to a ::RECEIVER_INFO structure providing the number of supported COM ports.
 *
 * @return  One of the @ref MBG_RETURN_CODES, but actually always ::MBG_SUCCESS.
 *
 * @ingroup cfg_hlp_com_parm_cnv_fncs
 * @see ::setup_port_info_from_port_parm
 */
int setup_default_str_type_info_idx( STR_TYPE_INFO_IDX stii[], const RECEIVER_INFO *p_ri )
{
  int i;

  for ( i = 0; i < p_ri->n_str_type; i++ )
  {
    STR_TYPE_INFO_IDX *stip = &stii[i];
    stip->idx = i;
    stip->str_type_info = default_str_type_info[i];
  }

  return MBG_SUCCESS;

}  // setup_default_str_type_info_idx



/*HDR*/
/**
 * @brief Determine how many GNSS systems are marked to be supported.
 *
 * This function counts the number of bits that are set to indicate
 * that specific GNSS types (GPS, GLONASS, Galileo, ...) are supported
 * by the device, and sets up the ::ALL_GNSS_INFO::n_gnss_supp field
 * accordingly.
 *
 * @param[in,out]  p_agi  Address of an ::ALL_GNSS_INFO structure to be updated.
 *
 * @return  ::MBG_SUCCESS on success, or ::MBG_ERR_N_GNSS_EXCEEDS_SUPP if the
 *          number of bits that are set exceeds the number of known GNSS types.
 */
int chk_set_n_gnss_supp( ALL_GNSS_INFO *p_agi )
{
  p_agi->n_gnss_supp = num_bits_set( p_agi->gnss_mode_info.supp_gnss_types );

  if ( p_agi->n_gnss_supp > N_GNSS_TYPES )
    return MBG_ERR_N_GNSS_EXCEEDS_SUPP;

  return MBG_SUCCESS;

}  // chk_set_n_gnss_supp



/*HDR*/
/**
 * @brief Setup GNSS sat info from the legacy GPS ::STAT_INFO.
 *
 * This simplyifies further evaluation since e.g. a common
 * display routine can be used.
 *
 * @param[in,out]  p_agi  Address of an ::ALL_GNSS_INFO structure to be updated.
 */
void setup_gps_only_gnss_sat_info_idx_from_stat_info( ALL_GNSS_INFO *p_agi )
{
  STAT_INFO *p_si = &p_agi->stat_info;
  GNSS_SAT_INFO_IDX *p_gsii = &p_agi->gnss_sat_info_idx[GNSS_TYPE_GPS];
  GNSS_SAT_INFO *p_gsi = &p_gsii->gnss_sat_info;

  memset( p_gsii, 0, sizeof( *p_gsii ) );
  p_gsii->idx = GNSS_TYPE_GPS;

  p_gsi->gnss_type = GNSS_TYPE_GPS;
  p_gsi->svs_in_view = p_si->svs_in_view;
  p_gsi->good_svs = p_si->good_svs;

}  // setup_gps_only_gnss_sat_info_idx_from_stat_info



/*HDR*/
/**
 * @brief Setup GNSS mode info from the legacy GPS ::STAT_INFO.
 *
 * This simplyifies further evaluation since e.g. a common
 * display routine can be used.
 *
 * @param[in,out]  p_agi  Address of an ::ALL_GNSS_INFO structure to be updated.
 */
int setup_gps_only_gnss_mode_info_from_stat_info( ALL_GNSS_INFO *p_agi )
{
  MBG_GNSS_MODE_INFO *p_gmi = &p_agi->gnss_mode_info;

  memset( p_gmi, 0, sizeof( *p_gmi ) );

  p_gmi->supp_gnss_types = MBG_GNSS_TYPE_MSK_GPS;
  p_gmi->settings.gnss_set = p_gmi->supp_gnss_types;

  memset( p_agi->gnss_sat_info_idx, 0, sizeof( p_agi->gnss_sat_info_idx ) );

  setup_gps_only_gnss_sat_info_idx_from_stat_info( p_agi );

  return chk_set_n_gnss_supp( p_agi );

}  // setup_gps_only_gnss_mode_info_from_stat_info



/*HDR*/
void chk_free_dev_hw_id( DEVICE_INFO *p )
{
  if ( p->hw_id )
  {
    free( p->hw_id );
    p->hw_id = NULL;
  }

}  // chk_free_dev_hw_id



/*HDR*/
int alloc_dev_hw_id( DEVICE_INFO *p, size_t len )
{
  if ( p->hw_id )
    return MBG_ERR_ALREADY_ALLOC;


  p->hw_id = (char *) malloc( len );

  if ( p->hw_id == NULL )
    return MBG_ERR_NO_MEM;

  return MBG_SUCCESS;

}  // alloc_dev_hw_id



/*HDR*/
const char *get_fw_id_from_hw_id( const char *hw_id )
{
  int i;

  for ( i = 0; i < N_SUPP_DEV_TOTAL; i++ )
  {
    DEVICE_INFO *p = &device_list[i];

    if ( strlen( p->fw_id ) && p->hw_id )  //### TODO check if this still works as expected
    {
      if ( strcmp( hw_id, p->hw_id ) == 0 )
      {
        if ( strlen( p->fw_id ) > 0 )
          return p->fw_id;

        #if defined( DEBUG )
          fprintf( stderr, "Length of fw_id is 0 in %s for device %i (%s)\n",
                   __func__, i, p->hw_id );
        #endif
      }
    }
  }

  return NULL;

}  // get_fw_id_from_hw_id



/*HDR*/
const char *get_hw_id_from_fw_id( const char *fw_id )
{
  int i;

  for ( i = 0; i < N_SUPP_DEV_TOTAL; i++ )
  {
    DEVICE_INFO *p = &device_list[i];

    if ( strlen( p->fw_id ) && p->hw_id )  //### TODO check if this still works as expected
    {
      if ( strcmp( fw_id, p->fw_id ) == 0 )
      {
        if ( strlen( p->hw_id ) > 0 )
          return p->hw_id;

        #if defined( DEBUG )
          fprintf( stderr, "Length of hw_id is 0 in %s for device %i (%s)\n",
                   __func__, i, p->fw_id );
        #endif
      }
    }
  }

  return NULL;

}  // get_hw_id_from_fw_id



/*HDR*/
/**
 * @brief Return the currently used ::MBG_IO_PORT_TYPE_INFO for the appropriate ::MBG_IO_PORT.
 *
 * @param[in]  port       Pointer to the ::MBG_IO_PORT to search for port_type
 * @param[in]  port_type  Enum value of ::MBG_IO_PORT_TYPES to search for in @a port (::MBG_IO_PORT)
 *
 * @return  Pointer to the ::MBG_IO_PORT_TYPE_INFO_IDX found, or NULL.
 */
MBG_IO_PORT_TYPE_INFO *get_io_port_type_info( const MBG_IO_PORT *port,
                                              uint16_t port_type )
{
  unsigned i;
  MBG_IO_PORT_TYPE_INFO *pti;

  if ( !port )
    return NULL;

  for ( i = 0; i < port->p_info.num_types; ++i )
  {
    pti = &port->pt_infos[i];

    if ( pti->port_type == port_type )
    {
      if ( pti->port_type == MBG_IO_PORT_TYPE_GPIO )
      {
        if ( pti->data.gpio_limits.type == port->setts->data.gpio_settings.type )
          return pti;
      }
      else
        return pti;
    }
  }

  return NULL;

}  // get_io_port_type_info



/*HDR*/
/**
 * @brief Initialize a ::MBG_TLV_ANNOUNCE structure.
 *
 * @param[out]  tlv            Pointer to a ::MBG_TLV_ANNOUNCE structure.
 * @param[in]   uid            Unique sender ID used as identifier with all
 *                             subsequent messages related to this transaction.
 * @param[in]   tlv_feat_type  One of the ::MBG_TLV_FEAT_TYPES.
 * @param[in]   total_bytes    Total number of bytes of all upcoming TLVs.
 */
void mbg_tlv_announce_init( MBG_TLV_ANNOUNCE *tlv, MBG_TLV_UID uid,
                            MBG_TLV_TYPE tlv_feat_type, uint32_t total_bytes )
{
  memset( tlv, 0, sizeof( *tlv ) );
  tlv->data.uid = uid;
  tlv->data.type = tlv_feat_type;
  tlv->data.total_bytes = total_bytes;
  tlv->data.reserved_1 = 0;
  tlv->reserved_1 = 0;
  tlv->reserved_2 = 0;

}  // mbg_tlv_announce_init



/*HDR*/
/**
 * @brief Initialize a ::MBG_TLV structure.
 *
 * @param[out]  tlv          Pointer to a valid ::MBG_TLV structure.
 * @param[in]   uid          Unique sender ID used as identifier for each further
 *                           TLV message related to this type.
 * @param[in]   tlv_type     Type identifier, see ::MBG_TLV_TYPES.
 * @param[in]   total_bytes  Total number of bytes belonging to this TLV transaction
 *                           (which is very likely split into several TLVs)
 */
void mbg_tlv_init( MBG_TLV *tlv, MBG_TLV_UID uid,
                   MBG_TLV_TYPE tlv_type, uint32_t total_bytes )
{
  memset( tlv, 0, sizeof( *tlv ) );
  tlv->hdr.uid = uid;
  tlv->hdr.tlv_type = tlv_type;
  tlv->hdr.cur_bytes = 0;
  tlv->hdr.trans_bytes = 0;
  tlv->hdr.total_bytes = total_bytes;
  tlv->hdr.reserved_1 = 0;
  tlv->hdr.reserved_2 = 0;
  tlv->hdr.reserved_3 = 0;

}  // mbg_tlv_init



/*HDR*/
/**
 * @brief Initialize ::MBG_TLV_RCV_STATE structure.
 *
 * @param[out]  state        Pointer to ::MBG_TLV_RCV_STATE structure to be initialized.
 * @param[in]   uid          Unique sender ID used as identifier for each further
 *                           TLV message related to this type.
 * @param[in]   total_bytes  Total number of bytes belonging to this TLV transaction
 *                           (which is very likely split into several TLVS)
 */
void mbg_tlv_rcv_state_init( MBG_TLV_RCV_STATE *state, MBG_TLV_UID uid, uint32_t total_bytes )
{
  state->data.uid = uid;
  state->data.type = 0;
  state->data.total_bytes = total_bytes;
  state->data.reserved_1 = 0;
  state->read_bytes = 0;
  state->reserved_1 = 0;

}  // mbg_tlv_state_init



/*HDR*/
/**
 * @brief Print some Meinberg OS revision information into a string buffer.
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  prefix   Pointer to an optional prefix string, or NULL.
 * @param[in]  suffix   Pointer to an optional suff string, or NULL.
 * @param[in]  rev      The revision code which will be split into major, minor, and patch version.
 *
 * @return Length of the string in the buffer.
 *
 * @see @ref group_ext_sys_info
 */
int mbg_snprint_revision( char *s, size_t max_len,
                          const char *prefix, const char *suffix,
                          uint32_t rev )
{
  int n = 0;
  uint32_t major, minor, patch;

  if ( prefix )
    n += snprintf_safe( &s[n], max_len - n, "%s", prefix );

  _mbg_decode_revision( rev, major, minor, patch );

  n += snprintf_safe( &s[n], max_len - n, "%u.%u.%u",
                      major, minor, patch);

  if ( suffix )
    n += snprintf_safe( &s[n], max_len - n, "%s", suffix );

  return n;

}  // mbg_snprint_revision



/*HDR*/
/**
 * @brief Check if all ::RECEIVER_INFO capabilities reported by a device are supported by the current driver.
 *
 * @param[in]  p  Address of a ::RECEIVER_INFO structure to be checked.
 *
 * @return  ::MBG_SUCCESS if everything is OK, one of the @ref MBG_ERROR_CODES, as appropriate.
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_receiver_info( const RECEIVER_INFO *p )
{
  // Make sure this program supports at least as many serial ports
  // as the current device.
  if ( p->n_com_ports > MAX_PARM_PORT )
    return MBG_ERR_N_COM_EXCEEDS_SUPP;

  // Make sure this program supports at least as many string types
  // as the current device.
  if ( p->n_str_type > MAX_PARM_STR_TYPE )
    return MBG_ERR_N_STR_EXCEEDS_SUPP;

  // Make sure this program supports at least as many programmable
  // outputs as the current device.
  if ( p->n_prg_out > MAX_PARM_POUT )
    return MBG_ERR_N_STR_EXCEEDS_SUPP;

  return MBG_SUCCESS;

}  // chk_dev_receiver_info



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_xbp_supp_nodes( const ALL_XBP_INFO *info )
{
  if ( info )
  {
    if ( ( info->limits.features & XBP_FEAT_MASK_NODES ) == XBP_FEAT_MASK_NODES )
      return MBG_SUCCESS;

    return MBG_ERR_NOT_SUPP_BY_DEV;
  }

  return MBG_ERR_INV_PARM;

}  // chk_dev_xbp_supp_nodes



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_net_cfg_supp_stage_2( const ALL_NET_CFG_INFO *info )
{
  if ( info->glb_cfg_info.feat_flags & MBG_NET_GLB_SUPP_STAGE_2_MASK )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_net_cfg_supp_stage_2



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_ntp_supp_client( const ALL_NTP_CFG_INFO *info )
{
  if ( ( ( info->glb_info.supp_ntp_roles & NTP_MSK_ROLE_CLIENT ) == NTP_MSK_ROLE_CLIENT ) ||
       ( ( info->glb_info.supp_ntp_roles & NTP_MSK_ROLE_CLIENT_SERVER ) == NTP_MSK_ROLE_CLIENT_SERVER ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_ntp_supp_client



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_ntp_supp_server( const ALL_NTP_CFG_INFO *info )
{
  if ( ( ( info->glb_info.supp_ntp_roles & NTP_MSK_ROLE_SERVER ) == NTP_MSK_ROLE_SERVER ) ||
       ( ( info->glb_info.supp_ntp_roles & NTP_MSK_ROLE_CLIENT_SERVER ) == NTP_MSK_ROLE_CLIENT_SERVER ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_ntp_supp_server



static __mbg_inline /*HDR*/
/**
 * @brief Check if a specific bit is set in ::XMULTI_REF_INSTANCES::flags.
 *
 * This indicates if a specific feature is supported.
 * The ::XMULTI_REF_INSTANCES structure is part of an ::ALL_XMULTI_REF_INFO
 * structure, namely ::ALL_XMULTI_REF_INFO::instances.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure
 *                   of which to check the ::XMULTI_REF_INSTANCES::flags.
 *
 * @param[in]  mask  One of the ::XMR_INST_FLAG_BIT_MASKS.
 *
 * @return  ::MBG_SUCCESS if the flag specified in @a mask is set,
 *          else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @see ::XMR_INST_FLAG_BIT_MASKS
 */
int chk_mrs_instances_flags( const ALL_XMULTI_REF_INFO *info, int mask )
{
  return ( info->instances.flags ) & mask ? MBG_SUCCESS :MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_mrs_instances_flags



/*HDR*/
/**
 * @brief Check if the ::XMULTI_EXT_REF_INSTANCES structure is supported.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 *
 * @return  ::MBG_SUCCESS or ::MBG_ERR_NOT_SUPP_BY_DEV,
 *          as returned by ::chk_mrs_instances_flags.
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_supp_ext_instances( const ALL_XMULTI_REF_INFO *info )
{
  return chk_mrs_instances_flags( info, XMRIF_MSK_EXT_REF_INSTANCES_SUPP );

}  // chk_dev_xmulti_supp_ext_instances



/*HDR*/
/**
 * @brief Check if the ::XMULTI_EXT_REF_INSTANCES structure is supported.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 *
 * @return  ::MBG_SUCCESS or ::MBG_ERR_NOT_SUPP_BY_DEV,
 *          as returned by ::chk_mrs_instances_flags.
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_not_configurable( const ALL_XMULTI_REF_INFO *info )
{
  return chk_mrs_instances_flags( info, XMRIF_MSK_NOT_CONFIGURABLE );

}  // chk_dev_xmulti_not_configurable



/*HDR*/
/**
 * @brief Check if the ::XMRIF_BIT_NO_STATUS bit is set.
 *
 * If this feature bit is set, ***no*** status, stats, etc., are provided.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 *
 * @return  ::MBG_SUCCESS if the bit is set, or ::MBG_ERR_NOT_SUPP_BY_DEV,
 *          as returned by ::chk_mrs_instances_flags.
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_no_status( const ALL_XMULTI_REF_INFO *info )
{
  return chk_mrs_instances_flags( info, XMRIF_MSK_NO_STATUS );

}  // chk_dev_xmulti_no_status



/*HDR*/
/**
 * @brief Check if the ::XMRIF_BIT_MRF_NONE_SUPP bit is set.
 *
 * If this feature bit is set, the MRS pseudo-type ::MULTI_REF_NONE is supported.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 *
 * @return  ::MBG_SUCCESS if the bit is set, or ::MBG_ERR_NOT_SUPP_BY_DEV,
 *          as returned by ::chk_mrs_instances_flags.
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_ref_supp_mrf_none( const ALL_XMULTI_REF_INFO *info )
{
  return chk_mrs_instances_flags( info, XMRIF_MSK_MRF_NONE_SUPP );

}  // chk_dev_xmulti_ref_supp_mrf_none



/*HDR*/
/**
 * @brief Check if the ::XMR_EXT_SRC_INFO structure is supported.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 *
 * @return  ::MBG_SUCCESS or ::MBG_ERR_NOT_SUPP_BY_DEV,
 *          as returned by ::chk_mrs_instances_flags.
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_ref_supp_ext_src_info( const ALL_XMULTI_REF_INFO *info )
{
  return chk_mrs_instances_flags( info, XMRIF_MSK_EXT_SRC_INFO_SUPP );

}  // chk_dev_xmulti_ref_supp_ext_src_info



/*HDR*/
/**
 * @brief Check if the ::::XMR_EXT_SRC_INFO structure is supported.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 *
 * @return  ::MBG_SUCCESS or ::MBG_ERR_NOT_SUPP_BY_DEV,
 *          as returned by ::chk_mrs_instances_flags.
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_ref_supp_holdover_status( const ALL_XMULTI_REF_INFO *info )
{
  return chk_mrs_instances_flags( info, XMRIF_MSK_HOLDOVER_STATUS_SUPP );

}  // chk_dev_xmulti_ref_supp_holdover_status



static __mbg_inline /*HDR*/
/**
 * @brief Check if an MRS type code is valid.
 *
 * A valid type must be one of the currently defined ::MULTI_REF_TYPES,
 * except ::MULTI_REF_NONE, which is < 0, or any other value which is
 * less than ::MAX_N_MULTI_REF_TYPES.
 *
 * @param[in]  type  The MRS type code to be checked, see ::MULTI_REF_TYPES.
 *
 * @return  @a true if the MRS type code is valid, else @a false.
 *
 * @see ::MULTI_REF_TYPES
 */
bool is_valid_mrs_type( int type )
{
  return ( type >= 0 ) && ( type < MAX_N_MULTI_REF_TYPES );

}  // is_valid_mrs_type



static __mbg_inline /*HDR*/
/**
 * @brief Check if an extended MRS source feature is supported.
 *
 * Whether a feature is supported, or not, depends on whether a specific
 * bit is set in the @a feat_flags field of the @a info field of the
 * @a ext_src_infos array entry for the specified @a type.
 *
 * The bit masks of the features that can be checked are defined
 * in ::XMR_EXT_SRC_FEAT_FLAG_MSKS.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure
 *                   of which to check the @a ext_src_infos flags.
 *
 * @param[in]  type  A valid code of ::MULTI_REF_TYPES, see ::is_valid_mrs_type.
 *
 * @param[in]  mask  One of the ::XMR_EXT_SRC_FEAT_FLAG_MSKS.
 *
 * @return  ::MBG_SUCCESS if the flag specified in @a mask is set,
 *          ::MBG_ERR_NOT_SUPP_BY_DEV if the bit is not set, or
 *          ::MBG_ERR_INV_PARM if the @a type is out of range.
 *
 * @see ::XMR_EXT_SRC_FEAT_FLAG_MSKS
 */
int chk_mrs_ext_src_feat( const ALL_XMULTI_REF_INFO *info, int type, int mask )
{
  if ( !is_valid_mrs_type( type ) )
    return MBG_ERR_INV_PARM;

  return ( info->ext_src_infos[type].info.feat_flags & mask ) ?
         MBG_SUCCESS : MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_mrs_ext_src_feat



/*HDR*/
/**
 * @brief Check if an XMR source provides ::XMR_STATS.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 * @param[in]  type  One of the ::MULTI_REF_TYPES, except ::MULTI_REF_NONE which is -1.
 *
 * @see ::chk_dev_supp_xmulti_ref_ext_src_info
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_ref_supp_ext_source_stats( const ALL_XMULTI_REF_INFO *info, int type )
{
  return chk_mrs_ext_src_feat( info, type, XMR_EXT_SRC_FEAT_FLAG_MSK_STATS );

}  // chk_dev_xmulti_ref_supp_ext_source_stats



/*HDR*/
/**
 * @brief Check if an XMR source provides ::XMR_METRICS.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 * @param[in]  type  One of the ::MULTI_REF_TYPES, except ::MULTI_REF_NONE which is -1.
 *
 * @see ::chk_dev_xmulti_ref_supp_ext_source_adv_metrics
 * @see ::chk_dev_supp_xmulti_ref_ext_src_info
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_ref_supp_ext_source_metrics( const ALL_XMULTI_REF_INFO *info, int type )
{
  return chk_mrs_ext_src_feat( info, type, XMR_EXT_SRC_FEAT_FLAG_MSK_METRICS );

}  // chk_dev_xmulti_ref_supp_ext_source_metrics



/*HDR*/
/**
 * @brief Check if an XMR source supports advanced XMR QL metrics configuration
 *
 * If this feature is not available, ::XMR_METRICS can not be configured.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 * @param[in]  type  One of the ::MULTI_REF_TYPES, except ::MULTI_REF_NONE which is -1.
 *
 * @see ::XMR_QL_LIMITS
 * @see ::chk_dev_xmulti_ref_supp_ext_source_metrics
 * @see ::chk_dev_supp_xmulti_ref_ext_src_info
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_ref_supp_ext_source_adv_metrics( const ALL_XMULTI_REF_INFO *info, int type )
{
  return chk_mrs_ext_src_feat( info, type, XMR_EXT_SRC_FEAT_FLAG_MSK_ADV_METRICS );

}  // chk_dev_xmulti_ref_supp_ext_source_adv_metrics



/*HDR*/
/**
 * @brief Check if an XMR source XMR source supports coasting mode.
 *
 * If this feature is not available, ::XMR_METRICS can not be configured.
 *
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 * @param[in]  type  One of the ::MULTI_REF_TYPES, except ::MULTI_REF_NONE which is -1.
 *
 * @see ::XMR_QL_LIMITS
 * @see ::chk_dev_supp_xmulti_ref_ext_src_info
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_xmulti_ref_supp_ext_source_coasting( const ALL_XMULTI_REF_INFO *info, int type )
{
  return chk_mrs_ext_src_feat( info, type, XMR_EXT_SRC_FEAT_FLAG_MSK_COASTING );

}  // chk_dev_xmulti_ref_supp_ext_source_coasting



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_ims_has_fdm( const ALL_IMS_INFO *info )
{
  if ( info->state.flags & MBG_IMS_STATE_FLAG_MSK_HAS_FDM )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_ims_has_fdm



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_ims_is_volt_out_enabled( const ALL_IMS_STATE *ims_state, unsigned idx )
{
  const MBG_IMS_SENSOR_STATE *sstate = &ims_state->sensor_state_idx[idx].state;

  if ( ( sstate->type == MBG_IMS_SENSOR_VOLTAGE ) && ( sstate->flags & MBG_IMS_SENSOR_VOLTAGE_OUT_ENB ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_ims_is_volt_out_enabled



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_ims_is_volt_out_overload( const ALL_IMS_STATE *ims_state, unsigned idx )
{
  const MBG_IMS_SENSOR_STATE *sstate = &ims_state->sensor_state_idx[idx].state;

  if ( ( sstate->type == MBG_IMS_SENSOR_VOLTAGE ) && ( sstate->flags & MBG_IMS_SENSOR_VOLTAGE_OUT_OVR ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_ims_is_volt_out_overload



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_ims_is_pll_locked( const ALL_IMS_STATE *ims_state, unsigned idx )
{
  const MBG_IMS_SENSOR_STATE *sstate = &ims_state->sensor_state_idx[idx].state;

  if ( ( sstate->type == MBG_IMS_SENSOR_PLL ) && ( sstate->flags & MBG_IMS_SENSOR_PLL_LOCKED ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_ims_is_pll_locked



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_gpio_supp_ass_idx( const ALL_GPIO_INFO *gpio_info, unsigned idx )
{
  const MBG_GPIO_LIMITS *limits = &gpio_info->infos[idx].info.limits;

  if ( limits->supp_flags & MSK_MBG_GPIO_DEPENDS_ON_ASS_IO_IDX )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_gpio_supp_ass_idx



/*HDR*/
_NO_MBG_API_ATTR int _MBG_API chk_dev_gpio_dep_on_ass_idx( const ALL_GPIO_INFO *gpio_info, unsigned idx )
{
  const MBG_GPIO_SETTINGS *settings = &gpio_info->infos[idx].info.settings;

  if ( settings->flags & MSK_MBG_GPIO_DEPENDS_ON_ASS_IO_IDX )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_gpio_dep_on_ass_idx



/*HDR*/
/**
 * @brief Check whether GPIO provides a status.
 *
 * @param[out]  info  Pointer to a ::ALL_GPIO_INFO structure to be filled up ::FIXME
 * @param[in]  info  Address of an ::ALL_XMULTI_REF_INFO structure to be checked.
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::mbgextio_dev_has_gpio
 * @see ::mbg_chk_dev_has_gpio
 * @see ::free_all_gpio_info
 */
_NO_MBG_API_ATTR int _MBG_API chk_dev_gpio_has_status( const ALL_GPIO_INFO *info )
{
  if ( info->cfg_limits.flags & MBG_GPIO_CFG_LIMIT_FLAG_MASK_STATUS_SUPP )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_dev_gpio_has_status



/*HDR*/
/**
 * @brief Free an ::ALL_XBP_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_XBP_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_xbp_info
 */
void free_all_xbp_info( ALL_XBP_INFO *p )
{
  if ( p )
  {
    if ( p->node_limits )
      free( p->node_limits );

    if ( p->node_infos )
      free( p->node_infos );

    free( p );
  }

}  // free_all_xbp_info



/*HDR*/
/**
 * @brief Free an ::ALL_NET_CFG_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_NET_CFG_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_net_cfg_info
 */
void free_all_net_cfg_info( ALL_NET_CFG_INFO *p )
{
  if ( p )
  {
    if ( p->link_infos )
      free( p->link_infos );

    if ( p->addr_infos )
      free( p->addr_infos );

    if ( p->dns_srvrs )
      free( p->dns_srvrs );

    if ( p->dns_srch_doms )
      free( p->dns_srch_doms );

    if ( p->route_infos )
      free( p->route_infos );

    free( p );
  }

}  // free_all_net_cfg_info



/*HDR*/
/**
 * @brief Free an ::ALL_NET_STATUS_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_NET_STATUS_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_net_status_info
 */
void free_all_net_status_info( ALL_NET_STATUS_INFO *p )
{
  if ( p )
  {
    if ( p->link_infos )
      free( p->link_infos );

    if ( p->addr_infos )
      free( p->addr_infos );

    if ( p->dns_srvrs )
      free( p->dns_srvrs );

    if ( p->dns_srch_doms )
      free( p->dns_srch_doms );

    if ( p->route_infos )
      free( p->route_infos );

    free( p );
  }

}  // free_all_net_status_info



/*HDR*/
/**
 * @brief Free an ::ALL_SNMP_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_SNMP_INFO structure to be freed
 */
void free_all_snmp_info ( ALL_SNMP_INFO *p )
{
  if ( p )
  {
    if ( p->v12_infos )
      free( p->v12_infos );

    if ( p->v12_trap_infos )
      free( p->v12_trap_infos );

    if ( p->v3_infos )
      free( p->v3_infos );

    if ( p->v3_trap_infos )
      free( p->v3_trap_infos );

    free( p );
  }

}  // free_all_snmp_info



/*HDR*/
/**
 * @brief Free an ::ALL_SYSLOG_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_SYSLOG_INFO structure to be freed
 */
void free_all_syslog_info ( ALL_SYSLOG_INFO *p )
{
  if ( p )
  {
    if ( p->servers )
    {
      unsigned i;
      MBG_SYSLOG_SERVER *srv;

      for (i = 0; i < p->glb_info.settings.num_servers; ++i)
      {
        srv = &p->servers[i];

        if ( srv->release_priv )
          srv->release_priv( srv );
      }

      free( p->servers );
    }

    free( p );
  }

}  // free_all_syslog_info



/*HDR*/
/**
 * @brief Free an ::ALL_MONITORING_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_MONITORING_INFO structure to be freed
 */
void free_all_monitoring_info ( ALL_MONITORING_INFO *p )
{
  if ( p )
  {
    if ( p->all_snmp_info )
      free_all_snmp_info( p->all_snmp_info );

    if ( p->all_events )
      free_all_events( p->all_events );

    if ( p->all_syslog_info )
      free_all_syslog_info( p->all_syslog_info );

    free ( p );
  }

}  // free_all_monitoring_info



/*HDR*/
/**
 * @brief Free an ::ALL_EVENTS structure
 *
 * @param[in]  p  Pointer to the ::ALL_EVENTS structure to be freed
 */
void free_all_events( ALL_EVENTS *p )
{
  if ( p )
  {
    if ( p->events )
    {
      unsigned i;
      MBG_EVENT *evt;

      for (i = 0; i < p->num_events; ++i)
      {
        evt = &p->events[i];

        if ( evt->release_priv )
          evt->release_priv( evt );

        if ( evt->release_backref )
          evt->release_backref( evt );
      }

      free( p->events );
    }

    free ( p );
  }

}  // free_all_events



/*HDR*/
/**
 * @brief Free an ::ALL_XMULTI_REF_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_XMULTI_REF_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_xmulti_ref_info
 * @see ::mbg_get_all_xmulti_ref_info
 */
void free_all_xmulti_ref_info( ALL_XMULTI_REF_INFO *p )
{
  if ( p )
  {
    if ( p->infos )
      free( p->infos );

    if ( p->ext_src_infos )
      free( p->ext_src_infos );

    if ( p->ql_settings )
      free( p->ql_settings );

    free ( p );
  }

}  // free_all_xmulti_ref_info



/*HDR*/
/**
 * @brief Free an ::ALL_XMULTI_REF_STATUS structure
 *
 * @param[in]  p  Pointer to the ::ALL_XMULTI_REF_STATUS structure to be freed
 *
 * @see ::mbgextio_get_all_xmulti_ref_status
 * @see ::mbg_get_all_xmulti_ref_status
 */
void free_all_xmulti_ref_status( ALL_XMULTI_REF_STATUS *p )
{
  if ( p )
  {
    if ( p->status )
      free( p->status );

    if ( p->holdover_status )
      free( p->holdover_status );

    if ( p->stats_idx )
      free( p->stats_idx );

    if ( p->metrics_idx )
      free( p->metrics_idx );

    free( p );
  }

}  // free_all_xmulti_ref_status



/*HDR*/
/**
 * @brief Free an ::ALL_PTP_V1_COMMON_DATASETS structure allocated by ::mbgextio_get_all_ptp_v1_common_datasets
 *
 * @param[in]  p  Pointer to the ::ALL_PTP_V1_COMMON_DATASETS structure to be freed
 *
 * @see ::mbgextio_get_all_ptp_v1_common_datasets
 */
void free_all_ptp_v1_common_datasets( ALL_PTP_V1_COMMON_DATASETS *p )
{
  if ( p )
  {
    if ( p->port_datasets )
      free( p->port_datasets );

    free( p );
  }

}  // free_all_ptp_v1_common_datasets



#if !OMIT_PTPV2

/*HDR*/
/**
 * @brief Free an ::ALL_PTP_V2_COMMON_DATASETS structure allocated by ::mbgextio_get_all_ptp_v2_common_datasets
 *
 * @param[in]  p  Pointer to the ::ALL_PTP_V2_COMMON_DATASETS structure to be freed
 *
 * @see ::mbgextio_get_all_ptp_v2_common_datasets
 */
void free_all_ptp_v2_common_datasets( ALL_PTP_V2_COMMON_DATASETS *p )
{
  if ( p )
  {
    if ( p->port_datasets )
      free( p->port_datasets );

    free( p );
  }

}  // free_all_ptp_v2_common_datasets

#endif  // !OMIT_PTPV2



/*HDR*/
/**
 * @brief Free an ::ALL_NTP_CFG_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_NTP_CFG_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_ntp_cfg_info
 */
void free_all_ntp_cfg_info( ALL_NTP_CFG_INFO *p )
{
  if ( p )
  {
    if ( p->symm_key_limits )
      free( p->symm_key_limits );

    if ( p->symm_key_info_idx )
      free( p->symm_key_info_idx );

    if ( p->trusted_key_info_idx )
      free( p->trusted_key_info_idx );

    if ( p->clnt_info )
      free( p->clnt_info );

    if ( p->peer_settings_idx )
      free( p->peer_settings_idx );

    if ( p->srv_info )
      free( p->srv_info );

    if ( p->refclk_info_idx )
      free( p->refclk_info_idx );

    if ( p->misc_limits )
      free( p->misc_limits );

    if ( p->orphan_mode_info )
      free( p->orphan_mode_info );

    free( p );
  }

}  // free_all_ntp_cfg_info



/*HDR*/
/**
 * @brief Free an ::ALL_NTP_STATUS structure
 *
 * @param[in]  p  Pointer to the ::ALL_NTP_STATUS structure to be freed
 *
 * @see ::mbgextio_get_all_ntp_status
 */
void free_all_ntp_status( ALL_NTP_STATUS *p )
{
  if ( p )
  {
    if ( p->refclk_states )
      free( p->refclk_states );

    if ( p->peer_states )
      free( p->peer_states );

    free( p );
  }

}  // free_all_ntp_status



/*HDR*/
/**
 * @brief Free an ::ALL_PTP_NG_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_PTP_NG_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_ptp_ng_info
 */
void free_all_ptp_ng_info( ALL_PTP_NG_INFO *p )
{
  if ( p )
  {
    MBG_PTP_NG_UC_MASTER *uc_master;
    MBG_PTP_NG_TSTAMPER *tstamper;
    MBG_PTP_NG_INSTC *instc;
    unsigned i;

    for (i = 0; i < p->glb_info.num_timestampers; ++i)
    {
      tstamper = &p->timestampers[i];

      if ( tstamper->release_priv )
        tstamper->release_priv( tstamper );
    }

    for (i = 0; i < p->glb_info.settings.num_instances; ++i)
    {
      instc = &p->instances[i];

      if ( instc->uc_slave_list )
        free( instc->uc_slave_list );

      if ( instc->release_priv )
        instc->release_priv( instc );
    }

    for (i = 0; i < p->glb_info.settings.num_uc_masters; ++i)
    {
      uc_master = &p->uc_masters[i];

      if ( uc_master->release_priv )
        uc_master->release_priv( uc_master );
    }

    if ( p->timestampers )
      free( p->timestampers );

    if ( p->instances )
      free( p->instances );

    if ( p->uc_masters )
      free( p->uc_masters );

    free( p );
  }

}  // free_all_ptp_ng_info



/*HDR*/
/**
 * @brief Free a list of ::MBG_SYS_REF_SRC structures
 *
 * @param[in]  p            Pointer to the ::MBG_SYS_REF_SRC list to be freed
 * @param[in]  free_source  Address of a function to be called to free resources.
 *
 * @see ::mbgextio_get_all_sys_ref_info
 * @see ::free_all_sys_ref_info
 */
void free_all_sys_ref_srcs( struct mbg_klist_head *p,
                            void (*free_source)( MBG_SYS_REF_SRC * ) )
{
  struct mbg_klist_head *pos, *next;
  MBG_SYS_REF_SRC *src;

  if ( !p || mbg_klist_is_empty( p ) )
    return;

  mbg_klist_for_each_safe( p, pos, next )
  {
    src = mbg_container_of( pos, MBG_SYS_REF_SRC, head );

    mbg_klist_delete_item( &src->head );
    if ( src->release_priv )
      src->release_priv( src );
    if ( free_source )
      free_source( src );
    else
      free( src );
  }

}  // free_all_sys_ref_srcs



/*HDR*/
/**
 * @brief Free an ::ALL_SYS_REF_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_SYS_REF_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_sys_ref_info
 * @see ::free_all_sys_ref_srcs
 */
void free_all_sys_ref_info( ALL_SYS_REF_INFO *p )
{
  if ( !p )
    return;

  free_all_sys_ref_srcs( &p->sources, p->free_source );
  free( p );

}  // free_all_sys_ref_info



/*HDR*/
/**
 * @brief Free memory allocated by ::mbgextio_get_all_ims_info
 *
 * @param[in]  p  Pointer to the ::ALL_IMS_INFO structure to be freed
 *
 * @see ::mbgextio_dev_has_ims
 * @see ::mbgextio_get_all_ims_info
 * @see ::mbgextio_get_all_ims_state
 */
void free_all_ims_info( ALL_IMS_INFO *p )
{
  if ( p )
  {
    if ( p->fdm_info )
      free( p->fdm_info );

    if ( p->fdm_limits )
      free( p->fdm_limits );

    if ( p->fdm_outinfo_idx )
      free( p->fdm_outinfo_idx );

    free( p );
  }

}  // free_all_ims_info



/*HDR*/
/**
 * @brief Free memory allocated by ::mbgextio_get_all_ims_state
 *
 * @param[in]  p  Pointer to the ::ALL_IMS_STATE structure to be freed
 *
 * @see ::mbgextio_dev_has_ims
 * @see ::mbgextio_get_all_ims_info
 * @see ::mbgextio_get_all_ims_state
 */
void free_all_ims_state( ALL_IMS_STATE *p )
{
  if ( p )
  {
    if ( p->sensor_state_idx )
      free( p->sensor_state_idx );

    if ( p->fdm_state )
      free( p->fdm_state );

    if ( p->fdm_output_state_idx )
      free( p->fdm_output_state_idx );

    free( p );
  }

}  // free_all_ims_state



/*HDR*/
/**
 * @brief Free memory allocated by ::mbgextio_get_all_gpio_info
 *
 * @param[in]  p  Pointer to the ::ALL_GPIO_INFO structure to be freed
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::mbgextio_dev_has_gpio
 * @see ::mbgextio_get_all_gpio_info
 */
void free_all_gpio_info( ALL_GPIO_INFO *p )
{
  if ( p )
  {
    if ( p->infos )
      free( p->infos );

    free( p );
  }

}  // free_all_gpio_info



/*HDR*/
/**
 * @brief Free memory allocated by ::mbgextio_get_all_io_ports
 *
 * @param[in]  p  Pointer to the ::ALL_MBG_IO_PORTS structure to be freed
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::mbgextio_dev_has_io_ports
 * @see ::mbgextio_get_all_io_ports
 * @see ::mbgextio_get_all_io_port_status
 */
void free_all_io_ports( ALL_MBG_IO_PORTS *p )
{
  if ( p )
  {
    if ( p->ports )
    {
      unsigned i;
      MBG_IO_PORT *port;

      for ( i = 0; i < p->num_ports; ++i )
      {
        port = &p->ports[i];

        if ( port->free_priv_data )
          port->free_priv_data( port, port->priv_data );

        if ( port->pt_infos )
            free( port->pt_infos );
      }

      free( p->ports );
    }

    free( p );
  }

}  // free_all_io_ports



/*HDR*/
/**
 * @brief Free memory allocated by ::mbgextio_get_all_gpio_state
 *
 * @param[in]  p  Pointer to the ::ALL_GPIO_STATE structure to be freed
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::mbgextio_dev_has_gpio
 * @see ::mbgextio_get_all_gpio_state
 */
void free_all_gpio_state( ALL_GPIO_STATE *p )
{
  if ( p )
  {
    if ( p->states )
      free( p->states );

    free( p );
  }

}  // free_all_gpio_state



/*HDR*/
/**
 * Allocates memory for a new ::UCAP_ENTRY structure
 *
 * @return The new allocated ::UCAP_ENTRY or NULL if the calloc call was not successful
 */
UCAP_ENTRY *calloc_ucap_entry( void )
{
  UCAP_ENTRY *entry = (UCAP_ENTRY *) calloc( 1, sizeof( *entry ) );

  if ( entry )
    mbg_klist_init( &entry->head );

  return entry;

}  // calloc_ucap_entry



/*HDR*/
/**
 * @brief Free memory allocated by ::mbgextio_get_all_ucap_info
 *
 * @param[in]  p  Pointer to the ::ALL_UCAP_INFO structure to freed
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::mbgextio_dev_has_ucap
 * @see ::mbg_chk_dev_has_ucap
 * @see ::mbgextio_get_all_ucap_info
 * @see ::mbg_get_all_ucap_info
 */
void free_all_ucap_info( ALL_UCAP_INFO *p )
{
  if ( p )
  {
    UCAP_ENTRY *entry;

    while ( !mbg_klist_is_empty( &p->list ) )
    {
      entry = mbg_klist_first_entry( &p->list, UCAP_ENTRY, head );
      mbg_klist_delete_item( &entry->head );
      free( entry );
    }

    free( p );
  }

}  // free_all_ucap_info



/*HDR*/
/**
 * @brief Free an ::ALL_UCAP_NET_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_UCAP_NET_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_ucap_net_info
 */
void free_all_ucap_net_info( ALL_UCAP_NET_INFO *p )
{
  if ( p )
  {
    if ( p->recv_infos )
      free( p->recv_infos );

    free( p );
  }

}  // free_all_ucap_net_info



/*HDR*/
/**
 * @brief Free an ::ALL_USER_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_USER_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_user_info
 */
void free_all_user_info( ALL_USER_INFO *p )
{
  if ( p )
  {
    if ( p->users )
    {
      unsigned i;
      MBG_USER *usr;

      for ( i = 0; i < p->user_mngmnt_info.settings.num_users; ++i )
      {
        usr = &p->users[i];

        if ( usr->release_priv )
          usr->release_priv( usr );

        if ( usr->release_backref )
          usr->release_backref( usr );
      }

      free( p->users );
    }

    free( p );
  }

}  // free_all_user_info



/*HDR*/
/**
 * @brief Free an ::ALL_SERVICE_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_SERVICE_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_user_info
 */
void free_all_svc_info( ALL_SERVICE_INFO *p )
{
  if ( p )
  {
    if ( p->svcs )
    {
      unsigned i;
      MBG_SERVICE *svc;

      for ( i = 0; i < p->mgmt_info.num_services; ++i )
      {
          svc = &p->svcs[i];

        if ( svc->release_priv )
          svc->release_priv( svc );

        if ( svc->release_backref )
          svc->release_backref( svc );
      }

      free( p->svcs );
    }

    free( p );
  }

}  // free_all_svc_info



/*HDR*/
/**
 * @brief Free an ::ALL_FIRMWARE_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_FIRMWARE_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_firmware_info
 */
void free_all_firmware_info( ALL_FIRMWARE_INFO *p )
{
  if ( p )
  {
    if ( p->firmwares )
    {
      MBG_FIRMWARE_UFU *ufu;
      MBG_FIRMWARE *fw;
      unsigned i, j;

      for ( i = 0; i < p->glb_info.installed_fws; ++i )
      {
        fw = &p->firmwares[i];

        for ( j = 0; j < fw->info.num_ufus; ++j )
        {
          ufu = &fw->ufus[j];

          if ( ufu->release_priv )
            ufu->release_priv( ufu );
        }
        free( fw->ufus );

        if ( fw->release_priv )
          fw->release_priv( fw );
      }

      free( p->firmwares );
    }

    free( p );
  }

}  // free_all_firmware_info



/*HDR*/
/**
 * @brief Free an ::ALL_DATABASE_INFO structure
 *
 * @param[in]  p  Pointer to the ::ALL_DATABASE_INFO structure to be freed
 *
 * @see ::mbgextio_get_all_database_info
 */
void free_all_database_info( ALL_DATABASE_INFO *p )
{
  if ( !p )
    return;

  if ( p->dbs )
  {
    uint8_t i;
    MBG_DATABASE *db;

    for ( i = 0; i < p->glb_info.num_dbs; ++i )
    {
        db = &p->dbs[i];

        if ( db->release_priv )
            db->release_priv( db );
    }

    free( p->dbs );
  }

  free( p );

}  // free_all_database_info



/*HDR*/
/**
 * @brief Set up a ::NTP_TSTAMP structure from a hex string with a time in seconds and binary fractions
 *
 * @param[in]   s  A string with a time in seconds since NTP epoch 1900-01-01,
 *                 with binary fractions separated by decimal point,
 *                 e.g. 'dc763e43.73bd5a8f' as printed by the ntpq utility
 * @param[out]  p  Address of a ::NTP_TSTAMP structure to be set up
 *
 * @see ::str_ntp_hex_to_nano_time_64
 */
void str_ntp_hex_to_ntp_tstamp( const char *s, NTP_TSTAMP *p )
{
  char *cp = NULL;

  p->seconds = strtoul( s, &cp, 16 );

  if ( *cp == '.' )  // fractions may follow
  {
    cp++;  // skip '.'

    p->fractions = strtoul( cp, NULL, 16 );
  }
  else
    p->fractions = 0;

}  // str_ntp_hex_to_ntp_tstamp



#if !defined( MBG_TGT_MISSING_64_BIT_TYPES )

/*HDR*/
/**
 * @brief Convert a ::NTP_TSTAMP structure to a ::NANO_TIME_64 structure
 *
 * @param[in]   p_nts   The ::NTP_TSTAMP structure to be converted
 * @param[out]  p_nt64  The ::NANO_TIME_64 structure to be filled up
 */
void ntp_tstamp_to_nanotime_64( const NTP_TSTAMP *p_nts, NANO_TIME_64 *p_nt64 )
{
  p_nt64->secs = p_nts->seconds - NTP_SEC_BIAS;
  p_nt64->nano_secs = bin_frac_32_to_dec_frac( p_nts->fractions, NSEC_PER_SEC );

}  // ntp_tstamp_to_nanotime_64



/*HDR*/
/**
 * @brief Set up a ::NANO_TIME_64 structure from a hex string with a time in seconds and binary fractions
 *
 * @param[in]   s  A string with a time in seconds since epoch 1900-01-01,
 *                 with binary fractions separated by decimal point,
 *                 e.g. 'dc763e43.73bd5a8f' as printed by the ntpq utility
 * @param[out]  p  Address of a ::NANO_TIME_64 structure to be set up
 *
 * @see ::str_ntp_hex_to_ntp_tstamp
 */
void str_ntp_hex_to_nano_time_64( const char *s, NANO_TIME_64 *p )
{
  NTP_TSTAMP nts = { 0 };

  str_ntp_hex_to_ntp_tstamp( s, &nts );
  ntp_tstamp_to_nanotime_64( &nts, p );

}  // str_ntp_hex_to_nano_time_64

#endif  // !defined( MBG_TGT_MISSING_64_BIT_TYPES )



#if defined( MBG_TGT_UNIX )

#define MAX_GRANTOR_PARAM_SIZE 256
#define MAX_PARAM_STR_SIZE     64

/*HDR*/
/**
 * @brief Convert PTP binary config to Oregano config file
 *
 * @param[in] ptp_info          Complete PTP config binary structure
 * @param[in] instc             PTP instance to write config for
 * @param[in] inst_num          PTP instance number on its timestamper
 * @param[in] intf_lnk          Associated interface link settings to determine VLAN ID
 * @param[in] ip_addr           Associated interface link settings to determine VLAN ID
 * @param[in] ascii_file_name   Target file name for the oregano cfg file
 *
 */
_NO_MBG_API_ATTR int _MBG_API mbg_cnv_ptp_cfg_to_oreg_cfg_file( const ALL_PTP_NG_INFO *ptp_info,
                                                                const MBG_PTP_NG_INSTC *instc,
                                                                unsigned inst_num,
                                                                const MBG_NET_INTF_LINK_SETTINGS *intf_lnk,
                                                                const MBG_IP_ADDR *ip_addr,
                                                                const char *ascii_file_name )
{
  char outStr[MAX_PARAM_STR_SIZE];
  int i;

  FILE* fileWrite = fopen(ascii_file_name, "w");

  if ( fileWrite == NULL )
    return mbg_get_last_error( NULL );


  memset(outStr, 0, sizeof(outStr));

  fprintf(fileWrite, "# Default Configuration File for the ptp stack\n");
  fprintf(fileWrite, "# File can be used as a commandline parameter, e.g: \"ptp default.cfg\"\n");
  fprintf(fileWrite, "\n");
  fprintf(fileWrite, "# For your convenience, every configurable parameter is described in this\n");
  fprintf(fileWrite, "# default configuration file. Each parameter is commented out by a hash (#).\n");
  fprintf(fileWrite, "# If you want to use/configure specific parameters, just uncomment them\n");
  fprintf(fileWrite, "# by removing the hash (#). If not described otherwise, the value for a con-\n");
  fprintf(fileWrite, "# figuration parameter represents the default value of this parameter.\n");
  fprintf(fileWrite, "\n");
  fprintf(fileWrite, "# Port Selection:\n");
  fprintf(fileWrite, "# Some syn1588(R)-products do have more than one configurable port.\n");
  fprintf(fileWrite, "# For each configured port, the section name \"port\" with a following port\n");
  fprintf(fileWrite, "# number (starting with 1) and a colon starts a new configuration section.\n");

  fprintf(fileWrite, "    port%u:\n", inst_num);

  fprintf(fileWrite, "\n");
  fprintf(fileWrite, "# profile: (string)\n");
  fprintf(fileWrite, "# The profile parameter has to be the first parameter after declaring\n");
  fprintf(fileWrite, "# a port (e.g. \"port 1:\") in a configuration file. It presets all\n");
  fprintf(fileWrite, "# parameters as defined in the respective profile. Afterwards, other\n");
  fprintf(fileWrite, "# parameters may be defined to overwrite the default values from the\n");
  fprintf(fileWrite, "# profile.\n");
  fprintf(fileWrite, "# Overwriting settings may result in non-standard compliant behaviour.\n");
  fprintf(fileWrite, "# Possible values for the parameter \"profile\" are:\n");
  fprintf(fileWrite, "# default   ... use default profile (IEEE1588-2008 Annex J.3)\n");
  fprintf(fileWrite, "# default_p ... use P2P default profile (IEEE1588-2008 Annex J.4)\n");
  fprintf(fileWrite, "# power     ... use power profile (C37.238 2011)\n");
  fprintf(fileWrite, "# power_s   ... use power profile, slave only (C37.238 2011)\n");
  fprintf(fileWrite, "# power2    ... use power profile (C37.238 2014)\n");
  fprintf(fileWrite, "# power2_s  ... use power profile, slave only (C37.238 2014)\n");
  fprintf(fileWrite, "# smpte     ... use SMPTE profile (ST 2059-2)\n");
  fprintf(fileWrite, "# smpte_s   ... use SMPTE profile, slave only (ST 2059-2)\n");
  fprintf(fileWrite, "# telecom   ... use telecom profile, master only (G.8265.1)\n");
  fprintf(fileWrite, "# telecom2  ... *deprecated* use telecom profile (G.8275.1)\n");
  fprintf(fileWrite, "#               use the following instead\n");
  fprintf(fileWrite, "# g8275_1  ... use telecom profile (G.8275.1), master only\n");
  fprintf(fileWrite, "# telecom2_s... *deprecated* use telecom profile, slave only (G.8275.1)\n");
  fprintf(fileWrite, "#               use the following instead\n");
  fprintf(fileWrite, "# g8275_1_s... use telecom profile (G.8275.1), slave only\n");
  fprintf(fileWrite, "# 802.1as   ... use gPTP profile (IEEE 802.1as)\n");
  fprintf(fileWrite, "# 802.1as_s ... use gPTP profile, slave only (IEEE 802.1as)\n");

  {
    switch(instc->settings->profile)
    {
      case PTP_PRESETS_POWER:
        if ( instc->settings->role == PTP_ROLE_MULTICAST_SLAVE )
          fprintf(fileWrite, "    profile                     power_s\n");
        else
          fprintf(fileWrite, "    profile                     power\n");
        break;
      case PTP_PRESETS_C37238_2017:
        if ( instc->settings->role == PTP_ROLE_MULTICAST_SLAVE )
          fprintf(fileWrite, "    profile                     power2_s\n");
        else
          fprintf(fileWrite, "    profile                     power2\n");
        break;
      case PTP_PRESETS_TELECOM_PHASE:
        if ( instc->settings->role == PTP_ROLE_MULTICAST_SLAVE )
          fprintf(fileWrite, "    profile                     g8275_1_s\n");
        else
          fprintf(fileWrite, "    profile                     g8275_1\n");
        break;
      case PTP_PRESETS_TELECOM_PTS:
        if ( instc->settings->role == PTP_ROLE_UNICAST_SLAVE )
          fprintf(fileWrite, "    profile                     g8275_2_s\n");
        else
          fprintf(fileWrite, "    profile                     g8275_2\n");
        break;
      case PTP_PRESETS_SMPTE:
        if ( instc->settings->role == PTP_ROLE_MULTICAST_SLAVE ||  instc->settings->role == PTP_ROLE_UNICAST_SLAVE)
          fprintf(fileWrite, "    profile                     smpte_s\n");
        else
          fprintf(fileWrite, "    profile                     smpte\n");
        break;
      case PTP_PRESETS_8021AS:
        if ( instc->settings->role == PTP_ROLE_MULTICAST_SLAVE ||  instc->settings->role == PTP_ROLE_UNICAST_SLAVE)
          fprintf(fileWrite, "    profile                     802.1as_s\n");
        else
          fprintf(fileWrite, "    profile                     802.1as\n");
        break;
    }

    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "#### Application Related Parameters  ####\n");
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# filelog: (string)\n");
    fprintf(fileWrite, "# If status, warning and error messages should be logged to a file instead of\n");
    fprintf(fileWrite, "# stdout, specify a log-file-name at the 'filelog'-parameter.\n");
    fprintf(fileWrite, "# File system paths are supported too.\n");

    fprintf(fileWrite, "    filelog                     /var/log/ptpstack_%s.log\n", instc->settings->intf_label);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# loglevel: (0-4) [0]\n");
    fprintf(fileWrite, "# Sets the log level of the PTP Stack. Range from 0 (Errors only) to 4\n");
    fprintf(fileWrite, "# (all Debug output)\n");

    //fprintf(fileWrite, "    loglevel                    2\n");
    fprintf(fileWrite, "    loglevel                    %d\n", instc->settings->log_severity);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# logseverity: (bool) [false]\n");
    fprintf(fileWrite, "# If enabled, a severity string will be prepended to every log message.\n");
    fprintf(fileWrite, "    #logseverity                 false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# logstamp: (bool) [false]\n");
    fprintf(fileWrite, "# If enabled, a timestamp string will be prepended to every log message.\n");
    fprintf(fileWrite, "    logstamp                    true\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# system_info: (bool) [false]\n");
    fprintf(fileWrite, "# Creates a system information report which contains information about\n");
    fprintf(fileWrite, "# the network setup and driver configurations. This file may be used to\n");
    fprintf(fileWrite, "# gather debugging information for contacting the Oregano Systems support.\n");
    fprintf(fileWrite, "    #system_info                 false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "#### Network Related Parameters  ####\n");
    fprintf(fileWrite, "\n");


    fprintf(fileWrite, "#### G.8275.1 Related Parameters  ####\n");
    fprintf(fileWrite, "\n");

    if ( instc->settings->profile == PTP_PRESETS_TELECOM_PHASE )
    {
      fprintf(fileWrite, "# alternative_mc_address: (bool) [false]\n");

      fprintf(fileWrite, "# If layer2 trasnport mechanism is used it's possible to use different\n");
      fprintf(fileWrite, "# multicast addresses to transmit messages.\n");
      fprintf(fileWrite, "# Note: The P2P delay mechanism always uses the non-forwardable multicast\n");
      fprintf(fileWrite, "# address.\n");
      fprintf(fileWrite, "# false: 01:1B:19:00:00:00 (forwardable multicast address)\n");
      fprintf(fileWrite, "# true:  01:80:C2:00:00:0E (non-forwardable multicast address)\n");

      if (instc->settings->profile_settings.g82751.flags & MBG_PTP_NG_82751_FLAG_USE_ALT_MC_ADDRESS_MSK)
        fprintf(fileWrite, "    alternative_mc_address      true\n");
      else
        fprintf(fileWrite, "    alternative_mc_address      false\n");

      fprintf(fileWrite, "\n");

      fprintf(fileWrite, "# port_master_only: (bool) [false]\n");
      fprintf(fileWrite, "# The alternate BMCA of G.8275.1 considers the per-port Boolean attribute\n");
      fprintf(fileWrite, "# masterOnly. If masterOnly is TRUE, the port is never placed in the SLAVE\n");
      fprintf(fileWrite, "# state. If masterOnly is FALSE, the port can be placed in the SLAVE state.\n");
      fprintf(fileWrite, "# set when using the g8275_1 profile specifier to TRUE by default\n");

      if (instc->settings->profile_settings.g82751.flags & MBG_PTP_NG_82751_FLAG_PORT_NOT_SLAVE_MSK)
        fprintf(fileWrite, "    port_master_only            true\n");
      else
        fprintf(fileWrite, "    port_master_only            false\n");

      fprintf(fileWrite, "\n");

      fprintf(fileWrite, "# default_local_priority: (uint8_t) [128]\n");
      fprintf(fileWrite, "# The attribute localPriority is assigned to the local clock, to be used\n");
      fprintf(fileWrite, "# if needed when the data associated with the local clock, D0, is compared\n");
      fprintf(fileWrite, "# with data on another potential grandmaster received via an Announce\n");
      fprintf(fileWrite, "# message. Range is {1 - 255}. The default value is 128.\n");
      fprintf(fileWrite, "#    default_local_priority      %d\n", instc->settings->profile_settings.g82751.default_local_priority);
      fprintf(fileWrite, "\n");

      fprintf(fileWrite, "# port_local_priority: (uint8_t) [128]\n");
      fprintf(fileWrite, "# The per-port attribute localPriority is assigned to each port r of a\n");
      fprintf(fileWrite, "# clock and is used in the determination of Erbest and Ebest. This\n");
      fprintf(fileWrite, "# attribute is used as a tie-breaker in the dataset comparison algorithm,\n");
      fprintf(fileWrite, "# in the event that all other previous attributes of the datasets being\n");
      fprintf(fileWrite, "# compared are equal. Range is {1 - 255}. The default value is 128.\n");
      fprintf(fileWrite, "#    port_local_priority         %d\n", instc->settings->profile_settings.g82751.port_local_priority );
      fprintf(fileWrite, "\n");
    }
    else
    {
      fprintf(fileWrite, "    #alternative_mc_address      false\n");
      fprintf(fileWrite, "\n");
      fprintf(fileWrite, "    #default_local_priority      128\n");
      fprintf(fileWrite, "\n");
      fprintf(fileWrite, "    #port_local_priority         128\n");
      fprintf(fileWrite, "\n");
    }

    fprintf(fileWrite, "# interface:\n");
    fprintf(fileWrite, "# The IP-Address (IPv4 or IPv6; windows and linux) or interface name\n");
    fprintf(fileWrite, "# (IPv4 or Layer2; linux only) of the desired\n");
    fprintf(fileWrite, "# of the Port you want to configure.\n");

    if( ( instc->settings->protocol == PTP_NW_PROT_UDP_IPV6 ) && ( ip_addr->type == MBG_IP_ADDR_TYPE_IP6 ) )
      inet_ntop( AF_INET6, ip_addr->u_addr.ip6_addr.b, outStr, sizeof(outStr) );
    else
    {
      if (intf_lnk->type == MBG_NET_INTF_LINK_TYPE_VLAN )
        strncpy_safe( outStr, (char*)intf_lnk->name, sizeof(outStr) );
      else
        strncpy_safe( outStr, (char*)instc->settings->intf_label, sizeof(outStr) );
    }

    fprintf(fileWrite, "    interface                   %s\n", outStr);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# layer2: (bool) [false]\n");
    fprintf(fileWrite, "# This parameter enables PTP transport using IEEE802.3/Ethernet (layer 2).\n");
    fprintf(fileWrite, "# Layer 2 transport is only available in Linux and requires the interface\n");
    fprintf(fileWrite, "# name syntax (-i parameter).\n");

    if( instc->settings->protocol == PTP_NW_PROT_IEEE_802_3 )
      fprintf(fileWrite, "    layer2                      true\n");
    else
      fprintf(fileWrite, "    layer2                      false\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# dsf: (uint8_t) [0]\n");
    fprintf(fileWrite, "# Allows to set the 8 bit differentiated service field of the IPv4 header (linux only).\n");
    fprintf(fileWrite, "# The first 6 bit (MSB) represent the Differentiated Services Codepoint (DSCP) and the\n");
    fprintf(fileWrite, "# last 2 bit the Explicit Congestion Notification (ECN).\n");
    fprintf(fileWrite, "    dsf                         0x%02X\n", instc->settings->dsf);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# swmode: (bool) [false]\n");
    fprintf(fileWrite, "# This parameter enables/forces pure software timestamping mode for the PTP Stack,\n");
    fprintf(fileWrite, "# even if there would be hardware timestamping support.\n");

    if( instc->settings->flags & MBG_PTP_NG_FLAG_SW_TSTAMPING_MSK )
      fprintf(fileWrite, "    swmode                      true\n");
    else
      fprintf(fileWrite, "    swmode                      false\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# ipv6_multicast_scope: (0x0-0xe) [0]\n");
    fprintf(fileWrite, "# This parameter configures the IPv6 multicast scope to use.\n");
    fprintf(fileWrite, "# The default value 0 means autodetect.\n");
    fprintf(fileWrite, "# Refer to RFC 4291 for further details about IPv6 multicast scope.\n");
    fprintf(fileWrite, "#\n");
    fprintf(fileWrite, "# Autodetection:\n");
    fprintf(fileWrite, "#   if the selected interface is link local, use link local scope multicast\n");
    fprintf(fileWrite, "#   if the selected interface is NOT link local, use global scope multicast\n");
    fprintf(fileWrite, "#\n");

    if ( ( instc->settings->ipv6_multicast_scope > 0 ) && ( instc->settings->ipv6_multicast_scope <= 0xE ) )
      fprintf(fileWrite, "    ipv6_multicast_scope        0x%X\n", instc->settings->ipv6_multicast_scope);
    else
      fprintf(fileWrite, "    ipv6_multicast_scope       0x00\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# multicast_ttl: (1-255) [1]\n");
    fprintf(fileWrite, "# Sets the multicast time to live field in the IPv4 header or the multicast hop\n");
    fprintf(fileWrite, "# count in the IPv6 header respectively.\n");

    fprintf(fileWrite, "    multicast_ttl              %d\n", instc->settings->multicast_ttl);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# unicast_ttl: (1-255) [64]\n");
    fprintf(fileWrite, "# Sets the unicast time to live field in the IPv4 header or the unicast hop\n");
    fprintf(fileWrite, "# count in the IPv6 header respectively.\n");

    fprintf(fileWrite, "    unicast_ttl              %d\n", instc->settings->unicast_ttl);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# vlanid: (disbaled or 0-4095) [disabled]\n");
    fprintf(fileWrite, "# Configures a VLAN ID for the interace. This information is necessary for\n");
    fprintf(fileWrite, "# the timestamper.\n");
    fprintf(fileWrite, "# Note: The network interface must still be configured with the corresponding\n");
    fprintf(fileWrite, "# VLAN ID.\n");

    if (intf_lnk->type == MBG_NET_INTF_LINK_TYPE_VLAN )
      fprintf(fileWrite, "    vlanid                      %d\n", _decode_vlan_id(intf_lnk->vlan_cfg) );
    else
      fprintf(fileWrite, "    vlanid                      disabled\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# card_index: (0-127) [0]\n");
    fprintf(fileWrite, "# This parameter is only considered when the Linux PTP hardware\n");
    fprintf(fileWrite, "# interface (PHC) is used. If the network adapter selected with the\n");
    fprintf(fileWrite, "# -i parameter supports the PHC interface, the syn1588(R) PTP Stack\n");
    fprintf(fileWrite, "# automatically tries to use this feature by opening the related ptp\n");
    fprintf(fileWrite, "# device. If there is more than one ptp device available, it tries to\n");
    fprintf(fileWrite, "# find the the appropriate device using an ethtool ioctl. If this fails\n");
    fprintf(fileWrite, "# (or is not available), this parameter allows to manually select the\n");
    fprintf(fileWrite, "# desired interface (/dev/ptpX).\n");
    fprintf(fileWrite, "    #card_index                 0\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "#### PTP Related Parameters  ####\n");
    fprintf(fileWrite, "\n");


    fprintf(fileWrite, "# accuracy: (uint8_t) [0x27]\n");
    fprintf(fileWrite, "# Sets the clock accuracy field in the default dataset.\n");
    fprintf(fileWrite, "# This value will be used in the BMCA (third decision point)\n");
    fprintf(fileWrite, "# to determine the best master clock. (lower is better)\n");

    if ( instc->settings->fixed_quality.clk_accuracy != 0 )
      fprintf(fileWrite, "    accuracy                    0x%x\n",  instc->settings->fixed_quality.clk_accuracy);
    else
      fprintf(fileWrite, "    accuracy                    0xFE\n");

    fprintf(fileWrite, "\n");

    if( instc->settings->role != PTP_ROLE_UNICAST_SLAVE ) // will be set in Unicast Master config section
    {
      fprintf(fileWrite, "# announce_interval: (-5...5) [0]\n");
      fprintf(fileWrite, "# Sets the default logarithmic announce interval.\n");

      fprintf(fileWrite, "    announce_interval           %d\n", instc->settings->intvs.ann_intv);
      fprintf(fileWrite, "\n");
    }

    fprintf(fileWrite, "# announce_reception: [3]\n");
    fprintf(fileWrite, "# number of announce-messages which might be lost before a new\n");
    fprintf(fileWrite, "# master is selected\n");
    fprintf(fileWrite, "# set by this profile by default to a specified and valid value\n");

    fprintf(fileWrite, "    announce_reception          %d\n", instc->settings->ann_rcpt_timeout);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# boundary_clock: (bool) [false]\n");
    fprintf(fileWrite, "# Enable boundary clock mode for this port.If true, this port will\n");
    fprintf(fileWrite, "# act as part of a boundary clock together with all other ports\n");
    fprintf(fileWrite, "# in this mode. The BMCA will not only consider one port, but all\n");
    fprintf(fileWrite, "# ports to determine the best master in a network as well as the\n");
    fprintf(fileWrite, "# the states of every individual port.\n");

    fprintf(fileWrite, "   # boundary_clock              false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# class: (uint8_t or string) [248]\n");
    fprintf(fileWrite, "# Sets the PTP clock class of the device. This parameter defines\n");
    fprintf(fileWrite, "# the clock's role in a network:\n");
    fprintf(fileWrite, "#    M_EXT....Master on External Reference (6)\n");
    fprintf(fileWrite, "#    M_HOLD...Master on External Reference (in Holdover) (7)\n");
    fprintf(fileWrite, "#    M_NSYNC..Master on External Reference (not sync'd) (52)\n");
    fprintf(fileWrite, "#    M_SLAVE..Master on External Reference (may be Slave) (187)\n");
    fprintf(fileWrite, "#    S...Slave Only (255)\n");
    fprintf(fileWrite, "# This value will be used in the BMCA (second decision point)\n");
    fprintf(fileWrite, "# to determine the best master clock. (lower is better)\n");

    if ( instc->settings->fixed_quality.clk_class_never_sync != 0 )
      fprintf(fileWrite, "    class                       %d\n", instc->settings->fixed_quality.clk_class_never_sync);
    else
      fprintf(fileWrite, "#    class                       248\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# delay_mechanism: (E2E,P2P,None) [E2E]\n");
    fprintf(fileWrite, "# Selects the used delay mechanism:\n");
    fprintf(fileWrite, "#     E2E...End to End delay measurement\n");
    fprintf(fileWrite, "#     P2P...Peer to Peer delay measurement\n");
    fprintf(fileWrite, "#     None..No delay measurement (syntonization)\n");

    switch ( instc->settings->delay_mech )
    {
      case PTP_DELAY_MECH_E2E:
        fprintf(fileWrite, "    delay_mechanism             E2E\n");
        break;
      case PTP_DELAY_MECH_P2P:
        fprintf(fileWrite, "    delay_mechanism             P2P\n");
        break;
      default:
        fprintf(fileWrite, "    delay_mechanism             E2E\n");
        break;
    }

    fprintf(fileWrite, "\n");

    if( instc->settings->role != PTP_ROLE_UNICAST_SLAVE ) // will be set in Unicast Master config section
    {
      fprintf(fileWrite, "# delay_request_offset: (0...5) [4]\n");
      fprintf(fileWrite, "# Sets the default logarithmic delay request offset relativ to\n");
      fprintf(fileWrite, "# the sync interval.\n");

      if (instc->settings->delay_mech == PTP_DELAY_MECH_E2E)
        fprintf(fileWrite, "    delay_request_offset        %d\n", instc->settings->intvs.del_req_intv - instc->settings->intvs.sync_intv);
      else
        fprintf(fileWrite, "    #delay_request_offset        0\n");

      fprintf(fileWrite, "\n");
    }

    fprintf(fileWrite, "# domain: (uint8_t) [0]\n");
    fprintf(fileWrite, "# Sets the PTP domain number.\n");
    fprintf(fileWrite, "    domain                      %d\n", instc->settings->domain_number);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# enable_management: (bool) [true]\n");
    fprintf(fileWrite, "# Enables (true) or disables (false) PTP management support. If disabled,\n");
    fprintf(fileWrite, "# all PTP management messages are ignored.\n");

    if( instc->settings->flags & MBG_PTP_NG_FLAG_MANAGEMENT_MSK )
      fprintf(fileWrite, "    enable_management           true\n");
    else
      fprintf(fileWrite, "    enable_management           false\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# fast_locking_boundary: (int32_t) [65536000]\n");
    fprintf(fileWrite, "# This parameter allows to set a fast locking boundary\n");
    fprintf(fileWrite, "# (offset between master and slave). This boundary is only used\n");
    fprintf(fileWrite, "# as slave and determines if fast locking is used after a master\n");
    fprintf(fileWrite, "# was selected or changed. If the current offset from the master\n");
    fprintf(fileWrite, "# is above the boundary, the fast locking process is started.\n");
    fprintf(fileWrite, "# once the boundary is reached/left.\n");
    fprintf(fileWrite, "# Attention: Fast locking causes the clock to jump by values up\n");
    fprintf(fileWrite, "# to a few microseconds.\n");
    fprintf(fileWrite, "# Specified in scaled nanoseconds (ns << 16), default value\n");
    fprintf(fileWrite, "# is 1000 ns (= 65536000 in scaled nanoseconds). A value of\n");
    fprintf(fileWrite, "# 0 disables fast locking entirely.\n");

    fprintf(fileWrite, "  #  fast_locking_boundary       %d\n",instc->settings->fast_locking_boundary );
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# grantor: (comma-seperated list of IP addresses)\n");
    fprintf(fileWrite, "# Configures the IPv4 or IPv6 address of PTP nodes which will be\n");
    fprintf(fileWrite, "# contacted for unicast connections. The local node will step\n");
    fprintf(fileWrite, "# through the list and try to establich a unicast negotiation with\n");
    fprintf(fileWrite, "# each node.\n");


    if( instc->settings->role == PTP_ROLE_UNICAST_SLAVE )
    {
      size_t len;
      char grantors[MAX_GRANTOR_PARAM_SIZE];
      int param_size = 0;

      memset( grantors, 0, sizeof(grantors) );
      memset( outStr, 0, sizeof(outStr) );

      for ( i = 0; i < ptp_info->glb_info.settings.num_uc_masters; i++ )
      {
        if ( ptp_info->uc_masters[i].instc == instc )
        {
          if( instc->settings->protocol == PTP_NW_PROT_UDP_IPV6 )
            inet_ntop( AF_INET6, ptp_info->uc_masters[i].settings->grantor_addr, outStr, sizeof( outStr ) );
          else if ( instc->settings->protocol == PTP_NW_PROT_UDP_IPV4 )
              snprintf_safe( outStr, sizeof( outStr ), "%i.%i.%i.%i",
                             ptp_info->uc_masters[i].settings->grantor_addr[0],
                             ptp_info->uc_masters[i].settings->grantor_addr[1],
                             ptp_info->uc_masters[i].settings->grantor_addr[2],
                             ptp_info->uc_masters[i].settings->grantor_addr[3]);
          else
            snprintf_safe( outStr, sizeof( outStr ), "%02x:%02x:%02x:%02x:%02x:%02x",
                           ptp_info->uc_masters[i].settings->grantor_addr[0],
                           ptp_info->uc_masters[i].settings->grantor_addr[1],
                           ptp_info->uc_masters[i].settings->grantor_addr[2],
                           ptp_info->uc_masters[i].settings->grantor_addr[3],
                           ptp_info->uc_masters[i].settings->grantor_addr[4],
                           ptp_info->uc_masters[i].settings->grantor_addr[5]);

          param_size = MAX_GRANTOR_PARAM_SIZE - (IP6_ADDR_STR_SIZE + strlen(grantors));

          // Check if maximum size of grantors param is already reached
          if ( param_size > ( IP6_ADDR_STR_SIZE+1 ) )
          {
            strncat( grantors, outStr, param_size );
            strncat( grantors, ",", param_size );
          }

          // only set duration once
          if ( i == 0 )
          {
            fprintf(fileWrite, "# unicast_duration: (uint32_t) [60]\n");
            fprintf(fileWrite, "# Sets the duration of a unicast connection before it is renegotiated. This\n");
            fprintf(fileWrite, "# parameter is only used when requesting a connection from a grantor.\n");
            fprintf(fileWrite, "  unicast_duration %d\n", ptp_info->uc_masters[0].settings->message_duration);
            fprintf(fileWrite, "\n");

            fprintf(fileWrite, "# sync_interval: (-8...8) [0]\n");
            fprintf(fileWrite, "# Sets the default logarithmic sync message interval.\n");

            fprintf(fileWrite, "    sync_interval               %d\n", ptp_info->uc_masters[0].settings->intvs.sync_intv);
            fprintf(fileWrite, "\n");

            if ( ( ptp_info->uc_masters[0].settings->intvs.sync_intv < -3 ) && ( ptp_info->uc_masters[0].settings->intvs.del_req_intv < -3 ))
            {
              fprintf(fileWrite, "lucky_filter_depth 2\n" );
              fprintf(fileWrite, "\n");
              fprintf(fileWrite, "lucky_packet_median %d\n", lucky_packet_median_nvals( ptp_info->uc_masters[0].settings->intvs.sync_intv ) );
              fprintf(fileWrite, "\n");
            }

            fprintf(fileWrite, "# announce_interval: (-5...5) [0]\n");
            fprintf(fileWrite, "# Sets the default logarithmic announce interval.\n");

            fprintf(fileWrite, "    announce_interval           %d\n", ptp_info->uc_masters[0].settings->intvs.ann_intv);
            fprintf(fileWrite, "\n");

            fprintf(fileWrite, "# delay_request_offset: (0...5) [4]\n");
            fprintf(fileWrite, "# Sets the default logarithmic delay request offset relativ to\n");
            fprintf(fileWrite, "# the sync interval.\n");

            fprintf(fileWrite, "    delay_request_offset        %d\n", ptp_info->uc_masters[0].settings->intvs.del_req_intv - ptp_info->uc_masters[0].settings->intvs.sync_intv);

            fprintf(fileWrite, "\n");

          }
        }
      }

      len = strlen(grantors);
      if (len)
          grantors[len - 1] = 0; // remove last ','
      fprintf(fileWrite, "    grantor   %s\n", grantors);
      fprintf(fileWrite, "\n");
    }
    else
    {
      fprintf(fileWrite, "    #grantor\n");
      fprintf(fileWrite, "\n");
    }

    fprintf(fileWrite, "# network_mode: (U,M,B,H) [B]\n");
    fprintf(fileWrite, "# Selects the used network mode:\n");
    fprintf(fileWrite, "#     U...Unicast\n");
    fprintf(fileWrite, "#     M...Multicast\n");
    fprintf(fileWrite, "#     B...Both (Unicast and Multicast\n");
    fprintf(fileWrite, "#     H...Hybrid (Multicast: Sync, FollowUp +\n");
    fprintf(fileWrite, "#                 Unicast: DlyReq, DlyResp, Mgmt)\n");

    switch ( instc->settings->role )
    {
      case PTP_ROLE_UNICAST_SLAVE:
      case PTP_ROLE_UNICAST_MASTER:
        fprintf(fileWrite, "    network_mode                U\n");
        break;
      case PTP_ROLE_BOTH_MASTER:
        fprintf(fileWrite, "    network_mode                B\n");
        break;
      default:
        if ( instc->settings->flags & MBG_PTP_NG_FLAG_HYBRID_MODE_MSK )
          fprintf(fileWrite, "    network_mode                H\n");
        else
          fprintf(fileWrite, "    network_mode                M\n");
    }

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# no_clock_adjustment: (bool) [false]\n");
    fprintf(fileWrite, "# Disable the clock adjustment mechanism. This mode can be used for\n");
    fprintf(fileWrite, "# network analysis.\n");
    fprintf(fileWrite, "    #no_clock_adjustment         false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# path_trace: (bool) [false]\n");
    fprintf(fileWrite, "# Enable or disable the PTP path trace feature.\n");

    if ( instc->settings->flags & MBG_PTP_NG_FLAG_PATH_TRACE_MSK )
      fprintf(fileWrite, "   path_trace                   true\n");
    else
      fprintf(fileWrite, "   path_trace                   false\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# pdelay_request: (0...8) [0]\n");
    fprintf(fileWrite, "# Sets the default logarithmic pdelay request interval.\n");

    if (instc->settings->delay_mech == PTP_DELAY_MECH_P2P)
      fprintf(fileWrite, "    pdelay_request              %d\n", instc->settings->intvs.pdel_req_intv );
    else
      fprintf(fileWrite, "    #pdelay_request              0\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# prio1: (uint8_t) [128]\n");
    fprintf(fileWrite, "# Sets the priority 1 field of the default dataset. This value will\n");
    fprintf(fileWrite, "# be used in the BMCA (first decision point) to determine the best\n");
    fprintf(fileWrite, "# master clock. (lower is better)\n");


    if ((instc->settings->profile == PTP_PRESETS_TELECOM_PTS)
         ||(instc->settings->profile == PTP_PRESETS_TELECOM_PHASE))
        fprintf(fileWrite, "    prio1                       128\n");
    else
        fprintf(fileWrite, "    prio1                       %d\n", instc->settings->priority_1 );
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# prio2: (uint8_t) [128]\n");
    fprintf(fileWrite, "# Sets the priority 2 field of the default dataset. This value will\n");
    fprintf(fileWrite, "# be used in the BMCA (fifth decision point) to determine the best\n");
    fprintf(fileWrite, "# master clock. (lower is better)\n");

    if (((instc->settings->profile == PTP_PRESETS_TELECOM_PTS)
         ||(instc->settings->profile == PTP_PRESETS_TELECOM_PHASE))
         && (instc->settings->role == PTP_ROLE_UNICAST_SLAVE) )
      fprintf(fileWrite, "    prio2                       255\n");
    else
      fprintf(fileWrite, "    prio2                       %d\n", instc->settings->priority_2);
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# ptp_timescale: (bool) [true]\n");
    fprintf(fileWrite, "# Sets the PTP timescale flag in the PTP header of all PTP messages\n");

    if ( instc->settings->flags & MBG_PTP_NG_FLAG_ARB_TIMESCALE_MSK )
      fprintf(fileWrite, "    ptp_timescale               false\n");
    else
      fprintf(fileWrite, "    ptp_timescale               true\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# security: (bool) [false]\n");
    fprintf(fileWrite, "# Enables the security mechanism defined in Annex K of IEEE1588-2008\n");
    fprintf(fileWrite, "# Note: this is an optional feature and is only available on request\n");
    fprintf(fileWrite, "    #security                    false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# statistic (bool) [false]\n");
    fprintf(fileWrite, "# if enabled statistics are printed to the defined logging interface\n");
    fprintf(fileWrite, "    statistic                   true\n");
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "    statistic_period            0\n");
    fprintf(fileWrite, "\n");

    if( instc->settings->role != PTP_ROLE_UNICAST_SLAVE ) // will be set in Unicast Master config section
    {
      fprintf(fileWrite, "# sync_interval: (-8...8) [0]\n");
      fprintf(fileWrite, "# Sets the default logarithmic sync message interval.\n");

      fprintf(fileWrite, "    sync_interval               %d\n", instc->settings->intvs.sync_intv);
      fprintf(fileWrite, "\n");

      if ( instc->settings->intvs.sync_intv < -3 )
      {
          fprintf(fileWrite, "lucky_filter_depth 2\n" );
          fprintf(fileWrite, "\n");
          fprintf(fileWrite, "lucky_packet_median %d\n", lucky_packet_median_nvals( instc->settings->intvs.sync_intv ) );
          fprintf(fileWrite, "\n");
      }
    }

    fprintf(fileWrite, "# time_source: (string) [IXO]\n");
    fprintf(fileWrite, "# Sets the PTP time source:\n");
    fprintf(fileWrite, "#      ATO... connected to an Atomic clock               (0x10)\n");
    fprintf(fileWrite, "#      GPS... synchronized to GPS                        (0x20)\n");
    fprintf(fileWrite, "#      TER... synchronized via radio distribution system (0x30)\n");
    fprintf(fileWrite, "#      PTP... synchronized via PTP                       (0x40)\n");
    fprintf(fileWrite, "#      NTP... synchronized via NTP                       (0x50)\n");
    fprintf(fileWrite, "#      HAN... hand set by means of a human interface     (0x60)\n");
    fprintf(fileWrite, "#      OTH... other source of time                       (0x90)\n");
    fprintf(fileWrite, "#      IXO... internal, free-running oscillator          (0xA0)\n");
    fprintf(fileWrite, "    time_source                 IXO\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# ts_range_window: (0-31) [2]\n");
    fprintf(fileWrite, "# Allows to set the timestamp range window where out of range\n");
    fprintf(fileWrite, "# timestamps will be ignored.\n");
    fprintf(fileWrite, "    #ts_range_window             2\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# two_step: (bool) [true]\n");
    fprintf(fileWrite, "# Enables two step (true -> Sync + Follow Up messages) or one step\n");
    fprintf(fileWrite, "# (false -> only Sync messages) synchronization mechanism in master mode.\n");
    fprintf(fileWrite, "# In slave mode, the mechanism is selected automatically.\n");

    if ( instc->tstamper->settings->flags & PTP_NG_TSTAMPER_FLAG_ONE_STEP_MSK )
      fprintf(fileWrite, "    two_step                    false\n");
    else
      fprintf(fileWrite, "    two_step                    true\n");

    fprintf(fileWrite, "# unicast_max_connections: (1-512) [512]\n");
    fprintf(fileWrite, "# Sets the maximum number of unicast connections. In unicast mode, a PTP\n");
    fprintf(fileWrite, "# master services PTP slaves with announce, sync and/or delay measurement\n");
    fprintf(fileWrite, "# messages. The master needs a dedicated connection for every slave. The\n");
    fprintf(fileWrite, "# connections are negotiated using the unicast negotiation mechanism.\n");
    fprintf(fileWrite, "# If the maximum number of connections is reached, the unicast master\n");
    fprintf(fileWrite, "# denies all further unicast service requests.\n");
    fprintf(fileWrite, "# This parameter limits the maxmimum number of connections. It can be used\n");
    fprintf(fileWrite, "# to adapt the maximum number of connections according to the host's\n");
    fprintf(fileWrite, "# performance.\n");
    fprintf(fileWrite, "    # unicast_max_connections 512\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# unicast_max_messages: (uint32_t) [18432]\n");
    fprintf(fileWrite, "# Sets the number of maximum messages per second. As unicast_max_connections,\n");
    fprintf(fileWrite, "# this parameter may be used to adapt the PTP Stack to the host's performance.\n");
    fprintf(fileWrite, "# If the maximum number of messages is reached, the unicast master\n");
    fprintf(fileWrite, "# outputs a warning for all further unicast service requests.\n");
    fprintf(fileWrite, "# The default value (18432) is calculated as follows:\n");
    fprintf(fileWrite, "# (32 sync/sec + 32 dly resp/sec + 8 ann/sec) * 256 conns\n");
    fprintf(fileWrite, "    # unicast_max_messages 18432\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# utc_offset: (int16_t) [37]\n");
    fprintf(fileWrite, "# Sets the currentUtcOffset. This value is used by\n");
    fprintf(fileWrite, "# the PTP Stack in Master state only. In the Slave state, the PTP Stack\n");
    fprintf(fileWrite, "# uses the grandmaster's UTC offset.\n");

    fprintf(fileWrite, "    utc_offset                  37\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# utc_offset_valid: (bool) [false]\n");
    fprintf(fileWrite, "# Sets the currentUtcOffsetValid flag of the PTP Stack.\n");
    fprintf(fileWrite, "# The flag should be set if the currentUtcOffset is known to be valid.\n");
    fprintf(fileWrite, "# The value is used by the PTP Stack in Master state only. In the Slave\n");
    fprintf(fileWrite, "# state, the PTP Stack applies the grandmaster's flags.\n");
    fprintf(fileWrite, "    utc_offset_valid            false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# variance: (uint16_t) [0xFFFF]\n");
    fprintf(fileWrite, "# Sets the clock variance field in the default dataset.\n");
    fprintf(fileWrite, "# This value will be used in the BMCA (fourth decision point)\n");
    fprintf(fileWrite, "# to determine the best master clock. (lower is better)\n");

    if ( instc->settings->fixed_quality.clk_accuracy != 0 )
      fprintf(fileWrite, "    variance                    0x%04X\n", instc->settings->fixed_quality.fixed_clk_variance);
    else
      fprintf(fileWrite, "    variance                    0xFFFF\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# v1_hardware_compatibility: (bool) [false]\n");
    fprintf(fileWrite, "# If enabled, all event messages will be padded with 0 to match\n");
    fprintf(fileWrite, "# the size of PTPv1 (IEEE1588-2003) event messages.\n");

    if( instc->settings->flags & MBG_PTP_NG_FLAG_V1_HW_COMPATIBILITY_MSK )
      fprintf(fileWrite, "    v1_hw_compatibility         true\n");
    else
      fprintf(fileWrite, "    v1_hw_compatibility         false\n");

    fprintf(fileWrite, "\n");

    if ( instc->tstamper->settings->flags & PTP_NG_TSTAMPER_FLAG_PACKET_GENERATOR_MSK )
      fprintf(fileWrite, "    use_packet_generator         true\n");
    else
      fprintf(fileWrite, "    use_packet_generator         false\n");

    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "#### 802.1AS Related Parameters     ####\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# bridge: (bool) [false]\n");
    fprintf(fileWrite, "# Enables the bridge mode of 802.1as. This requires at least two\n");
    fprintf(fileWrite, "# PTP ports. Synchronization information will be forwarded between\n");
    fprintf(fileWrite, "# the two ports according to the BMCA.\n");
    fprintf(fileWrite, "    #bridge                      false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# neighbor_prop_delay_threshold: (int32_t) [52428800]\n");
    fprintf(fileWrite, "# Sets the maximum peer path delay (propagation delay) measured\n");
    fprintf(fileWrite, "# with the P2P delay mechanism. If this value is exceeded, a \n");
    fprintf(fileWrite, "# neihbor will not be regarded as asCapable and no synchronization\n");
    fprintf(fileWrite, "# information will be exchanged.\n");
    fprintf(fileWrite, "# Specified in scaled nanoseconds (ns << 16), default value\n");
    fprintf(fileWrite, "# is 800 ns (= 52428800 in scaled nanoseconds).\n");
    fprintf(fileWrite, "    #neighbor_prop_delay_threshold 52428800\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# sync_receipt_timeout: (2-8) [3]\n");
    fprintf(fileWrite, "#The value is the number of sync intervals that a slave port\n");
    fprintf(fileWrite, "# waits without receiving Sync/Follow-up messages, before assuming\n");
    fprintf(fileWrite, "# that the master is no longer transmitting synchronization\n");
    fprintf(fileWrite, "# information and that the BMCA needs to be run (if appropriate).\n");
    fprintf(fileWrite, "    #sync_receipt_timeout        3\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# time_base_indicator: (uint16_t) [0]\n");
    fprintf(fileWrite, "# This parameter will identifies the time base of the current\n");
    fprintf(fileWrite, "# grandmaster. It will be used in Sync/Follow-up messages once this\n");
    fprintf(fileWrite, "# clock becomes grandmaster.\n");
    fprintf(fileWrite, "    #time_base_indicator         0\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# transport_802_1as: (bool) [false]\n");
    fprintf(fileWrite, "# Set the 802.1as conformance flag in the transport specific field\n");
    fprintf(fileWrite, "# of the PTP header. Only available in Layer 2 mode.\n");
    fprintf(fileWrite, "    #transport_802_1as           false\n");
    fprintf(fileWrite, "    \n");
    fprintf(fileWrite, "    \n");

    fprintf(fileWrite, "    #gm_inaccuracy 0\n");

    // ### TODO
/*
    fprintf(fileWrite, "    # gm_id %d\n",
        current_bin_cfg->ptp_power_profile_cfg.grandmaster_id
    );
    fprintf(fileWrite, "    nw_inaccuracy %d\n",
        current_bin_cfg->ptp_power_profile_cfg.network_incaccuracy
    );

    fprintf(fileWrite, "\n");
    */

    fprintf(fileWrite, "#### SMPTE Profile Related Parameters  ####\n");
    fprintf(fileWrite, "# The following parameters are only relevant if the SMPTE profile is activated\n");
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# default_system_frame_rate: (comma-seperated list of two uint32_t) [25,1]\n");
    fprintf(fileWrite, "# Default video frame rate of the slave system as a lowest term rational.\n");
    fprintf(fileWrite, "# The data type shall be composed of a pair of unsigned Int32 values coded \n");
    fprintf(fileWrite, "# in big-endian form where the first shall be the numerator and the second \n");
    fprintf(fileWrite, "# shall be the denominator. The denominator shall be the smallest value \n");
    fprintf(fileWrite, "# that represents the frame rate denominator\n");
    fprintf(fileWrite, "# For example, 29.97 Hz: (30000/1001) or 25 Hz: (25/1).\n");

    //### TODO

    /*
    fprintf(fileWrite, "    default_system_frame_rate   %d,%d\n",
      current_bin_cfg->ptp_smpte_profile_cfg.defaultSystemFrameRateNum,
      current_bin_cfg->ptp_smpte_profile_cfg.defaultSystemFrameRateDenum
    );

    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# master_locking_status: (uint8_t) [0]\n");
    fprintf(fileWrite, "# Complementary information to clockClass (0: Not in use, 1: Free Run, \n");
    fprintf(fileWrite, "# 2: Cold Locking, 3: Warm Locking, 4: Locked)\n");

    bufVal = current_bin_cfg->ptp_smpte_profile_cfg.masterLockingStatus;
    if(bufVal < 0) bufVal = 0;
    if(bufVal > 4) bufVal = 4;

    fprintf(fileWrite, "    master_locking_status       %d\n", bufVal);
    fprintf(fileWrite, "  \n");
    fprintf(fileWrite, "# time_address_flags: (uint8_t) [0]\n");
    fprintf(fileWrite, "# Indicates the intended ST 12-1 flags. \n");
    fprintf(fileWrite, "# Bit 0: Drop frame (0: Non-drop-frame, 1: Drop-frame)\n");
    fprintf(fileWrite, "# Bit 1: Color Frame Identification (0: Not in use, 1: In use)\n");
    fprintf(fileWrite, "# Bits 2-7:  Reserved\n");
    fprintf(fileWrite, "    time_address_flags          %d\n",
      current_bin_cfg->ptp_smpte_profile_cfg.timeAddressFlags & 0x3
    );
    fprintf(fileWrite, "    \n");
    fprintf(fileWrite, "# current_local_offset: (int32_t) 0\n");
    fprintf(fileWrite, "# Offset in seconds of Local Time from PTP time. For example, if Local \n");
    fprintf(fileWrite, "# Time is Eastern Standard Time (North America) UTC-5 and the number of \n");
    fprintf(fileWrite, "# leap seconds is 35, the value will be -18035 (decimal). \n");
    fprintf(fileWrite, "    current_local_offset        %d\n",
      current_bin_cfg->ptp_smpte_profile_cfg.currentLocalOffset
    );
    fprintf(fileWrite, "    \n");
    fprintf(fileWrite, "# jump_seconds: (int32_t) [0]\n");
    fprintf(fileWrite, "# The size of the next discontinuity, in seconds, of Local Time. A value \n");
    fprintf(fileWrite, "# of zero indicates that no discontinuity is expected. A positive value \n");
    fprintf(fileWrite, "# indicates that the discontinuity will cause the currentLocalOffset to increase.\n");
    fprintf(fileWrite, "    jump_seconds                %d\n",
      current_bin_cfg->ptp_smpte_profile_cfg.jumpSeconds
    );
    fprintf(fileWrite, "    \n");
    fprintf(fileWrite, "# time_of_next_jump: (uint64_t) [0]\n");
    fprintf(fileWrite, "# The value of the seconds portion of the master PTP  time at the time \n");
    fprintf(fileWrite, "# that the next discontinuity of the currentLocalOffset will occur. The \n");
    fprintf(fileWrite, "# discontinuity occurs at the start of the second indicated\n");
    fprintf(fileWrite, "    time_of_next_jump           %llu\n",
      cnvrt_bytes_to_long(current_bin_cfg->ptp_smpte_profile_cfg.timeOfNextJump, 6)
    );
    fprintf(fileWrite, "    \n");
    fprintf(fileWrite, "# time_of_next_jam: (uint64_t) [0]\n");
    fprintf(fileWrite, "# The value of the seconds portion of the PTP time corresponding to the next \n");
    fprintf(fileWrite, "# scheduled occurrence of the Daily Jam. If no Daily Jam is scheduled, the \n");
    fprintf(fileWrite, "# value of timeOfNextJam shall be zero.\n");
    fprintf(fileWrite, "    time_of_next_jam            %llu\n",
      cnvrt_bytes_to_long(current_bin_cfg->ptp_smpte_profile_cfg.timeOfNextJam, 6)
    );
    fprintf(fileWrite, "    \n");
    fprintf(fileWrite, "# time_of_previous_jam: (uint64_t) [0]\n");
    fprintf(fileWrite, "# The value of the seconds portion of the PTP time corresponding to the \n");
    fprintf(fileWrite, "# previous occurrence of the Daily Jam.\n");
    fprintf(fileWrite, "    time_of_previous_jam        %llu\n",
      cnvrt_bytes_to_long(current_bin_cfg->ptp_smpte_profile_cfg.timeOfPreviousJam, 6)
    );
    fprintf(fileWrite, "    \n");
    fprintf(fileWrite, "# previous_jam_local_offset: (int32_t) [0]\n");
    fprintf(fileWrite, "# The value of currentLocalOffset at the previous daily jam time.\n");
    fprintf(fileWrite, "# If a discontinuity of Local Time occurs at the jam time, this parameter \n");
    fprintf(fileWrite, "# reflects the offset after the discontinuity\n");
    fprintf(fileWrite, "    previous_jam_local_offset   %d\n",
      current_bin_cfg->ptp_smpte_profile_cfg.previousJamLocalOffset
    );
    fprintf(fileWrite, "  \n");
    fprintf(fileWrite, "# daylight_saving: (uint8_t) [0]\n");
    fprintf(fileWrite, "# Bit 0:  Current Daylight Saving (0: Not in effect, 1: In effect)\n");
    fprintf(fileWrite, "# Bit 1: Daylight Saving at next discontinuity (0: Not in effect, 1: In effect)\n");
    fprintf(fileWrite, "# Bit 2: Daylight Saving at previous daily jam time (0: Not in effect, 1: In effect)\n");
    fprintf(fileWrite, "# Bits 3-7: Reserved\n");
    fprintf(fileWrite, "    daylight_saving             %d\n",
      current_bin_cfg->ptp_smpte_profile_cfg.daylightSaving & 0x7
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# leap_second_jump: (uint8_t) [0]\n");
    fprintf(fileWrite, "# The reason for the forthcoming discontinuity of currentLocalOffset indicated by \n");
    fprintf(fileWrite, "# timeOfNextJump\n");
    fprintf(fileWrite, "# Bit 0:\n");
    fprintf(fileWrite, "#   0: Other than a change in the number of leap seconds (default)\n");
    fprintf(fileWrite, "#   1: A change in number of leap seconds\n");
    fprintf(fileWrite, "# Bits 1-7: Reserved\n");
    fprintf(fileWrite, "    leap_second_jump            %d\n",
      current_bin_cfg->ptp_smpte_profile_cfg.leapSecondJump & 0x1
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "#### Servo properties ####\n");
    fprintf(fileWrite, "## please be careful when changing any of the parameters below.\n");
    fprintf(fileWrite, "## any changes may deteroide the synchronization performance.\n");
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# adjust_interval: (-8...8) [0]\n");
    fprintf(fileWrite, "# Sets the default logarithmic clock adjust interval.\n");
    fprintf(fileWrite, "# set by this profile by default to a specified and valid value\n");

    bufVal = current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.adjust_interval;
    if(bufVal < -8) bufVal = -8;
    if(bufVal > 8) bufVal = 8;

    fprintf(fileWrite, "    adjust_interval             %d\n", bufVal);
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# boundary: (int32_t) [65536000]\n");
    fprintf(fileWrite, "# This parameter allows to set a synchronization boundary\n");
    fprintf(fileWrite, "# (offset between master and slave). A message is displayed\n");
    fprintf(fileWrite, "# once the boundary is reached/left.\n");
    fprintf(fileWrite, "# Specified in scaled nanoseconds (ns << 16), default value\n");
    fprintf(fileWrite, "# is 1000 ns (= 65536000 in scaled nanoseconds). A value of\n");
    fprintf(fileWrite, "# 0 disables the boundary entirely.\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# change_epoch_boundary: (int64_t) [32768000000000]\n");
    fprintf(fileWrite, "# If the master-to-slave delay (calculated using the timestamps\n");
    fprintf(fileWrite, "# of a Sync messages)exceeds this boundary \\a tsRange times, the new\n");
    fprintf(fileWrite, "# epoch is applied by setting the slave's time to the master's time.\n");
    fprintf(fileWrite, "# Specified in scaled nanoseconds (ns << 16), default value\n");
    fprintf(fileWrite, "# is 0.5 s (= 32768000000000 in scaled nanoseconds). A value of\n");
    fprintf(fileWrite, "# 0 disables the boundary entirely.\n");

    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# cold_start: (bool) [false]\n");
    fprintf(fileWrite, "# Performs a cold start by initializing and resetting the hardware clock\n");
    fprintf(fileWrite, "# as well as the clock rate.\n");

    fprintf(fileWrite, "    cold_start                  false\n");
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# delay_asymmetry: (int64_t) [0]\n");
    fprintf(fileWrite, "# This paramter can be used to compensate asymmetries\n");
    fprintf(fileWrite, "# in scaled nanoseconds (ns << 16)\n");
    fprintf(fileWrite, "    delay_asymmetry              %lld\n",
        current_bin_cfg->ptp_cfg_x_info.settings.delay_asymmetry
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Servo Properties\n");
    fprintf(fileWrite, "#             +- Outbound Pi.K [16500]\n");
    fprintf(fileWrite, "#             |      +- Outbound Pi.T [100]\n");
    fprintf(fileWrite, "#             |      |      +- iirM2S.Smin [0x8000]\n");
    fprintf(fileWrite, "#             |      |      |      +- iirPath.Smin [0x8000]\n");
    fprintf(fileWrite, "#             |      |      |      |      +- iir.logAdjPrd [4]\n");
    fprintf(fileWrite, "#             |      |      |      |      |      +- iir.logAdjGain [16]\n");

    fprintf(fileWrite, "servo_props %d  %d    0x%04X 0x%04X  %d     %d\n",
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.outbound_pi_K,
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.outbound_pi_T,
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.iirM2S_smin,
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.iirPath_smin,
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.iir_logAdjPrd,
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.iir_logAdjGain
    );

    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Outbound delta rate max (scaledNs) [0x200000000]\n");
    fprintf(fileWrite, "# 1024 ns/s\n");
    fprintf(fileWrite, "outbound_delta_rate_max 0x%llX\n",
      (uint64_t)(current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.outbound_delta_rate_max)
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Outbound anti windup max (scaledNs) [0xEE6B2800000]\n");
    fprintf(fileWrite, "outbound_anti_windup_max 0x%llX\n",
      (uint64_t)(current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.outbound_anti_windup_max)
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Inbound Pi.K [6500]\n");
    fprintf(fileWrite, "inbound_pi_k %d\n",
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.inbound_pi_K
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Inbound Pi.T [100]\n");
    fprintf(fileWrite, "inbound_pi_t %d\n",
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.inbound_pi_T
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Inbound delta rate max (scaledNs) [0x200000000]\n");
    fprintf(fileWrite, "# 8 ns/s\n");
    fprintf(fileWrite, "inbound_delta_rate_max 0x%llX\n",
      (uint64_t)(current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.inbound_delta_rate_max)
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Inbound anti windup max (scaledNs) [0xEE6B2800000]\n");
    fprintf(fileWrite, "inbound_anti_windup_max 0x%llX\n",
      (uint64_t)(current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.inbound_anti_windup_max)
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Lucky Packet Filter Depth (log2 sec) [-1]\n");
    fprintf(fileWrite, "# -1: Lucky packet filter disabled\n");
    fprintf(fileWrite, "lucky_filter_depth %d\n",
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.lucky_packet_flt_depth
    );
    fprintf(fileWrite, "\n");

    fprintf(fileWrite, "# IIR filter enable [true]\n");

    if(current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.flags.use_iir_filter)
      fprintf(fileWrite, "use_iir_filter true\n");
    else
      fprintf(fileWrite, "use_iir_filter false\n");

    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# spike filter enable [true]\n");

    if(current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.flags.use_spike_filter)
      fprintf(fileWrite, "use_spike_filter true\n");
    else
      fprintf(fileWrite, "use_spike_filter false\n");

    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# sample rate converter [false]\n");

    if(current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.flags.use_sample_rate_converter)
      fprintf(fileWrite, "use_sample_rate_converter true\n");
    else
      fprintf(fileWrite, "use_sample_rate_converter false\n");

    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# lucky packet median filter [1]\n");
    fprintf(fileWrite, "# count of packets which are used for median calculation in lucky packet filter\n");
    fprintf(fileWrite, "lucky_packet_median %d\n",
      current_bin_cfg->ptp_cfg_x_info.settings.servo_cfg.clk_servo_cfg.lucky_packet_median
    );
    fprintf(fileWrite, "\n");
    fprintf(fileWrite, "# Second port\n");
    fprintf(fileWrite, "# For boundary clock operation, insert a second configuration here:\n");
    fprintf(fileWrite, "# Note: A second physical interface is required. If you are using one\n");
    fprintf(fileWrite, "# or more syn1588 PCIe cards, they have to be synchronized with the syn1588(R)\n");
    fprintf(fileWrite, "# tool lSync as well.\n");
    fprintf(fileWrite, "    #port2:\n");
    */

    fprintf(fileWrite, "\n");

  }

  fclose(fileWrite);

  return MBG_SUCCESS;

}

#endif // defined( MBG_TGT_UNIX )



/*HDR*/
/**
 * @brief cnvrt_bytes_to_long
 */
uint64_t cnvrt_bytes_to_long( const uint8_t byte_array[], int num_bytes )
{
  int i;
  uint64_t val = 0;

  for ( i = 0; i < num_bytes; i++ )
    val = (val << 8) | byte_array[i];

  return val;

}  // cnvrt_bytes_to_long



/*HDR*/
/**
 * @brief cnvrt_long_to_bytes
 */
void cnvrt_long_to_bytes( uint8_t byte_array[], int num_bytes, uint64_t val )
{
  int i;

  for ( i = num_bytes - 1; i >= 0; i-- )
  {
    byte_array[i] = (uint8_t) val;  // masked by 0xFF anyway
    val >>= 8;
  }

  return;

}  // cnvrt_long_to_bytes



/*HDR*/
int nw_str_mac_addr_to_str( MBG_MAC_ADDR *mac_addr, char *buf, size_t buflen )
{
  int len = 0;

  if ( !mac_addr || !buf )
    return len;

  len += snprintf_safe( &buf[len], buflen - len, "%02X:%02X:%02X:%02X:%02X:%02X",
                        mac_addr->b[0], mac_addr->b[1], mac_addr->b[2], mac_addr->b[3], mac_addr->b[4], mac_addr->b[5] );

  return len;

}  // nw_str_mac_addr_to_str



/*HDR*/
int nw_str_to_mac_addr( MBG_MAC_ADDR *mac_addr, const char *str )
{
  int a, b, c, d, e, f;
  memset( mac_addr, 0, sizeof( *mac_addr ) );

  if ( (sscanf(str, "%02x:%02x:%02x:%02x:%02x:%02x", &a, &b, &c, &d, &e, &f) == 6 ) ||
     ( sscanf(str, "%02x-%02x-%02x-%02x-%02x-%02x", &a, &b, &c, &d, &e, &f) == 6 ) )
  {
    mac_addr->b[0] = a;
    mac_addr->b[1] = b;
    mac_addr->b[2] = c;
    mac_addr->b[3] = d;
    mac_addr->b[4] = e;
    mac_addr->b[5] = f;
    return MBG_SUCCESS;
  }

  return MBG_ERR_GENERIC;

}  // nw_str_to_mac_addr



/*HDR*/
int nw_ip6_addr_bytes_to_str( const uint8_t *addr, uint8_t protocol, char *str, size_t buflen )
{
  switch(protocol)
  {
    case PTP_NW_PROT_UDP_IPV4 :
      snprint_ip4_addr( str, buflen, (IP4_ADDR *) addr, NULL ); break;

    case PTP_NW_PROT_UDP_IPV6 :
      snprint_ip6_addr( str, buflen, (IP6_ADDR *) addr, NULL ); break;

    case PTP_NW_PROT_IEEE_802_3 :
      nw_str_mac_addr_to_str( (MBG_MAC_ADDR*) addr, str, buflen ); break;

    default :
      return MBG_ERR_INV_PARM;
  }

  return MBG_SUCCESS;

}  // nw_ip6_addr_bytes_to_str



/*HDR*/
int nw_str_to_ip6_addr_bytes( uint8_t *addr, const uint8_t protocol, char *str, size_t buflen )
{
  int rc = 0;

  switch ( protocol )
  {
    case PTP_NW_PROT_UDP_IPV4 :
      rc = str_to_ip4_addr( (IP4_ADDR *) addr, str ); break;

    case PTP_NW_PROT_UDP_IPV6 :
      rc = str_to_ip6_addr( (IP6_ADDR *) addr, str ); break;

    case PTP_NW_PROT_IEEE_802_3 :
      rc = nw_str_mac_addr_to_str( (MBG_MAC_ADDR *) addr, str, buflen ); break;

    default :
      return MBG_ERR_INV_PARM;
  }

  return rc;

}  // nw_str_to_ip6_addr_bytes



/*HDR*/
int mbg_os_version_to_str( const MBG_EXT_SYS_INFO *info, char *buf, size_t buflen, int long_version )
{
  ssize_t bytes;
  const char* os_strs[] = MBG_EXT_SYS_INFO_OS_SHORT_STRS;

  if ( !( info->supp_members & MBG_EXT_SYS_INFO_MSK_OS_TYPE ) )
    return MBG_ERR_NOT_SUPP_BY_DEV;

  if ( info->mbg_os_type >= N_MBG_EXT_SYS_INFO_OS_TYPES )
    return MBG_ERR_INV_PARM;

  bytes = 0;

  if ( ( info->supp_members & MBG_EXT_SYS_INFO_MSK_OS_NAME ) && long_version )
    bytes += snprintf_safe(&buf[bytes], buflen - bytes, "%s ", info->os_name);

  if ( info->supp_members & MBG_EXT_SYS_INFO_MSK_SW_REV )
  {
    unsigned year, month, patch;

    _mbg_decode_revision( info->sw_rev, year, month, patch );
    year += MBG_OS_YEAR_CONSTANT;

    if ( ( info->supp_members & MBG_EXT_SYS_INFO_MSK_RELEASE_CANDIDATE ) &&
           info->release_candidate )
    {
      if ( info->release_candidate != MBG_REVISION_RC_DEVEL )
        bytes += snprintf_safe(&buf[bytes], buflen - bytes,
                               "%04u.%02u.%u-rc%i-",
                               year, month, patch, info->release_candidate);
      else
        bytes += snprintf_safe(&buf[bytes], buflen - bytes,
                               "%04u.%02u.%u-%s-",
                               year, month, patch, MBG_REVISION_RC_DEVEL_STR);
    }
    else
    {
      bytes += snprintf_safe(&buf[bytes], buflen - bytes,
                             "%04u.%02u.%u-",
                             year, month, patch);
    }
  }

  bytes += snprintf_safe(&buf[bytes], buflen - bytes,
                         "%s ",
                         os_strs[info->mbg_os_type]);


  if ( ( info->supp_members & MBG_EXT_SYS_INFO_MSK_COMMIT_HASH ) && long_version )
    bytes += snprintf_safe(&buf[bytes], buflen - bytes,
                           "%08x ", info->commit_hash);

  if ( bytes >= (ssize_t)buflen )
    return MBG_ERR_OVERFLOW;

  buf[--bytes] = 0;

  return MBG_SUCCESS;

}  // mbg_os_version_to_str



/*HDR*/
int mbg_fw_can_set_osv(const MBG_FW_GLB_INFO *info)
{
  if ( info && ( info->flags & MBG_FW_GLB_MSK_CAN_SET_OSV ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // mbg_fw_can_set_osv

