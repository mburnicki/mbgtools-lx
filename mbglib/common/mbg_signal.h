
/**************************************************************************
 *
 *  $Id: mbg_signal.h 1.2 2021/04/07 17:49:06 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Provide type-safe prototypes for functions used with signalling.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_signal.h $
 *  Revision 1.2  2021/04/07 17:49:06  martin
 *  Removed trailing spaces.
 *  Revision 1.1  2021/03/25 18:09:15  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_SIGNAL_H
#define _MBG_SIGNAL_H


/* Other headers to be included */

#ifdef _MBG_SIGNAL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Type of a common signal handler function.
 *
 * Can be used to define signal handler functions for
 * @a signal and <em>struct sigaction::sa_handler</em>.
 *
 * @param[in]  sig  A signal number, e.g. @a SIGKILL or @a SIGHUP.
 */
typedef void MBG_SIG_HANDLER_FNC( int sig );



/**
 * @brief Type of a common function to register signal handler(s).
 */
typedef void MBG_REG_SIG_HANDLER_FNC( void );



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

/* (no header definitions found) */

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_SIGNAL_H */
