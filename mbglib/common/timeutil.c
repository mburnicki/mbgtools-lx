
/**************************************************************************
 *
 *  $Id: timeutil.c 1.16 2022/12/21 15:32:49 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Meinberg Library module for safe time conversion routines.
 *
 * -----------------------------------------------------------------------
 *  $Log: timeutil.c $
 *  Revision 1.16  2022/12/21 15:32:49  martin.burnicki
 *  Removed some obsolete _int_from_size_t() stuff.
 *  Revision 1.15  2021/03/22 11:52:30  martin
 *  Updated some comments.
 *  Revision 1.14  2021/03/12 12:32:18  martin
 *  Updated some comments.
 *  Revision 1.13  2019/09/27 15:22:30  martin
 *  Fixed naming of some variables, and updated some comments.
 *  Revision 1.12  2019/09/10 13:13:23  martin
 *  Fixed doxygen comments.
 *  Revision 1.11  2019/07/19 14:20:52  martin
 *  Cleaned up mbg_clock_gettime() and mbg_clock_settime(),
 *  and let them set an appropriate POSIX error code on error.
 *  Revision 1.10  2019/02/08 10:51:44  martin
 *  Removed some definitions that are also in the header file.
 *  Fixed a compiler warning.
 *  Revision 1.9  2018/12/18 11:00:48  martin
 *  Implemented setting time on Windows.
 *  Revision 1.8  2018/12/11 15:02:57Z  martin
 *  Cast to avoid build error on Windows.
 *  Revision 1.7  2018/01/30 09:51:35Z  martin
 *  Let snprint_gmtime_error() return an int instead of size_t.
 *  Updated mbg_clock_gettime():
 *  Revision 1.6  2018/01/15 18:31:08Z  martin
 *  Account for renamed library symbol NSEC_PER_SEC.
 *  Moved function snprint_utc_offs() here.
 *  Changed return type of some printing functions to int.
 *  Revision 1.5  2017/11/16 13:34:09  philipp
 *  Extended ISO printing helper functions
 *  Revision 1.4  2017/11/16 11:33:28  philipp
 *  Added functions to print a time_t to ISO format
 *  Revision 1.3  2017/08/24 13:51:48  martin
 *  Fixed a xcompiler warning on Windows (x64).
 *  Revision 1.2  2017/07/05 07:10:48Z  martin
 *  Added mbg_clock_gettime(), mbg_clock_settime(),
 *  and check_precise_time_api() for Windows.
 *  Revision 1.1  2016/07/15 14:14:19Z  martin
 *  Initial revision
 *
 **************************************************************************/

#define _TIMEUTIL
 #include <timeutil.h>
#undef _TIMEUTIL

#include <mbgtime.h>
#include <str_util.h>

#if defined( MBG_TGT_WIN32 )
  #include <stdio.h>
  #include <errno.h>
#endif



/*HDR*/
int snprint_gmtime_error( char *s, size_t max_len, int mbg_errno, time_t t, const char *calling_fnc )
{
  int n = snprintf_safe( s, max_len, "gmtime() call failed" );

  if ( calling_fnc )
    n += snprintf_safe( &s[n], max_len - n, " in %s", calling_fnc );

  #if defined( MBG_TGT_MISSING_64_BIT_TYPES )
    //### TODO E.g. in VC6 time_t is only 32 bit anyway.
    n += snprintf_safe( &s[n], max_len - n, " for time_t %lu: %s",
                        (unsigned long) t, mbg_strerror( mbg_errno ) );
  #else
    n += snprintf_safe( &s[n], max_len - n, " for time_t %" PRIu64 ": %s",
                        (uint64_t) t, mbg_strerror( mbg_errno ) );
  #endif

  return n;

}  // snprint_gmtime_error



#if defined( MBG_TGT_WIN32 )

/*HDR*/
/**
 * @brief A Windows implementation for POSIX @a clock_gettime.
 *
 * @param[in]   clock_id  Identifier of a specific clock, e.g.
 *                        @a CLOCK_REALTIME, @a CLOCK_MONOTONIC, etc.
 * @param[out]  tp        Address of a <em>struct timespec</em>
 *                        to take up the current time.
 *
 * @return  0 on success, -1 on error, just like the POSIX function.
 *          In case of an error the POSIX errno variable is set
 *          appropriately.
 */
int mbg_clock_gettime( clockid_t clock_id, struct timespec *tp )
{
  switch ( clock_id )
  {
    case CLOCK_REALTIME:
    case CLOCK_MONOTONIC:
    // FIXME TODO CLOCK_MONOTONIC should be implemented separately,
    // e.g. using QPC.
    {
      #if defined( TIME_UTC )  // C11 / VS2015+
        if ( timespec_get( tp, TIME_UTC ) == 0 )
          goto fail;  // error
      #else
        FILETIME ft;
        uint64_t u64;
        gstaft_fnc( &ft );
        // Always succeeds.
        u64 = ( ( (uint64_t) ft.dwHighDateTime ) << 32 ) | ft.dwLowDateTime;
        u64 -= FILETIME_1970;   // Convert to Unix epoch.
        u64 *= 100;             // Convert to nanoseconds.
        // 'time_t' is 64 bits wide on Windows, if using
        // Visual Studio 2005 or newer.
        tp->tv_sec = (time_t) ( u64 / NSEC_PER_SEC );
        tp->tv_nsec = (long) ( u64 % NSEC_PER_SEC );
      #endif
    } break;

    default:
      goto fail;
  }

  // Just like POSIX clock_gettime(), we return 0 on success.
  return 0;

fail:
  // The specified clock_id is not supported.
  // Set the POSIX errno variable appropriately
  // and return -1 just like clock_gettime()
  // does in case of error.
  errno = EINVAL;
  return -1;

}  // mbg_clock_gettime



/*HDR*/
/**
 * @brief A Windows implementation for POSIX @a clock_settime.
 *
 * @param[in]  clock_id  Identifier of a specific clock, i.e.
 *                       @a CLOCK_REALTIME which is the only clock
 *                       supported by this call.
 * @param[in]  tp        Pointer to a struct timespec providing
 *                       the time to be set.
 *
 * @return  0 on success, -1 on error, just like the POSIX function.
 *          In case of an error the POSIX errno variable is set
 *          appropriately.
 */
int mbg_clock_settime( clockid_t clock_id, const struct timespec *tp )
{
  DWORD dw;

  if ( clock_id == CLOCK_REALTIME )
  {
    SYSTEMTIME st;
    union
    {
      FILETIME ft;
      ULONGLONG ull;
    } t;

    t.ull = FILETIME_1970 +
      (ULONGLONG) tp->tv_sec * HNS_PER_SEC +
      (ULONGLONG) tp->tv_nsec / 100;

    // According to the MS API docs the FILETIME to be converted
    // must be less than 0x8000000000000000. Otherwise, the function
    // fails, and apparently sets error code ERROR_INVALID_PARAMETER.
    if ( !FileTimeToSystemTime( &t.ft, &st ) )
      goto fail;

    if ( !SetSystemTime( &st ) )
      goto fail;

    // Just like POSIX clock_settime(), we return 0 on success.
    return 0;
  }

fail:
  // One of the Windows API calls above failed, so the
  // original error information is a Windows error code.
  // Anyway, we try to find an appropriate POSIX error
  // code, set the POSIX errno variable accordingly, and
  // return -1 just like POSIX clock_settime() does
  // in case of error.

  dw = GetLastError();

  switch ( dw )
  {
    case ERROR_PRIVILEGE_NOT_HELD:
      errno = EPERM;
      break;

    case ERROR_INVALID_PARAMETER:
    default:
      errno = EINVAL;
  }

  return -1;

}  // mbg_clock_settime


bool force_legacy_gstaft;


/*HDR*/
void check_precise_time_api( void )
{
  const char *info ="";
  GSTAFT_FNC tmp_fnc;
  HINSTANCE h = LoadLibrary( "kernel32.dll" );

  if ( h == NULL )   // TODO Error msg.
  {
    info = "Precise system time may not be supported; failed to get handle for kernel32.dll.";
    goto out;
  }

  tmp_fnc = (GSTAFT_FNC) GetProcAddress( h, "GetSystemTimePreciseAsFileTime" );

  if ( tmp_fnc == NULL )
  {
    info = "Precise system time NOT supported";
    goto out;
  }

  if ( force_legacy_gstaft )
  {
    info = "Precise system time is supported, but legacy function used by request";
    goto out;
  }

  gstaft_fnc = tmp_fnc;
  info = "Precise system time is supported and used";

out:
  printf( "%s\n", info );

}  // check_precise_time_api

#endif



/*HDR*/
/**
 * @brief Print a %UTC offset into a string.
 *
 * Format of the string is "[info]+hh[:mm[:ss]]h"
 *
 * @param[out]  s         Address of a string buffer to be filled.
 * @param[in]   max_len   Size of the string buffer.
 * @param[in]   info      An optional info string to be prepended, may be @a NULL.
 * @param[in]   utc_offs  The %UTC offset to be printed, in [s].
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
int snprint_utc_offs( char *s, size_t max_len, const char *info, long utc_offs )
{
  int n = 0;

  // utc_offs is in [s]
  char utc_offs_sign = (char) ( ( utc_offs < 0 ) ? '-' : '+' );
  ulong abs_utc_offs = labs( utc_offs );
  ulong utc_offs_hours = abs_utc_offs / SECS_PER_HOUR;
  ulong tmp = abs_utc_offs % SECS_PER_HOUR;
  ulong utc_offs_mins = tmp / MINS_PER_HOUR;
  ulong utc_offs_secs = tmp % MINS_PER_HOUR;

  if ( info )
    n += snprintf_safe( &s[n], max_len - n, "%s", info );

  n += snprintf_safe( &s[n], max_len - n, "%c%lu", utc_offs_sign, utc_offs_hours );

  if ( utc_offs_mins || utc_offs_secs )
    n += snprintf_safe( &s[n], max_len - n, ":%02lu", utc_offs_mins );

  if ( utc_offs_secs )
    n += snprintf_safe( &s[n], max_len - n, ":%02lu", utc_offs_secs );

  n += sn_cpy_str_safe( &s[n], max_len - n, "h" );

  return n;

}  // snprint_utc_offs



/*HDR*/
int snprint_time_t_to_iso( time_t tstamp, int offs_hours, char *buf, size_t len )
{
  struct tm tm = { 0 };
  int n;
  int rc;

  if ( offs_hours )
    tstamp += offs_hours * SECS_PER_HOUR;

  rc = mbg_gmtime( &tm, &tstamp );

  if ( mbg_rc_is_error( rc ) )
  {
    // TODO Error: conversion failed.
  }

  n = snprintf_safe( buf, len, "%04d-%02d-%02d, %02d:%02d:%02d",
                     tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday,
                     tm.tm_hour, tm.tm_min, tm.tm_sec );

  return n;

}  // snprint_time_t_to_iso



static __mbg_inline /*HDR*/
int __to_iso_frac( time_t tstamp, int offset, time_t frac,
                   const char* fmt, char *buf, size_t max_len )
{
  int n = snprint_time_t_to_iso( tstamp, offset, buf, max_len );
  n += snprintf_safe( &buf[n], max_len - n, fmt, frac );

  return n;

}  // __to_iso_frac



/*HDR*/
int snprint_time_t_to_iso_ms( time_t tstamp, int offs_hours, time_t frac,
                              char *buf, size_t len )
{
  tstamp += frac / MSEC_PER_SEC;
  frac %= MSEC_PER_SEC;

  return __to_iso_frac( tstamp, offs_hours, frac, ".%03lu", buf, len );

}  // snprint_time_t_to_iso_ms



/*HDR*/
int snprint_time_t_to_iso_us( time_t tstamp, int offs_hours, time_t frac,
                                char *buf, size_t len )
{
  tstamp += frac / USEC_PER_SEC;
  frac %= USEC_PER_SEC;

  return __to_iso_frac( tstamp, offs_hours, frac, ".%06lu", buf, len );

}  // snprint_time_t_to_iso_us



/*HDR*/
int snprint_time_t_to_iso_ns( time_t tstamp, int offs_hours, time_t frac,
                              char *buf, size_t len )
{
  tstamp += frac / NSEC_PER_SEC;
  frac %= NSEC_PER_SEC;

  return __to_iso_frac( tstamp, offs_hours, frac, ".%09lu", buf, len );

}  // snprint_time_t_to_iso_ns



/*HDR*/
int snprint_ntp_tstamp_to_iso_ms( const NTP_TSTAMP *ts, char *buf, size_t len )
{
  time_t secs = cvt_to_time_t( ts->seconds ) - NTP_SEC_BIAS;
  time_t fracs = bin_frac_32_to_msec( ts->fractions );

  return snprint_time_t_to_iso_ms( secs, 0, fracs, buf, len );

}  // snprint_ntp_tstamp_to_iso_ms



/*HDR*/
int snprint_ntp_tstamp_to_iso_us( const NTP_TSTAMP *ts, char *buf, size_t len )
{
  time_t secs = cvt_to_time_t( ts->seconds ) - NTP_SEC_BIAS;
  time_t fracs = bin_frac_32_to_usec( ts->fractions );

  return snprint_time_t_to_iso_us( secs, 0, fracs, buf, len );

}  // snprint_ntp_tstamp_to_iso_us



/*HDR*/
int snprint_ntp_tstamp_to_iso_ns( const NTP_TSTAMP *ts, char *buf, size_t len )
{
  time_t secs = cvt_to_time_t( ts->seconds ) - NTP_SEC_BIAS;
  time_t fracs = bin_frac_32_to_nsec( ts->fractions );

  return snprint_time_t_to_iso_ns( secs, 0, fracs, buf, len );

}  // snprint_ntp_tstamp_to_iso_ns

