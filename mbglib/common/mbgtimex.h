
/**************************************************************************
 *
 *  $Id: mbgtimex.h 1.3 2020/02/28 13:13:49 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for mbgtimex.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgtimex.h $
 *  Revision 1.3  2020/02/28 13:13:49  martin
 *  Updated function prototypes.
 *  Revision 1.2  2019/09/27 15:09:51  martin
 *  Cleanup.
 *  Updated function names and prototypes.
 *  Revision 1.1  2019/08/08 13:12:14  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBGTIMEX_H
#define _MBGTIMEX_H


/* Other headers to be included */

#include <mbgtime.h>
#include <timeutil.h>

#include <time.h>


#ifdef _MBGTIMEX
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif



/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif

#if !defined( _T_DEPRECATED_BY )
  #if 0
   #define _T_DEPRECATED_BY( _x )   _DEPRECATED_BY( _x )
  #else
    #define _T_DEPRECATED_BY( _x )
  #endif
#endif



/**
 * @defgroup mbgtimex_fncs Meinberg extended time conversion functions
 */

/**
 * @defgroup mbgtimex_gps_time_fncs Meinberg GPS time functions
 * @ingroup mbgtimex_fncs
 *
 * Functions that deal with Meinberg ::T_GPS, ::TM_GPS, week numbers, etc.
 *
 * @see @ref mbgtimex_time_fncs
 * @see @ref mbgtimex_tzdl_fncs
 * @see @ref mbgtimex_gps_posix_time_cnv_fncs
 * @see @ref mbgtimex_fncs
 * @see @ref group_true_gps_wn_fncs
 */

/**
 * @defgroup mbgtimex_time_fncs Meinberg functions that deal with POSIX-like time
 * @ingroup mbgtimex_fncs
 *
 * Convert 'time_t'-like timestamps between %UTC, TAI, and local time,
 * based on rules stored in Meinberg data structures.
 *
 * @see @ref mbgtimex_gps_time_fncs
 * @see @ref mbgtimex_tzdl_fncs
 * @see @ref mbgtimex_gps_posix_time_cnv_fncs
 * @see @ref mbgtimex_fncs
 */

/**
 * @defgroup mbgtimex_tzdl_fncs Meinberg functions that evaluate Meinberg time zone and daylight saving data
 * @ingroup mbgtimex_fncs
 *
 * Functions to deal with Meinberg ::T_GPS, ::TM_GPS, week numbers, etc.
 *
 * @see The ::TZDL structure.
 * @see @ref mbgtimex_gps_time_fncs
 * @see @ref mbgtimex_time_fncs
 * @see @ref mbgtimex_gps_posix_time_cnv_fncs
 * @see @ref mbgtimex_fncs
 */

/**
 * @defgroup mbgtimex_gps_posix_time_cnv_fncs Meinberg GPS time to POSIX time conversion functions
 * @ingroup mbgtimex_fncs
 *
 * Functions to convert from Meinberg ::T_GPS, ::TM_GPS, etc.,
 * to POSIX time_t, struct tm, or the Meinberg-specific POSIX-like
 * ::MBG_TIME64_T type.
 *
 * @see @ref mbgtimex_gps_time_fncs
 * @see @ref mbgtimex_time_fncs
 * @see @ref mbgtimex_tzdl_fncs
 * @see @ref mbgtimex_fncs
 */



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Convert date and time from <em>struct tm</em> to GPS week number and second-of-week.
 *
 * Only the data format is converted, the offset between GPS time scale
 * and %UTC scale is not taken into account.
 *
 * The calculated second-of-week is always greater than 0, but the week number
 * can be less than 0, if the original time is before the GPS time epoch.
 *
 * @param[out]  p_wn    Address of a ::GPS_WNUM variable to take the computed week number.
 *
 * @param[out]  p_wsec  Address of a ::GPS_WSEC variable to take the computed second-of-week.
 *
 * @param[in]   p_tm    Pointer to a <em>struct tm</em> providing the date and time to be converted.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 */
 int mbg_struct_tm_to_gps_wn_wsec( GPS_WNUM *p_wn, GPS_WSEC *p_wsec, const struct tm *p_tm ) ;

 /**
 * @brief Convert GPS week number plus second-of-week to ::MBG_TIME64_T.
 *
 * Only the data format is converted, the offset between GPS time scale
 * and %UTC scale is not taken into account.
 *
 * @param[out] p_t64  Address of an ::MBG_TIME64_T to take the computed timestamp.
 *
 * @param[in]  wn     A GPS week number as ::GPS_WNUM.
 *
 * @param[in]  wsec   Seconds of the week as ::GPS_WSEC.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 */
 int mbg_gps_wn_wsec_to_time64_t( MBG_TIME64_T *p_t64, GPS_WNUM wn, GPS_WSEC wsec ) ;

 /**
 * @brief Convert a GPS week number / day-of-week pair to ::MBG_TIME64_T.
 *
 * Only the data format is converted, the offset between GPS time scale
 * and %UTC scale is not taken into account.
 *
 * @note If this function is called to compute the leap second date
 * from the GPS ::UTC parameters, the computed timestamp is associated
 * with <b>the end of the leap second transition</b>, e.g.
 * <em>2017-01-01 00:00:00</em> rather than <em>2016-12-31 23:59:59</em>.
 * Also, first calling the function ::mbg_find_true_gps_wn_lsf may be
 * required to resolve an ambiguity of the week number.
 * See @ref group_true_gps_wn_fncs.
 *
 * @param[out] p_t64  Address of an ::MBG_TIME64_T to take the computed timestamp.
 *
 * @param[in]  wn     A GPS week number as ::GPS_WNUM, e.g. ::UTC::WNlsf.
 *
 * @param[in]  dn     A day number as ::GPS_DNUM, e.g. ::UTC::DNt.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 * @see ::mbg_find_true_gps_wn_lsf
 * @see @ref group_true_gps_wn_fncs
 */
 int mbg_gps_wn_dn_to_time64_t( MBG_TIME64_T *p_t64, GPS_WNUM wn, GPS_DNUM dn ) ;

 /**
 * @brief Convert an ::MBG_TIME64_T to GPS week number and second-of-week.
 *
 * Only the data format is converted, the offset between GPS time scale
 * and %UTC scale is not taken into account.
 *
 * The calculated week number can be negative if @a *p_t64 is before
 * the GPS epoch. The calculated values are normalized so that the
 * second-of-week is always non-negative, i.e. contains the seconds
 * after the beginning of the week, even if the week number is negative.
 *
 * @param[out]  p_wn    Address of a ::GPS_WNUM variable to take the computed week number.
 *
 * @param[out]  p_wsec  Address of a ::GPS_WSEC variable to take the computed second-of-week.
 *
 * @param[in]   p_t64   Pointer to an ::MBG_TIME64_T timestamp to be converted.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 */
 int mbg_time64_t_to_gps_wn_wsec( GPS_WNUM *p_wn, GPS_WSEC *p_wsec, const MBG_TIME64_T *p_t64 ) ;

 /**
 * @brief Convert an ::MBG_TIME64_T to ::T_GPS.
 *
 * Only the data format is converted, the offset between GPS time scale
 * and %UTC scale is not taken into account.
 *
 * The week number of the calculated ::T_GPS can be negative if @a *p_t64
 * is before the GPS epoch. The calculated values are normalized so that
 * the second-of-week is always non-negative, i.e. contains the seconds
 * after the beginning of the week, even if the week number is negative.
 *
 * @param[out]  p_t_gps  Address of a ::T_GPS variable to take the computed timestamp.
 *
 * @param[in]   p_t64    Pointer to an ::MBG_TIME64_T timestamp.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 * @see ::mbg_t_gps_to_time_t
 */
 int mbg_time64_t_to_t_gps( T_GPS *p_t_gps, MBG_TIME64_T *p_t64 ) ;

 /**
 * @brief Convert ::T_GPS to ::MBG_TIME64_T.
 *
 * Only the data format is converted, the offset between GPS time scale
 * and %UTC scale is not taken into account.
 *
 * @param[out] p_t64  Address of an ::MBG_TIME64_T type to take the computed timestamp.
 *
 * @param[in]  p_t    Pointer to the timestamp to be converted, in ::T_GPS format.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 * @see ::mbg_time_t_to_t_gps
 */
 int mbg_t_gps_to_time64_t( MBG_TIME64_T *p_t64, const T_GPS *p_t ) ;

 /**
 * @brief Check is a <em>struct tm</em> and a ::TM_GPS refer to the same date and time.
 *
 * This has to take into account that some fields in a <em>struct tm</em>
 * have different meanings than the associated fiellds in a ::TM_GPS.
 *
 * @param[in]  p_tm      Pointer to the <em>struct tm</em> to be compared.
 * @param[in]  p_tm_gps  Pointer to the ::TM_GPS to be compared.
 *
 * @return  @a true, if both variables refer to the same date and time, else @a false.
 */
 bool is_same_tm_tm_gps( const struct tm *p_tm, const TM_GPS *p_tm_gps ) ;

 /**
 * @brief Determine the true week number for an ambiguous ::UTC::WNlsf number.
 *
 * See @ref group_true_gps_wn_fncs for details how this is supposed to work.
 *
 * This variant only checks calculated potential dates.
 * See ::mbg_find_true_gps_wn_lsf for a variant which searches a table
 * of known leaps second dates first, and thus executes faster.
 *
 * @param[in,out]  p_wn  The GPS week number of the leap second, see ::UTC::WNlsf.
 *                       Updated if a solution could be found. If @a srch_all
 *                       is @a true, the last update is done for the last match
 *                       found.
 *
 * @param[in]      dn_t  The day-of-week number at the end of which the
 *                       leap second is to be inserted. See ::UTC::DNt,
 *                       which is usually in the range 1..7.
 *
 * @param[in]  srch_all  If this flag is @a true then always the full range
 *                       is searched. The search is even continued after
 *                       a first match has already been found. This takes
 *                       longer to execute, but allows detection
 *                       of multiple matches, e.g. for testing.
 *
 * @param[in]  first_wn  First GPS week number to start the search.
 *                       Can be 0 to search the full range, or the latest
 *                       known week number from a leap second table to check
 *                       only dates that are after the last week number
 *                       present in the table.
 *
 * @return  The number of matches found. Should be 1 for an unambiguous result,
 *          even if @a srch_all was @a true
 *
 * @ingroup group_true_gps_wn_fncs
 * @see ::mbg_find_true_gps_wn_lsf
 * @see @ref group_true_gps_wn_fncs
 * @see ::mbg_gps_wn_dn_to_time_t
 * @see ::UTC
 */
 int mbg_find_true_gps_wn_lsf_ex( GPS_WNUM *p_wn, GPS_DNUM dn_t, bool srch_all, GPS_WNUM first_wn ) ;

 /**
 * @brief Determine the true week number for an ambiguous ::UTC::WNlsf number.
 *
 * See @ref group_true_gps_wn_fncs for details how this is supposed to work.
 *
 * To reduce the execution time, this variant uses a table to search past,
 * known leap second dates, and calls ::mbg_find_true_gps_wn_lsf_ex only
 * if no result has been found in the table, or the @a srch_all flag is @a true.
 *
 * @param[in,out]  p_wn  The GPS week number of the leap second, see ::UTC::WNlsf.
 *                       Updated if a solution could be found. If @a srch_all
 *                       is @a true, the last update is done for the last match
 *                       found.
 *
 * @param[in]      dn_t  The day-of-week number at the end of which the
 *                       leap second is to be inserted. See ::UTC::DNt,
 *                       which is usually in the range 1..7.
 *
 * @param[in]  srch_all  If this flag is @a true then always the full range
 *                       is searched. The search is even continued after
 *                       a first match has already been found. This takes
 *                       longer to execute, but allows detection
 *                       of multiple matches, e.g. for testing.
 *
 * @return  The number of matches found. Should be 1 for an unambiguous result,
 *          even if @a srch_all was @a true
 *
 * @ingroup group_true_gps_wn_fncs
 * @see ::mbg_find_true_gps_wn_lsf
 * @see @ref group_true_gps_wn_fncs
 * @see ::mbg_gps_wn_dn_to_time_t
 * @see ::UTC
 */
 int mbg_find_true_gps_wn_lsf( GPS_WNUM *p_wn, GPS_DNUM dn_t, int srch_all ) ;

 /**
 * @brief Set up an ::MBG_TZ_INFO structure for a given year.
 *
 * This function should be called whenever @a p_tzdl has been updated,
 * or the current @a year has changed.
 *
 * @param[out]  p_tzi   Address of an ::MBG_TZ_INFO variable be set up.
 *
 * @param[in]   p_tzdl  Pointer to a ::TZDL structure providing
 *                      local time offset and DST rules.
 *
 * @param[in]   p_t64_std  The standard time (%UTC + ::TZDL::offs) for which
 *                         to calculate the relevant switching times.
 *
 * @param[in]   year    The calendar year for which to calculate the
 *                      switching times.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_tzdl_fncs
 * @see ::mbg_set_tz_info_for_utc_time64_t
 * @see ::set_tzi_time
 */
 int mbg_set_tz_info_for_year( MBG_TZ_INFO *p_tzi, const TZDL *p_tzdl, const MBG_TIME64_T *p_t64_std, int year ) ;

 /**
 * @brief Set up an ::MBG_TZ_INFO structure for a given %UTC time.
 *
 * This function should be called whenever @a p_tzdl has been updated,
 * or the current time @a t_utc has increased into a different year.
 *
 * @param[out]  p_tzi      Address of an ::MBG_TZ_INFO variable be set up.
 *
 * @param[in]   p_tzdl     Pointer to a ::TZDL structure providing
 *                         local time offset and DST rules.
 *
 * @param[in]   p_t64_utc  Pointer to the %UTC time for which to calculate
 *                         the relevant switching times.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_tzdl_fncs
 * @see ::mbg_set_tz_info_for_year
 * @see ::set_tzi_time
 */
 int mbg_set_tz_info_for_utc_time64_t( MBG_TZ_INFO *p_tzi, const TZDL *p_tzdl, const MBG_TIME64_T *p_t64_utc ) ;

 /**
 * @brief Convert an ::MBG_TZ_INFO to TAI.
 *
 * By default, an ::MBG_TZ_INFO structure stores the
 * switching times for start and end of DST as local
 * standard time. This function can be used to set up
 * another structure where the switching times are TAI,
 * which allows for faster evaluation in some cases.
 *
 * The local time zone offset values are left untouched.
 * The field @a offs_dl anyway only depends on the
 * selected time zone, and the standard time offset
 * from TAI may change in the middle of a DST or
 * non-DST interval, whenever a leap second event
 * occurs, so the exact %UTC/TAI offset needs to be
 * determined whenever a local time is to be
 * derived from TAI.
 *
 * This function should be called whenever @a p_tzi
 * or @a p_lsi have been updated.
 *
 * @param[out]  p_tzi_tai  Address of an ::MBG_TZ_INFO variable to be set up.
 *
 * @param[in]   p_tzi      Pointer to an ::MBG_TZ_INFO variable with the standard settings,
 *                         where switching time are local standard times.
 *
 * @param[in]   p_lsi      Pointer to an ::MBG_LS_INFO variable with current %UTC/leap second information.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_time_fncs
 */
 int mbg_tz_info_to_tai( MBG_TZ_INFO *p_tzi_tai, const MBG_TZ_INFO *p_tzi, const MBG_LS_INFO *p_lsi ) ;

 /**
 * @brief Set up an ::MBG_LS_INFO structure from a given GPS ::UTC structure.
 *
 * This function should be called whenever @a p_utc has been updated.
 *
 * @param[out]  p_lsi  Address of an ::MBG_LS_INFO variable be set up.
 *
 * @param[in]   p_utc  Pointer to ::UTC structure providing a valid GPS/%UTC
 *                     time offset and leap second information in GPS format.
 *
 * @param[out]  p_gps_wn_ls  Optional address of a ::GPS_WNUM variable that can take
 *                           the true GPS week number of the leap second as determined
 *                           during the conversion, can be NULL.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_time_fncs
 */
 int mbg_set_ls_info_from_gps_utc( MBG_LS_INFO *p_lsi, const UTC *p_utc, GPS_WNUM *p_gps_wn_ls ) ;

 /**
 * @brief Determine DST status for a given local standard time in ::MBG_TIME64_T format.
 *
 * Determine the DST and DST announcement status for a local standard time
 * in ::MBG_TIME64_T format (i.e. %UTC plus standard time zone offset already applied).
 * DST is in effect after @a t_on and before @a t_off.
 *
 * In the Northern hemisphere @a t_on is usually before @a t_off, so DST is <b>off</b>
 * at the beginning of the year until @a t_on, then it is <b>on</b> for the interval between
 * @a t_on and @a t_off, and <b>off</b> again after @a t_off until the end of the year.
 *
 * However, in the Southern hemisphere DST switching times are usually reversed:
 * @a t_off is <b>before</b> @a t_on in a given year, so DST is observed from the start of
 * the year until @a t_off, and later from @a t_on until the end of the year, but
 * DST is <b>off</b> in the middle of the year in the interval from @a t_off to @a t_on.
 *
 * @param[in]   p_t64_std     Pointer to an ::MBG_TIME64_T type providing local standard time
 *                            (i.e. %UTC plus standard time zone offset already applied) for
 *                            which to determine the DST and DST announcement status.
 *
 * @param[in]   p_tzi         Pointer to a valid ::MBG_TZ_INFO structure.
 *
 * @param[in]   ann_limit_dl  The announcement interval before a DST status change, in seconds.
 *                            Must be a negative number, see e.g. ::ANN_LIMIT.
 *
 * @param[out]  p_intv        An optional address of a variable into which the time from the
 *                            nearest change is saved, or NULL. If the value is negative, the
 *                            current timestamp @a *p_t64_std is still <b>before</b> the next change,
 *                            e.g. -30 means the next change will occur 30 seconds later.
 *
 * @return  A status word of combined ::TM_GPS_STATUS_BIT_MASKS flags, namely ::TM_DL_ANN and ::TM_DL_ENB.
 *
 * @ingroup mbgtimex_tzdl_fncs
 */
 TM_GPS_STATUS mbg_time_dst_status( const MBG_TIME64_T *p_t64_std, const MBG_TZ_INFO *p_tzi, long ann_limit_dl, int64_t *p_intv ) ;

 /**
 * @brief Convert a %UTC time to local time, and update a status accordingly.
 *
 * Conversion to a valid local time can only be done if the @a p_tzi parameter
 * set is valid. The @a xstatus value is updated accordingly.
 *
 * Optionally the local standard time and the offset applied to yield local time
 * can be returned to the caller.
 *
 * @param[out]  p_t64_loc  Address of an ::MBG_TIME64_T type to take the calculated local timestamp.
 *
 * @param[in]   p_t64_utc  Pointer to an ::MBG_TIME64_T providing the %UTC time to be converted.
 *
 * @param[in]   p_tzi      Pointer to an ::MBG_TZ_INFO variable with the standard settings,
 *                         where switching time are local standard times.
 *
 * @param[in]  ann_limit_dl  The announcement interval before a DST status change, in seconds.
 *                           Must be a negative number, see e.g. ::ANN_LIMIT.
 *
 * @param[in,out]  p_status  Optional address of a ::TM_GPS_STATUS_EXT variable which has to be set
 *                           to 0 or something meaningful before this function is called.
 *                           Additional flags will be set as appropriate.
 *                           May be @a NULL.
 *
 * @param[out]  p_t64_std  An optional address of an ::MBG_TIME64_T type which is set to
 *                         the standard time, i.e. without DST adjustment applied.
 *                         May be @a NULL.
 *
 * @param[out]  p_offs     Optional pointer to a variable to take the offset
 *                         that has been subtracted from the TAI time stamp.
 *                         May be @a NULL.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_time_fncs
 */
 int mbg_utc_to_local_time( MBG_TIME64_T *p_t64_loc, const MBG_TIME64_T *p_t64_utc, const MBG_TZ_INFO *p_tzi, long ann_limit_dl, TM_GPS_STATUS_EXT *p_status, MBG_TIME64_T *p_t64_std, long *p_offs ) ;

 /**
 * @brief Set up ::MBG_LS_INFO and ::MBG_TZ_INFO structures for %UTC.
 *
 * The structures are used to store intermediate results, and thus
 * avoid unnecessary computation when converting between %UTC and
 * local time.
 *
 * This variant expects a %UTC timestamp as input.
 *
 * This function should be called after program startup, when valid
 * ::TZDL and ::UTC data sets are already available, and whenever
 * the the GPS ::UTC parameters have changed, or the computed DST
 * switching times are in the past even though automatic DST computation
 * has been configured.
 *
 * @param[out]  p_lsi      Address of an ::MBG_LS_INFO structure to be set up.
 *
 * @param[out]  p_tzi      Address of an ::MBG_TZ_INFO structure to be set up
 *                         where the switching times are %UTC.
 *
 * @param[in]   p_t64_utc  Pointer to the current %UTC time for which to set up @a p_tzi.
 *
 * @param[in]   p_utc      Pointer to a valid GPS ::UTC parameter set.
 *
 * @param[in]   p_tzdl     Pointer to a valid ::TZDL parameter set.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_tzdl_fncs
 * @see mbg_setup_lsi_tzi_for_tai
 */
 int mbg_setup_lsi_tzi_for_utc( MBG_LS_INFO *p_lsi, MBG_TZ_INFO *p_tzi, const MBG_TIME64_T *p_t64_utc, const UTC *p_utc, const TZDL *p_tzdl ) ;

 /**
 * @brief Set up ::MBG_LS_INFO and ::MBG_TZ_INFO structures for TAI.
 *
 * The structures are used to store intermediate results, and thus
 * avoid unnecessary computation when converting between TAI / %UTC,
 * and local time.
 *
 * This variant expects a TAI timestamp as input and also sets up
 * an ::MBG_TZ_INFO structure where the switching times are TAI.
 *
 * This function should be called after program startup, when valid
 * ::TZDL and ::UTC data sets are already available, and whenever
 * the the GPS ::UTC parameters have changed, or the computed DST
 * switching times are in the past even though automatic DST computation
 * has been configured.
 *
 * @param[out]  p_lsi      Address of an ::MBG_LS_INFO structure to be set up.
 *
 * @param[out]  p_tzi      Address of an ::MBG_TZ_INFO structure to be set up
 *                         where the switching times are %UTC.
 *
 * @param[out]  p_tzi_tai  Address of an additional ::MBG_TZ_INFO structure
 *                         to be set up, where the switching times are TAI.
 *
 * @param[in]   p_t64_tai  Pointer to the current TAI time for which to
 *                         set up @a p_tzi and @a p_tzi_tai.
 *
 * @param[in]   p_utc      Pointer to a valid GPS ::UTC parameter set.
 *
 * @param[in]   p_tzdl     Pointer to a valid ::TZDL parameter set.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_tzdl_fncs
 * @see mbg_setup_lsi_tzi_for_utc
 */
 int mbg_setup_lsi_tzi_for_tai( MBG_LS_INFO *p_lsi, MBG_TZ_INFO *p_tzi, MBG_TZ_INFO *p_tzi_tai, const MBG_TIME64_T *p_t64_tai, const UTC *p_utc, const TZDL *p_tzdl ) ;


/* ----- function prototypes end ----- */



static __mbg_inline /*HDR*/
/**
 * @brief Normalize a GPS week number / second-of-week pair.
 *
 * If a GPS week number / second-of-week pair has been computed
 * for a point in time before the GPS epoch (see ::GPS_INITIAL_DAY),
 * both the week number and the second-of-week can be negative and should
 * be normalized so that the second-of-week is always non-negative, i.e.
 * ***after*** the beginning of the week with the computed number.
 * This avoids errors in further computations.
 *
 * @param[in,out]  p_wn    Address of a variable containing the week number.
 * @param[in,out]  p_wsec  Address of a variable containing the second-of-week.
 *
 * @ingroup mbgtimex_gps_time_fncs
 */
void normalize_wn_wsec( GPS_WNUM *p_wn, GPS_WSEC *p_wsec )
{
  while ( *p_wsec < 0 )
  {
    *p_wsec += SECS_PER_WEEK;
    (*p_wn)--;
  }

  while ( *p_wsec >= SECS_PER_WEEK )
  {
    *p_wsec -= SECS_PER_WEEK;
    (*p_wn)++;
  }

}  // normalize_wn_wsec



static __mbg_inline /*HDR*/
/**
 * @brief Determine the TAI/%UTC time offset for a given TAI time.
 *
 * If the current time @a *p_t64_tai is <b>after</b> the last known leap second,
 * we don't know if the last leap second was an insertion or deletion, and we
 * can't be sure if the stored offset before the LS is still the true offset
 * that was valid before the leap second. It might have been updated to match
 * the offset after the leap second (as in the GPS navigation message, where
 * ::UTC::delta_tls switches to the same value as ::UTC::delta_tlsf after the
 * leap second has passed, so we use the offset after the leap second by default.
 *
 * Only if a leap second is currently being announced, and @a *p_t64_tai is still
 * <b>before</b> the time the leap second occurs, we also apply the step count
 * to yield the offset that is valid before the leap second.
 *
 * @param[in]  p_t64_tai  Pointer to an ::MBG_TIME64_T type providing the current TAI time.

 * @param[in]  p_lsi      Pointer to an ::MBG_LS_INFO variable with current
 *                        %UTC/leap second information.
 *
 * @return  The determined offset required to compute %UTC.
 *
 * @ingroup mbgtimex_time_cnv_fncs
 * @see ::mbg_time64_utc_to_tai
 * @see ::mbg_time64_tai_to_utc
 */
long mbg_offs_utc_from_tai( const MBG_TIME64_T *p_t64_tai, const MBG_LS_INFO *p_lsi )
{
  long l = p_lsi->offs_tai_utc;

  if ( p_lsi->ls_step && ( *p_t64_tai < p_lsi->t64_ls_tai ) )
    l -= p_lsi->ls_step;

  return l;

}  // mbg_offs_utc_from_tai



static __mbg_inline /*HDR*/
/**
 * @brief Convert a TAI timestamp in ::MBG_TIME64_T format to %UTC.
 *
 * TAI is ahead of %UTC by 37 or even more seconds, so the value
 * of the computed %UTC timestamp is less than the value of the
 * original TAI timestamp.
 *
 * @param[out]  p_t64_utc  Address of an ::MBG_TIME64_T to take the computed %UTC timestamp.
 *
 * @param[in]   p_t64_tai  Pointer to an ::MBG_TIME64_T providing the TAI timestamp to be converted.
 *
 * @param[in]   p_lsi      Pointer to an ::MBG_LS_INFO variable with current %UTC/leap second information.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_time_fncs
 * @see ::mbg_time64_utc_to_tai
 * @see ::mbg_offs_utc_from_tai
 */
int mbg_time64_tai_to_utc( MBG_TIME64_T *p_t64_utc, const MBG_TIME64_T *p_t64_tai, const MBG_LS_INFO *p_lsi )
{
  *p_t64_utc = *p_t64_tai - mbg_offs_utc_from_tai( p_t64_tai, p_lsi );

  return MBG_SUCCESS;

}  // mbg_time64_tai_to_utc



static __mbg_inline /*HDR*/
/**
 * @brief Convert a %UTC timestamp in ::MBG_TIME64_T format to TAI.
 *
 * NOTE: If a leap second is inserted, the conversion may
 * be <b>ambiguous during the leap second</b> itself because
 * the %UTC time is usually simply stepped back by 1 s, which
 * results in 2 consecutive %UTC timestamps with the same
 * numeric value.
 *
 * @param[out]  p_t64_tai  Address of an ::MBG_TIME64_T to take the computed TAI timestamp.
 *
 * @param[in]   p_t64_utc  Pointer to a %UTC timestamp in ::MBG_TIME64_T format to be converted.
 *
 * @param[in]   p_lsi      Pointer to an ::MBG_LS_INFO variable with current %UTC/leap second information.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_time_fncs
 * @see ::mbg_time64_tai_to_utc
 */
int mbg_time64_utc_to_tai( MBG_TIME64_T *p_t64_tai, const MBG_TIME64_T *p_t64_utc, const MBG_LS_INFO *p_lsi )
{
  // First we assume the TAI time to be computed
  // is *after* the last known leap second.
  *p_t64_tai = *p_t64_utc + p_lsi->offs_tai_utc;

  // If a leap second is currently being announced,
  // and the *current time* is still *before* the LS,
  // we have to subtract the offset that will be
  // applied when the leap second is handled.
  // NOTE This is ambiguous during the leap second itself.
  // TODO Check if in the comparison below '<=' is better
  // than '<'.
  if ( p_lsi->ls_step && ( *p_t64_tai < p_lsi->t64_ls_tai ) )
    *p_t64_tai -= p_lsi->ls_step;

  return MBG_SUCCESS;

}  // mbg_time64_utc_to_tai



static __mbg_inline /*HDR*/
/**
 * @brief Convert GPS week number plus second-of-week to to POSIX @a time_t format.
 *
 * Only the data format is converted, the offset between
 * GPS time scale and %UTC scale is not taken into account.
 *
 * @deprecated This function is deprecated, use ::mbg_gps_wn_wsec_to_time64_t preferably.
 *
 * @param[out] p_t   Address of an ::MBG_TIME64_T type to take the computed timestamp.
 *
 * @param[in]  wn    A GPS week number as ::GPS_WNUM.
 *
 * @param[in]  wsec  Seconds of the week as ::GPS_WSEC, >= 0.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 */
int _T_DEPRECATED_BY( "mbg_gps_wn_wsec_to_time64_t" ) mbg_gps_wn_wsec_to_time_t( time_t *p_t, GPS_WNUM wn, GPS_WSEC wsec )
{
  MBG_TIME64_T t64;
  int rc = mbg_gps_wn_wsec_to_time64_t( &t64, wn, wsec );

  // On success we still check if the result is in a valid range
  // and convert the result, if required.
  if ( mbg_rc_is_success( rc ) )
    rc = mbg_trnc_time64_t_to_time_t( p_t, &t64 );

  return rc;

}  // mbg_gps_wn_wsec_to_time_t



static __mbg_inline /*HDR*/
/**
 * @brief Convert a GPS week number / day-of-week pair to POSIX @a time_t format.
 *
 * Only the data format is converted, the offset between
 * GPS time scale and %UTC scale is not taken into account.
 *
 * @note The week number WNlsf from the ::UTC parameter set contains
 * the 8 LSBs of the full week number only, covering a +/- ~128 week
 * range. So if the leap second date is ~128 weeks or more before or
 * after the time of reception from the satellites, the WNlsf field
 * is ambiguous, and the ::mbg_find_true_gps_wn_lsf function
 * can be used to try to solve this ambiguity and return the
 * true extended week number, if possible.
 *
 * @note If this function is called to compute the leap second
 * date from the GPS ::UTC parameters, the computed timestamp is
 * associated with <b>the end of the leap second transition</b>, e.g.
 * <em>2017-01-01 00:00:00</em> rather than <em>2016-12-31 23:59:59</em>.
 *
 * @param[out] p_t     Address of a POSIX @a time_t type to take the computed timestamp.
 *
 * @param[in]  wn_lsf  The true extended week number in which
 *                     a leap second occurs, see ::UTC::WNlsf.
 *
 * @param[in]  dn_t    A day-of-week at the end of which the
 *                     leap second occurs, see ::UTC::DNt.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 * @see ::mbg_find_true_gps_wn_lsf
 */
int _T_DEPRECATED_BY( "mbg_gps_wn_dn_to_time64_t" ) mbg_gps_wn_dn_to_time_t( time_t *p_t, GPS_WNUM wn_lsf, GPS_DNUM dn_t )
{
  return mbg_gps_wn_wsec_to_time_t( p_t, wn_lsf, (GPS_WSEC) dn_t * SECS_PER_DAY );

}  // mbg_gps_wn_dn_to_time_t



static __mbg_inline /*HDR*/
/**
 * @brief Convert POSIX @a time_t format to GPS week number and second-of-week.
 *
 * Only the data format is converted, the offset between
 * GPS time scale and %UTC scale is not taken into account.
 *
 * The computed week number can be negative if @a *p_t is before
 * the GPS epoch. The computed values are normalized so that
 * the second-of-week is always non-negative, i.e. contains
 * the seconds after the beginning of the week, even if
 * the week number is negative.
 *
 * @param[out]  p_wn    Address of a ::GPS_WNUM variable for the computed week number.
 *
 * @param[out]  p_wsec  Address of a ::GPS_WSEC variable for the computed second-of-week.
 *
 * @param[in]   p_t     Pointer to the original timestamp in POSIX @a time_t format.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 */
int _T_DEPRECATED_BY( mbg_time64_t_to_gps_wn_wsec ) mbg_time_t_to_gps_wn_wsec( GPS_WNUM *p_wn, GPS_WSEC *p_wsec, const time_t *p_t )
{
  MBG_TIME64_T t64;

  int rc = mbg_exp_time_t_to_time64_t( &t64, p_t );

  if ( mbg_rc_is_success( rc ) )
    mbg_time64_t_to_gps_wn_wsec( p_wn, p_wsec, &t64 );

  return rc;

}  //  mbg_time_t_to_gps_wn_wsec



static __mbg_inline /*HDR*/
/**
 * @brief Convert POSIX @a time_t format to ::T_GPS.
 *
 * Only the data format is converted, the offset between
 * GPS time scale and %UTC scale is not taken into account.
 *
 * @param[out]  p_t_gps  Address of a ::T_GPS variable to take the computed timestamp.
 *
 * @param[in]   p_t      Pointer to the original timestamp in POSIX @a time_t format.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 * @see ::mbg_t_gps_to_time_t
 */
int _T_DEPRECATED_BY( mbg_time64_t_to_gps_wn_wsec ) mbg_time_t_to_t_gps( T_GPS *p_t_gps, const time_t *p_t )
{
  MBG_TIME64_T t64;

  int rc = mbg_exp_time_t_to_time64_t( &t64, p_t );

  if ( mbg_rc_is_success( rc ) )
    rc = mbg_time64_t_to_t_gps( p_t_gps, &t64 );

  return rc;

}  //  mbg_time_t_to_t_gps



static __mbg_inline /*HDR*/
/**
 * @brief Convert ::T_GPS to POSIX @a time_t format.
 *
 * Only the data format is converted, the offset between
 * GPS time scale and %UTC scale is not taken into account.
 *
 * @param[out] p_t      Address of a POSIX @a time_t type to take the computed timestamp.
 *
 * @param[in]  p_t_gps  The timestamp to be converted, in ::T_GPS format.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbgtimex_gps_posix_time_cnv_fncs
 * @see ::mbg_time_t_to_t_gps
 */
int _T_DEPRECATED_BY( mbg_gps_wn_wsec_to_time64_t ) mbg_t_gps_to_time_t( time_t *p_t, const T_GPS *p_t_gps )
{
  MBG_TIME64_T t64;
  int rc = mbg_gps_wn_wsec_to_time64_t( &t64, p_t_gps->wn, p_t_gps->sec );

  // On success we still check if the result is in a valid range
  // and convert the result, if required.
  if ( mbg_rc_is_success( rc ) )
    rc = mbg_trnc_time64_t_to_time_t( p_t, &t64 );

  return rc;

}  // mbg_t_gps_to_time_t



#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBGTIMEX_H */
