
/**************************************************************************
 *
 *  $Id: ctry.c 1.8 2021/03/16 12:21:12 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Functions providing support for different country settings
 *    and languages.
 *
 *    Some OS dependent functions may be required which can be found
 *    in the OS dependent modules ctry_xxx.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: ctry.c $
 *  Revision 1.8  2021/03/16 12:21:12  martin
 *  Updated some comments.
 *  Revision 1.7  2010/07/15 08:26:57  martin
 *  Added clstr_lng() implemented by stefan returning a translated string
 *  by giving the different strings as function arguments.
 *  Revision 1.6  2007/03/29 12:21:51  martin
 *  New functions lstr_idx() and lstr_array_idx().
 *  Revision 1.5  2004/10/26 07:39:37Z  martin
 *  Use C99 fixed-size definitions where appropriate.
 *  Revision 1.4  2001/09/14 12:02:12  MARTIN
 *  Modified parameters for lstr_lng().
 *  Revision 1.3  2000/11/27 14:09:24  MARTIN
 *  Replaced lstr() by lstr_lng() with takes a language paramter to allow
 *  retrieval of strings for another than the current language.
 *  A new macro _lstr() has been added to ctry.h which calls lstr_lng()
 *  passing the current language.
 *  The functions ctry_fmt_dt() and ctry_fmt_times() and associated
 *  definitions have been moved to a new module ctry_fmt.c/ctry_fmt.h.
 *  Revision 1.2  2000/07/21 10:00:08  MARTIN
 *  Initial revision
 *
 **************************************************************************/

#define _CTRY
 #include <ctry.h>
#undef _CTRY

#include <stdio.h>
#include <stdarg.h>
#include <string.h>



/*HDR*/
/**
 * @brief Return the index of a CLSTR component for a certain language code.
 *
 * If the strings for several languages are the same, only the first
 * array entry may be populated, while the others are all @a NULL,
 * so the returned index differs from the language code @p lng.
 *
 * @param[in]  s    Pointer to a constant array of localized strings.
 * @param[in]  lng  The language code 0..::N_LNG-1.
 *
 * @return  The index to the array @p s, derived from @p lng.
 */
int lstr_idx( CLSTR s, int lng )
{
  if ( lng >= N_LNG )  // If lng out of range,
    return 0;          // use default index.

  // If the array entry for the selected language is 0,
  // return index 0 for the default entry.
  return s[lng] ? lng : 0;

}  // lstr_idx



/*HDR*/
/**
 * @brief Return the index of a ::CLSTR component for a certain language.
 *
 * If the strings for several languages are the same, only the first
 * array entry may be populated, while the others are all @a NULL,
 * so the returned index differs from the language code @p lng.
 *
 * @param[in]  s      The array of ::CLSTR entries.
 * @param[in]  idx    The index of the array element.
 * @param[in]  n_lng  The number of supported languages.
 * @param[in]  lng    The language for which the index is to be retrieved.
 *
 * @return  The index to the array @p s, derived from @p lng.
 */
int lstr_array_idx( CLSTR s, int idx, int n_lng, int lng )
{
  int str_idx = n_lng * idx;
  return str_idx + lstr_idx( &s[str_idx], lng );

}  // lstr_array_idx



/*HDR*/
/**
 * @brief Return a localized string according to a language code.
 *
 * @param[in]  s    Pointer to a constant array of localized strings.
 * @param[in]  lng  The language code 0..::N_LNG-1.
 *
 * @return  The string according to the language code.
 */
const char *lstr_lng( CLSTR s, int lng )
{
  return s[lstr_idx( s, lng)];

}  // lstr_lng



/*HDR*/
/**
 * @brief Set up some localization depending on a specific country code.
 *
 * This sets up the global variable ::ctry.
 *
 * @param[in]  code  The ::CTRY_CODE.
 */
void ctry_setup( CTRY_CODE code )
{
  language = LNG_ENGLISH;
  ctry.code = code;

  switch ( code )
  {
    case CTRY_US:
      ctry.dt_fmt = DT_FMT_MMDDYYYY;
      ctry.dt_sep = DT_SEP_MINUS;
      ctry.tm_fmt = TM_FMT_24H;
      ctry.tm_sep = TM_SEP_COLON;
      break;


    case CTRY_UK:
      ctry.dt_fmt = DT_FMT_DDMMYYYY;
      ctry.dt_sep = DT_SEP_SLASH;
      ctry.tm_fmt = TM_FMT_24H;
      ctry.tm_sep = TM_SEP_COLON;
      break;


    default:
      language = LNG_GERMAN;
      ctry.code = CTRY_GERMANY;

      ctry.dt_fmt = DT_FMT_DDMMYYYY;
      ctry.dt_sep = DT_SEP_DOT;
      ctry.tm_fmt = TM_FMT_24H;
      ctry.tm_sep = TM_SEP_COLON;

  }  /* switch */


}  /* ctry_setup */



/*HDR*/
/**
 * @brief Set up localization for the next supported country code.
 *
 * Used to easily switch between different country codes.
 */
void ctry_next( void )
{
  switch ( ctry.code )
  {
    case CTRY_GERMANY:
      ctry_setup( CTRY_US );
      break;

    case CTRY_US:
      ctry_setup( CTRY_UK );
      break;

    default:
      ctry_setup( CTRY_GERMANY );
      break;

  }  // switch

}  // ctry_next



/*HDR*/
/**
 * @brief Return a string from an array of strings given as function arguments.
 *
 * @param[in]  index  Index of the string to be returned.
 * @param[in]  ...    Variable number of string arguments.
 *
 * @return The string associated with the index.
 */
const char *clstr_lng( int index, ... )
{
  const char *ret;
  const char *default_ret;
  int i;
  typedef char *MY_LSTR;

  va_list ap;
  va_start( ap, index );

  ret = va_arg( ap, MY_LSTR );
  default_ret = ret;

  for ( i = 1; ( i <= index ) && ( ret != NULL ); i++ )
    ret = va_arg( ap, MY_LSTR );

  va_end( ap );

  return ret ? ret : default_ret;

}  // clstr_lng

