
/**************************************************************************
 *
 *  $Id: deviohlp.h 1.8 2022/08/24 16:25:41 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for deviohlp.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: deviohlp.h $
 *  Revision 1.8  2022/08/24 16:25:41  martin.burnicki
 *  Removed obsolete function parameters.
 *  Revision 1.7  2022/06/30 09:10:39  martin.burnicki
 *  Updated function prototypes due to some doxygen comment fixes.
 *  Revision 1.6  2021/03/21 18:08:14  martin
 *  Updated a bunch of comments.
 *  Revision 1.5  2020/02/27 13:40:00  martin
 *  Added some new inline functions.
 *  Updated function prototypes.
 *  Revision 1.4  2017/07/05 13:50:18  martin
 *  Moved definition of PCPS_SER_PACK here.
 *  Defined function type MBG_ERR_MSG_FNC.
 *  New structure PCPS_TIME_EXT.
 *  New inline function pcps_time_is_valid().
 *  Updated function prototypes.
 *  Revision 1.3  2013/09/26 08:25:18Z  martin
 *  Moved ALL_PTP_CFG_INFO definition to cfg_hlp.h.
 *  Updated doxygen comments.
 *  Revision 1.2  2012/10/15 13:51:18Z  martin
 *  Include cfg_hlp.h.
 *  Added structure ALL_PTP_CFG_INFO.
 *  Updated function prototypes.
 *  Revision 1.1  2011/08/03 15:36:44Z  martin
 *  Initial revision with functions moved here from mbgdevio.
 *
 **************************************************************************/

#ifndef _DEVIOHLP_H
#define _DEVIOHLP_H


/* Other headers to be included */

#include <mbgdevio.h>
#include <cfg_hlp.h>


#ifdef _DEVIOHLP
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief A helper structure used with configuration of old DCF77 receivers.
 *
 * @deprecated This structure has been used with some very
 * old DCF77 receivers to configure the serial interface.
 * It has been deprecated by ::PORT_SETTINGS and ::PORT_INFO.
 */
typedef struct
{
  PCPS_SERIAL pack;  ///< This packed byte is read from or written to the device.

  uint8_t baud;      ///< The unpacked baud rate code, see ::PCPS_BD_CODES.
  uint8_t frame;     ///< The unpacked framing code, see ::PCPS_FR_CODES.
  uint8_t mode;      ///< The unpacked mode code, see ::PCPS_MOD_CODES.

} PCPS_SER_PACK;



typedef int MBG_ERR_MSG_FNC( const char *s );


/**
 * @brief An extended time and status structure
 *
 * This structure provides monitoring and configuration tools
 * with a unified structure containing the current time and
 * extended status. The structure needs to be set up depending
 * on the capabilities of a particular device and the API calls
 * which could be used to retrieve the information.
 */
typedef struct
{
  PCPS_TIME t;                  ///< current date, time, and limited status
  uint8_t comp_sig_mode;        ///< 0..::N_CONN_SIG_MODES-1, see ::COMP_SIG_MODES
  int16_t comp_sig_val;         ///< compensated signal value, see @ref PCPS_SIG_VAL_DEFS
  int32_t utc_offs;             ///< %UTC offset, always expanded to [seconds]
  PCPS_TIME_STATUS_X status_x;  ///< extended status, see @see @ref PCPS_TIME_STATUS_FLAGS
  uint16_t year;                ///< full year number
  uint32_t flags;               ///< see ::PCPS_TIME_EXT_FLAGS

} PCPS_TIME_EXT;



static __mbg_inline /*HDR*/
/**
 * @brief Check if the day-of-week in a ::PCPS_TIME structure is in the valid range.
 *
 * For historical reasons, the valid range for ::PCPS_TIME::wday
 * is 1 to 7, as transmitted by DCF77. See also ::PCPS_TIME::wday.
 *
 * @param[in] p  The structure to be checked.
 *
 * @return  @a true if day-of-week is valid, else @a false.
 *
 * @see ::pcps_date_time_is_valid
 * @see ::pcps_date_time_wday_is_valid
 */
int pcps_wday_is_valid( const PCPS_TIME *p )
{
  return ( p->wday >= 1 ) && ( p->wday <= 7 );

}  // pcps_wday_is_valid



static __mbg_inline /*HDR*/
/**
 * @brief Check if a ::PCPS_TIME structure contains valid date and time
 *
 * This function does ***not*** check if the day-of-week
 * (::PCPS_TIME::wday) is in the valid range.
 * Use ::pcps_date_time_wday_is_valid to check this, too,
 * ::pcps_wday_is_valid to explicitly check the day-of-week.
 *
 * @param[in] p  The structure to be checked
 *
 * @return  @a true if date and time are valid, else @a false.
 *
 * @see ::pcps_date_time_wday_is_valid
 * @see ::pcps_wday_is_valid
 */
bool pcps_date_time_is_valid( const PCPS_TIME *p )
{
  return ( p->sec100 <= 99 )
      && ( p->sec <= 60 )   // allow for leap second
      && ( p->min <= 59 )
      && ( p->hour <= 23 )
      && ( p->mday >= 1 ) && ( p->mday <= 31 )
      && ( p->month >= 1 ) && ( p->month <= 12 )
      && ( p->year <= 99 );

}  // pcps_date_time_is_valid



static __mbg_inline /*HDR*/
/**
 * @brief Check if a ::PCPS_TIME structure contains valid date, time, and day-of-week.
 *
 * Unlike ::pcps_date_time_is_valid, this function also checks
 * if the day-of-week (::PCPS_TIME::wday) is in the valid range.
 *
 * @param[in] p  The structure to be checked
 *
 * @return  @a true if date, time and day-of-week are valid, else @a false.
 *
 * @see ::pcps_date_time_is_valid
 */
int pcps_date_time_wday_is_valid( const PCPS_TIME *p )
{
  return pcps_date_time_is_valid( p )
      && pcps_wday_is_valid( p );

}  // pcps_date_time_wday_is_valid



static __mbg_inline /*HDR*/
/**
 * @brief Check if a ::PCPS_TIME structure contains valid date, time, and day-of-week.
 *
 * Unlike ::pcps_date_time_is_valid, this function also checks
 * if the day-of-week (::PCPS_TIME::wday) is in the valid range.
 *
 * @param[in] p  The structure to be checked
 *
 * @return  @a true if date, time and day-of-week are valid, else @a false.
 *
 * @see ::pcps_date_time_is_valid
 */
int _DEPRECATED_BY( "pcps_date_time_wday_is_valid" ) pcps_time_is_valid( const PCPS_TIME *p )
{
  return pcps_date_time_wday_is_valid( p );

}  // pcps_time_is_valid



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 int chk_feat_supp( MBG_DEV_HANDLE dh, MBG_CHK_SUPP_FNC *chk_supp_fnc, MBG_ERR_MSG_FNC *err_msg_fnc, const char *not_supp_msg ) ;
 /**
 * @brief Read or setup all GNSS status information.
 *
 * This function should be called preferably to get a summary of
 * the GNSS status from GNSS receivers (GPS, GLONASS, ...).
 *
 * The function ::mbg_get_device_info must have been called before, and
 * the returned ::PCPS_DEV structure has to be passed to this function.
 *
 * If the device supports this, the low level GNSS API functions
 * are called directly to collect the status information. If the device
 * doesn't support the GNSS API but is a pure GPS receiver, the GPS
 * API functions are called and the GNSS data structures are filled up
 * accordingly, so the calling application can always evaluate the
 * GNSS data structures in ::ALL_GNSS_INFO.
 *
 * If neither GPS nor another GNSS system is supported, this function
 * returns the ::MBG_ERR_NOT_SUPP_BY_DEV error.
 *
 * @param[in]  dh     Valid handle to a Meinberg device.
 * @param[out] p_agi  Pointer to an ::ALL_GNSS_INFO to be filled.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_get_gps_stat_info
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_get_gps_all_gnss_sat_info
 */
 int mbg_chk_get_all_gnss_info( MBG_DEV_HANDLE dh, ALL_GNSS_INFO *p_agi ) ;

 /**
 * @brief Read all serial port settings and supported configuration parameters.
 *
 * The functions ::mbg_get_device_info and ::mbg_setup_receiver_info
 * must have been called before, and the returned ::PCPS_DEV and
 * ::RECEIVER_INFO structures have to be passed to this function.
 *
 * The complementary function ::mbg_save_serial_settings should be used
 * to write the modified serial port configuration back to the device.
 *
 * @param[in]  dh       Valid handle to a Meinberg device.
 * @param[out] p_rpcfg  Pointer to a ::RECEIVER_PORT_CFG structure to be filled up.
 * @param[in]  p_ri     Pointer to a valid ::RECEIVER_INFO structure.
 *
 * @return ::MBG_SUCCESS, or error code returned by device I/O control function.
 *
 * @see ::mbg_get_device_info
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_save_serial_settings
 */
 int mbg_get_serial_settings( MBG_DEV_HANDLE dh, RECEIVER_PORT_CFG *p_rpcfg, const RECEIVER_INFO *p_ri ) ;

 /**
 * @brief Write the configuration settings for a single serial port to a device.
 *
 * Modifications to the serial port configuration should be made only
 * after ::mbg_get_serial_settings had been called to read all serial port
 * settings and supported configuration parameters.
 * This function has finally to be called once for every serial port
 * the configuration of which has been modified.
 *
 * As also required by ::mbg_get_serial_settings, the functions
 * ::mbg_get_device_info and ::mbg_setup_receiver_info must have been
 * called before, and the returned ::PCPS_DEV and ::RECEIVER_INFO structures
 * have to be passed to this function.
 *
 * @param[in]  dh        Valid handle to a Meinberg device.
 * @param[in]  p_rpcfg   Pointer to a valid ::RECEIVER_PORT_CFG structure.
 * @param[in]  port_num  Index of the serial port to be saved.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_serial_settings
 * @see ::mbg_get_device_info
 * @see ::mbg_setup_receiver_info
 */
 int mbg_save_serial_settings( MBG_DEV_HANDLE dh, RECEIVER_PORT_CFG *p_rpcfg, int port_num ) ;

 /**
 * @brief Read all network configuration into an ::ALL_NET_CFG_INFO structure.
 *
 * Reads the network configuration of a device via the LAN_IP4 API and
 * translates the structures into NET_CFG structures.
 *
 * As soon as available, this function should make use of the NET_CFG API.
 *
 * An ::ALL_NET_CFG_INFO and the appropriate number of ::MBG_NET_INTF_LINK_INFO_IDX,
 * ::MBG_NET_INTF_ADDR_INFO_IDX, ::MBG_IP_ADDR_IDX, ::MBG_NET_NAME_IDX and
 * ::MBG_NET_INTF_ROUTE_INFO_IDX will be allocated and need to be freed later
 * by calling ::free_all_net_cfg_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_NET_CFG_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_settings
 * @see ::free_all_net_cfg_info
 */
 int mbg_get_all_net_cfg_info( MBG_DEV_HANDLE dh, ALL_NET_CFG_INFO **p ) ;

 /**
 * @brief Write all network settings to a device.
 *
 * The complementary function ::mbg_get_all_net_cfg_info should
 * have been used to read the original network settings and
 * supported configuration parameters.
 *
 * The appropriate settings are translated into LAN_IP4 structures
 * and send to the device using the appropriate API functions.
 *
 * As soon as available, this function should make use of the NET_CFG API.
 *
 * @param[in]  dh  Valid handle to a Meinberg device.
 * @param[in]  p   Pointer to a pointer of ::ALL_NET_CFG_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_set_ip4_settings
 */
 int mbg_save_all_net_cfg_info( MBG_DEV_HANDLE dh, ALL_NET_CFG_INFO *p ) ;

 /**
 * @brief Read current network status into an ::ALL_NET_STATUS_INFO structure.
 *
 * Reads the network status of a device via the LAN_IP4 API and
 * translates the structures into NET_CFG structures.
 *
 * As soon as available, this function should make use of the NET_CFG API.
 *
 * An ::ALL_NET_STATUS_INFO and the appropriate number of ::MBG_NET_INTF_LINK_INFO_IDX,
 * ::MBG_NET_INTF_ADDR_INFO_IDX, ::MBG_IP_ADDR_IDX, ::MBG_NET_NAME_IDX and
 * ::MBG_NET_INTF_ROUTE_INFO_IDX will be allocated and need to be freed later
 * by calling ::free_all_net_status_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_NET_STATUS_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_state
 * @see ::free_all_net_status_info
 */
 int mbg_get_all_net_status_info( MBG_DEV_HANDLE dh, ALL_NET_STATUS_INFO **p ) ;

 /**
 * @brief Read all PTP settings and supported configuration parameters.
 *
 * The complementary function ::mbg_save_all_ptp_cfg_info should
 * be used to write the modified configuration back to the device.
 *
 * @param[in]  dh  Valid handle to a Meinberg device.
 * @param[out] p   Address of an ::ALL_PTP_CFG_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_save_all_ptp_cfg_info
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
 int mbg_get_all_ptp_cfg_info( MBG_DEV_HANDLE dh, ALL_PTP_CFG_INFO *p ) ;

 /**
 * @brief Write all PTP settings to a device.
 *
 * The complementary function ::mbg_get_all_ptp_cfg_info should
 * have been used to read the original PTP settings and supported
 * configuration parameters.
 *
 * @param[in] dh  Valid handle to a Meinberg device.
 * @param[in] p   Pointer to a valid ::ALL_PTP_CFG_INFO structure.
 *
 * @return ::MBG_SUCCESS or error code returned by device I/O control function.
 *
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
 int mbg_save_all_ptp_cfg_info( MBG_DEV_HANDLE dh, const ALL_PTP_CFG_INFO *p ) ;

 /**
 * @brief Read all XMR info into a newly or re-allocated ::ALL_XMULTI_REF_INFO.
 *
 * @note ::mbg_chk_dev_has_xmr should be called before using this function.
 *
 * An ::ALL_XMULTI_REF_INFO and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_INFO_IDX and ::XMR_EXT_SRC_INFO_IDX will be allocated and need
 * to be freed by calling ::free_all_xmulti_ref_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_XMULTI_REF_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::free_all_xmulti_ref_info
 */
 int mbg_get_all_xmulti_ref_info( MBG_DEV_HANDLE dh, ALL_XMULTI_REF_INFO **p ) ;

 /**
 * @brief Write all extended multi ref settings to a device.
 *
 * The complementary function ::mbg_get_all_xmulti_ref_info should
 * have been used to read the original extended multi ref info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::ALL_XMULTI_REF_INFO structure with all settings.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_set_gps_xmr_settings_idx
 */
 _NO_MBG_API_ATTR int _MBG_API mbg_save_all_xmulti_ref_info( MBG_DEV_HANDLE dh, ALL_XMULTI_REF_INFO *p ) ;

 /**
 * @brief Read all XMR status info into a newly or re-allocated ::ALL_XMULTI_REF_STATUS.
 *
 * @note ::mbg_chk_dev_has_xmr should be called before using this function.
 *
 * An ::ALL_XMULTI_REF_STATUS and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_STATUS_IDX will be allocated and need to be freed by calling
 * ::free_all_xmulti_ref_status.
 *
 * @param[in]   dh    Valid handle to a Meinberg device.
 * @param[in]   info  Pointer to the appropriate info structure.
 * @param[out]  p     Pointer to a pointer of ::ALL_XMULTI_REF_STATUS.
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::free_all_xmulti_ref_status
 */
 int mbg_get_all_xmulti_ref_status( MBG_DEV_HANDLE dh, const ALL_XMULTI_REF_INFO *info, ALL_XMULTI_REF_STATUS **p ) ;

 /**
 * @brief Read all user capture information and store it into a newly allocated or reused ::ALL_UCAP_INFO.
 *
 * @note ::mbg_chk_dev_has_ucap should be called to check if this API is supported.
 *
 * The appropriate number of ::TTM structures will be allocated and needs to be freed
 * by calling ::free_all_ucap_info. Existing user captures will not be removed, so the
 * number of user captures can never decrease.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer to ::ALL_UCAP_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::mbg_chk_dev_has_ucap
 * @see ::free_all_ucap_info
 */
 int mbg_get_all_ucap_info( MBG_DEV_HANDLE dh, ALL_UCAP_INFO **p ) ;

 void test_gpio( MBG_DEV_HANDLE dh, int verbose ) ;
 void test_xmr( MBG_DEV_HANDLE dh, int verbose ) ;
 void port_info_from_pcps_serial( PORT_INFO_IDX *p_pii, PCPS_SERIAL pcps_serial, uint32_t supp_baud_rates ) ;
 void pcps_serial_from_port_info( PCPS_SERIAL *p, const PORT_INFO_IDX *p_pii ) ;
 /**
 * @brief Unpack a structure with serial port parameters.
 *
 * @param[in,out]  p  Address of a structure holding both the
 *                    packed and unpacked information.
 */
 void pcps_unpack_serial( PCPS_SER_PACK *p ) ;

 /**
 * @brief Pack a structure with serial port parameters.
 *
 * @param[in,out]  p  Address of a structure holding both the
 *                    packed and unpacked information.
 */
 void pcps_pack_serial( PCPS_SER_PACK *p ) ;

 /**
 * @brief Setup a list of ISA port addresses from a string.
 *
 * @param[in]   s          A string with port addresses, separated by comma.
 * @param[out]  port_vals  An array of port addresses to be filled.
 * @param[in]   n_vals     The max number of possible entries in @p port_vals.
 */
 void pcps_setup_isa_ports( char *s, int *port_vals, int n_vals ) ;

 const char *setup_device_type_name( char *s, size_t max_len, MBG_DEV_HANDLE dh, const RECEIVER_INFO *p_ri ) ;
 const char *setup_asic_features( char *s, size_t max_len, MBG_DEV_HANDLE dh ) ;

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _DEVIOHLP_H */
