
/**************************************************************************
 *
 *  $Id: xdevfeat.c 1.10 2021/05/27 14:37:35 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Meinberg API functions to check extended device features.
 *
 *    See notes near "defgroup xdevfeat_chk_supp_fncs" in xdevfeat.h.
 *
 * -----------------------------------------------------------------------
 *  $Log: xdevfeat.c $
 *  Revision 1.10  2021/05/27 14:37:35  martin
 *  Tiny cleanup.
 *  Revision 1.9  2021/05/05 06:37:42  udo
 *  Added xdevfeat_has_pmu_cmd().
 *  Revision 1.8  2021/03/11 16:12:08  martin
 *  Tiny doxygen fix.
 *  Revision 1.7  2020/06/30 13:32:37  udo
 *  Added xdevfeat_has_ptp_state.
 *  Revision 1.6  2020/05/06 08:57:17  udo
 *  Added xdevfeat_has_raw_dac.
 *  Revision 1.5  2020/04/29 06:39:11  gregoire.diehl
 *  Added xdevfeat_has_xmrs_gpio_time_and_phase_supp.
 *  Revision 1.4  2019/09/27 15:35:52Z  martin
 *  A couple of new functions, and doxygen updates.
 *  Revision 1.3  2018/07/16 12:46:03  martin
 *  A couple of new functions New functions added by
 *  thomas-b and philipp.
 *  Doxygen fixes.
 *  Some _PRELIMINARY_CODE conditionals were removed.
 *  Revision 1.2  2017/07/06 07:47:06  martin
 *  Plenty of functions that test if a specific feature is
 *  supported provided by thomas-b, philipp, and martin.
 *  Doxygen comments.
 *  Revision 1.1  2016/03/16 14:32:52  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _XDEVFEAT
  #include <xdevfeat.h>
#undef _XDEVFEAT

#include <mbgerror.h>
#include <xtiocomm.h>



/**
 * @brief Entry for a table to specify model-depending built-in features
 */
typedef struct
{
  /// Model code according to ::RECEIVER_INFO::model_code, see ::GPS_MODEL_CODES
  uint16_t model_code;

  /// A combination of @ref GPS_FEATURE_MASKS bit masks specifying the
  /// built-in features supported by the specified device model.
  BUILTIN_FEATURE_MASK feature_mask;

} BUILTIN_FEATURE_TABLE_ENTRY;


/**
 * @brief A static table of builtin features
 *
 * Used to lookup the builtin features of a particular device model.
 */
static BUILTIN_FEATURE_TABLE_ENTRY builtin_feature_table[] = GPS_MODEL_BUILTIN_FEATURES;



static /*HDR*/
/**
 * @brief Check if a device supports a specific built-in feature or API
 *
 * Some features or API calls are implicitly supported by particular devices.
 * This function uses the ::RECEIVER_INFO::model_code to look up the specific
 * device in a table of ::BUILTIN_FEATURE_TABLE_ENTRY entries, and checks if
 * the requested feature bits are set for this device model.
 * If the model code of the specific device can't be found in the table then
 * then ::MBG_ERR_DEV_NOT_SUPP is returned and the source code files probably
 * need to be updated to support this device.
 *
 * @param[in] msk   One of the @ref GPS_FEATURE_MASKS bit masks
 * @param[in] p_ri  A ::RECEIVER_INFO structure read from the device before
 *
 * @return ::MBG_SUCCESS if all specified mask bits are set in ::RECEIVER_INFO::features,
 *         ::MBG_ERR_NOT_SUPP_BY_DEV if the bits are not set, and ::MBG_ERR_DEV_NOT_SUPP
 *         if the model code can't be found in the table.
 */
int check_builtin_feature( BUILTIN_FEATURE_MASK msk, const RECEIVER_INFO *p_ri )
{
  BUILTIN_FEATURE_TABLE_ENTRY *p;

  //### TODO Implement a kind of cache so we don't have to search the whole
  // table if several features of the same device are checked after each other.

  for ( p = builtin_feature_table; p->model_code || p->feature_mask; p++ )
    if ( p->model_code == p_ri->model_code )
      return ( ( p->feature_mask & msk ) == msk ) ? MBG_SUCCESS : MBG_ERR_NOT_SUPP_BY_DEV;

  return MBG_ERR_DEV_NOT_SUPP;

}  // check_builtin_feature



static /*HDR*/
/**
 * @brief Check if a device supports a specific feature or API
 *
 * This API call checks if a specific feature or API is supported
 * according to the ::RECEIVER_INFO::features mask read from the device.
 *
 * @param[in] msk   One of the @ref GPS_FEATURE_MASKS bit masks
 * @param[in] p_ri  A ::RECEIVER_INFO structure read from the device before
 *
 * @return ::MBG_SUCCESS if all specified mask bits are set in ::RECEIVER_INFO::features,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV
 */
int check_ri_feature( RI_FEATURES msk, const RECEIVER_INFO *p_ri )
{
  return _check_feat_supp_mask( p_ri->features, msk );

}  // check_ri_feature



static /*HDR*/
/**
 * @brief Check if a specific extended feature is supported
 *
 * This API call checks if a specific extended feature or API is supported
 * according to the ::MBG_XFEATURE_BUFFER read from the device.
 *
 * @param[in] xf_bit   One of the ::MBG_XFEATURE_BITS
 * @param[in] xf_buf   Pointer to a ::MBG_XFEATURE_BUFFER read from the device before
 *
 * @return ::MBG_SUCCESS if the specified feature bit is set, else ::MBG_ERR_NOT_SUPP_BY_DEV or ::MBG_ERR_RANGE
 *
 * @see ::check_feat_supp_byte_array
 */
int check_xfeature( int xf_bit, const MBG_XFEATURE_BUFFER *xf_buf )
{
  return check_feat_supp_byte_array( xf_bit, xf_buf->b, sizeof( xf_buf->b ) );

}  // check_xfeature



static /*HDR*/
/**
 * @brief Check if a specific bit is set in the TLV's byte array.
 *
 * This API call checks if a specific TLV feature is supported according to
 * the ::MBG_TLV_INFO read from the device.
 *
 * @param[in] tlv_feat_type  One of the ::MBG_TLV_FEAT_TYPES
 * @param[in] tlv_info       Pointer to a ::MBG_TLV_INFO read from the device before
 *
 * @return ::MBG_SUCCESS if the specified feature bit is set, else ::MBG_ERR_NOT_SUPP_BY_DEV or ::MBG_ERR_RANGE
 *
 * @see ::check_feat_supp_byte_array
 */
int check_tlv_feat_supp( int tlv_feat_type, const MBG_TLV_INFO *tlv_info )
{
  return check_feat_supp_byte_array( tlv_feat_type, tlv_info->supp_tlv_feat.b, sizeof( tlv_info->supp_tlv_feat.b ) );

}  // check_tlv_feat_supp



/*HDR*/
/**
 * @brief Check if a device can receive the GPS satellite system
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_dev_is_gps
 * @see ::mbg_chk_dev_is_gps
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_gps( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_GPS, &p_xdf->receiver_info );

}  // xdevfeat_is_gps

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_gps;



/*HDR*/
/**
 * @brief Check if a device supports the GNSS API
 *
 * This is usually supported by devices which can receive signals
 * from different satellite systems, e.g. GPS, GLONASS, ...
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_dev_is_gnss
 * @see ::mbg_chk_dev_is_gnss
 * @see ::MBG_GNSS_TYPES
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_gnss( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_GNSS, &p_xdf->receiver_info );

}  // xdevfeat_is_gnss

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_gnss;



/*HDR*/
/**
 * @brief Check if a device is a time code receiver (IRIG or similar)
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_dev_is_tcr
 * @see ::mbg_chk_dev_is_tcr
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_tcr( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_TCR, &p_xdf->receiver_info );

}  // xdevfeat_is_tcr

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_tcr;



/*HDR*/
/**
 * @brief Check if a device is a DCF77 AM receiver
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_dev_is_dcf
 * @see ::mbg_chk_dev_is_dcf
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_dcf( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_DCF_AM, &p_xdf->receiver_info );

}  // xdevfeat_is_dcf

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_dcf;



/*HDR*/
/**
 * @brief Check if a device can receive DCF77 PZF
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_dev_has_pzf
 * @see ::mbg_chk_dev_has_pzf
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_pzf( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_DCF_PZF, &p_xdf->receiver_info );

}  // xdevfeat_has_pzf

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_pzf;



/*HDR*/
/**
 * @brief Check if a device is an MSF receiver
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_msf( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_MSF, &p_xdf->receiver_info );

}  // xdevfeat_is_msf

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_msf;



/*HDR*/
/**
 * @brief Check if a device is a JJY receiver
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_jjy( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_JJY, &p_xdf->receiver_info );

}  // xdevfeat_is_jjy

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_jjy;



/*HDR*/
/**
 * @brief Check if a device is a WWVB receiver
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_wwvb( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_WWVB, &p_xdf->receiver_info );

}  // xdevfeat_is_wwvb

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_wwvb;



/*HDR*/
/**
 * @brief Check if a device is a bus level device
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_is_bus_lvl_dev( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_IS_BUS_LVL_DEV, &p_xdf->receiver_info );

}  // xdevfeat_is_bus_lvl_dev

XDEVFEAT_CHK_SUPP_FNC xdevfeat_is_bus_lvl_dev;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ims( const MBG_XDEV_FEATURES *p_xdf )
{
  if ( mbg_rc_is_success( check_ri_feature( GPS_HAS_IMS, &p_xdf->receiver_info ) ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // xdevfeat_has_ims

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ims;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_havequick( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_HAVEQUICK, &p_xdf->receiver_info );

}  // xdevfeat_has_havequick

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_havequick;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_gpio( const MBG_XDEV_FEATURES *p_xdf )
{
  if ( mbg_rc_is_success( check_ri_feature( GPS_HAS_GPIO, &p_xdf->receiver_info ) ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // xdevfeat_has_gpio

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_gpio;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_synth( const MBG_XDEV_FEATURES *p_xdf )
{
  if ( mbg_rc_is_success( check_ri_feature( GPS_HAS_SYNTH, &p_xdf->receiver_info ) ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // xdevfeat_has_synth

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_synth;



/*HDR*/
/**
 * @brief Retrieve the number of supported programmable pulse outputs.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return The number of programmable pulse outputs supported by the device.
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::xdevfeat_has_pout
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_num_pout( const MBG_XDEV_FEATURES *p_xdf )
{
  const RECEIVER_INFO *ri = &p_xdf->receiver_info;

  return ri->n_prg_out;

}  // xdevfeat_num_pout

XDEVFEAT_CHK_SUPP_FNC xdevfeat_num_pout;



/*HDR*/
/**
 * @brief Check if a device supports programmable pulse outputs.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::xdevfeat_num_pout
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_pout( const MBG_XDEV_FEATURES *p_xdf )
{
  return ( xdevfeat_num_pout( p_xdf ) > 0 ) ? MBG_SUCCESS : MBG_ERR_NOT_SUPP_BY_DEV;

}  // xdevfeat_has_pout

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_pout;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_irig_tx( const MBG_XDEV_FEATURES *p_xdf )
{
  if ( mbg_rc_is_success( check_ri_feature( GPS_HAS_IRIG_TX, &p_xdf->receiver_info ) ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // xdevfeat_has_irig_tx

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_irig_tx;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_irig_rx( const MBG_XDEV_FEATURES *p_xdf )
{
  if ( mbg_rc_is_success( check_ri_feature( GPS_HAS_IRIG_RX, &p_xdf->receiver_info ) ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // xdevfeat_has_irig_rx

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_irig_rx;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_mbg_os( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_MBG_OS, &p_xdf->receiver_info );

}  // xdevfeat_has_mbg_os

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_mbg_os;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_serouts( const MBG_XDEV_FEATURES *p_xdf )
{
  const RECEIVER_INFO *ri = &p_xdf->receiver_info;

  if ( ri->n_com_ports > 0 )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // xdevfeat_has_serouts

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_serouts;



/*HDR*/
/**
 * @brief Check if a device supports the ::BVAR_STAT structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_bvar_stat( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_BVAR_STAT, &p_xdf->receiver_info );

}  // xdevfeat_has_bvar_stat

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_bvar_stat;



/*HDR*/
/**
 * @brief Check if a device supports reading the position as ::XYZ array
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_pos_xyz( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_POS_XYZ, &p_xdf->receiver_info );

}  // xdevfeat_has_pos_xyz

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_pos_xyz;



/*HDR*/
/**
 * @brief Check if a device supports reading the position as ::LLA array
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_pos_lla( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_POS_LLA, &p_xdf->receiver_info );

}  // xdevfeat_has_pos_lla

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_pos_lla;



/*HDR*/
/**
 * @brief Check if the device supports the builtin feature TIME
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_time_ttm( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_TIME_TTM, &p_xdf->receiver_info );

} // xdevfeat_has_time_ttm



/*HDR*/
/**
 * @brief Check if a device supports the ::MBG_TIME_SCALE_INFO structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_time_scale_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_time_scale( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_TIME_SCALE, &p_xdf->receiver_info );

}  // xdevfeat_has_time_scale

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_time_scale;



/*HDR*/
/**
 * @brief Check if a device supports the ::TZDL structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_tzdl( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_TZDL, &p_xdf->receiver_info );

}  // xdevfeat_has_tzdl

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_tzdl;



/*HDR*/
/**
 * @brief Check if a device supports the ::TZCODE API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_tzcode( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_TZCODE, &p_xdf->receiver_info );

} // xdevfeat_has_tzcode

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_tzcode;



/*HDR*/
/**
 * @brief Check if a device supports the ::ANT_INFO structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ant_info( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_ANT_INFO, &p_xdf->receiver_info );

}  // xdevfeat_has_ant_info

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ant_info;



/*HDR*/
/**
 * @brief Check if a device supports the ::ENABLE_FLAGS structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_enable_flags( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_ENABLE_FLAGS, &p_xdf->receiver_info );

}  // xdevfeat_has_enable_flags

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_enable_flags;



/*HDR*/
/**
 * @brief Check if a device supports the ::STAT_INFO structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_gps_stat_info( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_STAT_INFO, &p_xdf->receiver_info );

}  // xdevfeat_has_gps_stat_info

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_gps_stat_info;



/*HDR*/
/**
 * @brief Check if a device supports the ::ANT_CABLE_LEN structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ant_cable_length( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_ANT_CABLE_LEN, &p_xdf->receiver_info );

}  // xdevfeat_has_ant_cable_length

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ant_cable_length;



/*HDR*/
/**
 * @brief Check if a device supports the ::IGNORE_LOCK structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_gps_ignore_lock( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_IGNORE_LOCK, &p_xdf->receiver_info );

}  // xdevfeat_has_gps_ignore_lock

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_gps_ignore_lock;



#if 0 //### TODO

 /*HDR*/
/**
 * @brief Check if a device supports the :: structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_, &p_xdf->receiver_info );

}  // xdevfeat_has_

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_;



 /*HDR*/
/**
 * @brief Check if a device supports the :: structure and API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_, &p_xdf->receiver_info );

}  // xdevfeat_has_

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_;

#endif  // ###


/*HDR*/
/**
 * @brief Check if the device supports the SCU_STAT structures
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 * @see @ref group_scu
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_scu_stat( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_SCU_STAT, &p_xdf->receiver_info );

} // xdevfeat_has_scu_stat

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_scu_stat;


/*HDR*/
/**
 * @brief Check if the device supports the ::SV_INFO structures
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_sv_info( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_builtin_feature( GPS_MODEL_HAS_SV_INFO, &p_xdf->receiver_info );

} // xdevfeat_has_sv_info

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_sv_info;


/*HDR*/
/**
 * @brief Check if a timecode receiver provides ::MBG_RAW_IRIG_DATA
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_raw_irig_data
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_raw_irig_data( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_RAW_IRIG_DATA, &p_xdf->receiver_info );

}  // xdevfeat_has_raw_irig_data

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_raw_irig_data;



/*HDR*/
/**
 * @brief Check if a device supports the old LAN_IP4 API
 *
 * The LAN_IP4 API provides structures and functions to configure
 * parts of the networking of a device and is superseded by the
 * NET_CFG API. Some devices combine NET_CFG and LAN_IP4.
 * Therefore, ::mbgextio_get_all_net_cfg_info should be used
 * preferably to read the network configuration.
 * It will translate the old structures into the new ones.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_net_cfg_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_lan_ip4( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_LAN_IP4, &p_xdf->receiver_info );

}  // xdevfeat_has_lan_ip4

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_lan_ip4;



/*HDR*/
/**
 * @brief Check if a device supports the new NET_CFG API
 *
 * The NET_CFG API provides structures and functions to configure
 * the complete networking part of a device and supersedes the
 * LAN_IP4 API. Not all devices support the whole feature set
 * of the NET_CFG API or combine NET_CFG and LAN_IP4.
 * Therefore, ::mbgextio_get_all_net_cfg_info should be used
 * preferably to read the network configuration.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_net_cfg_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_net_cfg( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_NET_CFG, &p_xdf->receiver_info );

}  // xdevfeat_has_net_cfg

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_net_cfg;



/*HDR*/
/**
 * @brief Check if a device supports the PTP API
 *
 * The PTP API consists of different calls and associated structures
 * which have evolved over time. Not all devices support every call,
 * so ::mbgextio_get_all_ptp_cfg_info takes care to check which parts are
 * supported and thus should be used preferably to read PTP information.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_ptp_cfg_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ptp( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_PTP, &p_xdf->receiver_info );

}  // xdevfeat_has_ptp

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ptp;


/*HDR*/
/**
 * @brief Check if a device supports the PTP next generation API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_ptp_ng_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ptp_ng( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_PTP_NG, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_ptp_ng

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ptp_ng;


/*HDR*/
/**
 * @brief Check if a device supports the sys ref API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_sys_ref_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_sys_ref( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_SYS_REF, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_sys_ref

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_sys_ref;


/*HDR*/
/**
 * @brief Check if a device supports the NTP API
 *
 * The NTP API consists of different calls and associated structures
 * which have evolved over time. Not all devices support every call,
 * so ::mbgextio_get_all_ntp_cfg_info takes care to check which parts are
 * supported and thus should be used preferably to read NTP information.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_ntp_cfg_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ntp( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_NTP, &p_xdf->receiver_info );

} // xdevfeat_has_ntp

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ntp;


/*HDR*/
/**
 * @brief Check if a device supports the FW management API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_firmware_info
 * @see ::mbgextio_get_fw_glb_info
 * @see ::mbgextio_get_fw_info_idx
 * @see ::mbgextio_get_fw_ufu_info_idx
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_fw_mngmnt( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_FW_MNGMNT, &p_xdf->xfeature_buffer );

} // xdevfeat_has_fw_mngmnt

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_fw_mngmnt;


/*HDR*/
/**
 * @brief Check if a device supports the event log API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_evt_log( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_EVT_LOG, &p_xdf->receiver_info );

} // xdevfeat_has_evt_log

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_evt_log;


/*HDR*/
/**
 * @brief Check if a device supports the user capture API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ucap( const MBG_XDEV_FEATURES *p_xdf )
{
  const RECEIVER_INFO *ri = &p_xdf->receiver_info;

  if ( ri->n_ucaps > 0 )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

} // xdevfeat_has_ucap

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ucap;


/*HDR*/
/**
 * @brief Check if a device supports the user capture via network feature
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref group_ucap_net
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ucap_net( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_UCAP_NET, &p_xdf->xfeature_buffer );

} // xdevfeat_has_ucap_net

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ucap_net;



/*HDR*/
/**
 * @brief Check if a device supports the TLV API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref group_tlv_api
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_tlv_api( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_TLV_API, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_ptp

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_tlv_api;



/*HDR*/
/**
 * @brief Check if a device supports a file request via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_req_file
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_file_req( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_FILE_REQUEST, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_file_req

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_file_req;



/*HDR*/
/**
 * @brief Check if a device supports a command execution via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_xmt_cmd_line
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_exec_cmd( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_EXEC_CMD, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_exec_cmd

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_exec_cmd;



/*HDR*/
/**
 * @brief Check if a device supports a factory reset via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_xmt_fw_rollback
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_fw_rollback( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_FW_ROLLBACK, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_fw_rollback

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_fw_rollback;



/*HDR*/
/**
 * @brief Check if a device supports a firmware update via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_xmt_fw_update
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_fw_update( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_FW_UPDATE, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_fw_update

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_fw_update;



/*HDR*/
/**
 * @brief Check if a device supports creating / sending a diagnostics file via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::TODO  //refer to get diag function
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_diag_file( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_DIAG_FILE, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_diag_file

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_diag_file;



/*HDR*/
/**
 * @brief Check if a device supports PTPv2 license infos
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_ptpv2_license( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_LICENSE_PTPV2_IDX, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_ptpv2_license

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_ptpv2_license;



/*HDR*/
/**
 * @brief Check if a device supports NTP license infos via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_ntp_license( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_LICENSE_NTP_IDX, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_ntp_license

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_ntp_license;



/*HDR*/
/**
 * @brief Check if a device supports PTPv1 License Infos via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_ptpv1_license( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_LICENSE_PTPV1_IDX, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_ptpv1_license

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_ptpv1_license;



/*HDR*/
/**
 * @brief Check if a device supports Time Monitor License infos via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_time_monitor_license( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_LICENSE_TIME_MONITOR_IDX, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_time_monitor_license

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_time_monitor_license;


/*HDR*/
/**
 * @brief Check if a device supports UFU (Unified Firmware Update) via TLV
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_tlv_feat_supp
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_supp_tlv_ufu( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_tlv_feat_supp( MBG_TLV_FEAT_TYPE_UFU, &p_xdf->tlv_info );

}  // xdevfeat_supp_tlv_ufu

XDEVFEAT_CHK_SUPP_FNC xdevfeat_supp_tlv_ufu;



/*HDR*/
/**
 * @brief Check if a device supports the ::GPS_SAVE_CFG command
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_cmd_save_cfg
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_cmd_save_cfg( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_SAVE_CFG, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_cmd_save_cfg

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_cmd_save_cfg;


/*HDR*/
/**
 * @brief Check if a device supports the ::MBG_XFEATURE_XMRS_GPIO_TIME_AND_PHASE_SUPP command
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_dev_supp_xmrs_gpio_time_and_phase
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_xmrs_gpio_time_and_phase_supp( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_XMRS_GPIO_TIME_AND_PHASE_SUPP, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_cmd_save_cfg

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_cmd_save_cfg;



/*HDR*/
/**
 * @brief Check if a device supports the extended feature monitoring
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_monitoring( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_MONITORING, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_monitoring

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_monitoring;



/*HDR*/
/**
 * @brief Check if a device supports the LED API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::TODO ###
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_led_api( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_LED_API, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_led_api

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_led_api;



/*HDR*/
/**
 * @brief Check if a device supports the LNE API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::TODO ###
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_lne_api( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_LNE_API, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_lne_api

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_lne_api;



/*HDR*/
/**
 * @brief Check if a device supports the power control API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref group_pwr_ctl_api
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_pwr_ctl_api( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_PWR_CTL_API, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_pwr_ctl_api

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_pwr_ctl_api;



/*HDR*/
/**
 * @brief Check if a device has extended system information.
 *
 * @param[in,out] p_xdf  Pointer to a valid message control structure
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref group_ext_sys_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ext_sys_info( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_EXT_SYS_INFO, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_ext_sys_info

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ext_sys_info;



/*HDR*/
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_io_ports( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_IO_PORTS, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_io_ports

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_io_ports;



/*HDR*/
/**
 * @brief Check if a device has ::MBG_XFEATURE_TRANSACTIONS
 *
 * @param[in,out] p_xdf  Pointer to a valid message control structure
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::TODO ###
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_transactions( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_TRANSACTIONS, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_transactions

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_transactions;



/*HDR*/
/**
 * @brief Check if a device has ::MBG_XFEATURE_REBOOT
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_reboot( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_REBOOT, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_reboot

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_reboot;



/*HDR*/
/**
 * @brief Check if a device has ::MBG_XFEATURE_REQ_TTM
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 * @see mbgextio_get_time
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_req_ttm( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_REQ_TTM, &p_xdf->xfeature_buffer );

} // xdevfeat_has_req_ttm

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_req_ttm;



/*HDR*/
/**
 * @brief Check if a device supports the extended multi ref features including multi instances
 *
 * The different multi ref feature and its appropriate flags have evolved over time.
 * This function only checks the currently up-to-date GPS_HAS_XMRS_MULT_INSTC flag.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see ::mbgextio_get_all_xmulti_ref_info
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_xmulti_ref( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_XMRS_MULT_INSTC, &p_xdf->receiver_info );

}  // xdevfeat_has_xmulti_ref

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_xmulti_ref;



/*HDR*/
/**
 * @brief Check if a device supports the extended binary protocol (XBP) feature
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_ri_feature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_xbp( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_ri_feature( GPS_HAS_XBP, &p_xdf->receiver_info );

}  // xdevfeat_has_xbp

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_xbp;



/*HDR*/
/**
 * @brief Check if a device supports database(s) feature
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_database( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_DATABASE, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_database

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_database;



/*HDR*/
/**
 * @brief Check if a device supports GNSS mode feature
 * All GNSS receivers support this feature by default, this is for GPS-only devices
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_gnss_mode( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_GNSS_MODE, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_gnss_mode

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_gnss_mode;



/*HDR*/
/**
 * @brief Check if a device supports the @ref group_tainted_cfg Tainted Config API
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_tainted_cfg( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_TAINTED_CFG, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_tainted_cfg

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_tainted_cfg;



/*HDR*/
/**
 * @brief Check if a device supports (un)registering for push messages
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_push_msgs( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_PUSH_MSGS, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_push_msgs

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_push_msgs;



/*HDR*/
/**
 * @brief Check if a device supports the ::MBG_CLK_RES_INFO structure
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_clk_res_info( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_CLK_RES_INFO, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_clk_res_info

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_clk_res_info;



/*HDR*/
/**
 * @brief Check if a device supports user authentification
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_user_auth( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_USER_AUTH, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_user_auth

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_user_auth;



/*HDR*/
/**
 * @brief Check if a device supports user management
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_user_mngmnt( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_USER_MNGMNT, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_user_mngmnt

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_user_mngmnt;



/*HDR*/
/**
 * @brief Check if a device supports services
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_service( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_SERVICE, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_service

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_service;



/*HDR*/
/**
 * @brief Check if a device supports the XHE I/O commands
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_xhe( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_XHE, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_xhe

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_xhe;



/*HDR*/
/**
 * @brief Check if the device has an L1 frequency up converter
 *
 * If this GNSS receiver supports this then a Meinberg antenna/converter
 * can be used instead of a standard L1 antenna.
 *
 * @param[in] p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else ::MBG_ERR_NOT_SUPP_BY_DEV
 *         or ::MBG_ERR_DEV_NOT_SUPP (see @ref xdevfeat_chk_supp_fncs)
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_up_conv( const MBG_XDEV_FEATURES *p_xdf )
{
  int rc = check_builtin_feature( GPS_MODEL_HAS_UP_CONV, &p_xdf->receiver_info );

  if ( mbg_rc_is_success( rc ) )
    goto out;

  rc = check_xfeature( MBG_XFEATURE_UP_CONV, &p_xdf->xfeature_buffer );

  if ( mbg_rc_is_success( rc ) )
    goto out;

  rc = MBG_ERR_NOT_SUPP_BY_DEV;

out:
  return rc;

} // xdevfeat_has_up_conv

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_up_conv;



/*HDR*/
/**
 * @brief Check if a device supports the FCU API.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref group_fcu_api
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_fcu_api( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_FCU_API, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_fcu_api

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_fcu_api;



/*HDR*/
/**
 * @brief Check if a device supports the RAW DAC.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref group_raw_dac
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_raw_dac( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_RAW_DAC, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_raw_dac

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_raw_dac;



/*HDR*/
/**
 * @brief Check if a device supports the PTP STATE LIST.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref group_raw_dac
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_ptp_state( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_PTP_STATE, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_ptp_state

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_ptp_state;



/*HDR*/
/**
 * @brief Check if a device supports the PMU CMD.
 *
 * @param[in]  p_xdf  Pointer to a ::MBG_XDEV_FEATURES buffer associated with a device.
 *
 * @return ::MBG_SUCCESS if supported, else error code from ::check_xfeature
 *
 * @ingroup xdevfeat_chk_supp_fncs
 * @see @ref xdevfeat_chk_supp_fncs
 */
_NO_MBG_API_ATTR int _NO_MBG_API xdevfeat_has_pmu_cmd( const MBG_XDEV_FEATURES *p_xdf )
{
  return check_xfeature( MBG_XFEATURE_PMU_CMD, &p_xdf->xfeature_buffer );

}  // xdevfeat_has_pmu_cmd

XDEVFEAT_CHK_SUPP_FNC xdevfeat_has_pmu_cmd;



