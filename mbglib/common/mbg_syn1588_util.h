
/**************************************************************************
 *
 *  $Id: mbg_syn1588_util.h 1.5 2023/08/02 10:07:50 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and prototypes for mbg_syn1588_util.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_syn1588_util.h $
 *  Revision 1.5  2023/08/02 10:07:50  martin.burnicki
 *  Fixes in SYN1588 debug code.
 *  Revision 1.4  2022/12/21 15:22:49  martin.burnicki
 *  Quieted some potential compiler warnings.
 *  Revision 1.3  2022/07/19 16:05:17  martin.burnicki
 *  Renamed some symbols to match a more common naming convention.
 *  Fixed a spelling error in a debug message.
 *  Revision 1.2  2022/07/06 15:09:08  martin.burnicki
 *  Support SYN1588  insync range boundary code instead of the original enum.
 *  Revision 1.1  2022/05/31 13:41:41  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_SYN1588_UTIL_H
#define _MBG_SYN1588_UTIL_H

/* Other headers to be included */

#include <mbg_syn1588_defs.h>

#include <mbg_tgt.h>
#include <pcpsdefs.h>
#include <mbgtime.h>
#include <mbgerror.h>
#include <cnv_wday.h>
#include <timeutil.h>

#include <stdio.h>


#if !defined( PRINT_INSYNC_BOUNDARY_FRAC )
  // If not 0, fractions of insync boundary values are
  // always printed, even if they are 0.
  // Useful for debugging, but output is prettier
  // without this.
  #define PRINT_INSYNC_BOUNDARY_FRAC  0
#endif

#if !defined( DEBUG_INSYNC_BOUNDARY )
  // If not 0, print intermediate values to stdout.
  // Just for debugging, should be 0 otherwise.
  #define DEBUG_INSYNC_BOUNDARY      0
#endif

#if !defined( OMIT_INSYNC_BOUNDARY_FRAC )
  // If not 0, print values * 10, without fractions.
  // Just for debugging, should be 0 otherwise.
  #define OMIT_INSYNC_BOUNDARY_FRAC  0
#endif


#ifdef _MBG_SYN1588_UTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


static __mbg_inline /*HDR*/
/**
 * @brief Check if a PTP sync status word is valid.
 *
 * This is usually the case if the PTP stack is runnung.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  @a true, if the status word is valid, else @a false.
 */
bool syn1588_ptp_sync_status_valid( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( ptp_sync_status & DEF_PSS_BIT_VALID ) != 0;

}  // syn1588_ptp_sync_status_valid



static __mbg_inline /*HDR*/
/**
 * @brief Decode the PTP stack state from the sync status word.
 *
 * The function ::syn1588_ptp_sync_status_valid can be used to
 * determine if the decoded state is valid.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  The decoded state of the PTP stack, see ::SYN1588_PTP_STATES.
 */
uint syn1588_ptp_state_from_sync_status( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( ptp_sync_status & DEF_PSS_BITMASK_PTP_STATE ) >> DEF_PSS_SHIFT_PTP_STATE;

}  // syn1588_ptp_state_from_sync_status



static __mbg_inline /*HDR*/
/**
 * @brief Check if the PTP stack is in sync.
 *
 * This is the case if the determined time offset is inside
 * a specified boundary. See ::syn1588_get_insync_boundary_code.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  @a true, if in sync, else @a false.
 *
 * @see ::syn1588_get_insync_boundary_code
 */
bool syn1588_ptp_in_sync( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( ptp_sync_status & DEF_PSS_BIT_PTP_INSYNC ) != 0;

}  // syn1588_ptp_in_sync



static __mbg_inline /*HDR*/
/**
 * @brief Check if the PTP stack is in slave mode and in sync.
 *
 * This is useful to check if the device is a synchronized PTP slave,
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  @a true, if in sync, else @a false.
 *
 * @see ::syn1588_ptp_state_from_sync_status
 * @see ::syn1588_ptp_in_sync
 */
bool syn1588_ptp_slave_in_sync( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( syn1588_ptp_state_from_sync_status( ptp_sync_status ) == SYN1588_PTP_STATE_SLAVE )
         && syn1588_ptp_in_sync( ptp_sync_status );

}  // syn1588_ptp_slave_in_sync



static __mbg_inline /*HDR*/
/**
 * @brief Decode the PTP in sync boundary from the sync status word.
 *
 * The function ::syn1588_ptp_sync_status_valid can be used to
 * determine if the decoded number is valid.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  The decoded in sync boundary. TODO Units?
 *
 * @see ::syn1588_ptp_in_sync
 */
uint syn1588_get_insync_boundary_code( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( ptp_sync_status & DEF_PSS_BITMASK_PTP_INSYNC_BND_CODE ) >> DEF_PSS_SHIFT_PTP_INSYNC_BND_CODE;

}  // syn1588_get_insync_boundary_code



static __mbg_inline /*HDR*/
/**
 * @brief Check if the PTP stack uses the PTP (TAI) time scale.
 *
 * If this is @a true, the original timestamp is TAI, and
 * can be converted to %UTC if ::syn1588_sync_status_utc_valid.
 *
 * Unlike ::syn1588_is_timescale_ptp, this function also checks
 * if the ::PTP_SYNC_STATUS is valid at all. This is faster if
 * we need to check the status, and execution time matters.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  @a true, if in sync, else @a false.
 *
 * @see ::syn1588_is_timescale_ptp
 * @see ::syn1588_sync_status_utc_valid
 */
bool syn1588_is_valid_timescale_ptp( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  // The flags to test to see if the timescale is TAI.
  SYN1588_PTP_SYNC_STATUS flags = DEF_PSS_BIT_VALID | DEF_PSS_BIT_PTP_TIMESCALE;

  return ( ptp_sync_status & flags ) == flags;

}  // syn1588_is_valid_timescale_ptp



static __mbg_inline /*HDR*/
/**
 * @brief Check if the PTP stack uses the PTP (TAI) time scale.
 *
 * If this is @a true, the original timestamp is TAI, and
 * can be converted to %UTC if ::syn1588_sync_status_utc_valid.
 *
 * Unlike ::syn1588_is_valid_timescale_ptp, this function only
 * checks the ::DEF_PSS_BIT_PTP_TIMESCALE bit, but dosn't check
 * if the ::PTP_SYNC_STATUS is valid at all. This can be helpful
 * for detailed inspection of the status.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  @a true, if in sync, else @a false.
 *
 * @see ::syn1588_is_valid_timescale_ptp
 * @see ::syn1588_sync_status_utc_valid
 */
bool syn1588_is_timescale_ptp( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( ptp_sync_status & DEF_PSS_BIT_PTP_TIMESCALE ) != 0;

}  // syn1588_is_timescale_ptp



static __mbg_inline /*HDR*/
/**
 * @brief Check if the %UTC offset field is valid.
 *
 * The function ::syn1588_utc_offs_from_sync_status can be used
 * to decode the current %UTC/TAI offset from the PTP sync status word.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  @a true, if valid, else @a false.
 *
 * @see ::syn1588_utc_offs_from_sync_status
 */
bool syn1588_sync_status_utc_valid( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( ptp_sync_status & DEF_PSS_BIT_UTC_OFFSET_VALID ) != 0;

}  // syn1588_sync_status_utc_valid



static __mbg_inline /*HDR*/
/**
 * @brief Decode the %UTC/TAI offset from the sync status word.
 *
 * The function ::syn1588_sync_status_utc_valid can be used to
 * determine if the decoded offset is valid.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  The decoded %UTC/TAI offset, in seconds.
 *
 * @see ::syn1588_sync_status_utc_valid
 */
int syn1588_utc_offs_from_sync_status( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return (int) ( ptp_sync_status & DEF_PSS_BITMASK_UTC_OFFSET );  // TODO Is this always unsigned?

}  // syn1588_utc_offs_from_sync_status



static __mbg_inline /*HDR*/
/**
 * @brief Check if the @a lSync program is running.
 *
 * This function returns @a true if the @a lSync program is running.
 *
 * On Windows, device access needs to be serialized in a multitasking
 * or multithreaded environment, which is done using a semaphore
 * implemented in the mbgdevio DLL.
 *
 * The @a lSync program does not use the DLL and thus does not use the semaphore,
 * so concurrent access can result in inconsistent data being returned.
 *
 * A programs that is aware of this problem can display a warning
 * when it detects that the @a lSync program is running, too.
 *
 * On Linux, a different API to the kernel driver is used, so there is
 * need to care about serialization, and no need to display a warning
 * if @a lSync is running.
 *
 * @param[in]  ptp_sync_status  The sync status word read from a device.
 *
 * @return  @a true, if the status reflects tha @ lSync is running, else @a false.
 */
bool syn1588_lsync_running( SYN1588_PTP_SYNC_STATUS ptp_sync_status )
{
  return ( ptp_sync_status & DEF_PSS_BIT_LSYNC_RUNNING ) != 0;

}  // syn1588_lsync_running



static  __mbg_inline /*HDR*/
/**
 * @brief Get the exponent from an insync range boundary code.
 *
 * @param[in]  code  The insync range boundary code to be decoded, see ::PTP_STATE_RANGE_TABLE_INIT.
 *
 * @return The decoded exponent, scaled to shift results to a useful range.
 *
 * @see ::syn1588_insync_boundary_mantissa
 * @see ::mbg_syn1588_snprint_insync_boundary_code
 */
int syn1588_insync_boundary_exponent( int code )
{
  return ( code >> 2 ) - 11;

}  // syn1588_insync_boundary_exponent



static  __mbg_inline /*HDR*/
/**
 * @brief Get the mantissa from an insync range boundary code.
 *
 * The decoded mantissa still needs special handling on range boundaries,
 * see implementation in ::mbg_syn1588_snprint_insync_boundary_code.
 *
 * @param[in]  code  The insync range boundary code to be decoded, see ::PTP_STATE_RANGE_TABLE_INIT.
 *
 * @return The decoded mantissa.
 *
 * @see ::syn1588_insync_boundary_mantissa
 * @see ::mbg_syn1588_snprint_insync_boundary_code
 */
int syn1588_insync_boundary_mantissa( int code )
{
  return code & 0x03;

}  // syn1588_insync_boundary_mantissa



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Retrieve a string representing the state of the PTP stack.
 *
 * @param[in]  state  The state of the PTP stack, see ::SYN1588_PTP_STATES.
 *
 * @return One of the strings defined in ::SYN1588_PTP_STATE_STRS.
 */
 const char *mbg_syn1588_chk_get_ptp_state_str( uint state ) ;

 /**
 * @brief Write the decoded value of an insync range boundary code to a string buffer.
 *
 * @param[out]  s          Pointer to the output buffer.
 * @param[in]   max_len    Size of the output buffer.
 * @param[in]   code       The insync range boundary code, see ::PTP_STATE_RANGE_TABLE_INIT.
 * @param[in]   check_max  If @a true, ::PTP_STATE_RANGE_MAX_CODE causes special string.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 int mbg_syn1588_snprint_insync_boundary_code( char *s, size_t max_len, uint code, bool check_max ) ;


/* ----- function prototypes end ----- */


static __mbg_inline /*HDR*/
/**
 * @brief Determine the time scale from the status of a SYN1588 clock.
 *
 * Check the flags and bit fields in @p ptp_sync_status to see if %UTC
 * can be unambiguously derived from the timestamp.
 *
 * Only if the timescale is PTP/TAI (and not arbitrary), we know that
 * the original timestamp refers to TAI, and only if the %UTC offset
 * is valid, too, we can convert the original TAI timestamp to %UTC.
 *
 * The determined %UTC/TAI offset is returned, and the the status
 * @p *p_st is updated accordingly.
 *
 * @param[in]      ptp_sync_status  The sync status flags read from a SYN1588 device.
 * @param[in,out]  p_st             Address of a ::PCPS_TIME_STATUS_X variable to be updated, or @a NULL.
 *
 * @return  The %UTC offset, if it can be determined, else 0.
 */
int syn1588_check_timescale( SYN1588_PTP_SYNC_STATUS ptp_sync_status, PCPS_TIME_STATUS_X *p_st )
{
  //  First check if the timestamps refer to the PTP/TAI time scale.
  if ( syn1588_is_valid_timescale_ptp( ptp_sync_status ) )
  {
    // If the UTC offset is also valid, we can determine UTC.
    if ( syn1588_sync_status_utc_valid( ptp_sync_status ) )
    {
      if ( p_st )
        *p_st |= PCPS_UTC;

      return syn1588_utc_offs_from_sync_status( ptp_sync_status );
    }

    if ( p_st )  // UTC offset is not available.
      *p_st |= PCPS_SCALE_TAI;
  }

  return 0;

}  // syn1588_check_timescale



static __mbg_inline /*HDR*/
/**
 * @brief Evaluate the status assiociated with a timestamp from a SYN1588 clock.
 *
 * Update the ::PCPS_HR_TIME structure @p *p accordingly.
 *
 * @param[in]      ptp_sync_status  The sync status flags read from a SYN1588 device.
 * @param[in,out]  p                Address of a ::PCPS_HR_TIME structure to be updated.
 */
void syn1588_decode_sync_status( SYN1588_PTP_SYNC_STATUS ptp_sync_status, PCPS_HR_TIME *p )
{
  PCPS_TIME_STATUS_X status_x = PCPS_INVT | PCPS_FREER;
  PCPS_SIG_VAL sig_val = PCPS_SIG_LVL_SIG_NOT_AVAIL;

  if ( !syn1588_ptp_sync_status_valid( ptp_sync_status ) )  // Status not available.
  {
    #if DEBUG_SYN1588_C_API
      fprintf( stderr, "syn1588 PTP sync status not available.\n" );
    #endif
    goto out;
  }

  #if DEBUG_SYN1588_C_API
  {
    char s[128];
    bool ptp_sync_status_valid = syn1588_ptp_sync_status_valid( ptp_sync_status );

    int utc_tai_offs = syn1588_utc_offs_from_sync_status( ptp_sync_status );
    bool utc_tai_offs_valid = syn1588_sync_status_utc_valid( ptp_sync_status );

    bool leap_59 = ( ptp_sync_status & DEF_PSS_BIT_LEAP59 ) != 0;
    bool leap_61 = ( ptp_sync_status & DEF_PSS_BIT_LEAP61 ) != 0;

    uint32_t ptp_state = syn1588_ptp_state_from_sync_status( ptp_sync_status );

    bool ptp_in_sync = syn1588_ptp_in_sync( ptp_sync_status );
    uint32_t insync_boundary_code = syn1588_get_insync_boundary_code( ptp_sync_status );

    bool timescale_is_ptp = ( ptp_sync_status & DEF_PSS_BIT_PTP_TIMESCALE ) != 0;

    bool lsync_running = syn1588_lsync_running( ptp_sync_status );

    mbg_syn1588_snprint_insync_boundary_code( s, sizeof( s ), insync_boundary_code, true );

    fprintf( stderr, "  PTP sync status: %svalid (0x%08lX)\n", ptp_sync_status_valid ? "" : "NOT ",
                        (ulong) ptp_sync_status );
    fprintf( stderr, "  Time scale: %s\n", timescale_is_ptp ? "PTP/TAI" : "arbitrary" );
    fprintf( stderr, "  UTC/TAI offs valid: %i, offs: %i (0x%04X) s,\n", utc_tai_offs_valid,
                        utc_tai_offs, utc_tai_offs );
    fprintf( stderr, "  Leap 59: %i, Leap 61: %i\n", leap_59, leap_61 );
    fprintf( stderr, "  PTP State:  0x%08X (%s), in sync: %i, boundary: %u (%s)\n", ptp_state,
                        mbg_syn1588_chk_get_ptp_state_str( ptp_state ),
                        ptp_in_sync, insync_boundary_code, s );
    fprintf( stderr, "  lSync running: %s\n\n", lsync_running ? "Yes" : "No" );
  }
  #endif

  // The PTP270PEX card checks if a network link is available
  // to report if a signal is available. The SYN1588
  // device can only synchronize if the PTP stack is running
  // and has updated the PTP_SYNC_STATUS register.
  // This is the case if we get here.
  sig_val = PCPS_SIG_LVL_SIG_AVAIL;

  // Set the initial status depending on whether the device is synchronized,
  // or not.
  //
  // Unfortunately, ::syn1588_ptp_in_sync() can also be set if the PTP stack
  // is in master mode, even if the on-board time is not disciplined
  // from a reference time source. So we only consider the device to
  // be synchronized if the stack is also in slave state.
  if ( syn1588_ptp_state_from_sync_status( ptp_sync_status ) == SYN1588_PTP_STATE_SLAVE )
    status_x = syn1588_ptp_in_sync( ptp_sync_status ) ? PCPS_SYNCD : PCPS_FREER;

  // Check the time scale and adjust the time stamp accordingly.
  p->tstamp.sec -= syn1588_check_timescale( ptp_sync_status, &status_x );

  // Evaluate the leap second status. Actually, leap second warnings
  // should only be available if the time scale is UTC or local time.
  // We assume that the stack takes care about this.
  if ( ptp_sync_status & DEF_PSS_BIT_LEAP61 )
  {
    if ( ptp_sync_status & DEF_PSS_BIT_LEAP59 )
      status_x |= PCPS_INVT;               // Invalid state: both announcements set.
    else
      status_x |= PCPS_LS_ANN;                    // Positive leap second announced.
  }
  else
    if ( ptp_sync_status & DEF_PSS_BIT_LEAP59 )
      status_x |= PCPS_LS_ANN | PCPS_LS_ANN_NEG;  // Negative leap second announced.

out:
  p->status = status_x;
  p->signal = sig_val;
  p->utc_offs = 0;  // Always 0 here because it refers to UTC/Local time, not UTC/TAI.

}  // syn1588_decode_sync_status



static __mbg_inline /*HDR*/
int pcps_time_from_pcps_hr_time( PCPS_TIME *p, const PCPS_HR_TIME *p_ht, int64_t nsec )
{
  struct tm tm = { 0 };
  MBG_TIME64_T t64 = p_ht->tstamp.sec;  // Will need extrapolation for Y2038.
  int rc = mbg_gmtime64( &tm, &t64 );

  if ( mbg_rc_is_error( rc ) )
    return rc;

  // Save the computed calendar date and time.
  p->sec = (uint8_t) tm.tm_sec;
  p->min = (uint8_t) tm.tm_min;
  p->hour = (uint8_t) tm.tm_hour;
  p->mday = (uint8_t) tm.tm_mday;
  p->wday = (uint8_t) _wday_sun06_to_mon17( tm.tm_wday );
  p->month = (uint8_t) ( tm.tm_mon + 1 );
  p->year = (uint8_t) ( tm.tm_year % 100 );

  // Convert fractions of the second.
  p->sec100 = (uint8_t) ( nsec / ( NSEC_PER_SEC / 100 ) );

  // Convert the remaining fields.
  p->status = (PCPS_TIME_STATUS) p_ht->status;
  p->signal = p_ht->signal;
  p->offs_utc = (int8_t) ( p_ht->utc_offs / SECS_PER_HOUR );

  return MBG_SUCCESS;

}  // pcps_time_from_pcps_hr_time



#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_SYN1588_UTIL_H */
