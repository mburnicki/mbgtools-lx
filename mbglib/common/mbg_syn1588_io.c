
/**************************************************************************
 *
 *  $Id: mbg_syn1588_io.c 1.14 2023/08/02 10:07:41 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    I/O functions for the SYN1588 PTP NIC.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_syn1588_io.c $
 *  Revision 1.14  2023/08/02 10:07:41  martin.burnicki
 *  Fixes in SYN1588 debug code.
 *  Revision 1.13  2023/05/03 16:49:27  martin.burnicki
 *  Fixed some format string compiler warnings.
 *  Revision 1.12  2023/02/21 10:15:34  martin.burnicki
 *  Fixed a potential compiler warning.
 *  Revision 1.11  2022/09/07 15:39:06  martin.burnicki
 *  Fixed some compiler warnings in debug code.
 *  Revision 1.10  2022/09/01 14:56:18Z  martin.burnicki
 *  Fix build in case mbgdevio is statically linked.
 *  Revision 1.9  2022/06/01 19:59:31Z  martin.burnicki
 *  Removed inappropriate function attributes from some functions.
 *  Revision 1.8  2022/05/31 13:09:30Z  martin.burnicki
 *  Support SYN1588 mutex on Windows.
 *  Revision 1.7  2022/05/25 09:52:35Z  martin.burnicki
 *  Refactured low level functions.
 *  Revision 1.6  2021/12/01 19:46:23  martin.burnicki
 *  New functions mbg_syn1588_get_time_info_hrt()
 *  and mbg_syn1588_get_time_info_tstamp().
 *  Revision 1.5  2021/11/08 21:15:15  martin.burnicki
 *  New function mbg_syn1588_get_ptp_sync_status().
 *  New functions mbg_syn1588_find_devices(),
 *  mbg_syn1588_chk_dev_fn_is_valid(), and
 *  mbg_syn1588_dev_fn_from_dev_idx to clean up
 *  and improve device handling.
 *  Revision 1.4  2021/10/26 15:45:47  martin
 *  Increased the number of supported SYN1588 devices,
 *  and modified a few comments and debug messages.
 *  Revision 1.3  2021/10/19 07:27:16Z  martin
 *  Improved snprintf_sernum().
 *  Revision 1.2  2021/09/30 14:02:10Z  martin
 *  Collapsed development branch.
 *  Works stable but gets status only from SHM.
 *  Revision 1.1  2019/05/03 10:37:50  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _MBG_SYN1588_IO
  #include <mbg_syn1588_io.h>
#undef _MBG_SYN1588_IO

#include <mbg_syn1588_util.h>
#include <mbgerror.h>
#include <str_util.h>
#include <cnv_wday.h>

#include <mbgdevio.h>

#if defined( MBG_TGT_WIN32 )
  #include <mbgsvctl.h>
#endif

#if !defined( EXCLUDE_SYN1588_ACCESS )
  #define EXCLUDE_SYN1588_ACCESS  0
#endif

#if !defined( DEBUG_SYN1588_OPEN_CLOSE )
  #define DEBUG_SYN1588_OPEN_CLOSE  ( 1 && defined( _DEBUG ) )
#endif

#if !defined( DEBUG_SYN1588_DETECTION )
  #define DEBUG_SYN1588_DETECTION   0
#endif

#if !defined( SUPP_SYN1588_USR_SPC_TESTS )
  #if SUPP_SYN1588_USR_SPC && ( defined( DEBUG ) || defined( _DEBUG ) ) && \
      ( !defined( MBG_TGT_WIN32 ) || defined( MBGSVCTL_STATIC_LIB ) )
    // If we build/use DLLs, linking can become pretty tricky
    // if the debug code is compiled in.
    #define SUPP_SYN1588_USR_SPC_TESTS   0  // 1 or 0, as appropriate
  #else
    #define SUPP_SYN1588_USR_SPC_TESTS   0  // always 0
  #endif
#endif



// At present, SYN1588 devices can only be distinguished
// through different index numbers that are associated
// with the same real file handle.
//
// We therefore use a pseudo device file name to distinguish
// SYN1588 devices from Meinberg devices, whose names can also
// consist of a device file name plus an index.

#define MBG_SYN1588_DEV_FN_BASE  "syn1588nic"
#define MBG_SYN1588_DEV_FN_FMT   MBG_SYN1588_DEV_FN_BASE "%i"

const char mbg_syn1588_dev_fn_base[] = MBG_SYN1588_DEV_FN_BASE;

const char mbg_syn1588_dev_fn_fmt[] = MBG_SYN1588_DEV_FN_FMT;



#if SUPP_SYN1588_USR_SPC_CPP

#define SYN1588_INVALID_DEV_IDX  ( (SYN1588_DEV_IDX) (-1) )


typedef struct
{
  ulong use_count;
  SYN1588_DEV_IDX dev_idx;
  MBG_DEV_HANDLE dh;

} MBG_SYN1588_DEV_INFO;



#define N_MBG_SYN1588_DEV  N_SUPP_DEV_BUS
static MBG_SYN1588_DEV_INFO syn1588_dev_info[N_MBG_SYN1588_DEV];


#endif  // SUPP_SYN1588_USR_SPC_CPP



#if SUPP_SYN1588_USR_SPC_CPP

static /*HDR*/
void check_api_initialized( void )
{
  static bool api_initialized;

  if ( !api_initialized )
  {
    syn1588_init_c_api( 0 );  // 0 is log level
    api_initialized = true;
  }

}  // check_api_initialized

#endif



static __mbg_inline /*HDR*/
/**
 * @brief find a device info slot for a device.
 *
 * Return either the address of a used slot with the
 * same device index, or the address of an unused slot.
 */
MBG_SYN1588_DEV_INFO *mbg_syn1588_find_dev_info_slot( SYN1588_DEV_IDX dev_idx )
{
  int i;

  // First loop over the slots and look for a used one
  // with the same device index.
  for ( i = 0; i < N_MBG_SYN1588_DEV; i++ )
  {
    MBG_SYN1588_DEV_INFO *p = &syn1588_dev_info[i];

    if ( p->use_count && ( p->dev_idx == dev_idx ) )
      return p;
  }

  // If no used slot was found, try to find an empty one.
  for ( i = 0; i < N_MBG_SYN1588_DEV; i++ )
  {
    MBG_SYN1588_DEV_INFO *p = &syn1588_dev_info[i];

    if ( p->use_count == 0 )
      return p;
  }

  return NULL;

}  // mbg_syn1588_find_dev_info_slot



static __mbg_inline /*HDR*/
void mbg_syn1588_set_dev_info( MBG_SYN1588_DEV_INFO *p, MBG_DEV_HANDLE dh,
                               SYN1588_DEV_IDX dev_idx )
{
  p->dh = dh;
  p->dev_idx = dev_idx;

}  // mbg_syn1588_set_dev_info



static __mbg_inline /*HDR*/
MBG_SYN1588_DEV_INFO *mbg_syn1588_find_dev_info( MBG_DEV_HANDLE dh )
{
  int i;

  for ( i = 0; i < N_MBG_SYN1588_DEV; i++ )
  {
    MBG_SYN1588_DEV_INFO *p = &syn1588_dev_info[i];

    if ( p->use_count == 0 )
      continue;

    if ( p->dh == dh )
      return p;
  }

  return NULL;

}  // mbg_syn1588_find_dev_info



/*HDR*/
/**
 * @brief Retrieve the device index associated with a device handle.
 *
 * @param[in]  dh  The device handle to be checked.
 *
 * @return  On success, a valid device index, else ::SYN1588_INVALID_DEV_IDX.
 *
 * @see ::is_mbg_syn1588_type
 */
SYN1588_DEV_IDX mbg_syn1588_get_dev_idx( MBG_DEV_HANDLE dh )
{
  const MBG_SYN1588_DEV_INFO *p = mbg_syn1588_find_dev_info( dh );

  if ( p )
    return p->dev_idx;

  return SYN1588_INVALID_DEV_IDX;

}  // mbg_syn1588_get_dev_idx



/*HDR*/
/**
 * @brief Check if a device handle is associated with a SYN1588 device.
 *
 * This function checks if a specified handle can be found in a
 * list of device handles associated with SYN1588 devices.
 *
 * It may take some time to scan the the list, so the function
 * ::chk_if_syn1588_type should be used preferably, if possible,
 * because that function scans the list only if any SYN1588 device
 * has been detected.
 *
 * @param[in]  dh  The device handle to be checked.
 *
 * @return true, if the device is a SYN1588 type, else false.
 *
 * @see ::chk_if_syn1588_type
 */
bool is_mbg_syn1588_type( MBG_DEV_HANDLE dh )
{
  return mbg_syn1588_find_dev_info( dh ) != NULL;

}  // is_mbg_syn1588_type



/*HDR*/
/**
 * @brief Check if a string is a valid SYN1588 pseudo device file name.
 *
 * @param[in]  s  The string to be checked.
 *
 * @return ::MBG_SUCCESS if @p s is a valid device file name, else ::MBG_ERR_INV_PARM.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
int mbg_syn1588_chk_dev_fn_is_valid( const char *s )
{
  if ( strncmp( s, mbg_syn1588_dev_fn_base, strlen( mbg_syn1588_dev_fn_base ) ) == 0 )
    return MBG_SUCCESS;

  return MBG_ERR_INV_PARM;

}  // mbg_syn1588_chk_dev_fn_is_valid



/*HDR*/
int mbg_syn1588_dev_fn_from_dev_idx( char *s, int max_len, int dev_idx )
{
  int n = 0;

  s[n] = 0;  // Make string empty.

  #if MBG_TGT_HAS_DEV_FN_BASE  // FIXME TODO Move this!
    n += snprintf_safe( &s[n], max_len - n, mbg_syn1588_dev_fn_fmt, dev_idx );
  #endif

  return n;

}  // mbg_syn1588_dev_fn_from_dev_idx



static /*HDR*/
int mbg_syn1588_create_device_name( SYN1588_DEV_IDX dev_idx, MBG_DEV_FN *p_dev_fn )
{
  // On Windows, the device name used to open a file handle
  // is always the same, so we append the device index to the
  // base name.
  return snprintf_safe( p_dev_fn->s, sizeof( *p_dev_fn ),
                        mbg_syn1588_dev_fn_fmt, dev_idx );

}  // mbg_syn1588_create_device_name



/*HDR*/
void mbg_syn1588_close_device( MBG_DEV_HANDLE *p_dh )
{
  if ( *p_dh == MBG_INVALID_DEV_HANDLE )
  {
    #if DEBUG_SYN1588_OPEN_CLOSE
      fprintf( stderr, "%s: file handle already invalid\n", __func__ );
    #endif
    goto out;
  }

  #if SUPP_SYN1588_USR_SPC_CPP
  {
    MBG_SYN1588_DEV_INFO *p = mbg_syn1588_find_dev_info( *p_dh );

    if ( p == NULL )
    {
      #if DEBUG_SYN1588_OPEN_CLOSE
        fprintf( stderr, "%s: no existing device slot found for file handle 0x%" PRIx64 "\n",
                 __func__, (uint64_t) *p_dh );
      #endif
      goto out;
    }

    if ( p->use_count == 0 )
    {
      #if DEBUG_SYN1588_OPEN_CLOSE
        fprintf( stderr, "%s: use count already 0 for file handle 0x%" PRIx64 ", dev idx %i\n",
                 __func__, (uint64_t) *p_dh, p->dev_idx );
      #endif

      goto out;
    }

    --p->use_count;

    if ( p->use_count == 0 )
    {
      #if DEBUG_SYN1588_OPEN_CLOSE
        fprintf( stderr, "%s: use count decreased to 0, closing device with idx %i, handle 0x%" PRIx64 "\n\n",
                 __func__, p->dev_idx, (uint64_t) p->dh );
      #endif
      syn1588_close( p->dev_idx );
      mbg_syn1588_set_dev_info( p, MBG_INVALID_DEV_HANDLE,
                                SYN1588_INVALID_DEV_IDX );
      goto out;
    }

    #if DEBUG_SYN1588_OPEN_CLOSE
      fprintf( stderr, "%s: decreased use count is %lu, not closing device with idx %i, handle 0x%" PRIx64 "\n",
               __func__, p->use_count, p->dev_idx, (uint64_t) p->dh );
    #endif
  }
  #else
    // FIXME TODO is this sufficient?
    CloseHandle( dh );
    *p_dh = MBG_INVALID_DEV_HANDLE;
  #endif

out:
  *p_dh = MBG_INVALID_DEV_HANDLE;

}  // mbg_syn1588_close_device



/*HDR*/
int mbg_syn1588_open_device( SYN1588_DEV_IDX dev_idx, MBG_DEV_HANDLE *p_dh )
{
  MBG_DEV_HANDLE dh = MBG_INVALID_DEV_HANDLE;
  int rc = MBG_ERR_NO_DEV;

  #if SUPP_SYN1588_USR_SPC_CPP
    SYN1588_RET_VAL o_rc;
    MBG_SYN1588_DEV_INFO *p = mbg_syn1588_find_dev_info_slot( dev_idx );

    if ( p == NULL )
    {
      #if DEBUG_SYN1588_OPEN_CLOSE
        fprintf( stderr, "%s: no device slot found for idx %i\n\n", __func__, dev_idx );
      #endif
      goto out;
    }

    // This has to be called at least once after program startup.
    check_api_initialized();

    if ( p->use_count == 0 )
    {
      // Try to open device with specified index.
      o_rc = syn1588_init( dev_idx );

      if ( o_rc != ENoErr )  // Failed to open device.
      {
        #if DEBUG_SYN1588_OPEN_CLOSE
          fprintf( stderr, "%s: syn1588_init failed for idx %i\n\n",
                   __func__, dev_idx );
        #endif
        goto out;
      }

      dh = syn1588_get_handle( dev_idx );
      mbg_syn1588_set_dev_info( p, dh, dev_idx );
      p->use_count++;

      #if DEBUG_SYN1588_OPEN_CLOSE
        fprintf( stderr, "%s: opened device idx %i, new handle 0x%" PRIx64 ", new use count: %li\n",
                 __func__, p->dev_idx, (uint64_t) p->dh, p->use_count );
      #endif

      #if SUPP_SYN1588_USR_SPC_TESTS
      {
        static bool has_run_tests;

        if ( !has_run_tests )
        {
          mbg_syn1588_tests( dh );
          has_run_tests = true;
        }
      }
      #endif

      rc = MBG_SUCCESS;
      goto out;
    }

    dh = p->dh;
    p->use_count++;

    #if DEBUG_SYN1588_OPEN_CLOSE
      fprintf( stderr, "%s: re-using handle 0x%" PRIx64 " for idx %i, new use count: %li\n",
              __func__, (uint64_t) p->dh, p->dev_idx, p->use_count );
    #endif

    rc = MBG_SUCCESS;

  #else
    // The C++ implementations finally just end up in syn1588_impl.cpp
    // for Windows where the code below is executed to open a device:

    dh = CreateFileA( syn1588_dev_fn_base, GENERIC_READ | GENERIC_WRITE,
                      FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
                      OPEN_EXISTING, 0, NULL );

    if ( dh == MBG_INVALID_DEV_HANDLE )
    {
      rc = mbg_get_last_error( NULL );
      printf( "Error: %s\n", mbg_strerror( ret_val ) );
      goto out;
    }

    // FIXME An IOCTL call is required here
    // to write the card index to the driver.
  #endif

out:
  *p_dh = dh;

  return rc;

}  // mbg_syn1588_open_device



/*HDR*/
int mbg_syn1588_open_device_by_dev_fn( const char *dev_fn, MBG_DEV_HANDLE *p_dh )
{
  MBG_DEV_HANDLE dh = MBG_INVALID_DEV_HANDLE;
  int rc = MBG_ERR_NO_DEV;
  size_t l;
  SYN1588_DEV_IDX dev_idx;

  if ( strstr( dev_fn, mbg_syn1588_dev_fn_base ) == NULL )  // Not a SYN1588 device.
    goto out;

  l = strlen( dev_fn );
  dev_idx = atoi( &dev_fn[l - 1] );
  rc = mbg_syn1588_open_device( dev_idx, &dh );

out:
  *p_dh = dh;
  return rc;

}  // mbg_syn1588_open_device_by_dev_fn



/*HDR*/
int mbg_syn1588_find_pnp_device( SYN1588_DEV_IDX dev_idx, MBG_DEV_FN *p_dev_fn )
{
  MBG_DEV_HANDLE dh;
  int rc;

  // Admin permissions are currently required
  // to be able to open the device successfully.

  rc = mbg_syn1588_open_device( dev_idx, &dh );

  if ( mbg_rc_is_success( rc ) )
  {
    if ( p_dev_fn )
      mbg_syn1588_create_device_name( dev_idx, p_dev_fn );

    mbg_syn1588_close_device( &dh );
  }

  return rc;

}  // mbg_syn1588_find_pnp_device



/*HDR*/
int mbg_syn1588_find_devices( int max_devs, MBG_DEV_FN dev_fn_array[] )
{
  int n_devs = 0;
  SYN1588_DEV_IDX dev_idx;
  MBG_DEV_FN *p_dev_fn = NULL;

  for ( dev_idx = 0; dev_idx < N_MBG_SYN1588_DEV; dev_idx++ )
  {
    int rc;

    if ( dev_fn_array )
      p_dev_fn = &dev_fn_array[dev_idx];

    rc = mbg_syn1588_find_pnp_device( dev_idx, p_dev_fn );

    if ( mbg_rc_is_error( rc ) )
    {
      // TODO Maybe we should print an error message here,
      // at least in debug mode, or "permission denied".
      #if DEBUG_SYN1588_DETECTION
        printf( "SYN1588 device #%i not found: %s\n", dev_idx, mbg_strerror( rc ) );
      #endif
      break;
    }

    #if DEBUG_SYN1588_DETECTION
      printf( "SYN1588 device #%i found!\n", dev_idx );
    #endif

    if ( ++n_devs >= max_devs )
      break;
  }

  syn1588_devices_found = n_devs;

  #if DEBUG_SYN1588_DETECTION
    printf( "SYN1588 devices found: %i\n", syn1588_devices_found );
  #endif

  return n_devs;

}  // mbg_syn1588_find_devices



static /*HDR*/
int snprintf_sernum( char *s, size_t max_len, MBG_DEV_HANDLE dh )
{
  // Required buffer size is 17, see description of getEEPROMSerialNumber().
  // Unfortunately there is neither a safe type, nor a defined constant.
  char ser_num[17] = { 0 };
  SYN1588_RET_VAL o_rc;
  int n;
  char *cp;
  SYN1588_DEV_IDX dev_idx = mbg_syn1588_get_dev_idx( dh );

  if ( dev_idx == SYN1588_INVALID_DEV_IDX )
  {
    // Device index could not be determined.
    n = snprintf_safe( s, max_len, "%s", str_not_avail );
    goto out;
  }

  // Try to read the serial number.
  o_rc = syn1588_getSerialNumber( dev_idx, ser_num );

  if ( o_rc != ENoErr )
  {
    // Failed to read the serial number.
    // Use the device index instead.
    n = snprintf_safe( s, max_len, "%03u", dev_idx );
    goto out;
  }

  // The serial number could be read successfully.
  // However, we may have to trim trailing whitespace.
  cp = &ser_num[strlen( ser_num )];

  for (;;)
  {
    if ( cp == ser_num )  // Reached beginning of string.
      break;

    cp--;

    if ( *cp != ' ' )  // If char is not a space, we're done.
      break;

    // Replace space by a terminating 0.
    *cp = 0;
  }

  // Print the serial number.
  n = snprintf_safe( s, max_len, "%s", ser_num );

out:
  return n;

}  // snprintf_sernum



/*HDR*/
int mbg_syn1588_get_device_info( MBG_DEV_HANDLE dh, PCPS_DEV *p )
{
  MBGDEVIO_RET_VAL rc = MBG_ERR_NO_DEV;

  if ( is_mbg_syn1588_type( dh ) )
  {
    memset( p, 0, sizeof( *p ) );

    // Type specific stuff.

    // num;                     ///< see ::PCPS_TYPES

    sn_cpy_str_safe( p->type.name, sizeof( p->type.name ), mbg_syn1588_type_name );

    // dev_id;            ///< see @ref MEINBERG_PCI_DEVICE_IDS and @ref MBG_USB_DEVICE_IDS
    // ref_type;          ///< see ::PCPS_REF_TYPES
    // bus_flags;         ///< see @ref PCPS_BUS_FLAG_MASKS


    // Configuration/instance specific stuff.

    // err_flags;         ///< See @ref PCPS_ERR_FLAG_MASKS
    // bus_num;
    // slot_num;
    // port[N_PCPS_PORT_RSRC];
    // short_status_port;

    p->cfg.irq_num = PCPS_IRQ_NUM_UNDEFINED;  // TODO

    // timeout_clk;

    {
      // fw_rev_num;
      uint32_t build_id;
      SYN1588_RET_VAL o_rc = syn1588_getBuildID( mbg_syn1588_get_dev_idx( dh ), &build_id );
      if ( o_rc == ENoErr )
        p->cfg.fw_rev_num = build_id;
    }

    p->cfg.features |= PCPS_HAS_HR_TIME;
    p->cfg.features |= PCPS_HAS_FAST_HR_TSTAMP;

    // fw_id;

    snprintf_sernum( p->cfg.sernum, sizeof( p->cfg.sernum ), dh );

    rc = MBG_SUCCESS;
  }

  return rc;

}  // mbg_syn1588_get_device_info



/*HDR*/
int mbg_syn1588_setup_receiver_info( MBG_DEV_HANDLE dh, RECEIVER_INFO *p )
{
  if ( !is_mbg_syn1588_type( dh ) )
    return MBG_ERR_NO_DEV;

  memset( p, 0, sizeof( *p ) );

  p->model_code = GPS_MODEL_UNKNOWN;
  // sw_rev: code, name, reserved ?
  sn_cpy_str_safe( p->model_name, sizeof( p->model_name ), mbg_syn1588_type_name );

  snprintf_sernum( p->sernum, sizeof( p->sernum ), dh );

  // epld_name
  // n_channels == 0

  p->ticks_per_sec = DEFAULT_GPS_TICKS_PER_SEC;
  p->features = 0;

  // fixed_freq

  p->osc_type = GPS_OSC_UNKNOWN;  // TODO
  p->osc_flags = 0;

  // n_ucaps = ??
  // n_com_ports = 0
  // n_str_type = 0
  // n_prg_out = 0

  return MBG_SUCCESS;

}  // mbg_syn1588_setup_receiver_info



/*HDR*/
/**
 * @brief Retrieve the content of a SYN1588 NIC's PTP_SYNC_STATUS register.
 *
 * @note The value returned via @p p is set to 0 if the status is not
 *       available, or an error occurred.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle.
 * @param[out]  p   Pointer to a ::SYN1588_PTP_SYNC_STATUS variable to be filled.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_syn1588_get_ptp_sync_status( MBG_DEV_HANDLE dh, SYN1588_PTP_SYNC_STATUS *p )
{
  SYN1588_RET_VAL o_rc;
  SYN1588_DEV_IDX dev_idx = mbg_syn1588_get_dev_idx( dh );

  if ( dev_idx == SYN1588_INVALID_DEV_IDX )
    return MBG_ERR_NO_DEV;

  o_rc = syn1588_getPtpSyncStatus( dev_idx, p );

  if ( o_rc != ENoErr )  // No success.
    return MBG_ERR_IO;

  return MBG_SUCCESS;

}  // mbg_syn1588_get_ptp_sync_status


#if !EXCLUDE_SYN1588_ACCESS

static __mbg_inline /*HDR*/
int do_syn1588_get_safe_pcps_time_stamp_raw( MBG_DEV_HANDLE dh,
                                             struct TimestampStatus_t *p_tss,
                                             PCPS_TIME_STAMP *p,
                                             MBG_PC_CYCLES *p_cyc )
{
  SYN1588_RET_VAL o_rc;

  SYN1588_DEV_IDX dev_idx = mbg_syn1588_get_dev_idx( dh );

  if ( dev_idx == SYN1588_INVALID_DEV_IDX )
    return MBG_ERR_NO_DEV;


  // If on Windows, we have to get a lock to protect
  // against concurrent access.
  #if defined( MBG_TGT_WIN32 ) && !defined( MBGDEVIO_STATIC_LIB )
    mbgdevio_syn1588_get_lock_win32();
  #endif

  if ( p_cyc )
    mbg_get_pc_cycles( p_cyc );

  o_rc = syn1588_getSafeTimeAndStatus( dev_idx, p_tss );

  // If on Windows, we have to release the lock we got above.
  #if defined( MBG_TGT_WIN32 ) && !defined( MBGDEVIO_STATIC_LIB )
    mbgdevio_syn1588_release_lock_win32();
  #endif

  if ( o_rc != ENoErr )  // No success.
    return MBG_ERR_IO;


  p->sec = p_tss->ts.sec_l;
  p->frac = nsec_to_bin_frac_32( p_tss->ts.nsec );

  return MBG_SUCCESS;

}  // do_syn1588_get_safe_pcps_time_stamp_raw



static __mbg_inline /*HDR*/
int do_syn1588_get_safe_pcps_time_stamp( MBG_DEV_HANDLE dh,
                                         struct TimestampStatus_t *p_tss,
                                         PCPS_TIME_STAMP *p,
                                         PCPS_TIME_STATUS_X *p_st,
                                         MBG_PC_CYCLES *p_cyc )
{
  int rc = do_syn1588_get_safe_pcps_time_stamp_raw( dh, p_tss, p, p_cyc );

  if ( mbg_rc_is_success( rc ) )
  {
    // The timestamp above is PTP/TAI, or arbitrary.
    // If possible, convert to UTC.
    p->sec -= syn1588_check_timescale( p_tss->sync_status, p_st );
  }

  return rc;

}  // do_syn1588_get_safe_pcps_time_stamp



static __mbg_inline /*HDR*/
int do_syn1588_get_safe_pcps_hr_time( MBG_DEV_HANDLE dh,
                                      struct TimestampStatus_t *p_tss,
                                      PCPS_HR_TIME *p,
                                      MBG_PC_CYCLES *p_cyc )
{
  int rc = do_syn1588_get_safe_pcps_time_stamp_raw( dh, p_tss, &p->tstamp,
                                                    p_cyc );

  if ( mbg_rc_is_success( rc ) )
    syn1588_decode_sync_status( p_tss->sync_status, p );

  return rc;

}  // do_syn1588_get_safe_pcps_hr_time



static __mbg_inline /*HDR*/
int do_syn1588_get_safe_pcps_time( MBG_DEV_HANDLE dh,
                                   PCPS_TIME *p,
                                   MBG_PC_CYCLES *p_cyc )
{
  struct TimestampStatus_t tss;
  PCPS_HR_TIME ht;

  int rc = do_syn1588_get_safe_pcps_hr_time( dh, &tss, &ht, p_cyc );

  if ( mbg_rc_is_success( rc ) )
    rc = pcps_time_from_pcps_hr_time( p, &ht, tss.ts.nsec );

  return rc;

}  // do_syn1588_get_safe_pcps_time



/*HDR*/
int mbg_syn1588_get_fast_hr_timestamp( MBG_DEV_HANDLE dh, PCPS_TIME_STAMP *p )
{
  struct TimestampStatus_t tss;

  return do_syn1588_get_safe_pcps_time_stamp( dh, &tss, p, NULL, NULL );

}  // mbg_syn1588_get_fast_hr_timestamp



/*HDR*/
int mbg_syn1588_get_fast_hr_timestamp_cycles( MBG_DEV_HANDLE dh, PCPS_TIME_STAMP_CYCLES *p )
{
  struct TimestampStatus_t tss;

  return do_syn1588_get_safe_pcps_time_stamp( dh, &tss, &p->tstamp, NULL, &p->cycles );

}  // mbg_syn1588_get_fast_hr_timestamp_cycles



/*HDR*/
int mbg_syn1588_get_hr_time( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p )
{
  struct TimestampStatus_t tss;

  return do_syn1588_get_safe_pcps_hr_time( dh, &tss, p, NULL );

}  // mbg_syn1588_get_hr_time



/*HDR*/
int mbg_syn1588_get_hr_time_cycles( MBG_DEV_HANDLE dh, PCPS_HR_TIME_CYCLES *p )
{
  struct TimestampStatus_t tss;

  return do_syn1588_get_safe_pcps_hr_time( dh, &tss, &p->t, &p->cycles );

}  // mbg_syn1588_get_hr_time_cycles



/*HDR*/
int mbg_syn1588_get_time( MBG_DEV_HANDLE dh, PCPS_TIME *p )
{
  return do_syn1588_get_safe_pcps_time( dh, p, NULL );

}  // mbg_syn1588_get_time



/*HDR*/
int mbg_syn1588_get_time_cycles( MBG_DEV_HANDLE dh, PCPS_TIME_CYCLES *p )
{
  return do_syn1588_get_safe_pcps_time( dh, &p->t, &p->cycles );

}  // mbg_syn1588_get_time



/*HDR*/
/**
 * @brief Read both system time and associated device time from the kernel driver.
 *
 * @todo This info as well as the implementation for SYN1588 devices
 * needs to be fixed.
 *
 * The kernel driver reads the current system time plus a HR time structure
 * from a device immediately after each other. The returned info structure also
 * contains some cycles counts to be able to determine the execution times
 * required to read those time stamps.
 *
 * The advantage of this call compared to ::mbg_get_time_info_tstamp is
 * that this call also returns the status of the device. On the other hand, reading
 * the HR time from the device may block e.g. if another application accesses
 * the device.
 *
 * This call makes a ::mbg_get_hr_time_cycles call internally so the API call
 * ::mbg_chk_dev_has_hr_time can be used to check whether this call is supported
 * by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::MBG_TIME_INFO_HRT structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_time_info_tstamp
 */
int mbg_syn1588_get_time_info_hrt( MBG_DEV_HANDLE dh, MBG_TIME_INFO_HRT *p )
{
  int rc = MBG_SUCCESS;

  memset( p, 0, sizeof( *p ) );

  mbg_get_pc_cycles( &p->sys_time_cycles.cyc_before );
  mbg_get_sys_time( &p->sys_time_cycles.sys_time );
  mbg_get_pc_cycles( &p->sys_time_cycles.cyc_after );

  rc = mbg_syn1588_get_hr_time_cycles( dh, &p->ref_hr_time_cycles );

  return rc;

}  // mbg_syn1588_get_time_info_hrt



/*HDR*/
/**
 * @brief Read both system time and associated device timestamp from the kernel driver.
 *
 * @todo This info as well as the implementation for SYN1588 devices
 * needs to be fixed.
 *
 * This call is similar to ::mbg_get_time_info_hrt except that a
 * ::mbg_get_fast_hr_timestamp_cycles call is made internally, so
 * the API call ::mbg_chk_dev_has_fast_hr_timestamp can be used to check whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_TIME_INFO_TSTAMP structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_time_info_hrt
 */
int mbg_syn1588_get_time_info_tstamp( MBG_DEV_HANDLE dh, MBG_TIME_INFO_TSTAMP *p )
{
  int rc = MBG_SUCCESS;

  memset( p, 0, sizeof( *p ) );

  mbg_get_pc_cycles( &p->sys_time_cycles.cyc_before );
  mbg_get_sys_time( &p->sys_time_cycles.sys_time );
  mbg_get_pc_cycles( &p->sys_time_cycles.cyc_after );

  rc = MBG_ERR_NOT_SUPP_BY_DRVR; // FIXME TODO mbg_syn1588_get_fast_hr_timestamp_cycles( dh, &p->ref_hr_time_cycles );

  return rc;

}  // mbg_syn1588_get_time_info_tstamp

#endif  // EXCLUDE_SYN1588_ACCESS



#if SUPP_SYN1588_USR_SPC_TESTS

volatile uint32_t tmp_frac;

/*HDR*/
void mbg_syn1588_tests( MBG_DEV_HANDLE dh )
{
  SYN1588_DEV_IDX dev_idx = mbg_syn1588_get_dev_idx( dh );

  if ( dev_idx != SYN1588_INVALID_DEV_IDX )
  {
    #define N_READS  10

    PCPS_HR_TIME ht[N_READS] = { { { 0 } } };
    struct Timestamp_t ts[N_READS] = { { 0 } };
    SYN1588_RET_VAL o_rc[N_READS] = { 0 };
    LEAP_INFO li;
    int i;
    int rc;

    o_rc[0] = syn1588_getLeapInfo( dev_idx, &li );

    printf( "Leap Info: 0x%08lX 0x%08lX %i\n",
            (ulong) li.leap_sec, (ulong) li.leap_applytime, o_rc[0] );

    rc = mbg_get_hr_time( dh, &ht[0] );

    (void) rc;  // avoid warning "is set but not used"

    for ( i = 0; i < N_READS; i++ )
      o_rc[i] = syn1588_getTime( dev_idx, &ts[i] );

    // mbg_date_time_dist = 1;  // space in string between date and time

    for ( i = 0; i < N_READS; i++ )
    {
      struct Timestamp_t *p_ts = &ts[i];
      NANO_TIME_64 nt64;
      NANO_TIME_64 prev_nt64;
      TM_GPS tm_gps;
      char ws[128];
      int n;
      size_t max_len = sizeof( ws );

      nt64.secs = p_ts->sec_l + ( ( (int64_t) p_ts->sec_h ) << 32 );
      nt64.nano_secs = p_ts->nsec;

      nano_time_64_to_tm_gps( &tm_gps, &nt64 );

      n = 0;
      n += snprint_nano_time_64( &ws[n], max_len - n, &nt64 );
      n += snprintf_safe(  &ws[n], max_len - n, " %i  ", o_rc[i] );
      n += mbg_str_tm_gps_date_time( &ws[n], max_len - n, &tm_gps );

      // FIXME Currently, nano_time_64_to_tm_gps does not set up the
      // TM_GPS::frac field, so we append the number of nanoseconds
      // manually.
      n += snprintf_safe(  &ws[n], max_len - n, ".%09lu  ", (ulong) p_ts->nsec );

      if ( i > 0 )
      {
        double d = nano_time_64_to_double( &nt64 ) - nano_time_64_to_double( &prev_nt64 );
        n += snprintf_safe(  &ws[n], max_len - n, " %+.9f", d );
      }

      prev_nt64 = nt64;

      printf( "SYN1588 time: %s\n", ws );
    }

    printf( "\n" );

    {
      uint32_t val;

      val = 0;
      tmp_frac = nsec_to_bin_frac_32( val );
      printf( "%08lX\n", (ulong) tmp_frac );

      val = NSEC_PER_SEC / 2;
      tmp_frac = nsec_to_bin_frac_32( val );
      printf( "%08lX\n", (ulong) tmp_frac );

      val = NSEC_PER_SEC - 1;
      tmp_frac = nsec_to_bin_frac_32( val );
      printf( "%08lX\n", (ulong) tmp_frac );

      val = NSEC_PER_SEC;
      tmp_frac = nsec_to_bin_frac_32( val );
      printf( "%08lX\n", (ulong) tmp_frac );
    }

    printf( "\n" );
  }

}  // mbg_syn1588_tests

#endif  // SUPP_SYN1588_USR_SPC_TESTS

