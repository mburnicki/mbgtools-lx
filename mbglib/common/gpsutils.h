
/**************************************************************************
 *
 *  $Id: gpsutils.h 1.9 2022/08/26 11:00:48 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for gpsutils.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: gpsutils.h $
 *  Revision 1.9  2022/08/26 11:00:48  martin.burnicki
 *  Changed the return value of some snprint_... functions from size_t to int.
 *  Revision 1.8  2017/07/05 13:58:25Z  martin
 *  Include stddef.h.
 *  Updated function prototypes.
 *  Revision 1.7  2010/07/15 09:32:09  martin
 *  Use DEG character definition from pcpslstr.h.
 *  Revision 1.6  2005/02/18 10:32:33Z  martin
 *  Check more predefined macros to determine if compiling for Windows.
 *  Revision 1.5  2003/02/04 09:18:48Z  MARTIN
 *  Updated function prototypes.
 *  Revision 1.4  2002/12/12 16:08:11  martin
 *  Definitions for degree character.
 *  Requires mbggeo.h.
 *  Updated function prototypes.
 *  Revision 1.3  2001/02/05 09:40:42Z  MARTIN
 *  New file header.
 *  Source code cleanup.
 *
 **************************************************************************/

#ifndef _GPSUTILS_H
#define _GPSUTILS_H


/* Other headers to be included */

#include <mbggeo.h>

#include <stddef.h>


#ifdef _GPSUTILS
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Swap the bytes of a single variable of type "double"
 *
 * The memory layout of a "double" on Meinberg bus level devices
 * and computers usually differs. This function can be used to
 * fix this and is usually called from inside API functions,
 * if required.
 *
 * @param[in,out] p   Pointer to a "double" to be swapped
 *
 * @see ::swap_eph_doubles
 * @see ::swap_alm_doubles
 * @see ::swap_utc_doubles
 * @see ::swap_iono_doubles
 * @see ::swap_pos_doubles
 */
 void swap_double( double *p ) ;

 /**
 * @brief Swap the "double" fields in an ::EPH structure
 *
 * See comments for ::swap_double
 *
 * @param[in,out] p  Pointer to an ::EPH structure to be converted
 *
 * @see ::swap_double
 * @see ::swap_alm_doubles
 * @see ::swap_utc_doubles
 * @see ::swap_iono_doubles
 * @see ::swap_pos_doubles
 */
 void swap_eph_doubles( EPH *p ) ;

 /**
 * @brief Swap the "double" fields in an ::ALM structure
 *
 * See comments for ::swap_double
 *
 * @param[in,out] p  Pointer to an ::ALM structure to be converted
 *
 * @see ::swap_double
 * @see ::swap_eph_doubles
 * @see ::swap_utc_doubles
 * @see ::swap_iono_doubles
 * @see ::swap_pos_doubles
 */
 void swap_alm_doubles( ALM *p ) ;

 /**
 * @brief Swap the "double" fields in a ::UTC structure
 *
 * See comments for ::swap_double
 *
 * @param[in,out] p  Pointer to a ::UTC structure to be converted
 *
 * @see ::swap_double
 * @see ::swap_eph_doubles
 * @see ::swap_alm_doubles
 * @see ::swap_iono_doubles
 * @see ::swap_pos_doubles
 */
 void swap_utc_doubles( UTC *p ) ;

 /**
 * @brief Swap the "double" fields in a ::IONO structure
 *
 * See comments for ::swap_double
 *
 * @param[in,out] p  Pointer to a ::IONO structure to be converted
 *
 * @see ::swap_double
 * @see ::swap_eph_doubles
 * @see ::swap_alm_doubles
 * @see ::swap_utc_doubles
 * @see ::swap_pos_doubles
 */
 void swap_iono_doubles( IONO *p ) ;

 /**
 * @brief Swap the "double" fields in a ::POS structure
 *
 * See comments for ::swap_double
 *
 * @param[in,out] p  Pointer to a ::POS structure to be converted
 *
 * @see ::swap_double
 * @see ::swap_eph_doubles
 * @see ::swap_alm_doubles
 * @see ::swap_utc_doubles
 * @see ::swap_iono_doubles
 */
 void swap_pos_doubles( POS *p ) ;

 /**
 * @brief Print the ::DMS part of a geo position into a string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  p        Pointer to a ::DMS structure to be printed
 * @param[in]  prec     Precision, i.e. number of fractions of the seconds
 *
 * @return Length of the string in the buffer
 *
 * @see snprint_dms
 * @see snprint_alt
 * @see snprint_pos_geo
 * @see snprint_fixed_freq
 */
 int snprint_dms( char *s, size_t max_len, const DMS *p, int prec ) ;

 /**
 * @brief Print the altitude part of a geo position into a string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  alt      The altitude value to be printed, in [m]
 *
 * @return Length of the string in the buffer
 *
 * @see snprint_dms
 * @see snprint_pos_geo
 * @see snprint_fixed_freq
 */
 int snprint_alt( char *s, size_t max_len, double alt ) ;

 /**
 * @brief Print a geo position in ::POS format into a string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  p        Pointer to a ::POS structure to be printed
 * @param[in]  sep      Separator character for the ::DMS part
 * @param[in]  prec     Precision, i.e. number of fractions of the seconds of the ::DMS part
 *
 * @return Length of the string in the buffer
 *
 * @see snprint_dms
 * @see snprint_alt
 * @see snprint_fixed_freq
 */
 int snprint_pos_geo( char *s, size_t max_len, const POS *p, char sep, int prec ) ;

 /**
 * @brief Print a formatted ::FIXED_FREQ_INFO into a string buffer
 *
 * @param[out] s        The string buffer to be filled
 * @param[in]  max_len  Size of the output buffer for 0-terminated string
 * @param[in]  p_ff     Pointer to a ::FIXED_FREQ_INFO structure to be printed
 *
 * @return Length of the string in the buffer
 *
 * @see snprint_dms
 * @see snprint_alt
 * @see snprint_pos_geo
 */
 int snprint_fixed_freq( char *s, size_t max_len, FIXED_FREQ_INFO *p_ff ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _GPSUTILS_H */

