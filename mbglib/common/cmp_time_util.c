
/**************************************************************************
 *
 *  $Id: cmp_time_util.c 1.10 2022/12/21 14:48:23 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Common device time compare utility functions.
 *
 * -----------------------------------------------------------------------
 *  $Log: cmp_time_util.c $
 *  Revision 1.10  2022/12/21 14:48:23  martin.burnicki
 *  Removed obsolete (PCPS_DEV *) parameters from a function.
 *  Removed some obsolete _int_from_size_t() stuff.
 *  Revision 1.9  2021/04/29 14:20:20  martin
 *  Variable pc_cycles_frequency was renamed to mbg_pc_cycles_frequency.
 *  Revision 1.8  2021/03/22 10:09:38  martin
 *  Updated some comments.
 *  Revision 1.7  2021/03/12 14:20:16  martin
 *  Use the new global variable pc_cycles_frequency.
 *  Revision 1.6  2020/11/04 17:17:38  martin
 *  Added some functions to support checking the continuity of the timestamps
 *  and status sequentially read from a device.
 *  Revision 1.5  2020/05/11 09:35:13  martin
 *  Removed obsolete 'extern' declaration of variable 'cyc_freq'.
 *  Revision 1.4  2019/09/25 16:07:44  martin
 *  Removed inclusion of obsolete header pcpsutil.h.
 *  Revision 1.3  2018/12/11 12:01:03  martin
 *  Fixed compiler warnings on Windows.
 *  Revision 1.2  2018/08/08 08:22:23Z  martin
 *  New function mbg_snprint_hr_time_loc().
 *  Account for renamed library symbols.
 *  Removed obsolete code.
 *  Revision 1.1  2013/07/25 10:42:59  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _CMP_TIME_UTIL
  #include <cmp_time_util.h>
#undef _CMP_TIME_UTIL

#include <mbgdevio.h>
#include <mbgutil.h>
#include <str_util.h>

#include <toolutil.h>   // for mbg_snprint_hr_tstamp(). FIXME: use function from mbgutil library

#include <stdio.h>


/*HDR*/
/**
 * @brief Check if both devices support fast HR timestamps.
 *
 * @param[in]  dh1          Handle of the first device.
 * @param[in]  dh2          Handle of the second device.
 * @param[in]  err_msg_fnc  Pointer to a function to be called in case of error.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int chk_fast_tstamp_supp( MBG_DEV_HANDLE dh1, MBG_DEV_HANDLE dh2,
                          MBG_ERR_MSG_FNC err_msg_fnc )
{
  int rc;
  const char n_supp_msg[] = "fast (memory mapped) time stamps";

  rc = chk_feat_supp( dh1, mbg_chk_dev_has_fast_hr_timestamp,
                      err_msg_fnc, n_supp_msg );

  if ( mbg_rc_is_success( rc ) )
    rc = chk_feat_supp( dh2, mbg_chk_dev_has_fast_hr_timestamp,
                        err_msg_fnc, n_supp_msg );
  return rc;

}  // chk_fast_tstamp_supp



/*HDR*/
/**
 * @brief Read timestamps and cycles from both devices.
 *
 * @param[in]   dh1        Handle of the first device.
 * @param[out]  p_htc1     Timestamp and cycles read from the first device.
 * @param[in]   dh2        Handle of the second device.
 * @param[out]  p_htc2     Timestamp and cycles read from the first device.
 * @param[in]   read_fast  Flag indicating that fast timestamps should be read.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int get_htc_timestamps( MBG_DEV_HANDLE dh1, PCPS_HR_TIME_CYCLES *p_htc1,
                        MBG_DEV_HANDLE dh2, PCPS_HR_TIME_CYCLES *p_htc2,
                        int read_fast )
{
  int rc;

  if ( read_fast )
  {
    PCPS_TIME_STAMP_CYCLES ts_c1;
    PCPS_TIME_STAMP_CYCLES ts_c2;

    rc = mbg_get_fast_hr_timestamp_cycles( dh1, &ts_c1 );

    if ( mbg_cond_err_msg( rc, "mbg_get_fast_hr_timestamp_cycles 1" ) )
      goto done;

    rc = mbg_get_fast_hr_timestamp_cycles( dh2, &ts_c2 );

    if ( mbg_cond_err_msg( rc, "mbg_get_fast_hr_timestamp_cycles 2" ) )
      goto done;

    setup_hr_time_cycles_from_timestamp_cycles( p_htc1, &ts_c1 );
    setup_hr_time_cycles_from_timestamp_cycles( p_htc2, &ts_c2 );
  }
  else
  {
    rc = mbg_get_hr_time_cycles( dh1, p_htc1 );

    if ( mbg_cond_err_msg( rc, "mbg_get_hr_time_cycles 1" ) )
      goto done;

    rc = mbg_get_hr_time_cycles( dh2, p_htc2 );

    if ( mbg_cond_err_msg( rc, "mbg_get_hr_time_cycles 2" ) )
      goto done;
  }

done:
  return rc;

}  // get_htc_timestamps



/*HDR*/
/**
 * @brief Compute the delta cycles and time difference.
 *
 * @param[in]   p_htc        Timestamp/cycles t from device under test.
 * @param[in]   p_htc_ref    Timestamp/cycles t_ref from reference device.
 * @param[out]  p_delta_ts   Pointer to save time difference (t - tref), can be @a NULL.
 * @param[out]  p_delta_cyc  Pointer to save delta cycles (t - tref), can be @a NULL.
 *
 * @return The difference between delta_t and delta_cycles.
 */
double get_htc_delta( const PCPS_HR_TIME_CYCLES *p_htc,
                      const PCPS_HR_TIME_CYCLES *p_htc_ref,
                      double *p_delta_ts, double *p_delta_cyc )
{
  double delta_ts;
  double delta_cyc;

  delta_ts = (double) p_htc->t.tstamp.sec + dfrac_sec_from_bin( p_htc->t.tstamp.frac )
           - (double) p_htc_ref->t.tstamp.sec - dfrac_sec_from_bin( p_htc_ref->t.tstamp.frac );

  #if defined( MBG_TGT_WIN32 )
  {
    __int64 dt_t = p_htc->cycles - p_htc_ref->cycles;
    delta_cyc = (double) dt_t / (__int64) mbg_pc_cycles_frequency;
  }
  #else
    delta_cyc = ( (double) ( p_htc->cycles - p_htc_ref->cycles ) ) / mbg_pc_cycles_frequency;
  #endif

  if ( p_delta_ts )
    *p_delta_ts = delta_ts;

  if ( p_delta_cyc )
    *p_delta_cyc = delta_cyc;

  return delta_ts - delta_cyc;

}  // get_htc_delta



/*HDR*/
/**
 * @brief Print a HR time stamp, and optionally a status and time difference.
 *
 * @param[in]  p_ts  Pointer to the time stamp.
 * @param[in]  p_st  Pointer to the status code, optional, can be @a NULL.
 * @param[in]  p_d   Pointer to the variable with time difference in microseconds, can be @a NULL.
 *
 * @see ::mbg_check_continuity
 */
void mbg_show_delta_exceeded( const PCPS_TIME_STAMP *p_ts, const PCPS_TIME_STATUS_X *p_st, const double *p_d )
{
  char s[256];
  size_t sz = sizeof( s );
  int n = 0;

  n += mbg_snprint_hr_tstamp( &s[n], sz - n, p_ts, 0, 1 );

  if ( p_st )
    n += snprintf_safe( &s[n], sz - n, ", st 0x%04X", *p_st );

  if ( p_d )
    n += snprintf_safe( &s[n], sz - n, ", d: %.3f us\n", *p_d );

  fprintf( stderr, "%s\n", s );

}  // mbg_show_delta_exceeded



/*HDR*/
/**
 * @brief Check continuity of 2 subsequent time stamps and status codes.
 *
 * Compares the current timestamp to the previous timestamp,
 * and optionally, the current sttus to the previous status.
 *
 * Calls ::mbg_show_delta_exceeded if the time difference
 * is less than 0, or exceeeds @p max_allowed_delta_us,
 * or if the status has changed.
 *
 * @param[in]  p_ts                  Pointer to the current time stamp.
 * @param[in]  p_prv_ts              Pointer to the previous time stamp.
 * @param[in]  p_st                  Pointer to the current status code, optional, can be @a NULL.
 * @param[in]  p_prv_st              Pointer to the previous status code, optional, can be @a NULL.
 * @param[in]  max_allowed_delta_us  Maximum allowed time difference, in microseconds.
 *
 * @see ::mbg_show_delta_exceeded
 */
void mbg_check_continuity( const PCPS_TIME_STAMP *p_ts, const PCPS_TIME_STAMP *p_prv_ts,
                           const PCPS_TIME_STATUS_X *p_st, const PCPS_TIME_STATUS_X *p_prv_st,
                           double max_allowed_delta_us )
{
  int must_show = 0;

  double d = delta_timestamp_us( p_ts, p_prv_ts );

  if ( ( d < 0 ) || ( d > max_allowed_delta_us ) )
    must_show = 1;

  if ( p_st && p_prv_st && ( *p_st != *p_prv_st ) )
    must_show = 1;

  if ( must_show )
  {
    mbg_show_delta_exceeded( p_prv_ts, p_prv_st, NULL );
    mbg_show_delta_exceeded( p_ts, p_st, &d );
  }

}  // mbg_check_continuity



/*HDR*/
/**
 * @brief Print %UTC date and time from a ::PCPS_TIME_STAMP structure to a string.
 *
 * This function is mostly similar to ::mbg_snprint_hr_tstamp, but optionally
 * appends the raw timestamp instead of inserting it at the beginning.
 *
 * @param[out]  s          Address of a string buffer to be filled.
 * @param[in]   max_len    Size of the string buffer.
 * @param[in]   p          Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 * @param[in]   print_raw  Flag indicating if a raw timestamp (hex) is to be appended.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_snprint_hr_tstamp
 * @see ::mbg_snprint_hr_time_loc
 */
int mbg_snprint_hr_tstamp_ext( char *s, int max_len,
                               const PCPS_TIME_STAMP *p, int print_raw )
{
  int n = mbg_snprint_hr_tstamp( s, max_len, p, 0, 0 );

  if ( print_raw )
    n += snprintf( &s[n], max_len - n, " (0x%08lX.%08lX)",
                   (ulong) p->sec,
                   (ulong) p->frac );

  return n;

}  // mbg_snprint_hr_tstamp_ext



/*HDR*/
/**
 * @brief Print local date and time from a ::PCPS_TIME_STAMP structure to a string.
 *
 * This function is mostly similar to ::mbg_snprint_hr_tstamp, but optionally
 * appends the raw timestamp instead of inserting it at the beginning.
 *
 * @param[out]  s          Address of a string buffer to be filled.
 * @param[in]   max_len    Size of the string buffer.
 * @param[in]   p          Pointer to a ::PCPS_TIME_STAMP structure to be evaluated.
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_snprint_hr_tstamp
 * @see ::mbg_snprint_hr_tstamp_ext
 */
int mbg_snprint_hr_time_loc( char *s, size_t max_len, const PCPS_HR_TIME *p )
{
  int n = mbg_snprint_hr_tstamp( s, max_len, &p->tstamp, p->utc_offs, 0 );

  n += snprintf_safe( &s[n], max_len - n, " UTC" );

  if ( !(p->status & PCPS_UTC ) )
    n += mbg_str_pcps_hr_time_offs( &s[n], (int) ( max_len - n ), p, "" );

#if 0
  if(p->utc_offs != 0)
    n += mbg_snprintf( n, len_s - n, "(%+02luh",
                       ( pt->utc_offs < 0 ) ? '-' : '+',
                       ldt.quot, ldt.rem );
#endif

  return n;

}  // mbg_snprint_hr_time_loc


