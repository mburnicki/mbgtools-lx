
/**************************************************************************
 *
 *  $Id: ctrydttm.c 1.11 2022/12/21 15:48:09 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Functions converting dates and time into strings depending on
 *    language/country settings.
 *
 * -----------------------------------------------------------------------
 *  $Log: ctrydttm.c $
 *  Revision 1.11  2022/12/21 15:48:09  martin.burnicki
 *  Removed some obsolete _int_from_size_t() stuff.
 *  Quieted some potential compiler warnings.
 *  Revision 1.10  2018/12/11 16:01:46  martin
 *  Use standard int types for more compatibility.
 *  Revision 1.9  2018/12/11 11:49:00Z  martin
 *  Fixed compiler warnings on Windows.
 *  Revision 1.8  2018/01/02 16:13:55Z  martin
 *  Changed return type of snprint_() functions from size_t to int.
 *  Revision 1.7  2015/11/09 11:22:12Z  martin
 *  Modified snprint_ctry_dt_short() and fixed snprint_ctry_dt().
 *  Revision 1.6  2015/08/31 10:00:46  martin
 *  Use safe string functions from str_util.c.
 *  This required renaming of functions and changing parameters.
 *  Revision 1.5  2008/11/24 16:15:46Z  martin
 *  Don't use sprintf() without format string.
 *  Revision 1.4  2000/11/27 10:06:27  MARTIN
 *  Renamed local variable wday_str to lstrs_wday.
 *  If macro USER_LSTR_WDAY is defined, lstrs_wday can be declared
 *  externally to override the defaults.
 *  Revision 1.3  2000/09/14 15:13:25  MARTIN
 *  Renamed sprint_short_ctry_dt() to sprint_ctry_dt_short() to match
 *  other naming conventions.
 *  Revision 1.2  2000/07/21 11:53:42  MARTIN
 *  Initial revision
 *
 **************************************************************************/

#define _CTRYDTTM
 #include <ctrydttm.h>
#undef _CTRYDTTM

#include <ctry.h>
#include <str_util.h>

#include <stdio.h>


#ifndef DAYS_PER_WEEK
  #define DAYS_PER_WEEK 7
#endif

#ifdef USER_LSTRS_WDAY
  extern const char *lstrs_wday[N_LNG][DAYS_PER_WEEK];
#else
  static const char *lstrs_wday[N_LNG][DAYS_PER_WEEK] =
  {
    { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" },
    { "So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"  }
  };
#endif

extern CTRY ctry;
extern LANGUAGE language;


/*HDR*/
int snprint_02u( char *s, size_t max_len, unsigned int uc )
{
  return snprintf_safe( s, max_len, "%02u", uc );

}  // snprint_02u



/*HDR*/
int snprint_04u( char *s, size_t max_len, unsigned int us )
{
  return snprintf_safe( s, max_len, "%04u", us );

}  // snprint_04u



/*HDR*/
int snprint_ctry_wday( char *s, size_t max_len, int wday, LANGUAGE language )
{
  if ( language >= N_LNG )
    language = LNG_ENGLISH;

  return snprintf_safe( s, max_len, "%s", ( wday < DAYS_PER_WEEK ) ?
                        lstrs_wday[language][wday] : "--" );

}  // snprint_ctry_wday



/*HDR*/
int snprint_ctry_dt_short( char *s, size_t max_len, uint mday, uint month )
{
  uint tmp_1;
  uint tmp_2;
  int n;

  switch ( ctry.dt_fmt )
  {
    case DT_FMT_YYYYMMDD:
    case DT_FMT_MMDDYYYY:
      tmp_1 = month;
      tmp_2 = mday;
      break;

    default:
      tmp_1 = mday;
      tmp_2 = month;
      break;

  }  // switch

  n = snprint_02u( s, max_len, tmp_1 );
  n += sn_cpy_char_safe( &s[n], max_len - n, ctry.dt_sep );
  n += snprint_02u( &s[n], max_len - n, tmp_2 );

  return n;

}  // snprint_ctry_dt_short



/*HDR*/
int snprint_ctry_dt( char *s, size_t max_len, int mday, int month, int year )
{
  int n = 0;

  if ( ctry.dt_fmt == DT_FMT_YYYYMMDD )
  {
    n += snprint_04u( &s[n], max_len - n, year );
    n += sn_cpy_char_safe( &s[n], max_len - n, ctry.dt_sep );
  }

  n += snprint_ctry_dt_short( &s[n], max_len - n, mday, month );

  if ( ctry.dt_fmt != DT_FMT_YYYYMMDD )
  {
    n += sn_cpy_char_safe( &s[n], max_len - n, ctry.dt_sep );
    n += snprint_04u( &s[n], max_len - n, year );
  }

  return n;

}  // snprint_ctry_dt



/*HDR*/
int snprint_ctry_tm_short( char *s, size_t max_len, uchar hour, uchar minute )
{
  int n = snprint_02u( s, max_len, hour );
  n += sn_cpy_char_safe( &s[n], max_len - n, ctry.tm_sep );
  n += snprint_02u( &s[n], max_len - n, minute );

  return n;

}  // snprint_ctry_tm_short



/*HDR*/
int snprint_ctry_tm( char *s, size_t max_len, uchar hour, uchar minute, uchar second )
{
  int n = snprint_ctry_tm_short( s, max_len, hour, minute );
  n += sn_cpy_char_safe( &s[n], max_len - n, ctry.tm_sep );
  n += snprint_02u( &s[n], max_len - n, second );

  return n;

}  // snprint_ctry_tm



/*HDR*/
int snprint_ctry_tm_long( char *s, size_t max_len, uchar hour, uchar minute,
                          uchar second, long frac, ushort frac_digits )
{
  int n = snprint_ctry_tm( s, max_len, hour, minute, second );
  n += sn_cpy_char_safe( &s[n], max_len - n, '.' );
  n += snprintf_safe( &s[n], max_len - n, "%0*lu", frac_digits, frac );

  return n;

}  // snprint_ctry_tm_long


