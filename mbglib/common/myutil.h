
/**************************************************************************
 *
 *  $Id: myutil.h 1.24 2022/12/21 16:00:50 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for myutil.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: myutil.h $
 *  Revision 1.24  2022/12/21 16:00:50  martin.burnicki
 *  Updated some comments.
 *  Revision 1.23  2019/08/09 12:04:12  martin
 *  Added _inrange_s() macro and associated structures.
 *  Updated some doxygen comments.
 *  Revision 1.22  2018/11/21 16:04:27  martin
 *  Doxygen fixes.
 *  Revision 1.21  2018/11/01 11:10:57  martin
 *  Updated function prototypes.
 *  Revision 1.20  2017/03/22 15:09:03Z  andre.hartmann
 *  added macros for lin. interpolation
 *  Revision 1.19  2017/01/27 12:24:08Z  martin
 *  Moved STRINGIFY() macro to words.h.
 *  Revision 1.18  2014/10/20 12:30:38  martin
 *  Moved macro _isdigit() to words.h.
 *  Revision 1.17  2014/07/15 06:19:48Z  andre
 *  added function _sgn( _x ), returns sign of _x
 *  Revision 1.16  2013/12/16 13:32:57Z  martin
 *  Don't redefine MIN and MAX.
 *  Revision 1.15  2012/10/02 18:51:52  martin
 *  Updated handling of pragma pack().
 *  Revision 1.14  2011/02/16 14:02:35  martin
 *  Added STRINGIFY() macro.
 *  Revision 1.13  2010/12/13 15:59:39  martin
 *  Moved definition of macro _frac() here.
 *  Revision 1.12  2008/01/30 10:28:17Z  martin
 *  Moved some macro definitions to words.h.
 *  Revision 1.11  2004/11/09 14:20:24Z  martin
 *  Redefined some data types using C99 fixed-size definitions.
 *  Removed duplicate definition of macro _mask().
 *  Revision 1.10  2004/04/14 08:57:59Z  martin
 *  Pack structures 1 byte aligned.
 *  Revision 1.9  2003/05/20 10:22:25Z  MARTIN
 *  Corrected endianess of union UL for CC51.
 *  Revision 1.8  2002/09/03 13:40:43  MARTIN
 *  New macros _memfill() and _memclr().
 *  Revision 1.7  2002/03/14 13:45:56  MARTIN
 *  Changed type CSUM from short to ushort.
 *  Revision 1.6  2002/03/05 14:14:21  MARTIN
 *  New macro _isdigit() to avoid inclusion of ctype.h.
 *  Revision 1.5  2002/01/25 10:54:26  MARTIN
 *  Added some useful macros.
 *  Revision 1.4  2001/03/30 09:07:52  Andre
 *  union UL byte order set to Big Endian if SH2 is used
 *  Revision 1.3  2000/08/18 07:22:07Z  MARTIN
 *  Modified the _csum() macro to support far data objects.
 *  Revision 1.2  2000/07/21 13:50:49  MARTIN
 *  Added some definitions and macros.
 *
 **************************************************************************/

#ifndef _MYUTIL_H
#define _MYUTIL_H


/* Other headers to be included */

#include <words.h>
#include <use_pack.h>


// _CS_FAR should be defined far if the csum of far data
// structures must be computed
#if !defined( _CSFAR )
  #define _CSFAR
#endif


#ifdef _MYUTIL
 #define _ext
#else
 #define _ext extern
#endif


/* Start of header body */

#if defined( _USE_PACK )
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif


#if MBG_TGT_HAS_64BIT_TYPES
  #define _frac( _x ) ( ( (_x) == 0.0 ) ? 0.0 : ( (_x) - (double) ( (int64_t) (_x) ) ) )
#else
  #define _frac( _x ) ( ( (_x) == 0.0 ) ? 0.0 : ( (_x) - (double) ( (long) (_x) ) ) )
#endif

/// Return a pointer to the end of a string.
#define _eos( _s )  ( &(_s)[strlen( _s )] )

#if !defined( MIN )
  #define MIN( _x, _y )      ( ( (_x) < (_y) ) ? (_x) : (_y) )
#endif

#if !defined( MAX )
  #define MAX( _x, _y )      ( ( (_x) > (_y) ) ? (_x) : (_y) )
#endif

#define SWAP( _x, _y )     { temp = (_x); (_x) = (_y); (_y) = temp; }
#define SQR( _x )          ( (_x) * (_x) )

#define DP                 (double *)

#define bcd_from_bin( _x )  ( ( ( (_x) / 10 ) << 4 ) | ( (_x) % 10 ) )
#define bin_from_bcd( _x )  ( ( ( (_x) >> 4 ) * 10 ) + ( (_x) & 0x0F ) )


typedef union
{
  uint32_t ul;

  struct
  {
    #if defined( _CC51 ) || defined( _SH2 )
      uint16_t hi;  // big endian
      uint16_t lo;
    #else
      uint16_t lo;  // little endian
      uint16_t hi;
    #endif
  } us;

} UL;


#ifndef _CSUM_DEFINED
  typedef uint16_t CSUM;
  #define _CSUM_DEFINED
#endif


/// Compute the checksum of a structure.
#define _csum( _p )            checksum( (void _CSFAR *)(_p), sizeof( *(_p) ) )

/// Set a structure's checksum, i.e. the csum field.
#define _set_csum( _p )        (_p)->csum = _csum( (_p) )

/// Compare a structure's computed checksum with its csum field.
#define _valid_csum( _p )      ( (_p)->csum  == _csum( (_p) ) )

/// Generic check if a value is in range.
#define _inrange( _val, _min, _max ) \
                ( ( (_val) >= (_min) ) && ( (_val) <= (_max) ) )


/**
 * @brief A structure used to define a set of limits of type 'double'.
 */
typedef struct
{
  double min_val;
  double max_val;

} LIMITS_D;


/**
 * @brief A structure used to define a set of limits of type 'long'.
 *
 */
typedef struct
{
  long min_val;
  long max_val;

} LIMITS_L;


/**
 * @brief A structure used to define a set of limits of type 'int'.
 *
 */
typedef struct
{
  int min_val;
  int max_val;

} LIMITS_I;


/**
 * @brief Check if a value is in a given range specified by a structure.
 *
 * @param[in]  _val  The value to be checked.
 * @param[in]  _p    Address of a structure defining the limits.
 *
 * @see ::LIMITS_D
 * @see ::LIMITS_L
 * @see ::LIMITS_I
 */
#define _inrange_s( _val, _p ) \
                  ( ( (_val) >= (_p)->min_val ) && ( (_val) <= (_p)->max_val ) )



/// Return a bit mask with @a n LSBs set to 1.
#define _mask( _n ) \
  ( ( 1UL << (_n) ) - 1 )

/// Return a bit mask with the @a i th LSB set to 1.
#define _idx_bit( _i ) \
  ( 1UL << (_i) )

/// Check if the @a i th bit is set in a mask @a _msk.
#define _is_supported( _i, _msk ) \
  ( ( (_msk) & _idx_bit( _i ) ) != 0 )

/// Return the sign of a number.
#define _sgn( _x )  ( ( ( _x ) < 0 ) ? -1 : 1 )


/// Macro for linear interpolation y = _m * x + _b between two points ( X1; Y1 ), ( X2; Y2 )
#define _m( _Y1, _X1, _Y2, _X2 )  ( ( _Y2 -_Y1 ) / ( _X2 -_X1 ) )
#define _b( _Y1, _X1, _Y2, _X2 )  ( ( ( _Y1 * _X2 ) - ( _Y2 * _X1 ) ) / ( _X2 -_X1 ) )


/**
 * @brief Copy a string safely.
 *
 * Copy a string while taking care not to write past the
 * end of the destination buffer, and making sure the string
 * is properly terminated by 0.
 *
 * @see ::sn_cpy_str_safe
 */
#define _strncpy_0( _dst, _src ) \
{                                \
  int n = sizeof( _dst ) - 1;    \
                                 \
  strncpy( _dst, _src, n );      \
  (_dst)[n] = 0;                 \
}


/*
 * The macros below set a memory range used by a variable
 * to a specified value, avoiding the need to type the name
 * twice for base address and size.
 */
#define _memfill( _p, _v ) \
        memset( _p, _v, sizeof( *(_p) ) )

#define _memclr( _p ) \
  _memfill( _p, 0 )



// generate a DOS idle interrupt to release CPU time
#define _dos_idle() geninterrupt( 0x28 )


#ifdef __cplusplus
extern "C" {
#endif

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Replace whitespace ' ' characters by 0 characters.
 *
 * @param[in,out] s  The string to be modified.
 */
 void spaces_to_zeros( char *s ) ;

 /**
 * @brief Create a simple checksum.
 *
 * @param[in] vp  Pointer to a data block
 * @param[in] n   Size of the datablock to calulate the checksum from.
 *
 * @return  The computed checksum.
 */
 CSUM checksum( const void _CSFAR *vp, int n ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext


#endif  /* _MYUTIL_H */
