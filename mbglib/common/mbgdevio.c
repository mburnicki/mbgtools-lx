
/**************************************************************************
 *
 *  $Id: mbgdevio.c 1.60 2023/12/05 20:21:39 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Functions called from user space to access Meinberg device drivers.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgdevio.c $
 *  Revision 1.60  2023/12/05 20:21:39  martin.burnicki
 *  New functions mbg_set_and_check_gps_ant_cable_len
 *  and mbg_set_and_check_tr_distance.
 *  Revision 1.59  2023/02/23 10:51:56  martin.burnicki
 *  Fixed a bug in mbg_save_all_irig_rx_settings(), which incorrectly
 *  checked for the presence of a timecode output instead of
 *  a timecode input.
 *  Also updated some comments.
 *  Revision 1.58  2022/12/21 14:55:38  martin.burnicki
 *  Quieted a potential compiler warning.
 *  Revision 1.57  2022/08/23 15:59:09  martin.burnicki
 *  Use feature check functions instead of macros.
 *  Updated some doxygen comments.
 *  Quieted some compiler warnings.
 *  Revision 1.56  2022/06/23 15:01:51  martin.burnicki
 *  Fixed functions mbg_chk_dev_has_xmr, mbg_chk_dev_has_ptp_unicast,
 *  and mbg_chk_dev_has_gpio.
 *  Also some cleanup.
 *  Revision 1.55  2022/05/31 13:57:07  martin.burnicki
 *  Implement a mutex for SYN1588 on Windows.
 *  Revision 1.54  2022/04/27 15:01:24  martin.burnicki
 *  Replaced the old static function snprint_dev_name() by a
 *  new inline function mbg_create_dev_name_from_dev_info(),
 *  which is based on another new, more versatile inline
 *  function mbg_create_dev_name().
 *  Also changed some function parameter types from MBG_DEV_NAME
 *  to (const char *) because the definition of MBG_DEV_NAME
 *  has changed so that this string type is more versatile.
 *  Revision 1.53  2022/03/30 15:37:30  martin.burnicki
 *  Use new function setup_default_receiver_info() to set up
 *  a RECEIVER_INFO structure for devices that don't provide one,
 *  so also the device name is copied into the model_name field.
 *  Revision 1.52  2022/02/10 13:55:39  martin.burnicki
 *  Properly cast the status value passed via API call.
 *  Fixed some doxygen comments.
 *  Revision 1.51  2021/11/16 11:57:23  martin.burnicki
 *  Added SYN1588 support for mbg_get_time_info_hrt()
 *  and mbg_get_time_info_tstamp().
 *  Revision 1.50  2021/11/08 21:28:18  martin.burnicki
 *  SYN1588 devices are now enumerated after native Meinberg devices.
 *  New functions mbg_chk_if_syn1588_supported(), mbg_get_syn1588_type_name()
 *  mbg_chk_dev_is_syn1588_type() to avoid conditional compilation
 *  in other source modules.
 *  New function mbg_get_syn1588_ptp_sync_status() to retrieve
 *  the SYN1588 PTP_SYNC_STATUS.
 *  New functions mbg_chk_dev_fn_is_valid() and mbg_dev_info_from_dev_idx()
 *  for cleaner device handling.
 *  Revision 1.49  2021/11/08 18:01:54  martin.burnicki
 *  Account for modified MBG_DEV_FN type.
 *  Revision 1.48  2021/05/04 21:11:15  martin
 *  Merged SYN1588 support from an earlier branch.
 *  Revision 1.47  2021/04/29 15:06:21  martin
 *  Support reading device CPU info.
 *  Revision 1.46  2021/04/29 14:20:59  martin
 *  Variable pc_cycles_frequency was renamed to mbg_pc_cycles_frequency.
 *  Revision 1.45  2021/03/22 10:11:19  martin
 *  Updated a bunch of comments.
 *  Revision 1.44  2021/03/12 14:18:12  martin
 *  Made variable pc_cycles_frequency global.
 *  mbg_init_pc_cycles_frequency() now provides a return code.
 *  Updated a bunch of comments.
 *  Revision 1.43  2020/02/21 15:33:31  martin
 *  Added more feature check functions. Some feature checks are now
 *  done by the probe routine of the kernel driver.
 *  Fixed an evil bug in mbg_chk_dev_has_ttm_time(), which reported
 *  that reading TTM was supported when in fact wasn't. Trying then to
 *  read TTM could lock up the computer, e.g. in case of a PZF180PEX
 *  card with an old firmware.
 *  Renamed some local variables for clearer code.
 *  Reordered two functions.
 *  Removed inclusion of obsolete header pcpsutil.h.
 *  Doxygen changes.
 *  Revision 1.42  2018/11/22 14:21:19  martin
 *  Use new preprocessor symbol MBG_TGT_USE_IOCTL instead of
 *  obsolete MBG_USE_KERNEL_DRIVER.
 *  Revision 1.41  2018/09/21 08:57:32  martin
 *  Updated some comments.
 *  Revision 1.40  2018/07/05 08:46:20Z  martin
 *  Fixed function mbg_open_device_by_name() which failed
 *  with short device names.
 *  Fixed function mbg_chk_dev_is_lwr().
 *  Open all device nodes with FILE_FLAG_OVERLAPPED on Windows.
 *  mbg_dev_fn_from_dev_idx() returns a generic device name
 *  instead of the hardware ID string on Windows.
 *  Added missing linkage attribute for mbg_open_device().
 *  Added function mbg_split_dev_name().
 *  Added function mbg_chk_dev_has_dac_ctrl().
 *  Also accept '?' in S/N of uninitialized devices.
 *  Refactored implementation of deprecated feature check functions.
 *  Updated naming for device feature stuff.
 *  Removed obsolete size_t to int conversion code.
 *  Account for changes in mbgmutex.h.
 *  Removed usage of obsolete symbol MBGDEVIO_SIMPLE.
 *  Moved checking of the receiver info to an extra function.
 *  Updated and added some comments.
 *  Revision 1.39  2017/07/05 15:02:47  martin
 *  Lots of new API functions provided by thomas-b, philipp, and martin.
 *  Fixed a potential buffer overflow in mbg_open_device_by_name().
 *  Fixed a bug where XYZ and LLA were not written properly
 *  on target systems not using a kernel driver.
 *  Proper return codes from thread functions.
 *  PCPS_DEV * parameter for mbg_setup_receiver_info may now be NULL.
 *  New set of functions to determine if a specific feature is supported.
 *  The functions are named mbg_chk_dev_...(), are  combined in a group
 *  mbgdevio_chk_supp_fncs, and replace and deprecate a set of older
 *  functions named mbg_chk_dev_(), which are collected in a group
 *  mbgdevio_chk_supp_fncs_deprecated. The new functions are much easier
 *  to be handled, and return a single, distinct return code which clearly
 *  tells if a feature is not supported, or an error occurred while trying
 *  to find this out.
 *  Older defines N_SUPP_DEV, PCPS_MAX_DDEVS, and MBG_MAX_DEVICES
 *  have been obsoleted by new defines N_SUPP_DEV_BUS, N_SUPP_DEV_EXT,
 *  and N_SUPP_DEV_TOTAL.
 *  Defined many macros in a safer way, and replaced some macros
 *  by inline functions.
 *  Improved doxygen comments.
 *  Revision 1.38  2013/11/08 15:04:54  martin
 *  Fixes for big endian targets.
 *  Revision 1.37  2013/09/26 08:54:22  martin
 *  Support GNSS API.
 *  Support mbg_open_device_by_name() on Unix.
 *  Updated doxygen comments.
 *  Revision 1.36  2012/10/02 18:37:09Z  martin
 *  There are some g++ versions which fail to compile source code using
 *  the macros provided by Linux to define IOCTL codes. If only the API
 *  functions are called by an application, the IOCTL codes aren't
 *  required anyway, so we just avoid inclusion of mbgioctl.h in
 *  general and but include it when this module is built.
 *  Updated doxygen comments.
 *  New functions mbg_get_all_irig_rx_info() and
 *  mbg_save_all_irig_rx_settings().
 *  Account for renamed macros.
 *  Support on-board event logs.
 *  New functions mbg_dev_has_debug_status() and
 *  mbg_get_debug_status().
 *  Added a workaround to make mbgmon (BC) build on Windows.
 *  In mbg_get_serial_settings() return an error if the number of provided
 *  serial ports or supported string types exceeds the numbers supported
 *  by the driver.
 *  Moved mbg_get_serial_settings() and mbg_save_serial_settings()
 *  to an extra file. These functions expect parameters the sizes of
 *  which might change in future API versions, which would make
 *  functions exported by shared libraries incompatible across versions.
 *  Made functions mbg_get_gps_all_port_info() and
 *  mbg_get_gps_all_str_type_info() non-static so they are now exported
 *  by the shared libraries built from this module.
 *  Include cfg_hlp.h.
 *  Conditionally use older IOCTL request buffer structures.
 *  Moved some macros etc. to the .h file.
 *  Modified generic IOCTL handling such that for calls requiring variable sizes
 *  a fixed request block containing input and output buffer pointers and sizes is
 *  passed down to the kernel driver. This simplifies implementation on *BSD
 *  and also works for other target systems.
 *  Support reading CORR_INFO, and reading/writing TR_DISTANCE.
 *  Added mbg_dev_has_pzf() function.
 *  Support PTP unicast configuration.
 *  Account for some IOCTL codes renamed to follow common naming conventions.
 *  Renamed mutex stuff to critical sections.
 *  Added API calls to support PTP unicast.
 *  Specify I/O range number when calling port I/O macros
 *  so they can be used for different ranges on BSD.
 *  Modifications to support FreeBSD.
 *  Moved definition of MBG_HW_NAME to the header file.
 *  Compute PC cycles frequency on Linux if cpu_tick is not set by the kernel.
 *  Fixed a bug that kept the function mbg_open_device_by_name in a loop under certain conditions.
 *  Made xhrt leap second check an inline function.
 *  Revision 1.35  2010/01/12 13:40:25  martin
 *  Fixed a typo in mbg_dev_has_raw_irig_data().
 *  Revision 1.34  2009/12/15 15:34:58Z  daniel
 *  Support reading the raw IRIG data bits for firmware versions
 *  which support this feature.
 *  Revision 1.33  2009/09/29 15:08:40Z  martin
 *  Support retrieving time discipline info.
 *  Revision 1.32  2009/08/17 13:46:29  martin
 *  Moved specific definition of symbol _HAVE_IOCTL_WITH_SIZE
 *  to mbgioctl.h and renamed it to _MBG_SUPP_VAR_ACC_SIZE.
 *  Revision 1.31  2009/08/12 14:28:26  daniel
 *  Included PTP functions in build.
 *  Revision 1.30  2009/06/19 12:19:41Z  martin
 *  Support reading raw IRIG time.
 *  Revision 1.29  2009/06/08 18:23:22  daniel
 *  Added PTP configuration functions and PTP state functions, but
 *  they are still excluded from build.
 *  Added calls to support simple LAN interface configuration.
 *  Revision 1.28  2009/03/19 15:30:04  martin
 *  Added support for configurable time scales.
 *  Support reading/writing GPS UTC parameters.
 *  Support reading IRIG control function bits.
 *  Support reading MM timestamps without cycles.
 *  Fixed endianess correction in mbg_get_gps_pos().
 *  Endianess correction for ASIC version and ASIC features
 *  is now done by the kernel driver.
 *  Use generic cycles types and functions in mbg_get_default_cycles_frequency().
 *  mbg_tgt.h is now included in mbgdevio.h.
 *  Account for renamed IOCTL codes.
 *  Revision 1.27  2008/12/17 10:37:37  martin
 *  Fixed a bug in mbg_open_device_by_name() with sel. mode MBG_MATCH_ANY.
 *  Support variable read buffer sizes on Linux, so
 *  mbg_get_all_port_info() and mbg_get_all_str_type_info()
 *  can now be used on Linux.
 *  Support PC cycles on Linux via inline rdtsc call.
 *  Use predefined constants to convert fractions.
 *  New API calls mbg_get_fast_hr_timestamp_cycles(), and
 *  mbg_get_fast_hr_timestamp_comp() which take memory mapped HR time stamps
 *  in kernel space, and mbg_dev_has_fast_hr_timestamp() to check whether
 *  this is supported by a device.
 *  Removed mm_*() functions since these are obsolete now.
 *  Support extrapolated HR time (xhrt) for Windows and Linux
 *  by providing a function mbg_xhrt_poll_thread_create() which
 *  starts a poll thread for a specific device which to read
 *  HR time plus associated cycles in regular intervals.
 *  Added functions mbg_get_process_affinity(), mbg_set_process_affinity(),
 *  and mbg_set_current_process_affinity_to_cpu(), and mbg_create_thread()
 *  and mbg_set_thread_affinity() to control the extrapolation feature.
 *  Use new preprocessor symbol MBGDEVIO_HAVE_THREAD_AFFINITY.
 *  Added new functions mbg_get_xhrt_time_as_pcps_hr_time() and
 *  mbg_get_xhrt_time_as_filetime() (Windows only) to retrieve
 *  fast extrapolated timestamps.
 *  Added function mbg_get_xhrt_cycles_frequency() to retrieve the
 *  cycles counter frequency computed by the polling thread.
 *  Added function mbg_get_default_cycles_frequency_from_dev(),
 *  and mbg_get_default_cycles_frequency() (Windows only).
 *  Moved mbg_open_device..() functions upwards.
 *  Made device_info_list common.
 *  New functions mbg_dev_is_msf(), mbg_dev_is_wwvb(), mbg_dev_is_lwr(),
 *  mbg_dev_has_asic_version(), mbg_dev_has_asic_features(),
 *  and mbg_get_irq_stat_info().
 *  Exclude 2 more functions from build if symbol MBGDEVIO_SIMPLE is not 0.
 *  Account for MBG_VIRT_ADDR having beenrenamed to MBG_MEM_ADDR.
 *  Account Linux for device names renamed from /dev/mbgclk to /dev/mbgclock.
 *  Support bigendian target platforms.
 *  Revision 1.26  2008/02/26 16:54:21  martin
 *  New/changed functions for memory mapped access which are
 *  currently excluded from build.
 *  Changed separator for device names from ' ' to '_'.
 *  Added new type MBG_HW_NAME.
 *  Comment cleanup for doxygen.
 *  Revision 1.25  2008/02/04 13:42:45Z  martin
 *  Account for preprocessor symbol MBG_TGT_SUPP_MMAP.
 *  Revision 1.24  2008/01/31 08:31:40Z  martin
 *  Picked up changes from 1.19.1.2:
 *  On DOS detect and disable any TSR while searching for devices.
 *  Exclude some complex configuration API calls from build
 *  if MBGDEVIO_SIMPLE is defined != 0.
 *  Revision 1.23  2008/01/30 10:32:35Z  daniel
 *  Renamed mapped memory funtions.
 *  Revision 1.22  2008/01/25 15:27:42Z  daniel
 *  Fixed a bug in mbg_get_hr_time_comp() where an overflow
 *  of the fractions was handled with wrong sign.
 *  Revision 1.21  2008/01/17 15:49:59Z  daniel
 *  Added functions mbg_find_devices_with_hw_id() and
 *  mbg_free_devics_list() to work with Linux Win32 OSs.
 *  Added Doxygen compliant comments for API functions.
 *  Support for mapped memory I/O on linux and Windows.
 *  Added functions mbg_get_mapped_memory_info(),
 *  mbg_unmap_mapped_memory() and
 *  mbg_get_hr_timestamp_memory_mapped().
 *  Account for PCI_ASIC_FEATURES.
 *  Cleanup for PCI ASIC version.
 *  Revision 1.20  2007/09/27 07:31:12  daniel
 *  Moved declaration of portable inline specifier to mbg_tgt.h.
 *  Support hotplugging of devices as used with USB by Daniel's new
 *  functions mbg_find_devices_with_names(), mbg_free_device_name_list(),
 *  mbg_open_device_by_hw_id(), and mbg_open_device_by_name().
 *  In mbg_get_serial_settings() account for devices which have no
 *  serial port at all.
 *  Register event source if Windows DLL is loaded.
 *  Revision 1.19  2007/05/21 15:00:00Z  martin
 *  Unified naming convention for symbols related to ref_offs.
 *  Revision 1.18  2007/03/02 10:17:41Z  martin
 *  Use generic port I/O macros.
 *  Changes due to modified/renamed macros.
 *  Changes due to renamed library symbols.
 *  Preliminary support for *BSD.
 *  Revision 1.17  2006/05/02 13:15:37  martin
 *  Added mbg_set_gps_port_settings(), mbg_get_gps_all_pout_info(),
 *  mbg_set_gps_pout_settings_idx(), mbg_set_gps_pout_settings().
 *  Revision 1.16  2005/06/02 11:53:12Z  martin
 *  Implemented existing mbg_generic_..() functions.
 *  Added new functions mbg_generic_io() and mbg_dev_has_generic_io().
 *  Added new function mbg_get_synth_state().
 *  Partially use inline function.
 *  Changed order of some functions.
 *  More simplifications using macros.
 *  Unified macros for Win32 and Linux.
 *  Fixed warning on Win32 using type cast.
 *  Revision 1.15  2005/01/31 16:44:21Z  martin
 *  Added function mbg_get_hr_time_comp() which returns HR timestamp
 *  which has latency compensated.
 *  Revision 1.14  2005/01/14 10:22:23Z  martin
 *  Added functions which query device features.
 *  Revision 1.13  2004/12/09 11:23:59Z  martin
 *  Support configuration of on-board frequency synthesizer.
 *  Revision 1.12  2004/11/09 14:11:07Z  martin
 *  Modifications were required in order to be able to configure IRIG
 *  settings of devices which provide both IRIG input and output.
 *  Renamed functions mbg_get_irig_info() and mbg_set_irig_settings()
 *  to mbg_get_irig_rx_info() and mbg_set_irig_rx_settings()
 *  New functions mbg_get_irig_tx_info() and mbg_set_irig_tx_settings().
 *  All API functions now use well defined parameter types instead of
 *  generic types. Some new types have been defined therefore.
 *  Added a workaround for GPS169PCI cards with early firmware versions
 *  which used the same codes to configure the IRIG output as the TCR
 *  devices use to configure the IRIG input. Those codes are now
 *  exclusively used to configure the IRIG input. The workaround
 *  has been included in order to let GPS169PCI cards work properly
 *  after a driver update, without requiring a firmware update.
 *  The macro _pcps_ddev_requires_irig_workaround() is used to check
 *  if the workaround is required.
 *  Renamed function mbg_get_gps_stat() to mbg_get_gps_bvar_stat().
 *  Revision 1.11  2004/08/17 11:13:45Z  martin
 *  Account for renamed symbols.
 *  Revision 1.10  2004/04/14 09:39:17Z  martin
 *  Allow [g|s]et_irig_info() also for new devices with IRIG output.
 *  Use MBGDEVIO_COMPAT_VERSION to check version.
 *  Revision 1.9  2003/12/22 15:30:52Z  martin
 *  Added functions mbg_get_asic_version(), mbg_get_time_cycles(),
 *  and mbg_get_hr_time_cycles().
 *  Support higher baud rates for TCR510PCI and PCI510.
 *  Support PCPS_HR_TIME for TCR510PCI.
 *  API calls return ioctl results instead of success/-1.
 *  Moved some Win32 specific code to mbgsvctl DLL.
 *  Log Win32 ioctl errors to event log for debugging.
 *  Revision 1.8  2003/06/19 08:42:33Z  martin
 *  Renamed function mbg_clr_cap_buff() to mbg_clr_ucap_buff().
 *  New functions mbg_get_ucap_entries() and mbg_get_ucap_event().
 *  New function mbg_get_hr_ucap().
 *  New functions mbgdevio_get_version() and mbgdevio_check_version().
 *  New functions for generic read/write access.
 *  New functions mbg_get_pcps_tzdl() and mbg_set_pcps_tzdl().
 *  Fixed a bug passing the wrong command code to a
 *  direct access target in mbg_get_sync_time().
 *  Return driver info for direct access targets.
 *  Include pcpsdrvr.h and pcps_dos.h, if applicable.
 *  For direct access targets, check if a function is supported
 *  before accessing the hardware.
 *  Use const parameter pointers if applicable.
 *  Changes due to renamed symbols/macros.
 *  Source code cleanup.
 *  Revision 1.7  2003/05/16 08:52:46  MARTIN
 *  Swap doubles inside API functions.
 *  Enhanced support for direct access targets.
 *  Removed obsolete code.
 *  Revision 1.6  2003/04/25 10:14:16  martin
 *  Renamed macros.
 *  Extended macro calls for direct access targets.
 *  Updated macros for Linux.
 *  Revision 1.5  2003/04/15 19:35:25Z  martin
 *  New functions mbg_setup_receiver_info(),
 *  mbg_get_serial_settings(), mbg_save_serial_settings().
 *  Revision 1.4  2003/04/09 16:07:16Z  martin
 *  New API functions mostly complete.
 *  Use renamed IOCTL codes from mbgioctl.h.
 *  Added DllEntry function for Win32.
 *  Made MBG_Device_count and MBG_Device_Path local.
 *  Revision 1.3  2003/01/24 13:44:40Z  martin
 *  Fixed get_ref_time_from_driver_at_sec_change() to be used
 *  with old kernel drivers.
 *  Revision 1.2  2002/09/06 11:04:01Z  martin
 *  Some old API functions have been replaced by new ones
 *  for a common PnP/non-PnP API.
 *  New API function which clears capture buffer.
 *  New function get_ref_time_from_driver_at_sec_change().
 *  Revision 1.1  2002/02/19 13:48:20Z  MARTIN
 *  Initial revision
 *
 **************************************************************************/

#define _MBGDEVIO
 #include <mbgdevio.h>
#undef _MBGDEVIO

#include <gpsutils.h>
#include <mbgerror.h>
#include <cfg_hlp.h>
#include <str_util.h>

#if SUPP_SYN1588_USR_SPC
  #include <mbg_syn1588_io.h>
#endif

#if defined( MBG_TGT_DOS_PM )
  #include <mbg_dpmi.h>
#endif


#if !MBG_TGT_USE_IOCTL

  #include <pcpsdrvr.h>
  #include <pci_asic.h>

  static PCPS_DRVR_INFO drvr_info = { MBGDEVIO_VERSION, 0, "MBGDEVIO direct" };

#endif


// Target specific code for different environments.

#if defined( MBG_TGT_WIN32 )

  #include <mbgsvctl.h>
  #include <mbgnames.h>
  #include <pci_asic.h>
  #include <timecnv.h>

  #include <tchar.h>
  #include <stdio.h>

#elif defined( MBG_TGT_POSIX ) && !defined( MBG_TGT_QNX_NTO )

  #include <unistd.h>
  #include <stdlib.h>
  #include <string.h>

  #if defined( DEBUG )
    #include <stdio.h>       // printf() debug output
  #endif

#else // Other target OSs which access the hardware directly.

  #if defined( MBG_TGT_QNX_NTO )
    #include <stdio.h>
    #include <sys/neutrino.h>
  #endif

  #if MBG_USE_DOS_TSR
    #include <pcps_dos.h>
  #else
    #define pcps_read_safe            _pcps_read
    #define pcps_write_safe           pcps_write
    #define pcps_read_gps_safe        pcps_read_gps
    #define pcps_write_gps_safe       pcps_write_gps

    #define _pcps_write_byte_safe     _pcps_write_byte
    #define _pcps_read_var_safe       _pcps_read_var
    #define _pcps_write_var_safe      _pcps_write_var
    #define _pcps_read_gps_var_safe   _pcps_read_gps_var
    #define _pcps_write_gps_var_safe  _pcps_write_gps_var
  #endif

#endif  // end of target specific code


#if SUPP_SYN1588_USR_SPC
  static int mbg_devices_found;
#endif


#if !defined( _MBG_SUPP_VAR_ACC_SIZE )
  // If this symbol has not yet been defined, mbgioctl.h has probably
  // not yet been included. Variable buffer sizes are usually supported
  // on target systems where the hardware is accessed directly without
  // a kernel driver, so we set the default to 1.
  #define _MBG_SUPP_VAR_ACC_SIZE   1
#endif


#if !defined( _mbgdevio_chk_cond )
  // If the macro has not been defined previously, it may not
  // be required for the target environment and is defined
  // to no operation.
  #define _mbgdevio_chk_cond( _cond ) _nop_macro_fnc()
#endif



#define _mbgdevio_read_chk( _dh, _cmd, _ioctl, _p, _sz, _cond ) \
do                                                              \
{                                                               \
  _mbgdevio_chk_cond( _cond );                                  \
  rc = _do_mbgdevio_read( _dh, _cmd, _ioctl, _p, _sz );         \
} while ( 0 )

#define _mbgdevio_read_var_chk( _dh, _cmd, _ioctl, _p, _cond ) \
do                                                             \
{                                                              \
  _mbgdevio_chk_cond( _cond );                                 \
  rc = _mbgdevio_read_var( _dh, _cmd, _ioctl, _p );            \
} while ( 0 )

#define _mbgdevio_write_var_chk( _dh, _cmd, _ioctl, _p, _cond ) \
do                                                              \
{                                                               \
  _mbgdevio_chk_cond( _cond );                                  \
  rc = _mbgdevio_write_var( _dh, _cmd, _ioctl, _p );            \
} while ( 0 )

#define _mbgdevio_write_cmd_chk( _dh, _cmd, _ioctl, _cond ) \
do                                                          \
{                                                           \
  _mbgdevio_chk_cond( _cond );                              \
  rc = _mbgdevio_write_cmd( _dh, _cmd, _ioctl );            \
} while ( 0 )

#define _mbgdevio_read_gps_chk( _dh, _cmd, _ioctl, _p, _sz, _cond ) \
do                                                                  \
{                                                                   \
  _mbgdevio_chk_cond( _cond );                                      \
  rc = _do_mbgdevio_read_gps( _dh, _cmd, _ioctl, _p, _sz );         \
} while ( 0 )

#define _mbgdevio_read_gps_var_chk( _dh, _cmd, _ioctl, _p, _cond ) \
do                                                                 \
{                                                                  \
  _mbgdevio_chk_cond( _cond );                                     \
  rc = _mbgdevio_read_gps_var( _dh, _cmd, _ioctl, _p );            \
} while ( 0 )

#define _mbgdevio_write_gps_var_chk( _dh, _cmd, _ioctl, _p, _cond ) \
do                                                                  \
{                                                                   \
  _mbgdevio_chk_cond( _cond );                                      \
  rc = _mbgdevio_write_gps_var( _dh, _cmd, _ioctl, _p );            \
} while ( 0 )



/*HDR*/
/**
 * @brief Get the version number of the precompiled DLL/shared object library.
 *
 * If this library is used as a DLL/shared object library, the version
 * number can be checked to see if the header files which are actually used
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API function are called
 * in the correct way.
 *
 * @return The version number.
 *
 * @see ::mbgdevio_check_version
 * @see ::MBGDEVIO_VERSION defined in mbgdevio.h
 */
_MBG_API_ATTR int _MBG_API mbgdevio_get_version( void )
{
  return MBGDEVIO_VERSION;

}  // mbgdevio_get_version



/*HDR*/
/**
 * @brief Check if the DLL/shared library is compatible with a given version.
 *
 * If this library is used as a DLL/shared object library, the version
 * number can be checked to see if the header files which are actually used
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API functions are called
 * in the correct way.
 *
 * @param[in] header_version  Version number to be checked, should be ::MBGDEVIO_VERSION
 *                            from the mbgdevio.h file version used to build the application
 *
 * @return ::MBG_SUCCESS if compatible, else ::MBG_ERR_LIB_NOT_COMPATIBLE.
 *
 * @see ::mbgdevio_get_version
 * @see ::MBGDEVIO_VERSION defined in mbgdevio.h
 */
_MBG_API_ATTR int _MBG_API mbgdevio_check_version( int header_version )
{
  if ( header_version >= MBGDEVIO_COMPAT_VERSION )
    return MBG_SUCCESS;

  return MBG_ERR_LIB_NOT_COMPATIBLE;

}  // mbgdevio_check_version



static __mbg_inline /*HDR*/
int err_syn1588_not_supp( void )
{
  #if defined( MBG_TGT_LINUX ) || defined( MBG_TGT_WIN32 )
    // Indicate that SYN1588 support could be available,
    // but the application has been built without it.
    return MBG_ERR_NOT_SUPP_BY_DRVR;
  #else
    // Indicate that SYN1588 support is not possible
    // at all on this target system.
    return MBG_ERR_NOT_SUPP_ON_OS;
  #endif

}  // err_syn1588_not_supp



/*HDR*/
/**
 * @brief Check if support for SYN1588 devices is available.
 *
 * @return ::MBG_SUCCESS if SYN1588 support is available,<br>
 *         ::MBG_ERR_NOT_SUPP_BY_DRVR if driver has been built without SYN1588 support, or<br>
 *         ::MBG_ERR_NOT_SUPP_ON_OS if SYN1588 devices are not at all supported on the OS.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
_MBG_API_ATTR int _MBG_API mbg_chk_if_syn1588_supported( void )
{
  #if SUPP_SYN1588
    return MBG_SUCCESS;
  #else
    return err_syn1588_not_supp();
  #endif

}  // mbg_chk_if_syn1588_supported



static /*HDR*/
/**
 * @brief A generic function to implement @ref mbgdevio_chk_supp_fncs.
 *
 * This function is used to implement the specific, global
 * feature check functions that can be called by applications.
 * See @ref mbgdevio_chk_supp_fncs.
 *
 * @param[in]  dh         Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  feat_type  One of the defined ::DEV_FEAT_TYPES.
 * @param[in]  feat_num   A feature number with range and meaning depending on the value of feat_type.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, see @ref mbgdevio_chk_supp_fncs
 *
 * @see @ref mbgdevio_chk_supp_fncs
 * @see ::do_chk_dev_feat_deprecated
 */
int do_chk_dev_feat( MBG_DEV_HANDLE dh, uint32_t feat_type, uint32_t feat_num )
{
  #if defined( _MBGIOCTL_H )
  {
    // This target uses a kernel driver, so we send an IOCTL
    // to the kernel driver to retrieve the information.
    MBGDEVIO_RET_VAL rc;
    IOCTL_DEV_FEAT_REQ req;
    req.feat_type = feat_type;
    req.feat_num = feat_num;
    rc = _do_mbg_ioctl( dh, IOCTL_CHK_DEV_FEAT,
                        &req, sizeof( req ), 0 );
    #if 0 && DEBUG
    {
      int rc2 = _mbgdevio_cnv_ret_val( rc );
      printf( "feat ioctl %lu:%lu ret: %i --> %i\n",
              (ulong) feat_type, (ulong) feat_num, rc, rc2 );
      return rc2;
    }
    #else
      return _mbgdevio_cnv_ret_val( rc );
    #endif
  }
  #else
    // This target doesn't use a kernel driver, so we directly call
    // the function that would be called by the kernel driver.
    return pcps_chk_dev_feat( dh, feat_type, feat_num );
  #endif

}  // do_chk_dev_feat



/**
 * @brief A generic function to implement @ref mbgdevio_chk_supp_fncs_deprecated.
 *
 * This function is used to implement the deprecated specific, global
 * feature check functions that can be called by applications.
 * See @ref mbgdevio_chk_supp_fncs_deprecated.
 *
 * @param[in]  dh   Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  fnc  Pointer to the non-deprecated function which actually does the check.
 * @param[out] p    Pointer to an int which is updated if the API call succeeds, i.e ::MBG_SUCCESS is returned.
 *                  The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS if the feature request was executed without error
 *         and *p was updated to indicate if the requested feature is supported,
 *         or not, else one of the @ref MBG_ERROR_CODES.
 *
 * @see @ref mbgdevio_chk_supp_fncs
 * @see @ref mbgdevio_chk_supp_fncs_deprecated
 * @see ::do_chk_dev_feat_deprecated
 */
int do_chk_dev_feat_deprecated( MBG_DEV_HANDLE dh, MBG_CHK_SUPP_FNC *fnc, int *p )
{
  // Call the current feature check function.
  int rc = fnc( dh );
  int is_success = mbg_rc_is_success( rc );

  if ( is_success                         // definitely supported
  || ( rc == MBG_ERR_NOT_SUPP_BY_DEV ) )  // definitely NOT supported
  {
    // We could definitely determine if the requested feature
    // is supported or not, so we accordingly update the integer
    // variable the address of which has been passed by the caller,
    // and return MBG_SUCCESS since the information could be
    // retrieved successfully.
    *p = is_success;
    return MBG_SUCCESS;
  }

  // If execution goes here, there was some error trying to
  // determine the requested information, e.g. an IOCTL error
  // when accessing the kernel driver, so we return the
  // associated error code.
  return rc;

}  // do_chk_dev_feat_deprecated



#if defined( _MBGIOCTL_H )

static /*HDR*/
/**
 * @brief A generic function to implement @ref mbgdevio_chk_supp_fncs.
 *
 * This function is used to implement the specific, global
 * feature check functions that can be called by applications.
 * See @ref mbgdevio_chk_supp_fncs.
 *
 * @param[in]  dh         Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  feat_type  One of the defined ::DEV_FEAT_TYPES.
 * @param[in]  feat_num   A feature number with range and meaning depending on the value of feat_type.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, see @ref mbgdevio_chk_supp_fncs
 *
 * @see @ref mbgdevio_chk_supp_fncs
 * @see ::do_chk_dev_feat_deprecated
 */
int do_query_cond( MBG_DEV_HANDLE dh, unsigned int ioctl_code )
{
    MBGDEVIO_RET_VAL rc;
    int flag = 0;
    rc = _mbgdevio_read_var( dh, -1, ioctl_code, &flag );

    if ( mbg_rc_is_success( rc ) )     // no IOCTL error
      if ( flag == 0 )                 // if returned value is 0
        rc = MBG_ERR_NOT_SUPP_BY_DEV;  // not supp.

    // success or IOCTL error
    return _mbgdevio_cnv_ret_val( rc );

}  // do_query_cond

#define _do_query_cond( _dh, _cond, _ioctl )  \
  do_query_cond( _dh, _ioctl )

#else

#define _do_query_cond( _dh, _cond, _ioctl )  \
  _cond( _dh ) ? MBG_SUCCESS : MBG_ERR_NOT_SUPP_BY_DEV

#endif



/*HDR*/
/**
 * @brief Check if a device supports the ::RECEIVER_INFO structure and related calls.
 *
 * Very old devices may not provide a ::RECEIVER_INFO structure.
 * The ::mbg_setup_receiver_info call should be used preferably to set up
 * a ::RECEIVER_INFO for a device. That function uses this call to determine
 * whether a ::RECEIVER_INFO can be read directly from a device, or sets up
 * a default structure using default values depending on the device type.
 *
 * @note This function should be preferred over ::mbg_dev_has_receiver_info,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_get_gps_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_receiver_info( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_RECEIVER_INFO );

}  // mbg_chk_dev_has_receiver_info

MBG_CHK_SUPP_FNC mbg_chk_dev_has_receiver_info;



/*HDR*/
/**
 * @brief Check if a device supports large configuration data structures.
 *
 * Such structures have been introduced with the first Meinberg GPS receivers.
 * Mostly all configuration structures are large data structures, and mostly all
 * current devices support this, but some old devices may not.
 *
 * @note This function should be preferred over ::mbg_dev_has_gps_data,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_gps_data( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_GPS_DATA_16 );

}  // mbg_chk_dev_has_gps_data

MBG_CHK_SUPP_FNC mbg_chk_dev_has_gps_data;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_generic_io API call.
 *
 * <b>Warning</b>: This call is for debugging purposes and internal use only!
 *
 * @note This function should be preferred over ::mbg_dev_has_generic_io,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_generic_io
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_generic_io( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_GENERIC_IO );

}  // mbg_chk_dev_has_generic_io

MBG_CHK_SUPP_FNC mbg_chk_dev_has_generic_io;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_asic_version API call.
 *
 * Whether ::mbg_get_asic_version is supported, or not, depends on
 * the bus interface chip assembled on the device.
 *
 * @note This function should be preferred over ::mbg_dev_has_asic_version,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_asic_version
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_asic_version( MBG_DEV_HANDLE dh )
{
  // TODO return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, ... );
  return _do_query_cond( dh, _pcps_ddev_has_asic_version, IOCTL_DEV_HAS_PCI_ASIC_VERSION );

}  // mbg_chk_dev_has_asic_version

MBG_CHK_SUPP_FNC mbg_chk_dev_has_asic_version;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_asic_features call.
 *
 * Whether ::mbg_get_asic_features is supported, or not, depends on
 * the bus interface chip assembled on the device.
 *
 * @note This function should be preferred over ::mbg_dev_has_asic_features,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_asic_features
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_asic_features( MBG_DEV_HANDLE dh )
{
  // TODO return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, ... );
  return _do_query_cond( dh, _pcps_ddev_has_asic_features, IOCTL_DEV_HAS_PCI_ASIC_FEATURES );

}  // mbg_chk_dev_has_asic_features

MBG_CHK_SUPP_FNC mbg_chk_dev_has_asic_features;



/*HDR*/
/**
 * @brief Check if a device provides eXtended Multi Ref (XMR) inputs.
 *
 * Devices providing XMR inputs can receive or decode different timing
 * signals in parallel, and the supported sources can be prioritized.
 *
 * @note This function should be preferred over ::mbg_dev_has_xmr,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_xmr( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_RI, GPS_FEAT_XMULTI_REF );

}  // mbg_chk_dev_has_xmr

MBG_CHK_SUPP_FNC mbg_chk_dev_has_xmr;



static /*HDR*/
/**
 * @brief Check if a device has a specific bus type.
 *
 * This function checks if the bus flags associated with
 * the device match the requested flags. This can be used e.g.
 * to distinguish if a device is a PCI or USB device.
 *
 * @param[in]  dh             Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  bus_flag_mask  The bus flags to be tested, see @ref PCPS_BUS_FLAG_MASKS.
 *
 * @return ::MBG_SUCCESS if the bus flags match, else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @see @ref PCPS_BUS_FLAG_MASKS
 */
int chk_bus_flags( MBG_DEV_HANDLE dh, int bus_flag_mask )
{
  PCPS_DEV dev_info;

  int rc = mbg_get_device_info( dh, &dev_info );

  if ( mbg_rc_is_error( rc ) )
    return rc;

  return ( dev_info.type.bus_flags & bus_flag_mask ) ? MBG_SUCCESS : MBG_ERR_NOT_SUPP_BY_DEV;

}  // chk_bus_flags



/*HDR*/
/**
 * @brief Check if the device is connected via the ISA bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the ISA bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_isa( MBG_DEV_HANDLE dh )
{
  return chk_bus_flags( dh, PCPS_BUS_ISA );

}  // mbg_chk_dev_is_isa

MBG_CHK_SUPP_FNC mbg_chk_dev_is_isa;



/*HDR*/
/**
 * @brief Check if the device is connected via the MCA bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the MCA bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_mca( MBG_DEV_HANDLE dh )
{
  return chk_bus_flags( dh, PCPS_BUS_MCA );

}  // mbg_chk_dev_is_mca

MBG_CHK_SUPP_FNC mbg_chk_dev_is_mca;



/*HDR*/
/**
 * @brief Check if the device is connected via the PCI bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the PCI bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_pci( MBG_DEV_HANDLE dh )
{
  return chk_bus_flags( dh, PCPS_BUS_PCI );

}  // mbg_chk_dev_is_pci

MBG_CHK_SUPP_FNC mbg_chk_dev_is_pci;



/*HDR*/
/**
 * @brief Check if the device is connected via the PCI Express bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the PCI Express bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_pci_express( MBG_DEV_HANDLE dh )
{
  PCPS_DEV dev_info;

  int rc = mbg_get_device_info( dh, &dev_info );

  if ( mbg_rc_is_error( rc ) )
    return rc;

  // TODO Move this to the kernel driver.
  if ( ( dev_info.type.bus_flags == PCPS_BUS_PCI_MBGPEX ) ||
       ( dev_info.type.bus_flags == PCPS_BUS_PCI_PEX8311 ) )
    return MBG_SUCCESS;

  return MBG_ERR_NOT_SUPP_BY_DEV;

}  // mbg_chk_dev_is_pci_express

MBG_CHK_SUPP_FNC mbg_chk_dev_is_pci_express;



/*HDR*/
/**
 * @brief Check if the device is connected via the USB bus.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the device is connected via the USB bus,
 *         else ::MBG_ERR_NOT_SUPP_BY_DEV.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_usb( MBG_DEV_HANDLE dh )
{
  return chk_bus_flags( dh, PCPS_BUS_USB );

}  // mbg_chk_dev_is_usb

MBG_CHK_SUPP_FNC mbg_chk_dev_is_usb;



/*HDR*/
/**
 * @brief Check if a device is a GPS receiver.
 *
 * The function also returns ::MBG_SUCCESS for GNSS receivers
 * which can track GPS satellites beside other GNSS systems.
 *
 * A pure GPS receiver is not considered a GNSS receiver, but
 * each of Meinberg's GNSS receivers is also a GPS receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_gps,
 * which has been deprecated.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_gps( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_REF_TYPE, PCPS_REF_GPS );

}  // mbg_chk_dev_is_gps

MBG_CHK_SUPP_FNC mbg_chk_dev_is_gps;



/*HDR*/
/**
 * @brief Check if a device is a GNSS receiver.
 *
 * This is usually the case if a device supports reception of
 * different satellite systems, e.g. GPS, Glonass, Galileo, etc.
 *
 * A pure GPS receiver is not considered a GNSS receiver, but
 * each of Meinberg's GNSS receivers is also a GPS receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_gnss,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_set_gps_gnss_mode_settings
 * @see ::mbg_get_gps_all_gnss_sat_info
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_gnss( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_IS_GNSS );

}  // mbg_chk_dev_is_gnss

MBG_CHK_SUPP_FNC mbg_chk_dev_is_gnss;



/*HDR*/
/**
 * @brief Check if a device is a DCF77 receiver.
 *
 * Beside standard DCF77 receivers which receive the legacy AM signal,
 * there are also PZF receivers which can decode the pseudo-random phase
 * modulation and thus yield a higher accuracy. See ::mbg_chk_dev_has_pzf.
 *
 * @note This function should be preferred over ::mbg_dev_is_dcf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_chk_dev_is_lwr
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_dcf( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_REF_TYPE, PCPS_REF_DCF );

}  // mbg_chk_dev_is_dcf

MBG_CHK_SUPP_FNC mbg_chk_dev_is_dcf;



/*HDR*/
/**
 * @brief Check if a device supports demodulation of the DCF77 PZF code.
 *
 * Beside the enhanced PZF correlation receivers which decode the DCF77's
 * pseudo-random phase modulation to yield a better accuracy, there are also
 * legacy DCF77 receivers which just decode the standard AM signal.
 * See ::mbg_chk_dev_is_dcf.
 *
 * @note This function should be preferred over ::mbg_dev_has_pzf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_corr_info
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_chk_dev_is_dcf
 * @see ::mbg_chk_dev_is_lwr
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_pzf( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_PZF );

}  // mbg_chk_dev_has_pzf

MBG_CHK_SUPP_FNC mbg_chk_dev_has_pzf;



/*HDR*/
/**
 * @brief Check if a device is a MSF receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_msf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_lwr
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_msf( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_REF_TYPE, PCPS_REF_MSF );

}  // mbg_chk_dev_is_msf

MBG_CHK_SUPP_FNC mbg_chk_dev_is_msf;



/*HDR*/
/**
 * @brief Check if a device is a WWVB receiver.
 *
 * @note This function should be preferred over ::mbg_dev_is_wwvb,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_lwr
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_wwvb( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_REF_TYPE, PCPS_REF_WWVB );

}  // _pcps_ddev_is_wwvb

MBG_CHK_SUPP_FNC _pcps_ddev_is_wwvb;



/*HDR*/
/**
 * @brief Check if a device is a JJY receiver.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_lwr
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_jjy( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_REF_TYPE, PCPS_REF_JJY );

}  // _pcps_ddev_is_jjy

MBG_CHK_SUPP_FNC _pcps_ddev_is_jjy;



/*HDR*/
/**
 * @brief Check if a device is any long wave signal receiver.
 *
 * Long wave receivers include e.g. DCF77, MSF, WWVB, or JJY.
 *
 * @note This function should be preferred over ::mbg_dev_is_lwr,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_dcf
 * @see ::mbg_chk_dev_is_msf
 * @see ::mbg_chk_dev_is_wwvb
 * @see ::mbg_chk_dev_is_jjy
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_lwr( MBG_DEV_HANDLE dh )
{
  int rc = mbg_chk_dev_is_dcf( dh );

  if ( mbg_rc_is_success( rc ) )
    goto out;

  rc = mbg_chk_dev_is_msf( dh );

  if ( mbg_rc_is_success( rc ) )
    goto out;

  rc = mbg_chk_dev_is_wwvb( dh );

  if ( mbg_rc_is_success( rc ) )
    goto out;

  rc = mbg_chk_dev_is_jjy( dh );

  if ( mbg_rc_is_success( rc ) )
    goto out;

out:
  return rc;

}  // mbg_chk_dev_is_lwr

MBG_CHK_SUPP_FNC mbg_chk_dev_is_lwr;



/*HDR*/
/**
 * @brief Check if a device is a time code receiver (TCR, IRIG or similar).
 *
 * @note This function should be preferred over ::mbg_dev_is_irig_rx,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_rx_info
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_has_irig
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_tcr( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_REF_TYPE, PCPS_REF_IRIG );

}  // mbg_chk_dev_is_tcr

MBG_CHK_SUPP_FNC mbg_chk_dev_is_tcr;



/*HDR*/
/**
 * @brief Check if a device is a SYN1588 card.
 *
 * @note This function always fails if the driver package
 * has been built without support for SYN1588 cards.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle.
 *
 * @return ::MBG_SUCCESS if the device is a SYN1588 card,
 *         ::MBG_ERR_NO_DEV if is is not,<br>
 *         ::MBG_ERR_NOT_SUPP_BY_DRVR if driver has been built without SYN1588 support, or<br>
 *         ::MBG_ERR_NOT_SUPP_ON_OS if SYN1588 devices are not at all supported on the OS.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_is_syn1588_type( MBG_DEV_HANDLE dh )
{
  #if SUPP_SYN1588
    return is_mbg_syn1588_type( dh ) ? MBG_SUCCESS : MBG_ERR_NO_DEV;
  #else
    (void) dh;
    return err_syn1588_not_supp();
  #endif

}  // mbg_chk_dev_is_syn1588_type

MBG_CHK_SUPP_FNC mbg_chk_dev_is_syn1588_type;



/*HDR*/
/**
 * @brief Check if a device supports simple LAN interface API calls.
 *
 * @note This function should be preferred over ::mbg_dev_has_lan_intf,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_state
 * @see ::mbg_get_ip4_settings
 * @see ::mbg_set_ip4_settings
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_lan_intf( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_LAN_INTF );

}  // mbg_chk_dev_has_lan_intf

MBG_CHK_SUPP_FNC mbg_chk_dev_has_lan_intf;



/*HDR*/
/**
 * @brief Check if a device supports PTP configuration/status calls.
 *
 * @note This function should be preferred over ::mbg_dev_has_ptp,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_state
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ptp( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_PTP );

}  // mbg_chk_dev_has_ptp

MBG_CHK_SUPP_FNC mbg_chk_dev_has_ptp;



/*HDR*/
/**
 * @brief Check if a device supports PTP unicast feature/configuration.
 *
 * Not all devices which support PTP do also support PTP Unicast. This API
 * call checks if PTP Unicast is supported in addition to the standard
 * PTP multicast.
 *
 * @note This function should be preferred over ::mbg_dev_has_ptp_unicast,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_get_ptp_state
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ptp_unicast( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_RI, GPS_FEAT_PTP_UNICAST );

}  // mbg_chk_dev_has_ptp_unicast

MBG_CHK_SUPP_FNC mbg_chk_dev_has_ptp_unicast;



/*HDR*/
/**
 * @brief Check if a device supports the mbg_get_hr_time... functions.
 *
 * @note This function should be preferred over ::mbg_dev_has_hr_time,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_hr_time( MBG_DEV_HANDLE dh )
{
  #if SUPP_SYN1588_USR_SPC
    if ( chk_if_syn1588_type( dh ) )
      return MBG_SUCCESS;
  #endif

  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_HR_TIME );

}  // mbg_chk_dev_has_hr_time

MBG_CHK_SUPP_FNC mbg_chk_dev_has_hr_time;



/*HDR*/
/**
 * @brief Check if a device supports the mbg_get_fast_hr_timestamp... calls.
 *
 * @note This function should be preferred over ::mbg_dev_has_fast_hr_timestamp,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_fast_hr_timestamp_cycles
 * @see ::mbg_get_fast_hr_timestamp_comp
 * @see ::mbg_get_fast_hr_timestamp
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_fast_hr_timestamp( MBG_DEV_HANDLE dh )
{
  #if SUPP_SYN1588_USR_SPC
    if ( chk_if_syn1588_type( dh ) )
      return MBG_SUCCESS;
  #endif

  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_FAST_HR_TSTAMP );

}  // mbg_chk_dev_has_fast_hr_timestamp

MBG_CHK_SUPP_FNC mbg_chk_dev_has_fast_hr_timestamp;



/*HDR*/
/**
 * @brief Check if a device supports configurable time scales.
 *
 * By default the devices return %UTC and/or local time. However, some devices
 * can be configured to return raw GPS time or TAI instead.
 *
 * @note This function should be preferred over ::mbg_dev_has_time_scale,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_time_scale_info
 * @see ::mbg_set_time_scale_settings
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_time_scale( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_TIME_SCALE );

}  // mbg_chk_dev_has_time_scale

MBG_CHK_SUPP_FNC mbg_chk_dev_has_time_scale;



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Check if a device supports setting an event time.
 *
 * This feature is only supported by some special custom firmware
 * to preset a %UTC time at which the device is to generate an output signal.
 *
 * @note This function should be preferred over ::mbg_dev_has_event_time,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_set_event_time
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_event_time( MBG_DEV_HANDLE dh )
{
  return _do_query_cond( dh, _pcps_ddev_has_event_time, IOCTL_DEV_HAS_EVENT_TIME );

}  // mbg_chk_dev_has_event_time

MBG_CHK_SUPP_FNC mbg_chk_dev_has_event_time;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_ucap_entries and ::mbg_get_ucap_event calls.
 *
 * If the device doesn't support this but is a GPS card, it provides
 * a time capture FIFO buffer and the obsolete ::mbg_get_gps_ucap call
 * can be used to retrieve entries from the FIFO buffer.
 *
 * @note This function should be preferred over ::mbg_dev_has_ucap,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 * @see ::mbg_clr_ucap_buff
 * @see ::mbg_get_gps_ucap
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ucap( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_UCAP );

}  // mbg_chk_dev_has_ucap

MBG_CHK_SUPP_FNC mbg_chk_dev_has_ucap;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_clr_ucap_buff call.
 *
 * The ::mbg_clr_ucap_buff call can be used to clear the on-board
 * time capture FIFO buffer of the device, but the call may not
 * be supported by some older GPS devices.
 *
 * @note This function should be preferred over ::mbg_dev_can_clr_ucap_buff,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_clr_ucap_buff
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_can_clr_ucap_buff( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_CAN_CLR_UCAP_BUFF );

}  // mbg_chk_dev_can_clr_ucap_buff

MBG_CHK_SUPP_FNC mbg_chk_dev_can_clr_ucap_buff;



/*HDR*/
/**
 * @brief Check if a device supports timezone configuration using the ::TZDL structure.
 *
 * @note This function should be preferred over ::mbg_dev_has_tzdl,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gps_tzdl
 * @see ::mbg_set_gps_tzdl
 * @see ::mbg_chk_dev_has_tz
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tzdl( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_TZDL );

}  // mbg_chk_dev_has_tzdl

MBG_CHK_SUPP_FNC mbg_chk_dev_has_tzdl;



/*HDR*/
/**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZDL structure.
 *
 * @note This function should be preferred over ::mbg_dev_has_pcps_tzdl,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_pcps_tzdl
 * @see ::mbg_set_pcps_tzdl
 * @see ::mbg_chk_dev_has_tz
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_pcps_tzdl( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_PCPS_TZDL );

}  // mbg_chk_dev_has_pcps_tzdl

MBG_CHK_SUPP_FNC mbg_chk_dev_has_pcps_tzdl;



/*HDR*/
/**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZCODE type.
 *
 * @note This function should be preferred over ::mbg_dev_has_tzcode,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_tzcode
 * @see ::mbg_set_tzcode
 * @see ::mbg_chk_dev_has_tz
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tzcode( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_TZCODE );

}  // mbg_chk_dev_has_tzcode

MBG_CHK_SUPP_FNC mbg_chk_dev_has_tzcode;



/*HDR*/
/**
 * @brief Check if a device supports any kind of timezone configuration.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @note This function should be preferred over ::mbg_dev_has_tz,
 * which has been deprecated.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_tzdl
 * @see ::mbg_chk_dev_has_pcps_tzdl
 * @see ::mbg_chk_dev_has_tzcode
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tz( MBG_DEV_HANDLE dh )
{
  return _do_query_cond( dh, _pcps_ddev_has_tz, IOCTL_DEV_HAS_TZ );

}  // mbg_chk_dev_has_tz

MBG_CHK_SUPP_FNC mbg_chk_dev_has_tz;



/*HDR*/
/**
 * @brief Check if a device provides either an IRIG input or output.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig_tx
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig( MBG_DEV_HANDLE dh )
{
  return _do_query_cond( dh, _pcps_ddev_has_irig, IOCTL_DEV_HAS_IRIG );

}  // mbg_chk_dev_has_irig

MBG_CHK_SUPP_FNC mbg_chk_dev_has_irig;



/*HDR*/
/**
 * @brief Check if a device provides a configurable IRIG output.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_tx_info
 * @see ::mbg_set_irig_tx_settings
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig_tx( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_IRIG_TX );

}  // mbg_chk_dev_has_irig_tx

MBG_CHK_SUPP_FNC mbg_chk_dev_has_irig_tx;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_irig_ctrl_bits call.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig_ctrl_bits,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_ctrl_bits
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig_ctrl_bits( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_IRIG_CTRL_BITS );

}  // mbg_chk_dev_has_irig_ctrl_bits

MBG_CHK_SUPP_FNC mbg_chk_dev_has_irig_ctrl_bits;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_raw_irig_data call.
 *
 * @note This function should be preferred over ::mbg_dev_has_raw_irig_data,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_raw_irig_data
 * @see ::mbg_get_raw_irig_data_on_sec_change
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_raw_irig_data( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_RAW_IRIG_DATA );

}  // mbg_chk_dev_has_raw_irig_data

MBG_CHK_SUPP_FNC mbg_chk_dev_has_raw_irig_data;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_irig_time call.
 *
 * @note This function should be preferred over ::mbg_dev_has_irig_time,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_irig_time
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_irig_time( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_IRIG_TIME );

}  // mbg_chk_dev_has_irig_time

MBG_CHK_SUPP_FNC mbg_chk_dev_has_irig_time;



/*HDR*/
/**
 * @brief Check if a device provides the level of its inputs signal.
 *
 * This is useful to display the signal level of e.g. an IRIG or similar
 * time code, or a long wave signal.
 *
 * @note This function should be preferred over ::mbg_dev_has_signal,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_signal( MBG_DEV_HANDLE dh )
{
  return _do_query_cond( dh, _pcps_ddev_has_signal, IOCTL_DEV_HAS_SIGNAL );

}  // mbg_chk_dev_has_signal

MBG_CHK_SUPP_FNC mbg_chk_dev_has_signal;



/*HDR*/
/**
 * @brief Check if a device provides a modulation signal.
 *
 * Modulation signals are e.g. the second marks provided by a long wave receiver.
 *
 * @note This function should be preferred over ::mbg_dev_has_mod,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_mod( MBG_DEV_HANDLE dh )
{
  return _do_query_cond( dh, _pcps_ddev_has_mod, IOCTL_DEV_HAS_MOD );

}  // mbg_chk_dev_has_mod

MBG_CHK_SUPP_FNC mbg_chk_dev_has_mod;



/*HDR*/
/**
 * @brief Check if a device supports the "last sync time".
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_sync_time( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_SYNC_TIME );

}  // mbg_chk_dev_has_sync_time

MBG_CHK_SUPP_FNC mbg_chk_dev_has_sync_time;



/*HDR*/
/**
 * @brief Check if a device provides the receiver position.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_rcvr_pos( MBG_DEV_HANDLE dh )
{
  return mbg_chk_dev_is_gps( dh );  // All GPS/GNSS receivers support this.

}  // mbg_chk_dev_has_rcvr_pos

MBG_CHK_SUPP_FNC mbg_chk_dev_has_rcvr_pos;



/*HDR*/
/**
 * @brief Check if a device provides the status of buffered variables.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_bvar_stat( MBG_DEV_HANDLE dh )
{
  // All pure GPS receivers should support this, but
  // GNSS receivers using a 3rd party core module may not.
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_BUILTIN, GPS_BIT_MODEL_HAS_BVAR_STAT );

}  // mbg_chk_dev_has_bvar_stat

MBG_CHK_SUPP_FNC mbg_chk_dev_has_bvar_stat;



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_ttm_time API call.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ttm_time( MBG_DEV_HANDLE dh )
{
  // All GPS/GNSS receivers should support this, but
  // other types of device may or may not, maybe
  // depending on the firmware version.
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_BUILTIN, GPS_BIT_MODEL_HAS_TIME_TTM );

}  // mbg_chk_dev_has_ttm_time

MBG_CHK_SUPP_FNC mbg_chk_dev_has_ttm_time;



/*HDR*/
/**
 * @brief Check if a device provides one or more serial ports.
 *
 * @note This function should only be used for legacy devices that
 * don't provide a ::RECEIVER_INFO structure.
 * Otherwise ::RECEIVER_INFO::n_com_ports should be checked to see
 * if and how many serial ports are provided by the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_serial( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_SERIAL );

}  // mbg_chk_dev_has_serial

MBG_CHK_SUPP_FNC mbg_chk_dev_has_serial;



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Check if a device supports higher baud rates than usual.
 *
 * Check if a device provides a serial output that supports
 * higher baud rates than older ones, i.e. ::DEFAULT_BAUD_RATES_DCF_HS
 * rather than ::DEFAULT_BAUD_RATES_DCF.
 *
 * The call ::mbg_get_serial_settings takes care of this, so applications
 * which use that call as suggested don't need to use this call directly.
 *
 * @note This function should be preferred over ::mbg_dev_has_serial_hs,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_serial_settings
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_serial_hs( MBG_DEV_HANDLE dh )
{
  return _do_query_cond( dh, _pcps_ddev_has_serial_hs, IOCTL_DEV_HAS_SERIAL_HS );

}  // mbg_chk_dev_has_serial_hs

MBG_CHK_SUPP_FNC mbg_chk_dev_has_serial_hs;



/*HDR*/
/**
 * @brief Check if a device provides a programmable frequency synthesizer.
 *
 * @note This function should be preferred over ::mbg_dev_has_synth,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_synth
 * @see ::mbg_set_synth
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_synth( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_SYNTH );

}  // mbg_chk_dev_has_synth

MBG_CHK_SUPP_FNC mbg_chk_dev_has_synth;



/*HDR*/
/**
 * @brief Check if a device provides GPIO signal inputs and/or outputs.
 *
 * @note This function should be preferred over ::mbg_dev_has_gpio,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_gpio( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_RI, GPS_FEAT_GPIO );

}  // mbg_chk_dev_has_gpio

MBG_CHK_SUPP_FNC mbg_chk_dev_has_gpio;



/*HDR*/
/**
 * @brief Check if a device supports configuration of antenna cable length.
 *
 * @note This function should be preferred over ::mbg_dev_has_cab_len,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_gps_ant_cable_len
 * @see ::mbg_set_gps_ant_cable_len
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_cab_len( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_CABLE_LEN );

}  // mbg_chk_dev_has_cab_len

MBG_CHK_SUPP_FNC mbg_chk_dev_has_cab_len;



/*HDR*/
/**
 * @brief Check if a device provides a configurable ref time offset.
 *
 * This may be required to convert the received time to %UTC, if the input
 * signal doesn't specify this (e.g. with most IRIG code formats).
 *
 * @note This function should be preferred over ::mbg_dev_has_ref_offs,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_ref_offs
 * @see ::mbg_set_ref_offs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_ref_offs( MBG_DEV_HANDLE dh )
{
  return mbg_chk_dev_is_tcr( dh );  // All TCRs have this.

}  // mbg_chk_dev_has_ref_offs

MBG_CHK_SUPP_FNC mbg_chk_dev_has_ref_offs;



/*HDR*/
/**
 * @brief Check if a device supports the ::MBG_OPT_INFO/::MBG_OPT_SETTINGS.
 *
 * These structures contain optional settings, controlled by flags.
 * See ::MBG_OPT_SETTINGS and associated definitions.
 *
 * @note This function should be preferred over ::mbg_dev_has_opt_flags,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_opt_info
 * @see ::mbg_set_opt_settings
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_opt_flags( MBG_DEV_HANDLE dh )
{
  return mbg_chk_dev_is_tcr( dh );  // All TCRs have this.

}  // mbg_chk_dev_has_opt_flags

MBG_CHK_SUPP_FNC mbg_chk_dev_has_opt_flags;



/*HDR*/
/**
 * @brief Check if a device supports reading/writing of %UTC parameters.
 *
 * This API call checks if a device supports reading/writing a GPS %UTC
 * parameter set via the PC bus. Reading/writing these parameters via the
 * serial port using the Meinberg binary data protocol is supported by all
 * Meinberg GPS and GNSS devices.
 *
 * The %UTC parameter set is usually received from the satellites' broadcasts
 * and contains the current time offset between GPS time and %UTC, plus information
 * on a pending leap second event.
 *
 * It may be useful to overwrite them to do some tests, or for applications
 * where the device is freewheeling anyway.
 *
 * @note This function should be preferred over ::mbg_dev_has_utc_parm,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_utc_parm
 * @see ::mbg_set_utc_parm
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_utc_parm( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_UTC_PARM );

}  // mbg_chk_dev_has_utc_parm

MBG_CHK_SUPP_FNC mbg_chk_dev_has_utc_parm;



/*HDR*/
/**
 * @brief Check if a device supports reading correlation info.
 *
 * The PZF phase modulation decoded by DCF77 PZF receivers is decoded
 * using a correlation approach, and optionally there's an API call
 * supported which can be used to retrieve the correlation value
 * and thus determine the quality of the input signal.
 *
 * @note This function should be preferred over ::mbg_dev_has_corr_info,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_get_corr_info
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_corr_info( MBG_DEV_HANDLE dh )
{
  return mbg_chk_dev_has_pzf( dh );  // All DCF77 PZF receivers have this.

}  // mbg_chk_dev_has_corr_info

MBG_CHK_SUPP_FNC mbg_chk_dev_has_corr_info;



/*HDR*/
/**
 * @brief Check if a device supports configurable distance from transmitter.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, mostly with long wave receivers.
 *
 * @note This function should be preferred over ::mbg_dev_has_tr_distance,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_get_tr_distance
 * @see ::mbg_set_tr_distance
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_tr_distance( MBG_DEV_HANDLE dh )
{
  return mbg_chk_dev_has_pzf( dh );  // All DCF77 PZF receivers have this.

}  // mbg_chk_dev_has_tr_distance

MBG_CHK_SUPP_FNC mbg_chk_dev_has_tr_distance;



/*HDR*/
/**
 * @brief Check if a device provides a debug status word to be read.
 *
 * @note This function should be preferred over ::mbg_dev_has_debug_status,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_get_debug_status
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_debug_status( MBG_DEV_HANDLE dh )
{
  return mbg_chk_dev_is_tcr( dh );  // All TCRs have this.

}  // mbg_chk_dev_has_debug_status

MBG_CHK_SUPP_FNC mbg_chk_dev_has_debug_status;



/*HDR*/
/**
 * @brief Check if a device provides an on-board event log.
 *
 * @note This function should be preferred over ::mbg_dev_has_evt_log,
 * which has been deprecated.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_first_evt_log_entry
 * @see ::mbg_get_next_evt_log_entry
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_evt_log( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_PCPS, PCPS_BIT_HAS_EVT_LOG );

}  // mbg_chk_dev_has_evt_log

MBG_CHK_SUPP_FNC mbg_chk_dev_has_evt_log;



/*HDR*/
/**
 * @brief Check if a device supports bus level DAC control commands.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS if the requested feature is supported, ::MBG_ERR_NOT_SUPP_BY_DEV
 *         if not supported, or one of the other @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_has_dac_ctrl( MBG_DEV_HANDLE dh )
{
  return do_chk_dev_feat( dh, DEV_FEAT_TYPE_XFEAT, MBG_XFEATURE_DAC_CTRL_PCI );

}  // mbg_chk_dev_has_dac_ctrl

MBG_CHK_SUPP_FNC mbg_chk_dev_has_dac_ctrl;



/*HDR*/
/**
 * @brief Check if a device supports the ::RECEIVER_INFO structure and related calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_receiver_info preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_receiver_info
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_receiver_info" ) _MBG_API mbg_dev_has_receiver_info( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_receiver_info, p );

}  // mbg_dev_has_receiver_info



/*HDR*/
/**
 * @brief Check if a device supports large configuration data structures.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_gps_data preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_gps_data
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_gps_data" ) _MBG_API mbg_dev_has_gps_data( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_gps_data, p );

}  // mbg_dev_has_gps_data



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_generic_io API call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_generic_io preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_generic_io
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_generic_io" ) _MBG_API mbg_dev_has_generic_io( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_generic_io, p );

}  // mbg_dev_has_generic_io



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_asic_version call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_asic_version preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_asic_version
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_asic_version" ) _MBG_API mbg_dev_has_asic_version( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_asic_version, p );

}  // mbg_dev_has_asic_version



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_asic_features call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_asic_features preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_asic_features
 * @see ::mbg_get_asic_features
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_asic_features" ) _MBG_API mbg_dev_has_asic_features( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_asic_features, p );

}  // mbg_dev_has_asic_features



/*HDR*/
/**
 * @brief Check if a device provides eXtended Multi Ref (XMR) inputs.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_xmr preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_xmr
 * @see @ref group_multi_ref_ext
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_xmr" ) _MBG_API mbg_dev_has_xmr( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_xmr, p );

}  // mbg_dev_has_xmr



/*HDR*/
/**
 * @brief Check if a device supports GNSS configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_gnss preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_gnss
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_gnss" ) _MBG_API mbg_dev_is_gnss( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_is_gnss, p );

}  // mbg_dev_is_gnss



/*HDR*/
/**
 * @brief Check if a device is a GPS receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_gps preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_gps
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_gps" ) _MBG_API mbg_dev_is_gps( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_is_gps, p );

}  // mbg_dev_is_gps



/*HDR*/
/**
 * @brief Check if a device is a DCF77 receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_dcf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_dcf
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_dcf" ) _MBG_API mbg_dev_is_dcf( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_is_dcf, p );

}  // mbg_dev_is_dcf



/*HDR*/
/**
 * @brief Check if a device supports demodulation of the DCF77 PZF code.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_pzf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_pzf
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_pzf" ) _MBG_API mbg_dev_has_pzf( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_pzf, p );

}  // mbg_dev_has_pzf



/*HDR*/
/**
 * @brief Check if a device is a MSF receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_msf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_msf
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_msf" ) _MBG_API mbg_dev_is_msf( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_is_msf, p );

}  // mbg_dev_is_msf



/*HDR*/
/**
 * @brief Check if a device is a WWVB receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_wwvb preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_wwvb
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_wwvb" ) _MBG_API mbg_dev_is_wwvb( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_is_wwvb, p );

}  // mbg_dev_is_wwvb



/*HDR*/
/**
 * @brief Check if a device is any long wave signal receiver.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_lwr preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_lwr
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_lwr" ) _MBG_API mbg_dev_is_lwr( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_is_lwr, p );

}  // mbg_dev_is_lwr



/*HDR*/
/**
 * @brief Check if a device provides a configurable IRIG input.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_is_tcr preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_is_tcr
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_is_tcr" ) _MBG_API mbg_dev_is_irig_rx( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_is_tcr, p );

}  // mbg_dev_is_irig_rx



/*HDR*/
/**
 * @brief Check if a device supports simple LAN interface API calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_lan_intf preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_lan_intf
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_lan_intf" ) _MBG_API mbg_dev_has_lan_intf( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_lan_intf, p );

}  // mbg_dev_has_lan_intf



/*HDR*/
/**
 * @brief Check if a device supports PTP configuration/status calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ptp preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ptp
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ptp" ) _MBG_API mbg_dev_has_ptp( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_ptp, p );

}  // mbg_dev_has_ptp



/*HDR*/
/**
 * @brief Check if a device supports PTP unicast feature/configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ptp_unicast preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ptp_unicast" ) _MBG_API mbg_dev_has_ptp_unicast( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_ptp_unicast, p );

}  // mbg_dev_has_ptp_unicast



/*HDR*/
/**
 * @brief Check if a device supports the HR_TIME functions.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_hr_time preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_hr_time
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_hr_time" ) _MBG_API mbg_dev_has_hr_time( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_hr_time, p );

}  // mbg_dev_has_hr_time



/*HDR*/
/**
 * @brief Check if a device supports the mbg_get_fast_hr_timestamp_...() calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_fast_hr_timestamp preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_fast_hr_timestamp" ) _MBG_API mbg_dev_has_fast_hr_timestamp( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_fast_hr_timestamp, p );

}  // mbg_dev_has_fast_hr_timestamp



/*HDR*/
/**
 * @brief Check if a device supports configurable time scales.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_time_scale preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_time_scale
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_time_scale" ) _MBG_API mbg_dev_has_time_scale( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_time_scale, p );

}  // mbg_dev_has_time_scale



/*HDR*/
/**
 * @brief Check if a device supports setting an event time.
 *
 * @note This is only supported by some customized devices
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_event_time preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_event_time
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_event_time" ) _MBG_API mbg_dev_has_event_time( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_event_time, p );

}  // mbg_dev_has_event_time



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_ucap_entries and ::mbg_get_ucap_event calls.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ucap preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ucap
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ucap" ) _MBG_API mbg_dev_has_ucap( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_ucap, p );

}  // mbg_dev_has_ucap



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_clr_ucap_buff call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_can_clr_ucap_buff preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_can_clr_ucap_buff
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_can_clr_ucap_buff" ) _MBG_API mbg_dev_can_clr_ucap_buff( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_can_clr_ucap_buff, p );

}  // mbg_dev_can_clr_ucap_buff



/*HDR*/
/**
 * @brief Check if a device supports timezone configuration using the ::TZDL structure.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tzdl preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tzdl
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tzdl" ) _MBG_API mbg_dev_has_tzdl( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_tzdl, p );

}  // mbg_dev_has_tzdl



/*HDR*/
/**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZDL structure.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_pcps_tzdl preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_pcps_tzdl
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_pcps_tzdl" ) _MBG_API mbg_dev_has_pcps_tzdl( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_pcps_tzdl, p );

}  // mbg_dev_has_pcps_tzdl



/*HDR*/
/**
 * @brief Check if a device supports timezone configuration using the ::PCPS_TZCODE type.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tzcode preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tzcode
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tzcode" ) _MBG_API mbg_dev_has_tzcode( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_tzcode, p );

}  // mbg_dev_has_tzcode



/*HDR*/
/**
 * @brief Check if a device supports any kind of timezone configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tz preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tz
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tz" ) _MBG_API mbg_dev_has_tz( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_tz, p );

}  // mbg_dev_has_tz



/*HDR*/
/**
 * @brief Check if a device provides either an IRIG input or output.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig" ) _MBG_API mbg_dev_has_irig( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_irig, p );

}  // mbg_dev_has_irig



/*HDR*/
/**
 * @brief Check if a device provides a configurable IRIG output.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig_tx preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig_tx
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig_tx" ) _MBG_API mbg_dev_has_irig_tx( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_irig_tx, p );

}  // mbg_dev_has_irig_tx



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_irig_ctrl_bits call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig_ctrl_bits preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig_ctrl_bits
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig_ctrl_bits" ) _MBG_API mbg_dev_has_irig_ctrl_bits( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_irig_ctrl_bits, p );

}  // mbg_dev_has_irig_ctrl_bits



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_raw_irig_data call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_raw_irig_data preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_raw_irig_data
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_raw_irig_data" ) _MBG_API mbg_dev_has_raw_irig_data( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_raw_irig_data, p );

}  // mbg_dev_has_raw_irig_data



/*HDR*/
/**
 * @brief Check if a device supports the ::mbg_get_irig_time call.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_irig_time preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_irig_time
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_irig_time" ) _MBG_API mbg_dev_has_irig_time( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_irig_time, p );

}  // mbg_dev_has_irig_time



/*HDR*/
/**
 * @brief Check if a device provides the level of its inputs signal.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_signal preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_signal
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_signal" ) _MBG_API mbg_dev_has_signal( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_signal, p );

}  // mbg_dev_has_signal



/*HDR*/
/**
 * @brief Check if a device provides a modulation signal.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_mod preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_mod
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_mod" ) _MBG_API mbg_dev_has_mod( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_mod, p );

}  // mbg_dev_has_mod



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Check if a device supports higher baud rates than usual.
 *
 * Check if a device provides a serial output that supports
 * higher baud rates than older ones, i.e. ::DEFAULT_BAUD_RATES_DCF_HS
 * rather than ::DEFAULT_BAUD_RATES_DCF.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_serial_hs preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_serial_hs
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_serial_hs" ) _MBG_API mbg_dev_has_serial_hs( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_serial_hs, p );

}  // mbg_dev_has_serial_hs



/*HDR*/
/**
 * @brief Check if a device provides a programmable frequency synthesizer.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_synth preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_synth
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_synth" ) _MBG_API mbg_dev_has_synth( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_synth, p );

}  // mbg_dev_has_synth



/*HDR*/
/**
 * @brief Check if a device provides GPIO signal inputs and/or outputs.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_gpio preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_gpio
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_gpio" ) _MBG_API mbg_dev_has_gpio( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_gpio, p );

}  // mbg_dev_has_gpio



/*HDR*/
/**
 * @brief Check if a device supports configuration of antenna cable length.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_cab_len preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_cab_len
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_cab_len" ) _MBG_API mbg_dev_has_cab_len( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_cab_len, p );

}  // mbg_dev_has_cab_len



/*HDR*/
/**
 * @brief Check if a device provides a configurable ref time offset.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_ref_offs preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_ref_offs
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_ref_offs" ) _MBG_API mbg_dev_has_ref_offs( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_ref_offs, p );

}  // mbg_dev_has_ref_offs



/*HDR*/
/**
 * @brief Check if a device supports the ::MBG_OPT_INFO/::MBG_OPT_SETTINGS.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_opt_flags preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_opt_flags
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_opt_flags" ) _MBG_API mbg_dev_has_opt_flags( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_opt_flags, p );

}  // mbg_dev_has_opt_flags



/*HDR*/
/**
 * @brief Check if a device support reading/writing of ::UTC parameters.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_utc_parm preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_utc_parm
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_utc_parm" ) _MBG_API mbg_dev_has_utc_parm( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_utc_parm, p );

}  // mbg_dev_has_utc_parm



/*HDR*/
/**
 * @brief Check if a device supports reading correlation info.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_corr_info preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_corr_info
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_corr_info" ) _MBG_API mbg_dev_has_corr_info( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_corr_info, p );

}  // mbg_dev_has_corr_info



/*HDR*/
/**
 * @brief Check if a device supports configurable distance from transmitter.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_tr_distance preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_tr_distance
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_tr_distance" ) _MBG_API mbg_dev_has_tr_distance( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_tr_distance, p );

}  // mbg_dev_has_tr_distance



/*HDR*/
/**
 * @brief Check if a device provides a debug status word to be read.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_debug_status preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_debug_status
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_debug_status" ) _MBG_API mbg_dev_has_debug_status( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_debug_status, p );

}  // mbg_dev_has_debug_status



/*HDR*/
/**
 * @brief Check if a device provides an on-board event log.
 *
 * @deprecated This function is deprecated, use ::mbg_chk_dev_has_evt_log preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to an int which is updated if the API call succeeds.
 *                 The flag is set != 0 if the requested feature is supported, else 0.
 *
 * @return ::MBG_SUCCESS on success, if *p has been updated, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_chk_supp_fncs_deprecated
 * @see ::mbg_chk_dev_has_evt_log
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_chk_dev_has_evt_log" ) _MBG_API mbg_dev_has_evt_log( MBG_DEV_HANDLE dh, int *p )
{
  return do_chk_dev_feat_deprecated( dh, mbg_chk_dev_has_evt_log, p );

}  // mbg_dev_has_evt_log



static /*HDR*/
/**
 * @brief Update a ::PCPS_TIME_STAMP to compensate the latency of an API call.
 *
 * @param[in,out] ts            The timestamp to be updated.
 * @param[in]     p_cyc_ts      Cycles value taken when the function was called.
 * @param[in]     p_cyc_ontime  Cycles value taken when access to the device was made.
 * @param[in]     p_cyc_freq    Cycles frequency determined by the application, depending on the type of cycles counter.
 * @param[out]    hns_latency   Optional pointer to a variable to receive the computed latency, or @a NULL.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_comp_hr_latency( PCPS_TIME_STAMP *ts,
                         const MBG_PC_CYCLES *p_cyc_ts,
                         const MBG_PC_CYCLES *p_cyc_ontime,
                         const MBG_PC_CYCLES_FREQUENCY *p_cyc_freq,
                         int32_t *hns_latency )
{
  #if defined( MBG_TGT_WIN32 ) || defined( MBG_TGT_POSIX )

    int64_t cyc_latency;
    int64_t frac_latency;
    int64_t comp_frac;  //uint??

    // Compute latency in cycles counter units
    cyc_latency = mbg_delta_pc_cycles( p_cyc_ts, p_cyc_ontime );

    #if DEBUG && defined( MBG_TGT_LINUX ) && defined( MBG_ARCH_X86 )
      printf( "comp_lat: %08llX.%08llX %llX - %llX = %lli",
              (unsigned long long) ts->sec,
              (unsigned long long) ts->frac,
              (unsigned long long) *p_cyc_ts,
              (unsigned long long) *p_cyc_ontime,
              (unsigned long long) cyc_latency
            );
    #endif

    // Account for cycles counter overflow. If the CPU's TSC is used,
    // this is supposed to happen once every 2^^63 units of the
    // cycles counter frequency, i.e. about every 97 years on a system
    // with 3 GHz clock.
    if ( cyc_latency < 0 )
    {
      cyc_latency += ( (uint64_t) -1 ) >> 1;

      #if DEBUG && defined( MBG_TGT_LINUX )
        printf( "->%lli (%llX)",
                (unsigned long long) cyc_latency,
                (unsigned long long) ( ( (uint64_t) -1 ) >> 1 )
              );
      #endif
    }

    // Convert latency to binary fractions of seconds,
    // i.e. units of 2^^-32.
    frac_latency = (*p_cyc_freq) ? ( cyc_latency * ( ( (int64_t) 1 ) << 32 ) / *p_cyc_freq ) : 0;

    // Compute the compensated fractional part of the HR timestamp
    // and account for borrows from the sec field.
    comp_frac = ts->frac - frac_latency;
    ts->frac = (uint32_t) comp_frac;            // yields 32 LSBs
    ts->sec += (uint32_t) ( comp_frac >> 32 );  // yields 32 MSBs

    #if DEBUG && defined( MBG_TGT_LINUX )
      printf( " frac_lat: %llX comp_frac: %08llX.%08llX",
              (unsigned long long) frac_latency,
              (unsigned long long) ts->sec,
              (unsigned long long) ts->frac
            );
    #endif

    if ( hns_latency && *p_cyc_freq )
    {
      int64_t tmp_hns_latency;

      // Convert to hectonanoseconds.
      tmp_hns_latency = cyc_latency * 10000000L / *p_cyc_freq;

      // Check for range overflow.
      #define MAX_HNS_LATENCY    0x7FFFFFFF   // int32_t
      #define MIN_HNS_LATENCY    ( -MAX_HNS_LATENCY - 1 )

      if ( tmp_hns_latency > MAX_HNS_LATENCY )
        tmp_hns_latency = MAX_HNS_LATENCY;
      else
        if ( tmp_hns_latency < MIN_HNS_LATENCY )
          tmp_hns_latency = MIN_HNS_LATENCY;

      *hns_latency = (int32_t) tmp_hns_latency;
    }

    #if DEBUG && defined( MBG_TGT_LINUX )
      printf( "\n" );
    #endif

    return MBG_SUCCESS;

  #else

    // This is currently not supported by the target environment.
    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_comp_hr_latency



/*HDR*/
/**
 * @brief Check if a string is a valid device file name.
 *
 * @param[in]  s  The string to be checked.
 *
 * @return ::MBG_SUCCESS if @p s is a valid device file name, else ::MBG_ERR_INV_PARM.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
_MBG_API_ATTR int _MBG_API mbg_chk_dev_fn_is_valid( const char *s )
{
  #if MBG_TGT_HAS_DEV_FN_BASE
    if ( strncmp( s, mbg_dev_fn_base, strlen( mbg_dev_fn_base ) ) == 0 )
      return MBG_SUCCESS;
  #endif

  #if SUPP_SYN1588
    // For SYN1588 devices, we use pseudo device file names even
    // on systems where we don't support native device file names.
    if ( mbg_rc_is_success( mbg_syn1588_chk_dev_fn_is_valid( s ) ) )
      return MBG_SUCCESS;
  #endif

  return MBG_ERR_INV_PARM;

}  // mbg_chk_dev_fn_is_valid



/*HDR*/
/**
 * @brief Create a device file name associated with an index number.
 *
 * Create a system-specific device file name string, e.g. @a /dev/mbgclock0
 * on Linux and similar systems, which can be used with the @a open call
 * on systems which support this, and is also appropriate for informational
 * messages, e.g. when referring to a specific device.
 *
 * On Windows, a complex hardware ID string is required to open the device,
 * and the Windows-specific function mbg_svc_get_device_path can be used
 * to get the hardware ID string associated with a specific device index.
 *
 * However, the Windows hardware ID string is not appropriate for
 * informational messages referring to a device, so this function
 * may return an empty string on Windows, or a pseudo device name
 * for devices like SYN1588 PCIe NIC devices.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   dev_idx  The device index number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_dev_info_from_dev_idx
 * @see mbg_svc_get_device_path on Windows
 */
_MBG_API_ATTR int _MBG_API mbg_dev_fn_from_dev_idx( char *s, int max_len, int dev_idx )
{
  int n = 0;

  s[n] = 0;  // Make string empty.

  #if SUPP_SYN1588_USR_SPC
    if ( syn1588_devices_found && ( dev_idx >= mbg_devices_found ) )
      return mbg_syn1588_dev_fn_from_dev_idx( s, max_len, dev_idx - mbg_devices_found );
  #endif

  #if MBG_TGT_HAS_DEV_FN_BASE
    n += snprintf_safe( s, max_len, mbg_dev_fn_fmt, dev_idx );
  #endif

  return n;

}  // mbg_dev_fn_from_dev_idx



/*HDR*/
/**
 * @brief Create a device info string associated with an index number.
 *
 * This consists of a generic string such as "Device #0", followed
 * by an associated device file name (e.g. "/dev /mbgclock0"),
 * if appropriate.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   dev_idx  The device index number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 *
 * @see ::mbg_dev_fn_from_dev_idx
 */
_MBG_API_ATTR int _MBG_API mbg_dev_info_from_dev_idx( char *s, int max_len, int dev_idx )
{
  int n = snprintf_safe( s, max_len, "Device #%i", dev_idx );

  #if MBG_TGT_HAS_DEV_FN_BASE
    n += sn_cpy_str_safe( &s[n], max_len - n, ", " );
    n += mbg_dev_fn_from_dev_idx( &s[n], max_len - n, dev_idx );
  #endif

  return n;

}  // mbg_dev_info_from_dev_idx



/*HDR*/
/**
 * @brief Open a device by index number.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  dev_idx  Device index number, starting from 0.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 */
_MBG_API_ATTR MBG_DEV_HANDLE _MBG_API mbg_open_device( int dev_idx )
{
  if ( dev_idx < 0 )
    goto fail;

  #if defined( MBG_TGT_WIN32 )
  {
    const char *device_path = mbg_svc_get_device_path( dev_idx );

    return mbg_open_device_by_dev_fn( device_path );
  }
  #elif MBG_TGT_HAS_DEV_FN
  {
    MBG_DEV_FN dev_fn;
    MBG_DEV_HANDLE dh;

    mbg_dev_fn_from_dev_idx( dev_fn.s, sizeof( dev_fn.s ), dev_idx );

    dh = mbg_open_device_by_dev_fn( dev_fn.s );

    return dh;
  }
  #else

    #if defined( _PCPSDRVR_H )
      if ( dev_idx < n_ddevs )
        return &pcps_ddev[dev_idx];
    #endif

    // Intentionally fall-through to "fail".
  #endif

fail:
  errno = ENODEV;  // No such device.
  return MBG_INVALID_DEV_HANDLE;

} // mbg_open_device



/*HDR*/
/**
 * @brief Open a device specified by a device file name.
 *
 * The format the device file name depends on the operating system.
 * see ::MBG_DEV_FN.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  dev_fn  The device file name.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 * @see ::MBG_DEV_FN
 */
_MBG_API_ATTR MBG_DEV_HANDLE _MBG_API mbg_open_device_by_dev_fn( const char *dev_fn )
{
  if ( dev_fn == NULL )
  {
    #if defined( MBG_TGT_WIN32 )
      SetLastError( ERROR_INVALID_PARAMETER );  // The parameter is incorrect.
    #elif defined( MBG_TGT_POSIX ) || defined( MBG_TGT_QNX_NTO )
      errno = EINVAL;                           // Invalid argument.
    #else
      #error Need to set error code.
    #endif

    return MBG_INVALID_DEV_HANDLE;
  }

  #if SUPP_SYN1588_USR_SPC
  {
    MBG_DEV_HANDLE dh = MBG_INVALID_DEV_HANDLE;

    if ( mbg_rc_is_success( mbg_syn1588_open_device_by_dev_fn( dev_fn, &dh ) ) )
      return dh;
  }
  #endif

  #if defined( MBG_TGT_WIN32 )

    return CreateFileA(
          dev_fn,                        // File name.
          GENERIC_READ | GENERIC_WRITE,  // Access mode.
          0,                             // Ahare mode.
          NULL,                          // Security descriptor.
          OPEN_EXISTING,                 // How to create.
          // If we don't use FILE_FLAG_OVERLAPPED here,
          // the Windows kernel serializes IOCTL calls (IRPs)
          // from several applications or threads. However,
          // serialization is anyway done by a device mutex
          // in the kernel driver, if required, so the
          // Windows kernel doesn't have to care.
          FILE_FLAG_OVERLAPPED,          // File attributes.
          NULL                           // Handle to template file.
        );

  #elif defined( MBG_TGT_POSIX ) && !defined( MBG_TGT_QNX_NTO )

    return open( dev_fn, O_RDWR );

  #else

    errno = ( dev_fn == NULL ) ? EFAULT : ENODEV;
    return MBG_INVALID_DEV_HANDLE;

  #endif

} // mbg_open_device_by_dev_fn



/*HDR*/
/**
 * @brief Open a device specified by a device file name.
 *
 * @deprecated This function is deprecated, use
 * ::mbg_open_device_by_dev_fn preferably.
 *
 * The format the device file name depends on the operating system.
 * see ::MBG_DEV_FN.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  dev_fn  The device file name, see ::MBG_DEV_FN.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 * @see ::MBG_DEV_FN
 */
_MBG_API_ATTR MBG_DEV_HANDLE _DEPRECATED_BY( "mbg_open_device_by_dev_fn" ) _MBG_API mbg_open_device_by_hw_id( const char *dev_fn )
{
  return mbg_open_device_by_dev_fn( dev_fn );

} // mbg_open_device_by_hw_id



static /*HDR*/
int do_find_mbg_devices( int max_devs )
{
  int found = 0;
  int dev_idx;

  for ( dev_idx = 0; dev_idx < N_SUPP_DEV_BUS; dev_idx++ )
  {
    MBG_DEV_FN dev_fn;
    MBG_DEV_HANDLE dh;

    mbg_dev_fn_from_dev_idx( dev_fn.s, sizeof( dev_fn.s ), dev_idx );

    dh = mbg_open_device_by_dev_fn( dev_fn.s );

    if ( dh != MBG_INVALID_DEV_HANDLE )
    {
      mbg_close_device( &dh );

      if ( ++found >= max_devs )
        break;
    }
  }

  #if SUPP_SYN1588_USR_SPC
    mbg_devices_found = found;
  #endif

  return found;

}  // do_find_mbg_devices



/*HDR*/
/**
 * @brief Get the number of devices installed on the computer.
 *
 * The function ::mbg_find_devices_with_names should probably
 * be used preferably. See @ref mbgdevio_open_fncs.
 *
 * @return The number of devices found.
 *
 * @see ::mbg_find_devices_with_names
 * @see ::mbg_open_device
 * @see @ref mbgdevio_open_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_find_devices( void )
{
  #if defined( _PCPSDRVR_H )

    #if defined( MBG_TGT_QNX_NTO )
      // Since this program accessed the hardware directly,
      // I/O privileges have to be assigned to the thread.
      if ( ThreadCtl( _NTO_TCTL_IO, NULL ) == -1 )
      {
        fprintf( stderr, "** ThreadCtl() failed to request I/O privileges: %s\n\n", strerror( errno ) );
        exit( 1 );
      }
    #endif

    #if defined( MBG_TGT_DOS ) && MBG_USE_DOS_TSR
    {
      short prv_busy;

      pcps_detect_any_tsr();
      prv_busy = pcps_tsr_set_busy_flag( 1 );
      pcps_detect_devices( pcps_isa_ports, NULL );
      pcps_tsr_set_busy_flag( prv_busy );
    }
    #else
      pcps_detect_devices( pcps_isa_ports, NULL );
    #endif

    return n_ddevs;

  #elif defined( MBG_TGT_WIN32 )

    return mbg_svc_find_devices();

  #elif defined( MBG_TGT_POSIX )

    // Look for native Meinberg devices.
    int n_devs = do_find_mbg_devices( N_SUPP_DEV_BUS );

    #if SUPP_SYN1588
      // Look for SYN1588 devices.
      n_devs += mbg_syn1588_find_devices( N_SUPP_DEV_BUS - n_devs, NULL );
    #endif

    return n_devs;

  #endif

}  // mbg_find_devices



#if MBG_TGT_HAS_DEV_FN

/**
 * @brief Free a list of device names that has been allocated before.
 *
 * This function should be called to free a list that has been
 * allocated by ::setup_dev_fn_list.
 *
 * @param[out]  list  Pointer to a list to be freed.
 *
 * @see ::setup_dev_fn_list
 */
static /*HDR*/
void free_dev_fn_list( MBG_DEV_FN_LIST_ENTRY *list )
{
  #if defined( MBG_TGT_WIN32 )
    mbg_svc_free_device_list( list );  // TODO
  #else
    while ( list )
    {
      MBG_DEV_FN_LIST_ENTRY *next;

      if ( list->dev_fn_ptr )
      {
        free( list->dev_fn_ptr );
        list->dev_fn_ptr = NULL;
      }

      next = list->next;
      free( list );
      list = next;
    }
  #endif

}  // free_dev_fn_list

#endif  // MBG_TGT_HAS_DEV_FN



#if MBG_TGT_HAS_DEV_FN

static /*HDR*/
/**
 * @brief Allocate and fill a list of device file names.
 *
 * The function ::free_dev_fn_list should be called when the
 * returned list has been evaluated and isn't needed anymore.
 *
 * @param[out]  p_list    Pointer to a list to be allocated and
 *                        set up, set to @a NULL if no devices were found.
 * @param[in]   max_devs  Max. number of devices to be added to the list.
 *
 * @return The number of devices found.
 *
 * @see ::free_dev_fn_list
 */
int setup_dev_fn_list( MBG_DEV_FN_LIST_ENTRY **p_list, int max_devs )
{
  // The way to retrieve a list of devices currently present
  // in the system depends on the operating system.

  #if defined( MBG_TGT_WIN32 )

    return mbg_svc_find_devices_with_hw_id( p_list, max_devs );

  #elif defined( MBG_TGT_POSIX )

    MBG_DEV_FN_LIST_ENTRY *list_head = NULL;
    MBG_DEV_FN_LIST_ENTRY *pos = NULL;
    int i;
    int n_dev_fn = 0;
    int n_devs = mbg_find_devices();

    if ( n_devs > max_devs )
      n_devs = max_devs;

    for ( i = 0; i < n_devs; i++ )
    {
      MBG_DEV_FN dev_fn;
      MBG_DEV_HANDLE dh;

      mbg_dev_fn_from_dev_idx( dev_fn.s, sizeof( dev_fn.s ), i );

      dh = mbg_open_device_by_dev_fn( dev_fn.s );

      if ( dh != MBG_INVALID_DEV_HANDLE )
      {
        size_t sz = strlen( dev_fn.s ) + 1;  // Including terminating 0.

        if ( list_head == NULL )  // First turn.
        {
          list_head = (MBG_DEV_FN_LIST_ENTRY *) calloc( 1, sizeof( *list_head ) );
          pos = list_head;
        }
        else
        {
          pos->next = (MBG_DEV_FN_LIST_ENTRY *) calloc( 1, sizeof( *pos ) );
          pos = pos->next;
        }

        if ( pos == NULL )
          goto out_free;

        pos->dev_fn_ptr = (char *) calloc( 1, sz );

        if ( pos->dev_fn_ptr == NULL )
          goto out_free;

        strncpy_safe( pos->dev_fn_ptr, dev_fn.s, sz );
        n_dev_fn++;

        mbg_close_device( &dh );
      }
    }

    if ( n_dev_fn )
      goto out;


out_free:
    free_dev_fn_list( list_head );
    list_head = NULL;
    n_dev_fn = 0;

out:
    *p_list = list_head;   // Return the list.
    return n_dev_fn;

  #else

    *p_list = NULL;
    return 0;

  #endif

}  // setup_dev_fn_list

#endif  // MBG_TGT_HAS_DEV_FN



#if MBG_TGT_HAS_DEV_FN

static /*HDR*/
/**
 * @brief Open a device, retrieve the ::PCPS_DEV info, then close it.
 *
 * @param[out]  p       Pointer to a ::PCPS_DEV to be filled.
 * @param[in]   dev_fn  The device file name of the device to be used.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int get_dev_info_for_dev_fn( PCPS_DEV *p, const char *dev_fn )
{
  MBG_DEV_HANDLE dh;
  int rc;

  memset( p, 0, sizeof( *p ) );

  // Try to retrieve device information.
  dh = mbg_open_device_by_dev_fn( dev_fn );

  if ( dh == MBG_INVALID_DEV_HANDLE )
    return mbg_get_last_error( NULL );

  rc = mbg_get_device_info( dh, p );

  mbg_close_device( &dh );

  return rc;

}  // get_dev_info_for_dev_fn

#endif  // MBG_TGT_HAS_DEV_FN



static /*HDR*/
/**
 * @brief Lookup a specific device in an array of ::PCPS_DEV structures.
 *
 * Look for a matching name, if a type name is specified, and for
 * a matching serial number if that is also given.
 *
 * @param[in]  dev_array  The array of ::PCPS_DEV structures to be searched.
 * @param[in]  n_dev      The number of entries in @p dev_array.
 * @param[in]  type_name  An optional type name to search for, may be @a NULL.
 * @param[in]  sernum     An optional serial number to search for, may be @a NULL.
 *
 * @return An index value >= 0 on success, or -1 if no entry was found.
 *
 * @see ::lookup_dev_idx_ex
 */
int lookup_dev_idx( const PCPS_DEV *dev_array, int n_dev,
                    const char *type_name, const char *sernum )
{
  int i;

  for ( i = 0; i < n_dev; i++ )
  {
    const PCPS_DEV *p_dev = &dev_array[i];

    if ( type_name && strcmp( _pcps_type_name( p_dev ), type_name ) )
      continue;   // Type name given, but doesn't match.

    if ( sernum && strcmp( _pcps_sernum( p_dev ), sernum ) )
      continue;   // Serial number given, but doesn't match.

    return i;  // Matching device found.
  }

  return -1;  // No matching device found.

}  // lookup_dev_idx



static /*HDR*/
/**
 * @brief Lookup a specific device in an array, depending on a match code.
 *
 * The function first looks for an exact match of type name
 * and serial number, then for a matching type name only, if
 * @p selection_mode allows, and if no matching devic model
 * can be found at all, returns the index of the first device
 * found, if an appropriate @p selection_mode has been specified.
 *
 * @param[in]  dev_array  The array of ::PCPS_DEV structures to be searched.
 * @param[in]  n_dev      The number of entries in @p dev_array.
 * @param[in]  type_name  An optional type name to search for, may be @a NULL.
 * @param[in]  sernum     An optional serial number to search for, may be @a NULL.
 * @param[in]  selection_mode  One of the ::MBG_MATCH_MODES.
 *
 * @return An index value >= 0 on success, or -1 if no entry was found.
 *
 * @see ::lookup_dev_idx
 * @see ::MBG_MATCH_MODES
 */
int lookup_dev_idx_ex( const PCPS_DEV *dev_array, int n_dev,
                       const char *type_name, const char *sernum,
                       int selection_mode )
{
  int dev_idx = -1;

  if ( n_dev == 0 )  // No devices available.
    goto out;

  dev_idx = lookup_dev_idx( dev_array, n_dev, type_name, sernum );

  if ( dev_idx >= 0 )
    goto out;


  // The requested combination of a device model name and
  // serial number was not found. If an exact match was
  // requested, we're done anyway.
  if ( selection_mode == MBG_MATCH_EXACTLY )
    goto out;


  // Look for device with matching name only, ignoring
  // the serial number.
  dev_idx = lookup_dev_idx( dev_array, n_dev, type_name, NULL );

  if ( dev_idx >= 0 )
    goto out;


  // As a last resort select the first device that
  // has been found, if the selection mode allows.
  if ( selection_mode == MBG_MATCH_ANY )
    dev_idx = 0;

out:
  return dev_idx;

}  // lookup_dev_idx_ex



/*HDR*/
/**
 * @brief Allocate memory and set up a list of installed and supported devices.
 *
 * Allocate and fill a list with the names of Meinberg devices currently
 * present in the system.
 *
 * This can be used e.g. to populate a device selection dialog
 * in a configuration program.
 *
 * When the list is not used anymore it can be freed by calling
 * ::mbg_free_device_name_list.
 *
 * @param[in]  p_list       Pointer to a linked list to be allocated.
 * @param[in]  max_devices  Maximum number of devices to be searched for
 *                          (must not exceed ::N_SUPP_DEV_BUS).
 *
 * @return The number of present devices.
 *
 * @see ::mbg_free_device_name_list
 * @see ::mbg_find_devices
 * @see ::MBG_DEV_NAME
 */
_MBG_API_ATTR int _MBG_API mbg_find_devices_with_names( MBG_DEV_NAME_LIST_ENTRY **p_list,
                                                        int max_devices )
{
#if MBG_TGT_HAS_DEV_FN

  MBG_DEV_FN_LIST_ENTRY *dev_fn_list_head = NULL;
  MBG_DEV_FN_LIST_ENTRY *dev_fn_pos = NULL;

  MBG_DEV_NAME_LIST_ENTRY *dev_name_list_head = NULL;
  MBG_DEV_NAME_LIST_ENTRY *dev_name_pos = NULL;

  int n_dev_fn = 0;
  int n_dev_name = 0;

  // First set up a list of device names, the format of which
  // depends on the OS.
  n_dev_fn = setup_dev_fn_list( &dev_fn_list_head, max_devices );

  // Now iterate through the device ***file name*** list
  // and set up a ***device name*** list.
  for ( dev_fn_pos = dev_fn_list_head; dev_fn_pos; dev_fn_pos = dev_fn_pos->next )
  {
    PCPS_DEV dev;
    int rc;

    if ( dev_name_list_head == NULL )  // First turn.
    {
      dev_name_list_head = (MBG_DEV_NAME_LIST_ENTRY *) calloc( 1, sizeof( *dev_name_list_head ) );
      dev_name_pos = dev_name_list_head;
    }
    else
    {
      dev_name_pos->next = (MBG_DEV_NAME_LIST_ENTRY *) calloc( 1, sizeof( *dev_name_pos ) );
      dev_name_pos = dev_name_pos->next;
    }

    if ( dev_name_pos == NULL )  // Failed to allocate memory.
    {
      mbg_free_device_name_list( dev_name_list_head );  // Free this list.
      dev_name_list_head = NULL;
      n_dev_name = 0;
      break;
    }

    // Get device details for the device.
    rc = get_dev_info_for_dev_fn( &dev, dev_fn_pos->dev_fn_ptr );

    if ( mbg_rc_is_success( rc ) )
      mbg_create_dev_name_from_dev_info( dev_name_pos->dev_name,
                                 sizeof( dev_name_pos->dev_name ), &dev );

    if ( ++n_dev_name > n_dev_fn )  // This should never happen.
      break;
  }

  // Free the device name list which we don't need anymore.
  free_dev_fn_list( dev_fn_list_head );

  *p_list = dev_name_list_head;   // Return the list.

  return n_dev_name;

#else

  *p_list = NULL;
  return 0;

#endif

}  // mbg_find_devices_with_names



/*HDR*/
/**
 * @brief Free the memory allocated for a list of ::MBG_DEV_NAME_LIST_ENTRY entries.
 *
 * The list may have been set up and allocated before
 * by ::mbg_find_devices_with_names.
 *
 * @param[in,out]  list  Linked list of ::MBG_DEV_NAME_LIST_ENTRY entries.
 *
 * @see ::mbg_find_devices_with_names
 */
_MBG_API_ATTR void _MBG_API mbg_free_device_name_list( MBG_DEV_NAME_LIST_ENTRY *list )
{
  while ( list )
  {
    MBG_DEV_NAME_LIST_ENTRY *next = list->next;

    free( list );
    list = next;
  }

}  // mbg_free_device_name_list



/*HDR*/
 /**
 * @brief Split a string in ::MBG_DEV_NAME format its components.
 *
 * The components are the 'type name' and 'serial number' strings.
 *
 * See ::MBG_DEV_NAME for the possible formats of a device name.
 * If no @p dev_name is passed, the destination buffers are set
 * to empty strings.
 *
 * One of the complementary functions ::mbg_create_dev_name or
 * ::mbg_create_dev_name_from_dev_info can be used to generate
 * a device name string.
 *
 * @param[in]   dev_name        The string in ::MBG_DEV_NAME format to be split up.
 * @param[out]  type_name       Output buffer for the type name part, e.g. a ::PCPS_CLOCK_NAME.
 * @param[in]   type_name_size  Size of the output buffer for the type name string.
 * @param[out]  sernum          Output buffer for the sernum part, e.g. a ::PCPS_SN_STR.
 * @param[in]   sernum_size     Size of the output buffer for the sernum string.
 *
 * @see ::mbg_create_dev_name
 * @see ::mbg_create_dev_name_from_dev_info
 * @see ::MBG_DEV_NAME
 * @see ::MBG_DEV_NAME_FMT
 */
_MBG_API_ATTR void _MBG_API mbg_split_dev_name( const char *dev_name,
                                                char *type_name, size_t type_name_size,
                                                char *sernum, size_t sernum_size )
{
  size_t dev_name_len = 0;
  size_t i = 0;
  char c;

  type_name[0] = 0;  // Make string empty.
  sernum[0] = 0;     // Make string empty.

  if ( dev_name )
    dev_name_len = strlen( dev_name );

  if ( dev_name_len )
  {
    // Device model name.
    const char *cp = dev_name;
    i = 0;

    for (;;)
    {
      c = *cp;

      if ( c == 0 )  // End of string.
        break;

      if ( c == '_' )  // Separator before S/N, see ::MBG_DEV_NAME_FMT.
        break;

      if ( i == ( type_name_size - 1 ) )
        break;

      type_name[i] = ( c >= 'a' && c <= 'z' ) ? ( c - 0x20 ) : c;

      i++;
      cp++;
    }

    type_name[i] = 0;  // Terminate string.

    if ( *cp == '_' )
    {
      // Serial number.
      cp++;
      i = 0;

      for (;;)
      {
        c = *cp;

        if ( c == 0 )  // End of string.
          break;

        if ( i == ( sernum_size - 1 ) )
          break;

        if ( ( c < '0' || c > '9' ) && ( c != '?' ) )  // not a digit
          break;

        sernum[i] = c;

        i++;
        cp++;
      }

      sernum[i] = 0;  // Terminate string.
    }
  }

}  // mbg_split_dev_name



/*HDR*/
/**
 * @brief Return a handle to a device with a particular device name.
 *
 * See ::MBG_DEV_NAME for the possible formats of a device name.
 *
 * If the call fails, ::mbg_get_last_error should immediately
 * be called to get the reason why.
 *
 * For details and similar functions see  @ref mbgdevio_open_fncs.
 *
 * @param[in]  srch_name       String in ::MBG_DEV_NAME format of a device to be opened.
 * @param[in]  selection_mode  One of the ::MBG_MATCH_MODES.
 *
 * @return A valid device handle on success, else ::MBG_INVALID_DEV_HANDLE.
 *
 * @ingroup mbgdevio_open_fncs
 * @see ::mbg_close_device
 * @see @ref mbgdevio_open_fncs
 * @see ::MBG_DEV_NAME
 * @see ::MBG_MATCH_MODES
 */
_MBG_API_ATTR MBG_DEV_HANDLE _MBG_API mbg_open_device_by_name( const char *srch_name, int selection_mode )
{
  PCPS_DEV dev_array[N_SUPP_DEV_BUS] = { { { 0 } } };
  PCPS_CLOCK_NAME type_name;
  PCPS_SN_STR sernum;
  int i = 0;
  int dev_idx = -1;

  mbg_split_dev_name( srch_name, type_name, sizeof( type_name ),
                      sernum, sizeof( sernum ) );

  #if MBG_TGT_HAS_DEV_FN
  {
    MBG_DEV_FN dev_fn_array[N_SUPP_DEV_BUS] = { { { 0 } } };
    MBG_DEV_FN_LIST_ENTRY *list_head = NULL;

    // Set up a temporary list with file names of devices
    // that are currently present in the system.
    int n_devices = setup_dev_fn_list( &list_head, N_SUPP_DEV_BUS );

    if ( n_devices )
    {
      MBG_DEV_FN_LIST_ENTRY *pos;

      for ( i = 0, pos = list_head; pos && pos->dev_fn_ptr; pos = pos->next )
      {
        int rc = get_dev_info_for_dev_fn( &dev_array[i], pos->dev_fn_ptr );

        if ( mbg_rc_is_success( rc ) )
        {
          // Save the associated device file name.
          sn_cpy_str_safe( dev_fn_array[i].s, sizeof( dev_fn_array[i].s ), pos->dev_fn_ptr );
          i++;
        }
      }

      dev_idx = lookup_dev_idx_ex( dev_array, _int_from_size_t( i ), type_name,
                                   sernum, selection_mode );
    }

    free_dev_fn_list( list_head );

    // We even try to open the device if no match was found.
    // This causes an "invalid device handle" value to be
    // returned, and sets an appropriate "last error" code.
    return mbg_open_device_by_dev_fn( ( dev_idx >= 0 ) ? dev_fn_array[dev_idx].s : "" );
  }
  #else
    #if defined( _PCPSDRVR_H )
      mbg_find_devices();

      for ( i = 0; i < n_ddevs; i++ )
        dev_array[i] = pcps_ddev[i].dev;

      dev_idx = lookup_dev_idx_ex( dev_array, _int_from_size_t( i ), type_name,
                                   sernum, selection_mode );

      if ( dev_idx >= 0 )
        return mbg_open_device( dev_idx );
    #endif

    errno = ENODEV;
    return MBG_INVALID_DEV_HANDLE;

  #endif

} // mbg_open_device_by_name



/*HDR*/
/**
 * @brief Close a device handle and set the handle value to ::MBG_INVALID_DEV_HANDLE.
 *
 * @param[in,out] p_dh  Pointer to a Meinberg device handle.
 *
 * @see @ref mbgdevio_open_fncs
 */
_MBG_API_ATTR void _MBG_API mbg_close_device( MBG_DEV_HANDLE *p_dh )
{
  if ( *p_dh != MBG_INVALID_DEV_HANDLE && *p_dh != 0 )   // TODO *p_dh NULL or 0 ?
  {
    #if SUPP_SYN1588_USR_SPC
      if ( is_mbg_syn1588_type( *p_dh ) )
      {
        mbg_syn1588_close_device( p_dh );
        return;
      }
    #endif

    #if defined( MBG_TGT_WIN32 )
      CloseHandle( *p_dh );
    #elif defined( MBG_TGT_POSIX ) && !defined( MBG_TGT_QNX_NTO )
      close( *p_dh );
    #endif
  }

  *p_dh = MBG_INVALID_DEV_HANDLE;

}  // mbg_close_device



/*HDR*/
/**
 * @brief Read information about the driver handling a given device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::PCPS_DRVR_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
_MBG_API_ATTR int _MBG_API mbg_get_drvr_info( MBG_DEV_HANDLE dh, PCPS_DRVR_INFO *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_PCPS_DRVR_INFO, p );
    return _mbgdevio_cnv_ret_val( rc );
  #else
    (void) dh;   // avoid warning "never used"
    drvr_info.n_devs = n_ddevs;
    *p = drvr_info;
    return MBG_SUCCESS;
  #endif

}  // mbg_get_drvr_info



/*HDR*/
/**
 * @brief Read detailed device information.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Address of a ::PCPS_DEV structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
_MBG_API_ATTR int _MBG_API mbg_get_device_info( MBG_DEV_HANDLE dh, PCPS_DEV *p )
{
  #if SUPP_SYN1588_USR_SPC
    MBGDEVIO_RET_VAL rc = mbg_syn1588_get_device_info( dh, p );

    if ( mbg_rc_is_success( rc ) )
      return rc;
  #endif

  #if defined( _MBGIOCTL_H )
  {
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_PCPS_DEV, p );
    // Endianess is converted inside the kernel driver, if necessary.
    return _mbgdevio_cnv_ret_val( rc );
  }
  #else
    *p = dh->dev;
    return MBG_SUCCESS;
  #endif

}  // mbg_get_device_info



/*HDR*/
/**
 * @brief Read CPU type info of the device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Address of a ::PCPS_CPU_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
_MBG_API_ATTR int _MBG_API mbg_get_pcps_cpu_info( MBG_DEV_HANDLE dh, PCPS_CPU_INFO *p )
{
  #if SUPP_SYN1588_USR_SPC
    if ( chk_if_syn1588_type( dh ) )
    {
      memset( p, 0, sizeof( *p ) );
      return MBG_SUCCESS;
    }
  #endif

  #if defined( _MBGIOCTL_H )
  {
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_PCPS_CPU_INFO, p );
    // Endianess is converted inside the kernel driver, if necessary.
    return _mbgdevio_cnv_ret_val( rc );
  }
  #else
    *p = dh->pdt->cpu_info;
    return MBG_SUCCESS;
  #endif

}  // mbg_get_pcps_cpu_info



/*HDR*/
/**
 * @brief Read the current state of the on-board ::PCPS_STATUS_PORT.
 *
 * This function is useful to read the status port of the device which
 * also includes the ::PCPS_ST_MOD bit, which reflects the time code
 * modulation of long wave receivers.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::PCPS_STATUS_PORT value to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see @ref group_status_port "bitmask"  //### TODO check syntax
 */
_MBG_API_ATTR int _MBG_API mbg_get_status_port( MBG_DEV_HANDLE dh, PCPS_STATUS_PORT *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_PCPS_STATUS_PORT, p );
    // No endianess conversion required for 8 bit data.
    return _mbgdevio_cnv_ret_val( rc );
  #else
    // When using memory-mapped registers, the internal type used
    // for status port data may be larger than the PCPS_STATUS_PORT type,
    // so we need a cast here.
    *p = (PCPS_STATUS_PORT) _pcps_ddev_read_status_port( dh );
    // No endianess conversion required for 8 bit data.
    return MBG_SUCCESS;
  #endif

}  // mbg_get_status_port



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Generic read function.
 *
 * Writes a command code to a device and reads a number
 * of replied data to a generic buffer.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   cmd   Can be any @ref PCPS_CMD_CODES "command code" supported by the device.
 * @param[out]  p     Pointer to a buffer to be filled up.
 * @param[in]   size  Size of the output buffer.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_generic_write
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_write_gps
 * @see ::mbg_generic_io
 */
_MBG_API_ATTR int _MBG_API mbg_generic_read( MBG_DEV_HANDLE dh, int cmd,
                                             void *p, int size )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_gen_read( dh, cmd, p, size );
  // No type information available, so endianess has to be
  // converted by the caller, if required.
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_generic_read



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Generic read GPS function.
 *
 * Writes a GPS command code to a device and reads a number
 * of data bytes back into a generic buffer.
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   cmd   One of the ::PCPS_CMD_CODES supported by the device.
 * @param[out]  p     Pointer to a buffer to be filled up.
 * @param[in]   size  Size of the buffer, has to match the expected data size associated with @p cmd.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gps_data
 * @see ::mbg_generic_write_gps
 * @see ::mbg_generic_read
 * @see ::mbg_generic_write
 * @see ::mbg_generic_io
 */
_MBG_API_ATTR int _MBG_API mbg_generic_read_gps( MBG_DEV_HANDLE dh, int cmd,
                                                 void *p, int size )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_gen_read_gps( dh, cmd, p, size );
  // No type information available, so endianess has to be
  // converted by the caller, if required.
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_generic_read_gps



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Generic write function.
 *
 * Writes a command code plus an associated number
 * of data bytes to a device.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]  dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  cmd   One of the ::PCPS_CMD_CODES supported by the device.
 * @param[in]  p     Pointer to a buffer of data to be written.
 * @param[in]  size  Size of the buffer, has to match the expected data size associated with @p cmd.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_generic_read
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_write_gps
 * @see ::mbg_generic_io
 */
_MBG_API_ATTR int _MBG_API mbg_generic_write( MBG_DEV_HANDLE dh, int cmd,
                                              const void *p, int size )
{
  MBGDEVIO_RET_VAL rc;
  // No type information available, so endianess has to be
  // converted by the caller, if required.
  rc = _mbgdevio_gen_write( dh, cmd, p, size );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_generic_write



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Generic write GPS function.
 *
 * Writes a GPS command code plus an associated number
 * of data bytes to a device.
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Warning</b>: This is for debugging purposes only!
 * The specialized API calls should be used preferably.
 * Not all devices support each of the ::PC_GPS_COMMANDS.
 *
 * @param[in]  dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  cmd   One of the ::PCPS_CMD_CODES supported by the device.
 * @param[in]  p     Pointer to a buffer of data to be written.
 * @param[in]  size  Size of the buffer, has to match the expected data size associated with @p cmd.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gps_data
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_read
 * @see ::mbg_generic_write
 * @see ::mbg_generic_io
 */
_MBG_API_ATTR int _MBG_API mbg_generic_write_gps( MBG_DEV_HANDLE dh, int cmd,
                                                  const void *p, int size )
{
  MBGDEVIO_RET_VAL rc;
  // No type information available, so endianess has to be
  // converted by the caller, if required.
  rc = _mbgdevio_gen_write_gps( dh, cmd, p, size );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_generic_write_gps



/*HDR*/
/* (Intentionally excluded from Doxygen)
 * @brief Write and/or read generic data to/from a device.
 *
 * The function ::mbg_chk_dev_has_generic_io checks
 * whether this call is supported by a device.
 *
 * <b>Warning</b>: This call is for debugging purposes and internal use only!
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   type    The type of data to be read/written, see @ref PCPS_CMD_CODES.
 * @param[in]   in_p    Pointer to an input buffer with the data to be written.
 * @param[in]   in_sz   Size of the input buffer, has to match the expected data size associated with @p type.
 * @param[out]  out_p   Pointer to an output buffer to be filled up.
 * @param[in]   out_sz  Size of the output buffer, has to match the expected data size associated with @p type.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_generic_io
 * @see ::mbg_generic_read
 * @see ::mbg_generic_write
 * @see ::mbg_generic_read_gps
 * @see ::mbg_generic_write_gps
 */
_MBG_API_ATTR int _MBG_API mbg_generic_io( MBG_DEV_HANDLE dh, int type,
                                           const void *in_p, int in_sz,
                                           void *out_p, int out_sz )
{
  MBGDEVIO_RET_VAL rc;

  #if !defined( _MBGIOCTL_H )
    // The hardware is accessed directly, so we must check
    // here if this call is supported.
    _mbgdevio_chk_cond( _pcps_ddev_has_generic_io( dh ) );
  #endif

  // No type information available, so endianess must be
  // converted by the caller, if required.
  rc = _mbgdevio_gen_io( dh, type, in_p, in_sz, out_p, out_sz );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_generic_io



/*HDR*/
/**
 * @brief Read a ::PCPS_TIME structure returning the current date/time/status.
 *
 * The returned time is local time according to the zone setting onboard
 * the device, with a resolution of 10 ms (i.e. 10ths of seconds) only.
 *
 * This call is supported by every device manufactured by Meinberg.
 * However, for higher accuracy and resolution the @ref mbgdevio_hr_time_fncs or
 * the @ref mbgdevio_fast_timestamp_fncs group of calls should be used preferably,
 * if supported by the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_legacy_time_fncs
 * @see ::mbg_get_hr_time
 * @see ::mbg_set_time
 * @see ::mbg_get_sync_time
 */
_MBG_API_ATTR int _MBG_API mbg_get_time( MBG_DEV_HANDLE dh, PCPS_TIME *p )
{
  MBGDEVIO_RET_VAL rc;

  #if SUPP_SYN1588_USR_SPC
    if ( chk_if_syn1588_type( dh ) )
    {
      rc = mbg_syn1588_get_time( dh, p );
      return _mbgdevio_cnv_ret_val( rc );
    }
  #endif

  rc = _mbgdevio_read_var( dh, PCPS_GIVE_TIME, IOCTL_GET_PCPS_TIME, p );
  // No endianess conversion required.
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_time



/*HDR*/
/**
 * @brief Set the clock onboard the device to a given date and time.
 *
 * The macro ::_pcps_can_set_time checks whether
 * this call is supported by a device.
 *
 * @todo Provide an API function replacing ::_pcps_can_set_time.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_STIME structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_time
 */
_MBG_API_ATTR int _MBG_API mbg_set_time( MBG_DEV_HANDLE dh, const PCPS_STIME *p )
{
  MBGDEVIO_RET_VAL rc;
  // No endianess conversion required.
  _mbgdevio_write_var_chk( dh, PCPS_SET_TIME, IOCTL_SET_PCPS_TIME, p,
                           _pcps_ddev_can_set_time( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_time



/*HDR*/
/**
 * @brief Read the time when the device has last recently synchronized.
 *
 * Fills a ::PCPS_TIME structure with the date/time/status reporting
 * when the device was synchronized the last time to its time source,
 * e.g. the DCF77 signal, the GPS satellites, or similar.
 *
 * The API call ::mbg_chk_dev_has_sync_time checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> If that information is not available on the device,
 * the value of the returned ::PCPS_TIME::sec field is set to 0xFF.
 * The macro ::_pcps_time_is_read can be used to check whether the
 * returned information is valid, or "not set".
 *
 * @todo Provide an API function replacing ::_pcps_has_sync_time.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_sync_time
 * @see ::mbg_get_time
 */
_MBG_API_ATTR int _MBG_API mbg_get_sync_time( MBG_DEV_HANDLE dh, PCPS_TIME *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GIVE_SYNC_TIME, IOCTL_GET_PCPS_SYNC_TIME,
                          p, _pcps_ddev_has_sync_time( dh ) );
  // No endianess conversion required.
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_sync_time



/*HDR*/
/**
 * @brief Wait until the next second change, then return current time.
 *
 * Returns time in a ::PCPS_TIME structure similar to ::mbg_get_time.
 *
 * <b>Note:</b> This API call is supported on Windows only.
 * The call blocks until the kernel driver detects a second change
 * reported by the device. The accuracy of this call is limited
 * to a few milliseconds.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_legacy_time_fncs
 * @see ::mbg_get_time
 */
_MBG_API_ATTR int _MBG_API mbg_get_time_sec_change( MBG_DEV_HANDLE dh, PCPS_TIME *p )
{
  #if defined( MBG_TGT_WIN32 )
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_PCPS_TIME_SEC_CHANGE, p );
    // No endianess conversion required.
    return _mbgdevio_cnv_ret_val( rc );
  #else
    (void) dh;   // avoid warning "never used"
    (void) p;    // avoid warning "never used"
    return MBG_ERR_NOT_SUPP_ON_OS;
  #endif

}  // mbg_get_time_sec_change



/*HDR*/
/**
 * @brief Read the current high resolution time, including status.
 *
 * Fills up a ::PCPS_HR_TIME (High Resolution time) structure containing
 * the current %UTC time (seconds since 1970), %UTC offset, and status
 * (see @ref PCPS_TIME_STATUS_FLAGS).
 *
 * The API call ::mbg_chk_dev_has_hr_time checks whether
 * this call is supported by a device.
 *
 * For details see @ref ::mbgdevio_hr_time_fncs
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_HR_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_fast_timestamp_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_get_hr_time( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p )
{
  MBGDEVIO_RET_VAL rc;

  #if SUPP_SYN1588_USR_SPC
    if ( chk_if_syn1588_type( dh ) )
    {
      rc = mbg_syn1588_get_hr_time( dh, p );
      return _mbgdevio_cnv_ret_val( rc );
    }
  #endif

  _mbgdevio_read_var_chk( dh, PCPS_GIVE_HR_TIME, IOCTL_GET_PCPS_HR_TIME,
                          p, _pcps_ddev_has_hr_time( dh ) );
  _mbg_swab_pcps_hr_time( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_hr_time



/*HDR*/
/* (Intentionally excluded from Doxygen )
 * @brief Write a high resolution time stamp ::PCPS_TIME_STAMP to a device.
 *
 * Used to configure a %UTC time when the device shall generate an event.
 * The API call ::mbg_chk_dev_has_event_time checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This is only supported by devices running some special firmware.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_TIME_STAMP structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_event_time
 */
_MBG_API_ATTR int _MBG_API mbg_set_event_time( MBG_DEV_HANDLE dh, const PCPS_TIME_STAMP *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    PCPS_TIME_STAMP tmp = *p;
    _mbg_swab_pcps_time_stamp( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_var_chk( dh, PCPS_SET_EVENT_TIME, IOCTL_SET_PCPS_EVENT_TIME,
                           p, _pcps_ddev_has_event_time( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_event_time



/*HDR*/
/**
 * @brief Read the serial port configuration from an old type of device.
 *
 * @deprecated Direct usage of this function is deprecated. The generic
 * API function ::mbg_get_serial_settings should be used instead
 * which fully supports the capabilities of current devices.
 *
 * The API call ::mbg_chk_dev_has_serial checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_SERIAL structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 */
_MBG_API_ATTR int _MBG_API mbg_get_serial( MBG_DEV_HANDLE dh, PCPS_SERIAL *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_var( dh, PCPS_GET_SERIAL, IOCTL_GET_PCPS_SERIAL, p );
  // No endianess conversion required.
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_serial



/*HDR*/
/**
 * @brief Write the serial port configuration to an old type of device.
 *
 * @deprecated Direct usage of this function is deprecated. The generic
 * API function ::mbg_save_serial_settings should be used instead
 * which fully supports the capabilities of current devices.
 *
 * The API call ::mbg_chk_dev_has_serial checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_SERIAL structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 */
_MBG_API_ATTR int _MBG_API mbg_set_serial( MBG_DEV_HANDLE dh, const PCPS_SERIAL *p )
{
  MBGDEVIO_RET_VAL rc;
  // No endianess conversion required.
  rc = _mbgdevio_write_var( dh, PCPS_SET_SERIAL, IOCTL_SET_PCPS_SERIAL, p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_serial



/*HDR*/
/**
 * @brief Read time zone/daylight saving configuration code from a device.
 *
 * The APIs using ::PCPS_TZCODE are only supported by some simpler devices
 * and allow just a very basic configuration.
 *
 * The API call ::mbg_chk_dev_has_tzcode checks whether
 * this call is supported by a device.
 *
 * Other devices may support the ::mbg_get_pcps_tzdl or ::mbg_get_gps_tzdl
 * calls instead which allow for a more detailed configuration of the
 * time zone and daylight saving settings.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TZCODE structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzcode
 * @see ::mbg_set_tzcode
 * @see ::mbg_get_pcps_tzdl
 * @see ::mbg_get_gps_tzdl
 */
_MBG_API_ATTR int _MBG_API mbg_get_tzcode( MBG_DEV_HANDLE dh, PCPS_TZCODE *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_TZCODE, IOCTL_GET_PCPS_TZCODE,
                          p, _pcps_ddev_has_tzcode( dh ) );
  // No endianess conversion required.
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_tzcode



/*HDR*/
/**
 * @brief Write time zone/daylight saving configuration code to a device.
 *
 * The APIs using ::PCPS_TZCODE are only supported by some simpler devices
 * and allow only a very basic configuration.
 *
 * The API call ::mbg_chk_dev_has_tzcode checks whether
 * this call is supported by a device.
 *
 * Other devices may support the ::mbg_set_pcps_tzdl or ::mbg_set_gps_tzdl
 * calls instead which allow for a more detailed configuration of the
 * time zone and daylight saving settings.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_TZCODE structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzcode
 * @see ::mbg_get_tzcode
 * @see ::mbg_set_pcps_tzdl
 * @see ::mbg_set_gps_tzdl
 */
_MBG_API_ATTR int _MBG_API mbg_set_tzcode( MBG_DEV_HANDLE dh, const PCPS_TZCODE *p )
{
  MBGDEVIO_RET_VAL rc;
  // No endianess conversion required.
  _mbgdevio_write_var_chk( dh, PCPS_SET_TZCODE, IOCTL_SET_PCPS_TZCODE,
                           p, _pcps_ddev_has_tzcode( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_tzcode



/*HDR*/
/**
 * @brief Read time zone/daylight saving parameters from a device.
 *
 * This function fills up a ::PCPS_TZDL structure which supports a more
 * detailed configuration of time zone and daylight saving than the ::PCPS_TZCODE
 * type.
 *
 * The API call ::mbg_chk_dev_has_pcps_tzdl checks whether
 * this call is supported by a device.
 *
 * Other devices may support the ::mbg_get_tzcode or ::mbg_get_gps_tzdl
 * calls instead.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TZDL structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pcps_tzdl
 * @see ::mbg_set_pcps_tzdl
 * @see ::mbg_get_tzcode
 * @see ::mbg_get_gps_tzdl
 */
_MBG_API_ATTR int _MBG_API mbg_get_pcps_tzdl( MBG_DEV_HANDLE dh, PCPS_TZDL *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_PCPS_TZDL, IOCTL_GET_PCPS_TZDL,
                          p, _pcps_ddev_has_pcps_tzdl( dh ) );
  _mbg_swab_pcps_tzdl( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_pcps_tzdl



/*HDR*/
/**
 * @brief Write time zone/daylight saving parameters to a device.
 *
 * This function passes a ::PCPS_TZDL structure to a device which supports
 * a more detailed configuration of time zone and daylight saving than the
 * ::PCPS_TZCODE type.
 *
 * The API call ::mbg_chk_dev_has_pcps_tzdl checks whether
 * this call is supported by a device.
 * Other devices may support the ::mbg_set_tzcode or ::mbg_set_gps_tzdl
 * calls instead.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PCPS_TZDL structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pcps_tzdl
 * @see ::mbg_get_pcps_tzdl
 * @see ::mbg_set_tzcode
 * @see ::mbg_set_gps_tzdl
 */
_MBG_API_ATTR int _MBG_API mbg_set_pcps_tzdl( MBG_DEV_HANDLE dh, const PCPS_TZDL *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    PCPS_TZDL tmp = *p;
    _mbg_swab_pcps_tzdl( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_var_chk( dh, PCPS_SET_PCPS_TZDL, IOCTL_SET_PCPS_TZDL,
                           p, _pcps_ddev_has_pcps_tzdl( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_pcps_tzdl



/*HDR*/
/**
 * @brief Read the %UTC offset configuration of the reference time from a device.
 *
 * This parameter is used to specify the %UTC offset of an incoming
 * reference time signal if a kind of time signal e.g. an IRIG input
 * signal) does not provide this information.
 *
 * The API call ::mbg_chk_dev_has_ref_offs checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_REF_OFFS value to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ref_offs
 * @see ::mbg_set_ref_offs
 */
_MBG_API_ATTR int _MBG_API mbg_get_ref_offs( MBG_DEV_HANDLE dh, MBG_REF_OFFS *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_REF_OFFS, IOCTL_GET_REF_OFFS,
                          p, _pcps_ddev_has_ref_offs( dh ) );
  _mbg_swab_mbg_ref_offs( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ref_offs



/*HDR*/
/**
 * @brief Write the %UTC offset configuration of the reference time to a device.
 *
 * This parameter is used to specify the %UTC offset of an incoming
 * reference time signal if a kind of time signal e.g. an IRIG input
 * signal) does not provide this information.
 *
 * The API call ::mbg_chk_dev_has_ref_offs checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::MBG_REF_OFFS value to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ref_offs
 * @see ::mbg_get_ref_offs
 */
_MBG_API_ATTR int _MBG_API mbg_set_ref_offs( MBG_DEV_HANDLE dh, const MBG_REF_OFFS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    MBG_REF_OFFS tmp = *p;
    _mbg_swab_mbg_ref_offs( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_var_chk( dh, PCPS_SET_REF_OFFS, IOCTL_SET_REF_OFFS,
                           p, _pcps_ddev_has_ref_offs( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_ref_offs



/*HDR*/
/**
 * @brief Read a ::MBG_OPT_INFO structure containing optional settings, controlled by flags.
 *
 * The ::MBG_OPT_INFO structure contains a mask of supported flags plus the current
 * settings of those flags.
 *
 * The API call ::mbg_chk_dev_has_opt_flags checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_OPT_INFO structure to be filled up
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_opt_flags
 * @see ::mbg_set_opt_settings
 */
_MBG_API_ATTR int _MBG_API mbg_get_opt_info( MBG_DEV_HANDLE dh, MBG_OPT_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_OPT_INFO, IOCTL_GET_MBG_OPT_INFO,
                          p, _pcps_ddev_has_opt_flags( dh ) );
  _mbg_swab_mbg_opt_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_opt_info



/*HDR*/
/**
 * @brief Write a ::MBG_OPT_SETTINGS structure containing optional device settings.
 *
 * The API call ::mbg_chk_dev_has_opt_flags checks whether
 * this call is supported by a device.
 *
 * ::mbg_get_opt_info should be called first to check which of
 * the specified flags is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_OPT_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_opt_flags
 * @see ::mbg_get_opt_info
 */
_MBG_API_ATTR int _MBG_API mbg_set_opt_settings( MBG_DEV_HANDLE dh, const MBG_OPT_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    MBG_OPT_SETTINGS tmp = *p;
    _mbg_swab_mbg_opt_settings( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_var_chk( dh, PCPS_SET_OPT_SETTINGS,
                           IOCTL_SET_MBG_OPT_SETTINGS, p,
                           _pcps_ddev_has_opt_flags( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_opt_settings



/*HDR*/
/**
 * @brief Read the current IRIG input settings plus capabilities.
 *
 * @deprecated Calling this function directly is deprecated. The function
 * ::mbg_get_all_irig_rx_info should be used instead which also reads some
 * other associated parameters affecting the behaviour of the IRIG input.
 *
 * The API call ::mbg_chk_dev_is_tcr checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Address of an ::IRIG_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_all_irig_rx_info
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
_MBG_API_ATTR int _MBG_API mbg_get_irig_rx_info( MBG_DEV_HANDLE dh, IRIG_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_IRIG_RX_INFO, IOCTL_GET_PCPS_IRIG_RX_INFO,
                          p, _pcps_ddev_is_irig_rx( dh ) );
  _mbg_swab_irig_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_irig_rx_info



/*HDR*/
/**
 * @brief Write an ::IRIG_SETTINGS structure to a device to configure an IRIG input.
 *
 * @deprecated Calling this function directly is deprecated. The function
 * ::mbg_save_all_irig_rx_settings should be used instead which also writes some
 * other associated parameters affecting the behaviour of the IRIG input.
 *
 * The API call ::mbg_chk_dev_is_tcr checks whether
 * this call is supported by a device.
 * ::mbg_get_irig_rx_info should be called first to determine
 * the possible settings supported by the input of the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::IRIG_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_all_irig_rx_settings
 * @see ::mbg_get_irig_rx_info
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
_MBG_API_ATTR int _MBG_API mbg_set_irig_rx_settings( MBG_DEV_HANDLE dh, const IRIG_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    IRIG_SETTINGS tmp = *p;
    _mbg_swab_irig_settings( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_var_chk( dh, PCPS_SET_IRIG_RX_SETTINGS,
                           IOCTL_SET_PCPS_IRIG_RX_SETTINGS, p,
                           _pcps_ddev_is_irig_rx( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_irig_rx_settings



/*HDR*/
/**
 * @brief Read all IRIG input configuration information from a device.
 *
 * @param[in]  dh           Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p_obs        Obsolete pointer kept for compatibility, should be @a NULL.
 * @param[in]  p_irig_info  Pointer to a ::IRIG_SETTINGS structure to be read.
 * @param[in]  p_ref_offs   Pointer to a ::MBG_REF_OFFS structure to be read.
 * @param[in]  p_opt_info   Pointer to a ::MBG_OPT_SETTINGS structure to be read.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_all_irig_rx_settings
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_set_ref_offs
 * @see ::mbg_set_opt_settings
 */
_MBG_API_ATTR int _MBG_API mbg_get_all_irig_rx_info( MBG_DEV_HANDLE dh,
                                                     const void *p_obs,
                                                     IRIG_INFO *p_irig_info,
                                                     MBG_REF_OFFS *p_ref_offs,
                                                     MBG_OPT_INFO *p_opt_info )
{
  int rc = mbg_chk_dev_is_tcr( dh );

  if ( mbg_rc_is_error( rc ) )
    goto out;

  (void) p_obs;  // Avoid warning "never used".

  rc = mbg_get_irig_rx_info( dh, p_irig_info );

  if ( mbg_rc_is_success( rc ) )
  {
    rc = mbg_chk_dev_has_ref_offs( dh );

    if ( mbg_rc_is_success( rc ) )
      rc = mbg_get_ref_offs( dh, p_ref_offs );
  }

  if ( mbg_rc_is_success( rc ) )
  {
    rc = mbg_chk_dev_has_opt_flags( dh );

    if ( mbg_rc_is_success( rc ) )
      rc = mbg_get_opt_info( dh, p_opt_info );
  }

out:
  return rc;

}  // mbg_get_all_irig_rx_info



/*HDR*/
/**
 * @brief Write all IRIG input configuration settings to a device.
 *
 * The API call ::mbg_chk_dev_is_tcr checks whether
 * this call is supported by a device.
 * ::mbg_get_all_irig_rx_info should be called before to determine
 * the possible settings supported by the IRIG input.
 *
 * @param[in]   dh              Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   p_obs           Obsolete pointer kept for compatibility, should be @a NULL.
 * @param[out]  p_irig_settings Pointer to a ::IRIG_SETTINGS structure to be written.
 * @param[out]  p_ref_offs      Pointer to a ::MBG_REF_OFFS structure to be written.
 * @param[out]  p_opt_settings  Pointer to a ::MBG_OPT_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_all_irig_rx_info
 * @see ::mbg_set_irig_rx_settings
 * @see ::mbg_set_ref_offs
 * @see ::mbg_set_opt_settings
 */
_MBG_API_ATTR int _MBG_API mbg_save_all_irig_rx_settings( MBG_DEV_HANDLE dh,
                                                          const void *p_obs,
                                                          const IRIG_SETTINGS *p_irig_settings,
                                                          const MBG_REF_OFFS *p_ref_offs,
                                                          const MBG_OPT_SETTINGS *p_opt_settings )
{
  int rc = mbg_chk_dev_is_tcr( dh );

  if ( mbg_rc_is_error( rc ) )
    goto out;

  (void) p_obs;  // Avoid warning "never used".

  rc = mbg_set_irig_rx_settings( dh, p_irig_settings );

  if ( mbg_rc_is_success( rc ) )
  {
    rc = mbg_chk_dev_has_ref_offs( dh );

    if ( mbg_rc_is_success( rc ) )
      rc = mbg_set_ref_offs( dh, p_ref_offs );
  }

  if ( mbg_rc_is_success( rc ) )
  {
    rc = mbg_chk_dev_has_opt_flags( dh );

    if ( mbg_rc_is_success( rc ) )
      rc = mbg_set_opt_settings( dh, p_opt_settings );
  }

out:
  return rc;

}  // mbg_save_all_irig_rx_settings



/*HDR*/
/**
 * @brief Read the control function bits received from an incoming IRIG signal.
 *
 * This function fills an ::MBG_IRIG_CTRL_BITS structure with the control function
 * bits decoded from the incoming IRIG signal.
 *
 * The meaning of these bits depends on the selected time code format.
 *
 * In some IRIG formats these bits provide some well-known information which can
 * also be evaluated by the device. For example, in IEEE 1344 or IEEE C37.118 code
 * the control function bits are used to provide the year number, %UTC offset,
 * DST status, leap second warning, etc.
 *
 * For most IRIG code formats, however, these bits are reserved, i.e. not used
 * at all, or application defined, depending on the configuration of the IRIG
 * generator providing the IRIG signal.
 *
 * In the latter case the application has to evaluate the received control function
 * bits and can use this function to retrieve these bits from the receiver device.
 *
 * The API call ::mbg_chk_dev_has_irig_ctrl_bits checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_IRIG_CTRL_BITS type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_irig_ctrl_bits
 */
_MBG_API_ATTR int _MBG_API mbg_get_irig_ctrl_bits( MBG_DEV_HANDLE dh,
                                                   MBG_IRIG_CTRL_BITS *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_var( dh, PCPS_GET_IRIG_CTRL_BITS, IOCTL_GET_IRIG_CTRL_BITS, p );
  _mbg_swab_irig_ctrl_bits( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_irig_ctrl_bits



/*HDR*/
/**
 * @brief Read raw IRIG data from an IRIG receiver.
 *
 * This function reads an ::MBG_RAW_IRIG_DATA structure with the raw data bits received
 * from the incoming IRIG signal. This enables an application itself to decode the
 * information provided by the IRIG signal.
 *
 * The API call ::mbg_chk_dev_has_raw_irig_data checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_RAW_IRIG_DATA type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_raw_irig_data
 * @see ::mbg_get_raw_irig_data_on_sec_change
 */
_MBG_API_ATTR int _MBG_API mbg_get_raw_irig_data( MBG_DEV_HANDLE dh,
                                                  MBG_RAW_IRIG_DATA *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_var( dh, PCPS_GET_RAW_IRIG_DATA, IOCTL_GET_RAW_IRIG_DATA, p );
  // No endianess conversion required.
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_raw_irig_data



/*HDR*/
/**
 * @brief Wait for second changeover then read raw IRIG data from an IRIG receiver.
 *
 * This function waits until the second of the on-board time rolls over,
 * and then reads the last recent raw IRIG data from the device.
 *
 * The API call ::mbg_chk_dev_has_raw_irig_data checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> The ::mbg_get_time_sec_change function called
 * by this function is supported on Windows only, so this function
 * can also be used on Windows only.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_RAW_IRIG_DATA type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_raw_irig_data
 * @see ::mbg_get_raw_irig_data
 * @see ::mbg_get_time_sec_change
 */
_MBG_API_ATTR int _MBG_API mbg_get_raw_irig_data_on_sec_change( MBG_DEV_HANDLE dh,
                                                                MBG_RAW_IRIG_DATA *p )
{
  PCPS_TIME t;
  MBGDEVIO_RET_VAL rc;

   rc = mbg_get_time_sec_change( dh, &t );

   if ( mbg_rc_is_success( rc ) )
     rc = mbg_get_raw_irig_data( dh, p );

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_raw_irig_data_on_sec_change



/*HDR*/
/**
 * @brief Read the IRIG time and day-of-year number from an IRIG receiver.
 *
 * Reads a ::PCPS_IRIG_TIME structure with the raw IRIG day-of-year number
 * and time decoded from the latest IRIG input frame. If the configured IRIG code
 * also contains the year number, the year number is also returned, otherwise
 * the returned year number is 0xFF.
 *
 * The API call ::mbg_chk_dev_has_irig_time checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_IRIG_TIME type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_irig_time
 */
_MBG_API_ATTR int _MBG_API mbg_get_irig_time( MBG_DEV_HANDLE dh,
                                              PCPS_IRIG_TIME *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_var( dh, PCPS_GIVE_IRIG_TIME, IOCTL_GET_IRIG_TIME, p );
  _mbg_swab_pcps_irig_time( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_irig_time



/*HDR*/
/**
 * @brief Clear the on-board time capture FIFO buffer.
 *
 * The API call ::mbg_chk_dev_can_clr_ucap_buff checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_can_clr_ucap_buff
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 */
_MBG_API_ATTR int _MBG_API mbg_clr_ucap_buff( MBG_DEV_HANDLE dh )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_write_cmd_chk( dh, PCPS_CLR_UCAP_BUFF, IOCTL_PCPS_CLR_UCAP_BUFF,
                           _pcps_ddev_can_clr_ucap_buff( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_clr_ucap_buff



/*HDR*/
/**
 * @brief Read information on the event capture buffer.
 *
 * Reads a ::PCPS_UCAP_ENTRIES structure with the number of user capture
 * events actually stored in the FIFO buffer, and the maximum number of
 * events that can be held by the buffer.
 *
 * The API call ::mbg_chk_dev_has_ucap checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_UCAP_ENTRIES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ucap
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 */
_MBG_API_ATTR int _MBG_API mbg_get_ucap_entries( MBG_DEV_HANDLE dh, PCPS_UCAP_ENTRIES *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GIVE_UCAP_ENTRIES,
                          IOCTL_GET_PCPS_UCAP_ENTRIES, p,
                          _pcps_ddev_has_ucap( dh ) );
  _mbg_swab_pcps_ucap_entries( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ucap_entries



/*HDR*/
/**
 * @brief Retrieve a single time capture event from the on-board FIFO buffer.
 *
 * The capture event is returned in a ::PCPS_HR_TIME structure. The oldest entry
 * in the FIFO is retrieved and then removed from the FIFO.
 *
 * If no capture event is available in the FIFO buffer, both the seconds
 * and the fractions of the returned timestamp are 0.
 *
 * The API call ::mbg_chk_dev_has_ucap checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This call is very much faster than the older ::mbg_get_gps_ucap
 * call which is obsolete but still supported for compatibility with
 * older devices.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_HR_TIME structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ucap
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_clr_ucap_buff
 */
_MBG_API_ATTR int _MBG_API mbg_get_ucap_event( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GIVE_UCAP_EVENT,
                          IOCTL_GET_PCPS_UCAP_EVENT, p,
                          _pcps_ddev_has_ucap( dh ) );
  _mbg_swab_pcps_hr_time( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ucap_event



/*HDR*/
/**
 * @brief Read the time zone/daylight saving parameters.
 *
 * This function returns the time zone/daylight saving parameters
 * in a ::TZDL structure.
 *
 * The API call ::mbg_chk_dev_has_tzdl checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> In spite of the function name this call may also be
 * supported by non-GPS devices. Other devices may support the ::mbg_get_tzcode
 * or ::mbg_get_pcps_tzdl calls instead.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TZDL structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzdl
 * @see ::mbg_set_gps_tzdl
 * @see ::mbg_get_tzcode
 * @see ::mbg_get_pcps_tzdl
 * @see @ref group_tzdl
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_tzdl( MBG_DEV_HANDLE dh, TZDL *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_TZDL, IOCTL_GET_GPS_TZDL, p );
  _mbg_swab_tzdl( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_tzdl



/*HDR*/
/**
 * @brief Write the time zone/daylight saving parameters.
 *
 * This function writes the time zone/daylight saving parameters
 * in a ::TZDL structure to a device.
 *
 * The API call ::mbg_chk_dev_has_tzdl checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> In spite of the function name this call may also be
 * supported by non-GPS devices. Other devices may support the ::mbg_set_tzcode
 * or ::mbg_set_pcps_tzdl calls instead.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::TZDL structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tzdl
 * @see ::mbg_get_gps_tzdl
 * @see ::mbg_set_tzcode
 * @see ::mbg_set_pcps_tzdl
 * @see @ref group_tzdl
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_tzdl( MBG_DEV_HANDLE dh, const TZDL *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    TZDL tmp = *p;
    _mbg_swab_tzdl( &tmp );
    p = &tmp;
  #endif
  rc = _mbgdevio_write_gps_var( dh, PC_GPS_TZDL, IOCTL_SET_GPS_TZDL, p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_tzdl



/*HDR*/
/**
 * @brief Retrieve the software revision of a GPS receiver.
 *
 * @deprecated This function is deprecated.
 *
 * This function is deprecated, but still supported
 * for compatibility with older GPS devices. Normally
 * the software revision is part of the ::RECEIVER_INFO
 * structure. See ::mbg_setup_receiver_info which takes
 * care of the different options.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::SW_REV structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_chk_dev_is_gps
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_sw_rev( MBG_DEV_HANDLE dh, SW_REV *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_SW_REV, IOCTL_GET_GPS_SW_REV, p );
  _mbg_swab_sw_rev( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_sw_rev



/*HDR*/
/**
 * @brief Retrieve the status of the battery buffered GPS variables.
 *
 * GPS receivers require some navigational data set to be available
 * to be able to decode position and time accurately. This data set
 * is transmitted periodically by the satellites, so it can
 * simply be collected if it's not available.
 *
 * The ::BVAR_STAT type reports which parts of the data set are
 * available in the receiver, and which are not.
 *
 * If the available data set is not complete, the receiver
 * stays in COLD BOOT mode until all data have been received
 * and thus all data sets are valid.
 *
 * The API call ::mbg_chk_dev_has_bvar_stat checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::BVAR_STAT structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_bvar_stat
 * @see ::BVAR_FLAGS
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_bvar_stat( MBG_DEV_HANDLE dh, BVAR_STAT *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_BVAR_STAT, IOCTL_GET_GPS_BVAR_STAT, p );
  _mbg_swab_bvar_stat( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_bvar_stat



/*HDR*/
/**
 * @brief Read the current on-board time using a ::TTM structure.
 *
 * The API call ::mbg_chk_dev_has_ttm_time checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This API call is very slow, so ::mbg_get_hr_time or
 * ::mbg_get_fast_hr_timestamp or associated calls should be used preferably.
 *
 * <b>Note:</b> This function should be preferred over ::mbg_get_gps_time,
 * which has been deprecated.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TTM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ttm_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_fast_hr_timestamp
 */
_MBG_API_ATTR int _MBG_API mbg_get_ttm_time( MBG_DEV_HANDLE dh, TTM *p )
{
  MBGDEVIO_RET_VAL rc;

  rc = _mbgdevio_read_gps_var( dh, PC_GPS_TIME, IOCTL_GET_GPS_TIME, p );

  if ( mbg_rc_is_success( rc ) )
    _mbg_swab_ttm( p );

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ttm_time



/*HDR*/
/**
 * @brief Read the current on-board time using a ::TTM structure.
 *
 * The API call ::mbg_chk_dev_has_ttm_time checks whether
 * this call is supported by a device.
 *
 * @deprecated This function is deprecated, use ::mbg_get_ttm_time preferably.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TTM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ttm_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_fast_hr_timestamp
 */
_MBG_API_ATTR int _DEPRECATED_BY( "mbg_get_ttm_time" ) _MBG_API mbg_get_gps_time( MBG_DEV_HANDLE dh, TTM *p )
{
  return mbg_get_ttm_time( dh, p );

}  // mbg_get_gps_time



/*HDR*/
/**
 * @brief Set the time on a GPS receiver device.
 *
 * Write a ::TTM structure to a GPS receiver in order to set
 * the on-board date and time. Date and time must be local time
 * according to the time zone configuration (::TZDL) onboard
 * the device.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::TTM structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_time( MBG_DEV_HANDLE dh, const TTM *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    TTM tmp = *p;
    _mbg_swab_ttm( &tmp );
    p = &tmp;
  #endif
  rc = _mbgdevio_write_gps_var( dh, PC_GPS_TIME, IOCTL_SET_GPS_TIME, p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_time



/*HDR*/
/**
 * @brief Read a ::PORT_PARM structure with the serial port configuration.
 *
 * @deprecated This function is deprecated, use ::mbg_get_serial_settings preferably.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This function is deprecated since it is only
 * supported by a certain class of devices and can handle only
 * up to 2 serial ports. The generic function ::mbg_get_serial_settings
 * should be used instead.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PORT_PARM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_port_parm( MBG_DEV_HANDLE dh, PORT_PARM *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_PORT_PARM, IOCTL_GET_GPS_PORT_PARM, p );
  _mbg_swab_port_parm( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_port_parm



/*HDR*/
/**
 * @brief Write a ::PORT_PARM structure to configure the on-board serial ports.
 *
 * @deprecated This function is deprecated, use ::mbg_save_serial_settings preferably.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This function is deprecated since it is only
 * supported by a certain class of devices and can handle only
 * up to 2 ports. The generic function ::mbg_save_serial_settings
 * should be used instead.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PORT_PARM structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_port_parm( MBG_DEV_HANDLE dh, const PORT_PARM *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    PORT_PARM tmp = *p;
    _mbg_swab_port_parm( &tmp );
    p = &tmp;
  #endif
  rc = _mbgdevio_write_gps_var( dh, PC_GPS_PORT_PARM, IOCTL_SET_GPS_PORT_PARM, p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_port_parm



/*HDR*/
/**
 * @brief Read an ::ANT_INFO structure to retrieve an extended GPS antenna status.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> Normally the current antenna connection status can also be
 * determined by evaluation of the ::PCPS_TIME::signal or ::PCPS_HR_TIME::signal
 * fields. The "disconnected" status reported by ::ANT_INFO disappears only if
 * the antenna has been reconnected <b>and</b> the receiver has synchronized
 * to the GPS satellites again.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::ANT_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_ant_info( MBG_DEV_HANDLE dh, ANT_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_ANT_INFO, IOCTL_GET_GPS_ANT_INFO, p );
  _mbg_swab_ant_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_ant_info



/*HDR*/
/**
 * @brief Read a time capture event from the on-board FIFO buffer using a ::TTM structure.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * <b>Note:</b> This call is pretty slow and has been obsoleted by
 * ::mbg_get_ucap_event which should be used preferably, if supported
 * by the device. Anyway, this call is still supported for compatibility
 * with older devices which don't support ::mbg_get_ucap_event.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TTM structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_ucap_entries
 * @see ::mbg_get_ucap_event
 * @see ::mbg_clr_ucap_buff
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_ucap( MBG_DEV_HANDLE dh, TTM *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_UCAP, IOCTL_GET_GPS_UCAP, p );
  _mbg_swab_ttm( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_ucap



/*HDR*/
/**
 * @brief Read the ::ENABLE_FLAGS structure controlling when outputs are to be enabled.
 *
 * The ::ENABLE_FLAGS structure controls whether certain signal outputs
 * are to be enabled immediately after the device was powered up, or only
 * after the device has synchronized to its input signal.
 *
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Note:</b> Not all of the input signals specified for the
 * ::ENABLE_FLAGS structure can be modified individually.
 * See ::ENABLE_FLAGS_CODES.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::ENABLE_FLAGS structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::ENABLE_FLAGS
 * @see ::ENABLE_FLAGS_CODES
 * @see ::mbg_set_gps_enable_flags
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_enable_flags( MBG_DEV_HANDLE dh, ENABLE_FLAGS *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_ENABLE_FLAGS,
                          IOCTL_GET_GPS_ENABLE_FLAGS, p );
  _mbg_swab_enable_flags( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_enable_flags



/*HDR*/
/**
 * @brief Write an ::ENABLE_FLAGS structure to configure when outputs shall be enabled.
 *
 * The ::ENABLE_FLAGS structure controls whether certain signal outputs
 * are to be enabled immediately after the device was powered up, or only
 * after the device has synchronized to its input signal.
 *
 * The function ::mbg_chk_dev_has_gps_data can be used to check
 * whether this call is supported by a device.
 *
 * <b>Note:</b> Not all of the input signals specified for the
 * ::ENABLE_FLAGS structure can be modified individually.
 * See ::ENABLE_FLAGS_CODES.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p Pointer to a ENABLE_FLAGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::ENABLE_FLAGS
 * @see ::ENABLE_FLAGS_CODES
 * @see ::mbg_get_gps_enable_flags
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_enable_flags( MBG_DEV_HANDLE dh,
                                       const ENABLE_FLAGS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    ENABLE_FLAGS tmp = *p;
    _mbg_swab_enable_flags( &tmp );
    p = &tmp;
  #endif
  rc = _mbgdevio_write_gps_var( dh, PC_GPS_ENABLE_FLAGS,
                           IOCTL_SET_GPS_ENABLE_FLAGS, p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_enable_flags



/*HDR*/
/**
 * @brief Read the extended GPS receiver status from a device.
 *
 * The ::STAT_INFO structure reports the status of the GPS receiver,
 * including mode of operation and number of visible/usable satellites.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::STAT_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::STAT_INFO
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_stat_info( MBG_DEV_HANDLE dh, STAT_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_STAT_INFO, IOCTL_GET_GPS_STAT_INFO, p );
  _mbg_swab_stat_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_stat_info



/*HDR*/
/**
 * @brief Send one of the ::PC_GPS_COMMANDS to a GPS receiver device.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::GPS_CMD.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::PC_GPS_COMMANDS
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_cmd( MBG_DEV_HANDLE dh, const GPS_CMD *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    GPS_CMD tmp = *p;
    _mbg_swab_gps_cmd( &tmp );
    p = &tmp;
  #endif
  rc = _mbgdevio_write_gps_var( dh, PC_GPS_CMD, IOCTL_SET_GPS_CMD, p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_cmd


/*HDR*/
/**
 * @brief Read the current geographic position from a GPS/GNSS device.
 *
 * The returned ::POS structure contains the current position in
 * ECEF (Earth Centered, Earth Fixed) kartesian coordinates, and in
 * geographic coordinates with different formats, using the WGS84
 * geographic datum.
 *
 * The API call ::mbg_chk_dev_has_rcvr_pos checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::POS structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pos_xyz
 * @see ::mbg_set_gps_pos_lla
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_pos( MBG_DEV_HANDLE dh, POS *p )
{
  MBGDEVIO_RET_VAL rc;
  rc = _mbgdevio_read_gps_var( dh, PC_GPS_POS, IOCTL_GET_GPS_POS, p );
  swap_pos_doubles( p );
  _mbg_swab_pos( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_pos



/*HDR*/
/**
 * @brief Set the GPS/GNSS receiver position using ::XYZ coordinates.
 *
 * The structure ::XYZ must specify the new position in ECEF
 * (Earth Centered, Earth Fixed) kartesian coordinates.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Position in ::XYZ format to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pos_lla
 * @see ::mbg_get_gps_pos
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_pos_xyz( MBG_DEV_HANDLE dh, const XYZ p )
{
  MBGDEVIO_RET_VAL rc;
  XYZ xyz;
  int i;

  for ( i = 0; i < N_XYZ; i++ )
  {
    xyz[i] = p[i];
    swap_double( &xyz[i] );
    _mbg_swab_double( &xyz[i] );
  }

  // _mbgdevio_write_gps_var() would fail here because
  // XYZ is an array, not a structure.
  rc = _mbgdevio_write_gps( dh, PC_GPS_POS_XYZ, IOCTL_SET_GPS_POS_XYZ,
                            xyz, sizeof( XYZ ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_pos_xyz



/*HDR*/
/**
 * @brief Set the GPS/GNSS receiver position using ::LLA coordinates.
 *
 * The structure ::LLA must specify the new position as longitude,
 * latitude, and altitude, using the WGS84 geographic datum.
 *
 * The API call ::mbg_chk_dev_is_gps checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Position in ::LLA format to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pos_xyz
 * @see ::mbg_get_gps_pos
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_pos_lla( MBG_DEV_HANDLE dh, const LLA p )
{
  MBGDEVIO_RET_VAL rc;
  LLA lla;
  int i;

  for ( i = 0; i < N_LLA; i++ )
  {
    lla[i] = p[i];
    swap_double( &lla[i] );
    _mbg_swab_double( &lla[i] );
  }

  // _mbgdevio_write_gps_var() would fail here because
  // LLA is an array, not a structure.
  rc = _mbgdevio_write_gps( dh, PC_GPS_POS_LLA, IOCTL_SET_GPS_POS_LLA, lla, sizeof( LLA ) );

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_pos_lla



/*HDR*/
/**
 * @brief Read the configured antenna cable length from a device.
 *
 * The antenna cable length parameter is used by GPS/GNSS receivers
 * to compensate the propagation delay of the RF signal over the antenna
 * cable, which is about 5 ns/m.
 *
 * The API call ::mbg_chk_dev_has_cab_len checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::ANT_CABLE_LEN structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_cab_len
 * @see ::mbg_set_gps_ant_cable_len
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_ant_cable_len( MBG_DEV_HANDLE dh, ANT_CABLE_LEN *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_ANT_CABLE_LEN,
                              IOCTL_GET_GPS_ANT_CABLE_LEN, p,
                              _pcps_ddev_has_cab_len( dh ) );
  _mbg_swab_ant_cable_len( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_ant_cable_len



/*HDR*/
/**
 * @brief Write the GPS antenna cable length configuration to a device.
 *
 * The antenna cable length parameter is used by GPS/GNSS receivers
 * to compensate the propagation delay of the RF signal over the antenna
 * cable, which is about 5 ns/m.
 *
 * The API call ::mbg_chk_dev_has_cab_len checks whether
 * this call is supported by a device.
 *
 * @note Unfortunately, the maximum value accepted by a device
 * may vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value.
 * The function ::mbg_set_and_check_gps_ant_cable_len checks
 * whether the new value was accepted by the device, and therefore
 * should be used preferably.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::ANT_CABLE_LEN structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_cab_len
 * @see ::mbg_get_gps_ant_cable_len
 * @see ::mbg_set_and_check_gps_ant_cable_len
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_ant_cable_len( MBG_DEV_HANDLE dh,
                                                      const ANT_CABLE_LEN *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    ANT_CABLE_LEN tmp = *p;
    _mbg_swab_ant_cable_len( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_ANT_CABLE_LEN,
                               IOCTL_SET_GPS_ANT_CABLE_LEN, p,
                               _pcps_ddev_has_cab_len( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_ant_cable_len



/*HDR*/
/**
 * @brief Write and check the GPS antenna cable length.
 *
 * The antenna cable length parameter is used by GPS/GNSS receivers
 * to compensate the propagation delay of the RF signal over the antenna
 * cable, which is about 5 ns/m.
 *
 * The API call ::mbg_chk_dev_has_cab_len checks whether
 * this call is supported by a device.
 *
 * Unfortunately, the maximum value accepted by a device may
 * vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value:
 *
 * - Some devices truncate the new value to the maximum value that
 *   is hard-coded in the firmware, so that the effective value
 *   corresponds to the maximum value, but neither matches the
 *   original value, nor the desired new value. This case can only
 *   be determined if the optional parameter @p p_org_val is provided.
 *
 * - Some devices simply don't accept and store a value that is
 *   out of range, so the original value is left unchanged.
 *
 * @param[in]      dh         Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in,out]  p_new_val  Pointer to a ::ANT_CABLE_LEN variable to be written.
 * @param[in]      p_org_val  Optional pointer to a ::ANT_CABLE_LEN variable with
 *                            the original value read previously, may be @a NULL.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *         If the return code is ::MBG_ERR_CFG or ::MBG_ERR_CFG_TRUNC,
 *         the re-read value is stored in @p p_new_val for convenience.
 *
 * @see ::mbg_chk_dev_has_cab_len
 * @see ::mbg_set_gps_ant_cable_len
 * @see ::mbg_get_gps_ant_cable_len
 */
_MBG_API_ATTR int _MBG_API mbg_set_and_check_gps_ant_cable_len( MBG_DEV_HANDLE dh,
                                                                 ANT_CABLE_LEN *p_new_val,
                                                                 const ANT_CABLE_LEN *p_org_val )
{
  ANT_CABLE_LEN re_read_val = 0;
  int rc;

  // Write the new value to the device.
  rc = mbg_set_gps_ant_cable_len( dh, p_new_val );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  // Read back the parameter.
  rc = mbg_get_gps_ant_cable_len( dh, &re_read_val );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  if ( re_read_val == *p_new_val )
  {
    // Saved successfully.
    rc = MBG_SUCCESS;
    goto out;
  }


  // If execution gets here, the re-read value doesn't match
  // the desired new value. Either the original value is unchanged,
  // or the device has stored a truncated value.

  // Only if the original value has been provided, we can also check
  // whether the re-read value matches the original value, or not.
  if ( p_org_val )
  {
    if ( re_read_val != *p_org_val )
    {
      // The re-read value doesn't match the original value, either,
      // so a truncated value has been saved by the device.
      rc = MBG_ERR_CFG_TRUNC;
      goto out_ret_re_read;
    }
  }

  // The new value has not been accepted.
  rc = MBG_ERR_CFG;

out_ret_re_read:
  // For convenience, we store the re-read value in *p_new_val,
  // so it can be made available to the caller.
  *p_new_val = re_read_val;

out:
  return rc;

}  // mbg_set_and_check_gps_ant_cable_len



/*HDR*/
/**
 * @brief Read the ::RECEIVER_INFO structure from a device.
 *
 * The API call ::mbg_chk_dev_has_receiver_info checks
 * whether this call is supported by a device.
 *
 * <b>Note:</b> Applications should call ::mbg_setup_receiver_info
 * preferably, which also sets up a basic ::RECEIVER_INFO structure
 * for devices which don't provide that structure by themselves.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::RECEIVER_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_setup_receiver_info
 * @see ::mbg_chk_dev_has_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_receiver_info( MBG_DEV_HANDLE dh, RECEIVER_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_RECEIVER_INFO,
                              IOCTL_GET_GPS_RECEIVER_INFO, p,
                              _pcps_ddev_has_receiver_info( dh ) );

  _mbg_swab_receiver_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_receiver_info



/*HDR*/
/**
 * @brief Read a ::STR_TYPE_INFO_IDX array of supported string types.
 *
 * A valid ::RECEIVER_INFO associated with the device
 * has to be passed to this function.
 *
 * <b>Note:</b> The function ::mbg_get_serial_settings should be used preferably
 * to get retrieve the current port settings and configuration options.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  stii  Pointer to a an array of string type information to be filled up.
 * @param[in]   p_ri  Pointer to the ::RECEIVER_INFO associated with the device. TODO Make this obsolete!
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 * @see ::mbg_get_gps_all_port_info
 * @see ::mbg_setup_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_str_type_info( MBG_DEV_HANDLE dh,
                                                          STR_TYPE_INFO_IDX stii[],
                                                          const RECEIVER_INFO *p_ri )
{
  MBGDEVIO_RET_VAL rc;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_STR_TYPE_INFO,
                            IOCTL_GET_GPS_ALL_STR_TYPE_INFO, stii,
                            p_ri->n_str_type * sizeof( stii[0] ),
                            _pcps_ddev_has_receiver_info( dh ) );
  #else
    // We check the model_code to see whether the receiver info
    // has been read from a device which really supports it, or
    // a dummy structure has been setup.
    if ( p_ri && ( p_ri->model_code != GPS_MODEL_UNKNOWN ) )
      rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_STR_TYPE_INFO, stii,
                                   p_ri->n_str_type * sizeof( stii[0] ) );
    else
      return MBG_ERR_NOT_SUPP_BY_DEV;
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < p_ri->n_str_type; i++ )
      {
        STR_TYPE_INFO_IDX *p = &stii[i];
        _mbg_swab_str_type_info_idx( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_str_type_info



/*HDR*/
/**
 * @brief Read a ::PORT_INFO_IDX array of supported serial port configurations.
 *
 * A valid ::RECEIVER_INFO associated with the device
 * has to be passed to this function.
 *
 * <b>Note:</b> The function ::mbg_get_serial_settings should be used preferably
 * to get retrieve the current port settings and configuration options.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  pii   Pointer to a an array of port configuration information to be filled up.
 * @param[in]   p_ri  Pointer to the ::RECEIVER_INFO associated with the device. TODO Make this obsolete!
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_serial_settings
 * @see ::mbg_get_gps_all_str_type_info
 * @see ::mbg_setup_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_port_info( MBG_DEV_HANDLE dh,
                                                      PORT_INFO_IDX pii[],
                                                      const RECEIVER_INFO *p_ri )
{
  MBGDEVIO_RET_VAL rc;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_PORT_INFO,
                            IOCTL_GET_GPS_ALL_PORT_INFO, pii,
                            p_ri->n_com_ports * sizeof( pii[0] ),
                            _pcps_ddev_has_receiver_info( dh ) );
  #else
    // We check the model_code to see whether the receiver info
    // has been read from a device which really supports it, or
    // a dummy structure has been set up.
    if ( p_ri && ( p_ri->model_code != GPS_MODEL_UNKNOWN ) )
      rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_PORT_INFO, pii,
                                   p_ri->n_com_ports * sizeof( pii[0] ) );
    else
      return MBG_ERR_NOT_SUPP_BY_DEV;
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < p_ri->n_com_ports; i++ )
      {
        PORT_INFO_IDX *p = &pii[i];
        _mbg_swab_port_info_idx( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_port_info



/*HDR*/
/**
 * @brief Write the configuration for a single serial port to a device.
 *
 * The ::PORT_SETTINGS_IDX structure contains both the ::PORT_SETTINGS
 * and the port index value. Except for the parameter types this call is
 * equivalent to ::mbg_set_gps_port_settings.
 *
 * The API call ::mbg_chk_dev_has_receiver_info checks
 * whether this call is supported by a device.
 *
 * <b>Note:</b> The function ::mbg_save_serial_settings should be used preferably
 * to write new port configuration to the device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::PORT_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 * @see ::mbg_set_gps_port_settings
 * @see ::mbg_chk_dev_has_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_port_settings_idx( MBG_DEV_HANDLE dh,
                                                          const PORT_SETTINGS_IDX *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    PORT_SETTINGS_IDX tmp = *p;
    _mbg_swab_port_settings_idx( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_PORT_SETTINGS_IDX,
                               IOCTL_SET_GPS_PORT_SETTINGS_IDX, p,
                               _pcps_ddev_has_receiver_info( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_port_settings_idx



/*HDR*/
/**
 * @brief Write the configuration for a single serial port to a device.
 *
 * The ::PORT_SETTINGS structure does not contain the port index, so the
 * the port index must be given separately. Except for the parameter types
 * this call is equivalent to ::mbg_set_gps_port_settings_idx.
 *
 * The API call ::mbg_chk_dev_has_receiver_info checks
 * whether this call is supported by a device.
 *
 * <b>Note:</b> The function ::mbg_save_serial_settings should be used preferably
 * to write new port configuration to the device.
 *
 * @param[in]  dh   Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p    Pointer to a ::PORT_SETTINGS structure to be written.
 * @param[in]  idx  Index of the serial port to be configured (starting from 0).
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_save_serial_settings
 * @see ::mbg_set_gps_port_settings_idx
 * @see ::mbg_chk_dev_has_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_port_settings( MBG_DEV_HANDLE dh,
                                                      const PORT_SETTINGS *p, int idx )
{
  PORT_SETTINGS_IDX psi = { 0 };

  psi.idx = idx;
  psi.port_settings = *p;
  _mbg_swab_port_settings_idx( &psi );

  return mbg_set_gps_port_settings_idx( dh, &psi );

}  // mbg_set_gps_port_settings



/*HDR*/
/**
 * @brief Set up a ::RECEIVER_INFO structure for a device.
 *
 * If the device supports the ::RECEIVER_INFO structure, the structure
 * is read from the device, otherwise a structure is set up using
 * default values depending on the device type.
 *
 * Optionally, the function ::mbg_get_device_info may have been called
 * before, and the returned ::PCPS_DEV structure can be passed to this
 * function.
 *
 * If a @a NULL pointer is passed instead, the device info is retrieved
 * directly from the device, using the device handle.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   p_obs  Obsolete pointer kept for compatibility, should be @a NULL.
 * @param[out]  p      Pointer to a ::RECEIVER_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_device_info
 * @see ::mbg_chk_dev_has_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_setup_receiver_info( MBG_DEV_HANDLE dh,
                                                    const void *p_obs,
                                                    RECEIVER_INFO *p )
{
  int rc;

  (void) p_obs;  // Avoid warning "never used".

  // If the device supports the receiver_info structure,
  // read it from the device, otherwise set up some default
  // values depending on the device type.
  if ( mbg_rc_is_success( mbg_chk_dev_has_receiver_info( dh ) ) )
  {
    rc = mbg_get_gps_receiver_info( dh, p );

    if ( mbg_rc_is_error( rc ) )
      return rc;

    goto check;
  }

  #if SUPP_SYN1588_USR_SPC
    rc = mbg_syn1588_setup_receiver_info( dh, p );

    if ( mbg_rc_is_success( rc ) )
      goto check;
  #endif

  {
    bool b = mbg_rc_is_success( mbg_chk_dev_is_gps( dh ) );
    PCPS_DEV dev = { { 0 } };

    mbg_get_device_info( dh, &dev );

    setup_default_receiver_info( p, &dev, b );
  }

check:
  return chk_dev_receiver_info( p );

}  // mbg_setup_receiver_info



/*HDR*/
/**
 * @brief Read the version code of the on-board PCI/PCIe interface ASIC.
 *
 * The API call ::mbg_chk_dev_has_asic_version checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCI_ASIC_VERSION type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_asic_version
 */
_MBG_API_ATTR int _MBG_API mbg_get_asic_version( MBG_DEV_HANDLE dh, PCI_ASIC_VERSION *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_PCI_ASIC_VERSION, p );
    return _mbgdevio_cnv_ret_val( rc );
  #else
    if ( !_pcps_ddev_has_asic_version( dh ) )
      return MBG_ERR_NOT_SUPP_BY_DEV;

    // TODO Should we care about memory mapped vs. port I/O access ?
    *p = _mbg_inp32_to_cpu( dh, 0, _pcps_ddev_io_base_mapped( dh, 0 )
             + offsetof( PCI_ASIC, raw_version ) );

    return MBG_SUCCESS;
  #endif

}  // mbg_get_asic_version



/*HDR*/
/**
 * @brief Read the features of the on-board PCI/PCIe interface ASIC.
 *
 * The API call ::mbg_chk_dev_has_asic_features checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCI_ASIC_FEATURES type to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_asic_features
 */
_MBG_API_ATTR int _MBG_API mbg_get_asic_features( MBG_DEV_HANDLE dh,
                                                  PCI_ASIC_FEATURES *p )
{

  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_PCI_ASIC_FEATURES, p );
    return _mbgdevio_cnv_ret_val( rc );
  #else
    if ( !_pcps_ddev_has_asic_features( dh ) )
    {
      *p = 0;
      return MBG_ERR_NOT_SUPP_BY_DEV;
    }

    // TODO Should we care about memory mapped vs. port I/O access ?
    *p = _mbg_inp32_to_cpu( dh, 0, _pcps_ddev_io_base_mapped( dh, 0 )
             + offsetof( PCI_ASIC, features ) );

    return MBG_SUCCESS;
  #endif

}  // mbg_get_asic_features



/*HDR*/
/**
 * @brief Read the current time scale settings and which time scales are supported.
 *
 * The ::MBG_TIME_SCALE_INFO structure tells which time scale settings are supported
 * by a device, and which time scale is currently configured.
 *
 * The API call ::mbg_chk_dev_has_time_scale checks whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_has_time_scale.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_TIME_SCALE_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_time_scale_settings
 * @see ::mbg_chk_dev_has_time_scale
 */
_MBG_API_ATTR int _MBG_API mbg_get_time_scale_info( MBG_DEV_HANDLE dh, MBG_TIME_SCALE_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_TIME_SCALE,
                              IOCTL_GET_GPS_TIME_SCALE_INFO, p,
                              _pcps_ddev_has_time_scale( dh ) );
  _mbg_swab_mbg_time_scale_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_time_scale_info



/*HDR*/
/**
 * @brief Write the time scale configuration to a device.
 *
 * The ::MBG_TIME_SCALE_SETTINGS structure determines which time scale
 * is to be used for the time stamps which can be read from a device.
 *
 * The API call ::mbg_chk_dev_has_time_scale checks whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_has_time_scale.
 *
 * The function ::mbg_get_time_scale_info should have been called before
 * in order to determine which time scales are supported by the card.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::MBG_TIME_SCALE_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_time_scale_info
 * @see ::mbg_chk_dev_has_time_scale
 */
_MBG_API_ATTR int _MBG_API mbg_set_time_scale_settings( MBG_DEV_HANDLE dh, const MBG_TIME_SCALE_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    MBG_TIME_SCALE_SETTINGS tmp = *p;
    _mbg_swab_mbg_time_scale_settings( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_TIME_SCALE,
                               IOCTL_SET_GPS_TIME_SCALE_SETTINGS, p,
                               _pcps_ddev_has_time_scale( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_time_scale_settings



/*HDR*/
/**
 * @brief Read a ::UTC parameter structure from a device.
 *
 * The API call ::mbg_chk_dev_has_utc_parm checks whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_has_utc_parm.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::UTC structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_utc_parm
 * @see ::mbg_set_utc_parm
 */
_MBG_API_ATTR int _MBG_API mbg_get_utc_parm( MBG_DEV_HANDLE dh, UTC *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_UTC,
                              IOCTL_GET_GPS_UTC_PARM, p,
                              _pcps_ddev_has_utc_parm( dh ) );
  _mbg_swab_utc_parm( p );
  swap_double( &p->A0 );
  swap_double( &p->A1 );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_utc_parm



/*HDR*/
/**
 * @brief Write a ::UTC parameter structure to a device.
 *
 * This should only be done for testing, or if a device is operated
 * in always freewheeling mode. If the receiver is tracking any satellites,
 * the settings written to the device are overwritten by the parameters
 * broadcast by the satellites.
 *
 * The API call ::mbg_chk_dev_has_utc_parm checks whether
 * this call is supported by a device.
 *
 * See also the notes for mbg_chk_dev_has_utc_parm.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a valid ::UTC structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_utc_parm
 * @see ::mbg_get_utc_parm
 */
_MBG_API_ATTR int _MBG_API mbg_set_utc_parm( MBG_DEV_HANDLE dh, const UTC *p )
{
  MBGDEVIO_RET_VAL rc;

  // The original parameters need to be modified anyway, so always use a copy.
  UTC tmp = *p;

  #if defined( MBG_ARCH_BIG_ENDIAN )
    _mbg_swab_utc_parm( &tmp );
  #endif

  swap_double( &tmp.A0 );
  swap_double( &tmp.A1 );

  _mbgdevio_write_gps_var_chk( dh, PC_GPS_UTC,
                               IOCTL_SET_GPS_UTC_PARM, &tmp,
                               _pcps_ddev_has_utc_parm( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_utc_parm



/*HDR*/
/**
 * @brief Read the current time plus the associated PC cycles from a device.
 *
 * The ::PCPS_TIME_CYCLES structure contains a ::PCPS_TIME structure
 * and a PC cycles counter value which can be used to compensate the latency
 * of the call, i.e. the program execution time until the time stamp has actually
 * been read from the device.
 *
 * This call is supported for any card, similar to ::mbg_get_time. However,
 * the ::mbg_get_hr_time_cycles call should be used preferably if supported by
 * the device since that call provides much better accuracy than this one.
 *
 * The cycles counter value corresponds to a value returned by @a QueryPerformanceCounter
 * on Windows, and @a get_cycles on Linux. On operating systems or targets which don't
 * provide a cycles counter the returned cycles value is always 0.
 *
 * Applications should first pick up their own cycles counter value and then call
 * this function. The difference of the cycles counter values corresponds to the
 * latency of the call in units of the cycles counter clock frequency, e.g. as reported
 * by @a QueryPerformanceFrequency on Windows.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TIME_CYCLES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_legacy_time_fncs
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_time
 */
_MBG_API_ATTR int _MBG_API mbg_get_time_cycles( MBG_DEV_HANDLE dh, PCPS_TIME_CYCLES *p )
{
  MBGDEVIO_RET_VAL rc;

  #if SUPP_SYN1588_USR_SPC
    if ( chk_if_syn1588_type( dh ) )
    {
      rc = mbg_syn1588_get_time_cycles( dh, p );
      return _mbgdevio_cnv_ret_val( rc );
    }
  #endif

  rc = _mbgdevio_read_var( dh, PCPS_GIVE_TIME, IOCTL_GET_PCPS_TIME_CYCLES, p );
  // No endianess conversion required.
  #if !defined( _MBGIOCTL_H )
    // Only if not using IOCTLs for PCPS_TIME,
    // read stamp AFTER the call.
    p->cycles = 0;  // TODO
  #endif
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_time_cycles



/*HDR*/
/**
 * @brief Read the current high resolution time plus the associated PC cycles from a device.
 *
 * The returned ::PCPS_HR_TIME_CYCLES structure contains a ::PCPS_HR_TIME
 * structure and a PC cycles counter value which can be used to compensate
 * the latency of the call, i.e. the program execution time until the time stamp
 * has actually been read from the device.
 *
 * The API call ::mbg_chk_dev_has_hr_time checks whether
 * this call is supported by the device.
 *
 * For details see @ref ::mbgdevio_hr_time_fncs
 *
 * The cycles counter value corresponds to a value returned by @a QueryPerformanceCounter
 * on Windows, and @a get_cycles on Linux. On operating systems or targets which don't
 * provide a cycles counter the returned cycles value is always 0.
 *
 * Applications should first pick up their own cycles counter value and then call
 * this function. The difference of the cycles counter values corresponds to the
 * latency of the call in units of the cycles counter clock frequency, e.g.task as reported
 * by @a QueryPerformanceFrequency on Windows.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::PCPS_HR_TIME_CYCLES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_hr_time_fncs
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_fast_timestamp_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_get_hr_time_cycles( MBG_DEV_HANDLE dh,
                                                   PCPS_HR_TIME_CYCLES *p )
{
  MBGDEVIO_RET_VAL rc;

  #if !defined( _MBGIOCTL_H )
    // only if not using IOCTLs
    // for PCPS_HR_TIME, read stamp BEFORE the call
    p->cycles = 0;  //##++
  #endif

  #if SUPP_SYN1588_USR_SPC
    if ( chk_if_syn1588_type( dh ) )
    {
      rc = mbg_syn1588_get_hr_time_cycles( dh, p );
      return _mbgdevio_cnv_ret_val( rc );
    }
  #endif

  _mbgdevio_read_var_chk( dh, PCPS_GIVE_HR_TIME,
                          IOCTL_GET_PCPS_HR_TIME_CYCLES,
                          p, _pcps_ddev_has_hr_time( dh ) );
  _mbg_swab_pcps_hr_time_cycles( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_hr_time_cycles



/*HDR*/
/**
 * @brief Read the current high resolution time, and compensate the latency of the call.
 *
 * Read a ::PCPS_HR_TIME structure plus cycles counter value, and correct the
 * time stamp for the latency of the call as described for ::mbg_get_hr_time_cycles,
 * then return the compensated time stamp, and optionally the latency.
 *
 * The API call ::mbg_chk_dev_has_hr_time checks whether
 * this call is supported by the device.
 *
 * For details see @ref ::mbgdevio_hr_time_fncs.
 *
 * The cycles counter value corresponds to a value returned by @a QueryPerformanceCounter
 * on Windows, and @a get_cycles on Linux. On operating systems or targets which don't
 * provide a cycles counter the returned cycles value is always 0.
 *
 * Applications should first pick up their own cycles counter value and then call
 * this function. The difference of the cycles counter values corresponds to the
 * latency of the call in units of the cycles counter clock frequency, e.g. as reported
 * by @a QueryPerformanceFrequency on Windows.
 *
 * @param[in]  dh           Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p            Pointer to a ::PCPS_HR_TIME structure to be filled up.
 * @param[out] hns_latency  Optional pointer to an @a int32_t value to return
 *                          the latency in 100ns units, or @a NULL, if not used.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_hr_time_fncs
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_hr_time
 * @see ::mbg_get_hr_time_cycles
 * @see ::mbg_get_hr_time_comp
 * @see @ref mbgdevio_hr_time_fncs
 * @see @ref mbgdevio_fast_timestamp_fncs
 * @see @ref mbgdevio_legacy_time_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_get_hr_time_comp( MBG_DEV_HANDLE dh, PCPS_HR_TIME *p,
                                                 int32_t *hns_latency )
{
  PCPS_HR_TIME_CYCLES htc;
  MBG_PC_CYCLES cyc_now;
  int rc;

  // First get current time stamp counter value, then read
  // a high resolution time stamp from the device, plus the
  // associated time stamp counter value.
  mbg_get_pc_cycles( &cyc_now );

  rc = mbg_get_hr_time_cycles( dh, &htc );

  if ( mbg_rc_is_success( rc ) )
  {
    mbg_init_pc_cycles_frequency( dh );
    rc = mbg_comp_hr_latency( &htc.t.tstamp, &htc.cycles, &cyc_now, &mbg_pc_cycles_frequency, hns_latency );
    *p = htc.t;
  }

  return rc;

}  // mbg_get_hr_time_comp



/*HDR*/
/**
 * @brief Read the current IRIG output settings plus the supported settings.
 *
 * The returned ::IRIG_INFO structure contains the configuration of an IRIG output
 * plus the possible settings supported by that output.
 *
 * The API call ::mbg_chk_dev_has_irig_tx checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to an ::IRIG_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_irig_tx_settings
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
_MBG_API_ATTR int _MBG_API mbg_get_irig_tx_info( MBG_DEV_HANDLE dh, IRIG_INFO *p )
{
  MBGDEVIO_RET_VAL rc;

  #if !defined( _MBGIOCTL_H )
    // This is a workaround for GPS169PCIs with early
    // firmware versions. See RCS log for details.
    uint8_t pcps_cmd = PCPS_GET_IRIG_TX_INFO;

    if ( _pcps_ddev_requires_irig_workaround( dh ) )
      pcps_cmd = PCPS_GET_IRIG_RX_INFO;

    #define _PCPS_CMD   pcps_cmd
  #else
    #define _PCPS_CMD   PCPS_GET_IRIG_TX_INFO
  #endif

  _mbgdevio_read_var_chk( dh, _PCPS_CMD, IOCTL_GET_PCPS_IRIG_TX_INFO,
                          p, _pcps_ddev_has_irig_tx( dh ) );
  #undef _PCPS_CMD

  _mbg_swab_irig_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_irig_tx_info



/*HDR*/
/**
 * @brief Write an ::IRIG_SETTINGS structure to a device to configure the IRIG output.
 *
 * The API call ::mbg_chk_dev_has_irig_tx checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to an ::IRIG_INFO structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_irig_tx_info
 * @see ::mbg_chk_dev_has_irig_tx
 * @see ::mbg_chk_dev_is_tcr
 * @see ::mbg_chk_dev_has_irig
 * @see @ref group_icode
 */
_MBG_API_ATTR int _MBG_API mbg_set_irig_tx_settings( MBG_DEV_HANDLE dh, const IRIG_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if !defined( _MBGIOCTL_H )
    uint8_t pcps_cmd;
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    IRIG_SETTINGS tmp = *p;
    _mbg_swab_irig_settings( &tmp );
    p = &tmp;
  #endif

  #if !defined( _MBGIOCTL_H )
    // This is a workaround for GPS169PCIs with early
    // firmware versions. See RCS log for details.
    pcps_cmd = PCPS_SET_IRIG_TX_SETTINGS;

    if ( _pcps_ddev_requires_irig_workaround( dh ) )
      pcps_cmd = PCPS_SET_IRIG_RX_SETTINGS;

    #define _PCPS_CMD   pcps_cmd
  #else
    #define _PCPS_CMD   PCPS_SET_IRIG_TX_SETTINGS
  #endif

  _mbgdevio_write_var_chk( dh, _PCPS_CMD, IOCTL_SET_PCPS_IRIG_TX_SETTINGS,
                           p, _pcps_ddev_has_irig_tx( dh ) );
  #undef _PCPS_CMD

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_irig_tx_settings



/*HDR*/
/**
 * @brief Read the current frequency synthesizer settings from a device.
 *
 * Read a ::SYNTH structure containing the configuration of an optional
 * on-board programmable frequency synthesizer.
 *
 * The API call ::mbg_chk_dev_has_synth checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::SYNTH structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_synth
 * @see ::mbg_set_synth
 * @see ::mbg_get_synth_state
 * @see @ref group_synth
 */
_MBG_API_ATTR int _MBG_API mbg_get_synth( MBG_DEV_HANDLE dh, SYNTH *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_SYNTH, IOCTL_GET_SYNTH,
                          p, _pcps_ddev_has_synth( dh ) );
  _mbg_swab_synth( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_synth



/*HDR*/
/**
 * @brief Write frequency synthesizer configuration settings to a device.
 *
 * Write a ::SYNTH structure containing the configuration of an optional
 * on-board programmable frequency synthesizer.
 *
 * The API call ::mbg_chk_dev_has_synth checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::SYNTH structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_synth
 * @see ::mbg_get_synth
 * @see ::mbg_get_synth_state
 * @see @ref group_synth
 */
_MBG_API_ATTR int _MBG_API mbg_set_synth( MBG_DEV_HANDLE dh, const SYNTH *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    SYNTH tmp = *p;
    _mbg_swab_synth( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_var_chk( dh, PCPS_SET_SYNTH, IOCTL_SET_SYNTH,
                           p, _pcps_ddev_has_synth( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_synth



/*HDR*/
/**
 * @brief Read the current status of the on-board frequency synthesizer.
 *
 * The API call ::mbg_chk_dev_has_synth checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::SYNTH_STATE structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_synth
 * @see ::mbg_get_synth
 * @see ::mbg_set_synth
 * @see @ref group_synth
 */
_MBG_API_ATTR int _MBG_API mbg_get_synth_state( MBG_DEV_HANDLE dh, SYNTH_STATE *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_SYNTH_STATE, IOCTL_GET_SYNTH_STATE,
                          p, _pcps_ddev_has_synth( dh ) );
  _mbg_swab_synth_state( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_synth_state



/*HDR*/
/**
 * @brief Read a high resolution ::PCPS_TIME_STAMP_CYCLES structure via memory mapped access.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TIME_STAMP_CYCLES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_fast_timestamp_fncs
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp_comp
 * @see ::mbg_get_fast_hr_timestamp
 * @see @ref mbgdevio_fast_timestamp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_get_fast_hr_timestamp_cycles( MBG_DEV_HANDLE dh,
                                                             PCPS_TIME_STAMP_CYCLES *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;

    #if SUPP_SYN1588_USR_SPC
      if ( chk_if_syn1588_type( dh ) )
      {
        rc = mbg_syn1588_get_fast_hr_timestamp_cycles( dh, p );
        return _mbgdevio_cnv_ret_val( rc );
      }
    #endif

    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_FAST_HR_TIMESTAMP_CYCLES, p );
    // native endianess, no need to swap bytes
    return _mbgdevio_cnv_ret_val( rc );
  #else
    // This is currently not supported by the target environment.
    return MBG_ERR_NOT_SUPP_ON_OS;
  #endif

}  // mbg_get_fast_hr_timestamp_cycles



/*HDR*/
/**
 * @brief Read a high resolution timestamp and compensate the latency of the call.
 *
 * The retrieved ::PCPS_TIME_STAMP is read from memory mapped registers,
 * before it is returned, it is compensated for the latency of the call.
 *
 * @param[in]   dh           Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p            Pointer to a ::PCPS_TIME_STAMP structure to be filled up.
 * @param[out]  hns_latency  Optionally receives the latency in hectonanoseconds. TODO Check if hns.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_fast_timestamp_fncs
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp_cycles
 * @see ::mbg_get_fast_hr_timestamp
 * @see @ref mbgdevio_fast_timestamp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_get_fast_hr_timestamp_comp( MBG_DEV_HANDLE dh,
                                                           PCPS_TIME_STAMP *p,
                                                           int32_t *hns_latency )
{
  PCPS_TIME_STAMP_CYCLES tc;
  MBG_PC_CYCLES cyc_now;
  int rc;

  // First get current time stamp counter value, then read
  // a high resolution time stamp from the device, plus the
  // associated time stamp counter value.
  mbg_get_pc_cycles( &cyc_now );

  rc = mbg_get_fast_hr_timestamp_cycles( dh, &tc );

  if ( mbg_rc_is_success( rc ) )
  {
    mbg_init_pc_cycles_frequency( dh );
    rc = mbg_comp_hr_latency( &tc.tstamp, &tc.cycles, &cyc_now, &mbg_pc_cycles_frequency, hns_latency );
    *p = tc.tstamp;
  }

  return rc;

}  // mbg_get_fast_hr_timestamp_comp



/*HDR*/
/**
 * @brief Read a high resolution ::PCPS_TIME_STAMP structure via memory mapped access.
 *
 * This function does not return or evaluate a cycles count, so the latency
 * of the call can not be determined. However, depending on the timer hardware
 * used as cycles counter it may take quite some time to read the cycles count
 * on some hardware architectures, so this call can be used to yield lower
 * latencies, under the restriction to be unable to determine the exact latency.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_TIME_STAMP structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbgdevio_fast_timestamp_fncs
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_fast_hr_timestamp_comp
 * @see ::mbg_get_fast_hr_timestamp_cycles
 * @see @ref mbgdevio_fast_timestamp_fncs
 */
_MBG_API_ATTR int _MBG_API mbg_get_fast_hr_timestamp( MBG_DEV_HANDLE dh,
                                                      PCPS_TIME_STAMP *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;

    #if SUPP_SYN1588_USR_SPC
      if ( chk_if_syn1588_type( dh ) )
      {
        rc = mbg_syn1588_get_fast_hr_timestamp( dh, p );
        return _mbgdevio_cnv_ret_val( rc );
      }
    #endif

    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_FAST_HR_TIMESTAMP, p );
    // native endianess, no need to swap bytes
    return _mbgdevio_cnv_ret_val( rc );
  #else
    // This is currently not supported by the target environment.
    return MBG_ERR_NOT_SUPP_ON_OS;
  #endif

}  // mbg_get_fast_hr_timestamp



/*HDR*/
/**
 * @brief Read current configuraton and features provided by the programmable pulse outputs.
 *
 * Read a ::POUT_INFO_IDX array of current settings and configuration
 * options of the programmable pulse outputs of a device.
 *
 * A valid ::RECEIVER_INFO associated with the device
 * has to be passed to this function.
 *
 * The function should only be called if the ::RECEIVER_INFO::n_prg_out
 * field (i.e. the number of programmable outputs of the device) is not 0.
 *
 * The array passed to this function to receive the returned data
 * must be able to hold at least ::RECEIVER_INFO::n_prg_out elements.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  pii   Pointer to a an array of ::POUT_INFO_IDX structures to be filled up.
 * @param[in]   p_ri  Pointer to the ::RECEIVER_INFO associated with the device. TODO Make this obsolete!
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_pout_settings_idx
 * @see ::mbg_set_gps_pout_settings
 * @see ::mbg_setup_receiver_info
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_pout_info( MBG_DEV_HANDLE dh,
                                        POUT_INFO_IDX pii[],
                                        const RECEIVER_INFO *p_ri )
{
  MBGDEVIO_RET_VAL rc;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_POUT_INFO,
                            IOCTL_GET_GPS_ALL_POUT_INFO, pii,
                            p_ri->n_prg_out * sizeof( pii[0] ),
                            _pcps_ddev_has_receiver_info( dh ) );
  #else
    // We check the model_code to see whether the receiver info
    // has been read from a device which really supports it, or
    // a dummy structure has been setup.
    if ( p_ri && ( p_ri->model_code != GPS_MODEL_UNKNOWN ) )
      rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_POUT_INFO, pii,
                                   p_ri->n_prg_out * sizeof( pii[0] ) );
    else
      return MBG_ERR_NOT_SUPP_BY_DEV;
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < p_ri->n_prg_out; i++ )
      {
        POUT_INFO_IDX *p = &pii[i];
        _mbg_swab_pout_info_idx_on_get( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_pout_info



/*HDR*/
/**
 * @brief Write the configuration for a single programmable pulse output.
 *
 * The ::POUT_SETTINGS_IDX structure contains both the ::POUT_SETTINGS
 * and the output index value. Except for the parameter types this call
 * is equivalent to ::mbg_set_gps_pout_settings.
 *
 * The function should only be called if the ::RECEIVER_INFO::n_prg_out field
 * (i.e. the number of programmable outputs of the device) is not 0, and the
 * output index value must be in the range 0..::RECEIVER_INFO::n_prg_out.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::POUT_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_all_pout_info
 * @see ::mbg_set_gps_pout_settings
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_pout_settings_idx( MBG_DEV_HANDLE dh,
                                                          const POUT_SETTINGS_IDX *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    POUT_SETTINGS_IDX tmp = *p;
    _mbg_swab_pout_settings_idx_on_set( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_POUT_SETTINGS_IDX,
                               IOCTL_SET_GPS_POUT_SETTINGS_IDX, p,
                               _pcps_ddev_has_receiver_info( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_pout_settings_idx



/*HDR*/
/**
 * @brief Write the configuration for a single programmable pulse output.
 *
 * The ::POUT_SETTINGS structure does not contain the index of the
 * programmable output to be configured, so the index must explicitly
 * be passed to this function. Except for the parameter types this call
 * is equivalent to ::mbg_set_gps_pout_settings_idx.
 *
 * The function should only be called if the ::RECEIVER_INFO::n_prg_out field
 * (i.e. the number of programmable outputs of the device) is not 0, and the
 * output index value must be in the range 0..::RECEIVER_INFO::n_prg_out.
 *
 * @param[in]  dh   Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p    Pointer to a ::POUT_SETTINGS structure to be written.
 * @param[in]  idx  Index of the programmable pulse output to be configured (starting from 0 ).
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_all_pout_info
 * @see ::mbg_set_gps_pout_settings_idx
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_pout_settings( MBG_DEV_HANDLE dh,
                                                      const POUT_SETTINGS *p, int idx )
{
  POUT_SETTINGS_IDX psi = { 0 };

  psi.idx = idx;
  psi.pout_settings = *p;

  return mbg_set_gps_pout_settings_idx( dh, &psi );

}  // mbg_set_gps_pout_settings



/*HDR*/
/**
 * @brief Read the IRQ status of a device.
 *
 * IRQ status information includes flags indicating whether IRQs are
 * actually enabled, and whether IRQ support by a device is possibly
 * unsafe due to the firmware and interface chip version.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PCPS_IRQ_STAT_INFO variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see @ref PCPS_IRQ_STAT_INFO_DEFS
 */
_MBG_API_ATTR int _MBG_API mbg_get_irq_stat_info( MBG_DEV_HANDLE dh, PCPS_IRQ_STAT_INFO *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_IRQ_STAT_INFO, p );
    // Native endianess, no need to swap bytes.
    return _mbgdevio_cnv_ret_val( rc );
  #else
    *p = dh->irq_stat_info;
    return MBG_SUCCESS;
  #endif

}  // mbg_get_irq_stat_info



/*HDR*/
/**
 * @brief Read LAN interface information from a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::LAN_IF_INFO variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_ip4_state
 * @see ::mbg_get_ip4_settings
 * @see ::mbg_set_ip4_settings
 */
_MBG_API_ATTR int _MBG_API mbg_get_lan_if_info( MBG_DEV_HANDLE dh, LAN_IF_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_LAN_IF_INFO,
                              IOCTL_GET_LAN_IF_INFO, p,
                              _pcps_ddev_has_lan_intf( dh ) );
  _mbg_swab_lan_if_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_lan_if_info



/*HDR*/
/**
 * @brief Read LAN IPv4 state from a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::IP4_SETTINGS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_settings
 * @see ::mbg_set_ip4_settings
 */
_MBG_API_ATTR int _MBG_API mbg_get_ip4_state( MBG_DEV_HANDLE dh, IP4_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_IP4_STATE,
                              IOCTL_GET_IP4_STATE, p,
                              _pcps_ddev_has_lan_intf( dh ) );
  _mbg_swab_ip4_settings( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ip4_state



/*HDR*/
/**
 * @brief Read LAN IPv4 settings from a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::IP4_SETTINGS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_state
 * @see ::mbg_set_ip4_settings
 */
_MBG_API_ATTR int _MBG_API mbg_get_ip4_settings( MBG_DEV_HANDLE dh, IP4_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_IP4_SETTINGS,
                              IOCTL_GET_IP4_SETTINGS, p,
                              _pcps_ddev_has_lan_intf( dh ) );
  _mbg_swab_ip4_settings( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ip4_settings



/*HDR*/
/**
 * @brief Write LAN IPv4 settings to a device.
 *
 * The API call ::mbg_chk_dev_has_lan_intf checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   ::IP4_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_lan_intf
 * @see ::mbg_get_lan_if_info
 * @see ::mbg_get_ip4_settings
 */
_MBG_API_ATTR int _MBG_API mbg_set_ip4_settings( MBG_DEV_HANDLE dh,
                                                 const IP4_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    IP4_SETTINGS tmp = *p;
    _mbg_swab_ip4_settings( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_IP4_SETTINGS,
                               IOCTL_SET_IP4_SETTINGS, p,
                               _pcps_ddev_has_lan_intf( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_ip4_settings



/*HDR*/
/**
 * @brief Read PTP/IEEE1588 status from a device.
 *
 * The API call ::mbg_chk_dev_has_ptp checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PTP_STATE variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
_MBG_API_ATTR int _MBG_API mbg_get_ptp_state( MBG_DEV_HANDLE dh, PTP_STATE *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_PTP_STATE,
                              IOCTL_GET_PTP_STATE, p,
                              _pcps_ddev_has_ptp( dh ) );
  _mbg_swab_ptp_state( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ptp_state



/*HDR*/
/**
 * @brief Read PTP/IEEE1588 config info and current settings from a device.
 *
 * The API call ::mbg_chk_dev_has_ptp checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PTP_CFG_INFO variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_state
 * @see ::mbg_set_ptp_cfg_settings
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
_MBG_API_ATTR int _MBG_API mbg_get_ptp_cfg_info( MBG_DEV_HANDLE dh, PTP_CFG_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_PTP_CFG,
                              IOCTL_GET_PTP_CFG_INFO, p,
                              _pcps_ddev_has_ptp( dh ) );
  _mbg_swab_ptp_cfg_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ptp_cfg_info



/*HDR*/
/**
 * @brief Write PTP/IEEE1588 configuration settings to a device.
 *
 * The API call ::mbg_chk_dev_has_ptp checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   ::PTP_CFG_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_state
 * @see ::mbg_get_ptp_cfg_info
 * @see ::mbg_chk_dev_has_ptp_unicast
 */
_MBG_API_ATTR int _MBG_API mbg_set_ptp_cfg_settings( MBG_DEV_HANDLE dh,
                                                     const PTP_CFG_SETTINGS *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    PTP_CFG_SETTINGS tmp = *p;
    _mbg_swab_ptp_cfg_settings( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_PTP_CFG,
                               IOCTL_SET_PTP_CFG_SETTINGS, p,
                               _pcps_ddev_has_ptp( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_ptp_cfg_settings



/*HDR*/
/**
 * @brief Read PTP/IEEE1588 unicast master configuration limits from a device.
 *
 * The API call ::mbg_chk_dev_has_ptp_unicast checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::PTP_UC_MASTER_CFG_LIMITS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp_unicast
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_get_ptp_state
 */
_MBG_API_ATTR int _MBG_API mbg_get_ptp_uc_master_cfg_limits( MBG_DEV_HANDLE dh, PTP_UC_MASTER_CFG_LIMITS *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_PTP_UC_MASTER_CFG_LIMITS,
                              IOCTL_PTP_UC_MASTER_CFG_LIMITS, p,
                              _pcps_has_ri_ptp_unicast( _ri_addr( dh ) ) );

  _mbg_swab_ptp_uc_master_cfg_limits( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_ptp_uc_master_cfg_limits



/*HDR*/
/**
 * @brief Read PTP Unicast master settings and configuration options.
 *
 * The array passed to this function to receive the returned data
 * must be able to hold at least ::PTP_UC_MASTER_CFG_LIMITS::n_supp_master elements.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  pii     Pointer to a an array of ::PTP_UC_MASTER_INFO_IDX structures to be filled up.
 * @param[in]   p_umsl  Pointer to a ::PTP_UC_MASTER_CFG_LIMITS structure returned by ::mbg_get_ptp_uc_master_cfg_limits.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp_unicast
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_set_ptp_uc_master_settings_idx
 * @see ::mbg_get_ptp_state
 */
_MBG_API_ATTR int _MBG_API mbg_get_all_ptp_uc_master_info( MBG_DEV_HANDLE dh,
                                                           PTP_UC_MASTER_INFO_IDX pii[],
                                                           const PTP_UC_MASTER_CFG_LIMITS *p_umsl )
{
  MBGDEVIO_RET_VAL rc;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_PTP_UC_MASTER_INFO,
                            IOCTL_GET_ALL_PTP_UC_MASTER_INFO, pii,
                            p_umsl->n_supp_master * sizeof( pii[0] ),
                            _pcps_ddev_has_ptp_unicast( dh ) );
  #else
    if ( p_umsl && p_umsl->n_supp_master )
      rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_PTP_UC_MASTER_INFO, pii,
                                             p_umsl->n_supp_master * sizeof( pii[0] ) );
    else
      return MBG_ERR_NOT_SUPP_BY_DEV;
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < p_umsl->n_supp_master; i++ )
      {
        PTP_UC_MASTER_INFO_IDX *p = &pii[i];
        _mbg_swab_ptp_uc_master_info_idx( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_all_ptp_uc_master_info



/*HDR*/
/**
 * @brief Write PTP/IEEE1588 unicast configuration settings to a device.
 *
 * The API call ::mbg_chk_dev_has_ptp_unicast checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   ::PTP_UC_MASTER_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_ptp_unicast
 * @see ::mbg_get_all_ptp_cfg_info
 * @see ::mbg_get_ptp_uc_master_cfg_limits
 * @see ::mbg_get_all_ptp_uc_master_info
 * @see ::mbg_get_ptp_state
 */
_MBG_API_ATTR int _MBG_API mbg_set_ptp_uc_master_settings_idx( MBG_DEV_HANDLE dh,
                                                     const PTP_UC_MASTER_SETTINGS_IDX *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    PTP_UC_MASTER_SETTINGS_IDX tmp = *p;
    _mbg_swab_ptp_uc_master_settings_idx( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_PTP_UC_MASTER_SETTINGS_IDX,
                               IOCTL_SET_PTP_UC_MASTER_SETTINGS_IDX, p,
                               _pcps_ddev_has_ptp_unicast( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_ptp_uc_master_settings_idx



/*HDR*/
/**
 * @brief Read both system time and associated device time from the kernel driver.
 *
 * The kernel driver reads the current system time plus a HR time structure
 * from a device immediately after each other. The returned info structure also
 * contains some cycles counts to be able to determine the execution times
 * required to read those time stamps.
 *
 * The advantage of this call compared to ::mbg_get_time_info_tstamp is
 * that this call also returns the status of the device. On the other hand, reading
 * the HR time from the device may block e.g. if another application accesses
 * the device.
 *
 * This call makes a ::mbg_get_hr_time_cycles call internally so the API call
 * ::mbg_chk_dev_has_hr_time can be used to check whether this call is supported
 * by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out] p   Pointer to a ::MBG_TIME_INFO_HRT structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_hr_time
 * @see ::mbg_get_time_info_tstamp
 */
_MBG_API_ATTR int _MBG_API mbg_get_time_info_hrt( MBG_DEV_HANDLE dh, MBG_TIME_INFO_HRT *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;

    #if SUPP_SYN1588_USR_SPC
      if ( chk_if_syn1588_type( dh ) )
      {
        rc = mbg_syn1588_get_time_info_hrt( dh, p );
        return _mbgdevio_cnv_ret_val( rc );
      }
    #endif

    _mbgdevio_read_var_chk( dh, -1, IOCTL_GET_TIME_INFO_HRT, p,
                            _pcps_ddev_has_hr_time( dh ) );
    _mbg_swab_mbg_time_info_hrt( p );
    return _mbgdevio_cnv_ret_val( rc );
  #else
    return MBG_ERR_NOT_SUPP_ON_OS;
  #endif

}  // mbg_get_time_info_hrt



/*HDR*/
/**
 * @brief Read both system time and associated device timestamp from the kernel driver.
 *
 * This call is similar to ::mbg_get_time_info_hrt except that a
 * ::mbg_get_fast_hr_timestamp_cycles call is made internally, so
 * the API call ::mbg_chk_dev_has_fast_hr_timestamp can be used to check whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_TIME_INFO_TSTAMP structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_fast_hr_timestamp
 * @see ::mbg_get_time_info_hrt
 */
_MBG_API_ATTR int _MBG_API mbg_get_time_info_tstamp( MBG_DEV_HANDLE dh, MBG_TIME_INFO_TSTAMP *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;

    #if SUPP_SYN1588_USR_SPC
      if ( chk_if_syn1588_type( dh ) )
      {
        rc = mbg_syn1588_get_time_info_tstamp( dh, p );
        return _mbgdevio_cnv_ret_val( rc );
      }
    #endif

    _mbgdevio_read_var_chk( dh, -1, IOCTL_GET_TIME_INFO_TSTAMP, p,
                            _pcps_ddev_has_fast_hr_timestamp( dh ) );
    _mbg_swab_mbg_time_info_tstamp( p );
    return _mbgdevio_cnv_ret_val( rc );
  #else
    return MBG_ERR_NOT_SUPP_ON_OS;
  #endif

}  // mbg_get_time_info_tstamp



/*HDR*/
/**
 * @brief Read PZF correlation info from a device.
 *
 * The API call ::mbg_chk_dev_has_corr_info checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::CORR_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_chk_dev_has_corr_info
 */
_MBG_API_ATTR int _MBG_API mbg_get_corr_info( MBG_DEV_HANDLE dh, CORR_INFO *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_CORR_INFO,
                              IOCTL_GET_CORR_INFO, p,
                              _pcps_ddev_has_corr_info( dh ) );
  _mbg_swab_corr_info( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_corr_info



/*HDR*/
/**
 * @brief Read configurable "distance from transmitter" parameter from a device.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, mostly with long wave receivers.
 *
 * The API call ::mbg_chk_dev_has_tr_distance checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::TR_DISTANCE variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_pzf
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_set_tr_distance
 */
_MBG_API_ATTR int _MBG_API mbg_get_tr_distance( MBG_DEV_HANDLE dh, TR_DISTANCE *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_TR_DISTANCE,
                              IOCTL_GET_TR_DISTANCE, p,
                              _pcps_ddev_has_tr_distance( dh ) );
  _mbg_swab_tr_distance( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_tr_distance



/*HDR*/
/**
 * @brief Write configurable "distance from transmitter" parameter to a device.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, mostly with long wave receivers.
 *
 * The API call ::mbg_chk_dev_has_tr_distance checks whether
 * this call is supported by a device.
 *
 * @note Unfortunately, the maximum value accepted by a device
 * may vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value.
 * The function ::mbg_set_and_check_tr_distance checks whether
 * the new value was accepted by the device, and therefore
 * should be used preferably.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]  p   Pointer to a ::TR_DISTANCE variable to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_get_tr_distance
 * @see ::mbg_set_and_check_tr_distance
 */
_MBG_API_ATTR int _MBG_API mbg_set_tr_distance( MBG_DEV_HANDLE dh, const TR_DISTANCE *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    TR_DISTANCE tmp = *p;
    _mbg_swab_tr_distance( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_var_chk( dh, PCPS_SET_TR_DISTANCE, IOCTL_SET_TR_DISTANCE,
                           p, _pcps_ddev_has_tr_distance( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_tr_distance



/*HDR*/
/**
 * @brief Write and check the configurable "distance from transmitter" parameter.
 *
 * The distance from transmitter parameter is used to compensate
 * the RF propagation delay, usually in long wave receivers.
 *
 * The API call ::mbg_chk_dev_has_tr_distance checks whether
 * this call is supported by a device.
 *
 * Unfortunately, the maximum value accepted by a device may
 * vary depending on the device type and/or firmware version,
 * and likewise the device may behave differently when attempting
 * to configure a value that exceeds this maximum value:
 *
 * - Some devices truncate the new value to the maximum value that
 *   is hard-coded in the firmware, so that the effective value
 *   corresponds to the maximum value, but neither matches the
 *   original value, nor the desired new value. This case can only
 *   be determined if the optional parameter @p p_org_val is provided.
 *
 * - Some devices simply don't accept and store a value that is
 *   out of range, so the original value is left unchanged.
 *
 * @param[in]      dh         Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in,out]  p_new_val  Pointer to a ::TR_DISTANCE variable to be written.
 * @param[in]      p_org_val  Optional pointer to a ::TR_DISTANCE variable with
 *                            the original value read previously, may be @a NULL.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *         If the return code is ::MBG_ERR_CFG or ::MBG_ERR_CFG_TRUNC,
 *         the re-read value is stored in @p p_new_val for convenience.
 *
 * @see ::mbg_chk_dev_has_tr_distance
 * @see ::mbg_set_tr_distance
 * @see ::mbg_get_tr_distance
 */
_MBG_API_ATTR int _MBG_API mbg_set_and_check_tr_distance( MBG_DEV_HANDLE dh,
                                                          TR_DISTANCE *p_new_val,
                                                          const TR_DISTANCE *p_org_val )
{
  TR_DISTANCE re_read_val = 0;
  int rc;

  // Write the new value to the device.
  rc = mbg_set_tr_distance( dh, p_new_val );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  // Read back the parameter.
  rc = mbg_get_tr_distance( dh, &re_read_val );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  if ( re_read_val == *p_new_val )
  {
    // Saved successfully.
    rc = MBG_SUCCESS;
    goto out;
  }


  // If execution gets here, the re-read value doesn't match
  // the desired new value. Either the original value is unchanged,
  // or the device has stored a truncated value.

  // Only if the original value has been provided, we can also check
  // whether the re-read value matches the original value, or not.
  if ( p_org_val )
  {
    if ( re_read_val != *p_org_val )
    {
      // The re-read value doesn't match the original value, either,
      // so a truncated value has been saved by the device.
      rc = MBG_ERR_CFG_TRUNC;
      goto out_ret_re_read;
    }
  }

  // The new value has not been accepted.
  rc = MBG_ERR_CFG;

out_ret_re_read:
  // For convenience, we store the re-read value in *p_new_val,
  // so it can be made available to the caller.
  *p_new_val = re_read_val;

out:
  return rc;

}  // mbg_set_and_check_tr_distance



/*HDR*/
/**
 * @brief Read a debug status word from a device.
 *
 * This is mainly supported by IRIG timecode receivers, and the status
 * word is intended to provide more detailed information why a device
 * might not synchronize to an incoming timecode signal.
 *
 * See ::MBG_DEBUG_STATUS and related definitions for details.
 *
 * The API call ::mbg_chk_dev_has_debug_status checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_DEBUG_STATUS variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_debug_status
 */
_MBG_API_ATTR int _MBG_API mbg_get_debug_status( MBG_DEV_HANDLE dh, MBG_DEBUG_STATUS *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_GET_DEBUG_STATUS,
                              IOCTL_GET_DEBUG_STATUS, p,
                              _pcps_ddev_has_debug_status( dh ) );
  _mbg_swab_debug_status( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_debug_status



/*HDR*/
/**
 * @brief Clear the event log buffer onboard a device.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * @param[in]  dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_first_evt_log_entry
 * @see ::mbg_get_next_evt_log_entry
 */
_MBG_API_ATTR int _MBG_API mbg_clr_evt_log( MBG_DEV_HANDLE dh )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_write_cmd_chk( dh, PCPS_CLR_EVT_LOG, IOCTL_CLR_EVT_LOG,
                           _pcps_ddev_has_evt_log( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_clr_evt_log



/*HDR*/
/**
 * @brief Read details about the event log buffer onboard a device.
 *
 * The returned structure ::MBG_NUM_EVT_LOG_ENTRIES indicates how many
 * event log entries can be stored on the device, and how many entries
 * are actually stored.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_NUM_EVT_LOG_ENTRIES variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_first_evt_log_entry
 * @see ::mbg_get_next_evt_log_entry
 */
_MBG_API_ATTR int _MBG_API mbg_get_num_evt_log_entries( MBG_DEV_HANDLE dh, MBG_NUM_EVT_LOG_ENTRIES *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_NUM_EVT_LOG_ENTRIES,
                              IOCTL_GET_NUM_EVT_LOG_ENTRIES, p,
                              _pcps_ddev_has_evt_log( dh ) );
  _mbg_swab_mbg_num_evt_log_entries( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_num_evt_log_entries



/*HDR*/
/**
 * @brief Read the first (oldest) event log entry from a device.
 *
 * @note Subsequent reads should be made using ::mbg_get_next_evt_log_entry.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * If no (more) event log entry is available onboard the device,
 * the returned MBG_EVT_LOG_ENTRY::code is MBG_EVT_ID_NONE.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_EVT_LOG_ENTRY variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_next_evt_log_entry
 */
_MBG_API_ATTR int _MBG_API mbg_get_first_evt_log_entry( MBG_DEV_HANDLE dh, MBG_EVT_LOG_ENTRY *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_FIRST_EVT_LOG_ENTRY,
                              IOCTL_GET_FIRST_EVT_LOG_ENTRY, p,
                              _pcps_ddev_has_evt_log( dh ) );
  _mbg_swab_mbg_evt_log_entry( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_first_evt_log_entry



/*HDR*/
/**
 * @brief Read the next event log entry from a device.
 *
 * @note The first read should be made using ::mbg_get_first_evt_log_entry
 * to set the on-board read index to the oldest entry.
 *
 * The API call ::mbg_chk_dev_has_evt_log checks whether
 * this call is supported by a device.
 *
 * If no (more) event log entry is available onboard the device,
 * the returned MBG_EVT_LOG_ENTRY::code is MBG_EVT_ID_NONE.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_EVT_LOG_ENTRY variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_evt_log
 * @see ::mbg_clr_evt_log
 * @see ::mbg_get_num_evt_log_entries
 * @see ::mbg_get_first_evt_log_entry
 */
_MBG_API_ATTR int _MBG_API mbg_get_next_evt_log_entry( MBG_DEV_HANDLE dh, MBG_EVT_LOG_ENTRY *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_var_chk( dh, PCPS_NEXT_EVT_LOG_ENTRY,
                              IOCTL_GET_NEXT_EVT_LOG_ENTRY, p,
                              _pcps_ddev_has_evt_log( dh ) );
  _mbg_swab_mbg_evt_log_entry( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_next_evt_log_entry



/*HDR*/
/**
 * @brief Read the current GNSS mode info including current settings.
 *
 * The ::MBG_GNSS_MODE_INFO structure tells which GNSS systems are supported
 * by a device, and also includes the settings currently in effect.
 *
 * The API call ::mbg_chk_dev_is_gnss can be used to check whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_is_gnss.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p_mi  Pointer to a ::MBG_GNSS_MODE_INFO structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_gps_gnss_mode_settings
 * @see ::mbg_get_gps_all_gnss_sat_info
 * @see ::mbg_chk_dev_is_gnss
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_gnss_mode_info( MBG_DEV_HANDLE dh, MBG_GNSS_MODE_INFO *p_mi )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_GNSS_MODE,
                              IOCTL_GET_GNSS_MODE_INFO, p_mi,
                              _pcps_ddev_is_gnss( dh ) );
  _mbg_swab_mbg_gnss_mode_info( p_mi );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_gnss_mode_info



/*HDR*/
/**
 * @brief Write the GNSS mode configuration to a device.
 *
 * The ::MBG_GNSS_MODE_SETTINGS structure determines the GNSS settings
 * for a device, e.g. which satellite systems have to be used.
 *
 * The function ::mbg_get_gps_gnss_mode_info should have been called before
 * to determine which GNSS settings are supported by the device.
 *
 * The API call ::mbg_chk_dev_is_gnss can be used to check whether
 * this call is supported by a device.
 *
 * See also the notes for ::mbg_chk_dev_is_gnss.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p_ms  Pointer to a ::MBG_GNSS_MODE_SETTINGS structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_get_gps_all_gnss_sat_info
 * @see ::mbg_chk_dev_is_gnss
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_gnss_mode_settings( MBG_DEV_HANDLE dh,
                                                           const MBG_GNSS_MODE_SETTINGS *p_ms )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    MBG_GNSS_MODE_SETTINGS tmp = *p_ms;
    _mbg_swab_mbg_gnss_mode_settings( &tmp );
    p_ms = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_GNSS_MODE,
                               IOCTL_SET_GNSS_MODE_SETTINGS, p_ms,
                               _pcps_ddev_is_gnss( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_gnss_mode_settings



/*HDR*/
/**
 * @brief Read a ::GNSS_SAT_INFO_IDX array of satellite status information.
 *
 * The function ::mbg_get_gps_gnss_mode_info must have been called before,
 * and the returned ::MBG_GNSS_MODE_INFO structure be passed to this function.
 *
 * @param[in]   dh    Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  gsii  Pointer to a an array of satellite info structures to be filled up.
 * @param[in]   p_mi  Pointer to a ::MBG_GNSS_MODE_INFO structure returned by ::mbg_get_gps_gnss_mode_info.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_gps_gnss_mode_info
 * @see ::mbg_set_gps_gnss_mode_settings
 * @see ::mbg_chk_dev_is_gnss
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_gnss_sat_info( MBG_DEV_HANDLE dh,
                                                          GNSS_SAT_INFO_IDX gsii[],
                                                          const MBG_GNSS_MODE_INFO *p_mi )
{
  MBGDEVIO_RET_VAL rc;

  int n_supp = num_bits_set( p_mi->supp_gnss_types );

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_GNSS_SAT_INFO,
                            IOCTL_GET_ALL_GNSS_SAT_INFO, gsii,
                            n_supp * sizeof( gsii[0] ),
                            _pcps_ddev_is_gnss( dh ) );
  #else
    rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_GNSS_SAT_INFO, gsii,
                                 n_supp * sizeof( gsii[0] ) );
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < n_supp; i++ )
      {
        GNSS_SAT_INFO_IDX *p = &gsii[i];
        _mbg_swab_gnss_sat_info_idx( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_gnss_sat_info



/*HDR*/
/**
 * @brief Read common GPIO configuration limits.
 *
 * The API call ::mbg_chk_dev_has_gpio checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::MBG_GPIO_CFG_LIMITS structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
_MBG_API_ATTR int _MBG_API mbg_get_gpio_cfg_limits( MBG_DEV_HANDLE dh, MBG_GPIO_CFG_LIMITS *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_GPIO_CFG_LIMITS,
                              IOCTL_GET_GPIO_CFG_LIMITS, p,
                              _pcps_ddev_has_gpio( dh ) );
  _mbg_swab_mbg_gpio_cfg_limits( p );

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gpio_cfg_limits



/*HDR*/
/**
 * @brief Get all GPIO settings and capabilities.
 *
 * The API call ::mbg_chk_dev_has_gpio checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  gii    An array of ::MBG_GPIO_STATUS_IDX structures to be filled up.
 * @param[in]   p_gcl  Pointer to a ::MBG_GPIO_CFG_LIMITS structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_gpio_info( MBG_DEV_HANDLE dh,
                                                      MBG_GPIO_INFO_IDX gii[],
                                                      const MBG_GPIO_CFG_LIMITS *p_gcl )
{
  MBGDEVIO_RET_VAL rc;

  uint32_t n_supp = p_gcl->num_io;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_GPIO_INFO,
                            IOCTL_GET_ALL_GPIO_INFO, gii,
                            n_supp * sizeof( gii[0] ),
                            _pcps_ddev_has_gpio( dh ) );
  #else
    rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_GPIO_INFO, gii,
                                 n_supp * sizeof( gii[0] ) );
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < n_supp; i++ )
      {
        MBG_GPIO_INFO_IDX *p = &gii[i];
        _mbg_swab_mbg_gpio_info_idx( p, 1 );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_gpio_info



/*HDR*/
/**
 * @brief Write the configuration for a single GPIO port to a device.
 *
 * The ::MBG_GPIO_SETTINGS_IDX structure contains both the ::MBG_GPIO_SETTINGS
 * and the port index value.
 *
 * The API call ::mbg_chk_dev_has_gpio checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   Pointer to a ::MBG_GPIO_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 * @see ::mbg_get_gps_all_gpio_status
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_gpio_settings_idx( MBG_DEV_HANDLE dh,
                                                          const MBG_GPIO_SETTINGS_IDX *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    MBG_GPIO_SETTINGS_IDX tmp = *p;
    _mbg_swab_mbg_gpio_settings_idx( &tmp, 1 );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_GPIO_SETTINGS_IDX,
                               IOCTL_SET_GPIO_SETTINGS_IDX, p,
                               _pcps_ddev_has_gpio( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_gpio_settings_idx



/*HDR*/
/**
 * @brief Read the status of all GPIO signal ports.
 *
 * @param[in]   dh     Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  gsi    An array of ::MBG_GPIO_STATUS_IDX structures to be filled up.
 * @param[in]   p_gcl  Pointer to a ::MBG_GPIO_CFG_LIMITS structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_gpio
 * @see ::mbg_get_gpio_cfg_limits
 * @see ::mbg_get_gps_all_gpio_info
 * @see ::mbg_set_gps_gpio_settings_idx
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_gpio_status( MBG_DEV_HANDLE dh,
                                                        MBG_GPIO_STATUS_IDX gsi[],
                                                        const MBG_GPIO_CFG_LIMITS *p_gcl )
{
  MBGDEVIO_RET_VAL rc;

  uint32_t n_supp;

  if ( !( p_gcl->flags & MBG_GPIO_CFG_LIMIT_FLAG_MASK_STATUS_SUPP ) )
    return MBG_ERR_NOT_SUPP_BY_DEV;

  n_supp = p_gcl->num_io;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_GPIO_STATUS,
                            IOCTL_GET_ALL_GPIO_STATUS, gsi,
                            n_supp * sizeof( gsi[0] ),
                            _pcps_ddev_has_gpio( dh ) );
  #else
    rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_GPIO_STATUS, gsi,
                                 n_supp * sizeof( gsi[0] ) );
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < n_supp; i++ )
      {
        MBG_GPIO_STATUS_IDX *p = &gsi[i];
        _mbg_swab_mbg_gpio_status_idx( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_gpio_status



/*HDR*/
/**
 * @brief Read ::XMULTI_REF_INSTANCES.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p   A ::XMULTI_REF_INSTANCES structure to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
_MBG_API_ATTR int _MBG_API mbg_get_xmr_instances( MBG_DEV_HANDLE dh, XMULTI_REF_INSTANCES *p )
{
  MBGDEVIO_RET_VAL rc;
  _mbgdevio_read_gps_var_chk( dh, PC_GPS_XMR_INSTANCES,
                              IOCTL_GET_XMR_INSTANCES, p,
                              _pcps_ddev_has_xmr( dh ) );
  _mbg_swab_xmulti_ref_instances( p );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_xmr_instances



/*HDR*/
/**
 * @brief Read the status of all XMR sources.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  xmrsi   An array of ::XMULTI_REF_STATUS_IDX structures to be filled up.
 * @param[in]   p_xmri  Pointer to a ::XMULTI_REF_INSTANCES structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_xmr_status( MBG_DEV_HANDLE dh,
                                                       XMULTI_REF_STATUS_IDX xmrsi[],
                                                       const XMULTI_REF_INSTANCES *p_xmri )
{
  MBGDEVIO_RET_VAL rc;

  int n_supp = p_xmri->n_xmr_settings;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_XMR_STATUS,
                            IOCTL_GET_ALL_XMR_STATUS, xmrsi,
                            n_supp * sizeof( xmrsi[0] ),
                            _pcps_ddev_has_xmr( dh ) );
  #else
    rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_XMR_STATUS, xmrsi,
                                 n_supp * sizeof( xmrsi[0] ) );
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < n_supp; i++ )
      {
        XMULTI_REF_STATUS_IDX *p = &xmrsi[i];
        _mbg_swab_xmulti_ref_status_idx( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_xmr_status



/*HDR*/
/**
 * @brief Read all XMR settings and capabilities.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  xmrii   An array of ::XMULTI_REF_INFO_IDX structures to be filled up.
 * @param[in]   p_xmri  Pointer to a ::XMULTI_REF_INSTANCES structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_set_gps_xmr_settings_idx
 * @see ::mbg_get_xmr_holdover_status
 */
_MBG_API_ATTR int _MBG_API mbg_get_gps_all_xmr_info( MBG_DEV_HANDLE dh,
                                                     XMULTI_REF_INFO_IDX xmrii[],
                                                     const XMULTI_REF_INSTANCES *p_xmri )
{
  MBGDEVIO_RET_VAL rc;

  int n_supp = p_xmri->n_xmr_settings;

  #if _MBG_SUPP_VAR_ACC_SIZE
    _mbgdevio_read_gps_chk( dh, PC_GPS_ALL_XMR_INFO,
                            IOCTL_GET_ALL_XMR_INFO, xmrii,
                            n_supp * sizeof( xmrii[0] ),
                            _pcps_ddev_has_xmr( dh ) );
  #else
    rc = _mbgdevio_gen_read_gps( dh, PC_GPS_ALL_XMR_INFO, xmrii,
                                 n_supp * sizeof( xmrii[0] ) );
  #endif

  #if defined( MBG_ARCH_BIG_ENDIAN )
    if ( mbg_rc_is_success( rc ) )
    {
      int i;
      for ( i = 0; i < n_supp; i++ )
      {
        XMULTI_REF_INFO_IDX *p = &xmrii[i];
        _mbg_swab_xmulti_ref_info_idx( p );
      }
    }
  #endif

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_gps_all_xmr_info



/*HDR*/
/**
 * @brief Write a single XMR setting to a device.
 *
 * The ::XMULTI_REF_SETTINGS_IDX structure contains both the ::XMULTI_REF_SETTINGS
 * and the index value.
 *
 * The API call ::mbg_chk_dev_has_xmr checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[in]   p   Pointer to a ::XMULTI_REF_SETTINGS_IDX structure to be written.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_get_xmr_holdover_status
 */
_MBG_API_ATTR int _MBG_API mbg_set_gps_xmr_settings_idx( MBG_DEV_HANDLE dh,
                                                         const XMULTI_REF_SETTINGS_IDX *p )
{
  MBGDEVIO_RET_VAL rc;
  #if defined( MBG_ARCH_BIG_ENDIAN )
    XMULTI_REF_SETTINGS_IDX tmp = *p;
    _mbg_swab_xmulti_ref_settings_idx( &tmp );
    p = &tmp;
  #endif
  _mbgdevio_write_gps_var_chk( dh, PC_GPS_XMR_SETTINGS_IDX,
                               IOCTL_SET_XMR_SETTINGS_IDX, p,
                               _pcps_ddev_has_xmr( dh ) );
  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_set_gps_xmr_settings_idx



/*HDR*/
/**
 * @brief Read the current XMR holdover interval from a device.
 *
 * The API call ::mbg_chk_dev_has_xmr checks whether
 * this call is supported by a device.
 *
 * @param[in]   dh      Valid ::MBG_DEV_HANDLE handle to a Meinberg device.
 * @param[out]  p       Pointer to a ::XMR_HOLDOVER_INTV structure to be filled up.
 * @param[in]   p_xmri  Pointer to a ::XMULTI_REF_INSTANCES structure read before.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_chk_dev_has_xmr
 * @see ::mbg_get_xmr_instances
 * @see ::mbg_get_gps_all_xmr_status
 * @see ::mbg_get_gps_all_xmr_info
 * @see ::mbg_set_gps_xmr_settings_idx
 */
_MBG_API_ATTR int _MBG_API mbg_get_xmr_holdover_status( MBG_DEV_HANDLE dh, XMR_HOLDOVER_STATUS *p,
                                                        const XMULTI_REF_INSTANCES *p_xmri )
{
  MBGDEVIO_RET_VAL rc;

  if ( !( p_xmri->flags & XMRIF_MSK_HOLDOVER_STATUS_SUPP ) )
    return MBG_ERR_NOT_SUPP_BY_DEV;

  _mbgdevio_read_gps_var_chk( dh, PC_GPS_XMR_HOLDOVER_STATUS,
                              IOCTL_GET_XMR_HOLDOVER_STATUS, p,
                              _pcps_ddev_has_xmr( dh ) );
  _mbg_swab_xmr_holdover_status( p );

  return _mbgdevio_cnv_ret_val( rc );

}  // mbg_get_xmr_holdover_status



/*HDR*/
/**
 * @brief Read the CPU affinity of a process.
 *
 * This means on which of the available CPUs or CPU cores
 * a process may be executed.
 *
 * @param[in]   pid  The process ID.
 * @param[out]  p    Pointer to a ::MBG_CPU_SET variable which contains a mask of CPUs.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_set_process_affinity
 * @see ::mbg_set_current_process_affinity_to_cpu
 */
_MBG_API_ATTR int _MBG_API mbg_get_process_affinity( MBG_PROCESS_ID pid, MBG_CPU_SET *p )
{
  #if defined( MBG_TGT_LINUX )  // TODO Maybe generally for POSIX?

    if ( sched_getaffinity( pid, sizeof( *p ), p ) == 0 )  // success
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #elif defined( MBG_TGT_WIN32 )

    MBG_CPU_SET system_affinity_mask = 0;

    if ( GetProcessAffinityMask( pid, p, &system_affinity_mask ) )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #else

    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_get_process_affinity



/*HDR*/
/**
 * @brief Set the CPU affinity of a process.
 *
 * This determines on which of the available CPUs
 * or CPU cores the process is allowed to be executed.
 *
 * @param[in]   pid  The process ID.
 * @param[out]  p    Pointer to a ::MBG_CPU_SET variable which contains a mask of CPUs.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_process_affinity
 * @see ::mbg_set_current_process_affinity_to_cpu
 */
_MBG_API_ATTR int _MBG_API mbg_set_process_affinity( MBG_PROCESS_ID pid, MBG_CPU_SET *p )
{
  #if defined( MBG_TGT_LINUX )  // TODO Maybe generally for POSIX?

    if ( sched_setaffinity( pid, sizeof( *p ), p ) == 0 )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #elif defined( MBG_TGT_WIN32 )

    if ( SetProcessAffinityMask( pid, *p ) )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #else

    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_set_process_affinity



/*HDR*/
/**
 * @brief Set the CPU affinity of a process for a single CPU only.
 *
 * This means the process may only be executed on that single CPU.
 *
 * @param[in]  cpu_num  The number of the CPU.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_process_affinity
 * @see ::mbg_set_process_affinity
 */
_MBG_API_ATTR int _MBG_API mbg_set_current_process_affinity_to_cpu( int cpu_num )
{
  MBG_CPU_SET cpu_set;

  _mbg_cpu_clear( &cpu_set );
  _mbg_cpu_set( cpu_num, &cpu_set );

  return mbg_set_process_affinity( _mbg_get_current_process(), &cpu_set );

}  // mbg_set_current_process_affinity_to_cpu



#if MBGDEVIO_USE_THREAD_API

/*HDR*/
/**
 * @brief Create a new execution thread for the current process.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in]  p_ti  Pointer to a ::MBG_THREAD_INFO structure to be filled up.
 * @param[in]  fnc   The name of the thread function to be started.
 * @param[in]  arg   A generic argument passed to the thread function.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_stop
 * @see ::mbg_thread_sleep_interruptible
 * @see ::mbg_thread_set_affinity
 */
_MBG_API_ATTR int _MBG_API mbg_thread_create( MBG_THREAD_INFO *p_ti,
                                              MBG_THREAD_FNC fnc, void *arg )
{
  #if defined( MBG_TGT_LINUX )  // TODO Maybe generally for POSIX?

    if ( pthread_create( &p_ti->thread_id, NULL, fnc, arg ) == 0 )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #elif defined( MBG_TGT_WIN32 )

    HANDLE h;
    DWORD thread_id = 0;

    p_ti->exit_request = CreateEvent( NULL, FALSE, FALSE, NULL );

    if ( p_ti->exit_request == NULL )
      goto fail;

    h = CreateThread( NULL, 0, fnc, arg, 0, &thread_id );

    if ( h == NULL )
    {
      CloseHandle( p_ti->exit_request );
      goto fail;
    }

    p_ti->thread_id = h;

    return MBG_SUCCESS;

fail:
    return mbg_get_last_error( NULL );

  #else

    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_thread_create



/*HDR*/
/**
 * @brief Stop a thread which has been created by ::mbg_thread_create.
 *
 * Wait until the thread has finished and release all resources.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p_ti  Pointer to a ::MBG_THREAD_INFO structure associated with the thread.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_create
 * @see ::mbg_thread_sleep_interruptible
 * @see ::mbg_thread_set_affinity
 */
_MBG_API_ATTR int _MBG_API mbg_thread_stop( MBG_THREAD_INFO *p_ti )
{
  #if defined( MBG_TGT_LINUX )  // TODO Maybe generally for POSIX?

    pthread_cancel( p_ti->thread_id );

    if ( pthread_join( p_ti->thread_id, NULL ) == 0 )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #elif defined( MBG_TGT_WIN32 )

    if ( SetEvent( p_ti->exit_request ) &&
         WaitForSingleObject( p_ti->thread_id, 10000L ) == 0 )
    {
      CloseHandle( p_ti->exit_request );
      p_ti->exit_request = NULL;

      CloseHandle( p_ti->thread_id );
      p_ti->thread_id = NULL;

      return MBG_SUCCESS;
    }

    return mbg_get_last_error( NULL );

  #else

    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_thread_stop



/*HDR*/
/**
 * @brief Let the current thread sleep for a certain interval.
 *
 * The sleep is interrupted if a signal is received indicating
 * the thread should terminate.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p_ti      Pointer to a ::MBG_THREAD_INFO structure associated with the thread.
 * @param[in]      sleep_ms  The number of milliseconds to sleep.
 *
 * @return ::MBG_SUCCESS   if the sleep interval has expired normally,<br>
 *         ::MBG_ERR_INTR  if a signal to terminate has been received,<br>
 *         or one of the other @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_create
 * @see ::mbg_thread_stop
 * @see ::mbg_thread_set_affinity
 */
_MBG_API_ATTR int _MBG_API mbg_thread_sleep_interruptible( MBG_THREAD_INFO *p_ti, ulong sleep_ms )
{
  #if defined( MBG_TGT_LINUX )  // TODO Maybe generally for POSIX?

    (void) p_ti;  // Avoid warning "never used".

    if ( usleep( sleep_ms * 1000 ) == 0 )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #elif defined( MBG_TGT_WIN32 )

    DWORD dw = WaitForSingleObject( p_ti->exit_request, sleep_ms );

    switch ( dw )
    {
      case WAIT_OBJECT_0: // has been interrupted to terminate
        return MBG_ERR_INTR;

      case WAIT_TIMEOUT:  // sleep interval expired without interruption
        return MBG_SUCCESS;
    }

    return mbg_get_last_error( NULL );

  #else

    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_thread_sleep_interruptible



#if MBGDEVIO_HAVE_THREAD_AFFINITY

/*HDR*/
/**
 * @brief Set the CPU affinity of a single thread.
 *
 * This determines on which of the available CPUs the thread
 * is allowed to be executed.
 *
 * This function is only implemented for targets that support thread affinity.
 *
 * @param[in,out]  p_ti  Pointer to a ::MBG_THREAD_INFO structure associated with the thread.
 * @param[in]      p     Pointer to a ::MBG_CPU_SET variable which contains a mask of CPUs.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_thread_create
 * @see ::mbg_thread_stop
 * @see ::mbg_thread_sleep_interruptible
 */
_MBG_API_ATTR int _MBG_API mbg_thread_set_affinity( MBG_THREAD_INFO *p_ti, MBG_CPU_SET *p )
{
  #if defined( MBG_TGT_LINUX )  // TODO Maybe generally for POSIX?

    if ( pthread_setaffinity_np( tid, sizeof( *p ), p ) == 0 )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #elif defined( MBG_TGT_WIN32 )

    MBG_CPU_SET prv_thread_affinity = SetThreadAffinityMask( p_ti->thread_id, *p );

    if ( prv_thread_affinity )
      return MBG_SUCCESS;

    return mbg_get_last_error( NULL );

  #else

    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_thread_set_affinity

#endif



static /*HDR*/
/**
 * @brief A thread function implementing polling of a device at regular intervals.
 *
 * At each polling, a high resolution time stamp and an associated cycles count
 * are saved which can be used to retrieve extrapolated time stamps using the
 * cycles counter. The thread also computes the frequency of the system's cycles
 * counter.
 *
 * On systems where the cycles counter is implemented by a CPU's time stamp
 * counter (TSC) it may be required to set the thread or process affinity to a
 * single CPU to get reliable cycles counts. In this case also care should be
 * taken that the CPU's clock frequency is not stepped up and down e.g. due
 * to power saving mechanisms (e.g. Intel SpeedStep, or AMD Cool'n'Quiet).
 * Otherwise time interpolation may be messed up.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p_void  Pointer to a ::MBG_POLL_THREAD_INFO structure.
 *
 * @return ::MBG_SUCCESS or nothing, depending on the taget system.
 *
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 */
MBG_THREAD_FNC_RET_VAL MBG_THREAD_FNC_ATTR mbg_xhrt_poll_thread_fnc( void *p_void )
{
  MBG_XHRT_VARS prv_xhrt_vars;

  MBG_POLL_THREAD_INFO *p_pti = (MBG_POLL_THREAD_INFO *) p_void;
  MBG_XHRT_INFO *p = &p_pti->xhrt_info;

  memset( &prv_xhrt_vars, 0, sizeof( prv_xhrt_vars ) );

  for (;;)
  {
    MBG_XHRT_VARS xhrt_vars;
    MBG_PC_CYCLES_FREQUENCY freq = 0;
    int sleep_ms;

    int rc = mbg_get_hr_time_cycles( p->dh, &xhrt_vars.htc );

    if ( mbg_rc_is_success( rc ) )
    {
      xhrt_vars.pcps_hr_tstamp64 = pcps_time_stamp_to_uint64( &xhrt_vars.htc.t.tstamp );

      if ( prv_xhrt_vars.pcps_hr_tstamp64 && ( ( xhrt_vars.htc.t.status & PCPS_LS_ENB ) == 0 ) )
        freq = ( mbg_delta_pc_cycles( &xhrt_vars.htc.cycles, &prv_xhrt_vars.htc.cycles ) * MBG_FRAC32_UNITS_PER_SEC )
                                    / ( xhrt_vars.pcps_hr_tstamp64 - prv_xhrt_vars.pcps_hr_tstamp64 );
    }


    _mbg_crit_sect_enter( &p->crit_sect );

    if ( mbg_rc_is_success( rc ) )
    {
      p->vars = xhrt_vars;
      p->prv_vars = prv_xhrt_vars;

      if ( freq )
        p->freq_hz = freq;
    }

    p->ioctl_status = rc;

    sleep_ms = p->sleep_ms;

    _mbg_crit_sect_leave( &p->crit_sect  );


    if ( mbg_rc_is_success( rc ) )
      prv_xhrt_vars = xhrt_vars;

    if ( mbg_rc_is_error( mbg_thread_sleep_interruptible( &p_pti->ti, sleep_ms ) ) )
      break;
  }

  _mbg_thread_exit( MBG_SUCCESS );  // TODO or an optional error code?

}  // mbg_xhrt_poll_thread_fnc

MBG_THREAD_FNC mbg_xhrt_poll_thread_fnc;



/*HDR*/
/**
 * @brief Set up a ::MBG_POLL_THREAD_INFO structure and start a new thread.
 *
 * The new thread runs a function which periodically reads
 * a time stamp / cycles pair from a device.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p_pti     Pointer to a ::MBG_POLL_THREAD_INFO structure.
 * @param[in]      dh the    Handle of the device to be polled.
 * @param[in]      freq_hz   The initial cycles frequency, if known, in Hz.
 * @param[in]      sleep_ms  the sleep interval for the poll thread function in ms.
 *                           If this parameter is 0, the default sleep interval is used.
 *
 * @return ::MBG_SUCCESS on success,<br>
 *         ::MBG_ERR_NOT_SUPP_BY_DEV if the device to poll does not support HR time,<br>
 *         else the result of ::mbg_thread_create.
 *
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 */
_MBG_API_ATTR int _MBG_API mbg_xhrt_poll_thread_create( MBG_POLL_THREAD_INFO *p_pti, MBG_DEV_HANDLE dh,
                                                        MBG_PC_CYCLES_FREQUENCY freq_hz, int sleep_ms )
{
  int rc = mbg_chk_dev_has_hr_time( dh );

  if ( mbg_rc_is_error( rc ) )
    return rc;

  memset( p_pti, 0, sizeof( *p_pti ) );

  p_pti->xhrt_info.dh = dh;
  p_pti->xhrt_info.freq_hz = freq_hz;
  p_pti->xhrt_info.sleep_ms = sleep_ms ? sleep_ms : 1000;   // sleep 1 second by default
  _mbg_crit_sect_init( &p_pti->xhrt_info.crit_sect, "MBGDEVIO_xhrt_info" );

  rc = mbg_thread_create( &p_pti->ti, mbg_xhrt_poll_thread_fnc, p_pti );

  return rc;

}  // mbg_xhrt_poll_thread_create



/*HDR*/
/**
 * @brief Stop a polling thread started by ::mbg_xhrt_poll_thread_create.
 *
 * This call also releases all associated resources.
 *
 * @param[in,out]  p_pti  Pointer to a ::MBG_POLL_THREAD_INFO structure.
 *
 * @return The result of ::mbg_thread_stop.
 *
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 */
_MBG_API_ATTR int _MBG_API mbg_xhrt_poll_thread_stop( MBG_POLL_THREAD_INFO *p_pti )
{
  int rc = mbg_thread_stop( &p_pti->ti );

  if ( mbg_rc_is_success( rc ) )
    _mbg_crit_sect_destroy( &p_pti->xhrt_info.crit_sect );

  return rc;

}  // mbg_xhrt_poll_thread_stop



static __mbg_inline /*HDR*/
/**
 * @brief Retrieve an extrapolated time stamp.
 *
 * Get the system's current cycles count and compute the current time
 * based on last recent time stamp/cycles pair read by the polling thread.
 *
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in]   p         Pointer to a ::MBG_XHRT_INFO structure providing data from the polling thread.
 * @param[out]  p_tstamp  Optional pointer to a variable to take the extrapolated time stamp, may be @a NULL.
 * @param[out]  p_vars    Optional pointer to a ::MBG_XHRT_VARS structure to be filled up, may be @a NULL.
 *
 * @return The return code from the API call used by the polling thread
 *         to read the time from the device, usually ::mbg_get_hr_time_cycles,
 *         so ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbg_xhrt_poll_group
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 * @see @ref mbg_xhrt_poll_group
 */
int mbg_get_xhrt_data( MBG_XHRT_INFO *p, uint64_t *p_tstamp, MBG_XHRT_VARS *p_vars )
{
  MBG_XHRT_VARS xhrt_vars;
  MBG_PC_CYCLES cyc_now;
  uint64_t t_now = 0;
  MBG_PC_CYCLES_FREQUENCY freq_hz;
  int ioctl_status;

  mbg_get_pc_cycles( &cyc_now );

  _mbg_crit_sect_enter( &p->crit_sect );
  xhrt_vars = p->vars;
  freq_hz = p->freq_hz;
  ioctl_status = p->ioctl_status;
  _mbg_crit_sect_leave( &p->crit_sect );

  if ( freq_hz && xhrt_vars.pcps_hr_tstamp64 )
  {
    t_now = xhrt_vars.pcps_hr_tstamp64 +
      ( mbg_delta_pc_cycles( &cyc_now, &xhrt_vars.htc.cycles ) * MBG_FRAC32_UNITS_PER_SEC ) / freq_hz;
    mbg_chk_tstamp64_leap_sec( &t_now, &xhrt_vars.htc.t.status );
  }

  if ( p_tstamp )
    *p_tstamp = t_now;

  if ( p_vars )
    *p_vars = xhrt_vars;

  return ioctl_status;

}  // mbg_get_xhrt_data



/*HDR*/
/**
 * @brief Retrieve an extrapolated time stamp in ::PCPS_HR_TIME format.
 *
 * The time stamp is extrapolated using the system's current cycles counter value
 * and a time stamp plus associated cycles counter value saved by the polling thread.
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p      Pointer to a ::MBG_XHRT_INFO structure used to retrieve data from the polling thread.
 * @param[out]     p_hrt  Pointer to a ::PCPS_HR_TIME structure to be filled up.
 *
 * @return The return code from the API call used by the polling thread
 *         to read the time from the device, usually ::mbg_get_hr_time_cycles,
 *         so ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbg_xhrt_poll_group
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see ::mbg_get_xhrt_cycles_frequency
 * @see @ref mbg_xhrt_poll_group
 */
_MBG_API_ATTR int _MBG_API mbg_get_xhrt_time_as_pcps_hr_time( MBG_XHRT_INFO *p, PCPS_HR_TIME *p_hrt )
{
  uint64_t tstamp64;
  MBG_XHRT_VARS xhrt_vars;

  int rc = mbg_get_xhrt_data( p, &tstamp64, &xhrt_vars );

  // Even if an IOCTL error has occurred recently in the polling thread
  // the interpolation may still work correctly. So we just continue
  // normally but pass the return code on to the calling function.

  uint64_to_pcps_time_stamp( &p_hrt->tstamp, tstamp64 );

  // Update status (valid only for the previous second!)
  p_hrt->signal   = xhrt_vars.htc.t.signal;
  p_hrt->status   = xhrt_vars.htc.t.status;
  p_hrt->utc_offs = xhrt_vars.htc.t.utc_offs;

  return rc;

}  // mbg_get_xhrt_time_as_pcps_hr_time



#if defined( MBG_TGT_WIN32 )

/*HDR*/
/**
 * @brief Retrieve an extrapolated time stamp in 'FILETIME' format.
 *
 * The time stamp is extrapolated using the system's current cycles counter value
 * and a time stamp plus associated cycles counter value saved by the polling thread.
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * Since @a FILETIME is a Windows-specific type, this function is only
 * implemented on Windows.
 *
 * @param[in,out]  p     Pointer to a ::MBG_XHRT_INFO structure used to retrieve data from the polling thread.
 * @param[out]     p_ft  Pointer to a FILETIME structure to be filled up.
 *
 * @return The return code from the API call used by the polling thread
 *         to read the time from the device, usually ::mbg_get_hr_time_cycles,
 *         so ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbg_xhrt_poll_group
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_cycles_frequency
 * @see @ref mbg_xhrt_poll_group
 */
_MBG_API_ATTR int _MBG_API mbg_get_xhrt_time_as_filetime( MBG_XHRT_INFO *p, FILETIME *p_ft )
{
  uint64_t tstamp64;

  int rc = mbg_get_xhrt_data( p, &tstamp64, NULL );

  // Even if an IOCTL error has occurred recently in the polling thread
  // the interpolation may still work correctly. So we just continue
  // normally but pass the return code on to the calling function.

  mbg_pcps_tstamp64_to_filetime( p_ft, &tstamp64 );

  return rc;

}  // mbg_get_xhrt_time_as_filetime

#endif



/*HDR*/
/**
 * @brief Retrieve the frequency of the system's cycles counter.
 *
 * The frequency is determined by the device polling thread.
 * See @ref mbg_xhrt_poll_group for details and limitations.
 *
 * This function is only implemented for targets that support threads.
 *
 * @param[in,out]  p          Pointer to a ::MBG_XHRT_INFO structure used to retrieve data from the polling thread.
 * @param[out]     p_freq_hz  Pointer to a ::MBG_PC_CYCLES_FREQUENCY variable in which the frequency is returned.
 *
 * @return The return code from the API call used by the polling thread
 *         to read the time from the device, usually ::mbg_get_hr_time_cycles,
 *         so ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @ingroup mbg_xhrt_poll_group
 * @see ::mbg_xhrt_poll_thread_create
 * @see ::mbg_xhrt_poll_thread_stop
 * @see ::mbg_get_xhrt_time_as_pcps_hr_time
 * @see ::mbg_get_xhrt_time_as_filetime
 * @see @ref mbg_xhrt_poll_group
 */
_MBG_API_ATTR int _MBG_API mbg_get_xhrt_cycles_frequency( MBG_XHRT_INFO *p, MBG_PC_CYCLES_FREQUENCY *p_freq_hz )
{
  MBG_PC_CYCLES_FREQUENCY freq_hz;
  int ioctl_status;

  _mbg_crit_sect_enter( &p->crit_sect );
  freq_hz = p->freq_hz;
  ioctl_status = p->ioctl_status;
  _mbg_crit_sect_leave( &p->crit_sect );

  if ( p_freq_hz )
    *p_freq_hz = freq_hz;

  return ioctl_status;

}  // mbg_get_xhrt_cycles_frequency

#endif // defined MBGDEVIO_USE_THREAD_API



/*HDR*/
/**
 * @brief Retrieve the system's default cycles counter frequency from the kernel driver.
 *
 * This API call can be used on systems that don't provide this information in user space.
 *
 * @param[in]   dh  Handle of the device to which the IOCTL call is sent.
 * @param[out]  p   Pointer of a ::MBG_PC_CYCLES_FREQUENCY variable to be filled up.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::mbg_get_default_cycles_frequency
 */
_MBG_API_ATTR int _MBG_API mbg_get_default_cycles_frequency_from_dev( MBG_DEV_HANDLE dh, MBG_PC_CYCLES_FREQUENCY *p )
{
  #if defined( _MBGIOCTL_H )
    MBGDEVIO_RET_VAL rc;
    rc = _mbgdevio_read_var( dh, -1, IOCTL_GET_CYCLES_FREQUENCY, p );
    // native endianess, no need to swap bytes
    if ( mbg_rc_is_error( rc ) )
      *p = 0;

    #if defined( MBG_TGT_LINUX )
      if ( *p == 0 )
      {
        rc = mbg_chk_dev_has_hr_time( dh );

        if ( mbg_rc_is_success( rc ) )
        {
          PCPS_HR_TIME_CYCLES htc1;
          PCPS_HR_TIME_CYCLES htc2;
          double delta_cycles;
          double delta_t;

          rc = mbg_get_hr_time_cycles( dh, &htc1 );

          if ( mbg_rc_is_error( rc ) )
            goto done;

          sleep( 1 );

          rc = mbg_get_hr_time_cycles( dh, &htc2 );

          if ( mbg_rc_is_error( rc ) )
            goto done;

          // compute cycles frequency from delta htc
          delta_cycles = mbg_delta_pc_cycles( &htc2.cycles, &htc1.cycles );
          delta_t = pcps_time_stamp_to_uint64( &htc2.t.tstamp ) - pcps_time_stamp_to_uint64( &htc1.t.tstamp );
          *p = ( delta_cycles * MBG_FRAC32_UNITS_PER_SEC ) / delta_t;
        }
      }
done:
    #endif

    return _mbgdevio_cnv_ret_val( rc );

  #else

    *p = 0;

    return MBG_ERR_NOT_SUPP_ON_OS;

  #endif

}  // mbg_get_default_cycles_frequency_from_dev



/*HDR*/
/**
 * @brief Retrieve the system's default cycles counter frequency.
 *
 * @note This may not be supported on all target platforms, in which case the
 * returned frequency is 0 and the ::mbg_get_default_cycles_frequency_from_dev
 * call should be used instead.
 *
 * @return The default cycles counter frequency in Hz, or 0 if the value is not available.
 *
 * @see ::mbg_get_default_cycles_frequency_from_dev
 */
_MBG_API_ATTR MBG_PC_CYCLES_FREQUENCY _MBG_API mbg_get_default_cycles_frequency( void )
{
  #if defined MBG_TGT_WIN32

    MBG_PC_CYCLES_FREQUENCY mbg_pc_cycles_frequency;

    mbg_get_pc_cycles_frequency( &mbg_pc_cycles_frequency );

    return mbg_pc_cycles_frequency;

  #else

    return 0;

  #endif

}  // mbg_get_default_cycles_frequency



/*HDR*/
/**
 * @brief Retrieve a string with the SYN1588 type name.
 *
 * @return  The type name string, usually "SYN1588".
 */
_MBG_API_ATTR const char * _MBG_API mbg_get_syn1588_type_name( void )
{
  #if defined( SUPP_1588 )
    return mbg_syn1588_type_name;
  #else
    return MBG_SYN1588_TYPE_NAME;
  #endif

}  // mbg_get_syn1588_type_name



/*HDR*/
/**
 * @brief Retrieve the content of a SYN1588 NIC's PTP_SYNC_STATUS register.
 *
 * @note The variable addressed by @p p is unchanged if an error occurred.
 *
 * @param[in]   dh  Valid ::MBG_DEV_HANDLE handle.
 * @param[out]  p   Pointer to a uint32_t variable to be filled.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
_MBG_API_ATTR int _MBG_API mbg_get_syn1588_ptp_sync_status( MBG_DEV_HANDLE dh, uint32_t *p )
{
  #if SUPP_SYN1588
    return mbg_syn1588_get_ptp_sync_status( dh, p );
  #else
    (void) dh;
    (void ) p;
    return err_syn1588_not_supp();
  #endif

}  // mbg_get_syn1588_ptp_sync_status



#if defined( MBG_TGT_WIN32 )

#if defined( _USRDLL )
#pragma data_seg( "shared" )

// By default every process which uses a DLL gets its own instance
// of the variables defined inside the DLL, so one process can not
// unintentionally or even intentionally access DLL variables of a
// different process.
//
// This is a special data section for this DLL. All variables defined
// inside this section are explicitly *shared* between all processes
// which use this DLL, so a mutex needs to be used to make sure only
// one process accesses these variables at the same time. The mutex
// is initialized when the first instance of the DLL is loaded.
//
// These variables are not exported, so access to the variables is
// only possible via predefined functions which take care access is
// protected by the mutex.
//
// ATTENTION:
// Every variable defined in this section must be explicitly initialized,
// otherwise its initial value is undefined.
static MBG_MUTEX mbgdevio_syn1588_mutex = { 0 };
static bool mbgdevio_syn1588_mutex_initialized = false;

#pragma data_seg()
#pragma comment( linker, "/SECTION:shared,RWS" )



// Prototype generated manually /*HDR*/
void mbgdevio_syn1588_get_lock_win32( void )
{
  _mbg_mutex_acquire( &mbgdevio_syn1588_mutex );

}  // mbgdevio_syn1588_get_lock_win32



// Prototype generated manually /*HDR*/
void mbgdevio_syn1588_release_lock_win32( void )
{
  _mbg_mutex_release( &mbgdevio_syn1588_mutex );

}  // mbgdevio_syn1588_release_lock_win32



BOOL APIENTRY DllMain( HANDLE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved )
{
  if ( ul_reason_for_call == DLL_PROCESS_ATTACH )
    mbg_svc_register_event_source( MBG_APP_EVTLOG_NAME_MBGDEVIO_DLL );

  #if defined( MBG_TGT_WIN32 )
    if ( !mbgdevio_syn1588_mutex_initialized )
    {
      _mbg_mutex_init( &mbgdevio_syn1588_mutex, "mbgdevio_syn1588_mutex" );
      mbgdevio_syn1588_mutex_initialized = true;
    }
  #endif  // defined( MBG_TGT_WIN32 )

  return TRUE;
}

#endif // defined( _USRDLL )

#endif  // defined( MBG_TGT_WIN32 )

