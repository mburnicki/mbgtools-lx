/**************************************************************************
 *
 *  $Id: mbgserio.c 1.13 2023/05/09 10:00:38 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Meinberg serial I/O functions.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgserio.c $
 *  Revision 1.13  2023/05/09 10:00:38  martin
 *  Fixed a doxygen comment.
 *  Revision 1.12  2023/02/16 07:34:33  gregoire.diehl
 *  Added strdup define for target CVI
 *  Revision 1.11  2021/12/01 10:19:07Z  martin.burnicki
 *  Make a parameter of mbgserio_get_duration_rcv() const.
 *  Revision 1.10  2021/05/04 20:31:36  martin
 *  New functions mbgserio_get_rx_fifo_threshold,
 *  mbgserio_get_port_basename and mbgserio_free_port_basename,
 *  mbgserio_get_dev_poll_timeout and mbgserio_set_dev_poll_timeout,
 *  mbgserio_get_duration_xmt and mbgserio_get_duration_rcv.
 *  New structure MBGSERIO_SPEED_PARMS which is also
 *  part of the control structure, and useful to calculate
 *  the transmission time.
 *  Renamed some local variables.
 *  Moved device list functions to the bottom of the file.
 *  Updated some comments and removed trailing spaces.
 *  Revision 1.9  2021/04/07 17:20:04  martin
 *  Updated a bunch of comments, and renamed some local variables.
 *  Revision 1.8  2019/09/27 15:33:18  martin
 *  Account for a renamed library function.
 *  Excluded definition of dev_dir from build since it is
 *  anyway only used in some conditional code.
 *  Revision 1.7  2017/07/06 07:21:18  martin
 *  Allocate a device control structure when opening a port,
 *  and free the structure when the port is closed.
 *  Use safe string functions from str_util.c.
 *  Use common Meinberg error codes from mbgerror.h.
 *  Check for MBG_TGT_POSIX instead of MBG_TGT_UNIX.
 *  Account for renamed library symbols.
 *  Added doxygen comments.
 *  Revision 1.6  2013/04/18 13:50:58Z  udo
 *  use O_CLOEXEC on open serial port if defined
 *  Revision 1.5  2013/02/01 16:06:33  martin
 *  Always use mbgserio_read/write rather than the macros.
 *  Got rid of _mbg_open/close/read/write() macros.
 *  Check return code of tcgetattr() when opening port on Linux.
 *  Set up DCB on Windows when opening the port,
 *  not when setting parameters.
 *  Fixed setting up COM port list on Windows.
 *  Flush output on close.
 *  Debug code to test flush on Windows (doesn't seem
 *  to work properly).
 *  New code trying different ways to detect existing ports
 *  reliably on Linux.
 *  Syntax workaround which is required until this module becomes a DLL.
 *  Revision 1.4  2011/07/29 10:12:15  martin
 *  Allow baud rates 115200 and 230400 on Linux, if supported by the OS version.
 *  Revision 1.3  2009/09/01 10:49:30  martin
 *  Cleanup for CVI.
 *  Use new portable timeout functions from mbg_tmo.h.
 *  Timeouts are now specified in milliseconds.
 *  Set DOS/v24tools low level receive timeout to minimum on open.
 *  Let functions return predefined codes.
 *  Revision 1.2  2008/09/04 15:34:18Z  martin
 *  Moved support for different target environments from other files here.
 *  Added mbgserio_set_parms() and don't set parms when opening a port.
 *  Fixed bugs in timeout calculations in mbgserio_read_wait().
 *  Preliminary support for port device lists.
 *  Revision 1.1  2007/11/12 16:48:02  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _MBGSERIO
 #include <mbgserio.h>
#undef _MBGSERIO

#include <mbgerror.h>
#include <str_util.h>
#include <mbg_tgt.h>

#include <stdio.h>
#include <ctype.h>
#include <time.h>

#if defined( MBG_TGT_POSIX )
  #include <fcntl.h>
  #include <dirent.h>
  #include <errno.h>
#endif

#if defined( MBG_TGT_CVI )
   #include "toolbox.h"
   #define strdup StrDup
#elif defined( MBG_TGT_WIN32 )
  #define strdup _strdup
#endif


// This can be defined != 0 to enabled some
// preliminary, untested code.
#define USE_LINUX_DEV_DIR  0

#if USE_LINUX_DEV_DIR
  static const char dev_dir[] = "/dev";
#endif



#if defined( _USE_V24TOOLS )

/*------------------------------------------------------------------------
 * The definitions in this block and all v24...() functions are part of a
 * third-party library called V.24 Tools Plus by Langner Expertensysteme.
 *
 * That library may not be freely distributed, so the v24..() functions
 * must be replaced by user-written functions or some other library.
 *-----------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

int v24open( char *portname, int mode );
/* Open a port with specified name (e.g. "COM1"). The functions return a
 * handle to be used with the other functions. If the handle is < 0, the
 * port could not be opened.
 */

#define O_DIRECT     0x0100  /* open port with direct access to the hardware */
#define O_HIGHPRIO   0x2000  /* open port with high priority (fastopen) */

/* This is the open mode we use here. */
#define OPEN_MODE    ( O_DIRECT | O_HIGHPRIO )


int v24setparams( int port, long speed, int dbits, int parity, int stopbits );
int v24write(int port,void *s,unsigned len);
int v24read(int port,void *s,unsigned len);
int v24flush(int port);
int v24close( int port );

#ifdef __cplusplus
}
#endif

#endif  // defined( _USE_V24TOOLS )



static /*HDR*/
/**
 * @brief Deallocate a serial device control structure.
 *
 * Free the memory allocated for the device control structure
 * and set the pointer to @a NULL.
 *
 * @param[in,out]  pp_sdev  Address of a pointer to a device control structure.
 *
 * @see ::alloc_mbgserio_dev
 */
void dealloc_mbgserio_dev( MBGSERIO_DEV **pp_sdev )
{
  MBGSERIO_DEV *sdev = *pp_sdev;

  if ( sdev )
  {
    free( sdev );
    *pp_sdev = NULL;
  }

}  // dealloc_mbgserio_dev



static /*HDR*/
/**
 * @brief Allocate a serial device control structure.
 *
 * @param[in,out]  pp_sdev  Address of a pointer to a device control structure.
 *
 * @return ::MBG_SUCCESS or one of the @ref MBG_ERROR_CODES.
 *
 * @see ::dealloc_mbgserio_dev
 */
int alloc_mbgserio_dev( MBGSERIO_DEV **pp_sdev )
{
  int rc;
  MBGSERIO_DEV *sdev = (MBGSERIO_DEV *) malloc( sizeof( *sdev ) );

  if ( sdev == NULL )
  {
    rc = mbg_get_last_error( "malloc failed in alloc_mbgserio_dev" );
    goto out;
  }

  memset( sdev, 0, sizeof( *sdev ) );
  sdev->port_handle = MBG_INVALID_PORT_HANDLE;
  rc = MBG_SUCCESS;

out:
  *pp_sdev = sdev;
  return rc;

}  // alloc_mbgserio_dev



/*HDR*/
/**
 * @brief Return the base name of the device name of a serial port.
 *
 * In order to leave the original device name string unchanged,
 * a buffer is allocated with a duplicate of the original string,
 * and a pointer to the basename component in that buffer is returned.
 *
 * After usage, the allocated buffer whose address is saved in
 * @p *pp_dup_port_name should be freed by calling ::mbgserio_free_port_basename.
 *
 * @param[in]  pp_dup_port_name  Address of a <em>char *</em> that takes the address
 *                               of the allocated string buffer.
 * @param[in]  port_name         The original device name string.
 *
 * @return  A pointer to the basename which is part of an allocated copy of the original string.
 *
 * @see ::mbgserio_free_port_basename
 */
_NO_MBG_API_ATTR char * _MBG_API mbgserio_get_port_basename( char **pp_dup_port_name, const char *port_name )
{
  char *cp = NULL;

  *pp_dup_port_name = strdup( port_name );

  if ( *pp_dup_port_name )
  {
    #if defined( MBG_TGT_POSIX )
      // We remove e.g. the "/dev/" part
      // of the device name.
      cp = basename( *pp_dup_port_name );
    #else
      // Nothing to be removed from a
      // device name like "COM1".
      cp = *pp_dup_port_name;
    #endif
  }

  return cp;

}  // mbgserio_get_port_basename



/*HDR*/
/**
 * @brief Free a buffer that has been allocated by ::mbgserio_get_port_basename.
 *
 * This function should be called whenever a string buffer allocated
 * by an earlier ::mbgserio_get_port_basename call isn't needed anymore.
 *
 * @param[in]  pp_dup_port_name  Address of a <em>char *</em> with the address
 *                               of the allocated string buffer.
 *
 * @see ::mbgserio_get_port_basename
 */
_NO_MBG_API_ATTR void _MBG_API mbgserio_free_port_basename( char **pp_dup_port_name )
{
  if ( *pp_dup_port_name )
  {
    free( *pp_dup_port_name );
    *pp_dup_port_name = NULL;
  }

}  // mbgserio_free_port_basename



/*HDR*/
/**
 * @brief Determine the threshold of a serial RX FIFO buffer.
 *
 * This is useful to be able to exactly compensate the delay
 * of a received serial data string.
 *
 * @note This may not be supported on each target system.
 *
 * @param[in]  port_name  The device name of the serial port.
 *
 * @return  The number of bytes in the FIFO buffer after which
 *          a receive event (e.g. IRQ) is generated,
 *          or 0 if the real threshold can't be determined.
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_get_rx_fifo_threshold( const char *port_name )
{
  int n = 0;

  #if defined( MBG_TGT_LINUX )
  {
    static const char tty_basename[] = "/sys/class/tty";
    static const char tty_rx_trig_fn[] = "rx_trig_bytes";

    char *dup_name = NULL;
    const char *cp = mbgserio_get_port_basename( &dup_name, port_name );
    char fn[256];
    FILE *fp;

    snprintf_safe( fn, sizeof( fn ), "%s/%s/%s",
                   tty_basename, cp, tty_rx_trig_fn );

    fp = fopen( fn, "rt" );

    if ( fp )
    {
      char s[80];
      cp = fgets( s, sizeof( s ), fp );

      if ( cp )
        n = atoi( cp );

      fclose( fp );
    }

    mbgserio_free_port_basename( &dup_name );
  }
  #endif

  return n;

}  // mbgserio_get_rx_fifo_threshold



#if defined( MBG_TGT_WIN32 )

static /*HDR*/
/**
 * @brief Win32-specific function which opens a serial port.
 *
 * @param[in]   dev_name  Basic device name, e.g. "COM1".
 * @param[out]  ph        Pointer to a @a HANDLE which is set up on success.
 *
 * @return ::MBG_SUCCESS on success, or one of the @ref MBG_ERROR_CODES.
 */
int win32_open_serial_port( const char *dev_name, HANDLE *ph )
{
  // A prefix is required for the device name to be able
  // to open ports with large numbers, e.g. COM15.
  static const char prefix[] = "\\\\.\\";

  size_t len;
  char *tmp_name;
  size_t n = 0;
  int rc = MBG_ERR_UNSPEC;

  if ( dev_name == NULL )
    return MBG_ERR_INV_PARM;

  len = strlen( prefix ) + strlen( dev_name ) + 1;
  tmp_name = (char *) malloc( len );

  if ( tmp_name == NULL )  // Unable to allocate memory.
  {
    rc = MBG_ERR_NO_MEM;
    goto out;
  }

  n = sn_cpy_str_safe( tmp_name, len, prefix );
  n += sn_cpy_str_safe( &tmp_name[n], len - n, dev_name );

  *ph = CreateFileA( tmp_name, GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL, // TODO Also use FILE_FLAG_WRITE_THROUGH ?
        NULL );

  if ( *ph == INVALID_HANDLE_VALUE )
  {
    rc = mbg_win32_sys_err_to_mbg( GetLastError(), "CreateFile failed in win32_open_serial_port" );
    goto out_free;
  }

  rc = MBG_SUCCESS;

out_free:
  free( tmp_name );

out:
  return rc;

}  // win32_open_serial_port

#endif  // defined( MBG_TGT_WIN32 )



/*HDR*/
/**
 * @brief Close a serial port specified by a ::MBGSERIO_DEV structure.
 *
 * After the port has been closed, the buffer that had been allocated
 * for the ::MBGSERIO_DEV structure is freed, and the pointer to that
 * buffer is set to @a NULL.
 *
 * @param[in,out]  pp_sdev  Address of a pointer to the control structure including the port handle.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_close( MBGSERIO_DEV **pp_sdev )
{
  if ( *pp_sdev)  // Only if a MBGSERIO_DEV structure has been allocated.
  {
    MBGSERIO_DEV *sdev = *pp_sdev;
    MBG_PORT_HANDLE *p_handle = &sdev->port_handle;

    if ( *p_handle != MBG_INVALID_PORT_HANDLE )
    {
      mbgserio_flush_tx( sdev );

      #if defined( MBG_TGT_CVI )

        CloseCom( *p_handle );

      #elif defined( MBG_TGT_WIN32 )

        // Restore original settings that have been saved.

        if ( sdev->org_dcb_has_been_read )
          SetCommState( *p_handle, &sdev->org_dcb );

        if ( sdev->org_commtimeouts_have_been_read )
          SetCommTimeouts( *p_handle, &sdev->org_commtimeouts );

        CloseHandle( *p_handle );

      #elif defined( MBG_TGT_POSIX )

        // Restore original settings that have been saved.

        if ( sdev->org_tio_has_been_read )
          tcsetattr( *p_handle, TCSANOW, &sdev->org_tio );

        close( *p_handle );

      #elif defined( MBG_TGT_DOS )
        #if defined( _USE_V24TOOLS )

          v24close( *p_handle );

        #else

          #error Target DOS requires v24tools for serial I/O.

        #endif

      #else

        #error This target OS is not supported.

      #endif
    }

    dealloc_mbgserio_dev( pp_sdev );
  }

  return MBG_SUCCESS;

}  // mbgserio_close



/*HDR*/
/**
 * @brief Open a serial port and set up a ::MBGSERIO_DEV structure.
 *
 * @param[in,out]  pp_sdev   Address of a pointer to a ::MBGSERIO_DEV device control structure
 *                           allocated and set up by this call. Set to @a NULL on error.
 * @param[in]      dev_name  Device name of the port to be opened.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_open( MBGSERIO_DEV **pp_sdev, const char *dev_name )
{
  MBGSERIO_DEV *sdev;
  MBG_PORT_HANDLE *p_handle;

  int rc = alloc_mbgserio_dev( pp_sdev );

  if ( mbg_rc_is_error( rc ) )
    return rc;


  sdev = *pp_sdev;
  p_handle = &sdev->port_handle;

  #if defined( MBG_TGT_CVI )
  {
    MBGSERIO_SPEED_PARMS sp = { 0 };
    int parity_code = 0;
    int i;
    int len;
    int rc;

    sp.data_bits = 8;
    sp.baud_rate = 19200;
    sp.stop_bits = 1;

    // On CVI, the port handle passed to OpenComConfig and used furtheron
    // corresponds to the COM port number, e.g. 1 for COM1, so we extract
    // the number from the device name passed as parameter.
    len = strlen( dev_name );

    for ( i = 0; i < len; i++ )
    {
      char c = dev_name[i];

      if ( c >= '0' && c <= '9' )
        break;
    }

    if ( i == len )
    {
      rc = MBG_ERR_PARM_FMT;   // No numeric substring found.
      goto out;
    }

    *p_handle = atoi( &dev_name[i] );

    rc = OpenComConfig( *p_handle, NULL, sp.baud_rate, parity_code,
                        sp.data_bits, sp.stop_bits, 8192, 1024 );  // TODO

    if ( rc < 0 )
    {
      rc = mbg_get_last_error( "OpenComConfig failed in mbgserio_open" );
      *p_handle = MBG_INVALID_PORT_HANDLE;
      goto out;
    }

    rc = SetComTime( *p_handle, 1.0 );   // FIXME TODO WTF?

    if ( rc < 0 )
    {
      rc = mbg_get_last_error( "SetComTime failed in mbgserio_open" );
      goto out;
    }

    rc = SetXMode( *p_handle, 0 );

    if ( rc < 0 )
    {
      rc = mbg_get_last_error( "SetXMode failed in mbgserio_open" );
      goto out;
    }

    sdev->sp = sp;
  }
  #elif defined( MBG_TGT_WIN32 )
  {
    COMMTIMEOUTS commtimeouts;
    DCB dcb;

    rc = win32_open_serial_port( dev_name, p_handle );

    // rc is now one of the @ref MBG_RETURN_CODES, and in case of an error
    // the handle has already been set to ::MBG_INVALID_PORT_HANDLE.
    if ( mbg_rc_is_error( rc ) )
      goto out;

    // Save the settings found at startup.
    sdev->org_dcb.DCBlength = sizeof( sdev->org_dcb );

    if ( !GetCommState( *p_handle, &sdev->org_dcb ) )
    {
      rc = mbg_get_last_error( "GetCommState failed in mbgserio_open" );
      goto out;
    }

    sdev->org_dcb_has_been_read = 1;

    if ( !GetCommTimeouts( *p_handle, &sdev->org_commtimeouts ) )
    {
      rc = mbg_get_last_error( "GetCommTimeouts failed in mbgserio_open" );
      goto out;
    }

    sdev->org_commtimeouts_have_been_read = 1;

    if ( !GetCommProperties( *p_handle, &sdev->comm_prop ) )
    {
      rc = mbg_get_last_error( "GetCommProperties failed in mbgserio_open" );
      goto out;
    }

    sdev->comm_prop_have_been_read = 1;

    #if 0 // fields provided by the Windows DCB structure
      DWORD DCBlength;      /* sizeof(DCB)                     */
      DWORD BaudRate;       /* Baudrate at which running       */
      DWORD fBinary: 1;     /* Binary Mode (skip EOF check)    */
      DWORD fParity: 1;     /* Enable parity checking          */
      DWORD fOutxCtsFlow:1; /* CTS handshaking on output       */
      DWORD fOutxDsrFlow:1; /* DSR handshaking on output       */
      DWORD fDtrControl:2;  /* DTR Flow control                */
      DWORD fDsrSensitivity:1; /* DSR Sensitivity              */
      DWORD fTXContinueOnXoff: 1; /* Continue TX when Xoff sent */
      DWORD fOutX: 1;       /* Enable output X-ON/X-OFF        */
      DWORD fInX: 1;        /* Enable input X-ON/X-OFF         */
      DWORD fErrorChar: 1;  /* Enable Err Replacement          */
      DWORD fNull: 1;       /* Enable Null stripping           */
      DWORD fRtsControl:2;  /* Rts Flow control                */
      DWORD fAbortOnError:1; /* Abort all reads and writes on Error */
      DWORD fDummy2:17;     /* Reserved                        */
      WORD wReserved;       /* Not currently used              */
      WORD XonLim;          /* Transmit X-ON threshold         */
      WORD XoffLim;         /* Transmit X-OFF threshold        */
      BYTE ByteSize;        /* Number of bits/byte, 4-8        */
      BYTE Parity;          /* 0-4=None,Odd,Even,Mark,Space    */
      BYTE StopBits;        /* 0,1,2 = 1, 1.5, 2               */
      char XonChar;         /* Tx and Rx X-ON character        */
      char XoffChar;        /* Tx and Rx X-OFF character       */
      char ErrorChar;       /* Error replacement char          */
      char EofChar;         /* End of Input character          */
      char EvtChar;         /* Received Event character        */
      WORD wReserved1;      /* Fill for now.                   */
    #endif

    // Configure our settings.
    dcb = sdev->org_dcb;         // Current settings as default.
    dcb.fOutxCtsFlow = FALSE;      // CTS output flow control.
    dcb.fOutxDsrFlow = FALSE;      // DSR output flow control.
    dcb.fDtrControl = DTR_CONTROL_ENABLE;  // Enable DTR.
    dcb.fDsrSensitivity = FALSE;   // Don't require DSR input active.
    // TODO more missing here.
    dcb.fRtsControl = RTS_CONTROL_ENABLE;  // Enable RTS as required for for C28COM.
    dcb.fOutX = FALSE;

    if ( !SetCommState( *p_handle, &dcb ) )
    {
      rc = mbg_get_last_error( "SetCommState failed in mbgserio_open" );
      goto out;
    }

    memset( &commtimeouts, 0, sizeof( commtimeouts ) );

    if ( !SetCommTimeouts( *p_handle, &commtimeouts ) )
    {
      rc = mbg_get_last_error( "SetCommTimeout failed in mbgserio_open" );
      goto out;
    }

    #if !defined( MBGSERIO_IN_BUFFER_SIZE )
      #define MBGSERIO_IN_BUFFER_SIZE 2048
    #endif

    #if !defined( MBGSERIO_OUT_BUFFER_SIZE )
      #define MBGSERIO_OUT_BUFFER_SIZE 2048
    #endif

    if ( !SetupComm( *p_handle, MBGSERIO_IN_BUFFER_SIZE, MBGSERIO_OUT_BUFFER_SIZE ) )
    {
      rc = mbg_get_last_error( "SetupComm failed in mbgserio_open" );
      goto out;
    }

    PurgeComm( *p_handle, PURGE_TXABORT | PURGE_TXCLEAR );
    PurgeComm( *p_handle, PURGE_RXABORT | PURGE_RXCLEAR );

    // TODO Call mbgextio_set_console_control_handler() ?
  }
  #elif defined( MBG_TGT_POSIX )
  {
    // Open as not controlling TTY to prevent from being
    // killed if CTRL-C is received.
    // O_NONBLOCK is the same as O_NDELAY.
    int flags = O_RDWR | O_NOCTTY | O_NONBLOCK;

    #if defined( O_CLOEXEC )
      flags |= O_CLOEXEC;
    #endif

    *p_handle = open( dev_name, flags );   // Returns -1 on error.

    if ( *p_handle < 0 )
    {
      rc = mbg_get_last_error( "open failed in mbgserio_open" );
      *p_handle = MBG_INVALID_PORT_HANDLE;
      goto out;
    }

    // TODO On Unix, a serial port can by default be opened
    // by several processes. However, we don't want that, so we
    // should care about this using a lock file for the device
    // and/or setting the TIOCEXCL flag (which unfortunately
    // is not an atomic operation with open()).

    // Save current device settings.
    if ( tcgetattr( *p_handle, &sdev->org_tio ) < 0 )
    {
      rc = mbg_get_last_error( "tcgetattr failed in mbgserio_open" );
      goto out;
    }

    sdev->org_tio_has_been_read = 1;

    // TODO Set an atexit() function to close the port?
  }
  #elif defined( MBG_TGT_DOS )
    #if defined( _USE_V24TOOLS )
    {
      *p_handle = v24open( (char *) dev_name, OPEN_MODE );

      if ( *p_handle < 0 )
      {
        rc = MBG_ERR_UNSPEC;   // TODO Translate rc ?
        *p_handle = MBG_INVALID_PORT_HANDLE;
        goto out;
      }

      v24settimeout( *p_handle, 1 );
    }
    #else

      #error Target DOS requires v24tools for serial I/O.
      rc = MBG_ERR_NOT_SUPP_ON_OS;
      goto out;

    #endif

  #else

    #error This target OS is not supported.
    rc = MBG_ERR_NOT_SUPP_ON_OS;
    goto out;

  #endif

  // Try to get the receive FIFO threshold.
  // Will be 0 if this isn't supported.
  sdev->rcv_fifo_threshold = mbgserio_get_rx_fifo_threshold( dev_name );

  rc = MBG_SUCCESS;

out:
  if ( mbg_rc_is_error( rc ) )
    mbgserio_close( pp_sdev );

  return rc;

}  // mbgserio_open



/*HDR*/
/**
 * @brief Set the transmission parameters of a serial port.
 *
 * @param[in,out]  sdev       Control structure of the device.
 * @param[in]      baud_rate  One of the well-known baud rates, e.g. 19200.
 * @param[in]      framing    A short string providing the framing, e.g. "8N1".
 *
 * @return One of the @ref MBG_RETURN_CODES.
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_set_parms( MBGSERIO_DEV *sdev,
                                                  uint32_t baud_rate, const char *framing )
{
  MBG_PORT_HANDLE port_handle = sdev->port_handle;
  MBGSERIO_SPEED_PARMS sp = { 0 };
  const char *cp;
  int rc = MBG_ERR_UNSPEC;

  sp.baud_rate = baud_rate;

  #if defined( MBG_TGT_CVI )
  {
    int parity_code = 0;

    // Setup framing.
    for ( cp = framing; *cp; cp++ )
    {
      char c = toupper( *cp );

      switch ( c )
      {
        case '7':
        case '8':
          sp.data_bits = c - '0';
          break;

        case 'N':
          parity_code = 0;
          sp.parity_bits = 0;
          break;

        case 'E':
          parity_code = 2;
          sp.parity_bits = 1;
          break;

        case 'O':
          parity_code = 1;
          sp.parity_bits = 1;
          break;

        case '1':
        case '2':
          sp.stop_bits = c - '0';
          break;

        default:
          return MBG_ERR_PARM_FMT;  // Invalid framing string.
      }
    }

    rc = OpenComConfig( port_handle, NULL, sp.baud_rate, parity_code,
                        sp.data_bits, sp.stop_bits, 8192, 1024 );
    if ( rc < 0 )
      return mbg_cvi_rs232_error_to_mbg( rc, "OpenComConfig failed in mbgserio_set_parms" );

    rc = SetComTime( port_handle, 1.0 );

    if ( rc < 0 )
      return mbg_cvi_rs232_error_to_mbg( rc, "SetComTime failed in mbgserio_set_parms" );

    rc = SetXMode( port_handle, 0 );

    if ( rc < 0 )
      return mbg_cvi_rs232_error_to_mbg( rc, "SetXMode failed in mbgserio_set_parms" );

    // On success, fall through.
  }
  #elif defined( MBG_TGT_WIN32 )
  {
    DCB dcb;

    // Get current settings.
    dcb.DCBlength = sizeof( DCB );

    if ( GetCommState( port_handle, &dcb ) == FALSE )
      return mbg_get_last_error( "GetCommState failed in mbgserio_set_parms" );

    // Change settings as requested.

    dcb.BaudRate = baud_rate;

    // Setup framing.
    for ( cp = framing; *cp; cp++ )
    {
      char c = toupper( *cp );

      switch ( c )
      {
        case '7':
        case '8':
          dcb.ByteSize = c - '0';
          sp.data_bits = dcb.ByteSize;
          break;

        case 'N':
          dcb.Parity = NOPARITY;
          sp.parity_bits = 0;
          break;

        case 'E':
          dcb.Parity = EVENPARITY;
          sp.parity_bits = 1;
          break;

        case 'O':
          dcb.Parity = ODDPARITY;
          sp.parity_bits = 1;
          break;

        case '1':
          dcb.StopBits = ONESTOPBIT;
          sp.stop_bits = 1;
          break;

        case '2':
          dcb.StopBits = TWOSTOPBITS;
          sp.stop_bits = 2;
          break;

        default:
          return MBG_ERR_PARM_FMT;  // Invalid framing string.
      }
    }

    if ( SetCommState( port_handle, &dcb ) == FALSE )
      return mbg_get_last_error( "SetCommState failed in mbgserio_set_parms" );

    // On success, fall through.
  }
  #elif defined( MBG_TGT_POSIX )
  {
    tcflag_t c_cflag = 0;
    struct termios tio;

    rc = tcgetattr( port_handle, &tio );

    if ( rc < 0 )
      return mbg_get_last_error( "tcgetattr failed in mbgserio_set_parms" );

    // Setup transmission speed.
    switch ( baud_rate )
    {
      case 300:    c_cflag = B300;    break;
      case 600:    c_cflag = B600;    break;
      case 1200:   c_cflag = B1200;   break;
      case 2400:   c_cflag = B2400;   break;
      case 4800:   c_cflag = B4800;   break;
      case 9600:   c_cflag = B9600;   break;
      case 19200:  c_cflag = B19200;  break;
      case 38400:  c_cflag = B38400;  break;
      case 57600:  c_cflag = B57600;  break;
      #if defined( B115200 )
        case 115200: c_cflag = B115200; break;
      #endif
      #if defined( B230400 )
        case 230400: c_cflag = B230400; break;
      #endif
      #if defined( B460800 )
        case 460800: c_cflag = B460800; break;
      #endif
      #if defined( B500000 )
        case 500000: c_cflag = B500000; break;
      #endif
      #if defined( B576000 )
        case 576000: c_cflag = B576000; break;
      #endif
      #if defined( B921600 )
        case 921600: c_cflag = B921600; break;
      #endif
      #if defined( B1000000 )
        case 1000000: c_cflag = B1000000; break;
      #endif
      #if defined( B1152000 )
        case 1152000: c_cflag = B1152000; break;
      #endif
      #if defined( B1500000 )
        case 1500000: c_cflag = B1500000; break;
      #endif
      #if defined( B2000000 )
        case 2000000: c_cflag = B2000000; break;
      #endif
      #if defined( B2500000 )
        case 2500000: c_cflag = B2500000; break;
      #endif
      #if defined( B3000000 )
        case 3000000: c_cflag = B3000000; break;
      #endif
      #if defined( B3500000 )
        case 3500000: c_cflag = B3500000; break;
      #endif
      #if defined( B4000000 )
        case 4000000: c_cflag = B4000000; break;
      #endif

      default:
        return MBG_ERR_INV_PARM;  // Invalid baud rate.
    }

    #if 0 // TODO This should preferably be used for portability reasons.
       int cfsetispeed( struct termios *termios_p, speed_t speed );
       int cfsetospeed( struct termios *termios_p, speed_t speed );
    #endif

    // Setup framing.
    for ( cp = framing; *cp; cp++ )
    {
      char c = toupper( *cp );

      switch ( c )
      {
        case '7':
          c_cflag |= CS7;
          sp.data_bits = 7;
          break;

        case '8':
          c_cflag |= CS8;
          sp.data_bits = 8;
          break;

        case 'N':
          sp.parity_bits = 0;
          break;

        case 'E':
          c_cflag |= PARENB;
          sp.parity_bits = 1;
          break;

        case 'O':
          c_cflag |= PARENB | PARODD;
          sp.parity_bits = 1;
          break;

        case '1':
          sp.stop_bits = 1;
          break;

        case '2':
          c_cflag |= CSTOPB;
          sp.stop_bits = 2;
          break;

        default:
          return MBG_ERR_INV_PARM;  // Invalid framing string.
      }
    }


    // Setup control flags. The following flags are defined:
    //   CBAUD   (not in POSIX) Baud speed mask (4+1 bits).
    //   CBAUDEX (not in POSIX) Extra baud speed mask (1 bit), included in CBAUD.
    //           (POSIX says that the baud speed is stored in the termios structure
    //           without specifying where precisely, and provides cfgetispeed() and
    //           cfsetispeed() for getting at it. Some systems use bits selected
    //           by CBAUD in c_cflag, other systems use separate fields,
    //           e.g. sg_ispeed and sg_ospeed.)
    //   CSIZE   Character size mask. Values are CS5, CS6, CS7, or CS8.
    //   CSTOPB  Set two stop bits, rather than one.
    //   CREAD   Enable receiver.
    //   PARENB  Enable parity generation on output and parity checking for input.
    //   PARODD  Parity for input and output is odd.
    //   HUPCL   Lower modem control lines after last process closes the device (hang up).
    //   CLOCAL  Ignore modem control lines.
    //   LOBLK   (not in POSIX) Block output from a noncurrent shell layer.
    //           (For use by shl)
    //   CIBAUD  (not in POSIX) Mask for input speeds. The values for the CIBAUD bits are
    //           the same as the values for the CBAUD bits, shifted left IBSHIFT bits.
    //   CRTSCTS (not in POSIX) Enable RTS/CTS (hardware) flow control.

    // We use these flags:
    //   Local connection, no modem control (CLOCAL).
    //   No flow control (no CRTSCTS).
    //   Enable receiving (CREAD).
    tio.c_cflag = c_cflag | CLOCAL | CREAD;


    // Setup input flags. The following flags are defined:
    //   IGNBRK  Ignore BREAK condition on input
    //   BRKINT  If IGNBRK is set, a BREAK is ignored. If it is not set
    //           but BRKINT is set, then a BREAK causes the input and output
    //           queues to be flushed, and if the terminal is the controlling
    //           terminal of a foreground process group, it will cause a
    //           SIGINT to be sent to this foreground process  group. When
    //           neither IGNBRK nor BRKINT are set, a BREAK reads as a NUL
    //           character, except when PARMRK is set, in which case it reads
    //           as the sequence \377 \0 \0.
    //   IGNPAR  Ignore framing errors and parity errors.
    //   PARMRK  If IGNPAR is not set, prefix a character with a parity error
    //           framing error with \377 \0. If neither IGNPAR nor PARMRK or
    //           is set, read a character with a parity error or framing error as \0.
    //   INPCK   Enable input parity checking.
    //   ISTRIP  Strip off eighth bit.
    //   INLCR   Translate NL to CR on input.
    //   IGNCR   Ignore carriage return on input.
    //   ICRNL   Translate carriage return to newline on input (unless IGNCR is set).
    //   IUCLC   (not in POSIX) Map uppercase characters to lowercase on input.
    //   IXON    Enable XON/XOFF flow control on output.
    //   IXANY   (not in POSIX.1; XSI) Enable any character to restart output.
    //   IXOFF   Enable XON/XOFF flow control on input.
    //   IMAXBEL (not in POSIX) Ring bell when input queue is full. Linux does not
    //           implement this bit, and acts as if it is always set.
    tio.c_iflag = 0;

    #if 0  // TODO Do we want or need this?
    if ( c_cflag & PARENB )
      tio.c_iflag |= IGNPAR;  // This also ignores framing errors.
    #endif


    // Setup output flags. The following flags are defined:
    //   OPOST   Enable implementation-defined output processing.
    // The remaining c_oflag flag constants are defined in POSIX 1003.1-2001,
    // unless marked otherwise.
    //   OLCUC   (not in POSIX) Map lowercase characters to uppercase on output.
    //   ONLCR   (XSI) Map NL to CR-NL on output.
    //   OCRNL   Map CR to NL on output.
    //   ONOCR   Don't output CR at column 0.
    //   ONLRET  Don't output CR.
    //   OFILL   Send fill characters for a delay, rather than using a timed delay.
    //   OFDEL   (not in POSIX) Fill character is ASCII DEL (0177). If unset,
    //           fill character is ASCII NUL.
    //   NLDLY   Newline delay mask. Values are NL0 and NL1.
    //   CRDLY   Carriage return delay mask. Values are CR0, CR1, CR2, or CR3.
    //   TABDLY  Horizontal tab delay mask. Values are TAB0, TAB1, TAB2, TAB3
    //           (or XTABS). A value of TAB3, that is, XTABS, expands tabs to
    //           spaces (with tab stops every eight columns).
    //   BSDLY   Backspace delay mask. Values are BS0 or BS1.
    //           (Has never been implemented.)
    //   VTDLY   Vertical tab delay mask. Values are VT0 or VT1.
    //   FFDLY   Form feed delay mask. Values are FF0 or FF1.
    tio.c_oflag = 0;


    // Setup local mode flags. The following flags are defined:
    //   ISIG    When any of the characters INTR, QUIT, SUSP, or DSUSP are
    //           received, generate the corresponding signal.
    //   ICANON  Enable canonical mode. This enables the special characters
    //           EOF, EOL, EOL2, ERASE, KILL, LNEXT, REPRINT, STATUS, and
    //           WERASE, and buffers by lines.
    //   XCASE   (not in POSIX; not supported under Linux) If ICANON is also
    //           set, terminal is uppercase only. Input is converted to
    //           lowercase, except for characters preceded by \. On output,
    //           uppercase characters are preceded by \ and lowercase
    //           characters are converted to uppercase.
    //   ECHO    Echo input characters.
    //   ECHOE   If ICANON is also set, the ERASE character erases the preceding
    //           input character, and WERASE erases the preceding word.
    //   ECHOK   If ICANON is also set, the KILL character erases the current line.
    //   ECHONL  If ICANON is also set, echo the NL character even if ECHO is not set.
    //   ECHOCTL (not  in  POSIX) If ECHO is also set, ASCII control signals
    //           other than TAB, NL, START, and STOP are echoed as ^X, where
    //           X is the character with ASCII code 0x40 greater than the control
    //           signal. For example, character 0x08 (BS) is echoed as ^H.
    //   ECHOPRT (not in POSIX) If ICANON and IECHO are also set, characters are
    //           printed as they are being erased.
    //   ECHOKE  (not in POSIX) If ICANON is also set, KILL is echoed by erasing
    //           each character on the line, as specified by ECHOE and ECHOPRT.
    //   DEFECHO (not in POSIX) Echo only when a process is reading.
    //   FLUSHO  (not in POSIX; not supported under Linux) Output is being flushed.
    //           This flag is toggled by typing the DISCARD character.
    //   NOFLSH  Disable flushing the input and output queues when generating the
    //           SIGINT, SIGQUIT and SIGSUSP signals.
    //   TOSTOP  Send the SIGTTOU signal to the process group of a background
    //           process which tries to write to its controlling terminal.
    //   PENDIN  (not in POSIX; not supported under Linux) All characters in the
    //           input queue are reprinted when the next character is read.
    //           (bash  handles typeahead this way.)
    //   IEXTEN  Enable implementation-defined input processing. This flag, as
    //           well as ICANON must be enabled for the special characters EOL2,
    //           LNEXT, REPRINT, WERASE to be interpreted, and for the IUCLC flag
    //           to be effective.
    tio.c_lflag = 0;


    //   VINTR   (003, ETX, Ctrl-C, or also 0177, DEL, rubout) Interrupt character.
    //           Send a SIGINT signal. Recognized when ISIG is set, and then not
    //           passed as input.
    //   VQUIT   (034, FS, Ctrl-\) Quit character. Send SIGQUIT signal. Recognized
    //           when ISIG is set, and then not passed as input.
    //   VERASE  (0177, DEL, rubout, or 010, BS, Ctrl-H, or also #) Erase character.
    //           This erases the previous not-yet-erased character, but does not erase
    //           past EOF or beginning-of-line. Recognized when ICANON is set,
    //           and then not passed as input.
    //   VKILL   (025, NAK, Ctrl-U, or Ctrl-X, or also @) Kill character. This erases
    //           the input since the last EOF or beginning-of-line. Recognized when
    //           ICANON is set, and then not passed as input.
    //   VEOF    (004, EOT, Ctrl-D) End-of-file character. More precisely: this
    //           character causes the pending tty buffer to be sent to the waiting
    //           user program without waiting for end-of-line. If it is the first
    //           character of the line, the read() in the user program returns 0,
    //           which signifies end-of-file. Recognized when ICANON is set, and
    //           then not passed as input.
    //   VMIN    Minimum number of characters for non-canonical read.
    //   VEOL    (0, NUL) Additional end-of-line character. Recognized when ICANON is set.
    //   VTIME   Timeout in deciseconds for non-canonical read.
    //   VEOL2   (not in POSIX; 0, NUL) Yet another end-of-line character.
    //           Recognized when ICANON is set.
    //   VSWTCH  (not in POSIX; not supported under Linux; 0, NUL) Switch character.
    //           (Used by shl only.)
    //   VSTART  (021, DC1, Ctrl-Q) Start character. Restarts output stopped by the Stop
    //           character. Recognized when IXON is set, and then not passed as input.
    //   VSTOP   (023, DC3, Ctrl-S) Stop character. Stop output until Start character
    //           typed. Recognized when IXON is set, and then not passed as input.
    //   VSUSP   (032, SUB, Ctrl-Z) Suspend character. Send SIGTSTP signal. Recognized
    //           when ISIG is set, and then not passed as input.
    //   VDSUSP  (not in POSIX; not supported under Linux; 031, EM, Ctrl-Y) Delayed
    //           suspend character: send SIGTSTP signal when the character is read by
    //           the user program. Recognized when IEXTEN and ISIG are set, and the
    //           system supports job control, and then not passed as input.
    //   VLNEXT  (not in POSIX; 026, SYN, Ctrl-V) Literal next. Quotes the next input
    //           character, depriving it of a possible special meaning.  Recognized
    //           when IEXTEN is set, and then not passed as input.
    //   VWERASE (not in POSIX; 027, ETB, Ctrl-W) Word erase. Recognized when ICANON
    //           and IEXTEN are set, and then not passed as input.
    //   VREPRINT (not in POSIX; 022, DC2, Ctrl-R) Reprint unread characters. Recognized
    //           when ICANON and IEXTEN are set, and then not passed as input.
    //   VDISCARD (not in POSIX; not supported under Linux; 017, SI, Ctrl-O) Toggle:
    //           start/stop discarding pending output. Recognized when IEXTEN is set,
    //           and then not passed as input.
    //   VSTATUS (not in POSIX; not supported under Linux; status request: 024, DC4, Ctrl-T).


    // Setting up c_cc[VMIN] and c_cc[VTIME]:
    // If MIN > 0 and TIME = 0, MIN sets the number of characters to receive before
    //            the read is satisfied. As TIME is zero, the timer is not used.
    // If MIN = 0 and TIME > 0, TIME serves as a timeout value. The read will be
    //            satisfied if a single character is read, or TIME is exceeded
    //            (t = TIME *0.1 s). If TIME is exceeded, no character will be
    //            returned.
    // If MIN > 0 and TIME > 0, TIME serves as an inter-character timer. The
    //            read will be satisfied if MIN characters are received, or the
    //            time between two characters exceeds TIME. The timer is restarted
    //            every time a character is received and only becomes active after
    //            the first character has been received.
    // If MIN = 0 and TIME = 0, read will be satisfied immediately. The number of
    //            characters currently available, or the number of characters
    //            requested will be returned. You could issue a
    //            fcntl(fd, F_SETFL, FNDELAY); before reading to get the same result.

    // Setup control characters for non-blocking read.
    tio.c_cc[VMIN] = 0;
    tio.c_cc[VTIME] = 0;

    // Now clean the modem line and activate the settings for modem.
    if ( tcflush( port_handle, TCIFLUSH ) < 0  )
      return mbg_get_last_error( "tcflush failed in mbgserio_set_parms" );

    if ( tcsetattr( port_handle, TCSANOW, &tio ) < 0 )
      return mbg_get_last_error( "tcsetattr failed in mbgserio_set_parms" );

    // On success, fall through.
  }
  #elif defined( MBG_TGT_DOS )
    #if defined( _USE_V24TOOLS )
    {
      char parity = 'N';

      sp.data_bits = 8;
      sp.parity_bits = 0;
      sp.stop_bits = 1;

      // Setup framing.
      for ( cp = framing; *cp; cp++ )
      {
        char c = toupper( *cp );

        switch ( c )
        {
          case '7':
          case '8':
            sp.data_bits = c - '0';
            break;

          case 'N':
            parity = c;
            sp.parity_bits = 0;
            break;

          case 'E':
          case 'O':
            parity = c;
            sp.parity_bits = 1;
            break;

          case '1':
          case '2':
            sp.stop_bits = c - '0';
            break;

          default:
            return MBG_ERR_PARM_FMT;  // Invalid framing string.
        }
      }

      v24setparams( port_handle, baud_rate, datab, parity, stopb );  // TODO check rc?

      // On success, fall through.
    }
    #else

      #error This has to be modified for DOS without v24tools.

    #endif

  #else

    #error This target OS is not supported.

  #endif

  // Remember the configured speed parameters.
  sdev->sp = sp;

  rc = MBG_SUCCESS;

  return rc;

}  // mbgserio_set_parms



/*HDR*/
/**
 * @brief Read from a serial port.
 *
 * @param[in,out]  sdev    Control structure of the device.
 * @param[out]     buffer  Buffer to take the bytes read.
 * @param[in]      count   Size of @p buffer.
 *
 * @return On Success, the (positive) number of byte read,
 *         or one of the (negative) @ref MBG_ERROR_CODES on error.
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_read( MBGSERIO_DEV *sdev, void *buffer, unsigned int count )
{
  #if defined( MBG_TGT_CVI )

    return ComRd( sdev->port_handle, (char *) buffer, count );  // TODO Check or translate return code.

  #elif defined( MBG_TGT_WIN32 )

    BOOL fReadStat;
    COMSTAT ComStat;
    DWORD dwErrorFlags;
    DWORD dwLength;

    ClearCommError( sdev->port_handle, &dwErrorFlags, &ComStat );

    if ( dwErrorFlags )   // Transmission error (parity, framing, etc.).
      return MBG_ERR_IO;  // TODO Retrieve and return real error code.


    dwLength = min( (DWORD) count, ComStat.cbInQue );

    if ( dwLength )
    {
      fReadStat = ReadFile( sdev->port_handle, buffer, dwLength, &dwLength, NULL );

      if ( !fReadStat )
        return MBG_ERR_IO;  // TODO Retrieve and return real error code.
    }

    return dwLength;

  #elif defined( MBG_TGT_POSIX )

    int rc = read( sdev->port_handle, buffer, count );

    if ( rc < 0 )  // Usually -1 in case of error.
      rc = mbg_get_last_error( "read failed in mbgserio_read" );

    return rc;

  #elif defined( MBG_TGT_DOS ) && defined( _USE_V24TOOLS )

    int rc = v24read( sdev->port_handle, buffer, count );

    if ( rc < 0 )
      rc = MBG_ERR_UNSPEC;

    return rc;

  #else

    #error mbgserio_read() not implemented for this target

  #endif

} // mbgserio_read



/*HDR*/
/**
 * @brief Write to a serial port.
 *
 * @param[in,out]  sdev    Control structure of the device.
 * @param[in]      buffer  Buffer with the bytes to write.
 * @param[in]      count   Number of bytes in @p buffer to write.
 *
 * @return On Success, the (positive) number of byte written,
 *         or one of the (negative) @ref MBG_ERROR_CODES on error.
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_write( MBGSERIO_DEV *sdev, const void *buffer, unsigned int count )
{
  #if defined( MBG_TGT_CVI )

    return ComWrt( sdev->port_handle, (char *) buffer, count );  // TODO Check or translate return code.

  #elif defined( MBG_TGT_WIN32 )

    BOOL fWriteStat;
    COMSTAT ComStat;
    DWORD dwErrorFlags;
    DWORD dwThisBytesWritten;
    DWORD dwTotalBytesWritten = 0;
    MBG_PORT_HANDLE h = sdev->port_handle;

    while ( dwTotalBytesWritten < (DWORD) count )
    {
      dwThisBytesWritten = 0;

      fWriteStat = WriteFile( h, ( (char *) buffer ) + dwTotalBytesWritten,
                              count - dwTotalBytesWritten,
                              &dwThisBytesWritten, NULL );
      if ( !fWriteStat )
        return mbg_get_last_error( "write failed in mbgserio_write" );

      dwTotalBytesWritten += dwThisBytesWritten;

      ClearCommError( h, &dwErrorFlags, &ComStat );

      if ( dwErrorFlags )
      {
        // Possible errors:
        //
        // CE_RXOVER    0x0001  // Receive Queue overflow
        // CE_OVERRUN   0x0002  // Receive Overrun Error, next character(s) lost
        // CE_RXPARITY  0x0004  // Receive Parity Error
        // CE_FRAME     0x0008  // Receive Framing error
        // CE_BREAK     0x0010  // Break Detected
        // CE_TXFULL    0x0100  // TX Queue is full
        // CE_PTO       0x0200  // LPTx Timeout
        // CE_IOE       0x0400  // LPTx I/O Error
        // CE_DNS       0x0800  // LPTx Device not selected
        // CE_OOP       0x1000  // LPTx Out-Of-Paper
        // CE_MODE      0x8000  // Requested mode unsupported
        //
        // Except CE_TXFULL these are usually RX error flags,
        // so we just ignore them.
      }
    }

    return dwTotalBytesWritten;

  #elif defined( MBG_TGT_POSIX )

    int rc = write( sdev->port_handle, buffer, count );  // Returns -1 on error, else number of byte written.

    if  ( rc == -1 )
      rc = mbg_get_last_error( "write failed in mbgserio_write" );

    return rc;

  #elif defined( MBG_TGT_DOS ) && defined( _USE_V24TOOLS )

    int rc = v24write( sdev->port_handle, buffer, count );  // TODO Check or translate return code.

    if ( rc < 0 )
      rc = MBG_ERR_UNSPEC;

    return rc;

  #else

    #error mbgserio_write() not implemented for this target

  #endif

}  // mbgserio_write



/*HDR*/
/**
 * @brief Flush the transmit buffer of a serial port.
 *
 * @param[in,out]  sdev  Control structure of the device.
 */
_NO_MBG_API_ATTR void _MBG_API mbgserio_flush_tx( MBGSERIO_DEV *sdev )
{
  #if defined( MBG_TGT_CVI )

    FlushOutQ( sdev->port_handle );

  #elif defined( MBG_TGT_WIN32 )

    #if defined( DEBUG )
      printf( "Flushing TX buffers ...\n" );
    #endif

    FlushFileBuffers( sdev->port_handle );

    #if ( 1 && defined( DEBUG ) ) // TODO
    {
      COMSTAT ComStat;
      DWORD dwErrorFlags;

      for (;;)
      {
        ClearCommError( sdev->port_handle, &dwErrorFlags, &ComStat );

        printf( "ErrFlags: %04X, out queue: %u\n", dwErrorFlags, ComStat.cbOutQue );

        if ( ComStat.cbOutQue == 0 )
          break;

        Sleep( 1 );
      }
    }
    #endif

  #elif defined( MBG_TGT_POSIX )

    tcdrain( sdev->port_handle );

  #elif defined( MBG_TGT_DOS ) && defined( _USE_V24TOOLS )

    v24flush( sdev->port_handle, SND );

  #else

    #error mbgserio_flush_tx() not implemented for this target

  #endif

}  // mbgserio_flush_tx



/*HDR*/
/**
 * @brief Get the current poll timeout count, in milliseconds.
 *
 * Used for timeout in ::mbgserio_read_wait.
 *
 * @param[in]  sdev  Control structure of the device.
 *
 * @return  The current timeout value, in milliseconds.
 *
 * @see ::mbgserio_set_dev_poll_timeout
 * @see ::mbgserio_read_wait
 */
ulong mbgserio_get_dev_poll_timeout( const MBGSERIO_DEV *sdev )
{
  return sdev->poll_timeout;

}  // mbgserio_get_dev_poll_timeout



/*HDR*/
/**
 * @brief Set a new poll timeout, in milliseconds.
 *
 * Used for timeout in ::mbgserio_read_wait.
 *
 * @param[in,out]  sdev         Control structure of the device.
 * @param[in]      new_timeout  New timeout, in milliseconds.
 *
 * @return  The previous timeout value, in milliseconds.
 *
 * @see ::mbgserio_get_dev_poll_timeout
 * @see ::mbgserio_read_wait
 */
ulong mbgserio_set_dev_poll_timeout( MBGSERIO_DEV *sdev, ulong new_timeout )
{
  ulong prev_timeout = mbgserio_get_dev_poll_timeout( sdev );

  sdev->poll_timeout = new_timeout;

  return prev_timeout;

}  // mbgserio_set_dev_poll_timeout



/*HDR*/
/**
 * @brief Wait for data and read from a serial port.
 *
 * @param[in,out]  sdev    Control structure of the device.
 * @param[out]     buffer  Buffer to take the bytes read.
 * @param[in]      count   Size of @p buffer.
 *
 * @return On Success, the (positive) number of byte read,
 *         or one of the (negative) @ref MBG_ERROR_CODES on error.
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_read_wait( MBGSERIO_DEV *sdev, void *buffer,
                                                  unsigned int count )
{
  int rc;

  #if _USE_SELECT_FOR_SERIAL_IO  // Should be used preferably.

    struct timeval tv_char_timeout;
    fd_set fds;
    int max_fd;

    mbg_msec_to_timeval( sdev->poll_timeout, &tv_char_timeout );

    FD_ZERO( &fds );
    FD_SET( sdev->port_handle, &fds );

    #if defined( MBG_TGT_WIN32 )
      // On Windows, an fd is a handle which can't simply
      // be converted to an int, but the first argument of
      // select() is ignored on Windows anyway, so we just
      // set max_fd to 0.
      max_fd = 0;
    #else
      max_fd = sdev->port_handle;
    #endif

    rc = select( max_fd + 1, &fds, NULL, NULL, &tv_char_timeout );

    if ( rc < 0 )     // Error.
      return mbg_get_last_error( "select failed in mbgserio_read_wait" );

    if ( rc == 0 )    // Timeout.
      return MBG_ERR_TIMEOUT;

    // Data is available.
    rc = mbgserio_read( sdev, buffer, count );

  #else

    MBG_TMO_TIME tmo;

    mbg_tmo_set_timeout_ms( &tmo, sdev->poll_timeout );

    for (;;)  // wait to read one new char
    {
      rc = mbgserio_read( sdev, buffer, count );

      if ( rc != 0 )     // Byte(s) received, or error.
        break;

      if ( mbg_tmo_curr_time_is_after( &tmo ) )
        return MBG_ERR_TIMEOUT;

      #if defined( MBG_TGT_POSIX )
        usleep( 10 * 1000 );
      #endif
    }
  #endif

  return rc;

}  // mbgserio_read_wait



/*HDR*/
/**
 * @brief Compute the time required to transmit a number of bytes, in nanoseconds.
 *
 * Depends on the baud rate and the effective number of
 * bits per byte, i.e. data / parity / stop bits.
 * Assuming the transmission is without gaps.
 *
 * If 2 stobits are used, this differs from ::mbgserio_get_duration_rcv.
 * See ::mbgserio_get_duration_rcv.
 *
 * @param[in]  p_sp     The relevant parameters of the transmission.
 * @param[in]  n_bytes  The number of bytes to transmit.
 *
 * @return  The the computed duration, in nanoseconds.
 *
 * @see ::mbgserio_get_duration_rcv
 */
_NO_MBG_API_ATTR int32_t _MBG_API mbgserio_get_duration_xmt( MBGSERIO_SPEED_PARMS *p_sp, int n_bytes )
{
  // Total number of bits, including start bit.
  int n_bits = n_bytes * ( 1 + p_sp->data_bits + p_sp->parity_bits + p_sp->stop_bits );

  return (int32_t) ( ( (int64_t) n_bits * NSEC_PER_SEC ) / p_sp->baud_rate );

}  // mbgserio_get_duration_xmt



/*HDR*/
/**
 * @brief Compute the receive delay for a number of bytes, in nanoseconds.
 *
 * Depends on the baud rate and the effective number of
 * bits per byte, i.e. data / parity / stop bits.
 * Assuming the transmission is without gaps.
 *
 * Differs from ::mbgserio_get_duration_xmt in case there are
 * 2 stop bits. This is because because UARTS usually flag
 * reception of a byte after the first stop bit, even if
 * 2 stop bits are used for transmission.
 *
 * @param[in]  p_sp     The relevant parameters of the transmission.
 * @param[in]  n_bytes  The number of bytes to receive.
 * @param[in]  n_bits   A number of additional bits to account for.
 *
 * @return  The the computed duration, in nanoseconds.
 *
 * @see ::mbgserio_get_duration_xmt
 */
_NO_MBG_API_ATTR int32_t _MBG_API mbgserio_get_duration_rcv( const MBGSERIO_SPEED_PARMS *p_sp,
                                                             int n_bytes, int n_bits )
{
  // Total number of bits, including start bit.
  int n_bits_total = n_bytes * ( 1 + p_sp->data_bits + p_sp->parity_bits + p_sp->stop_bits + n_bits );

  // If 2 stop bits are used for transmission, we
  // subtract 1 stop bit from the total number of bits.
  if ( p_sp->stop_bits > 1 )
    n_bits_total--;

  return (int32_t) ( ( (int64_t) n_bits_total * NSEC_PER_SEC ) / p_sp->baud_rate );

}  // mbgserio_get_duration_rcv



#if USE_LINUX_DEV_DIR

static /*HDR*/
int scandir_filter_serial_port( const struct dirent *pde )
{
  static const char dev_name_1[] = "ttyS";
  static const char dev_name_2[] = "ttyUSB";
  char tmp_str[100];
  MBGSERIO_DEV *sdev;
  int rc;

  int l;

  l = strlen( dev_name_1 );

  if ( strncmp( pde->d_name, dev_name_1, l ) == 0 )
    goto check;


  l = strlen( dev_name_2 );

  if ( strncmp( pde->d_name, dev_name_2, l ) == 0 )
    goto check;

  return 0;

check:
  // If the first character after the search string is not a digit,
  // the search result is not what we want.
  if ( pde->d_name[l] < '0' || pde->d_name[l] > '9' )
    return 0;

  snprintf_safe( tmp_str, sizeof( tmp_str ), "%s/%s", dev_dir, pde->d_name );

  rc = mbgserio_open( &sdev, tmp_str );

  if ( rc < 0 )
  {
    #if defined( DEBUG )
      fprintf( stderr, "failed to open %s: %i\n", tmp_str, rc );
    #endif

    return 0;
  }

  #if defined( DEBUG )
    fprintf( stderr, "port %s opened successfully\n", tmp_str );
  #endif

  mbgserio_close( &sdev );

  return 1;

}  // scandir_filter_serial_port

#endif  // USE_LINUX_DEV_DIR



/*HDR*/
/**
 * @brief Set up a string list with names of available serial ports.
 *
 * @param[out]  list      Address of the header of a list to be set up.
 * @param[in]   max_devs  Maximum number of list entries to create.
 *
 * @return  The number of entries in the list.
 *
 * @see ::mbgserio_free_str_list
 */
_NO_MBG_API_ATTR int _MBG_API mbgserio_setup_port_str_list( MBG_STR_LIST **list, int max_devs )
{
  MBG_STR_LIST *list_head;
  int n = 0;
  int i = 0;

  (*list) = (MBG_STR_LIST *) malloc( sizeof( **list ) );
  memset( (*list), 0, sizeof( **list ) );

  list_head = (*list);


#if 0 && USE_LINUX_DEV_DIR

  struct dirent **namelist;

  n = scandir( dev_dir, &namelist, scandir_filter_serial_port, versionsort );

  if ( n < 0 )
    perror( "scandir" );
  else
  {
    for ( i = 0; i < n; i++ )
    {
      printf( "%s/%s\n", dev_dir, namelist[i]->d_name );
      free( namelist[i] );
    }

    free( namelist );
  }

#elif 0 && USE_LINUX_DEV_DIR

  DIR *pd = opendir( dev_dir );

  if ( pd )
  {
    struct dirent *pde;

    while ( ( pde = readdir( pd ) ) != NULL )
    {
      if ( strncmp( pde->d_name, "ttyS", 4 ) == 0 )
        goto found;

      if ( strncmp( pde->d_name, "ttyUSB", 6 ) == 0 )
        goto found;

      continue;

found:
      fprintf( stderr, "found /dev/%s\n", pde->d_name );
    }

    closedir( pd );
  }

#else

  for ( i = 0; i < max_devs; i++ )
  {
    char dev_name[100] = { 0 };
    int rc;

    #if defined( MBG_TGT_WIN32 )

      HANDLE h = INVALID_HANDLE_VALUE;

      snprintf_safe( dev_name, sizeof( dev_name ), "COM%i", i + 1 );
      rc = win32_open_serial_port( dev_name, &h );

      // If access is denied, the port exists but is in use.
      if ( rc != MBG_SUCCESS && rc != MBG_ERR_ACCESS )
        continue;     // Memory allocation error.

      if ( h != INVALID_HANDLE_VALUE )
        CloseHandle( h );

    #elif defined( MBG_TGT_LINUX )

      MBGSERIO_DEV *sdev;
      snprintf_safe( dev_name, sizeof( dev_name ), "/dev/ttyS%i", i );
      rc = mbgserio_open( &sdev, dev_name );

      if ( rc < 0 )
      {
        snprintf_safe( dev_name, sizeof( dev_name ), "/dev/ttyUSB%i", i );

        rc = mbgserio_open( &sdev, dev_name );

        if ( rc < 0 )
          continue;
      }

      mbgserio_close( &sdev );

    #endif


    // Add the device name to the list.

    (*list)->s = (char *) malloc( strlen( dev_name ) + 1 );
    strcpy( (*list)->s, dev_name );

    (*list)->next = (MBG_STR_LIST *) malloc( sizeof( **list ) );
    (*list) = (*list)->next;

    memset( (*list), 0, sizeof( **list ) );
    n++;

    // TODO Do we want or need this?
    //
    // if ( ++i >= N_SUPP_DEV_EXT )
    //   break;
  }

  if ( n == 0 )
  {
    free( *list );
    list_head = NULL;
  }
#endif

  *list = list_head;

  return n;

}  // mbgserio_setup_port_str_list



/*HDR*/
/**
 * @brief Free a string list that has been allocated before.
 *
 * @param[out]  list  Address of the string list to be freed.
 *
 * @see ::mbgserio_setup_port_str_list
 */
_NO_MBG_API_ATTR void _MBG_API mbgserio_free_str_list( MBG_STR_LIST *list )
{
  int i = 0;

  while ( i < 1000 )  // FIXME TODO Why 1000 ?
  {
    if ( list )
    {
      if ( list->s )
      {
        free( list->s );
        list->s = NULL;
      }

      if ( list->next )
      {
        MBG_STR_LIST *next = list->next;
        free( list );
        list = next;
      }
      else
      {
        if ( list )
        {
          free( list );
          list = NULL;
        }
        break;
      }
    }
    else
      break;

    i++;
  }

}  // mbgserio_free_str_list

