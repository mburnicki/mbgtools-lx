
/**************************************************************************
 *
 *  $Id: mbg_daemonize.c 1.8 2021/04/28 19:54:43 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Generic functions to fork a process as daemon.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_daemonize.c $
 *  Revision 1.8  2021/04/28 19:54:43  martin
 *  Use dup2() to duplicate the fake file descriptors for stdout and stderr
 *  after forking. See the notes in close_files_and_redirect_stdio().
 *  Revision 1.7  2021/03/26 09:50:42  martin
 *  Function mbg_daemonize() now expects a pointer to an optional
 *  function that is called after the fork to register an own signal
 *  handler instead of the default one.
 *  The function mbg_pidfile_write_own_pid() isn't called anymore,
 *  this should be done by the calling application.
 *  Do logging via a function pointer that defaults to syslog()
 *  but can be changed to point to a different function.
 *  Updated lots of comments.
 *  Revision 1.6  2017/05/30 16:02:24  martin
 *  Quieted clang warning.
 *  Revision 1.5  2014/04/15 13:39:36  martin
 *  Added #define _GNU_SOURCE, required for old glibc versions.
 *  Revision 1.4  2013/07/30 15:30:49  martin
 *  Revision 1.3  2013/07/30 12:53:49  martin
 *  Revision 1.2  2013/07/24 15:29:38  martin
 *  Revision 1.1  2013/07/23 16:10:18  martin
 *  Initial revision.
 *
 **************************************************************************/

#if !defined( _GNU_SOURCE )
  #define _GNU_SOURCE  1    // Required for strsignal() in old glibc versions.
#endif

#define _MBG_DAEMONIZE
  #include <mbg_daemonize.h>
#undef _MBG_DAEMONIZE

#include <mbg_pidfile.h>

#include <unistd.h>
#include <stdlib.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>


static /*HDR*/
void close_files_and_redirect_stdio( void )
{
  int i;

  #if defined( DEBUG )
    mbg_daemonize_log_fnc( LOG_INFO, "Going to close all file handles, table size: %i, PID fd: %i",
             getdtablesize(), glb_pid_fd );
  #endif

  // Close all file descriptors, except the PID file descriptor.

  for ( i = getdtablesize(); i >= 0; --i )
  {
    if ( i != glb_pid_fd )
    {
      int rc = close( i );

      if ( rc < 0 )
      {
      #if !defined( DEBUG ) || ( DEBUG < 2 )
        if ( errno != EBADF )
      #endif
          mbg_daemonize_log_fnc( LOG_INFO, "Failed to close fd %i: %s", i, strerror( errno ) );
      }
      else
      {
      #if defined( DEBUG )
        mbg_daemonize_log_fnc( LOG_INFO, "fd %i closed successfully", i );
      #endif
      }
    }
    else
    {
      #if defined( DEBUG )
        mbg_daemonize_log_fnc( LOG_INFO, "Not closing PID fd %i", i );
      #endif
    }
  }

  // Handle standard I/O.
  //
  // Use dup2() to duplicate the fake file descriptors for stdout and stderr.
  // When dup() is used, the fd for stderr could be left unassigned, so a
  // subsequent regular open() could return fd 3, unless e.g. a syslog()
  // had been made before (e.g. in debug mode).

  #if defined( DEBUG )
    mbg_daemonize_log_fnc( LOG_INFO, "Going to open /dev/null as stdin, stdout, and stderr" );
  #endif

  if ( ( i = open( "/dev/null", O_RDWR ) ) == -1 )   // stdin
    mbg_daemonize_log_fnc( LOG_ERR, "Failed to open /dev/null as stdin: %s", strerror( errno ) );

  if ( dup2( i, 2 ) == -1 )                          // stdout
    mbg_daemonize_log_fnc( LOG_ERR, "Failed dup stdin as stdout: %s", strerror( errno ) );

  if ( dup2( i, 3 ) == -1 )                          // stderr
    mbg_daemonize_log_fnc( LOG_ERR, "Failed dup stdin as stderr: %s", strerror( errno ) );

}  // close_files_and_redirect_stdio



static /*HDR*/
void set_running_dir( const char *new_dir )
{
  // Change to new running dir.
  int rc = chdir( new_dir );

  if ( rc == -1 )
    mbg_daemonize_log_fnc( LOG_WARNING, "Failed to chdir to \"%s\": %s", new_dir, strerror( errno ) );
  else
  {
    #if defined( DEBUG )
      mbg_daemonize_log_fnc( LOG_INFO, "Successfully called chdir to \"%s\"", new_dir );
    #endif
  }

}  // set_running_dir



static /*HDR*/
void set_signal_handling( MBG_SIG_HANDLER_FNC new_signal_handler,
                          MBG_REG_SIG_HANDLER_FNC *reg_sig_handler_func )
{
  struct sigaction new_sig_action;
  sigset_t new_sig_set;

  // Set signal mask - signals we want to block.
  sigemptyset( &new_sig_set );
  sigaddset( &new_sig_set, SIGCHLD );  // Ignore child, i.e. we don't need to wait for it.
  sigaddset( &new_sig_set, SIGTSTP );  // Ignore TTY stop signals.
  sigaddset( &new_sig_set, SIGTTOU );  // Ignore TTY background writes.
  sigaddset( &new_sig_set, SIGTTIN );  // Ignore TTY background reads.
  sigprocmask( SIG_BLOCK, &new_sig_set, NULL );  // Block the signals specified above.

  #if defined( DEBUG )
    mbg_daemonize_log_fnc( LOG_INFO, "Blocked some signals (pid %i)", getpid() );
  #endif


  // Set up a signal handler.
  if ( reg_sig_handler_func )
  {
    // Call the external signal handler function
    // that has been provided.
    reg_sig_handler_func();
  }
  else
  {
    // Register the default signal handler.
    new_sig_action.sa_handler = new_signal_handler;
    sigemptyset( &new_sig_action.sa_mask );
    new_sig_action.sa_flags = 0;

    // Signals to handle.
    sigaction( SIGHUP, &new_sig_action, NULL );   // Catch hangup signal.
    sigaction( SIGTERM, &new_sig_action, NULL );  // Catch term signal.
    sigaction( SIGINT, &new_sig_action, NULL );   // Catch interrupt signal.
  }

  #if defined( DEBUG )
    mbg_daemonize_log_fnc( LOG_INFO, "Set some signals to be handled (pid %i)", getpid() );
  #endif

}  // set_signal_handling



static /*HDR*/
void signal_handler( int sig )
{
  pid_t pid = getpid();

  switch ( sig )
  {
    case SIGHUP:
      mbg_daemonize_log_fnc( LOG_WARNING, "Received SIGHUP signal (pid %i).", pid );
      break;

    case SIGINT:
    case SIGTERM:
      mbg_daemonize_log_fnc( LOG_INFO, "Received %s (pid %i), exiting.", strsignal( sig ), pid );
      exit( EXIT_SUCCESS );
      break;

    default:
      mbg_daemonize_log_fnc( LOG_WARNING, "Unhandled signal %s (pid %i)", strsignal( sig ), pid );
      break;

  }  // switch

}  // signal_handler

MBG_SIG_HANDLER_FNC signal_handler;



/*HDR*/
void mbg_daemonize( const char *running_dir, MBG_REG_SIG_HANDLER_FNC *reg_sig_handler_func )
{
  int rc;

  if ( getppid() == 1 )    // Already a daemon.
  {
    mbg_daemonize_log_fnc( LOG_WARNING, "Already daemon with PID %i when %s was called",
                           getpid(), __func__ );
    return;
  }

  set_signal_handling( signal_handler, reg_sig_handler_func );

  rc = fork();

  if ( rc < 0 )            // Failed to fork.
  {
    mbg_daemonize_log_fnc( LOG_ERR, "Failed to fork: %s", strerror( errno ) );
    exit( EXIT_FAILURE );
  }

  if ( rc > 0 )            // We are the parent process.
  {
    #if defined( DEBUG )
      mbg_daemonize_log_fnc( LOG_INFO, "Child process %i created successfully, parent %i exiting now",
                             rc, getpid() );
    #endif
    exit( EXIT_SUCCESS );  // Parent process exits now.
  }

  // rc == 0: We are the child process that continues.

  rc = setsid();            // Get a new process group.

  if ( rc < 0 )             // Error.
  {
    mbg_daemonize_log_fnc( LOG_ERR, "Failed to get new session ID: %s", strerror( errno ) );
    exit( EXIT_FAILURE );
  }

  #if defined( DEBUG )
    mbg_daemonize_log_fnc( LOG_INFO, "Got new session ID %i", rc );
  #endif

  close_files_and_redirect_stdio();

  // TODO Check if this should be done earlier, right after fork.
  umask( 027 );    // Restrict file permissions to 0750.

  set_running_dir( running_dir ? running_dir : DEFAULT_RUNNING_DIR );

}  // mbg_daemonize

