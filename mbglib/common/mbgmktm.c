
/**************************************************************************
 *
 *  $Id: mbgmktm.c 1.4 2019/09/27 14:39:27 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Function to convert broken down time to Unix time (seconds since 1970)
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgmktm.c $
 *  Revision 1.4  2019/09/27 14:39:27  martin
 *  Code cleanup and doxygen updates.
 *  Revision 1.3  2019/08/21 11:07:13  martin
 *  ATTENTION: Calling conventions have changed!
 *  Renamed mbg_mktime() to mbg_mktime64(), which always computes a
 *  64 bit timestamp and expects the address of a 64 bit variable to
 *  take the result. Unlike mktime(), this function now provides a
 *  proper return code indicating if the conversion was successful,
 *  or not.
 *  Also, the 'day' parameter now has to be in the same range as for
 *  POSIX mktime(), i.e. 1..31, instead of 0..30 as in the previous
 *  implementation of mbg_mktime. This also has to be taken into
 *  account wherever this function is called.
 *  The header file provides an inline function mbg_mktime() which
 *  is similar to mbg_mktime64() but expects the address of a POSIX
 *  time_t for the result, which can be 32 bit only on some systems,
 *  so the mbg_mktime64() function should be used preferably, if
 *  64 bit timestamps are used anyway.
 *  There are also new inline functions mbg_mktime64_from_tm() and
 *  mbg_mktime_from_tm(), which call the associated generic functions
 *  but expect a standard POSIX struct tm to provide the date and
 *  time to be converted.
 *  Revision 1.2  2017/07/05 10:00:29  martin
 *  Let mbg_mktime() fail if year is out of a range which depends on
 *  the size of the 'time_t' type provided by the build environment.
 *  Allow 0 as valid return value for mbg_mktime().
 *  Added some doxygen comments.
 *  Revision 1.1  2006/08/22 08:57:15  martin
 *  Former function totalsec() moved here from pcpsmktm.c.
 *
 **************************************************************************/

#define _MBGMKTM
 #include <mbgmktm.h>
#undef _MBGMKTM

#include <mbgerror.h>

#include <stdlib.h>



static const char days_per_month[MONTHS_PER_YEAR] =
{
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

static int ydays_per_month[MONTHS_PER_YEAR] =
{
  0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};



/*HDR*/
/**
 * @brief Compute a linear ::MBG_TIME64_T value from broken down date and time
 *
 * This function basically works like the POSIX @a mktime function but
 * doesn't apply a local time offset which depends on some timezone setting.
 *
 * Unlike @a mktime, it expects the address of a variable of type
 * ::MBG_TIME64_T, which is always 64 bits wide, even on systems
 * where @a time_t is only 32 bits wide.
 *
 * Also, it does not expect a structure, but a set of variables, which
 * makes it more versatile. The expected values are in the same ranges
 * as the members of the POSIX <em>struct tm</em> type used by @a mktime.
 *
 * Another difference is that unlike POSIX @a mktime, which simply returns
 * <em>(time_t) -1</em> in case of error, this function provides a
 * return code indicating if the calulation succeeded or not,
 * and @a -1 can be a valid timestamp indicating a time associated with
 * one second before the epoch.
 *
 * There are also some variants, implemented as inline functions.
 * See @ref mbg_mktime_fncs.
 *
 * @param[out]  p_t64  Address of a variable to take the result on success,
 *                     i.e. seconds since 1970-01-01 (POSIX @a time_t format,
 *                     but always 64 bits).
 * @param[in]   year   Years since 1900, i.e current year number - 1900.
 * @param[in]   month  Months since January, 0..11
 * @param[in]   day    Day of the month, 1..31
 * @param[in]   hour   0..23
 * @param[in]   min    0..59
 * @param[in]   sec    0..59, or 60 if inserted leap second
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int mbg_mktime64( MBG_TIME64_T *p_t64, int year, int month, int day,
                  int hour, int min, int sec )
{
  int rc = MBG_SUCCESS;
  int leaps;
  long days;  // type must be > 16 bits

#define USE_DIV  1

#if USE_DIV
  div_t dt;
#endif

  // The day parameter is expected to be in the same range as
  // for POSIX mktime(), i.e. 1..31. However, the algorithm below
  // expects 0 for the first day of a month, so we just
  // decrement the day number.
  day--;

#if USE_DIV

  // normalize seconds
  dt = div( sec, SECS_PER_MIN );
  min += dt.quot;
  sec = dt.rem;

  // normalize minutes
  dt = div( min, MINS_PER_HOUR );
  hour += dt.quot;
  min = dt.rem;

  // normalize hours
  dt = div( hour, HOURS_PER_DAY );
  day += dt.quot;
  hour = dt.rem;

  // normalize month (not necessarily finally)
  dt = div( month, MONTHS_PER_YEAR );
  year += dt.quot;
  month = dt.rem;

#else

  // normalize seconds
  min += sec / SECS_PER_MIN;
  sec %= SECS_PER_MIN;

  // normalize minutes
  hour += min / MINS_PER_HOUR;
  min %= MINS_PER_HOUR;

  // normalize hours
  day += hour / HOURS_PER_DAY;
  hour %= HOURS_PER_DAY;

  // normalize month (not necessarily finally)
  year += month / 12;
  month %= 12;

#endif

  while ( day >= days_per_month[month] )
  {
    if ( !( year & 3 ) && ( month == 1 ) )
    {
      if ( day > 28 )
      {
        day -= 29;
        month++;
      }
      else
        break;
    }
    else
    {
      day -= days_per_month[month];
      month++;
    }

    year += month / 12;  // normalize month
    month %= 12;
  }

  year -= 70;
  leaps = ( year + 2 ) / 4;

  if ( !( ( year + 70 ) & 3 ) && ( month < 2 ) )
    --leaps;

  days = year * 365L + leaps + ydays_per_month[month] + day;

  *p_t64 = (MBG_TIME64_T) days * SECS_PER_DAY + hour * SECS_PER_HOUR + min * SECS_PER_MIN + sec;

  // rc is still MBG_SUCCESS at this point.

  return rc;

}  // mbg_mktime64

