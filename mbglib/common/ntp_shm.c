
/**************************************************************************
 *
 *  $Id: ntp_shm.c 1.4 2021/06/25 14:20:18 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    NTP shared memory support functions.
 *
 * -----------------------------------------------------------------------
 *  $Log: ntp_shm.c $
 *  Revision 1.4  2021/06/25 14:20:18  martin
 *  Tiny changes to some message texts.
 *  Revision 1.3  2021/03/23 20:13:24  martin
 *  Call the logging function via a function pointer that defaults to
 *  syslog() but can be changed to point to an application-specific function.
 *  Also updated some comments.
 *  Revision 1.2  2017/07/05 16:49:00  martin
 *  Patch submitted by <juergen.perlinger@t-online.de>:
 *  Support number of the first SHM unit to use.
 *  This intentionally changes the prototype for ntpshm_init().
 *  Revision 1.1  2012/05/29 09:54:15  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _NTP_SHM
  #include <ntp_shm.h>
#undef _NTP_SHM

#include <errno.h>
#include <string.h>



/*HDR*/
struct shmTime *getShmTime( int unit )
{
  struct shmTime *p;

  int shmid = shmget( (key_t) ( NTPD_BASE + unit ),
                      sizeof( struct shmTime ), IPC_CREAT | 0644 );

  if ( shmid == -1 )
  {
    ntp_shm_log_fnc( LOG_ERR, "shmget %i failed: %s", unit, strerror( errno ) );
    return NULL;
  }


  p = (struct shmTime *) shmat( shmid, 0, 0 );

  if ( (long) p == -1L )
  {
    ntp_shm_log_fnc( LOG_ERR, "shmat %i failed: %s", unit, strerror( errno ) );
    return NULL;
  }

  return p;

}  // getShmTime



/*HDR*/
int ntpshm_init( struct shmTime **shmTime, int n_units, int n_unit0 )
{
  int i;
  int ret_val = 0;

  for ( i = 0; i < n_units; i++ )
  {
    int u = i + n_unit0;
    struct shmTime *p = getShmTime( u );
    shmTime[i] = p;

    if ( p == NULL )
    {
      ntp_shm_log_fnc( LOG_WARNING, "** Failed to initialize SHM unit %i", u );
      ret_val = -1;
      continue;
    }

    memset( p, 0, sizeof( *p ) );

    p->mode = 1;
    p->precision = -5;  // Initially 0.5 sec
    p->nsamples = 3;    // Stages of median filter.

    ntp_shm_log_fnc( LOG_INFO, "SHM unit %i initialized successfully", u );
  }

  return ret_val;

}  // ntpshm_init


