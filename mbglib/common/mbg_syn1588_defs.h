
/**************************************************************************
 *
 *  $Id: mbg_syn1588_defs.h 1.2 2022/07/06 15:09:08 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions for the SYN1588 API.
 *
 *  The original definitions are distributed and hidden in the C++
 *  source code which is not very portable, so some definitions have
 *  been duplicated here.
 *
 *  Of course this is dangerous because care must be taken that the
 *  original and duplicated definitions yield the same results, but
 *  it makes maintenance of an external projects very much easier.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_syn1588_defs.h $
 *  Revision 1.2  2022/07/06 15:09:08  martin.burnicki
 *  Support SYN1588  insync range boundary code instead of the original enum.
 *  Revision 1.1  2022/05/31 13:41:42  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_SYN1588_DEFS_H
#define _MBG_SYN1588_DEFS_H

/* Other headers to be included */

#include <words.h>


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif



/**
 * @brief The minimum required SYN1588 NIC build number.
 *
 * The ::PTP_SYNC_STATUS register is only supported by SYN1588 PCIe NICs
 * running firmware versions with build number 867 or later.
 *
 * Also, the PTP stack has to be current enough to populate the status
 * register. Version 1.14 of the stack, which does this, requires
 * firmware build 876 or later for the NIC, or it fails to read the
 * hardware timestamps from the card.
 *
 * So this is the smallest firmware build number required
 * for the SYN1588 PCIe NIC.
 */
#define SYN1588_MIN_REQ_BUILD_NUM  876



/**
 * @brief Type of a bit-coded PTP sync status.
 *
 * Can be read from the ::PTP_SYNC_STATUS register
 * of a SYN1588 PCI NIC.
 *
 * @note Requires firmware ::SYN1588_MIN_REQ_BUILD_NUM or later.
 *
 * @see @ref SYN1588_PTP_SYNC_STATUS_DEFS
 */
typedef uint32_t SYN1588_PTP_SYNC_STATUS;



/**
 * @brief Bit masks and fields used with ::SYN1588_PTP_SYNC_STATUS.
 *
 * The original definitions of these numbers are only compatible
 * with C++, but not with standard C, so some associated, portable
 * definitions are provided here as workaround.
 *
 * @see ::SYN1588_PTP_SYNC_STATUS
 * @see ::PTP_SYNC_STATUS
 *
 * @anchor SYN1588_PTP_SYNC_STATUS_DEFS @{ */

#define DEF_PSS_BITMASK_UTC_OFFSET          0x000000FF  ///< Bitfield mask for UTC Offset in [s], see ::PSS_BITMASK_UTC_OFFSET
#define DEF_PSS_BIT_UTC_OFFSET_VALID        0x00000100  ///< Bit signalling of UTC Offset is valid or not, see ::PSS_BIT_UTC_OFFSET_VALID
#define DEF_PSS_BIT_LEAP59                  0x00000200  ///< Bit signalling an upcoming negative leap second, see ::PSS_BIT_LEAP59
#define DEF_PSS_BIT_LEAP61                  0x00000400  ///< Bit signalling an upcoming positive leap second, see ::PSS_BIT_LEAP61
#define DEF_PSS_BITMASK_PTP_STATE           0x00007800  ///< Bitfield mask for the field representing the PTP State, see ::PSS_BITMASK_PTP_STATE
#define DEF_PSS_SHIFT_PTP_STATE             11          ///< Shift offset for the field representing the PTP State, see ::PSS_SHIFT_PTP_STATE
#define DEF_PSS_BIT_PTP_INSYNC              0x00008000  ///< Bit signalling that the PTP Stack is in-sync, see ::PSS_BIT_PTP_INSYNC
#define DEF_PSS_BITMASK_PTP_INSYNC_BND_CODE 0x003F0000  ///< Bitfield mask for the code indicating the insync boundary, see ::PTP_STATE_RANGE_TABLE_INIT and::PSS_BITMASK_PTP_INSYNC_BND_CODE
#define DEF_PSS_SHIFT_PTP_INSYNC_BND_CODE   16          ///< Shift offset for the field representing the in sync boundary, see ::PSS_SHIFT_PTP_INSYNC_BND_CODE
#define DEF_PSS_BIT_PTP_TIMESCALE           0x00400000  ///< Bit signalling the current PTP Timescale ('1' = PTP, '0' = ARB), see ::PSS_BIT_PTP_TIMESCALE
#define DEF_PSS_BIT_LSYNC_RUNNING           0x40000000  ///< Bit signalling that lSync is active, see ::LSS_BIT_LSYNC_RUNNING
#define DEF_PSS_BIT_VALID                   0x80000000  ///< Bit signalling that the register content is valid, see ::PSS_BIT_VALID

/** @} anchor MBG_DEBUG_STATUS_BIT_MASKS */



/**
 * @brief Possible states of the PTP stack.
 *
 * Reported by the ::DEF_PSS_BITMASK_PTP_STATE bit field
 * of the ::SYN1588_PTP_SYNC_STATUS type.
 *
 * @see ::SYN1588_PTP_STATE_STRS for an initializer for name strings.
 */
enum SYN1588_PTP_STATES
{
  SYN1588_PTP_STATE_UNDEFINED,     ///< Undefined state.
  SYN1588_PTP_STATE_INITIALIZING,  ///< Initializing, see ::SInitializing.
  SYN1588_PTP_STATE_FAULTY,        ///< Faulty, see ::SFaulty.
  SYN1588_PTP_STATE_DISABLED,      ///< Disabled, see ::SDisabled.
  SYN1588_PTP_STATE_LISTENING,     ///< Listening, see ::SListening.
  SYN1588_PTP_STATE_PRE_MASTER,    ///< Pre-Master, see ::SPreMaster.
  SYN1588_PTP_STATE_MASTER,        ///< Master, see ::SMaster.
  SYN1588_PTP_STATE_PASSIVE,       ///< Passive, see ::SPassive.
  SYN1588_PTP_STATE_UNCALIBRATED,  ///< Uncalibrated, see ::SUncalibrated.
  SYN1588_PTP_STATE_SLAVE,         ///< Slave, see ::SSlave.
  N_SYN1588_PTP_STATES             ///< Number of defined states, see ::SMaxStateArray.
};


/**
 * @brief Initializer for an array of PTP state name strings.
 *
 * The array that is initialized should have ::N_SYN1588_PTP_STATES entries.
 *
 * @see ::SYN1588_PTP_STATES.
 */
#define SYN1588_PTP_STATE_STRS \
{                              \
  "Undefined",                 \
  "Initializing",              \
  "Faulty",                    \
  "Disabled",                  \
  "Listening",                 \
  "PreMaster",                 \
  "Master",                    \
  "Passive",                   \
  "Uncalibrated",              \
  "Slave"                      \
}


#ifdef __cplusplus
}
#endif


/* End of header body */

#endif  /* _MBG_SYN1588_DEFS_H */
