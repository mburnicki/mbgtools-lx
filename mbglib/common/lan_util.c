
/**************************************************************************
 *
 *  $Id: lan_util.c 1.28 2023/03/21 16:00:26 kai.heine REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Utility functions useful for network programming.
 *
 * -----------------------------------------------------------------------
 *  $Log: lan_util.c $
 *  Revision 1.28  2023/03/21 16:00:26  kai.heine
 *  Fixed conversion of double colons in str_to_ip6_addr
 *  Revision 1.27  2023/03/03 14:37:08  kai.heine
 *  Fixed buffer underflow in str_to_ip6_addr in case addr end with colons
 *  Revision 1.26  2022/12/21 14:54:41  martin.burnicki
 *  Quieted another potential compiler warning.
 *  Revision 1.25  2022/12/13 15:25:16  martin.burnicki
 *  Fixed a potential compiler warning.
 *  Revision 1.24  2022/08/26 13:55:28  martin.burnicki
 *  Changed the return type of some snprint_... functions from size_t to int.
 *  Revision 1.23  2021/07/20 10:27:30  daniel
 *  On SOC platforms use alternative method via proc interface
 *  to retrieve current IP address of the IPv4 default gateway.
 *  Revision 1.22  2021/05/18 14:13:48  daniel
 *  Fixed NUM_WORDS bug again
 *  Revision 1.21  2021/05/17 13:23:22  daniel
 *  Fixed bug in definition of NUM_WORDS in snprint_ip6_addr
 *  Revision 1.20  2021/05/11 10:57:00  udo
 *  Include get_ip4_gateway() from build if USE_MBG_TSU is defined.
 *  Revision 1.19  2021/03/22 18:07:30  martin
 *  Updated a bunch of comments.
 *  Revision 1.18  2019/11/21 13:36:39  martin
 *  Use more portable code.
 *  Avoid unnecessary type casts.
 *  Revision 1.17  2019/03/21 08:50:27  thomas-b
 *  Resolved compiler warnings
 *  Revision 1.16  2018/04/06 14:58:54  martin
 *  Fixed ethtool support for older kernels / build environments.
 *  Revision 1.15  2018/02/28 16:56:39  martin
 *  Make parameters speed and duplex of check_port_status() optional.
 *  Doxygen fixes.
 *  Revision 1.14  2018/01/30 17:20:07  martin
 *  Fixed a compiler warning where a char was used as array index.
 *  Revision 1.13  2017/10/05 08:20:40  daniel
 *  Introduced check_port_status() function
 *  Revision 1.12  2017/07/05 14:07:56  martin
 *  IPv6 support functions added by hannes.
 *  Function snprint_ip4_cidr_mask() fixed by hannes. It now behaves
 *  as expected and appends the CIDR mask bits to the IP address now.
 *  Mostly use safe string functions from str_util.c.
 *  New function snprint_ptp_clock_id().
 *  Compiler warnings due to uninitialized vars quieted by thomas-b.
 *  Renamed check_octets_not_all_zero() to octets_are_all_zero(),
 *  which returns a bool now.
 *  Renamed check_mac_addr_not_all_zero() to mac_addr_all_zero(),
 *  which returns a bool now.
 *  Removed get_port_mac_addr_check().
 *  Removed definitions of old MBG_LU_... return codes, and
 *  only use standard MBG_RETURN_CODES instead.
 *  Fixed macro definition syntax to quiet clang compiler warnings.
 *  Let get_port_ip4_settings() check port link even if it
 *  returns an error (changed by daniel).
 *  Check for MBG_TGT_POSIX instead of MBG_TGT_UNIX.
 *  Doxygen comments.
 *  Revision 1.11  2015/04/01 14:31:14  hannes
 *  Fix: cidr_str_to_ip4_addr_and_net_mask: Defaults correctly to
 *  netmask 0.0.0.0 (/0) for no CIDR extension in cidr_str.
 *  Revision 1.10  2014/10/17 12:45:48  martin
 *  Let str_to_ip4_addr() return 0 if an empty string has been passed.
 *  Revision 1.9  2014/09/24 08:31:00  martin
 *  Exclude get_ip4_gateway() from build if USE_MBG_TSU is defined.
 *  Fixed some compiler warnings.
 *  Added and modified some comments.
 *  Revision 1.8  2013/10/02 07:19:13  martin
 *  New function get_port_intf_idx.
 *  Fixed naming.of local netlink support functions.
 *  Revision 1.7  2013/05/22 16:49:42  martin
 *  Fixed some return codes.
 *  Revision 1.6  2013/03/19 10:24:51  martin
 *  Fixed bugs in get_ip4_gateway(): A skipped routing entry was
 *  taken as default route, and thus a wrong default route 0.0.0.0
 *  could be returned erraneously. Also, malloc() was used without
 *  checking the result.
 *  Added conditional debug code.
 *  Revision 1.5  2013/02/19 15:13:10  martin
 *  Added some new functions.
 *  Updated doxygen comments.
 *  Revision 1.4  2012/11/02 09:16:57  martin
 *  Fixed build under Windows.
 *  Revision 1.3  2012/10/02 18:23:28Z  martin
 *  Removed obsolete code to avoid compiler warning.
 *  Revision 1.2  2012/03/09 08:49:19  martin
 *  Added some commonly used functions.
 *  Revision 1.1  2011/03/04 10:01:32Z  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _LAN_UTIL
  #include <lan_util.h>
#undef _LAN_UTIL

#include <words.h>
#include <str_util.h>
#include <mbgerror.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#if defined( MBG_TGT_POSIX )

  #if defined( MBG_TGT_LINUX )

    #include <linux/types.h>

    // Some older versions of linux/types.h don't define u8..u64
    // for user space applications. However, if they do they also
    // define BITS_PER_LONG, so we use this symbol to figure out
    // if we need to define u8..u64 by ourselves.
    #if !defined( BITS_PER_LONG )
      typedef uint8_t u8;
      typedef uint16_t u16;
      typedef uint32_t u32;
      typedef uint64_t u64;
    #endif

    #include <linux/sockios.h>
    #include <linux/ethtool.h>
    #include <linux/rtnetlink.h>
  #endif

  #include <unistd.h>
  #include <syslog.h>
  #include <sys/ioctl.h>
  #include <arpa/inet.h>
  #include <netinet/in.h>

#else

  // Dummy codes, the functions will report an error ...
  #define SIOCGIFADDR      0
  #define SIOCGIFNETMASK   0
  #define SIOCGIFBRDADDR   0

  #if defined( MBG_TGT_WIN32 )
    #define snprintf _snprintf
  #endif

#endif


#if !defined( DEBUG_NETLINK )
  #if ( 0 && defined( DEBUG ) && defined( MBG_TGT_LINUX ) )
    #define DEBUG_NETLINK  1
  #else
    #define DEBUG_NETLINK  0
  #endif
#endif


#if DEBUG_NETLINK
  // FIXME We need a function to output debugging information.
  // Maybe this can be implemented via a function pointer
  // that defaults to a wellknown function.
  #if !defined STANDALONE
    #include <ptp2_cnf.h>  // TODO For mbglog() from the ARM PTP projects
  #else
    // To be provided by the application
    extern __attribute__( ( format( printf, 2, 3 ) ) ) void mbglog( int priority, const char *fmt, ... );
  #endif
#endif  // DEBUG_NETLINK



/**
 * @brief Maximum size of an IPv4 address string in dotted quad format.
 *
 * Includes a terminating 0, so the required minimum size
 * for a buffer to take such a string. i.e. "aaa.bbb.ccc.ddd\0".
 */
#define MAX_IP4_ADDR_STR_SIZE   16


#if defined( MBG_TGT_LINUX )

struct route_info
{
  struct in_addr dstAddr;
  struct in_addr srcAddr;
  struct in_addr gateWay;
  char ifName[IF_NAMESIZE];
};

#endif



/*HDR*/
/**
 * @brief Count the number of sequential bits in an IPv4 net mask.
 *
 * Counting starts from MSB, i.e. for 0xC0 and 0xC1 the results.
 * are both 2 since only the 2 MSBs are sequentially set.
 *
 * @param[in]  p_mask  The IPv4 net mask to be evaluated.
 *
 * @return The number of sequential MSB bits set in @p *p_mask.
 *
 * @see ::ip4_net_mask_from_cidr
 */
int get_ip4_net_mask_bits( const IP4_ADDR *p_mask )
{
  IP4_ADDR msb_mask = IP4_MSB_MASK;
  int i;

  for ( i = 0; i < MAX_IP4_BITS; i++ )
  {
    if ( ( *p_mask & msb_mask ) == 0 )
      break;

    msb_mask >>= 1;
  }

  return i;

}  // get_ip4_net_mask_bits



/*HDR*/
/**
 * @brief Print an IPv4 address to a dotted quad formatted string.
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv4 address to be evaluated.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip4_cidr_addr
 * @see ::str_to_ip4_addr
 * @see ::cidr_str_to_ip4_addr_and_net_mask
 */
int snprint_ip4_addr( char *s, size_t max_len, const IP4_ADDR *p_addr, const char *info )
{
  int n = 0;
  ulong ul = *p_addr;

  if ( info )
    n += snprintf_safe( s, max_len, "%s", info );

  // Don't use byte pointers here because this is not safe
  // for both little and big endian targets.
  n += snprintf_safe( &s[n], max_len - n, "%u.%u.%u.%u",
                 BYTE_3( ul ), BYTE_2( ul ),
                 BYTE_1( ul ), BYTE_0( ul )
               );
  return n;

}  // snprint_ip4_addr



/*HDR*/
/**
 * @brief Print an IPv4 address plus net mask in CIDR notation to a string.
 *
 * The printed CIDR string is something like "172.16.3.250/24"
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv4 address to be evaluated.
 * @param[in]  p_mask   The associated IPv4 net mask.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip4_addr
 * @see ::str_to_ip4_addr
 * @see ::cidr_str_to_ip4_addr_and_net_mask
 */
int snprint_ip4_cidr_addr( char *s, size_t max_len, const IP4_ADDR *p_addr,
                           const IP4_ADDR *p_mask, const char *info )
{
  int cidr_mask_bits;
  int n = snprint_ip4_addr( s, max_len, p_addr, info );

  cidr_mask_bits = get_ip4_net_mask_bits( p_mask );

  if ( ( cidr_mask_bits >= MIN_IP4_CIDR_NETMASK_BITS ) &&
       ( cidr_mask_bits <= MAX_IP4_CIDR_NETMASK_BITS ) )
    n += snprintf_safe( &s[n], max_len - n, "/%i", cidr_mask_bits );

  return n;

}  // snprint_ip4_cidr_addr



/*HDR*/
/**
 * @brief Convert a string to an ::IP4_ADDR.
 *
 * If the output parameter is specified as @a NULL, this function can be used
 * to check if the IPv4 address string is formally correct.
 *
 * @param[out] p_addr  Pointer to an ::IP4_ADDR variable to be filled, or @a NULL.
 * @param[in]  s       An IPv4 address string to be converted.
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip4_addr
 * @see ::snprint_ip4_cidr_addr
 * @see ::cidr_str_to_ip4_addr_and_net_mask
 */
int str_to_ip4_addr( IP4_ADDR *p_addr, const char *s )
{
  IP4_ADDR tmp_ip4_addr = 0;
  const char *cp = s;
  int i;

  if ( strlen( s ) )
  {
    for ( i = 0; ; )
    {
      unsigned long ul = strtoul( (char *) cp, (char **) &cp, 10 );

      if ( ul > 0xFFUL )  // Invalid number.
        return MBG_ERR_PARM_FMT;

      tmp_ip4_addr |= ul << ( 8 * (3 - i) );

      if ( ++i >= 4 )
        break;        // Done.

      if ( *cp != '.' )
        return MBG_ERR_PARM_FMT;    // Invalid string format, dot expected.

      cp++;  // Skip dot.
    }
  }

  if ( p_addr )
    *p_addr = tmp_ip4_addr;

  // Success: return the number of evaluated chars.
  return (int) ( cp - s );

}  // str_to_ip4_addr



/*HDR*/
/**
 * @brief Convert a string in CIDR notation to an ::IP4_ADDR and net mask.
 *
 * If the output parameters are specified as @a NULL, this function
 * can be used to check if the CIDR string is formally correct.
 *
 * @param[out] p_addr    Pointer to an ::IP4_ADDR variable to be filled with
 *                       the IPv4 address, or @a NULL.
 * @param[out] p_mask    Pointer to an ::IP4_ADDR variable to be filled with
 *                       the IPv4 net mask, or @a NULL.
 * @param[in]  cidr_str  The string to be converted, in CIDR format, e.g. "172.16.3.250/24".
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip4_addr
 * @see ::snprint_ip4_cidr_addr
 * @see ::str_to_ip4_addr
 */
int cidr_str_to_ip4_addr_and_net_mask( IP4_ADDR *p_addr, IP4_ADDR *p_mask,
                                       const char *cidr_str )
{
  IP4_ADDR mask;
  long cidr_mask_bits;
  const char *cp;
  int l;
  int rc = str_to_ip4_addr( p_addr, cidr_str );

  if ( mbg_rc_is_error( rc ) )
    return rc;


  l = (int) strlen( cidr_str );

  if ( l < rc )  // Input string too short.
    return MBG_ERR_PARM_FMT;


  cp = &cidr_str[rc];

  if ( *cp == 0 )  // End of string.
  {
    // The string has no CIDR extension, so
    // assume "/0", i.e. host mask 0.0.0.0;
    mask = (IP4_ADDR) 0;
    goto done;
  }


  if ( *cp != '/' )
    return MBG_ERR_PARM_FMT;


  cp++;
  cidr_mask_bits = strtol( (char *) cp, (char **) &cp, 10 );

  if ( ( cidr_mask_bits < MIN_IP4_CIDR_NETMASK_BITS ) ||
       ( cidr_mask_bits > MAX_IP4_CIDR_NETMASK_BITS ) )
    return MBG_ERR_RANGE;


  mask = ip4_net_mask_from_cidr( (int) cidr_mask_bits );

done:
  if ( p_mask )
    *p_mask = mask;

  // Success: return the number of evaluated chars.
  return (int) ( cp - cidr_str );

}  // cidr_str_to_ip4_addr_and_net_mask



/*HDR*/
/**
 * @brief Count the number of sequential bits in an IPv6 net mask.
 *
 * Counting starts from MSB, i.e. for 0xC0 and 0xC1 the results
 * are both 2 since only the 2 MSBs are sequentially set.
 *
 * @param[in]  p_mask  The IPv6 net mask to be evaluated.
 *
 * @return The number of sequential MSB bits set in @p *p_mask.
 *
 * @see ::ip6_net_mask_from_cidr
 */
int get_ip6_net_mask_bits( const IP6_ADDR *p_mask )
{
  int i;
  int cnt = 0;
  uint8_t msb_mask = IP6_MSB_MASK;

  for ( i = IP6_ADDR_BYTES - 1 ; i >= 0; i-- )
  {
    if ( p_mask->b[i] == 0xff )
      cnt += 8;
    else
    {
      for ( ; cnt < MAX_IP6_CIDR_NETMASK_BITS; cnt++ )
      {
        if ( ( p_mask->b[i] & msb_mask ) == 0 )
          break;

        msb_mask >>= 1;
      }
      break;
    }
  }

  return cnt;

}  // get_ip6_net_mask_bits



/*HDR*/
/**
 * @brief Print an IPv6 address in optimized format to a string.
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv6 address to be evaluated.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip6_cidr_addr
 * @see ::snprint_ip6_cidr_mask_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
int snprint_ip6_addr( char *s, size_t max_len, const IP6_ADDR *p_addr, const char *info )
{
  // Copied from inet_ntop.c, and reversed byte order.

  IP6_ADDR_STR tmp;
  char *tp;
  #define NUM_WORDS ( (int) ( IP6_ADDR_BYTES / sizeof( uint16_t ) ) )
  uint16_t words[NUM_WORDS] = { 0 };
  int i;
  int n = 0;
  struct
  {
    int base;
    int len;
  } best = { 0 }, cur = { 0 };

  /*
   * Preprocess:
   *      Copy the input (bytewise) array into a wordwise array.
   *      Find the longest run of 0x00's in p_addr->b[] for :: shorthanding.
   */
  for ( i = IP6_ADDR_BYTES - 1; i >= 0; i-- )
    words[(IP6_ADDR_BYTES - 1 - i) / 2] |= (p_addr->b[i] << ((1 - ((IP6_ADDR_BYTES - 1  - i) % 2)) << 3));

  best.base = -1;
  cur.base = -1;

  for ( i = 0; i < NUM_WORDS; i++ )
  {
    if ( words[i] == 0 )
    {
      if ( cur.base == -1 )
        cur.base = i, cur.len = 1;
      else
        cur.len++;
    }
    else
    {
      if ( cur.base != -1 )
      {
        if ( best.base == -1 || cur.len > best.len )
            best = cur;

        cur.base = -1;
      }
    }
  }

  if ( cur.base != -1 )
  {
    if ( best.base == -1 || cur.len > best.len )
      best = cur;
  }

  if ( best.base != -1 && best.len < 2 )
    best.base = -1;

  // Format the result.

  tp = tmp;

  for ( i = 0; i < NUM_WORDS; i++ )
  {
    // Are we inside the best run of 0x00's?
    if ( best.base != -1 && i >= best.base && i < ( best.base + best.len ) )
    {
      if (i == best.base)
        *tp++ = ':';

      continue;
    }

    // Are we following an initial run of 0x00s or any real hex?
    if ( i != 0 )
      *tp++ = ':';

    // Is this address an encapsulated IPv4?
    if ( i == 6 && best.base == 0 && ( best.len == 6 || ( best.len == 5 && words[5] == 0xffff ) ) )
        return MBG_ERR_INV_PARM; // We don't support encapsulated IPv4.

    sprintf( tp, "%x", words[i] );
    tp += strlen( tp );
  }

  // Was it a trailing run of 0x00's?
  if ( best.base != -1 && ( best.base + best.len ) == NUM_WORDS )
    *tp++ = ':';

  *tp++ = '\0';

  /*
   * Check for overflow, copy, and we're done.
   */
  if ( ( tp - tmp ) > (int) max_len )
    return MBG_ERR_OVERFLOW;

  if ( info )
    n += snprintf_safe( s, max_len, "%s", info );

  n += snprintf_safe( &s[n], max_len - n, "%s", tmp );

  return n;

  #undef NUM_WORDS

}  // snprint_ip6_addr



/*HDR*/
/**
 * @brief Print an IPv6 address plus net mask to string in CIDR notation.
 *
 * The printed CIDR string is something like "2001:0DB8:0:CD30:EF45::/64".
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr   The IPv6 address to be evaluated.
 * @param[in]  p_mask   The associated IPv6 net mask.
 * @param[in]  info     An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_mask_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
int snprint_ip6_cidr_addr( char *s, size_t max_len, const IP6_ADDR *p_addr,
                           const IP6_ADDR *p_mask, const char *info )
{
  int n = snprint_ip6_addr( s, max_len, p_addr, info );

  int cidr_mask_bits = get_ip6_net_mask_bits( p_mask );

  if ( ( cidr_mask_bits >= MIN_IP6_CIDR_NETMASK_BITS ) &&
       ( cidr_mask_bits <= MAX_IP6_CIDR_NETMASK_BITS ) )
    n += snprintf_safe( &s[n], max_len - n, "/%i", cidr_mask_bits );

  return n;

}  // snprint_ip6_cidr_addr



/*HDR*/
/**
 * @brief Print an IPv6 address plus number of net mask bits to string in CIDR notation.
 *
 * The printed CIDR string is something like "2001:0DB8:0:CD30:EF45::/64".
 *
 * @param[out] s               The string buffer to be filled.
 * @param[in]  max_len         Size of the output buffer for 0-terminated string.
 * @param[in]  p_addr          The IPv6 address to be evaluated.
 * @param[in]  cidr_mask_bits  The CIDR number of bits specifying the IPv6 net mask.
 * @param[in]  info            An optional string which is prepended to the string, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
int snprint_ip6_cidr_mask_addr( char *s, size_t max_len, const IP6_ADDR *p_addr,
                                const int cidr_mask_bits, const char* info )
{
  int n;
  IP6_ADDR mask;

  ip6_net_mask_from_cidr( &mask, cidr_mask_bits );

  n = snprint_ip6_addr( s, max_len, p_addr, info );

  if ( ( cidr_mask_bits >= MIN_IP6_CIDR_NETMASK_BITS ) &&
        ( cidr_mask_bits <= MAX_IP6_CIDR_NETMASK_BITS ) )
    n += snprintf_safe( &s[n], max_len - n, "/%i", cidr_mask_bits );

  return n;

}  // snprint_ip6_cidr_mask_addr



/*HDR*/
/**
 * @brief Convert a string to an ::IP6_ADDR.
 *
 * If the output parameter is specified as @a NULL, this function
 * can be used to check if the string is formally correct.
 *
 * On success ::IP6_ADDR variable contains the IPv6 address
 * in little endian byte order.
 *
 * @param[out] p_addr  Pointer to the ::IP6_ADDR variable, or @a NULL.
 * @param[in]  s       A string containing an IPv6 address.
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_addr
 * @see ::snprint_ip6_cidr_mask_addr
 * @see ::str_to_ip6_addr
 * @see ::cidr_str_to_ip6_addr_and_cidr_bits
 * @see ::cidr_str_to_ip6_addr_and_net_mask
 */
int str_to_ip6_addr( IP6_ADDR *p_addr, const char *s )
{
  // Copied from inet_pton.c, and reversed byte order.
  IP6_ADDR tmp = { { 0 } };
  static const char xdigits[] = "0123456789abcdef";
  uint8_t *tp;
  uint8_t *startp;
  uint8_t *colonp;
  int ch;
  int saw_xdigit;
  int read_cnt = 0;
  unsigned int val;

  if ( p_addr )
    memset( p_addr, 0, sizeof( *p_addr ) );  // Set IP address to '::'.

  startp = tmp.b;
  tp = startp + sizeof( tmp ) - 1;

  colonp = NULL;

  // Leading '::' requires some special handling.
  if ( *s == ':' )
  {
    read_cnt++;

    if ( *++s != ':' )
      return MBG_ERR_PARM_FMT;
  }

  saw_xdigit = 0;
  val = 0;

  while ( ( ch = tolower( (int) *s++ ) ) != '\0' )
  {
    const char *pch;

    read_cnt++;

    pch = strchr( xdigits, ch );

    if ( pch != NULL )
    {
      val <<= 4;
      val |= ( pch - xdigits );

      if ( val > 0xffff )    // TODO Signed? unsigned?
        return MBG_ERR_PARM_FMT;

      saw_xdigit = 1;
      continue;
    }

    if ( ch == ':' )
    {
      if ( !saw_xdigit )
      {
        if ( colonp )
          return MBG_ERR_PARM_FMT;

        colonp = tp;
        continue;

      }
      else
        if ( *s == '\0' )
          return MBG_ERR_PARM_FMT;

      if ( tp - sizeof( uint16_t ) < startp )
        return MBG_ERR_PARM_FMT;

      *tp-- = (uint8_t) ( val >> 8 ) & 0xff;
      *tp-- = (uint8_t) val & 0xff;
      saw_xdigit = 0;
      val = 0;
      continue;
    }

    if ( ch == '/' )  // CIDR notation. We reached the end.
    {
      read_cnt--;
      break;
    }

    return MBG_ERR_PARM_FMT;
  }

  if ( saw_xdigit )
  {
    if ( ( tp - sizeof( uint16_t ) + 1 ) < startp )
      return MBG_ERR_PARM_FMT;

    *tp-- = (uint8_t) (val >> 8) & 0xff;
    *tp-- = (uint8_t) val & 0xff;
  }

  if ( colonp != NULL )
  {
    /*
     * Since some memmove()'s erroneously fail to handle
     * overlapping regions, we'll do the shift by hand.
     */
    const size_t n = colonp - tp;
    size_t i;

    if ( ( tp + 1 ) == startp )
      return MBG_ERR_PARM_FMT;

    for ( i = 0; i < n; i++ )
    {
      startp[i] = colonp[i + 1 - n];
      colonp[i + 1 - n] = 0;
    }

    tp = startp - 1;
  }

  if ( ( tp + 1 ) != startp )
    return MBG_ERR_PARM_FMT;

  if ( p_addr )
    *p_addr = tmp;

  return read_cnt;

}  // str_to_ip6_addr



/*HDR*/
/**
 * @brief Convert a string in CIDR notation to an ::IP6_ADDR and net mask.
 *
 * If the output parameters are specified as @a NULL, this function
 * can be used to check if the CIDR string is formally correct.
 *
 * @param[out] p_addr  Pointer to an ::IP6_ADDR variable to be filled up
 *                     with the IPv6 address, or @a NULL.
 * @param[out] p_mask  Pointer to an ::IP6_ADDR variable to be filled up
                       with the net mask bits, or @a NULL.
 * @param[in] cidr_str The string to be converted, in CIDR format, e.g. "2001:0DB8:0:CD30::/64".
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip4_addr
 * @see ::snprint_ip4_cidr_addr
 * @see ::str_to_ip4_addr
 */
int cidr_str_to_ip6_addr_and_net_mask( IP6_ADDR *p_addr, IP6_ADDR *p_mask, const char *cidr_str )
{
  int mask = 0;
  int rc = cidr_str_to_ip6_addr_and_cidr_bits( p_addr, &mask, cidr_str );

  if ( mbg_rc_is_error( rc ) )
    return rc;

  if ( p_mask )
    ip6_net_mask_from_cidr( p_mask, mask );

  return rc;

}  // cidr_str_to_ip6_addr_and_net_mask



/*HDR*/
/**
 * @brief Convert a string in CIDR notation to an ::IP6_ADDR and net mask bits.
 *
 * If the output parameters are specified as @a NULL, this function
 * can be used to check if the CIDR string is formally correct.
 *
 * @param[out] p_addr    Pointer to an ::IP6_ADDR variable for the IPv6 address, or @a NULL.
 * @param[out] p_cidr    Pointer to an int variable for the net mask bits, or @a NULL.
 * @param[in]  cidr_str  The string to be converted, in CIDR format, e.g. "2001:0DB8:0:CD30::/64".
 *
 * @return  A number >= 0 (::MBG_SUCCESS) according to the number of characters evaluated
 *          from the input string, or one of the @ref MBG_ERROR_CODES on error,
 *          specifically ::MBG_ERR_PARM_FMT if an invalid number or character was found in the string.
 *
 * @see ::snprint_ip6_addr
 * @see ::snprint_ip6_cidr_addr
 * @see ::str_to_ip6_addr
 */
int cidr_str_to_ip6_addr_and_cidr_bits( IP6_ADDR *p_addr, int *p_cidr,
                                        const char *cidr_str )
{
  long cidr_mask_bits;
  const char *cp;
  ssize_t l;
  int rc = str_to_ip6_addr( p_addr, cidr_str );

  if ( mbg_rc_is_error( rc ) )
    return rc;

  l = strlen( cidr_str );

  if ( l < rc )  // Input string too short.
    return MBG_ERR_PARM_FMT;

  cp = &cidr_str[rc];

  if ( *cp == 0 )  // End of string.
  {
    // The string has no CIDR extension, so
    // assume "/0", i.e. host mask ::
    cidr_mask_bits = 0;
    goto done;
  }

  if ( *cp != '/' )
    return MBG_ERR_PARM_FMT;

  cp++;
  cidr_mask_bits = strtol( (char *) cp, (char **) &cp, 10 );

  if ( ( cidr_mask_bits < MIN_IP6_CIDR_NETMASK_BITS ) ||
       ( cidr_mask_bits > MAX_IP6_CIDR_NETMASK_BITS ) )
    return MBG_ERR_RANGE;

done:
  if ( p_cidr )
    *p_cidr = (int) cidr_mask_bits;

  // Success: return the number of evaluated chars.
  return (int) ( cp - cidr_str );

}  // cidr_str_to_ip6_addr_and_cidr_bits



/*HDR*/
/**
 * @brief Compute an IPv6 net mask according to the number of CIDR netmask bits.
 *
 * E.g. the 64 bits mentioned in "2001:0DB8:0:CD30::/64" result in 2^64,
 * corresponding to FFFF:FFFF:FFFF:FFFF:: in IPv6 notation.
 *
 * @param[out] p_mask        Pointer to an ::IP6_ADDR variable for the IPv6 netmask.
 * @param[in]  netmask_bits  Number of netmask bits from CIDR notation.
 *
 * @see ::get_ip6_net_mask_bits
 */
void ip6_net_mask_from_cidr( IP6_ADDR *p_mask, int netmask_bits )
{
  int i = 0;

  if ( p_mask )
  {
    memset( p_mask, 0, sizeof( *p_mask ) );

    if ( netmask_bits < 0 )
      netmask_bits = 0;
    else
      if ( netmask_bits >= IP6_ADDR_BITS )
        netmask_bits = IP6_ADDR_BITS;

    for ( i = IP6_ADDR_BYTES - 1; i >= 0; i-- )
    {
      if ( netmask_bits > 8 )
      {
        p_mask->b[i] = 0xff;
        netmask_bits -= 8;
      }
      else
      {
        p_mask->b[i] = (0xff << ( 8 - netmask_bits ) ) & 0xff;
        netmask_bits = 0;
      }
    }
  }

}  // ip6_net_mask_from_cidr



/*HDR*/
/**
 * @brief Determine the network part of an IPv6 address based on the net mask.
 *
 * E.g. IP "2001:0DB8:0:CD30::", net mask "FFFF:FFFF::" yields network part "2001:0DB8::".
 *
 * @param[out] p_net_part  The extracted network part of the IPv6 address.
 * @param[in]  p_addr      The IPv6 address to be evaluated.
 * @param[in]  p_mask      The associated IPv6 net mask.
 */
void ip6_net_part_from_addr( IP6_ADDR *p_net_part, const IP6_ADDR *p_addr,
                             const IP6_ADDR *p_mask )
{
  int i;

  for ( i = IP6_ADDR_BYTES; i-- > 0; )
    p_net_part->b[i] = p_addr->b[i] & p_mask->b[i];

}  // ip6_net_part_from_addr



/*HDR*/
/**
 * @brief Print a MAC ID or similar array of octets to a string.
 *
 * @param[out] s           The string buffer to be filled.
 * @param[in]  max_len     Maximum length of the string, i.e. size of the buffer.
 * @param[in]  octets      An array of octets.
 * @param[in]  num_octets  The number of octets to be printed from the array.
 * @param[in]  sep         The separator printed between the bytes, or 0.
 * @param[in]  info        An optional string which is prepended to the output, or @a NULL.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_mac_addr
 * @see ::str_to_octets
 * @see ::octets_are_all_zero
 */
int snprint_octets( char *s, size_t max_len, const uint8_t *octets,
                    int num_octets, char sep, const char *info )
{
  int n = 0;
  int i;

  if ( info )
    n += snprintf_safe( s, max_len, "%s", info );

  for ( i = 0; i < num_octets; i++ )
  {
    if ( i && sep )
      n += snprintf_safe( &s[n], max_len - n, "%c", sep );

    n += snprintf_safe( &s[n], max_len - n, "%02X", octets[i] );
  }

  return n;

}  // snprint_octets



/*HDR*/
/**
 * @brief Print a ::PTP_CLOCK_ID to a string.
 *
 * @todo Maybe this function should be moved to a different module.
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Maximum length of the string, i.e. size of the buffer.
 * @param[in]  p        The ::PTP_CLOCK_ID to be printed.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_octets
 */
int snprint_ptp_clock_id( char *s, size_t max_len, const PTP_CLOCK_ID *p )
{
  return snprint_octets( s, max_len, p->b, sizeof( *p ), ':', NULL );

}  // snprint_ptp_clock_id



/*HDR*/
/**
 * @brief Print a MAC address to a string.
 *
 * @param[out] s           The string buffer to be filled.
 * @param[in]  max_len     Maximum length of the string, i.e. size of the buffer.
 * @param[in]  p_mac_addr  The MAC address to be printed.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::snprint_octets
 * @see ::str_to_octets
 * @see ::octets_are_all_zero
 */
int snprint_mac_addr( char *s, size_t max_len, const MBG_MAC_ADDR *p_mac_addr )
{
  return snprint_octets( s, max_len, p_mac_addr->b, sizeof( *p_mac_addr ), MAC_SEP_CHAR, NULL );

}  // snprint_mac_addr



/*HDR*/
/**
 * @brief Set a MAC ID or a similar array of octets from a string.
 *
 * @param[out] octets      An array of octets to be set up.
 * @param[in]  num_octets  The number of octets that can be stored.
 * @param[in]  s           The string to be converted.
 *
 * @return  The overall number of octets decoded from the string.
 *
 * @see ::snprint_octets
 * @see ::snprint_mac_addr
 * @see ::octets_are_all_zero
 */
int str_to_octets( uint8_t *octets, int num_octets, const char *s )
{
  char *cp = (char *) s;
  int i;

  // Don't use strtok() because that functions modifies the original string.
  for ( i = 0; i < num_octets; )
  {
    octets[i] = (uint8_t) strtoul( cp, &cp, 16 );
    i++;

    if ( *cp == 0 )
      break;      // End of string.

    if ( ( *cp != MAC_SEP_CHAR ) && ( *cp != MAC_SEP_CHAR_ALT ) )
      break;      // Invalid character.

    cp++;
  }

  return i;

}  // str_to_octets



/*HDR*/
/**
 * @brief Check if an array of octets is all zero.
 *
 * @param[in]  octets      Pointer to the array of octets.
 * @param[in]  num_octets  Number of octets.
 *
 * @return  @a true if all bytes are 0, else @a false.
 *
 * @see ::snprint_octets
 * @see ::snprint_mac_addr
 * @see ::str_to_octets
 */
bool octets_are_all_zero( const uint8_t *octets, int num_octets )
{
  int i;

  // Check if any of the bytes is != 0.
  for ( i = 0; i < num_octets; i++ )
    if ( octets[i] != 0 )
      break;

  return i == num_octets;  // True if *all* bytes are 0.

}  // octets_are_all_zero



/*HDR*/
/**
 * @brief Check if a MAC address is all zero.
 *
 * @param[in]  p_addr  Pointer to a MAC address to be checked.
 *
 * @return  @a true if all bytes of the MAC address are 0, else @a false.
 *
 * @see ::octets_are_all_zero
 */
bool mac_addr_is_all_zero( const MBG_MAC_ADDR *p_addr )
{
  return octets_are_all_zero( p_addr->b, sizeof( p_addr->b ) );

}  // mac_addr_is_all_zero



#if defined( MBG_TGT_POSIX )

/*HDR*/
/**
 * @brief Do a SIOCGxxx IOCTL call to read specific information from a LAN interface.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[in]  ioctl_code  One of the predefined system SIOCGxxx IOCTL codes.
 * @param[out] p_ifreq     Pointer to a request buffer
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int do_siocg_ioctl( const char *if_name, int ioctl_code, struct ifreq *p_ifreq )
{
  int sock_fd;
  int rc;

  if ( strlen( if_name ) > ( IFNAMSIZ - 1 ) )
    return MBG_ERR_PARM_FMT;

  sock_fd = socket( AF_INET, SOCK_DGRAM, 0 );   // TODO Or AF_INET6/PF_INET6 for IPv6.

  if ( sock_fd == MBG_INVALID_SOCK_FD )  // Failed to open socket.
    return mbg_get_last_socket_error( "failed to open socket in do_siocg_ioctl" );

  strncpy_safe( p_ifreq->ifr_name, if_name, sizeof( p_ifreq->ifr_name ) );

  rc = ioctl( sock_fd, ioctl_code, p_ifreq );

  if ( rc == -1 )  // IOCTL failed, errno has been set appropriately.
    rc = mbg_get_last_socket_error( "ioctl failed in do_siocg_ioctl" );
  else
    rc = MBG_SUCCESS;

  close( sock_fd );

  return rc;

}  // do_siocg_ioctl

#endif  //  defined( MBG_TGT_POSIX )



/*HDR*/
/**
 * @brief Retrieve the index of a specific network interface.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_intf_idx  Pointer to a variable to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_intf_idx is set to -1.
 */
int get_port_intf_idx( const char *if_name, int *p_intf_idx )
{
  int rc = MBG_ERR_NOT_SUPP_ON_OS;

  #if defined( MBG_TGT_LINUX )
    struct ifreq ifr = { { { 0 } } };

    rc = do_siocg_ioctl( if_name, SIOCGIFINDEX, &ifr );

    if ( mbg_rc_is_success( rc ) )
    {
      *p_intf_idx = ifr.ifr_ifindex;
      return rc;
    }
  #endif

  // We only get here on error.
  *p_intf_idx = -1;

  return rc;

}  // get_port_intf_idx



/*HDR*/
/**
 * @brief Retrieve the MAC address of a network interface.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_mac_addr  Pointer to the MAC address buffer to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, the MAC address @p *p_mac_addr is set to all 0.
 */
int get_port_mac_addr( const char *if_name, MBG_MAC_ADDR *p_mac_addr )
{
  int rc = MBG_ERR_NOT_SUPP_ON_OS;

  #if defined( MBG_TGT_LINUX )
    struct ifreq ifr = { { { 0 } } };

    rc = do_siocg_ioctl( if_name, SIOCGIFHWADDR, &ifr );

    if ( mbg_rc_is_success( rc ) )
    {
      memcpy( p_mac_addr, ifr.ifr_hwaddr.sa_data, sizeof( *p_mac_addr ) );
      return rc;
    }
  #endif

  // We only get here on error.
  memset( p_mac_addr, 0, sizeof( *p_mac_addr ) );

  return rc;

}  // get_port_mac_addr



/*HDR*/
/**
 * @brief Check the link state of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 *
 * @return  1 if link detected on port,<br>
 *          0 if no link detected on port,<br>
 *          one of the (negative) @ref MBG_ERROR_CODES in case of an error.
 */
int check_port_link( const char *if_name )
{
  int rc = MBG_ERR_NOT_SUPP_ON_OS;

  #if defined( MBG_TGT_LINUX )
    struct ifreq ifr = { { { 0 } } };
    struct ethtool_value edata = { 0 };

    edata.cmd = ETHTOOL_GLINK;  // Defined in ethtool.h.
    ifr.ifr_data = (caddr_t) &edata;

    rc = do_siocg_ioctl( if_name, SIOCETHTOOL, &ifr );

    if ( mbg_rc_is_success( rc ) )
      rc = ( edata.data == 0 ) ? 0 : 1;
  #endif

  return rc;

}  // check_port_link



/*HDR*/
/**
 * @brief Check the state of a network interface.
 *
 * @param[in]   if_name   Name of the interface to check.
 * @param[out]  p_speed   Optional pointer to a variable to take up the link speed, may be @a NULL.
 * @param[out]  p_duplex  Optional pointer to a variable to take up the duplex state, may be @a NULL.
 *
 * @return 1 if link detected on port,<br>
 *         0 if no link detected on port,<br>
 *         one of the (negative) @ref MBG_ERROR_CODES in case of an error.
 *         In case of error, @p *p_duplex is set to -1.
 */
int check_port_status( const char *if_name, int *p_speed, int *p_duplex )
{
  int rc = MBG_ERR_NOT_SUPP_ON_OS;

  #if defined( MBG_TGT_LINUX )
    struct ifreq ifr = { { { 0 } } };
    struct ethtool_cmd cmd = { 0 };
    int ok;

    ifr.ifr_data = (caddr_t) &cmd;
    cmd.cmd = ETHTOOL_GSET;  // "Get settings"

    rc = do_siocg_ioctl( if_name, SIOCETHTOOL, &ifr );

    ok = mbg_rc_is_success( rc );

    if ( p_speed )
    {
      #if defined( ETH_TEST_FL_EXTERNAL_LB_DONE )
        // ethtool_cmd_speed() is only provided by kernels ~3.7-rc1 and
        // newer, which should also define ETH_TEST_FL_EXTERNAL_LB_DONE.
        // The function assembles a 32 bit value from two 16 bit fields.
        *p_speed = ok ? ethtool_cmd_speed( &cmd ) : 0;  // [MHz]
      #else
        // Older kernels only provide a single 16 bit 'speed' field.
        *p_speed = ok ? cmd.speed : 0;  // [MHz]
      #endif
    }

    if ( p_duplex )
      *p_duplex = ok ? cmd.duplex : -1;  // 0x00 == half, 0x01 == full

  #endif

  return rc;

}  // check_port_status



static /*HDR*/
/**
 * @brief Retrieve some IPv4 address-like info from a network interface.
 *
 * @param[in]  if_name      Name of the interface.
 * @param[out] p_addr       Pointer to address field to be filled up.
 * @param[in]  sigioc_code  The IOCTL code associated with the address.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 */
int get_specific_port_ip4_addr( const char *if_name, IP4_ADDR *p_addr, int sigioc_code )
{
  int rc = MBG_ERR_NOT_SUPP_ON_OS;

  #if defined( MBG_TGT_LINUX )
    struct ifreq ifr = { { { 0 } } };

    rc = do_siocg_ioctl( if_name, sigioc_code, &ifr );

    if ( mbg_rc_is_success( rc ) )
    {
      *p_addr = ntohl( ( (struct sockaddr_in *) &ifr.ifr_addr )->sin_addr.s_addr );
      return rc;
    }
  #endif

  // We only get here on error.
  *p_addr = 0;  // Make empty address.

  return rc;

}  // get_specific_port_ip4_addr



/*HDR*/
/**
 * @brief Retrieve the IPv4 address of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p_addr   Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
int get_port_ip4_addr( const char *if_name, IP4_ADDR *p_addr )
{
  return get_specific_port_ip4_addr( if_name, p_addr, SIOCGIFADDR );

}  // get_port_ip4_addr



/*HDR*/
/**
 * @brief Retrieve the IPv4 net mask of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p_addr   Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
int get_port_ip4_netmask( const char *if_name, IP4_ADDR *p_addr )
{
  return get_specific_port_ip4_addr( if_name, p_addr, SIOCGIFNETMASK );

}  // get_port_ip4_netmask



/*HDR*/
/**
 * @brief Retrieve the IPv4 broadcast address of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p_addr   Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
int get_port_ip4_broad_addr( const char *if_name, IP4_ADDR *p_addr )
{
  return get_specific_port_ip4_addr( if_name, p_addr, SIOCGIFBRDADDR );

}  // get_port_ip4_broad_addr



#if defined( MBG_TGT_LINUX )

#if !defined( USE_MBG_SOC)

static /*HDR*/
/**
 * @brief Read a requested message from the netlink socket.
 *
 * @return Message length (>= 0) on success, or of the (negative) @ref MBG_ERROR_CODES.
 */
int nl_read( int sock_fd, char *buf_ptr, size_t buf_size, uint32_t seq_num, uint32_t pid )
{
  struct nlmsghdr *nl_hdr;
  int read_len = 0;
  int msg_len = 0;

  do
  {
    // Receive response from the kernel, can be several chunks.
    if ( ( read_len = recv( sock_fd, buf_ptr, buf_size - msg_len, 0 ) ) == -1 )
    {
      int rc = mbg_get_last_socket_error( NULL );
      #if DEBUG_NETLINK
        mbglog( LOG_ERR, "%s: Failed to receive netlink packet: %s",
                __func__, mbg_strerror( rc ) );
      #endif
      return rc;
    }

    nl_hdr = (struct nlmsghdr *) buf_ptr;

    // Check if the header is valid.
    if ( ( NLMSG_OK( nl_hdr, read_len ) == 0 ) || ( nl_hdr->nlmsg_type == NLMSG_ERROR ) )
    {
      #if DEBUG_NETLINK
        mbglog( LOG_ERR, "%s: Invalid header in received netlink packet",
                __func__ );
      #endif
      return MBG_ERR_DATA_FMT;
    }

    // Check if it's the last message.
    if ( nl_hdr->nlmsg_type == NLMSG_DONE )
    {
      #if DEBUG_NETLINK
        mbglog( LOG_ERR, "%s: Reached last message in received netlink packet",
                __func__ );
      #endif
      break;
    }

    // Appropriately move the pointer to the buffer.
    buf_ptr += read_len;
    msg_len += read_len;

    // Check if it's a multi part message.
    if ( ( nl_hdr->nlmsg_flags & NLM_F_MULTI ) == 0 )
    {
      // Return if it's not a multi part message.
      #if DEBUG_NETLINK
        mbglog( LOG_ERR, "%s: Received packet is not a multi part message",
                __func__ );
      #endif
      break;
    }

  } while ( ( nl_hdr->nlmsg_seq != seq_num ) || ( nl_hdr->nlmsg_pid != pid ) );

  #if DEBUG_NETLINK
    mbglog( LOG_ERR, "%s: Received packet has len %i",
            __func__, msg_len );
  #endif

  return msg_len;

}  // nl_read

#endif


#if DEBUG_NETLINK

static /*HDR*/
void nl_log_bytes( const char *fnc,  const char *info, const void *p, int n_bytes )
{
  char ws[80];
  int n = 0;
  int i;

  for ( i = 0; i < n_bytes; i++ )
    n += snprintf_safe( &ws[n], sizeof( ws ) - n, " %i", BYTE_OF( * (uint8_t *) p, i ) );

  mbglog( LOG_INFO, "%s: attibute %s, copying %i bytes:%s",
          fnc, info, n_bytes, ws );

}  // nl_log_bytes

#endif


#if !defined( USE_MBG_SOC)

static /*HDR*/
int nl_parse_routes( struct nlmsghdr *nl_hdr, struct route_info *rt_info )
{
  // Parse the route info returned.
  struct rtmsg *rt_msg;
  struct rtattr *rt_attr;
  int rt_len;

  rt_msg = (struct rtmsg *) NLMSG_DATA( nl_hdr );

  // If the route is not for AF_INET, return.
  // This could be also IPv6 routes.
  if ( rt_msg->rtm_family != AF_INET )   // TODO PF_INET6 for IPv6
  {
    #if DEBUG_NETLINK
      mbglog( LOG_ERR, "%s: Route is not AF_INET (%li), but %li",
              __func__, (long) AF_INET, (long) rt_msg->rtm_family );
    #endif
    return -1;
  }

  // If the route does not belong to main routing table, return.
  if ( rt_msg->rtm_table != RT_TABLE_MAIN )
  {
    #if DEBUG_NETLINK
      mbglog( LOG_ERR, "%s: Route does not belong to main table (%li), but %li",
              __func__, (long) RT_TABLE_MAIN, (long) rt_msg->rtm_table );
    #endif
    return -1;
  }


  // Get the rtattr field.
  rt_attr = (struct rtattr *) RTM_RTA( rt_msg );
  rt_len = RTM_PAYLOAD( nl_hdr );

  for ( ; RTA_OK( rt_attr, rt_len ); rt_attr = RTA_NEXT( rt_attr, rt_len ) )
  {
    #if DEBUG_NETLINK
      mbglog( LOG_ERR, "rt_attr at %p, %i bytes left", rt_attr, rt_len );
    #endif

    switch ( rt_attr->rta_type )
    {
      case RTA_OIF:
        if_indextoname( *(int *)RTA_DATA( rt_attr ), rt_info->ifName );
        #if DEBUG_NETLINK
          mbglog( LOG_ERR, "%s: attibute RTA_OIF, IF name: %s",
                  __func__, rt_info->ifName );
        #endif
        break;

      case RTA_GATEWAY:
        memcpy( &rt_info->gateWay, RTA_DATA( rt_attr ), sizeof( rt_info->gateWay ) );
        #if DEBUG_NETLINK
          nl_log_bytes( __func__, "RTA_GATEWAY", &rt_info->gateWay,  sizeof( rt_info->gateWay ) );
        #endif
        break;

      case RTA_PREFSRC:
        memcpy( &rt_info->srcAddr , RTA_DATA( rt_attr ), sizeof( rt_info->srcAddr ) );
        #if DEBUG_NETLINK
          nl_log_bytes( __func__, "RTA_PREFSRC", &rt_info->srcAddr,  sizeof( rt_info->srcAddr ) );
        #endif
        break;

      case RTA_DST:
        memcpy( &rt_info->dstAddr, RTA_DATA( rt_attr ), sizeof( rt_info->dstAddr ) );
        #if DEBUG_NETLINK
          nl_log_bytes( __func__, "RTA_DST", &rt_info->dstAddr,  sizeof( rt_info->dstAddr ) );
        #endif
        break;

      #if DEBUG_NETLINK
      case RTA_TABLE:
        {
          // The RTA_TABLE attribute was added to the kernel source in 2006 to support
          // more than 255 routing tables. Originally, the number of the routing table
          // to which an entry belongs had been passed only in struct rtmsg::rtm_table,
          // which is only 8 bits wide and should still hold the 8 LSBs of the full
          // routing table number for compatibility, so this should still work for the
          // standard routing tables, including the main table we are inspecting here.
          //
          // Whether this can be compiled in depends on the version of linux/rtnetlink.h
          // used by the compiler. Unfortunately RTA_TABLE is part of an enum, so you
          // can't simply use #ifdef RTA_TABLE to include this code conditionally.
          uint32_t rta_table;
          memcpy( &rta_table, RTA_DATA( rt_attr ), sizeof( rta_table ) );
          mbglog( LOG_ERR, "%s: attibute RTA_TABLE %i (%i)",
                  __func__, rta_table, rt_msg->rtm_table );
        }
        break;

      default:
        mbglog( LOG_ERR, "%s: Found unknown route type %li",
                __func__, (long) rt_attr->rta_type );
      #endif
    }
  }

  return 0;

}  // nl_parse_routes

#endif  // !defined( USE_MBG_SOC )

#endif  // defined( MBG_TGT_LINUX )



/*HDR*/
/**
 * @brief Retrieve the IPv4 gateway (default route).
 *
 * @param[out] p_addr  Pointer to address field to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, @p *p_addr is set to 0.
 */
int get_ip4_gateway( IP4_ADDR *p_addr )
{
  int rc = MBG_ERR_NOT_SUPP_ON_OS;

#if defined( MBG_TGT_LINUX )

#if !defined( USE_MBG_SOC)
  struct nlmsghdr *nlmsg;
  struct route_info route_info;
  char msg_buf[8192];   // TODO Pretty large buffer, why 8192 ?
  int sock_fd;
  int len;
  int msg_seq = 0;
  int idx;

  // Create socket.
  if ( ( sock_fd = socket( PF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE ) ) == -1 )
  {
    rc = mbg_get_last_socket_error( NULL );
    #if DEBUG_NETLINK
      mbglog( LOG_ERR, "%s: Failed to create netlink socket: %s",
              __func__, mbg_strerror( rc ) );
    #endif
    goto out;
  }


  // Initialize a buffer with a request message.
  memset( msg_buf, 0, sizeof( msg_buf ) );

  // Point the header and the msg structure pointers into the buffer.
  nlmsg = (struct nlmsghdr *) msg_buf;

  // Fill in the nlmsg header.
  nlmsg->nlmsg_len = NLMSG_LENGTH( sizeof( struct rtmsg ) );  // Length of message.
  nlmsg->nlmsg_type = RTM_GETROUTE;                 // Get the routes from kernel routing table.

  nlmsg->nlmsg_flags = NLM_F_DUMP | NLM_F_REQUEST;  // The message is a request for dump.
  nlmsg->nlmsg_seq = msg_seq++;                     // Sequence of the message packet.
  nlmsg->nlmsg_pid = getpid();                      // PID of process sending the request.

  // Send the request.
  if ( send( sock_fd, nlmsg, nlmsg->nlmsg_len, 0 ) == -1 )
  {
    rc = mbg_get_last_socket_error( NULL );
    #if DEBUG_NETLINK
      mbglog( LOG_ERR, "%s: Failed to write to netlink socket: %s",
              __func__, mbg_strerror( rc ) );
    #endif
    goto out;
  }

  // Read the response.
  if ( ( len = nl_read( sock_fd, msg_buf, sizeof( msg_buf ), msg_seq, getpid() ) ) < 0 )
  {
    #if DEBUG_NETLINK
      mbglog( LOG_ERR, "%s: Failed to read from netlink socket",
              __func__ );
    #endif
    rc = len;
    goto out;
  }

  #if DEBUG_NETLINK
    mbglog( LOG_ERR, "%s: Read %i bytes from netlink socket", __func__, len );
  #endif

  // Parse and print the response.
  for ( idx = 0; NLMSG_OK( nlmsg, len ); nlmsg = NLMSG_NEXT( nlmsg, len ), idx++ )
  {
    memset( &route_info, 0, sizeof( route_info ) );

    #if DEBUG_NETLINK
      mbglog( LOG_ERR, "\nnl_msg at %p, %i bytes left", nlmsg, len );
    #endif


    if ( nl_parse_routes( nlmsg, &route_info ) < 0 )
      continue;  // Don't check route_info if it has not been set up.

    #if DEBUG_NETLINK
    {
      char ws[100];
      int l = sizeof( ws );
      int n = 0;

      // inet_ntoa() uses a static buffer which is overwritten on every call.
      // TODO Rather than inet_ntoa, use inet_ntop which can also handle IPv6.
      n += snprintf_safe( &ws[n], l - n, "src %s", (char *) inet_ntoa( route_info.srcAddr ) );
      n += snprintf_safe( &ws[n], l - n, ", dst %s", (char *) inet_ntoa( route_info.dstAddr ) );
      n += snprintf_safe( &ws[n], l - n, ", gw %s", (char *) inet_ntoa( route_info.gateWay ) );

      mbglog( LOG_ERR, "%s: route %i: %s, if \"%s\"",
              __func__, idx, ws, route_info.ifName );
    }
    #endif

    // Check if default IPv4 gateway.
    // TODO Rather than inet_ntoa, use inet_ntop which can also handle IPv6.
    if ( strstr( (char *) inet_ntoa( route_info.dstAddr ), "0.0.0.0" ) )
    {
      *p_addr = ntohl( route_info.gateWay.s_addr );
      rc = MBG_SUCCESS;
      // Actually we could stop searching now. However, in case of debug
      // we'll continue to examine the rest of the routing message.
      #if DEBUG_NETLINK
      // TODO Rather than inet_ntoa, use inet_ntop which can also handle IPv6
        mbglog( LOG_ERR, "%s: Default gateway found: %s",
                __func__, (char *) inet_ntoa( route_info.gateWay ) );
      #else
        break;
      #endif
    }
  }

out:
  if ( sock_fd >= 0 )
    close( sock_fd );

#else

  IP4_ADDR dest, gateway, mask;
  unsigned int flags, refcnt, use, metric, mtu, window, irtt;
  int fret = 0;

  char name[ IFNAMSIZ ];

  FILE* f = fopen( "/proc/net/route", "r" );

  if( !f )
  {
    rc = MBG_ERR_IO;
    goto out;
  }

  while (fret != EOF)
  {
    fret = fscanf( f, "%s %08X %08X %04X %u %u %u %08X %u %u %u ",
        name, (uint32_t*) &dest, (uint32_t*) &gateway, &flags, &refcnt, &use, &metric,(uint32_t*) &mask, &mtu, &window, &irtt );

    if ( fret == 11 )
    {
      if (dest == 0 && gateway != 0)
      {
        *p_addr = ntohl(gateway);
        rc = MBG_SUCCESS;
        break;
      }
    }
  }

  fclose(f);

out:

#endif

#endif  // defined( MBG_TGT_LINUX )

  if ( mbg_rc_is_error( rc ) )
    *p_addr = 0;

  return rc;

}  // get_ip4_gateway



/*HDR*/
/**
 * @brief Retrieve the IPv4 address of a network interface as string.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_addr_buf  Pointer to the string buffer to be filled up.
 * @param[in]  buf_size    Size of the string buffer.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, a string "0.0.0.0" is generated.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
int get_port_ip4_addr_str( const char *if_name, char *p_addr_buf, int buf_size )
{
  IP4_ADDR addr;

  int rc = get_port_ip4_addr( if_name, &addr );

  snprint_ip4_addr( p_addr_buf, buf_size, &addr, NULL );

  return rc;

}  // get_port_ip4_addr_str



/*HDR*/
/**
 * @brief Retrieve the IPv4 net mask of a network interface as string.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_addr_buf  Pointer to the string buffer to be filled up.
 * @param[in]  buf_size    Size of the string buffer.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, a string "0.0.0.0" is generated.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
int get_port_ip4_netmask_str( const char *if_name, char *p_addr_buf, int buf_size )
{
  IP4_ADDR addr;

  int rc = get_port_ip4_netmask( if_name, &addr );

  snprint_ip4_addr( p_addr_buf, buf_size, &addr, NULL );

  return rc;

}  // get_port_ip4_netmask_str



/*HDR*/
/**
 * @brief Retrieve the IPv4 broadcast address of a network interface as string.
 *
 * @param[in]  if_name     Name of the interface.
 * @param[out] p_addr_buf  Pointer to the string buffer to be filled up.
 * @param[in]  buf_size    Size of the string buffer.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *          On error, a string "0.0.0.0" is generated.
 *
 * @see ::get_port_ip4_settings
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_specific_port_ip4_addr
 */
int get_port_ip4_broad_addr_str( const char *if_name, char *p_addr_buf, int buf_size )
{
  IP4_ADDR addr;

  int rc = get_port_ip4_broad_addr( if_name, &addr );

  snprint_ip4_addr( p_addr_buf, buf_size, &addr, NULL );

  return rc;

}  // get_port_ip4_broad_addr_str



/*HDR*/
/**
 * @brief Retrieve the current IPv4 settings of a network interface.
 *
 * @param[in]  if_name  Name of the interface.
 * @param[out] p        Pointer to a ::IP4_SETTINGS structure to be filled up.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 *
 * @see ::get_port_ip4_addr
 * @see ::get_port_ip4_addr_str
 * @see ::get_port_ip4_netmask
 * @see ::get_port_ip4_netmask_str
 * @see ::get_port_ip4_broad_addr
 * @see ::get_port_ip4_broad_addr_str
 * @see ::get_specific_port_ip4_addr
 */
int get_port_ip4_settings( const char *if_name, IP4_SETTINGS *p )
{
  int link_up;
  int rc;

  memset( p, 0, sizeof( *p ) );

  rc = get_port_ip4_addr( if_name, &p->ip_addr );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  rc = get_port_ip4_netmask( if_name, &p->netmask );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  rc = get_port_ip4_broad_addr( if_name, &p->broad_addr );

  if ( mbg_rc_is_error( rc ) )
    goto out;


  rc = get_ip4_gateway( &p->gateway );

  if ( mbg_rc_is_error( rc ) )
    goto out;

#if 0  // TODO
  // We could also try to check VLAN and DHCP settings here,
  // but as of now, this is specific to an application.

  // TODO The VLAN and DHCP status info collected below
  // just return what has been configured previously by this program,
  // however, it does not reflect any changes which have been made
  // manually, e.g. via the command line.
  if ( vlan_enabled )
  {
    p->flags |= IP4_MSK_VLAN;
    p->vlan_cfg = vlan_cfg;
  }

  if ( dhcp_enabled )
    p->flags |= IP4_MSK_DHCP;
#endif

out:

  // Check link state regardless of the result of getting IP4 parameters.
  link_up = check_port_link( if_name );

  if ( link_up )
    p->flags |= IP4_MSK_LINK;

  return rc;

}  // get_port_ip4_settings


