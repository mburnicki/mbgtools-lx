
/**************************************************************************
 *
 *  $Id: mbg_pps.h 1.1 2021/12/01 19:00:32 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and function prototypes for mbg_pps.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_pps.h $
 *  Revision 1.1  2021/12/01 19:00:32  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_PPS_H
#define _MBG_PPS_H


/* Other headers to be included */

#include <mbgserio.h>
#include <mbgsystm.h>
#include <mbg_syslog.h>

#if defined( MBG_TGT_LINUX )
  #include <timepps.h>
  #include <string.h>
  #include <syslog.h>
#endif

#ifdef _MBG_PPS
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if defined( DEBUG )
  #define DEBUG_PPS  1  // Can be 0 or > 0.
#else
  #define DEBUG_PPS  0  // Always 0.
#endif


#ifdef __cplusplus
extern "C" {
#endif


#define PPS_DEVICE_NAME_BASE     "pps"
#define PPS_DEVICE_PATH_BASE     "/dev/" PPS_DEVICE_NAME_BASE
#define PPS_DEVICE_PATH_FMT      PPS_DEVICE_PATH_BASE "%i"
#define PPS_DEVICE_PATH_MAX_LEN  16  // Arbitrary, 10 + 1 for terminating 0 shou

#if !defined( DEFAULT_PPS_DEVICE_NAME )
  // For applications that support 2 PPS inputs, we also
  // define default names for the second input to be used.
  #if defined( MBG_TGT_LINUX )
    #define DEFAULT_PPS_DEVICE_NAME   PPS_DEVICE_NAME_BASE "0"
    #define DEFAULT_PPS_DEVICE_NAME_2 PPS_DEVICE_NAME_BASE "1"
  #else
    #error DEFAULT_PPS_DEVICE_NAME needs to be defined for this platform!
  #endif
#endif


/**
 * @brief Summary of information and settings of a PPS device.
 */
typedef struct
{
  char dev_name[PPS_DEVICE_PATH_MAX_LEN];  ///< PPS device file name.
  int fd;                                  ///< PPS device file descriptor.

  pps_handle_t handle;    ///< Handle to be used for PPS API calls.
  int supp_modes;         ///< Supported PPS modes retrieved from the device

  pps_info_t info;        ///< Storage for retrieved time stamp info.

} PPS_DEV_INFO;



_ext const char pps_name_base[]
#ifdef _DO_INIT
 = "pps"
#endif
;


/**
 * @brief Pointer to the log function used by @a mbg_pidfile.  FIXME TODO
 *
 * Defaults to @a syslog, but can be changed to a different function
 * that is called in the same way.
 */
_ext MBG_SYSLOG_FNC *mbg_pps_log_fnc
#if defined( _DO_INIT )
  = syslog
#endif
;



/**
 * @brief Check if a device name refers to a PPS device.
 *
 * @param[in]  dev_name  A string with the device name.
 *
 * @see ::DEFAULT_PPS_DEVICE_NAME
 *
 * @return @a true if the device name contains the name of a serial port, else @a false.
 */
static __mbg_inline /*HDR*/
bool device_name_is_pps( const char *dev_name )
{
  #if defined( MBG_TGT_LINUX )
    return strstr( dev_name, "/dev/pps" ) != NULL;  // standard PPS device
  #else
    #error device_id_is_serial() needs to be implemented for this platform!
  #endif

}  // device_name_is_pps



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Initialize a @ref PPS_DEV_INFO structure.
 *
 * Generate the device file name from the PPS device index @p pps_idx.
 *
 * Open the PPS device and create a handle that can be used
 * with further API calls.
 *
 * Check if the preferred @p mode is supported, and set it.
 *
 * @param[out]  p_pps_di  Pointer to the structure to be initialized.
 * @param[in]   pps_idx   Index of the PPS device, used to create the
 *                        PPS device file name.
 * @param[in]   mode      PPS mode flags like PPS_CAPTUREASSERT,
 *                        defined in timepps.h.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_pps_init( PPS_DEV_INFO *p_pps_di, int pps_idx, int mode ) ;

 int mbg_pps_find_attached_pps( const char *dev_name ) ;
 /**
 * @brief Attach a PPS source to a serial device.
 *
 * This may require root privileges.
 *
 * @param[in]  sdev  Parameters of an opened serial device.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_pps_attach_to_serial( const MBGSERIO_DEV *sdev ) ;

 /**
 * @brief Set up a @ref PPS_DEV_INFO structure for a serial port.
 *
 * First looks if a PPS source is already attached to the
 * specified serial port.
 *
 * If it is not, tries to attach a PPS source and tries to find
 * the newly attached PPS source.
 *
 * @param[out]  p_pps_di  Pointer to the structure to be set up.
 * @param[in]   dev_name  Device file name of the serial port.
 * @param[in]   sdev      Parameters of an opened serial device with name @p dev_name.
 * @param[in]   mode      PPS mode flags like PPS_CAPTUREASSERT,
 *                        defined in timepps.h.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_pps_setup_pps_for_serial( PPS_DEV_INFO *p_pps_di, const char *dev_name, const MBGSERIO_DEV *sdev, int mode ) ;

 /**
 * @brief Fetch a PPS time stamp from the kernel.
 *
 * The time stamp is returned via @p p_t_pps. If no
 * valid time stamp can be retrieved, the variable
 * pointed to by @p p_t_pps is set to all 0.
 *
 * @param[out]     p_t_pps   Address of a ::MBG_SYS_TIME structure to take the time stamp.
 * @param[in,out]  p_pps_di  Address of a ::PPS_DEV_INFO structure that contains the PPS info
 *                           and stores the data retrieved from the kernel.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_pps_fetch_ts( MBG_SYS_TIME *p_t_pps, PPS_DEV_INFO *p_pps_di ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_PPS_H */
