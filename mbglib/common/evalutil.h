
/**************************************************************************
 *
 *  $Id: evalutil.h 1.1 2021/04/12 22:07:28 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and prototypes for evalutil.c, which provides
 *  utility functions to evaluate parameter strings.
 *
 * -----------------------------------------------------------------------
 *  $Log: evalutil.h $
 *  Revision 1.1  2021/04/12 22:07:28  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _EVALUTIL_H
#define _EVALUTIL_H


/* Other headers to be included */

#ifdef _EVALUTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Convert a string with hours, minutes, seconds to a time offset.
 *
 * @param[in]  s  A string to be evaluated, format "[-]hh[:mm[:ss]]".
 *
 * @return  The (signed) time offset decoded from the string, in seconds.
 */
 long time_offs_secs_from_string( const char *s ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _EVALUTIL_H */
