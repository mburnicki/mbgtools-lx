
/**************************************************************************
 *
 *  $Id: pcpststr.c 1.2 2021/12/01 10:17:47 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Combined support for serial time strings and bus-level devices.
 *
 *  See mbg_tstr.c if bus-level device support is not required.
 *
 * -----------------------------------------------------------------------
 *  $Log: pcpststr.c $
 *  Revision 1.2  2021/12/01 10:17:47  martin.burnicki
 *  Use the PPS system time stamp, if available.
 *  Revision 1.1  2021/04/21 11:40:49  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _PCPSTSTR
  #include <pcpststr.h>
#undef _PCPSTSTR



/*HDR*/
/**
 * @brief Convert time string data to the format used with bus-level devices.
 *
 * Data in @p *p_tsri should have been set up when the function
 * ::mbg_tstr_receive successfully received and decoded a specific
 * time string.
 *
 * Information is then converted to data formats used with bus-level devices
 * to make further processing easier in associated applications.
 *
 * @param[out]  p_cti   Address of an ::MBG_CHK_TIME_INFO structure to be filled.
 * @param[in]   p_tsri  Pointer to the data to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
int mbg_rcv_info_to_check_time_info( MBG_CHK_TIME_INFO *p_cti,
                                     const MBG_TSTR_RCV_INFO *p_tsri )
{
  const MBG_TSTR_DATA *p_data = &p_tsri->tstr_data;
  const TSTR_STATUS *p_st = &p_data->status;
  PCPS_HR_TIME *p_ref_t;

  memset( p_cti, 0, sizeof( *p_cti ) );


  // Set up ref time and status.

  p_ref_t = &p_cti->hrti.ref_hr_time_cycles.t;  // FIXME TODO
  p_ref_t->tstamp.sec = p_tsri->tstr_data.t64;
  // p_ref_t->tstamp.frac is always 0.

  if ( *p_st & TSTR_ST_UTC_OFFS_VALID )
  {
    if ( p_data->utc_offs )
    {
      // If the time from the string is not UTC, we
      // save the UTC offset and adjust the timestamp.
      p_ref_t->utc_offs = p_data->utc_offs;
      p_ref_t->tstamp.sec -= p_data->utc_offs;
    }
    else  // The UTC offset is 0.
      p_ref_t->status |= PCPS_UTC;
  }


  if ( *p_st & TSTR_ST_IS_SYNC )
    p_ref_t->status |= PCPS_SYNCD;
  else
    p_ref_t->status |= PCPS_FREER;

  if ( *p_st & TSTR_ST_DL_ENB )
    p_ref_t->status |= PCPS_DL_ENB;

  if ( *p_st & TSTR_ST_DL_ANN )
    p_ref_t->status |= PCPS_DL_ANN;

  if ( *p_st & TSTR_ST_LS_ENB )
    p_ref_t->status |= PCPS_LS_ENB;

  if ( *p_st & TSTR_ST_LS_ANN )
    p_ref_t->status |= PCPS_LS_ANN;

  if ( *p_st & TSTR_ST_INV_TIME )
    p_ref_t->status |= PCPS_INVT;

  // TODO Status information not yet converted / available:
  // - PCPS_IFTM
  // - PCPS_ANT_FAIL
  // - PCPS_LS_ANN_NEG
  // - PCPS_SCALE_GPS
  // - PCPS_SCALE_TAI
  //
  // Reference time characteristics:
  // p_ref_t->signal could reflect if signal/string available, or timeout.

  if ( !_mbg_sys_timestamp_is_empty( &p_tsri->ts_pps ) )
  {
    // Use the PPS time stamp of the system time.
    p_cti->hrti.sys_time_cycles.sys_time = p_tsri->ts_pps;
  }
  else
  {
    // Use the compensated receive time stamp.
    p_cti->hrti.sys_time_cycles.sys_time = p_tsri->ts_rcv_comp;
  }

  return MBG_SUCCESS;

}  // mbg_rcv_info_to_check_time_info

