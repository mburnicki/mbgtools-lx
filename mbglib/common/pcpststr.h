
/**************************************************************************
 *
 *  $Id: pcpststr.h 1.1 2021/04/21 11:40:45 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and prototypes for pcpststr.c, which provides
 *  combined support for serial time strings and bus-level devices.
 *
 *  See mbg_tstr.c if bus-level device support is not required.
 *
 * -----------------------------------------------------------------------
 *  $Log: pcpststr.h $
 *  Revision 1.1  2021/04/21 11:40:45  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _PCPSTSTR_H
#define _PCPSTSTR_H


/* Other headers to be included */

#include <mbg_tstr.h>       // Serial time string stuff.
#include <chk_time_info.h>  // Bus level device stuff.


#ifdef _PCPSTSTR
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Convert time string data to the format used with bus-level devices.
 *
 * Data in @p *p_tsri should have been set up when the function
 * ::mbg_tstr_receive successfully received and decoded a specific
 * time string.
 *
 * Information is then converted to data formats used with bus-level devices
 * to make further processing easier in associated applications.
 *
 * @param[out]  p_cti   Address of an ::MBG_CHK_TIME_INFO structure to be filled.
 * @param[in]   p_tsri  Pointer to the data to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_rcv_info_to_check_time_info( MBG_CHK_TIME_INFO *p_cti, const MBG_TSTR_RCV_INFO *p_tsri ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _PCPSTSTR_H */
