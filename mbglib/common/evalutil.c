
/**************************************************************************
 *
 *  $Id: evalutil.c 1.1 2021/04/12 22:07:26 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Utility functions to evaluate parameter strings.
 *
 * -----------------------------------------------------------------------
 *  $Log: evalutil.c $
 *  Revision 1.1  2021/04/12 22:07:26  martin
 *  Initial revision.
 *
 **************************************************************************/

#define _EVALUTIL
  #include <evalutil.h>
#undef _EVALUTIL

#include <words.h>
#include <mbgtime.h>

#include <stdlib.h>



/*HDR*/
/**
 * @brief Convert a string with hours, minutes, seconds to a time offset.
 *
 * @param[in]  s  A string to be evaluated, format "[-]hh[:mm[:ss]]".
 *
 * @return  The (signed) time offset decoded from the string, in seconds.
 */
long time_offs_secs_from_string( const char *s )
{
  long offs_secs;
  long tmp;
  bool is_negative = false;
  const char *cp = s;
  char *endp;

  // In case of e.g. "-0:30", conversion of the first part "-0" would yield
  // just 0, so the sign would get lost. Thus we explicitly check the sign.

  // Skip leading spaces.
  while ( *cp == ' ' )
    cp++;

  // Skip optional plus sign.
  while ( *cp == '+' )
    cp++;

  // Eval optional minus sign.
  if ( *cp == '-' )
  {
    is_negative = true;
    cp++;
  }


  // Expect a value for hours.
  tmp = strtol( cp, &endp, 10 );

  if ( endp == cp )  // No number found.
    goto invalid;


  offs_secs = tmp * SECS_PER_HOUR;
  cp = endp;


  if ( *cp++ != ':' )  // No additional field for minutes.
    goto done;


  // Expect a value for minutes.
  tmp = strtol( cp, &endp, 10 );

  if ( endp == cp )  // No number found.
    goto invalid;

  // The value after the colon should always be positive.
  if ( tmp < 0 || tmp >= MINS_PER_HOUR )
    goto invalid;


  offs_secs += tmp * SECS_PER_MIN;
  cp = endp;

  if ( *cp++ != ':' )  // No additional field for seconds.
    goto done;


  // Expect a value for seconds.
  tmp = strtol( cp, &endp, 10 );

  if ( endp == cp )  // No number found.
    goto invalid;

  // The value behind the colon should always be positive.
  if ( tmp < 0 || tmp >= SECS_PER_MIN )
    goto invalid;


  offs_secs += tmp;

done:
  if ( is_negative )
    offs_secs = -offs_secs;

  goto out;


invalid:
  offs_secs = 0;

out:
  return offs_secs;

}  // time_offs_secs_from_string


