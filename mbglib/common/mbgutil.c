
/**************************************************************************
 *
 *  $Id: mbgutil.c 1.18 2022/12/21 14:58:15 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Utility function used by Meinberg driver softwares.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgutil.c $
 *  Revision 1.18  2022/12/21 14:58:15  martin.burnicki
 *  Removed some obsolete _int_from_size_t() stuff.
 *  Revision 1.17  2022/08/23 17:30:49  martin.burnicki
 *  Quieted a compiler warning.
 *  Revision 1.16  2021/11/08 20:01:37  martin.burnicki
 *  In mbg_str_dev_name(), print build num in case
 *  of SYN1588 device.
 *  Revision 1.15  2021/04/29 13:00:24  martin
 *  Removed obsolete local variable mbg_year_lim, and
 *  use a global variable mbg_exp_year_limit instead.
 *  Updated some doxygen comments, and use common
 *  types for parameters.
 *  Revision 1.13  2021/03/22 10:13:45  martin
 *  Updated a bunch of comments.
 *  Revision 1.12  2021/03/12 12:32:10  martin
 *  Updated some comments.
 *  Revision 1.11  2020/02/27 13:41:41  martin
 *  Removed inclusion of obsolete header pcpsutil.h.
 *  Account for a renamed library function.
 *  Doxygen updates.
 *  Revision 1.10  2018/09/21 14:59:44  martin
 *  Fixed return value for mbg_snprintf().
 *  Revision 1.9  2018/08/13 14:53:40  martin
 *  Doxygen updates.
 *  Revision 1.8  2018/06/25 12:30:14Z  martin
 *  Use int rather than size_t in many places internally.
 *  Use predefined format string for firmware version string.
 *  Use macros to get major / minor version numbers.
 *  Revision 1.7  2017/07/05 16:40:35Z  martin
 *  Use functions from new module timeutil.
 *  Use safe string functions from str_util.c.
 *  Quieted some compiler warninges.
 *  Account for frac_sec_from_bin() obsoleted by bin_frac_32_to_dec_frac().
 *  Account for renamed library symbols.
 *  Added doxygen comments.
 *  Revision 1.6  2012/10/15 13:12:17  martin
 *  Fixed build on DOS.
 *  Fixed format/type mismatch when printing UTC offset.
 *  Fixed generation of firmware and ASIC version string.
 *  Revision 1.5  2009/03/19 09:09:55Z  daniel
 *  Fixed ambiguous syntax in mbg_str_dev_name().
 *  Support TAI and GPS time scales in mbg_str_pcps_hr_tstamp_loc().
 *  In mbg_str_pcps_hr_time_offs() append offset only if != 0.
 *  Revision 1.4  2009/01/12 09:34:12Z  daniel
 *  Added function mbg_str_dev_name().
 *  Revision 1.3  2006/05/10 10:57:44Z  martin
 *  Generally use and export mbg_snprintf().
 *  Revision 1.2  2005/02/18 15:12:13Z  martin
 *  Made functions mbg_strncpy() and mbg_strchar() public.
 *  New function mbg_str_tm_gps_date_time().
 *  Don't expand year number if already expanded.
 *  Revision 1.1  2005/02/18 10:39:49Z  martin
 *  Initial revision
 *
 **************************************************************************/

#define _MBGUTIL
 #include <mbgutil.h>
#undef _MBGUTIL

#include <cfg_hlp.h>
#include <mbgerror.h>
#include <charcode.h>
#include <pcpsdev.h>
#include <str_util.h>
#include <qsdefs.h>

#include <stdio.h>
#include <time.h>
#include <limits.h>

// Required at least for Linux:
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>


#if defined( MBG_TGT_WIN32 )
  #include <tchar.h>
#else
//  #include <>
#endif


static int mbg_date_time_dist = 2;
//##++ static int mbg_time_tz_dist = 1;
static int mbg_pos_dist = 2;

static const char str_inv_cnv[] = "(invalid conversion)";



/*HDR*/
/**
 * @brief Get the version number of the precompiled DLL/shared object library.
 *
 * If this library is used as a DLL/shared object library, the version.
 * number can be checked to see if the header files which are actually used.
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API function are called
 * in the correct way.
 *
 * @return The version number.
 *
 * @see ::mbgutil_check_version
 * @see ::MBGUTIL_VERSION defined in mbgutil.h
 */
_MBG_API_ATTR int _MBG_API mbgutil_get_version( void )
{
  return MBGUTIL_VERSION;

}  // mbgutil_get_version



/*HDR*/
/**
 * @brief Check if the DLL/shared library is compatible with a given version.
 *
 * If this library is used as a DLL/shared object library, the version
 * number can be checked to see if the header files which are actually used
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API functions are called
 * in the correct way.
 *
 * @param[in] header_version  Version number to be checked, should be ::MBGUTIL_VERSION
 *                            from the mbgutil.h file version used to build the application.
 *
 * @return ::MBG_SUCCESS if compatible, else ::MBG_ERR_LIB_NOT_COMPATIBLE.
 *
 * @see ::mbgutil_get_version
 * @see ::MBGUTIL_VERSION defined in mbgutil.h
 */
_MBG_API_ATTR int _MBG_API mbgutil_check_version( int header_version )
{
  if ( header_version >= MBGUTIL_COMPAT_VERSION )
    return MBG_SUCCESS;

  return MBG_ERR_LIB_NOT_COMPATIBLE;

}  // mbgutil_check_version



/*HDR*/
/**
 * @brief A portable, safer implementation of snprintf().
 *
 * The output string buffer is in any case properly terminated by 0.
 * For a detailed description see ::vsnprintf_safe.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   fmt      Format string according to subsequent parameters.
 * @param[in]   ...      Variable argument list according to the @p fmt format string.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::vsnprintf_safe
 * @see ::snprintf_safe
 */
__attribute__( ( format( printf, 3, 4 ) ) )
_MBG_API_ATTR int _MBG_API mbg_snprintf( char *s, size_t max_len, const char * fmt, ... )
{
  int n;
  va_list args;

  va_start( args, fmt );
  n = vsnprintf_safe( s, max_len, fmt, args );
  va_end( args );

  return n;

}  // mbg_snprintf



/*HDR*/
/**
 * @brief A portable, safe implementation of strncpy().
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the destination string buffer.
 * @param[in]   max_len  Size of the destination string buffer.
 * @param[in]   src      Pointer to the source string buffer.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_strncpy( char *s, size_t max_len, const char *src )
{
  return sn_cpy_str_safe( s, max_len, src );

}  // mbg_strncpy



/*HDR*/
/**
 * @brief Write a character multiple times to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   c        The character to write to the output buffer.
 * @param[in]   n        The number of characters to write to the output buffer.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_strchar( char *s, size_t max_len, char c, size_t n )
{
  size_t i;

  max_len--;

  for ( i = 0; i < n; i++ )
  {
    if ( i >= max_len )
      break;

    if ( i >= ( INT_MAX - 1 ) )
      break;

    s[i] = c;
  }

  s[i] = 0;

  return (int) i;

}  // mbg_strchar



/*HDR*/
/**
 * @brief Write a short date string "dd.mm." to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   mday     Day-of-month number, 1..31.
 * @param[in]   month    Month number, 1..12.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_date_short( char *s, int max_len,
                                               int mday, int month )
{
  int n = snprintf_safe( s, max_len, "%02u.%02u.", mday, month );

  return n;

}  // mbg_str_date_short



/*HDR*/
/**
 * @brief Write a date string "dd.mm.yyyy" to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   mday     Day-of-month number, 1..31.
 * @param[in]   month    Month number, 1..12.
 * @param[in]   year     Year number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_date( char *s, int max_len,
                                         int mday, int month, int year )
{
  int n = mbg_str_date_short( s, max_len, mday, month );

  n += snprintf_safe( &s[n], max_len - n, "%04u",
                      ( year < 256 ) ?
                        mbg_exp_year( (uint8_t) year, mbg_exp_year_limit ) :
                        year );

  return n;

}  // mbg_str_date



/*HDR*/
/**
 * @brief Write a short time string "hh:mm" to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   hour     Hours number, 0..23.
 * @param[in]   min      Minutes number, 0..59.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_time_short( char *s, int max_len,
                                               int hour, int min )
{
  return snprintf_safe( s, max_len, "%2u:%02u", hour, min );

}  // mbg_str_time_short



/*HDR*/
/**
 * @brief Write a time string "hh:mm:ss" to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   hour     Hours number, 0..23.
 * @param[in]   min      Minutes number, 0..59.
 * @param[in]   sec      Seconds number, 0..59, or 60 in case of leap second.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_time( char *s, int max_len,
                                         int hour, int min, int sec )
{
  int n = mbg_str_time_short( s, max_len, hour, min );
  n += snprintf_safe( &s[n], max_len - n, ":%02u", sec );

  return n;

}  // mbg_str_time



/*HDR*/
/**
 * @brief Write a long time string "hh:mm:ss.cc" to a string buffer.
 *
 * Include 100ths of seconds.
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   hour     Hours number, 0..23.
 * @param[in]   min      Minutes number, 0..59.
 * @param[in]   sec      Seconds number, 0..59, or 60 in case of leap second.
 * @param[in]   sec100   Hundreths of seconds, 0..99.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_time_long( char *s, int max_len,
                                              int hour, int min, int sec, int sec100 )
{
  int n = mbg_str_time( s, max_len, hour, min, sec );
  n += snprintf_safe( &s[n], max_len - n, ".%02u", sec100 );

  return n;

}  // mbg_str_time_long



/*HDR*/
/**
 * @brief Write a full date and time string to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::TM_GPS structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_tm_gps_date_time( char *s, int max_len,
                                                     const TM_GPS *pt )
{
  int n = mbg_str_date( s, max_len, pt->mday, pt->month, pt->year );
  n += mbg_strchar( &s[n], max_len - n, ' ', mbg_date_time_dist );
  n += mbg_str_time( &s[n], max_len - n, pt->hour, pt->min, pt->sec );

  return n;

}  // mbg_str_tm_gps_date_time



/*HDR*/
/**
 * @brief Write the short date given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_date_short( char *s, int max_len,
                                                    const PCPS_TIME *pt )
{
  return mbg_str_date_short( s, max_len, pt->mday, pt->month );

}  // mbg_str_pcps_date_short



/*HDR*/
/**
 * @brief Write the date given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_date( char *s, int max_len,
                                              const PCPS_TIME *pt )
{
  return mbg_str_date( s, max_len, pt->mday, pt->month, pt->year );

}  // mbg_str_pcps_date



/*HDR*/
/**
 * @brief Write the short time given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_time_short( char *s, int max_len,
                                                    const PCPS_TIME *pt )
{
  return mbg_str_time_short( s, max_len, pt->hour, pt->min );

}  // mbg_str_pcps_time_short



/*HDR*/
/**
 * @brief Write the time given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_time( char *s, int max_len,
                                              const PCPS_TIME *pt )
{
  return mbg_str_time( s, max_len, pt->hour, pt->min, pt->sec );

}  // mbg_str_pcps_time



/*HDR*/
/**
 * @brief Write the time including sec100ths given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_time_long( char *s, int max_len,
                                                   const PCPS_TIME *pt )
{
  return mbg_str_time_long( s, max_len, pt->hour, pt->min, pt->sec, pt->sec100 );

}  // mbg_str_pcps_time_long



/*HDR*/
/**
 * @brief Write date and time given as ::PCPS_TIME structure to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 * @param[in]   tz_str   Optional time zone string to be appended, currently not used, may be @a NULL.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_date_time( char *s, int max_len,
                                                   const PCPS_TIME *pt,
                                                   const char *tz_str )
{
  int n = mbg_str_pcps_date( s, max_len, pt );
  n += mbg_strchar( &s[n], max_len - n, ' ', mbg_date_time_dist );
  n += mbg_str_pcps_time( &s[n], max_len - n, pt );

  // FIXME Use tz_str ?
  (void) tz_str;

  return n;

}  // mbg_str_pcps_date_time



/*HDR*/
/**
 * @brief Write date derived from seconds-since-epoch to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   sec      Number of seconds since the epoch to be converted to date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_date( char *s, int max_len,
                                                 PCPS_SECONDS sec )
{
  struct tm tm = { 0 };
  time_t t = cvt_to_time_t( sec );
  int rc = mbg_gmtime( &tm, &t );

  return mbg_rc_is_success( rc ) ?
           mbg_str_date( s, max_len, tm.tm_mday, tm.tm_mon + 1, tm.tm_year ) :
           sn_cpy_str_safe( s, max_len, str_inv_cnv );

}  // mbg_str_pcps_hr_date



/*HDR*/
/**
 * @brief Write time derived from seconds-since-epoch to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   sec      Number of seconds since the epoch to be converted to date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time( char *s, int max_len,
                                                 PCPS_SECONDS sec )
{
  struct tm tm = { 0 };
  time_t t = cvt_to_time_t( sec );
  int rc = mbg_gmtime( &tm, &t );

  return mbg_rc_is_success( rc ) ?
           mbg_str_time( s, max_len, tm.tm_hour, tm.tm_min, tm.tm_sec ) :
           sn_cpy_str_safe( s, max_len, str_inv_cnv );

}  // mbg_str_pcps_hr_time



static /*HDR*/
/* (explicitly excluded from Doxygen)
 * @brief Write %UTC date and time given as ::PCPS_HR_TIME structure to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   p_t      Pointer to a time_t variable providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
int do_str_pcps_hr_date_time( char *s, int max_len, const time_t *p_t )
{
  struct tm tm = { 0 };
  int n = 0;
  int rc = mbg_gmtime( &tm, p_t );

  if ( mbg_rc_is_success( rc ) )
  {
    n = mbg_str_date( s, max_len, tm.tm_mday , tm.tm_mon + 1, tm.tm_year );
    n += mbg_strchar( &s[n], max_len - n, ' ', mbg_date_time_dist );
    n += mbg_str_time( &s[n], max_len - n, tm.tm_hour, tm.tm_min, tm.tm_sec );
  }
  else
    n = sn_cpy_str_safe( s, max_len, str_inv_cnv );

  return n;

}  // do_str_pcps_hr_date_time



/*HDR*/
/**
 * @brief Write %UTC date and time given as ::PCPS_HR_TIME structure to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_date_time_utc( char *s, int max_len,
                                                          const PCPS_HR_TIME *pt )
{
  time_t t = cvt_to_time_t( pt->tstamp.sec );

  return do_str_pcps_hr_date_time( s, max_len, &t );

}  // mbg_str_pcps_hr_date_time_utc



/*HDR*/
/**
 * @brief Write local date and time given as ::PCPS_HR_TIME structure to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_date_time_loc( char *s, int max_len,
                                                          const PCPS_HR_TIME *pt )
{
  long l = (long) pt->tstamp.sec + pt->utc_offs;
  time_t t = cvt_to_time_t( l );

  return do_str_pcps_hr_date_time( s, max_len, &t );

}  // mbg_str_pcps_hr_date_time_loc



/*HDR*/
/**
 * @brief Print binary ::PCPS_FRAC_32 fractions in decimal to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   frac     Binary fractions of a second in ::PCPS_FRAC_32 format.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time_frac( char *s, int max_len,
                                                      PCPS_FRAC_32 frac )
{
  return snprintf_safe( s, max_len, PCPS_HRT_FRAC_SCALE_FMT,
                        (ulong) bin_frac_32_to_dec_frac( frac, PCPS_HRT_FRAC_SCALE ) );

}  // mbg_str_pcps_hr_time_frac



/*HDR*/
/**
 * @brief Print the %UTC offset from a ::PCPS_HR_TIME structure to a string buffer.
 *
 * The output format is sign - hours - minutes, e.g. "+01:45h".
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date, and %UTC offset.
 * @param[in]   info     An informational text to be prepended.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time_offs( char *s, int max_len,
                                                      const PCPS_HR_TIME *pt,
                                                      const char *info )
{
  int n = snprintf_safe( s, max_len, "%s", info );

  if ( pt->utc_offs )
  {
    ldiv_t ldt = ldiv( labs( pt->utc_offs ) / 60, 60 );

    n += snprintf_safe( &s[n], max_len - n, "%c%02lu:%02luh",
                        ( pt->utc_offs < 0 ) ? '-' : '+',
                        ldt.quot, ldt.rem );
  }

  return n;

}  // mbg_str_pcps_hr_time_offs



/*HDR*/
/**
 * @brief Write a high resolution %UTC time stamp including fractions to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_tstamp_utc( char *s, int max_len,
                                                       const PCPS_HR_TIME *pt )
{
  int n = mbg_str_pcps_hr_date_time_utc( s, max_len, pt );

  if ( n < ( max_len - 1 ) )
  {
    s[n++] = '.';
    n += mbg_str_pcps_hr_time_frac( &s[n], max_len - n, pt->tstamp.frac );
  }

  return n;

}  // mbg_str_pcps_hr_tstamp_utc



/*HDR*/
/**
 * @brief Write a high resolution local time stamp including fractions to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_tstamp_loc( char *s, int max_len,
                                                       const PCPS_HR_TIME *pt )
{
  int n = mbg_str_pcps_hr_date_time_loc( s, max_len, pt );

  if ( n < ( max_len - 1 ) )
  {
    s[n++] = '.';
    n += mbg_str_pcps_hr_time_frac( &s[n], max_len - n, pt->tstamp.frac );
  }

  if ( n < ( max_len - 1 ) )
  {
    const char *cp = "UTC";

    if ( pt->status & PCPS_SCALE_TAI )
      cp = "TAI";
    else
      if ( pt->status & PCPS_SCALE_GPS )
        cp = "GPS";

    s[n++] = ' ';
    n += mbg_str_pcps_hr_time_offs( &s[n], max_len - n, pt, cp );
  }

  return n;

}  // mbg_str_pcps_hr_tstamp_loc



/*HDR*/
/**
 * @brief Write a raw high resolution time stamp to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_tstamp_raw( char *s, int max_len,
                                                    const PCPS_TIME_STAMP *pt )
{
  return snprintf_safe( s, max_len, "%08lX.%08lX", (ulong) pt->sec, (ulong) pt->frac );

}  // mbg_str_pcps_tstamp_raw



/*HDR*/
/**
 * @brief Write a raw high resolution time stamp plus converted local time to a string buffer.
 *
 * The output string also has the time status code appended as hex number.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time_raw( char *s, int max_len,
                                                     const PCPS_HR_TIME *pt )
{
  int n = mbg_str_pcps_tstamp_raw( s, max_len, &pt->tstamp );
  n += mbg_str_pcps_hr_time_offs( &s[n], max_len - n, pt, ", Loc: " );
  n += snprintf_safe( &s[n], max_len - n, ", st: 0x%04X", pt->status );

  return n;

}  // mbg_str_pcps_hr_time_raw



/*HDR*/
/**
 * @brief Write time capture / user capture time stamp to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure containing a user capture event.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_ucap( char *s, int max_len,
                                         const PCPS_HR_TIME *pt )
{
  int n = snprintf_safe( s, max_len, "CAP%u: ", pt->signal );
  n += mbg_str_pcps_hr_tstamp_loc( &s[n], max_len - n, pt );

  return n;

}  // mbg_str_ucap



/*HDR*/
/**
 * @brief Write a geographic coordinate in degrees - minutes - seconds to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pdms     Pointer to a ::DMS structure containing the coordinate.
 * @param[in]   prec     Number of digits of the geographic seconds after the decimal separator.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pos_dms( char *s, int max_len,
                                            const DMS *pdms, int prec )
{
  return snprintf_safe( s, max_len, "%c %i" DEG "%02i'%02.*f\"",
                        pdms->prefix, pdms->deg, pdms->min,
                        prec, pdms->sec );

}  // mbg_str_pos_dms



/*HDR*/
/**
 * @brief Write a position's altitude parameter to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   alt      The altitude parameter, in meters.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pos_alt( char *s, int max_len, double alt )
{
  return snprintf_safe( s, max_len, "%.0fm", alt );

}  // mbg_str_pos_alt



/*HDR*/
/**
 * @brief Write geographic coordinates to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   ppos     Pointer to a ::POS structure containing the coordinates.
 * @param[in]   prec     Number of digits of the geographic seconds after the decimal separator.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_pos( char *s, int max_len,
                                        const POS *ppos, int prec )
{
  int n;

  if ( ppos->lla[LON] || ppos->lla[LAT] || ppos->lla[ALT] )
  {
    n = mbg_str_pos_dms( s, max_len, &ppos->latitude, prec );
    n += mbg_strchar( &s[n], max_len - n, ',', 1 );
    n += mbg_strchar( &s[n], max_len - n, ' ', mbg_pos_dist );
    n += mbg_str_pos_dms( &s[n], max_len - n, &ppos->longitude, prec );
    n += mbg_strchar( &s[n], max_len - n, ',', 1 );
    n += mbg_strchar( &s[n], max_len - n, ' ', mbg_pos_dist );
    n += mbg_str_pos_alt( &s[n], max_len - n, ppos->lla[ALT] );
  }
  else
    n = mbg_strncpy( s, max_len, str_not_avail );

  return n;

}  // mbg_str_pos



/*HDR*/
/**
 * @brief Write device info to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s             Pointer to the output buffer.
 * @param[in]   max_len       Size of the output buffer.
 * @param[in]   short_name    Short device name, e.g. in ::MBG_DEV_NAME format.
 * @param[in]   fw_rev_num    The firmware revision number.
 * @param[in]   asic_ver_num  The ASIC version number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
_MBG_API_ATTR int _MBG_API mbg_str_dev_name( char *s, int max_len, const char *short_name,
                                             uint16_t fw_rev_num, PCI_ASIC_VERSION asic_ver_num )
{
  MBG_DEV_NAME dev_name = { 0 };
  PCPS_SN_STR sernum = { 0 };
  bool is_syn1588 = false;
  unsigned int i = 0;
  int n = 0;
  size_t l = strlen( short_name );

  if ( l > 0 )
  {
    // In case of a serial port string
    // we just copy the short_name.
    if ( device_id_is_serial( short_name ) )
    {
      n = sn_cpy_str_safe( s, max_len, short_name );
      goto out;
    }

    // If the short_name has more than sizeof( dev_name ) characters,
    // we have to reduce the maximum length.
    // For a bus level device the short_name usually consists of
    // the model name, followed by an underscore '_', followed
    // by the serial number, e.g. "PZF180PEX_046411000030".
    // See ::MBG_DEV_NAME.
    if ( l > sizeof( dev_name ) )
      l = sizeof( dev_name );

    for ( i = 0; i < l; i++ )
    {
      if ( short_name[i] == '_' )
      {
        i++;
        break;
      }

      dev_name[i] = short_name[i];
    }

    strncpy_safe( sernum, &short_name[i], sizeof( sernum ) );

    // TODO
    // Legal serial numbers have MBG_GRP_SERNUM_LEN characters, which is
    // less than the maximum length of a PCPS_SN_STR type string.
    // Do we really have to check and truncate the PCPS_SN_STR sernum?
    if ( sernum[MBG_GRP_SERNUM_LEN] == ' ' )
      sernum[MBG_GRP_SERNUM_LEN] = '\0';
  }

  is_syn1588 = strncmp( dev_name, MBG_SYN1588_TYPE_NAME, strlen( dev_name ) ) == 0;

  n = snprintf_safe( s, max_len, "%s, S/N %s", dev_name, sernum );

  if ( fw_rev_num || asic_ver_num || is_syn1588 )
    n += sn_cpy_str_safe( &s[n], max_len - n, " (" );

  if ( is_syn1588 )
    n += snprintf_safe( &s[n], max_len - n, "Build %i", fw_rev_num );
  else
  {
    if ( fw_rev_num )
    {
      n += snprintf_safe( &s[n], max_len - n, "FW v" PCPS_FW_STR_FMT,
                          _pcps_fw_rev_num_major( fw_rev_num ),
                          _pcps_fw_rev_num_minor( fw_rev_num ) );
      // Append a separator if ASIC version will follow.
      if ( asic_ver_num )
        n += sn_cpy_str_safe( &s[n], max_len - n, " / " );
    }

    if ( asic_ver_num )
    {
      asic_ver_num = _convert_asic_version_number( asic_ver_num );  // TODO Do we need this?

      n += snprintf_safe( &s[n], max_len - n, "ASIC v" PCPS_ASIC_STR_FMT,
                          _pcps_asic_version_major( asic_ver_num ),
                          _pcps_asic_version_minor( asic_ver_num ) );
    }
  }

  if ( fw_rev_num || asic_ver_num || is_syn1588 )
    n += sn_cpy_str_safe( &s[n], max_len - n, ")" );

out:
  return n;

}  // mbg_str_dev_name




#if defined( MBG_TGT_WIN32 )

BOOL APIENTRY DllMain( HANDLE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved )
{
  return TRUE;
}

#endif  // defined( MBG_TGT_WIN32 )



