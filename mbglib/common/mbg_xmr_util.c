
/**************************************************************************
 *
 *  $Id: mbg_xmr_util.c 1.5 2022/12/21 15:23:45 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Common Meinberg utility functions for XMR/MRS.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_xmr_util.c $
 *  Revision 1.5  2022/12/21 15:23:45  martin.burnicki
 *  Removed an obsolete function parameter.
 *  Revision 1.4  2022/10/07 16:41:55  martin.burnicki
 *  Increased string buffer for XMR status info.
 *  Revision 1.3  2022/07/06 14:56:09  martin.burnicki
 *  Code cleanup.
 *  Revision 1.2  2022/06/30 09:44:23  martin.burnicki
 *  Added some doxygen comments and made a function static.
 *  Revision 1.1  2022/06/24 12:54:33  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#define _MBG_XMR_UTIL
 #include <mbg_xmr_util.h>
#undef _MBG_XMR_UTIL

#include <str_util.h>
#include <nanotime.h>

#include <stdio.h>


#define XMR_STATUS_NAMES                                     \
{                                                            \
  "not supp.",           /* XMRS_BIT_NOT_SUPP,           */  \
  "not conn.",           /* XMRS_BIT_NO_CONN,            */  \
  "no signal",           /* XMRS_BIT_NO_SIGNAL,          */  \
  "is master",           /* XMRS_BIT_IS_MASTER,          */  \
  "is locked",           /* XMRS_BIT_IS_LOCKED,          */  \
  "is accurate",         /* XMRS_BIT_IS_ACCURATE,        */  \
  "not settled",         /* XMRS_BIT_NOT_SETTLED,        */  \
  "not phase-locked",    /* XMRS_BIT_NOT_PHASE_LOCKED,   */  \
  "exceeds supp.",       /* XMRS_BIT_NUM_SRC_EXC,        */  \
  "is external",         /* XMRS_BIT_IS_EXTERNAL,        */  \
  "low jitter",          /* XMRS_BIT_LOW_JITTER,         */  \
  "ITU limit violated",  /* XMRS_BIT_ITU_LIMIT_VIOLATED, */  \
  "TRS limit violated",  /* XMRS_BIT_TRS_LIMIT_VIOLATED, */  \
}

static const char *xmr_status_names[N_XMRS_BITS] = XMR_STATUS_NAMES;


static const char *multi_ref_names[N_MULTI_REF] = DEFAULT_MULTI_REF_NAMES;



static /*HDR*/
/**
 * @brief Print info on ::XMULTI_REF_STATUS::status bits to a string.
 *
 * @param[out] s           The string buffer to be filled.
 * @param[in]  max_len     Size of the output buffer for 0-terminated string.
 * @param[in]  xmr_status  The XMR status from ::XMULTI_REF_STATUS::status to decode.
 *
 * @return Length of the string in the buffer.
 */
int snprint_xmr_src_status( char *s, size_t max_len, unsigned xmr_status )
{
  int n = 0;
  bool seen = false;
  int i;

  s[0] = 0;  // Make string empty.

  for ( i = 0; i < N_XMRS_BITS; i++ )
  {
    if ( xmr_status & 0x01 )
    {
      n += snprintf_safe( &s[n], max_len - n, "%s%s",
                          seen ? ", " : "", xmr_status_names[i] );
      seen = true;
    }

    xmr_status >>= 1;
  }

  return n;

}  // snprint_xmr_src_status



/*HDR*/
/**
 * @brief Print all XMR status info.
 *
 * @param[in]  p_axrs           Pointer to an XMR status info structure read from a device.
 * @param[in]  n_src            Number of XMR ref. sources, from ::ALL_XMULTI_REF_INFO::instances.
 * @param[in]  info             An optional header string to print, may be @a NULL.
 * @param[in]  indent_str       An optional indentation string, may be @a NULL.
 */
void print_all_xmr_status( ALL_XMULTI_REF_STATUS *p_axrs, int n_src,
                           const char *info, const char *indent_str )
{
  size_t max_ref_name_len = 0;
  int i;

  // Determine the maximum length of the names of the
  // ref types in use.

  for ( i = 0; i < n_src; i++ )
  {
    int ref_type = p_axrs->status[i].status.id.type;

    if ( ref_type < N_MULTI_REF )
    {
      size_t l = strlen( multi_ref_names[ref_type] );

      if ( l > max_ref_name_len )
        max_ref_name_len = l;
    }
  }

  if ( info )
    printf( "%s\n", info );

  for ( i = 0; i < n_src; i++ )
  {
    char s[256];
    size_t sz = sizeof( s );
    int n;
    const XMULTI_REF_STATUS *p_xrs = &p_axrs->status[i].status;
    int ref_type = p_xrs->id.type;

    if ( ref_type == (uint8_t) -1 )  // No source configured.
      continue;

    n = snprintf_safe( s, sz, "Src %i:", i );

    n += snprintf_safe( &s[n], sz - n, " type %2i", ref_type );

    n += snprintf_safe( &s[n], sz - n, " (%s):", ( ref_type < N_MULTI_REF ) ?
                        multi_ref_names[ref_type] : "unknown" );

    n += snprintf_safe( &s[n], sz - n, "%*s", (int) ( 19 + max_ref_name_len - n ), "" );

    n += snprintf_safe( &s[n], sz - n, "offs: " );
    n += snprint_nano_time( &s[n], sz - n, &p_xrs->offset );

    n += snprintf_safe( &s[n], sz - n, " s, status 0x%04X: ", p_xrs->status );

    n += snprint_xmr_src_status( &s[n], sz - n, p_xrs->status );

    printf( "%s%s\n", indent_str ? indent_str : "", s );
  }

}  // print_all_xmr_status


