
/**************************************************************************
 *
 *  $Id: timeutil.h 1.17 2023/02/21 12:26:36 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for timeutil.c
 *
 * -----------------------------------------------------------------------
 *  $Log: timeutil.h $
 *  Revision 1.17  2023/02/21 12:26:36  martin.burnicki
 *  Removed trailing spaces.
 *  Revision 1.16  2021/04/22 08:37:50Z  martin
 *  Avoid some compiler warnings.
 *  Revision 1.15  2021/03/22 11:52:33  martin
 *  Updated some comments.
 *  Revision 1.14  2019/09/27 15:26:27  martin
 *  New inline functions mbg_exp_time_t_to_time64_t() and
 *  mbg_trnc_time64_t_to_time_t().
 *  Updated function prototypes and doxygen comments.
 *  Revision 1.13  2019/09/10 13:10:25  martin
 *  Fixed doxygen comments.
 *  Revision 1.12  2019/08/07 09:05:44  martin
 *  New inline function mbg_gmtime64().
 *  Updated doxygen comments.
 *  Revision 1.11  2019/07/19 14:29:07  martin
 *  Define CLOCK_MONOTONIC, clock_gettime() and
 *  clock_settime() on Windows, if appropriate.
 *  Updated function prototypes.
 *  Revision 1.10  2019/02/11 09:49:46  martin
 *  Support the mingw build environment.
 *  Fixed build for targets that don't support 64 bit types.
 *  Revision 1.9  2018/12/11 15:36:40  martin
 *  cvt_to_time_t() now expects an int64_t type parameter.
 *  Revision 1.8  2018/11/29 15:32:04Z  martin
 *  Moved some inline functions here.
 *  Include mbgtime.h.
 *  Revision 1.7  2018/01/30 09:54:12Z  martin
 *  Updated function prototypes.
 *  Revision 1.6  2018/01/15 18:31:21Z  martin
 *  Updated function prototypes.
 *  Revision 1.5  2017/11/16 13:34:09  philipp
 *  Extended ISO printing helper functions
 *  Revision 1.4  2017/11/16 11:33:28  philipp
 *  Added functions to print a time_t to ISO format
 *  Revision 1.3  2017/07/05 07:15:00  martin
 *  Provide basic support for clock_gettime()/clock_settime()
 *  compatible functions for Windows.
 *  Updated function prototypes.
 *  Revision 1.1  2016/07/15 14:14:20Z  martin
 *  Initial revision
 *
 **************************************************************************/

#ifndef _TIMEUTIL_H
#define _TIMEUTIL_H

/* Other headers to be included */

#include <gpsdefs.h>
#include <mbgerror.h>
#include <mbgtime.h>

#include <time.h>
#include <stddef.h>
#include <assert.h>


#ifdef _TIMEUTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


#if defined( MBG_TGT_WIN32 ) || defined( MBG_TGT_DOS )
  // We don't need this with MinGW.
  #if !defined( MBG_TGT_MINGW )
    typedef int clockid_t;
    #define clockid_t clockid_t

    #define CLOCK_REALTIME  ( (clockid_t) 0 )
    #define CLOCK_MONOTONIC ( (clockid_t) 1 )

    #define clock_gettime  mbg_clock_gettime
    #define clock_settime  mbg_clock_settime
  #endif
#endif



#if defined( MBG_TGT_WIN32 )

#define __const__ const

/**
 * @brief A pointer to a function returning the system time as @a FILETIME.
 *
 * This can be e.g. the standard Windows API call @a GetSystemTimeAsFileTime
 * or the @a GetSystemTimeAsPreciseFileTime API call available on Windows 8
 * and later Windows versions.
 */
typedef VOID (WINAPI *GSTAFT_FNC)(LPFILETIME lpSystemTimeAsFileTime);

_ext GSTAFT_FNC gstaft_fnc
#ifdef _DO_INIT
 = GetSystemTimeAsFileTime
#endif
;

#endif



#if !defined( MBG_TGT_MISSING_64_BIT_TYPES )
  typedef int64_t mbg_time_t;   // We try to always use 64 bit types.
#else
  typedef time_t mbg_time_t;    // Fall back to the default.
#endif



static __mbg_inline /*HDR*/
time_t cvt_to_time_t( mbg_time_t t )
{
  // Maybe we can do some epoch check / conversion here.
  return (time_t) t;

}  // cvt_to_time_t



static __mbg_inline /*HDR*/
int mbg_exp_time_t_to_time64_t( MBG_TIME64_T *p_t64, const time_t *p_t )
{
  if ( sizeof( time_t ) == sizeof( MBG_TIME64_T ) )
  {
    // No conversion required.
    *p_t64 = *p_t;
    return MBG_SUCCESS;
  }

  // 'time_t' has 32 bits only.

  *p_t64 = (MBG_TIME64_T) (*p_t);
  // FIXME TODO Maybe we should do some
  // epoch conversion / range mapping here.

  return MBG_SUCCESS;

}  // mbg_exp_time_t_to_time64_t



static __mbg_inline /*HDR*/
int mbg_trnc_time64_t_to_time_t( time_t *p_t, const MBG_TIME64_T *p_t64 )
{
  int rc = MBG_SUCCESS;

  if ( sizeof( time_t ) == sizeof( MBG_TIME64_T ) )
    goto done;  // No range check required.


  if ( sizeof( time_t ) == sizeof( MBG_TIME32_T ) )
  {
    // Must check if result in in range.
    MBG_TIME64_T t64 = *p_t64;
    int64_t upper_limit;
    int64_t lower_limit;

    if ( ( (time_t) -1 ) > 0 )  // time_t is unsigned.
    {
      upper_limit = 0xFFFFFFFFUL;
      lower_limit = 0;
    }
    else  // time_t is signed.
    {
      upper_limit = 0x7FFFFFFFL;
      lower_limit = 0x8000000L;
    }

    if ( ( t64 > upper_limit ) || ( t64 < lower_limit ) )
      rc = MBG_ERR_OVERFLOW;

    goto done;
  }

  rc = MBG_ERR_NOT_SUPP_ON_OS;
  goto out;


done:
  *p_t = (time_t) (*p_t64);

out:
  return rc;

}  // mbg_trnc_time64_t_to_time_t



static __mbg_inline /*HDR*/
int mbg_cvt_time32_t_to_time64_t( MBG_TIME64_T *p_t64, const MBG_TIME32_T *p_t32 )
{
  // FIXME TODO Move the checks below to an extra inline function
  // which does an epoch conversion, if required.

  // Avoid warnings "never used".
  (void) p_t64;
  (void) p_t32;

#if 0
  // time_t should be at least 4 bytes
  assert( sizeof( time_t ) >= 4 );

  if ( sizeof( time_t ) == 4 )
  {
    // 32 bit *signed* time_t will roll over after 2038-01-19 03:14:07
    // when the number of seconds reaches 0x7FFFFFFF, so we don't try
    // conversions for 2038 or later, though 32 bit *unsigned* time_t
    // may still work after year 2100.
    if ( year < 70 || year > 137 )
    {
      rc = MBG_ERR_RANGE;
      goto out;
    }
  }
  else
    if ( sizeof( time_t ) == 8 )
    {
      // 64 bit time_t will work for million years. However, it's not
      // clear what happens for dates before 1970-01-01T00:00:00 if time_t
      // is *unsigned*.
      if ( year < 70 )
      {
        rc = MBG_ERR_RANGE;
        goto out;  // TODO Maybe we can handle this ...
      }
    }
    else
    {
      rc = MBG_ERR_NOT_SUPP_ON_OS;
      goto out;
    }
#endif

  return MBG_SUCCESS;

}  // mbg_cvt_time32_t_to_time64_t




static __mbg_inline /*HDR*/
/**
 * @brief A replacement function for POSIX @a gmtime.
 *
 * This function calls the original @a gmtime function, but unlike @a gmtime,
 * it expects the address of a <em>struct tm</em> variable which is only filled
 * if @a gmtime completed successfully. An appropriate return code
 * is provided, indicating whether the conversion succeeded, or not.
 *
 * The original @a gmtime function returns a @a NULL pointer if the conversion
 * fails, so a programs which just uses the pointer without checking it first
 * may trap if the conversion fails. This can't happen with this function.
 *
 * This variant expects the address of a generic @a time_t variable. A @a time_t can
 * be 32 bit or 64 bit wide, depending on the build environment and target system.
 * Systems with 32 bit @a time_t will <b>suffer from the Y2038 problem</b>.
 *
 * There is also the ::mbg_gmtime64 variant, which expects a pointer to an
 * ::MBG_TIME64_T variable to be converted, which is always 64 bits, even
 * on 32 bit systems.
 *
 * @param[out]  p_tm  Address of a <em>struct tm</em> variable to take the conversion result.
 * @param[in]   p_t   Address of a @a time_t variable to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @see ::mbg_gmtime64
 */
int mbg_gmtime( struct tm *p_tm, const time_t *p_t )
{
  struct tm *p_tm_tmp = gmtime( p_t );

  if ( p_tm_tmp == NULL )  // Conversion failed.
    return mbg_get_last_error( NULL );

  *p_tm = *p_tm_tmp;

  return MBG_SUCCESS;

}  // mbg_gmtime



static __mbg_inline /*HDR*/
/**
 * @brief A replacement function for POSIX @a gmtime.
 *
 * This variant of ::mbg_gmtime expects a pointer to an ::MBG_TIME64_T variable
 * to be converted, which is always 64 bits, even on 32 bit systems.
 *
 * Actually, the ::MBG_TIME64_T value is truncated to a native @a time_t, which can
 * be 32 bit or 64 bit wide, depending on the build environment and target system.
 * So this "just works" on systems with 64 bit @a time_t, but systems with 32 bit @a time_t
 * will anyway suffer from the Y2038 problem, unless the @a gmtime call is replaced
 * by a function that does a 64 bit conversion even on systems with 32 bit @a time_t.
 *
 * @param[out]  p_tm   Address of a <em>struct tm</em> variable to take the conversion result.
 * @param[in]   p_t64  Pointer to an ::MBG_TIME64_T providing the timestamp to be converted.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @see ::mbg_gmtime
 */
int mbg_gmtime64( struct tm *p_tm, const MBG_TIME64_T *p_t64 )
{
  struct tm *p_tm_tmp;
  time_t t = cvt_to_time_t( *p_t64 );

  // FIXME TODO Need to implement gmtime64()
  // which always works with 64 bit timestamps.
  p_tm_tmp = gmtime( &t );

  if ( p_tm_tmp == NULL )  // Conversion failed.
    return mbg_get_last_error( NULL );

  *p_tm = *p_tm_tmp;

  return MBG_SUCCESS;

}  // mbg_gmtime64



#if !defined( MBG_TGT_MISSING_64_BIT_TYPES )

static __mbg_inline /*HDR*/
double ntp_tstamp_to_double( const NTP_TSTAMP *t )
{
  return (double) t->seconds + ( ( (double) t->fractions ) / NTP_FRAC_PER_SEC );

}  // ntp_tstamp_to_double



static __mbg_inline /*HDR*/
ulong ntp_frac_to_nsec( uint32_t frac )
{
  uint64_t tmp = ( (uint64_t) frac * NSEC_PER_SEC ) / NTP_FRAC_PER_SEC;

#if 0  // TODO Currently not supported
  if ( tmp >= NSEC_PER_SEC )
    mbglog( LOG_WARNING, "Range overflow in ntp_frac_to_nsec: 0x%X -> %Lu",
            frac, tmp );
#endif

  return (ulong) tmp;

}  // ntp_frac_to_nsec



static __mbg_inline /*HDR*/
uint32_t nsec_to_ntp_frac( ulong nsec )
{
  uint64_t tmp;

  tmp = ( (uint64_t) nsec * NTP_FRAC_PER_SEC ) / NSEC_PER_SEC;

#if 0  // TODO Currently not supported
  if ( tmp >= NTP_FRAC_PER_SEC )
    mbglog( LOG_WARNING, "Range overflow in nsec_to_ntp_frac: %lu -> 0x%LX",
            (ulong) nsec, tmp );
#endif

  return (uint32_t) tmp;

}  // nsec_to_ntp_frac



static __mbg_inline /*HDR*/
void timespec_to_ntp_tstamp( NTP_TSTAMP *t_ntp, const struct timespec *t_ts )
{
  t_ntp->seconds = (uint32_t) t_ts->tv_sec + NTP_SEC_BIAS;
  t_ntp->fractions = nsec_to_ntp_frac( t_ts->tv_nsec );

}  // timespec_to_ntp_tstamp



static __mbg_inline /*HDR*/
void ntp_tstamp_to_timespec( struct timespec *t_ts, const NTP_TSTAMP *t_ntp )
{
  t_ts->tv_sec = t_ntp->seconds - NTP_SEC_BIAS;
  t_ts->tv_nsec = ntp_frac_to_nsec( t_ntp->fractions );

}  // ntp_tstamp_to_timespec

#endif  // !defined( MBG_TGT_MISSING_64_BIT_TYPES )



static __mbg_inline /*HDR*/
int timespec_is_set( const struct timespec *p )
{
  return p->tv_sec != 0 || p->tv_nsec != 0;

}  // timespec_is_set



//
// compute the difference of two timespec variables
//
static __mbg_inline /*HDR*/
double delta_timespec_d_s( const struct timespec *ts,
                           const struct timespec *ts_ref )
{
  return ( (double) ts->tv_sec - (double) ts_ref->tv_sec )
       + ( (double) ts->tv_nsec - (double) ts_ref->tv_nsec ) / NSEC_PER_SEC;

}  // delta_timespec_d_s



#if !defined( MBG_TGT_MISSING_64_BIT_TYPES )

static __mbg_inline /*HDR*/
int64_t delta_timespec_ll_ns( const struct timespec *ts,
                              const struct timespec *ts_ref )
{
  int64_t tmp = ts->tv_sec - ts_ref->tv_sec;
  tmp = ( tmp * NSEC_PER_SEC ) + ( ts->tv_nsec - ts_ref->tv_nsec );

  return tmp;

}  // delta_timespec_ll_ns

#endif  // !defined( MBG_TGT_MISSING_64_BIT_TYPES )



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 int snprint_gmtime_error( char *s, size_t max_len, int mbg_errno, time_t t, const char *calling_fnc ) ;
 /**
 * @brief A Windows implementation for POSIX @a clock_gettime.
 *
 * @param[in]   clock_id  Identifier of a specific clock, e.g.
 *                        @a CLOCK_REALTIME, @a CLOCK_MONOTONIC, etc.
 * @param[out]  tp        Address of a <em>struct timespec</em>
 *                        to take up the current time.
 *
 * @return  0 on success, -1 on error, just like the POSIX function.
 *          In case of an error the POSIX errno variable is set
 *          appropriately.
 */
 int mbg_clock_gettime( clockid_t clock_id, struct timespec *tp ) ;

 /**
 * @brief A Windows implementation for POSIX @a clock_settime.
 *
 * @param[in]  clock_id  Identifier of a specific clock, i.e.
 *                       @a CLOCK_REALTIME which is the only clock
 *                       supported by this call.
 * @param[in]  tp        Pointer to a struct timespec providing
 *                       the time to be set.
 *
 * @return  0 on success, -1 on error, just like the POSIX function.
 *          In case of an error the POSIX errno variable is set
 *          appropriately.
 */
 int mbg_clock_settime( clockid_t clock_id, const struct timespec *tp ) ;

 void check_precise_time_api( void ) ;
 /**
 * @brief Print a %UTC offset into a string.
 *
 * Format of the string is "[info]+hh[:mm[:ss]]h"
 *
 * @param[out]  s         Address of a string buffer to be filled.
 * @param[in]   max_len   Size of the string buffer.
 * @param[in]   info      An optional info string to be prepended, may be @a NULL.
 * @param[in]   utc_offs  The %UTC offset to be printed, in [s].
 *
 * @return The number of characters written to the string buffer,
 *         except the terminating 0.
 */
 int snprint_utc_offs( char *s, size_t max_len, const char *info, long utc_offs ) ;

 int snprint_time_t_to_iso( time_t tstamp, int offs_hours, char *buf, size_t len ) ;
 int snprint_time_t_to_iso_ms( time_t tstamp, int offs_hours, time_t frac, char *buf, size_t len ) ;
 int snprint_time_t_to_iso_us( time_t tstamp, int offs_hours, time_t frac, char *buf, size_t len ) ;
 int snprint_time_t_to_iso_ns( time_t tstamp, int offs_hours, time_t frac, char *buf, size_t len ) ;
 int snprint_ntp_tstamp_to_iso_ms( const NTP_TSTAMP *ts, char *buf, size_t len ) ;
 int snprint_ntp_tstamp_to_iso_us( const NTP_TSTAMP *ts, char *buf, size_t len ) ;
 int snprint_ntp_tstamp_to_iso_ns( const NTP_TSTAMP *ts, char *buf, size_t len ) ;

/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _TIMEUTIL_H */
