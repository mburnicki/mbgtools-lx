
/**************************************************************************
 *
 *  $Id: mbgutil.h 1.31 2022/06/02 08:20:47 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions used with mbgutil.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbgutil.h $
 *  Revision 1.31  2022/06/02 08:20:47  martin.burnicki
 *  Updated version code to 3.14.
 *  Revision 1.30  2021/11/08 19:59:51Z  martin.burnicki
 *  Added definition MBG_SYN1588_TYPE_NAME.
 *  Revision 1.29  2021/10/05 09:11:44  martin
 *  Account for new definitions _MBG_API_ATTR_EXPORT and _MBG_API_ATTR_IMPORT.
 *  Revision 1.28  2021/09/13 09:35:10  martin
 *  Changed default 2-digit year limit from 1980 to 1970.
 *  Revision 1.27  2021/04/30 11:12:02  martin
 *  Re-added macro _mbg_strncpy() with proper doxygen comment.
 *  Revision 1.26  2021/04/29 14:48:07  martin
 *  Moved some inline functions here.
 *  Revision 1.25  2021/04/29 13:04:43  martin
 *  Moved common definitions for year limit here.
 *  Removed obsolete macro _mbg_strncpy().
 *  Updated function prototypes.
 *  Revision 1.24  2021/03/22 10:13:43  martin
 *  Updated a bunch of comments.
 *  Revision 1.23  2020/02/27 13:56:40  martin
 *  Updated function prototypes/doxygen comments.
 *  Revision 1.22  2019/07/31 15:55:50  martin
 *  Updated function prototypes.
 *  Revision 1.21  2018/09/21 08:18:05  martin
 *  New version code 0x0308, compatibility version still 0x0110.
 *  Revision 1.20  2018/08/13 14:54:24Z  martin
 *  Updated function prototypes.
 *  Revision 1.19  2018/08/07 15:09:11Z  martin
 *  New version code 0x0370, compatibility version still 0x0110.
 *  Revision 1.18  2017/07/05 16:44:35  martin
 *  New version code 0x0400, compatibility version still 0x0110.
 *  Include timeutil.h.
 *  windows.h is now included in mbg_tgt.h.
 *  Updated function prototypes.
 *  Revision 1.17  2012/10/15 10:08:32  martin
 *  Include stdlib.h.
 *  Cleaned up handling of pragma pack().
 *  Revision 1.16  2009/08/14 10:11:53Z  daniel
 *  New version code 306, compatibility version still 110.
 *  Revision 1.15  2009/06/09 08:57:47Z  daniel
 *  Rev No. 305
 *  Revision 1.14  2009/03/19 09:06:00Z  daniel
 *  New version code 304, compatibility version still 110.
 *  Revision 1.13  2009/01/12 09:35:41Z  daniel
 *  New version code 303, compatibility version still 110.
 *  Updated function prototypes.
 *  Revision 1.12  2008/01/17 10:15:26Z  daniel
 *  New version code 302, compatibility version still 110.
 *  Revision 1.11  2007/10/16 10:01:17Z  daniel
 *  New version code 301, compatibility version still 110.
 *  Revision 1.9  2007/03/21 16:48:31Z  martin
 *  New version code 219, compatibility version still 110.
 *  Revision 1.8  2006/08/09 13:18:18Z  martin
 *  New version code 218, compatibility version still 110.
 *  Revision 1.7  2006/06/08 10:48:52Z  martin
 *  New version code 217, compatibility version still 110.
 *  Added macro _mbg_strncpy().
 *  Revision 1.6  2006/05/10 10:56:54Z  martin
 *  Updated function prototypes.
 *  Revision 1.5  2006/05/02 13:24:49Z  martin
 *  New version code 216, compatibility version still 110.
 *  Revision 1.4  2006/01/11 12:24:05Z  martin
 *  New version code 215, compatibility version still 110.
 *  Revision 1.3  2005/12/15 10:01:51Z  martin
 *  New version 214, compatibility version still 110.
 *  Revision 1.2  2005/02/18 15:13:42Z  martin
 *  Updated function prototypes.
 *  Revision 1.1  2005/02/18 10:39:49Z  martin
 *  Initial revision
 *
 **************************************************************************/

#ifndef _MBGUTIL_H
#define _MBGUTIL_H


/* Other headers to be included */

#include <mbg_tgt.h>
#include <timeutil.h>
#include <use_pack.h>
#include <pcpsdefs.h>
#include <mbggeo.h>
#include <pci_asic.h>

#include <stdlib.h>


#define MBGUTIL_VERSION         0x0314

#define MBGUTIL_COMPAT_VERSION  0x0110


#if defined( MBG_TGT_WIN32 )


#elif defined( MBG_TGT_LINUX )


#elif defined( MBG_TGT_OS2 )


#elif defined( MBG_TGT_DOS )

  #if !defined( MBG_USE_DOS_TSR )
  #endif

#else

#endif

#ifdef _MBGUTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif

#if defined( MBGUTIL_STATIC_LIB )
  // We don't build/use a DLL.
  #define _MBG_API_ATTR_VAR
#else
  // We do build/use a DLL.
  #ifdef _MBGUTIL
    #define _MBG_API_ATTR_VAR _MBG_API_ATTR_EXPORT
  #else
    #define _MBG_API_ATTR_VAR _MBG_API_ATTR_IMPORT
  #endif
#endif


/* Start of header body */

#if defined( _USE_PACK )
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif


#ifdef __cplusplus
extern "C" {
#endif


#define MBG_SYN1588_TYPE_NAME  "SYN1588"



/**
 * @brief A macro to simplify calls to ::mbg_strncpy.
 *
 * Can be used whenever a string variable is used rather than a char *.
 */
#define _mbg_strncpy( _s, _src ) \
  mbg_strncpy( _s, sizeof( _s ), _src )



#if !defined( MBG_EXP_YEAR_LIMIT_DEFAULT )
  /**
   * @brief The default year limit when converting 2 digit year numbers to 4 digits.
   */
  #define MBG_EXP_YEAR_LIMIT_DEFAULT   1970
#endif


/**
  * @brief A global variable used when converting 2 digit year numbers to 4 digits.
  *
  * Can be passed to all calls to ::mbg_exp_year to get
  * the same conversion results across the whole application.
  *
  * Defaults to ::MBG_EXP_YEAR_LIMIT_DEFAULT, but can be changed at runtime.
  */
_ext _MBG_API_ATTR_VAR int mbg_exp_year_limit
#ifdef _DO_INIT
 = MBG_EXP_YEAR_LIMIT_DEFAULT
#endif
;



#if MBG_TGT_HAS_64BIT_TYPES

static __mbg_inline
/**
 * @brief Convert a @ref PCPS_TIME_STAMP to a 64 bit number.
 *
 * The 64 bit binary timestamp is composed of 32 bit seconds
 * in the high part, and 32 bit binary fractions in the low part,
 * so it can easily be used in computations.
 *
 * @param[in]  p_ts  Pointer to the ::PCPS_TIME_STAMP to be converted.
 *
 * @return  The 64 bit timestamp.
 *
 * @see ::uint64_to_pcps_time_stamp
 */
uint64_t pcps_time_stamp_to_uint64( const PCPS_TIME_STAMP *p_ts )
{
  return ( ( (uint64_t) p_ts->sec ) << 32 ) + p_ts->frac;

}  // pcps_time_stamp_to_uint64



static __mbg_inline
/**
 * @brief Convert a composed 64 bit time stamp to a @ref PCPS_TIME_STAMP.
 *
 * The 64 bit number which can easily be used in computations
 * is split into 32 bit seconds and 32 bit binary fractions.
 *
 * @param[out]  p_ts  Address of the ::PCPS_TIME_STAMP to take the result.
 * @param[in]   n     The composed 64 bit binary timestamp.
 *
 * @see ::pcps_time_stamp_to_uint64
 */
void uint64_to_pcps_time_stamp( PCPS_TIME_STAMP *p_ts, uint64_t n )
{
  p_ts->sec = (uint32_t) ( n >> 32 );
  p_ts->frac = (uint32_t) ( n & 0xFFFFFFFFUL );

}  // uint64_to_pcps_time_stamp



static __mbg_inline
/**
 * @brief Compute the difference between two ::PCPS_TIME_STAMP values.
 *
 * @param[in]  p_ts      Pointer to the current time stamp.
 * @param[in]  p_prv_ts  Pointer to a previous time stamp.
 *
 * @return The time difference as double, in microseconds.
 */
double delta_timestamp_us( const PCPS_TIME_STAMP *p_ts, const PCPS_TIME_STAMP *p_prv_ts )
{
  uint64_t ts = pcps_time_stamp_to_uint64( p_ts );
  uint64_t prv_ts = pcps_time_stamp_to_uint64( p_prv_ts );

  // We divide by MBG_FRAC32_UNITS_PER_SEC to get the correct fractions
  // and we multiply by 1E6 to get the result in microseconds.
  return (double) ( (int64_t) ( ts - prv_ts ) ) * 1E6 / MBG_FRAC32_UNITS_PER_SEC;

}  // delta_timestamp_us

#endif



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Get the version number of the precompiled DLL/shared object library.
 *
 * If this library is used as a DLL/shared object library, the version.
 * number can be checked to see if the header files which are actually used.
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API function are called
 * in the correct way.
 *
 * @return The version number.
 *
 * @see ::mbgutil_check_version
 * @see ::MBGUTIL_VERSION defined in mbgutil.h
 */
 _MBG_API_ATTR int _MBG_API mbgutil_get_version( void ) ;

 /**
 * @brief Check if the DLL/shared library is compatible with a given version.
 *
 * If this library is used as a DLL/shared object library, the version
 * number can be checked to see if the header files which are actually used
 * to build an application are compatible with the header files which have
 * been used to build the library, and thus the API functions are called
 * in the correct way.
 *
 * @param[in] header_version  Version number to be checked, should be ::MBGUTIL_VERSION
 *                            from the mbgutil.h file version used to build the application.
 *
 * @return ::MBG_SUCCESS if compatible, else ::MBG_ERR_LIB_NOT_COMPATIBLE.
 *
 * @see ::mbgutil_get_version
 * @see ::MBGUTIL_VERSION defined in mbgutil.h
 */
 _MBG_API_ATTR int _MBG_API mbgutil_check_version( int header_version ) ;

 /**
 * @brief A portable, safer implementation of snprintf().
 *
 * The output string buffer is in any case properly terminated by 0.
 * For a detailed description see ::vsnprintf_safe.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   fmt      Format string according to subsequent parameters.
 * @param[in]   ...      Variable argument list according to the @p fmt format string.
 *
 * @return  The number of characters written to the output buffer,
 *          except the terminating 0.
 *
 * @see ::vsnprintf_safe
 * @see ::snprintf_safe
 */
 __attribute__( ( format( printf, 3, 4 ) ) ) _MBG_API_ATTR int _MBG_API mbg_snprintf( char *s, size_t max_len, const char * fmt, ... ) ;

 /**
 * @brief A portable, safe implementation of strncpy().
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the destination string buffer.
 * @param[in]   max_len  Size of the destination string buffer.
 * @param[in]   src      Pointer to the source string buffer.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_strncpy( char *s, size_t max_len, const char *src ) ;

 /**
 * @brief Write a character multiple times to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   c        The character to write to the output buffer.
 * @param[in]   n        The number of characters to write to the output buffer.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_strchar( char *s, size_t max_len, char c, size_t n ) ;

 /**
 * @brief Write a short date string "dd.mm." to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   mday     Day-of-month number, 1..31.
 * @param[in]   month    Month number, 1..12.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_date_short( char *s, int max_len, int mday, int month ) ;

 /**
 * @brief Write a date string "dd.mm.yyyy" to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   mday     Day-of-month number, 1..31.
 * @param[in]   month    Month number, 1..12.
 * @param[in]   year     Year number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_date( char *s, int max_len, int mday, int month, int year ) ;

 /**
 * @brief Write a short time string "hh:mm" to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   hour     Hours number, 0..23.
 * @param[in]   min      Minutes number, 0..59.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_time_short( char *s, int max_len, int hour, int min ) ;

 /**
 * @brief Write a time string "hh:mm:ss" to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   hour     Hours number, 0..23.
 * @param[in]   min      Minutes number, 0..59.
 * @param[in]   sec      Seconds number, 0..59, or 60 in case of leap second.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_time( char *s, int max_len, int hour, int min, int sec ) ;

 /**
 * @brief Write a long time string "hh:mm:ss.cc" to a string buffer.
 *
 * Include 100ths of seconds.
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   hour     Hours number, 0..23.
 * @param[in]   min      Minutes number, 0..59.
 * @param[in]   sec      Seconds number, 0..59, or 60 in case of leap second.
 * @param[in]   sec100   Hundreths of seconds, 0..99.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_time_long( char *s, int max_len, int hour, int min, int sec, int sec100 ) ;

 /**
 * @brief Write a full date and time string to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::TM_GPS structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_tm_gps_date_time( char *s, int max_len, const TM_GPS *pt ) ;

 /**
 * @brief Write the short date given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_date_short( char *s, int max_len, const PCPS_TIME *pt ) ;

 /**
 * @brief Write the date given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_date( char *s, int max_len, const PCPS_TIME *pt ) ;

 /**
 * @brief Write the short time given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_time_short( char *s, int max_len, const PCPS_TIME *pt ) ;

 /**
 * @brief Write the time given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_time( char *s, int max_len, const PCPS_TIME *pt ) ;

 /**
 * @brief Write the time including sec100ths given as ::PCPS_TIME structure to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_time_long( char *s, int max_len, const PCPS_TIME *pt ) ;

 /**
 * @brief Write date and time given as ::PCPS_TIME structure to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_TIME structure providing date and time.
 * @param[in]   tz_str   Optional time zone string to be appended, currently not used, may be @a NULL.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_date_time( char *s, int max_len, const PCPS_TIME *pt, const char *tz_str ) ;

 /**
 * @brief Write date derived from seconds-since-epoch to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   sec      Number of seconds since the epoch to be converted to date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_date( char *s, int max_len, PCPS_SECONDS sec ) ;

 /**
 * @brief Write time derived from seconds-since-epoch to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   sec      Number of seconds since the epoch to be converted to date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time( char *s, int max_len, PCPS_SECONDS sec ) ;

 /**
 * @brief Write %UTC date and time given as ::PCPS_HR_TIME structure to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_date_time_utc( char *s, int max_len, const PCPS_HR_TIME *pt ) ;

 /**
 * @brief Write local date and time given as ::PCPS_HR_TIME structure to a string buffer.
 *
 * The number of space characters between date and time
 * is determined by the global variable ::mbg_date_time_dist.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_date_time_loc( char *s, int max_len, const PCPS_HR_TIME *pt ) ;

 /**
 * @brief Print binary ::PCPS_FRAC_32 fractions in decimal to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   frac     Binary fractions of a second in ::PCPS_FRAC_32 format.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time_frac( char *s, int max_len, PCPS_FRAC_32 frac ) ;

 /**
 * @brief Print the %UTC offset from a ::PCPS_HR_TIME structure to a string buffer.
 *
 * The output format is sign - hours - minutes, e.g. "+01:45h".
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date, and %UTC offset.
 * @param[in]   info     An informational text to be prepended.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time_offs( char *s, int max_len, const PCPS_HR_TIME *pt, const char *info ) ;

 /**
 * @brief Write a high resolution %UTC time stamp including fractions to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_tstamp_utc( char *s, int max_len, const PCPS_HR_TIME *pt ) ;

 /**
 * @brief Write a high resolution local time stamp including fractions to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_tstamp_loc( char *s, int max_len, const PCPS_HR_TIME *pt ) ;

 /**
 * @brief Write a raw high resolution time stamp to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_tstamp_raw( char *s, int max_len, const PCPS_TIME_STAMP *pt ) ;

 /**
 * @brief Write a raw high resolution time stamp plus converted local time to a string buffer.
 *
 * The output string also has the time status code appended as hex number.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure providing date and time.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pcps_hr_time_raw( char *s, int max_len, const PCPS_HR_TIME *pt ) ;

 /**
 * @brief Write time capture / user capture time stamp to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pt       Pointer to a ::PCPS_HR_TIME structure containing a user capture event.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_ucap( char *s, int max_len, const PCPS_HR_TIME *pt ) ;

 /**
 * @brief Write a geographic coordinate in degrees - minutes - seconds to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   pdms     Pointer to a ::DMS structure containing the coordinate.
 * @param[in]   prec     Number of digits of the geographic seconds after the decimal separator.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pos_dms( char *s, int max_len, const DMS *pdms, int prec ) ;

 /**
 * @brief Write a position's altitude parameter to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   alt      The altitude parameter, in meters.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pos_alt( char *s, int max_len, double alt ) ;

 /**
 * @brief Write geographic coordinates to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s        Pointer to the output buffer.
 * @param[in]   max_len  Size of the output buffer.
 * @param[in]   ppos     Pointer to a ::POS structure containing the coordinates.
 * @param[in]   prec     Number of digits of the geographic seconds after the decimal separator.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_pos( char *s, int max_len, const POS *ppos, int prec ) ;

 /**
 * @brief Write device info to a string buffer.
 *
 * The output string buffer is in any case properly terminated by 0.
 *
 * @param[out]  s             Pointer to the output buffer.
 * @param[in]   max_len       Size of the output buffer.
 * @param[in]   short_name    Short device name, e.g. in ::MBG_DEV_NAME format.
 * @param[in]   fw_rev_num    The firmware revision number.
 * @param[in]   asic_ver_num  The ASIC version number.
 *
 * @return The number of characters written to the output buffer,
 *         except the terminating 0.
 */
 _MBG_API_ATTR int _MBG_API mbg_str_dev_name( char *s, int max_len, const char *short_name, uint16_t fw_rev_num, PCI_ASIC_VERSION asic_ver_num ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT
#undef _MBG_API_ATTR_VAR

#endif  /* _MBGUTIL_H */

