
/**************************************************************************
 *
 *  $Id: ctry.h 1.17 2021/04/21 10:59:09 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for ctry.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: ctry.h $
 *  Revision 1.17  2021/04/21 10:59:09  martin
 *  Doxygen fixes.
 *  Revision 1.16  2021/04/13 21:31:52  martin
 *  Changed CTRY_CODES from enum back to defines because the enum type
 *  caused a build error on some system when being used as initializer.
 *  Updated some doxygen comments.
 *  Revision 1.15  2021/03/22 17:37:26  martin
 *  Updated some comments.
 *  Revision 1.14  2017/06/23 09:53:27  martin
 *  Fixed spelling in an earlier log message.
 *  Revision 1.13  2012/11/29 12:01:09  martin
 *  Don't include mbg_tgt.h explicitly since this will anyway be
 *  included by words.h for non-firmware targets. So mbg_tgt.h
 *  must not necessarily be added to firmware projects.
 *  Revision 1.12  2011/06/22 07:37:57Z  martin
 *  Cleaned up handling of pragma pack().
 *  Revision 1.11  2010/07/15 08:33:41  martin
 *  Added some macros implemented by Stefan.
 *  Updated function prototypes.
 *  Revision 1.10  2007/03/29 12:21:10  martin
 *  Updated function prototypes.
 *  Revision 1.9  2004/10/26 07:38:50Z  martin
 *  Redefined interface data types using C99 fixed-size definitions.
 *  Updated function prototypes.
 *  Revision 1.8  2004/04/14 08:47:28  martin
 *  Pack structures 1 byte aligned.
 *  Revision 1.7  2002/02/19 09:28:00Z  MARTIN
 *  Use new header mbg_tgt.h to check the target environment.
 *  Revision 1.6  2001/09/14 12:04:40  MARTIN
 *  Modified definition for CLSTR.
 *  Updated function prototypes.
 *  Revision 1.5  2001/02/28 15:07:06  MARTIN
 *  Modified preprocessor syntax.
 *  Revision 1.4  2000/11/27 14:13:27  MARTIN
 *  New types CLSTR, PLSTR, and PCLSTR.
 *  New macro _lstr() calls lstr_lng() for the current language.
 *  Definitions associated  with ctry_fmt_dt() and ctry_fmt_times()
 *  have been moved to a new file ctry_fmt.h.
 *  Updated function prototypes.
 *  Revision 1.3  2000/08/17 15:35:02  MARTIN
 *  No init function by default (previously DOS),
 *  Revision 1.2  2000/07/21 09:48:34  MARTIN
 *  Initial revision
 *
 **************************************************************************/

#ifndef _CTRY_H
#define _CTRY_H


/* Other headers to be included */

#include <words.h>

#if defined( MBG_TGT_NETWARE )
  #include <ctry_nw.h>
#elif defined( MBG_TGT_OS2 )
  #include <ctry_os2.h>
#elif defined( MBG_TGT_WIN32 )
  // #include <ctry_w32.h>
#elif defined( MBG_TGT_LINUX )
  // #include <ctry_lx.h>
#elif defined( MBG_TGT_DOS )
  #include <ctry_dos.h>
#else
  // nothing to include for C166 etc.
#endif

#include <use_pack.h>


#ifdef _CTRY
 #define _ext
#else
 #define _ext extern
#endif


/* Start of header body */

#if defined( _USE_PACK )
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif


#ifdef __cplusplus
extern "C" {
#endif


typedef uint8_t LANGUAGE;     ///< A language code, see ::LNG_CODES.

typedef uint16_t CTRY_CODE;


#if !defined LNG_DEFINED
  /**
   * @brief Codes used with ::LANGUAGE.
   */
  enum LNG_CODES
  {
    LNG_ENGLISH,
    LNG_GERMAN,
    N_LNG
  };

  #define LNG_DEFINED
#endif

typedef char *LSTR[N_LNG];      ///< An array of strings for several languages.
typedef char **PLSTR;           ///< A pointer to an array of strings for several languages.

typedef const char * const CLSTR[N_LNG];  ///< An array of strings like ::LSTR, but const.
typedef const char * const *PCLSTR;       ///< A pointer to an array like ::PLSTR, but with const strings.


/**
 * @brief A structure for easy localization.
 *
 * Specifies date and time formats for selected countries.
 */
typedef struct
{
  CTRY_CODE code;  ///< See @ref CTRY_CODES.

  uint8_t dt_fmt;  ///< See ::DT_FMT_CODES.
  uint8_t tm_fmt;  ///< See ::TM_FMT_CODES.
  char dt_sep;     ///< See @ref DT_SEP_CHARS.
  char tm_sep;     ///< See @ref TM_SEP_CHARS.
} CTRY;


/**
 * @defgroup CTRY_CODES Country codes
 *
 * Used with ::CTRY::code.
 *
 * @{ */

#define CTRY_US           1
#define CTRY_UK          44
#define CTRY_GERMANY     49

/** @} defgroup CTRY_CODES */


#ifndef CTRY_DEFINED

  #define CTRY_DEFINED

  /**
   * @brief Codes used with ::CTRY::dt_fmt.
   */
  enum DT_FMT_CODES
  {
    DT_FMT_DDMMYYYY,
    DT_FMT_MMDDYYYY,
    DT_FMT_YYYYMMDD,
    N_DT_FMT
  };


  /**
   * @brief Codes used with ::CTRY::tm_fmt.
   */
  enum TM_FMT_CODES
  {
    TM_FMT_24H,
    // TM_FMT_12H,  // Not yet supported.
    N_TM_FMT
  };


  /**
   * @defgroup DT_SEP_CHARS Date string separator codes
   *
   * Used with ::CTRY::dt_sep.
   *
   * @{ */

  #define DT_SEP_DOT    '.'
  #define DT_SEP_MINUS  '-'
  #define DT_SEP_SLASH  '/'

  /** @} defgroup DT_SEP_CHARS */

  /**
   * @brief A zero-terminated list of valid @ref DT_SEP_CHARS characters.
   */
  #define DT_SEP_LIST   { DT_SEP_DOT, DT_SEP_MINUS, DT_SEP_SLASH, 0 }


  /**
   * @defgroup TM_SEP_CHARS Time string separator codes
   *
   * Used with ::CTRY::tm_sep.
   *
   * @{ */

  #define TM_SEP_COLON  ':'
  #define TM_SEP_DOT    '.'

  /** @} defgroup TM_SEP_CHARS */

  /**
   * @brief A zero-terminated list of valid @ref TM_SEP_CHARS characters.
   */
  #define TM_SEP_LIST   { TM_SEP_COLON, TM_SEP_DOT, 0 }

#endif // CTRY_DEFINED


extern LANGUAGE language;
extern CTRY ctry;


#define _ctry_init() \
  ctry_setup( ctry_get_code() )


#define _next_language()      \
{                             \
  if ( ++language >= N_LNG )  \
    language = 0;             \
                              \
}  // next_language


// macro to call lstr_lng with the current language
#define _lstr( _s )   lstr_lng( (_s), language )

// macro to call clstr_lng with the current language, and a set of strings
#define _clstr( _s )  clstr_lng( language, _s )

// macros used in wxWidgets projects
#if defined( __WXWINDOWS__ )
  #define _wx_lstr( _s )   wxString::From8BitData( lstr_lng( (_s), language ) )
  #define _wx_clstr( _s )  wxString::From8BitData( clstr_lng( language, _s ) )
#endif


/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Return the index of a CLSTR component for a certain language code.
 *
 * If the strings for several languages are the same, only the first
 * array entry may be populated, while the others are all @a NULL,
 * so the returned index differs from the language code @p lng.
 *
 * @param[in]  s    Pointer to a constant array of localized strings.
 * @param[in]  lng  The language code 0..::N_LNG-1.
 *
 * @return  The index to the array @p s, derived from @p lng.
 */
 int lstr_idx( CLSTR s, int lng ) ;

 /**
 * @brief Return the index of a ::CLSTR component for a certain language.
 *
 * If the strings for several languages are the same, only the first
 * array entry may be populated, while the others are all @a NULL,
 * so the returned index differs from the language code @p lng.
 *
 * @param[in]  s      The array of ::CLSTR entries.
 * @param[in]  idx    The index of the array element.
 * @param[in]  n_lng  The number of supported languages.
 * @param[in]  lng    The language for which the index is to be retrieved.
 *
 * @return  The index to the array @p s, derived from @p lng.
 */
 int lstr_array_idx( CLSTR s, int idx, int n_lng, int lng ) ;

 /**
 * @brief Return a localized string according to a language code.
 *
 * @param[in]  s    Pointer to a constant array of localized strings.
 * @param[in]  lng  The language code 0..::N_LNG-1.
 *
 * @return  The string according to the language code.
 */
 const char *lstr_lng( CLSTR s, int lng ) ;

 /**
 * @brief Set up some localization depending on a specific country code.
 *
 * This sets up the global variable ::ctry.
 *
 * @param[in]  code  The ::CTRY_CODE.
 */
 void ctry_setup( CTRY_CODE code ) ;

 /**
 * @brief Set up localization for the next supported country code.
 *
 * Used to easily switch between different country codes.
 */
 void ctry_next( void ) ;

 /**
 * @brief Return a string from an array of strings given as function arguments.
 *
 * @param[in]  index  Index of the string to be returned.
 * @param[in]  ...    Variable number of string arguments.
 *
 * @return The string associated with the index.
 */
 const char *clstr_lng( int index, ... ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext

#endif  /* _CTRY_H */

