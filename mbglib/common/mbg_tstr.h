
/**************************************************************************
 *
 *  $Id: mbg_tstr.h 1.4 2021/12/01 10:34:21 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and prototypes for mbg_tstr.c, which provides
 *  support functions for serial time strings.
 *
 *  See also pcpststr.h for time string support with bus-level devices.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_tstr.h $
 *  Revision 1.4  2021/12/01 10:34:21  martin.burnicki
 *  Added field ts_pps to struct MBG_TSTR_RCV_INFO_s.
 *  Updated function prototypes.
 *  Revision 1.3  2021/04/21 10:59:47  martin
 *  Huge cleanup and refactoring.
 *  Revision 1.2  2011/04/18 10:44:04  martin
 *  Extended the TIMESTR_INFO structure.
 *  Removed obsolete variables.
 *  Updated function prototypes.
 *  Revision 1.1  2007/11/05 10:47:52  martin
 *  Initial revision
 *
 **************************************************************************/

#ifndef _MBG_TSTR_H
#define _MBG_TSTR_H


/* Other headers to be included */

#include <mbgserio.h>
#include <mbgtimex.h>
#include <mbgsystm.h>

#include <stdlib.h>
#include <time.h>

#ifdef _MBG_TSTR
  #define _ext
  #define _DO_INIT
#else
  #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


#ifndef SUPP_PREV_TSTR_DATA
  #define SUPP_PREV_TSTR_DATA     0
#endif



#if !defined( STX_CHAR )
  #define STX_CHAR  '\x2'
#endif

#if !defined( STX_STR )
  #define STX_STR   "\x2"
#endif

#if !defined( ETX_CHAR )
  #define ETX_CHAR  '\x3'
#endif

#if !defined( ETX_STR )
  #define ETX_STR   "\x3"
#endif



/**
 * @brief A collection of relevant data required to receive a time string.
 *
 * This is just a forward declaration.
 *
 * @see ::MBG_TSTR_RCV_INFO
 */
struct MBG_TSTR_RCV_INFO_s;

/**
 * @brief A collection of relevant data required to receive a time string.
 *
 * @see ::MBG_TSTR_RCV_INFO_s
 */
typedef struct MBG_TSTR_RCV_INFO_s MBG_TSTR_RCV_INFO;



/**
 * @brief Characteristics of a time string format.
 *
 * This is just a forward declaration.
 *
 * @see ::MBG_TSTR_SPEC
 */
struct MBG_TSTR_SPEC_s;

/**
 * @brief Characteristics of a time string format.
 *
 * @see ::MBG_TSTR_SPEC_s
 */
typedef struct MBG_TSTR_SPEC_s MBG_TSTR_SPEC;



/**
 * @brief Type of a callback function to notify the application.
 *
 * Called by ::mbg_tstr_receive, when appropriate.
 */
typedef int MBG_TSTR_RCV_CALLBACK_FNC( MBG_TSTR_RCV_INFO *p_tsri, int rc );



/**
 * @brief Type of a function that can be called to check if the basic string format matches.
 *
 * TODO Is this OBSOLETE ?
 */
typedef int MBG_TSTR_CHK_FNC( MBG_TSTR_RCV_INFO *p_tsri );



/**
 * @brief Type of a function that can be called to decode a specific time string format.
 */
typedef int MBG_TSTR_DECODE_FNC( MBG_TSTR_RCV_INFO *p_tsri );



/**
 * @brief A collection of status flags derived from a time string.
 *
 * @see ::TSTR_ST_MASKS
 */
typedef uint32_t TSTR_STATUS;


/**
 * @brief Enumeration of bits to define flag masks for @ref TSTR_STATUS.
 *
 * @see ::TSTR_ST_MASKS
 */
enum TSTR_ST_BITS
{
  TSTR_ST_BIT_UTC_OFFS_VALID,  ///< A valid %UTC offset could be determined, see ::MBG_TSTR_DATA::utc_offs.
  TSTR_ST_BIT_IS_SYNC,         ///< The device claims to be synchronized.
  TSTR_ST_BIT_DL_ENB,          ///< Daylight saving time (DST) is active.
  TSTR_ST_BIT_DL_ANN,          ///< A change in DST is being announced.
  TSTR_ST_BIT_LS_ENB,          ///< The current second is an inserted leap second.
  TSTR_ST_BIT_LS_ANN,          ///< A leap second is being announced.
  TSTR_ST_BIT_INV_TIME,        ///< Date and time sould be considered invalid.
  N_TSTR_ST_BITS               ///< The number of known ::TSTR_STATUS bits.
};


/**
 * @brief Status flag masks used with @ref TSTR_STATUS.
 *
 * @see ::TSTR_STATUS
 * @see ::TSTR_ST_BITS
 */
enum TSTR_ST_MASKS
{
  TSTR_ST_UTC_OFFS_VALID = ( 1UL << TSTR_ST_BIT_UTC_OFFS_VALID ),
  TSTR_ST_IS_SYNC        = ( 1UL << TSTR_ST_BIT_IS_SYNC ),
  TSTR_ST_DL_ENB         = ( 1UL << TSTR_ST_BIT_DL_ENB ),
  TSTR_ST_DL_ANN         = ( 1UL << TSTR_ST_BIT_DL_ANN ),
  TSTR_ST_LS_ENB         = ( 1UL << TSTR_ST_BIT_LS_ENB ),
  TSTR_ST_LS_ANN         = ( 1UL << TSTR_ST_BIT_LS_ANN ),
  TSTR_ST_INV_TIME       = ( 1UL << TSTR_ST_BIT_INV_TIME )
};



/**
 * @brief Collection of data decoded from a time string.
 */
typedef struct
{
  struct tm tm;        ///< The decoded date and time.
  TSTR_STATUS status;  ///< Status information, see ::TSTR_ST_MASKS.
  int utc_offs;        ///< %UTC offset, valid if ::TSTR_ST_UTC_OFFS_VALID is set.
  MBG_TIME64_T t64;    ///< A full POSIX-like timestamp.

} MBG_TSTR_DATA;



/**
 * @brief Collection of data and state required to decoded a time string.
 */
struct MBG_TSTR_RCV_INFO_s
{
  char *buffer;              ///< Address of a receive buffer.
  size_t buffer_size;        ///< Size of the @p buffer.
  size_t bytes_received;     ///< Number of bytes currently in the @p buffer.
  bool first_char_received;  ///< Flag indicating that the first char of a string has been received.

  const MBG_TSTR_SPEC *tstr_spec;       ///< Specification of the expected time string format.
  int32_t rcv_delay_ns;                 ///< Expected delay of @p ts_rcv_raw in [ns], depending on transmission speed, etc.
  MBG_TSTR_RCV_CALLBACK_FNC *callback;  ///< Optional function to be called when string complete, or @a NULL.

  void *ext_data;            ///< Optional pointer to some app-specific data, @a NULL if not used.

  MBG_SYS_TIME ts_rcv_raw;   ///< Receive time stamp, taken when the on-time character is detected.
  MBG_SYS_TIME ts_rcv_comp;  ///< Receive time stamp, compensated by the expected transmission delay in @p rcv_delay_ns.
  MBG_TSTR_DATA tstr_data;   ///< Data decoded from the current string in @p buffer.

  MBG_SYS_TIME ts_pps;       ///< Time stamp associated with a PPS slope.

  // FIXME TODO Should all or a few of the next fields be app-specific?
  // If yes, they could be passed via @p ext_data.

  #if SUPP_PREV_TSTR_DATA
    MBG_SYS_TIME prev_ts_rcv_raw;  ///< The previously taken raw receive time stamp.
    MBG_SYS_TIME prev_ts_rcv_comp; ///< The previously taken compensated receive time stamp.
    MBG_TSTR_DATA prev_tstr_data;  ///< The previously decoded data.
  #endif

  const char *info;          ///< Optional serial port info, usually @a NULL.
  const char *log_fn;        ///< Optional log file name, usually @a NULL.
};



// FIXME TODO Move inline functions elsewhere!

static __mbg_inline
bool is_valid_time( int hour, int minute, int sec )
{
  return ( hour >= 0 && hour < HOURS_PER_DAY ) &&
         ( minute >= 0 && minute < MINS_PER_HOUR ) &&
         ( sec >= 0 && sec < ( SECS_PER_MIN + 1 ) );  // Account for leap second 60!

}  // is_valid_time


static __mbg_inline
bool is_valid_short_date( int mday, int month )
{
  return ( mday >= 1 && mday <= 31 ) &&
         ( month >= 1 && month <= 12 );

}  // is_valid_short_date


static __mbg_inline
bool is_valid_short_year( int year )
{
  return ( year >= 0 && year <= 999 );

}  // is_valid_short_year


static __mbg_inline
bool is_valid_wday_1_7( int wday )
{
  return ( wday >= 1 && wday <= 7 );

}  // is_valid_wday_1_7



/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Print a time string to a string.
 *
 * Replace some control characters (e.g. STX, ETX, CR, LF)
 * by readable tags, e.g. "<stx>".
 *
 * @param[out] s        The string buffer to be filled.
 * @param[in]  max_len  Size of the output buffer for 0-terminated string.
 * @param[in]  ts       The time string to be printed.
 *
 * @return Length of the string in the buffer.
 */
 int snprint_time_str( char *s, size_t max_len, const char *ts ) ;

 /**
 * @brief Clear an @ref MBG_TSTR_DATA structure.
 *
 * @param[out]  p_data  Address of the structure to be cleared.
 */
 void mbg_tstr_clear_tstr_data( MBG_TSTR_DATA *p_data ) ;

 /**
 * @brief Decode date, day-of-week and time from some time string formats.
 *
 * Some time string formats use the same format for date, day-of-week,
 * and time, so we can use the same function to check and decode these.
 *
 * @param[out]  p_tm  Address of a <em>struct tm</em> to take the decoded data.
 * @param[in]   s     Start of the relevant part of a time string.
 *
 * @return  ::MBG_SUCCESS.
 */
 int decode_date_time_wday_mbg_std( struct tm *p_tm, const char *s ) ;

 /**
 * @brief Decode the contents of a "Meinberg Standard" time string.
 *
 * @param[in,out]  p_tsri  Address of the ::MBG_TSTR_RCV_INFO stucture to be evaluated.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_tstr_decode_mbg_std( MBG_TSTR_RCV_INFO *p_tsri ) ;

 /**
 * @brief Decode the contents of a "Uni Erlangen" time string.
 *
 * @param[in,out]  p_tsri  Address of the ::MBG_TSTR_RCV_INFO stucture to be evaluated.
 *
 * @return  ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_tstr_decode_uni_erl( MBG_TSTR_RCV_INFO *p_tsri ) ;

 /**
 * @brief Get the number of supported time string formats.
 *
 * @return The number of non-NULL entries in ::tstr_spec_table.
 */
 unsigned int mbg_tstr_get_fmt_cnt( void ) ;

 /**
 * @brief Set up the receive info structure for a specific time string.
 *
 * Check if the index @p tstr_idx is in range, store it and allocate
 * (or re-allocate) a receive buffer of @p rcv_buffer_size bytes.
 *
 * @param[in,out]  p_tsri           Address of the ::MBG_TSTR_RCV_INFO stucture to be updated.
 * @param[in]      tstr_idx         Index of the time string spec in ::tstr_spec_table.
 * @param[in]      rcv_buffer_size  Size of the receive buffer to be allocated.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES.
 */
 int mbg_tstr_setup_tstr_buffer( MBG_TSTR_RCV_INFO *p_tsri, unsigned int tstr_idx, size_t rcv_buffer_size ) ;

 /**
 * @brief Set up the expected transmission time for the receive case.
 *
 * The determined time depends on the receive FIFO threshold of the serial port,
 * the baud rate, framing, etc., and can be used determine the point in time
 * when the transmission was started.
 *
 * @param[in,out]  p_tsri      Address of the ::MBG_TSTR_RCV_INFO stucture to be updated.
 * @param[in]      sdev        Pointer to the serial device info.
 * @param[in]      n_adj_bits  The number of additional serial bits to be accounted for
 *                             when computing the full transmission time.
 * @return  ::MBG_SUCCESS.
 */
 int mbg_tstr_setup_rcv_delay( MBG_TSTR_RCV_INFO *p_tsri, const MBGSERIO_DEV *sdev, int n_adj_bits ) ;

 /**
 * @brief Register a receive callback function.
 *
 * The registered callback function is called to notifiy the application
 * when a time string has been successfully received, or when an error occured.
 *
 * @param[in,out]  p_tsri  Address of the ::MBG_TSTR_RCV_INFO stucture to be updated.
 * @param[in]      fnc     The callback function to be registered.
 *
 * @return  ::MBG_SUCCESS.
 */
 int mbg_tstr_set_rcv_callback( MBG_TSTR_RCV_INFO *p_tsri, MBG_TSTR_RCV_CALLBACK_FNC *fnc ) ;

 /**
 * @brief Receive characters of a time string.
 *
 * This function should be called for every character that
 * has been been received, to check if it matches the
 * expected time string format.
 *
 * @param[in]      c       A received character.
 * @param[in,out]  p_tsri  Address of a time string receive info structure.
 *
 * @return  ::MBG_SUCCESS if the character was accepted,<br>
 *          ::MBG_ERR_RANGE if the character was out of range, not accepted,<br>
 *          ::MBG_ERR_OVERFLOW if the receive buffer overflows,<br>
 *          ::MBG_ERR_STR_LEN if the string lenght doesn't match, or<br>
 *          ::MBG_ERR_DATA_FMT if the received string isn't appropriate for the template.
 */
 int mbg_tstr_receive( unsigned char c, MBG_TSTR_RCV_INFO *p_tsri ) ;


/* ----- function prototypes end ----- */


/**
 * @brief Characteristics of a time string format.
 */
struct MBG_TSTR_SPEC_s
{
  const char *name;          ///< Name of the time string format.
  const char *shortName;     ///< Short name of the string format.

  const char *str_template;  ///< A template of a generic time string format.

  char skip_char;     ///< Indicates which character in @p str_template is to be
                      ///< skipped when checking the general time string format.

  char first_char;    ///< The character marking the beginning of a string, e.g. @a STX. 0 if unused.
  char ontime_char;   ///< Character of the string at which an associated time is valid, e.g. @a STX or @a CR. 0 if unused.
  char term_char;     ///< The character marking the end of a string, e.g. @a CR, @a LF, or @a ETX. 0 if unused.
  size_t str_length;  ///< The standard length of the time string. 0 if length is variable.

  /// An optional function to be called to check if the basic string format matches, or @a NULL.
  MBG_TSTR_CHK_FNC *check_fnc;

  /// An optional function to be called to decode the time from the string, or @a NULL.
  MBG_TSTR_DECODE_FNC *decode_fnc;
};



/**
 * @brief Specification of the Meinberg Standard time string format.
 */
_ext const MBG_TSTR_SPEC tstr_spec_mbg_standard
#ifdef _DO_INIT
 = {
     "Meinberg Standard",
     "Meinberg Std.",
     STX_STR "D:__.__.__;T:_;U:__.__.__;____" ETX_STR,
     '_',
     STX_CHAR,
     STX_CHAR,
     ETX_CHAR,
     32,
     NULL,
     mbg_tstr_decode_mbg_std
   }
#endif
;


/**
 * @brief Specification of the Uni Erlangen time string format.
 */
_ext const MBG_TSTR_SPEC tstr_spec_uni_erlangen
#ifdef _DO_INIT
 = {
     "Uni Erlangen",
     "Uni Erlangen",
     STX_STR "__.__.__; _; __:__:__; ___:__; _____ _;___._____ ___._____ _____" ETX_STR,
     '_',
     STX_CHAR,
     STX_CHAR,
     ETX_CHAR,
     66,
     NULL,
     mbg_tstr_decode_uni_erl
   }
#endif
;



/**
 * @brief A table of supported time string formats.
 */
_ext const MBG_TSTR_SPEC *tstr_spec_table[]
#ifdef _DO_INIT
  =
  {
    &tstr_spec_mbg_standard,
    &tstr_spec_uni_erlangen,
    NULL   // terminate table
  }
#endif
;



#ifdef __cplusplus
}
#endif

/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_TSTR_H */






