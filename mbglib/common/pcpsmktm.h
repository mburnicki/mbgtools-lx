
/**************************************************************************
 *
 *  $Id: pcpsmktm.h 1.3 2019/09/27 12:17:19 martin REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for pcpsmktm.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: pcpsmktm.h $
 *  Revision 1.3  2019/09/27 12:17:19  martin
 *  Changed pcps_mktime() to pcps_mktime64() with calling convention
 *  similar to mbg_mktime64().
 *  Revision 1.2  2017/07/05 08:18:54  martin
 *  Cleaned up to conform to standard header file format.
 *  Updated function prototypes.
 *  Revision 1.1  2001/02/02 15:31:07  MARTIN
 *
 **************************************************************************/

#ifndef _PCPSMKTM_H
#define _PCPSMKTM_H


/* Other headers to be included */

#include <pcpsdefs.h>

#include <timeutil.h>
#include <mbgtime.h>
#include <mbgerror.h>



#ifdef _PCPSMKTM
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Compute a linear ::MBG_TIME64_T value from ::PCPS_TIME.
 *
 * This function basically works like the POSIX @a mktime function but
 * expects the date and time to be converted in ::PCPS_TIME format.
 *
 * Unlike POSIX @a mktime, which applies a local time offset specified
 * as environment variable that is read by the runtime library, this
 * function applies the %UTC offset from ::PCPS_TIME::offs_utc when
 * converting local time to %UTC.
 *
 * Unlike @a mktime, this function expects the address of a variable
 * of an ::MBG_TIME64_T to take the computed timestamp, and returns
 * a result code indicating if the conversion was successful, or not.
 * So @a -1 can be a valid timestamp, refering to the time
 * one second before the epoch.
 *
 * @note Since ::PCPS_TIME::year actually just contains the year
 * of the century, the year number needs to be extended before a
 * timestamp can be calculated.
 *
 * @param[out] p_t64  Address of a variable to take the result on success,
 *                    i.e. seconds since 1970-01-01 (POSIX @a time_t format,
 *                    but always 64 bits).
 *
 * @param[in]  p_tm   Pointer to a ::PCPS_TIME type providing the date
 *                    and time to be converted.
 *
 * @param[in]  year_lim  The year limit used when converting a 2 digit year number
 *                       to a full 4 digit year number. See ::mbg_exp_year.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
 int pcps_mktime64( MBG_TIME64_T *p_t64, const PCPS_TIME *p_tm, int year_lim ) ;


/* ----- function prototypes end ----- */



static __mbg_inline /*HDR*/
/**
 * @brief Compute a linear time_t value from broken down date and time.
 *
 * This function is a variant of ::pcps_mktime64, but expects
 * the address of a @a time_t variable for the result.
 * See ::pcps_mktime64 for a detailed description.
 *
 * On systems with a 64 bit @a time_t the result is the same as for
 * ::pcps_mktime64, but on systems where @a time_t is only 32 bits wide,
 * the returned value is truncated and suffers from the <b>Y2038 problem</b>.
 *
 * Use ::pcps_mktime64 preferably to compute a timestamp
 * which is always 64 bits wide.
 *
 * @note Since ::PCPS_TIME::year actually just contains the year
 * of the century, the year number needs to be extended before a
 * timestamp can be calculated.
 *
 * @param[out] p_t   Address of a variable to take the result on success,
 *                   i.e. seconds since 1970-01-01 (POSIX @a time_t format).
 *
 * @param[in]  p_tm  Pointer to a ::PCPS_TIME type providing the date
 *                   and time to be converted.
 *
 * @param[in]  year_lim  The year limit used when converting a 2 digit year number
 *                       to a full 4 digit year number, e.g. 1970.
 *                       See ::mbg_exp_year.
 *
 * @return ::MBG_SUCCESS on success, else one of the @ref MBG_ERROR_CODES
 *
 * @ingroup mbg_mktime_fncs
 * @see @ref mbg_mktime_fncs
 */
int _DEPRECATED_BY( "pcps_mktime64" ) pcps_mktime( time_t *p_t, const PCPS_TIME *p_tm, int year_lim )
{
  MBG_TIME64_T t64;

  int rc = pcps_mktime64( &t64, p_tm, year_lim );

  // On success we still check if the result is in a valid range
  // and convert the result, if required.
  if ( mbg_rc_is_success( rc ) )
    rc = mbg_trnc_time64_t_to_time_t( p_t, &t64 );

  return rc;

}  // pcps_mktime


#ifdef __cplusplus
}
#endif

/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _PCPSMKTM_H */
