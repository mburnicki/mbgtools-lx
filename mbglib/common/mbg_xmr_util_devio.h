
/**************************************************************************
 *
 *  $Id: mbg_xmr_util_devio.h 1.3 2022/12/21 15:26:03 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Definitions and prototypes for mbg_xmr_util_devio.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: mbg_xmr_util_devio.h $
 *  Revision 1.3  2022/12/21 15:26:03  martin.burnicki
 *  Removed an obsolete function parameter.
 *  Revision 1.2  2022/06/30 09:47:42  martin.burnicki
 *  Updated function prototypes.
 *  Revision 1.1  2022/06/24 12:54:29  martin.burnicki
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _MBG_XMR_UTIL_DEVIO_H
#define _MBG_XMR_UTIL_DEVIO_H


/* Other headers to be included */

#include <deviohlp.h>
#include <cfg_hlp.h>


#ifdef _MBG_XMR_UTIL_DEVIO
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#ifdef __cplusplus
extern "C" {
#endif


/* function prototypes: */

/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Force-read all XMR info into a newly or re-allocated ::ALL_XMULTI_REF_INFO.
 *
 * @note This variant of ::mbg_get_all_xmulti_ref_info function bypasses the check
 * whether XMR is supported by the device, and accesses the device in any case.
 * Calling this function with a device that generally doesn't support the XMR feature
 * may cause the device and even the whole computer to hang.
 *
 * An ::ALL_XMULTI_REF_INFO and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_INFO_IDX and ::XMR_EXT_SRC_INFO_IDX will be allocated and need
 * to be freed by calling ::free_all_xmulti_ref_info.
 *
 * @param[in]   dh  Valid handle to a Meinberg device.
 * @param[out]  p   Pointer to a pointer of ::ALL_XMULTI_REF_INFO.
 *
 * @return One of the @ref MBG_RETURN_CODES.
 *
 * @see ::free_all_xmulti_ref_info
 */
 int mbg_get_all_xmulti_ref_info_forced( MBG_DEV_HANDLE dh, ALL_XMULTI_REF_INFO **p ) ;

 /**
 * @brief Force-read all XMR status info into a newly or re-allocated ::ALL_XMULTI_REF_STATUS.
 *
 * @note This variant of ::mbg_get_all_xmulti_ref_status function bypasses the check
 * whether XMR is supported by the device, and accesses the device in any case.
 * Calling this function with a device that generally doesn't support the XMR feature
 * may cause the device and even the whole computer to hang.
 *
 * An ::ALL_XMULTI_REF_STATUS and a number of ::XMULTI_REF_INSTANCES::n_xmr_settings
 * of ::XMULTI_REF_STATUS_IDX will be allocated and need to be freed by calling
 * ::free_all_xmulti_ref_status.
 *
 * @param[in]   dh    Valid handle to a Meinberg device.
 * @param[in]   info  Pointer to the appropriate info structure.
 * @param[out]  p     Pointer to a pointer of ::ALL_XMULTI_REF_STATUS.
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::free_all_xmulti_ref_status
 */
 int mbg_get_all_xmulti_ref_status_forced( MBG_DEV_HANDLE dh, const ALL_XMULTI_REF_INFO *info, ALL_XMULTI_REF_STATUS **p ) ;

 /**
 * @brief Read and show all XMR status info.
 *
 * @note If @p force is true, the device is accessed to read the XMR data
 * even if the feature flag is not set. Use this very carefully only:
 * Doing so with a device that generally doesn't support the XMR feature
 * may cause the device and even the whole computer to hang!

 * @param[in]  dh               Valid handle to a Meinberg device.
 * @param[in]  info             An optional header string to print, may be @a NULL.
 * @param[in]  indent_str       An optional indentation string, may be @a NULL.
 * @param[in]  force            If @a true, force-read XMR data even if the feature flag is not set.
 *
 * @return One of the @ref MBG_RETURN_CODES
 *
 * @see ::print_all_xmr_status
 */
 void show_all_xmr_status( MBG_DEV_HANDLE dh, const char *info, const char *indent_str, bool force ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


/* End of header body */

#undef _ext
#undef _DO_INIT

#endif  /* _MBG_XMR_UTIL_DEVIO_H */
