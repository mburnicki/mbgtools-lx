
/**************************************************************************
 *
 *  $Id: ptp_util.h 1.8 2022/12/21 16:03:43 martin.burnicki REL_M $
 *
 *  Copyright (c) Meinberg Funkuhren, Bad Pyrmont, Germany
 *
 *  Description:
 *    Definitions and prototypes for ptp_util.c.
 *
 * -----------------------------------------------------------------------
 *  $Log: ptp_util.h $
 *  Revision 1.8  2022/12/21 16:03:43  martin.burnicki
 *  Updated some comments.
 *  Revision 1.7  2021/03/16 12:20:39  martin
 *  Updated some comments.
 *  Revision 1.6  2017/05/10 15:26:10  martin
 *  Tiny cleanup.
 *  Revision 1.5  2017/04/25 12:54:21  gregoire.diehl
 *  Merge changes from 1.3.1.7 & 1.4
 *  Revision 1.4  2016/11/10 09:05:26Z  martin
 *  Support for PTP Time Monitor role added by udo.
 *  Support for NTP modes added by daniel.
 *  Updated some comments.
 *  Revision 1.3  2013/09/12 11:05:44  martin
 *  Added inline function get_supp_ptp_role_mask().
 *  Revision 1.2  2013/08/06 12:08:06  udo
 *  new ptp_clock_id_from_str()
 *  Revision 1.1  2011/11/14 16:03:39  martin
 *  Initial revision.
 *
 **************************************************************************/

#ifndef _PTP_UTIL_H
#define _PTP_UTIL_H


/* Other headers to be included */
#include <stdio.h>
#include <lan_util.h>
#include <gpsdefs.h>


#ifdef _PTP_UTIL
 #define _ext
 #define _DO_INIT
#else
 #define _ext extern
#endif


/* Start of header body */

#if defined( _USE_PACK )
  #pragma pack( 1 )      // Set byte alignment.
  #define _USING_BYTE_ALIGNMENT
#endif

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Derive a "supported PTP roles" bit mask from ::PTP_CFG_INFO::supp_flags.
 *
 * The relevant bits used with ::PTP_CFG_INFO::supp_flags have not been
 * defined sequentially, so we need to test them individually to put a
 * supported roles bit mask together.
 *
 * @note Originally, all devices supported the multicast slave role, so
 * there was no extra flag to indicate this. However, some newer devices
 * may not support the multicast slave role, so two new flags have been
 * introduced to cope with this:<br>
 * If ::PTP_CFG_SUPP_MCAST_SLAVE_FLAG is set, a different flag
 * ::PTP_CFG_CAN_BE_MULTICAST_SLAVE needs to be checked to tell if
 * the multicast slave role is supported, or not.<br>
 * If ::PTP_CFG_SUPP_MCAST_SLAVE_FLAG is not set, the device
 * definitely supports the multicast slave role.
 *
 * @param flags bit mask from ::PTP_CFG_INFO::supp_flags, see @ref PTP_CFG_FLAG_MASKS.
 *
 * @return bit mask of supported PTP roles, see ::PTP_ROLE_MASKS.
 */
static __mbg_inline
uint32_t get_supp_ptp_role_mask( uint32_t flags )
{
  uint32_t role_mask = 0;

  if ( flags & PTP_CFG_MSK_SUPP_MCAST_SLAVE_FLAG )
  {
    // multicast slave role is only supported
    // if a different flag is also set
    if ( flags & PTP_CFG_MSK_CAN_BE_MULTICAST_SLAVE )
      role_mask |= PTP_ROLE_MSK_MULTICAST_SLAVE;
  }
  else  // multicast slave role is definitely supported
    role_mask |= PTP_ROLE_MSK_MULTICAST_SLAVE;

  if ( flags & PTP_CFG_MSK_CAN_BE_UNICAST_SLAVE )
    role_mask |= PTP_ROLE_MSK_UNICAST_SLAVE;

  if ( flags & PTP_CFG_MSK_CAN_BE_MULTICAST_MASTER )
    role_mask |= PTP_ROLE_MSK_MULTICAST_MASTER;

  if ( flags & PTP_CFG_MSK_CAN_BE_UNICAST_MASTER )
    role_mask |= PTP_ROLE_MSK_UNICAST_MASTER;

  if ( flags & PTP_CFG_MSK_CAN_BE_MULTICAST_AUTO )
    role_mask |= PTP_ROLE_MSK_MULTICAST_AUTO;

  if ( flags & PTP_CFG_MSK_CAN_BE_BOTH_MASTER )
    role_mask |= PTP_ROLE_MSK_BOTH_MASTER;

  if ( flags & PTP_CFG_MSK_NTP_HW_TS_MASTER )
    role_mask |= PTP_ROLE_MSK_NTP_SERVER;

  if ( flags & PTP_CFG_MSK_NTP_HW_TS_SLAVE )
    role_mask |= PTP_ROLE_MSK_NTP_CLIENT;

  if ( flags & PTP_CFG_MSK_CAN_BE_TIME_MONITOR )
    role_mask |= PTP_ROLE_MSK_TIME_MONITOR;

  if ( flags & PTP_CFG_MSK_CAN_BE_V1_MASTER )
    role_mask |= PTP_ROLE_MSK_V1_MASTER;

  if ( flags & PTP_CFG_MSK_CAN_BE_V1_SLAVE )
    role_mask |= PTP_ROLE_MSK_V1_SLAVE;

  return role_mask;

}  // get_supp_ptp_role_mask



/* ----- function prototypes begin ----- */

/* This section was generated automatically */
/* by MAKEHDR, do not remove the comments. */

 /**
 * @brief Get the MAC addr from a PTP clock ID
 *
 * The clock ID is usually the MAC ID with 2 octets
 * 0xFF and 0xFE inserted in the middle.
 *
 * @param p_mac_addr  The MAC ID to be filled up
 * @param p_clock_id  The PTP clock ID
 */
 void mac_from_ptp_clock_id( MBG_MAC_ADDR *p_mac_addr, const PTP_CLOCK_ID *p_clock_id ) ;

 /**
 * @brief Get the PTP clock ID from the MAC addr
 *
 * The clock ID is usually the MAC ID with 2 octets
 * 0xFF and 0xFE inserted in the middle.
 *
 * @param p_clock_id  The PTP clock ID
 * @param p_mac_addr  The MAC ID to be filled up
 */
 void ptp_clock_id_from_mac( PTP_CLOCK_ID *p_clock_id, const MBG_MAC_ADDR *p_mac_addr ) ;

 /**
 * @brief Get the PTP clock ID from a string
 *
 * The clock ID is usually the MAC ID with 2 octets
 * 0xFF and 0xFE inserted in the middle.
 *
 * @param p_clock_id  The PTP clock ID
 * @param p_str       The UUID as string with format e.g. 0050C2FFFED287DE
 */
 void ptp_clock_id_from_str( PTP_CLOCK_ID *p_clock_id, const char *p_str ) ;


/* ----- function prototypes end ----- */

#ifdef __cplusplus
}
#endif


#if defined( _USING_BYTE_ALIGNMENT )
  #pragma pack()      // Set default alignment.
  #undef _USING_BYTE_ALIGNMENT
#endif

/* End of header body */


#undef _ext
#undef _DO_INIT

#endif  /* _PTP_UTIL_H */

